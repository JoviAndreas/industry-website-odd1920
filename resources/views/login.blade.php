<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Standard Meta -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <!-- Site Properties -->
  <title>BINUS</title>
  <link rel="icon" type="image/png" href="{{ URL::asset('assets/images/favicon.png') }}">
  <link rel="stylesheet" type="text/css" href="{{ url('/') }}/semantic/dist/semantic.min.css">
  
  <script type="text/javascript" src="{{ url('/') }}/semantic/dist/jquery.js"></script>
  <script src="{{ url('/') }}/semantic/dist/semantic.min.js"></script>

  <style type="text/css">
    body {
      background-color: #ffffff;
    }
    body > .grid {
      height: 100%;
    }
    .column {
      max-width: 450px;
    }
    .ribbon {
      display: block;
      z-index: 1;
      margin-right: 10px;
      width: 35px;
      height: 107px;
      background: url('https://binusmaya.binus.ac.id/newDefault/images/login/ribbon.png');
    }
    .segment {
      margin-bottom: 10rem !important;
      padding-top: 0 !important;
      padding-bottom: 3rem !important;
    }
    .shadow {
      box-shadow: 0 .2rem 1rem rgba(0,0,0,.15) !important;
    }
    .binus-header {
      text-align: left;
      display: flex;
      align-items: center;
    }
    ul {
      list-style: none;
      text-decoration: none;
      padding: 0;
    }
  </style>
  <script>
  $(document).ready(
    function(){
      $('.ui.form').form(
        {
          fields: {
            userid: {
              identifier  : 'userid',
              rules: [
                {
                  type   : 'empty',
                  prompt : 'Please enter your username'
                }
              ]
            },
            password: {
              identifier  : 'password',
              rules: [
                {
                  type   : 'empty',
                  prompt : 'Please enter your password'
                },
                {
                  type   : 'length[6]',
                  prompt : 'Your password must be at least 6 characters'
                }
              ]
            }
          }
        }
      );  
    }
  );
  </script>
</head>
<body>
  <div class="ui middle aligned center aligned grid">
    <div class="row">
      <div class="column">
        {!! Form::open(['url' => 'auth/login', 'method' => 'post', 'role' => 'form', 'class' => 'ui large form error']) !!}
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="ui segment shadow">
            <div class="binus-header">
              <div class="ribbon"></div>
              <div class="ui image image-header">
                <img src="{{ url('/') }}/assets/images/logo.png" class="image">
              </div>
            </div>
            <h3 class="ui blue image header">
              <div class="content">
                Internship Recruitment Apps
              </div>
            </h3>
            <div class="field">
              <div class="ui left icon input">
                <i class="user icon"></i>
                <input type="text" name="userid" placeholder="Username / NIM">
              </div>
            </div>
            <div class="field">
              <div class="ui left icon input">
                <i class="lock icon"></i>
                <input type="password" name="password" placeholder="Password">
              </div>
            </div>
            <div class="ui fluid large blue submit button">Login</div>
            <div class="ui error message">{{ Session::get('messageError') }}</div>
            @if (count($errors) > 0)
              <div class="ui error message">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
            @endif
          </div>
        {!! Form::close() !!}
      </div>
    </div>
{{--    <div class="row">--}}
{{--      <div class="column">--}}
{{--        <div class="ui text container segment">--}}
{{--          If you want to participate in Binus 3+1 Internship Track, Register  <a href="{{ url('/signin') }}">here  </a>!--}}
{{--        </div>--}}
{{--        <div class="ui primary button showAttention">Attention!</div>--}}
{{--      </div>--}}
{{--    </div>--}}
  </div>

<div class="ui modal attention">
  <div class="header">
    Attention
  </div>
  <div class="content">
    <div class="ui warning message">Please use the latest version of Chrome</div>
    <div class="ui warning message">If you fail to change your profile with our guidelines, try to clear browser files and images stored in the cache</div>
    <div class="ui warning message">If you can't preview your CV, please re-submit your profile by go to Profile Page -> Change/Update Profile</div>
  </div>
  <div class="actions">
    <div class="ui red deny button">
      Close
    </div>
  </div>
</div>
<script>
  $('.showAttention').click(function(e) {  
    $('.ui.modal.attention').modal('show');
  });
</script>
</body>
</html>