@extends ('layout.masterC')

@section ('title', 'BINUS')

@section('content')
  <div class="ui inverted vertical center aligned segment invis">
    <div class="ui container">
      <div class="ui large secondary inverted pointing menu">
        <a class="toc item">
          <i class="sidebar icon"></i>
        </a>
        <a class="item" href="{{ url('/') }}/homec">Home</a>
        <a class="item" href="{{ url('/') }}/profilec/{{ $user->userid }}">Profile</a>
        <div class="ui pointing dropdown link item">
              <span class="text">Learning Plan</span>
              <i class="dropdown icon"></i>
              <div class="ui menu">
                <a href="{{ url('/') }}/job" class="item">Job</a>
                <a href="{{ url('/') }}/add" class="active item">Learning Plan</a>
              </div>
        </div>
        <a class="item" href="{{ url('/') }}/feedbacks">Feedback</a>
        <div class="right item">
          <div class="item" style="padding-top:0px; padding-bottom: 0px;">
            <a href="{{ url('/') }}/mailbox">
              <i id="mailboxLink" class="middle aligned mail outline large icon"></i>
            </a>
          </div>
          
         
          <div class="item" style="padding-top:0px; padding-bottom: 0px;">
            <span id="notification_count">3</span>
            <i class="middle aligned info circle large icon" id="notificationLink" style="cursor:pointer;"></i>
            <div id="notificationContainer">
            <div id="notificationTitle">Notifications</div>
            <div id="notificationsBody" class="notifications ">
              <div class="ui relaxed divided list">
                <div class="item">
                  <i class="large circle middle aligned icon"></i>
                  <div class="content">
                    <a class="header">Semantic-Org/Semantic-UI</a>
                    <div class="description">Updated 10 mins ago</div>
                  </div>
                </div>
                <div class="item">
                  <i class="large circle middle aligned icon"></i>
                  <div class="content">
                    <a class="header">Semantic-Org/Semantic-UI</a>
                    <div class="description">Updated 10 mins ago</div>
                  </div>
                </div>
                <div class="item">
                  <i class="large circle middle aligned icon"></i>
                  <div class="content">
                    <a class="header">Semantic-Org/Semantic-UI</a>
                    <div class="description">Updated 10 mins ago</div>
                  </div>
                </div>
              </div>
            </div>
            <div id="notificationFooter"><a href="asd">See All</a></div>
            </div>
          </div>

          <a class="ui inverted button" href="{{ url('/') }}/auth/logout">Log Out</a>
        </div>
      </div>
    </div>
  </div>

  <div class="ui vertical stripe segment">
    <div class="ui middle aligned stackable grid container">

      <div class="row">
          <h3>Insert Learning Plan by : </h3>
      </div>

      
      <div class="row">
        <div class="ui grid">
          <div class="five wide">
            <select class="ui dropdown" name="insertLP" id="LPMode">
            <option value="">-Choose One-</option>
            <option value="1">Company</option>
            <option value="2">Position</option>
            <option value="3">Student</option>
          </select>
          </div>
          <div class="eleven wide center aligned" id="LPDesc" style="font-size: 20px;padding-top:1.25%;padding-bottom:1.25%;"></div>
        </div>
      </div>

      <div class="row">
        <div class="ui small blue submit button">Save</div>
      </div>

      <div class="row">
       <table class="ui green striped selectable table" id="accept_table">
          <thead>
            <tr>
              <th class="two wide">NIM</th>
              <th class="three wide">Nama</th>
              <th class="two wide">Phone</th>
              <th class="three wide">Position</th>
              <th class="three wide">Company</th>
              <th class="three wide">Action</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1701303711</td>
              <td>Timothy Agustian</td>
              <td>085883522917</td>
              <td class="ui tr"  data-title="Job Description:" data-content="Confirms project requirements by reviewing program objective, input data, and output requirements with analyst, supervisor, and client.Arranges project requirements in programming sequence by analyzing requirements; preparing a work flow chart and diagram using knowledge of computer capabilities, subject matter, programming language, and logic.Encodes project requirements by converting work flow information into computer language.">Programmer</td>
              <td>Pt. Freeprot</td>
              <td> <div class="ui button">Insert Learning plan</div> </td>
            </tr>
            <tr>
              <td>1801801801</td>
              <td>Budi Anduk</td>
              <td>081219738221</td>
              <td class="ui tr"  data-title="Job Description:" data-content="Confirms project requirements by reviewing program objective, input data, and output requirements with analyst, supervisor, and client.Arranges project requirements in programming sequence by analyzing requirements; preparing a work flow chart and diagram using knowledge of computer capabilities, subject matter, programming language, and logic.Encodes project requirements by converting work flow information into computer language.">Designer</td>
              <td>Pt. Protprot</td>
              <td> <div class="ui button">Insert Learning plan</div> </td>
            </tr>
          </tbody>
        </table>
      </div>

      <div class="ui horizontal divider header">Job Description</div>
      <div class="row">
        <select name="skills" class="ui fluid dropdown">
          <option value="">Describe the Job Position and Job Description during enrichment.</option>
          <option value="1">Junior Web Programmer</option>
          <option value="2">Junior Mobile Programmer</option>
          <option value="3">Junior Network Programmer</option>
          <option value="4">Junior Network Infrastucture</option>
          <option value="5">Junior Database Administrator</option>
          <option value="6">Junior Multimedia Programmer</option>
          <option value="7">Junior Game Programmer</option>
        </select>
      </div>

      <div class="ui horizontal divider header">Learning Objective</div>
      <div class="row">
        <select name="skills" multiple="" class="ui fluid dropdown">
          <option value="">Area of Technical Competency</option>
          <option value="1">User Requirement gathering</option>
          <option value="2">Analysis of user requirements to find problems and solutions</option>
          <option value="3">Application design according user requirements to solve problems</option>
          <option value="4">Interface & UX design of application</option>
          <option value="5">Code and develop application (network, multimedia, web, desktop, mobile) according to the application design</option>
          <option value="6">Database Design for the application</option>
          <option value="7">Application testing (Quality Assurance)</option>
          <option value="8">Application implementation / publishing</option>
          <option value="9">Database administration</option>
          <option value="10">Network infrastucture design</option>
          <option value="11">Network administration</option>
          <option value="12">Applying software development metodology / process</option>
        </select>
      </div>

    </div>
  </div>

  <input type="hidden" value="<?php $a = ['Sales' => 3, 'Research' => 5, 'Accounting' => 2]; echo str_replace('"', '&quot;', json_encode($a)); ?>" id="dataTesting" />
@stop