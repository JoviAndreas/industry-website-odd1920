@extends ('layout.master')

@section ('title', 'BINUS')

@section('content')

@if (Auth::user()->role=='department')
  @include('partial/headerDepartment')
@elseif (Auth::user()->role=='company')
  @include('partial/headerCompany')
@else
  @include('partial/headerStudent')
@endif

<br><br>

@if (session('fyi'))
<div class="row">
    <div class="ui positive message">
        <i class="close icon"></i>
        <div class="header">
            {{ session('fyi') }}
        </div>
    </div>
</div>
@endif
<div class="ui stackable grid container">
    <div class="four wide column">
        <img style="max-height: 375px; max-width: 260px;" src="{{ url('images/'.$userProfile->companyid) }}" onerror="this.src='{{ URL::asset('assets/images/noimage.jpg') }}'">
    </div>
    <div class="twelve wide column">
        <div class="row">
            <h1>{{ $userProfile->name }}</h1>
            @if($company_user_status == 'nonactive')
            <span class="ui large label orange">This company is closed.</span>
            @endif
            @if(Auth::user()->userid == $userProfile->userid)
            <a class="ui button" href="{{ url('/') }}/updateprofiles" >
              Change/Update Profile
            </a>
            <a class="ui button" href="{{ url('/') }}/changepassword" >
              Change Password
            </a>
            @endif
        </div>
        <br><br>
        @if(Auth::user()->userid == $userProfile->userid)
        <div class="row">
            <div class="ui segment" style="padding: 1%;">
                <div class="row">
                    <strong>Current Company State: <span style="color: blue;">
                        @if($userProfile->state != 'closed')
                        OPEN
                        @else
                        CLOSED
                        @endif
                    </span></strong>
                </div>
                <hr>
                Change Company State to<br>
                <div class="row">
                @if($userProfile->state != 'closed')
                <a href="{{ url('/') }}/closeCompany" class="ui red button" onclick="return confirm('Are you sure you want to change state to CLOSED ?')">CLOSED</a>
                @endif

                @if($userProfile->state != 'open')
                <a href="{{ url('/') }}/openCompany" class="ui green button" onclick="return confirm('Are you sure you want to change state to OPEN ?')">OPEN</a>
                @endif
                </div>
                <hr>
                <ul>
                    <li>If you change to <span style="color: red;">CLOSED</span>, students cannot apply to your company, and you only could see the accepted students</li>

                    <li>If you change to <span style="color: green;">OPEN</span> students could apply to your company and you could process job applications</li>
                </ul>
            </div>
        </div>
        <br><br>
        @endif
        @if (Auth::user()->role=='department' && \App\Department::isSuperAdmin())
        <div class="row">
            <div class="ui segment" style="padding: 1%;">
                <div class="row"><h4>Contact Me</h4></div>
                <br>
                <div class="ui grid" style="padding-bottom: 0 auto; padding-top: 0 auto;">
                    <div class="three wide column">Contact Person</div>
                    <div class="twelve wide column">{{ $userProfile->pic }}</div>
                </div>
                <div class="ui grid" style="padding-bottom: 0 auto; padding-top: 0 auto;">
                    <div class="three wide column">Email</div>
                    <div class="twelve wide column"><a href="mailto:{{ $userProfile->email }}" target="_top">{{ $userProfile->email }}</a></div>
                </div>
                <div class="ui grid" style="padding-bottom: 0 auto; padding-top: 0 auto;">
                    <div class="three wide column">Phone</div>
                    <div class="twelve wide column">{{ $userProfile->phone }}</div>
                </div>
                <div class="ui grid" style="padding-bottom: 0 auto; padding-top: 0 auto;">
                    <div class="three wide column">Address</div>
                    <div class="twelve wide column">{{ $userProfile->address }}</div>
                </div>

                <div class="ui grid" style="padding-bottom: 0 auto; padding-top: 0 auto;">
                    <div class="three wide column">Business Category</div>
                    <div class="twelve wide column">{{ $userProfile->business_category ?? '-' }}</div>
                </div>
            </div>
        </div>
        <br><br>
        @endif
        <div class="row">
            <div class="ui segment" style="padding: 1%;">
                <div class="row"><h4>Social Network</h4></div>
                <br>
                <div class="ui grid" style="padding-bottom: 0 auto; padding-top: 0 auto;">
                    <div class="three wide column">Website</div>
                    <div class="twelve wide column">{!!Html::link($userProfile->website, null, array('target'=>'_blank'))!!}</div>
                </div>
            </div>
        </div>
    </div>
</div>

<br><br>

<div class="ui small modal confirmation">
  <div class="header">Confirmation</div>
  <div class="content">
    <p id="contentApllicant">Are you sure you want to apply to xx as a xxx?</p>
  </div>
  <div class="actions">
    <div class="ui approve green button">Yes</div>
    <div class="ui cancel red button">No</div>
  </div>
</div>

@stop
