@extends ('layout.masterC')

@section ('title', 'BINUS')

@section('content')

@include('partial/headerCompany')

 
<center>
<br><br>
<h2>Feedback</h2>
<br>

{!! Form::open(['url' => 'submitFeedbacks', 'method' => 'post', 'role' => 'form', 'class' => 'ui large feedback form']) !!}
<div class="ui center aligned container">
   <textarea name="txtfeed" placeholder="" required maxlength="500">{{ old('txtfeed') }}</textarea>
</div>


<br><br>
<button class="ui primary button" type="submit">Submit Feedback</button>

 {!! Form::close() !!}

  @if($errors->first() != null)
  <div class="ui red message">{{$errors->first()}}</div>
@endif
<br><br>
      @if (session('fyi'))
      <div class="row">
        @if (session('fyi') == 'Berhasil')
          <div class="ui positive message">
            <i class="close icon"></i>
            <div class="header">
              You have successfully submit your feedback.
            </div>
          </div>
        @endif
      @endif

</center>


@stop