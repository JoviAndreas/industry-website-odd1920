@extends ('layout.masterC')

@section ('title', 'BINUS')

@section('content')

@include('partial/headerCompany')

<br><br>
<div class="ui container">
  <div class="ui basic segment">
    <div class="ui grid">

      @if (session('fyi'))
      <div class="row">

        @if (session('fyi') == 'Berhasil')
          <div class="ui positive message">
            <i class="close icon"></i>
            <div class="header">
              You have successfully assign supervisor.
            </div>
          </div>
        @elseif (session('fyi') == 'Berhasilupdate')
        <div class="ui positive message">
          <i class="close icon"></i>
          <div class="header">
            You have successfully updated supervisor.
          </div>
        </div>
        @else
          <div class="ui negative message">
            <i class="close icon"></i>
            <div class="header">
              {{ session('fyi') }}
            </div>
          </div>
        @endif

      </div>
      @endif

      <div class="row">
        <a href="{{ url('/') }}/supervisor">
          <div class="ui gray label">
            <i class="big arrow circle left icon"></i>
            Back to Supervisor List
          </div>
        </a>
        &nbsp;&nbsp;
        <a href="{{ url('/') }}/listSupervisor">
          <div class="ui primary button interview" data-content="List of Student with Supervisor">
            <i class="download icon"></i>
            Download to Excel
          </div>
        </a>
      </div>

      <div class="row">
          <div class="column">
              Total Approved Student:<br>
              <span class="ui large label">
                  {{ $total_approved }} student(s)
              </span>
          </div>
      </div>

      <div class="row">
        <div class="column">
          <table class="ui table">
            <thead>
              <tr>
                <th class="three wide">Supervisor Name</th>
                <th class="three wide">Supervisor Position</th>
                <th class="ten wide">List Student</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($listspv as $spv)
              <tr>
                <td>{{ $spv->name }}</td>
                <td>{{ $spv->position }}</td>
                <td>
                    Total Student: {{ $spv->list_student->count() }}<br>
                    <div class="list_student" spv_id="{{ $spv->id }}">
                        @if($spv->list_student->count() > 0)
                            <ul>
                            @foreach($spv->list_student as $st)
                                <li>
                                    <a href="{{ url('/') }}/profile/{{ $st->studentid }}">{{ $st->studentid }}</a> - {{ $st->name }}
                                    &nbsp;
                                    <a href="/removeStudentFromSpv/{{ $spv->id }}/{{ $st->studentid }}">Remove</a>
                                </li>
                            @endforeach
                            </ul>
                        @endif
                    </div>
                    @if($listStudent->count() > 0)
                        <br>
                        Add New Student<br>
                        {!! Form::open(['url' => 'submitAssignSPV', 'method' => 'post', 'role' => 'form', 'class' => 'ui form']) !!}

                        <div class="field">
                            @foreach ($listStudent as $stu)
                                @if(empty($stu->supervisorid))
                                    <div class="ui checkbox" style="display: block;">
                                        <input type="checkbox" name="checkbox_{{ $stu->studentid }}" value="{{ $stu->studentid }}">
                                        <label>{{ $stu->studentid }} - {{ $stu->name }}</label>
                                    </div>
                                @endif
                            @endforeach
                        </div>

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="spv_id" value="{{ $spv->id }}">
                        <button class="ui primary button" type="submit">
                          Save Data
                        </button>
                        {!! Form::close() !!}
                    @endif
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>

    </div>
  </div>
  <br><br><br>
</div>

<script>

</script>
@stop
