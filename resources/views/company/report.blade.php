@extends ('layout.masterC')

@section ('title', 'BINUS')

@section('content')

@include('partial/headerCompany')

<div class="ui vertical stripe">
  <div class="ui middle aligned stackable grid container">
    <div class="row">
    <a class="ui blue large button" href="{{ url('/company/exportRecruitmentData') }}">Export to Excel</a>
    </div>
    <label>Filters:</label>
    <div class="row">
      <div id="filter-faculty" class="ui floating labeled icon dropdown button">
        <i class="filter icon"></i>
        <span class="text"></span>
        <div class="menu">
          <div class="header">
            <i class="tags icon"></i>
            Filter by Faculty
          </div>
          <div class="item" data-value="">All Faculty</div>
          @foreach($faculties as $faculty)
          <div class="item" data-value="{{ $faculty->faculty }}">{{ $faculty->faculty }}</div>
          @endforeach
        </div>
      </div>
      <div id="filter-job-position" class="ui floating labeled icon dropdown button">
        <i class="filter icon"></i>
        <span class="text"></span>
        <div class="menu">
          <div class="header">
            <i class="tags icon"></i>
            Filter by Job Position
          </div>
          <div class="item" data-value="">All Positions</div>
          @foreach($jobPositions as $jobPosition)
          <div class="item" data-value="{{ $jobPosition->position }}">{{ $jobPosition->position }}</div>
          @endforeach
        </div>
      </div>
    </div>

    <div class="row">
      @if (session('fyi'))
      @if (session('fyi') == 'Berhasil')
      <div class="ui positive message">
        <i class="close icon"></i>
        <div class="header">
          You have successfully accept.
        </div>
      </div>
      @elseif (session('fyi') == 'Interview Berhasil')
      <div class="ui positive message">
        <i class="close icon"></i>
        <div class="header">
          You have successfully set interview.
        </div>
      </div>
      @elseif (session('fyi') == 'BerhasilProcess')
      <div class="ui warning message">
        <i class="close icon"></i>
        <div class="header">
          You have successfully process.
        </div>
      </div>
      @elseif (session('fyi') == 'BerhasilReject')
      <div class="ui negative message">
        <i class="close icon"></i>
        <div class="header">
          You have successfully reject.
        </div>
      </div>
      @endif
      @elseif (!empty(session('err')))
      <div class="ui negative message">
        <i class="close icon"></i>
        <div class="header">
          {{ session('err') }}
        </div>
      </div>
      @endif
    </div>
    @if ($min_bipp_ipk = \App\Company::getBippIpk())
    <div class="ui small green message">
      <p>BIPP Member</p>
    </div>
    @endif
    <table class="ui celled table" id="tableProcessed" style="width: 100%;">
      <thead>
        <tr>
          <th class="three wide">Name</th>
          <th class="one wide">Gender</th>
          <th class="two wide">Position</th>
          <th class="one wide">Phone</th>
          <th class="one wide">E-mail</th>
          <th class="one wide">Last IPK</th>
          <th class="one wide">Faculty</th>
          <th class="one wide">CV</th>
          <th class="two wide">Will be removed on</th>
          <th class="four wide">Status</th>
        </tr>
      </thead>

      <tbody>
        @foreach ($datas as $data)
        @if ($data->reStatus == 'rejected')
        <tr class="negative">
          @elseif ($data->stateAccepted == 'approved' && $data->stateStudent !== $user->userid)
        <tr class="warning tr">
          @elseif ($data->reStatus == 'approved')
        <tr class="positive tr">
          @else
        <tr>
          @endif
          <td>
            <a href="{{ url('/create?studentid=' . $data->nim) }}" data-content="Send Message to this user"
              class="send_message_btn"><i class="teal mail icon"></i></a>&nbsp;|&nbsp;
            <a href="{{ url('profile/' . $data->nim) }}">{{ $data->student }}</a>
            @if(!empty($data->duration))
            <br>Internship Duration: {{ $data->duration }} month(s)
            @endif
          </td>
          <td>{{ ucfirst($data->gender) }}</td>
          <td>{{ $data->position }}</td>

          @if ($data->phone == NULL)
          <td>Not Set</td>
          @else
          <td>{{ $data->phone }}</td>
          @endif

          @if ($data->email == NULL)
          <td>Not Set</td>
          @else
          <td>{{ $data->email }}</td>
          @endif
          <td>
            {{ $data->last_ipk['ipk'] }}
          </td>
          <td>
            {{ $data->faculty }}
          </td>
          <td>
            @if ($data->cv == 1)
            <a href="{{ url('/') }}/previewcvs/{{ $data->nim }}" target="_blank">
              <div class="ui tiny button">Preview</div>
            </a>
            @else
            CV not available!
            @endif
          </td>
          <td>
            @if($data->reStatus == 'process')
            {{ $data->expired_at }}
            @endif
          </td>
          @if ($data->stateAccepted == 'approved' && $data->stateStudent !== $user->userid)
          <td class="td">
            <div class="fields">
              <div class="field"><i class="tiny icon close"></i>Has been approved in another company</div>
            </div>
          </td>
          @elseif ($data->stateAccepted == 'approved' && $data->stateStudent === $user->userid && $data->reStatus !=
          'approved')
          <td class="td">
            <div class="fields">
              <div class="field"><i class="tiny icon close"></i>Has been approved in your company</div>
            </div>
          </td>
          @elseif ($data->reStatus == 'rejected')
          <td>
            <div class="fields">
              <div class="field">
                <i class="icon close"></i>Rejected

                @if($data->is_rejected_by_department)
                <br>Rejected
                @elseif(!empty($data->rejected_by) && $data->rejected_by == 'system')
                <br>Rejected by system automatically (because the student did not reply to the interview invitation
                within one week)
                @endif

                <div class="unreject_action" token="{{ $data->rToken }}" style="display: inline-block;">
                  @if($data->unreject_request == null || $data->unreject_request == 0)
                  <span class="ui tiny button pink unreject" nim="{{ $data->nim }}" name="{{ $data->student }}"
                    token="{{ $data->rToken }}"><i class="icon undo"></i>Unreject</span>
                  @elseif ($data->unreject_request > 0)
                  <span style="color: pink;"><i class="tiny icon undo"></i>Waiting unreject approval from
                    department</span>
                  @endif
                </div>
              </div>
            </div>
          </td>
          @elseif ($data->reStatus == 'approved')
          <td class="td">
            <div class="fields">
              <div class="field">
                <i class="tiny icon check"></i>Accepted and Approved<br>
              </div>
            </div>
          </td>
          @elseif ($data->reStatus == 'accepted')
          <td class="td">
            <div class="fields">
              <div class="field">
                <i class="tiny icon check"></i>Accepted and Waiting Approval<br>
              </div>
            </div>
          </td>
          @elseif ($data->reStatus == 'process')
          <td>
            <div class="fields">
              <div class="field">
                <i class="tiny icon spinner"></i>On Process<br>
              </div>
            </div>
          </td>
          @else
          <td>
            <div class="ui tiny orange button process"
              data-value="{{$data->student.'process'.$data->rToken.$data->position}}"><i class="icon check"></i>Process
            </div>
            <div class="ui tiny button negative reject"
              data-value="{{$data->student.'reject'.$data->rToken.$data->position}}"><i class="icon close"></i>Reject
            </div>
          </td>
          @endif

        </tr>
        @endforeach
      </tbody>
    </table>


  </div>
</div>

<script type="text/javascript">
  $(document).ready(() => {
    $('#tableProcessed').dataTable().fnDestroy();
    let tab = $('#tableProcessed').DataTable({
      "aaSorting": []
    });

    $('#filter-faculty').dropdown({
      onChange: function () {
        tab.draw();
      }
    });
    $('#filter-job-position').dropdown({
      onChange: function () {
        tab.draw();
      }
    });

    $.fn.dataTable.ext.search.push(
      function (settings, data, dataIndex) {
        var filterFaculty = $('#filter-faculty').dropdown('get value');
        var filterJob = $('#filter-job-position').dropdown('get value');
        var position = data[2] || ''; // use data for the position column
        var faculty = data[6] || '';

        if ((filterFaculty == '' && filterJob == '') ||
          (filterFaculty == '' && filterJob == position) ||
          (filterFaculty == faculty && filterJob == '') ||
          (filterFaculty == faculty && filterJob == position)) {
          return true;
        }
        return false;
      }
    );
  });

  @if(\App\ Company::getBippIpk() != null)
  sortableTableUnprocessed.column('5').order('desc').draw();
  @endif
</script>
@stop