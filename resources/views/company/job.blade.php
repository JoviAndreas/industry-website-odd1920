@extends ('layout.masterC')

@section ('title', 'BINUS')

@section('content')

    @include('partial/headerCompany')

    <style>
        .accordion {
            -webkit-user-select: none; /* Safari 3.1+ */
            -moz-user-select: none; /* Firefox 2+ */
            -ms-user-select: none; /* IE 10+ */
            user-select: none; /* Standard syntax */
        }
    </style>

    <div class="ui vertical stripe segment">
        <div class="ui middle aligned stackable grid container">

            @if (session('fyi'))
                <div class="row">
                    @if (session('fyi') == 'Berhasil')
                        <div class="ui positive message">
                            <i class="close icon"></i>
                            <div class="header">
                                You have successfully added job.
                            </div>
                        </div>
                    @elseif (session('fyi') == 'Berhasilupdate')
                        <div class="ui positive message">
                            <i class="close icon"></i>
                            <div class="header">
                                You have successfully updated job.
                            </div>
                        </div>
                    @elseif (session('fyi') == 'Berhasildelete')
                        <div class="ui positive message">
                            <i class="close icon"></i>
                            <div class="header">
                                You have successfully deleted job.
                            </div>
                        </div>
                    @elseif (session('fyi') == 'Berhasilclose')
                        <div class="ui positive message">
                            <i class="close icon"></i>
                            <div class="header">
                                You have successfully closed the job.
                            </div>
                        </div>
                    @elseif (session('fyi') == 'Berhasilopen')
                        <div class="ui positive message">
                            <i class="close icon"></i>
                            <div class="header">
                                You have successfully opened the job.
                            </div>
                        </div>
                    @else
                        <div class="ui negative message">
                            <i class="close icon"></i>
                            <div class="header">
                                {{ session('fyi') }}
                            </div>
                        </div>
                    @endif
                </div>
            @endif

            <div class="ui large button insertJob blue">Insert</div>

            <a class="ui large button red" href="{{ url('/') }}/close_jobs"
               onclick="return confirm('Are you sure you want to CLOSE all jobs ?')">Close All Jobs</a>
            {{-- <a class="ui large button green" href="{{ url('/') }}/open_jobs"
               onclick="return confirm('Are you sure you want to OPEN all jobs ?')">Open All Jobs</a> --}}

            <div class="row">
                <ul>
                    <li>If the job status is <span style="color: orange;">WAITING</span>, the job is on the proccess of
                        approval
                    </li>
                    <li>If the job status is <span style="color: red;">REJECTED</span>, the job is rejected and you have
                        to resend job details
                    </li>
                    <li>If the job is approved, the job status will be <span style="color: red;">CLOSED</span> or <span
                                style="color: green;">OPEN</span></li>
                    <li>If the job status is <span style="color: red;">CLOSED</span>, students cannot apply to the jobs,
                        but you can still process current job applications
                    </li>
                    <li>If the job status is <span style="color: green;">OPEN</span>, students could apply to the jobs
                    </li>
                    <li>If you REQUEST UPDATE, the job will be updated and the status will change into <span
                                style="color: orange;">WAITING</span> for approval
                    </li>
                </ul>
            </div>

            <div class="row" style="max-height: 60vh; max-width: 100%; overflow-x: scroll;">
                <table class="ui striped table">
                    <thead>
                    <tr>
                        <th>Position</th>
                        <th>Full Quota</th>
                        <th>Current Quota</th>
                        <th>Location</th>
                        <th>PIC</th>
                        <th>PIC Contact</th>
                        <th>Allowance</th>
                        <th>Job Desc</th>
                        <th>Programs</th>
                        <th>Deadline Apply</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Duration</th>
                        <th>Min GPA</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($datas as $data)
                        <tr>
                            <td>
                                {{ $data->position }}<br><br>
                                [Status: <span
                                        style="color: {{ ($data->state == 'open') ? 'green' : (($data->state == 'waiting') ? 'orange' : 'red') }};">{{ strtoupper($data->state) }}</span>]
                            </td>
                            <td>{{ $data->full_quota }}</td>
                            <td><span class="ui green label">{{ $data->quota }}</span></td>
                            <td>{{ $data->location }}</td>
                            <td>{{ $data->supervisor_name }}</td>
                            <td>{{ $data->supervisor_contact }}</td>
                            <td>{{ 'Rp. ' . number_format($data->allowance,0,",",".") }}</td>
                            <td>
                                <div class="ui bulleted list">
                                    <?php
                                    $pieces = explode("\n", $data->description);
                                    ?>
                                    @foreach ($pieces as $piece)
                                        <div class="item">{{ $piece }}</div>
                                    @endforeach
                                </div>
                            </td>
                            <td>
                                <div class="ui bulleted list">
                                    @foreach ($data->degrees as $d)
                                        <div class="item">{{ $d->name }}</div>
                                    @endforeach
                                </div>
                            </td>
                            <td>{{ date("d M Y", strtotime($data->deadline)) }}</td>
                            <td>{{ ($data->startdate == null) ? '-' : date("d M Y", strtotime($data->startdate)) }}</td>
                            <td>{{ ($data->enddate == null) ? '-' : date("d M Y", strtotime($data->enddate)) }}</td>
                            <td>{{ $data->duration }} months</td>
                            <td>{{ ($data->is_using_bipp_min_ipk) ? \App\Bipp::first()->minimumIPK() : 'No Minimum GPA' }}</td>
                            <td>
                                @if ($data->state == 'waiting')
                                    <span>Waiting for Approval</span>
                                @endif
                                @if ($data->state == 'open')
                                    <div class="ui small button orange updateJob{{ $data->token }}"
                                         data-button="Request Update Job">Request Update
                                    </div>
                                @endif
                                @if ($data->state == 'rejected')
                                    <div class="ui small button orange updateJob{{ $data->token }}"
                                         data-button="Resend Job"><i class="icon undo"></i> Update and Resend Job
                                    </div>
                                @endif
                                {{-- <div class="ui small button delete job"  data-value="{{'job'.$data->token.$data->position}}">Delete</div> --}}

                                {{-- 
                                
                                @if($data->state == 'open')
                                    <a class="ui small button red" href="{{ url('/') }}/close_job/{{ $data->token }}"
                                       onclick="return confirm('Are you sure you want to CLOSE this job ?')">Close
                                        Job</a>
                                @elseif($data->state == 'closed')
                                    <a class="ui small button green" href="{{ url('/') }}/open_job/{{ $data->token }}"
                                       onclick="return confirm('Are you sure you want to OPEN this job ?')">Open Job</a>
                                @endif --}}

                                @if(!empty($data->linktest))
                                    <div class="ui small button showLink{{ $data->token }}">Info</div>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>

    @foreach ($datas as $data)

        <div class="ui modal showLink {{ $data->token }}">
            <div class="header">Information</div>
            <div class="content">

                <p>Job : {{$data->position}}</p>
                <p>Here is the<a target="_blank" href="{{$data->linktest}}"> Link </a> for Online test</p>
            </div>
            <div class="actions">
                <div class="ui red deny button">
                    Close
                </div>
            </div>
        </div>


        <div class="ui modal updateJob {{ $data->token }}" style="overflow: auto;">
            <div class="header">Update Job Vacancy</div>
            <div class="content">
                {!! Form::open(['url' => 'updateJobDescs', 'method' => 'post', 'id' => 'formInput', 'role' => 'form', 'class' => 'ui large form']) !!}
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="fields">
                    <p>Set minimal IPK >= {{ \App\Bipp::first()->minimumIPK() }}: </p>
                    <div class="field">
                        <div class="ui radio checkbox">
                            <input type="radio" name="rduHasMinIPK" value="yes" id="rdHasMinIPKYes" {{ ($data->is_using_bipp_min_ipk) ? 'checked' : '' }}>
                            <label for="rdHasMinIPKYes">Yes</label>
                        </div>
                    </div>
                    <div class="field">
                        <div class="ui radio checkbox">
                            <input type="radio" name="rduHasMinIPK" value="no" id="rdHasMinIPKNo" {{ ($data->is_using_bipp_min_ipk) ? '' : 'checked' }}>
                            <label for="rdHasMinIPKNo">No</label>
                        </div>
                    </div>
                </div>

                <div class="field">
                    <div class="ui left icon input">
                        <input type="text" name="txtuSupervisor" required id="idSupervisor" placeholder="PIC"
                               value="@if(old('txtuSupervisor')!=null){{old('txtuSupervisor')}}@else{{$data->supervisor_name}}@endif">
                        <i class="find icon"></i>
                    </div>
                </div>

                <div class="field">
                    <div class="ui left icon input">
                        <input type="text" name="txtuSupervisorContact" required id="idSupervisorContact"
                               placeholder="PIC Contact"
                               value="@if(old('txtuSupervisorContact')!=null){{old('txtuSupervisorContact')}}@else{{$data->supervisor_contact}}@endif">
                        <i class="find icon"></i>
                    </div>
                </div>

                <div class="field">
                    <div class="ui left icon input">
                        <input type="number" name="txtuAllowance" required min="1"
                               placeholder="Allowance (Rupiah)"
                               value="@if(old('txtuAllowance')!=null){{old('txtuAllowance')}}@else{{ $data->allowance }}@endif">
                        <i class="money icon"></i>
                    </div>
                </div>

                <div class="two fields">
                    <div class="field">
                        <div class="ui left icon input">
                            <input type="number" name="txtuQuantity" required id="idQuantity" placeholder="Quota"
                                   min="1"
                                   value="@if(old('txtuQuantity')!=null){{old('txtuQuantity')}}@else{{ $data->quota }}@endif">
                            <i class="calculator icon"></i>
                        </div>
                    </div>

                    <div class="field">
                        <div class="ui left icon input">
                            <input type="text" name="txtuLocation" required id="idLocation" placeholder="Location"
                                   value="@if(old('txtuLocation')!=null){{old('txtuLocation')}}@else{{$data->location}}@endif">
                            <i class="calculator icon"></i>
                        </div>
                    </div>
                </div>

                <div class="field">
                    <div>Programs: &nbsp;
                        {{-- <span class="ui label">To select program, please select the duration of your internship job</span> --}}
                    </div>
                    @foreach($faculties as $faculty)
                        <div class="ui accordion">
                            <div class="title">
                                <i class="dropdown icon"></i>
                                {{ $faculty->name }}
                            </div>
                            <div class="content">
                                <div class="field" style="padding-left: 2rem;">
                                    @foreach($faculty->degrees as $degree)
                                        <div class="ui left icon input">
                                            <input type="hidden" name="degreeu_{{ $data->id }}_{{ $degree->id }}"
                                                   value="off">
                                            <div class="ui checkbox checkboxDegreeU"
                                                 data_id="{{ $data->id }}">
                                                <input type="checkbox" class="inputCheckboxDegreeU"
                                                       data_id="{{ $data->id }}"
                                                       degree_id="{{ $degree->id }}"
                                                       name="degreeu_{{ $data->id }}_{{ $degree->id }}" value="on"
                                                       @if (!empty(old('degreeu_'.$data->id.'_' .$degree->id)) && old('degreeu_'.$data->id.'_' .$degree->id) == 'on') checked
                                                       @elseif (old('degreeu_'.$data->id.'_' .$degree->id) !== 'off' && $data->degrees->where('id', $degree->id)->count() == 1) checked @endif>
                                                <label>{{ $degree->name }}</label>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

                <div class="field">
                    <div class="ui left icon input">
                        <select class="ui dropdown" name="cmbPositionU" class="cmbPositionU" data_id="{{ $data->id }}" old_position="{{ $data->position }}">
                            <option value="">Job Position</option>
                            {{--                            @foreach($jobpositions as $jobposition)--}}
                            {{--                                <option value="{{$jobposition->name}}" @if($jobposition->id == old('cmbPosition'))--}}
                            {{--                                selected="selected"--}}
                            {{--                                        @endif>{{$jobposition->name}}</option>--}}
                            {{--                            @endforeach--}}
                        </select>
                    </div>
                </div>

                <div class="two fields">
                    <div class="field">
                        <div class="ui left icon input">
                            <input type="text" class="datepickerDea" name="txtuDeadline" data_id="{{ $data->id }}"
                                   placeholder="Deadline Apply" size="30"
                                   value="@if(old('txtuDeadline')!=null){{old('txtuDeadline')}}@else{{date('m/d/Y',strtotime($data->deadline))}}@endif">
                            <i class="calendar icon"></i>
                        </div>
                    </div>

                    <div class="field">
                        <div class="ui left icon input">
                            <select required class="ui dropdown" name="txtuDuration" id="datepickerDuru"
                                    data_id="{{ $data->id }}" aria-placeholder="Duration">
                                @if(old('txtuDuration')!=null)
                                    <option value="">Duration</option>
                                    <option value="4" @if(old('txtuDuration')==4)selected="selected"@endif>4 Months
                                    </option>
                                    <option value="5" @if(old('txtuDuration')==5)selected="selected"@endif>5 Months
                                    </option>
                                    <option value="6" id="durationOptionU6" data_id="{{ $data->id }}"
                                            {{-- availProgram="{{ $degrees_six_month }}"  --}}
                                            @if(old('txtuDuration')==6)selected="selected"@endif>
                                        6 Months
                                    </option>
                                    <option value="8" @if(old('txtuDuration')==8)selected="selected"@endif>8 Months
                                    </option>
                                    <option value="9" @if(old('txtuDuration')==9)selected="selected"@endif>9 Months
                                    </option>
                                    <option value="10" @if(old('txtuDuration')==10)selected="selected"@endif>10 Months
                                    </option>
                                    <option value="11" @if(old('txtuDuration')==11)selected="selected"@endif>11 Months
                                    </option>
                                    <option value="12" id="durationOptionU12" data_id="{{ $data->id }}"
                                            {{-- availProgram="{{ $degrees_twelve_month }}" --}}
                                            @if(old('txtuDuration')==12)selected="selected"@endif>
                                        12 Months
                                    </option>
                                @else
                                    <option value="">Duration</option>
                                    <option value="4" @if($data->duration==4)selected="selected"@endif>4 Months</option>
                                    <option value="5" @if($data->duration==5)selected="selected"@endif>5 Months</option>
                                    <option value="6" id="durationOptionU6" data_id="{{ $data->id }}"
                                            {{-- availProgram="{{ $degrees_six_month }}"  --}}
                                            @if($data->duration==6)selected="selected"@endif>
                                        6 Months
                                    </option>
                                    <option value="8" @if($data->duration==8)selected="selected"@endif>8 Months
                                    </option>
                                    <option value="9" @if($data->duration==9)selected="selected"@endif>9 Months
                                    </option>
                                    <option value="10" @if($data->duration==10)selected="selected"@endif>10 Months
                                    </option>
                                    <option value="11" @if($data->duration==11)selected="selected"@endif>11 Months
                                    </option>
                                    <option value="12" id="durationOptionU12" data_id="{{ $data->id }}"
                                            {{-- availProgram="{{ $degrees_twelve_month }}" --}}
                                            @if($data->duration==12)selected="selected"@endif>
                                        12 Months
                                    </option>
                                @endif
                            </select>
                        </div>
                    </div>
                </div>

                <div class="field">
                    <div class="ui left icon input">
                        <input type="text" class="datepickerDeaStartDate" name="txtuStartDate" data_id="{{ $data->id }} " min='2020-08-01'
                               placeholder="Start Date" size="30"
                               value="@if(old('txtuStartDate')!=null){{old('txtuStartDate')}}@else{{date('m/d/Y',strtotime($data->startdate))}}@endif">
                        <i class="calendar icon"></i>
                    </div>
                </div>

                <div class="field">
                    <div class="ui left icon input">
                        <input type="text" id="txtuEndDate" class="txtuEndDate" name="txtuEndDate" min="2020-08-01" max="2021-02-12"
                               data_id="{{ $data->id }}" placeholder="End Date" size="30" readonly
                               value="@if(old('txtuEndDate')!=null){{old('txtuEndDate')}}@else{{date('m/d/Y',strtotime($data->enddate))}}@endif">
                        <i class="calendar icon"></i>
                    </div>
                </div>

                <div class="field">
                    <div class="ui left icon input">
                        <textarea rows="2" name="txtuJobDescription" required id="idDescription"
                                  placeholder="Job Description ( Press enter to make points in the description. )">@if(old('txtuJobDescription')!=null){{old('txtuJobDescription')}}@else{{$data->description}}@endif</textarea>
                    </div>
                </div>

                <div class="field">
                    <div class="ui checkbox" id="checkboxLinkTestU{{ $data->token }}">
                        <input type="checkbox" id="checkboxLinkTestU" token="{{ $data->token }}"
                               name="checkboxLinkTestU"
                               @if(!empty(old('checkboxLinkTestU{{ $data->token }}')) || !empty($data->linktest)) checked @endif>
                        <label>Online Test</label>
                    </div>
                </div>

                <div class="field" id="fieldLinkTestU{{ $data->token }}"
                     style="@if(empty(old('checkboxLinkTestU{{ $data->token }}')) && empty($data->linktest)) display: none; @endif">
                    <div class="ui left icon input">
                        <input type="text" id="idLinkTest" name="txtuLinkTest" placeholder="Link Test (www.example.com)"
                               value="@if(old('txtuLinkTest')!=null){{old('txtuLinkTest')}}@else{{$data->linktest}}@endif">
                        <i class="linkify icon"></i>
                    </div>
                </div>

                <div class="field">
                    <div class="ui left icon input">
                        <input type="hidden" id="idToken" name="txtuToken" placeholder="Token"
                               value="{{ $data->token }}">
                    </div>
                </div>

                <div class="ui fluid large blue submit button">Resend Job</div>

            </div>
            <div class="ui error message"></div>
            {!! Form::close() !!}
        </div>
        </div>

        <script>
            $('.updateJob{{ $data->token }}').click(function (e) {
                $('.ui.modal.updateJob.{{ $data->token }}').modal('show');
            });

            $('.showLink{{ $data->token }}').click(function (e) {
                $('.ui.modal.showLink.{{ $data->token }}').modal('show');
            });
        </script>
    @endforeach

    <div class="ui modal insertJob" style="overflow: auto;">
        <div class="header">Insert Job Vacancy</div>
        <div class="content">
            {!! Form::open(['url' => 'insertJobDescs', 'method' => 'post', 'id' => 'formInput', 'role' => 'form', 'class' => 'ui large form']) !!}

            <div class="fields">
                <p>Set minimal IPK >= {{ \App\Bipp::first()->minimumIPK() }}: </p>
                <div class="field">
                    <div class="ui radio checkbox">
                        <input type="radio" name="rdHasMinIPK" value="yes" id="rdHasMinIPKYes">
                        <label for="rdHasMinIPKYes">Yes</label>
                    </div>
                </div>
                <div class="field">
                    <div class="ui radio checkbox">
                        <input type="radio" name="rdHasMinIPK" value="no" id="rdHasMinIPKNo" checked>
                        <label for="rdHasMinIPKNo">No</label>
                    </div>
                </div>
            </div>

            <div class="field">
                <div class="ui left icon input">
                    <input type="text" name="txtiSupervisor" required id="idSupervisor" placeholder="PIC"
                           value="@if(old('txtiSupervisor')!=null){{old('txtiSupervisor')}}@endif">
                    <i class="find icon"></i>
                </div>
            </div>

            <div class="field">
                <div class="ui left icon input">
                    <input type="text" name="txtiSupervisorContact" required id="idSupervisorContact"
                           placeholder="PIC Contact"
                           value="@if(old('txtiSupervisorContact')!=null){{old('txtiSupervisorContact')}}@endif">
                    <i class="find icon"></i>
                </div>
            </div>

            <div class="field">
                <div class="ui left icon input">
                    <input type="number" name="txtiAllowance" required min="1"
                           placeholder="Allowance (Rupiah)"
                           value="@if(old('txtiAllowance')!=null){{old('txtiAllowance')}}@endif">
                    <i class="money icon"></i>
                </div>
            </div>

            <div class="two fields">
                <div class="field">
                    <div class="ui left icon input">
                        <input type="number" name="txtiQuantity" required id="idQuantity" placeholder="Quota" min="1"
                               value="@if(old('txtiQuantity')!=null){{old('txtiQuantity')}}@endif">
                        <i class="calculator icon"></i>
                    </div>
                </div>

                <div class="field">
                    <div class="ui left icon input">
                        <input type="text" name="txtiLocation" required id="idLocation" placeholder="Location"
                               value="@if(old('txtiLocation')!=null){{old('txtiLocation')}}@endif">
                        <i class="calculator icon"></i>
                    </div>
                </div>
            </div>

            <div class="two fields">
                <div class="field">
                    <div class="ui left icon input">
                        <input type="text" class="datepickerDea" name="txtiDeadline" id="txtiDeadline"
                               placeholder="Deadline Apply" size="30"
                               value="@if(old('txtiDeadline')!=null){{old('txtiDeadline')}}@endif">
                        <i class="calendar icon"></i>
                    </div>
                </div>

                <div class="field">
                    <div class="ui left icon input">
                        <select required class="ui dropdown" name="txtiDuration" id="datepickerDuri"
                                aria-placeholder="Duration">
                            <option value="">Duration</option>
                            <option value="4" @if(old('txtiDuration')==4)selected="selected"@endif>4 Months</option>
                            <option value="5" @if(old('txtiDuration')==5)selected="selected"@endif>5 Months</option>
                            <option value="6" id="durationOption6"
                                    {{-- availProgram="{{ $degrees_six_month }}"  --}}
                                    @if(old('txtiDuration')==6)selected="selected"@endif>
                                6 Months
                            </option>
                            <option value="8" @if(old('txtiDuration')==8)selected="selected"@endif>8 Months</option>
                            <option value="9" @if(old('txtiDuration')==9)selected="selected"@endif>9 Months</option>
                            <option value="10" @if(old('txtiDuration')==10)selected="selected"@endif>10 Months</option>
                            <option value="11" @if(old('txtiDuration')==11)selected="selected"@endif>11 Months</option>
                            <option value="12" id="durationOption12"
                                    {{-- availProgram="{{ $degrees_twelve_month }}"  --}}
                                    @if(old('txtiDuration')==12)selected="selected"@endif>
                                12 Months
                            </option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="field">
                <div class="ui left icon input">
                    <input type="text" class="datepickerDeaStartDate" name="txtiStartDate" id="txtiStartDate" min='2020-08-01'
                           placeholder="Start Date" size="30"
                           value="@if(old('txtiStartDate')!=null){{old('txtiStartDate')}}@endif">
                    <i class="calendar icon"></i>
                </div>
            </div>

            <div class="field">
                <div class="ui left icon input">
                    <input type="text" id="txtiEndDate" name="txtiEndDate" placeholder="End Date" size="30" readonly max="2021-02-12"
                           value="@if(old('txtiEndDate')!=null){{old('txtiEndDate')}}@endif">
                    <i class="calendar icon"></i>
                </div>
            </div>

            <div class="field">
{{--                <div class="ui left icon input">--}}
{{--                    <select class="ui dropdown" name="cmbPosition" id="cmbPosition">--}}
{{--                        <option value="">Job Position</option>--}}
{{--                        @foreach($jobpositions as $jobposition)--}}
{{--                            <option value="{{$jobposition->name}}" @if($jobposition->id == old('cmbPosition'))--}}
{{--                            selected="selected"--}}
{{--                                    @endif>{{$jobposition->name}}</option>--}}
{{--                        @endforeach--}}
{{--                    </select>--}}
{{--                </div>--}}
            <div class="ui fluid labeled icon pointing dropdown button" id="cmbPosition">
                <input type="hidden" name="cmbPosition">
                <i class="icon briefcase"></i>
                <span class="text">Job Positions</span>
                <div class="menu">
                    <div class="ui search icon input" style="width: auto !important;">
                        <i class="search icon"></i>
                        <input type="text" name="search" placeholder="Search job positions...">
                    </div>
                    <div class="divider"></div>
                    <div class="header">
                        <i class="tags icon"></i>
                        Job Positions
                    </div>
                    <div class="scrolling menu">
                    @foreach($jobpositions as $jobposition)
                        <div class="item" data-value="{{ $jobposition->name }}" data-id="{{ $jobposition->id }}">
                            <div class="ui green empty circular label"></div>
                            {{ $jobposition->name }}
                        </div>
                    @endforeach
                    </div>
                </div>
            </div>

            <br>

            <div class="field">
                <div>Programs: &nbsp;
                    {{-- <span class="ui label">To select program, please select the duration of your internship job first</span> --}}
                </div>
                @foreach($faculties as $faculty)
                    <div class="ui accordion">
                        <div class="title">
                            <i class="dropdown icon"></i>
                            {{ $faculty->name }}
                        </div>
                        <div class="content">
                            <div class="field" style="padding-left: 2rem;">
                                @foreach($faculty->degrees as $degree)
                                    <div class="ui left icon input">
                                        <div class="ui checkbox checkboxDegree" degree_id="{{ $degree->id }}" id="degreei_{{ $degree->id }}">
                                            <input type="checkbox" class="inputCheckboxDegree"
                                                   name="degreei_{{ $degree->id }}" value="on"
                                                   degree_id="{{ $degree->id }}">
                                            <label>{{ $degree->name }}</label>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

            <div class="field">
                <div class="ui left icon input">
                    <textarea rows="2" name="txtiJobDescription" required id="idDescription"
                              placeholder="Job Description ( Press enter to make points in the description. )">{{old('txtiJobDescription')}}</textarea>
                </div>
            </div>

            <div class="field">
                <div class="ui checkbox" id="checkboxLinkTest">
                    <input type="checkbox" name="checkboxLinkTest" @if(!empty(old('checkboxLinkTest'))) checked; @endif>
                    <label>Online Test</label>
                </div>
            </div>

            <div class="field" id="fieldLinkTest" style="@if(empty(old('checkboxLinkTest'))) display: none; @endif">
                <div class="ui left icon input">
                    <input type="text" id="idLinkTest" name="txtiLinkTest" required
                           placeholder="Link Test (www.example.com)" size="30"
                           value="@if(old('txtiLinkTest')!=null){{old('txtiLinkTest')}}@endif">
                    <i class="linkify icon"></i>
                </div>
            </div>
            <div class="ui fluid large blue submit button">Insert</div>
            <div class="ui error message"></div>
        </div>
        {!! Form::close() !!}
    </div>
    </div>

    <div class="ui small modal confirmation">
        <div class="header">Confirmation</div>
        <div class="content">
            <p id="contentApllicant">Are you sure you want to apply to xx as a xxx?</p>
        </div>
        <div class="actions">
            <div class="ui approve green button">Yes</div>
            <div class="ui cancel red button">No</div>
        </div>
    </div>


    <script type="text/javascript">
        $(document).ready(function() {
            /**
             * HARDCODE
             */
            $('.datepickerDeaStartDate').datepicker({minDate: new Date(2020, 1, 17), maxDate: new Date(2021, 1, 28)}); //1 itu bulan februari, idx mulai dari 0
            /**
             * ENDOF HARDCODE
             */

            ///////////     JOB POSITION AJAX   /////////
            function loadJobPositionU(id) {
                let degrees = [];
                $(`.inputCheckboxDegreeU[data_id=${id}]`).each(function (index, checkbox) {
                    if ($(checkbox).is(':checked')) {
                        degrees.push($(checkbox).attr('degree_id'));
                    }
                });
                $.ajax({
                    type: 'GET',
                    url: "{{ url('/ajax/job_position') }}",
                    data: {'degrees': degrees},
                    success: function (data) {
                        $(`[name="cmbPositionU"][data_id=${id}]`).html('<option value="">Job Position</option>');
                        // let oldPositionName = $(`[name="cmbPositionU"][data_id=${id}]`).attr('old_position');
                        data.forEach(function (position) {
                            let positionId = position.id;
                            let positionName = position.name;
                            let str = '<option value="' + positionName + '">' + positionName + '</option>';
                            $(`[name="cmbPositionU"][data_id=${id}]`).append(str); // CHANGE THE ID/CLASS
                        });
                        // $(`[name="cmbPositionU"][data_id=${id}]`).dropdown('set selected', oldPositionName);
                    },
                    error: function () {
                        console.log("ERROR: AJAX");
                    }
                });
            }

            function loadJobDegrees(jobPositionId = undefined) {
                if (jobPositionId !== undefined) {
                    let jobDegreesMapping = JSON.parse(`{!! json_encode($jobPositionDegreesMapping) !!}`);
                    let jobDegrees = jobDegreesMapping[jobPositionId];
                    $('.checkboxDegree').each(function (index, checkbox) {
                        $(checkbox).checkbox('set unchecked');
                        $(checkbox).checkbox('set disabled');
                    });
                    jobDegrees.forEach(function(jobDegree) {
                        // console.log('#degreei_' + jobDegree.degree_id);
                        $('#degreei_' + jobDegree.degree_id).checkbox('set enabled');
                    });
                }
            }

            function initJobPosition() {
                let oldJobPosition = `{{ old('cmbPosition') }}`;

                $('#cmbPosition').dropdown('setting', 'onChange', function(value, text, jobPosition) {
                    loadJobDegrees(jobPosition.data('id'));
                });

                if (oldJobPosition === undefined || oldJobPosition === ``) {
                    $('.checkboxDegree').each(function (index, checkbox) {
                        $(checkbox).checkbox('set unchecked');
                        $(checkbox).checkbox('set disabled');
                    });
                } else {
                    $('#cmbPosition').dropdown('set selected', oldJobPosition);

                    @php
                        $oldJobPositionDegrees = array_filter((array) session()->getOldInput(), function($value, $key) {
                            return strpos($key, 'degreei_') === 0 && $value == 'on';
                        }, ARRAY_FILTER_USE_BOTH);
                    @endphp
                    let oldJobPositionDegrees = JSON.parse(`{!! json_encode($oldJobPositionDegrees) !!}`);
                    Object.keys(oldJobPositionDegrees).forEach(function(key) {
                        if ($('#' + key).checkbox('can change')) {
                            $('#' + key).checkbox('set checked');
                        }
                    });
                    // console.log(oldJobPositionDegrees);
                    // $('.checkboxDegree').each(function (index, checkbox) {
                    //
                    //     $(checkbox).checkbox('set enabled');
                    //     $(checkbox).checkbox('set checked');
                    // });
                }

                $('.inputCheckboxDegreeU').each(function (index, checkbox) {
                    $(checkbox).change(function () {
                        let id = $(this).attr('data_id');
                        loadJobPositionU(id);
                    });
                });

                @foreach ($datas as $data)
                loadJobPositionU({{ $data->id }});
                @endforeach
            }

            initJobPosition();
            /////////// END OF JOB POSITION AJAX /////////

            $('.accordion').accordion({
                'animateChildren': false,
                'duration': 0
            });

            function calculateEndDatei(e) {
                let startdateinput = $('#txtiStartDate');
                let duration = $('#datepickerDuri').val();
                if (duration != '' && startdateinput.val() != '') {
                    let date = new Date(startdateinput.val());
                    let endDate = new Date(date.setMonth(date.getMonth() + parseInt(duration)));
                    let formattedDate = (date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear();
                    $('#txtiEndDate').val(formattedDate);
                }
            }

            $('#txtiStartDate').change(calculateEndDatei);
            $('#datepickerDuri').change(calculateEndDatei);

            function calculateEndDateu(e) {
                let id = $(this).attr('data_id');
                let startdateInput = $(`[name="txtuStartDate"][data_id=${id}]`);
                let duration = $(`[name="txtuDuration"][data_id=${id}]`).val();
                if (duration != '' && startdateInput.val() != '') {
                    let date = new Date(startdateInput.val());
                    let endDate = new Date(date.setMonth(date.getMonth() + parseInt(duration)));
                    let formattedDate = (date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear();
                    $(`.txtuEndDate[data_id=${id}]`).val(formattedDate);
                }
            }

            $('[name="txtuStartDate"]').change(calculateEndDateu);
            $('[name=txtuDuration]').change(calculateEndDateu);

            var dataToken = "";
            var dataName = "";

            $(document).on('change', '#checkboxLinkTest', function () {
                if ($('[name="checkboxLinkTest"]').is(':checked') == true) {
                    $('#fieldLinkTest').show();
                } else {
                    $('#fieldLinkTest').hide();
                }
            });
            $(document).on('change', '#checkboxLinkTestU', function () {
                var token = $(this).attr('token');
                if ($('input[token="' + token + '"]').is(':checked') == true) {
                    $('#fieldLinkTestU' + token).show();
                } else {
                    $('#fieldLinkTestU' + token).hide();
                }
            });
        });
    </script>
@stop
                                      