@extends ('layout.masterC')

@section ('title', 'BINUS')

@section('content')

    <script type="text/javascript">
        $(document).ready(function () {
            var data = $('#dataTesting').val();
            var parsedData = JSON.parse(data);
            $('#positionStatus').change(function () {
                $('#valueApp').html(parsedData[$(this).val()]);
            });

            $('.send_message_btn').popup();
        });
    </script>

    @include('partial/headerCompany')

    <div class="ui vertical stripe segment">
        <div class="ui middle aligned stackable grid container-fluid" style="margin: 1rem;">
            <div class="ui message">
                <ul>
                    <li>You can't unaccept directly to the student you have accepted</li>
                    <li>If you want to change the placement status for some reasons, please click the "<strong
                                style="color: purple;">unaccept</strong>" button and submit the reason
                    </li>
                    <li>You have <span
                                class="ui orange label">{{ ($dataUnprocessed != null ? $dataUnprocessed->count() : 0) }}</span>
                        unprocessed student application.
                    </li>
                    <li>You have <span class="ui blue label">{{ $interview_reschedule_count }}</span> interview
                        reschedule request.
                    </li>
                </ul>
            </div>

            <div class="row">
                @if (session('fyi'))
                    @if (session('fyi') == 'Berhasil')
                        <div class="ui positive message">
                            <i class="close icon"></i>
                            <div class="header">
                                You have successfully accept.
                            </div>
                        </div>
                    @elseif (session('fyi') == 'Interview Berhasil')
                        <div class="ui positive message">
                            <i class="close icon"></i>
                            <div class="header">
                                You have successfully set interview.
                            </div>
                        </div>
                    @elseif (session('fyi') == 'BerhasilProcess')
                        <div class="ui warning message">
                            <i class="close icon"></i>
                            <div class="header">
                                You have successfully process.
                            </div>
                        </div>
                    @elseif (session('fyi') == 'BerhasilReject')
                        <div class="ui negative message">
                            <i class="close icon"></i>
                            <div class="header">
                                You have successfully reject.
                            </div>
                        </div>
                    @endif
                @elseif (!empty(session('err')))
                    <div class="ui negative message">
                        <i class="close icon"></i>
                        <div class="header">
                            {{ session('err') }}
                        </div>
                    </div>
                @endif
            </div>

            <div>
                <a href="{{ url('/') }}/interviewD">
                    <div class="ui primary button interview" data-content="Interview Schedule"><i
                                class="download icon"></i>Download Interview Schedule
                    </div>
                </a>
            </div>

            <div class="ui top attached tabular menu">

                @if($state == 'open')
                    <a class="active item" data-tab="second">Unprocessed</a>
                @endif
                @if($state == 'open')
                    <a class="item" data-tab="first">Processed</a>
                @elseif($state == 'closed')
                    <a class="item" data-tab="first">Accepted</a>
                @endif

            </div>
            @if($state == 'open')
                <div class="ui bottom attached active tab segment" data-tab="second" style="padding: 10px;">
                    <div class="ui small warning message">
                        <p><strong>Student's name will be removed automatically 2 weeks after student's apply
                                date.</strong></p>
                    </div>
                    @php $min_bipp_ipk = \App\Company::getBippIpk(); @endphp
                    @if (\App\Company::isBipp())
                        <div class="ui small green message">
                            <p>BIPP Member</p>
                        </div>
                    @endif
                    <table class="ui celled table" id="tableUnprocessed" style="width: 100%;">
                        <thead>
                        <tr>
                            <th class="three wide">Name</th>
                            <th class="one wide">Gender</th>
                            <th class="two wide">Position</th>
                            <th class="one wide">Phone</th>
                            <th class="one wide">E-mail</th>
                            <th class="one wide">Last IPK</th>
                            <th class="one wide">CV</th>
                            <th class="one wide">Job Desc</th>
                            <th class="one wide">Duration</th>
                            <th class="two wide">Will be removed on</th>
                            <th class="one wide">Program</th>
                            <th class="one wide">Campus</th>
                            <th class="four wide">Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($dataUnprocessed as $data)
                            @if ($data->reStatus == 'rejected')
                                <tr class="negative">
                            @elseif ($data->stateAccepted == 'approved' && $data->stateStudent !== $user->userid)
                                <tr class="warning tr popup">
                            @elseif ($data->reStatus == 'accepted')
                                <tr class="positive tr popup">
                            @else
                                <tr>
                                    @endif
                                    <td>
                                        <a href="{{ url('/create?studentid=' . $data->nim) }}"
                                           data-content="Send Message to this user" class="send_message_btn"><i
                                                    class="teal mail icon"></i></a>&nbsp;|&nbsp;
                                        <a href="{{ url('profile/' . $data->nim) }}">{{ $data->student }}</a>
                                        @if(!empty($data->duration))
                                            <br>Internship Duration: {{ $data->duration }} month(s)
                                        @endif
                                    </td>
                                    <td>{{ ucfirst($data->gender) }}</td>
                                    <td>{{ $data->position }}</td>

                                    @if ($data->phone == NULL)
                                        <td>Not Set</td>
                                    @else
                                        <td>{{ $data->phone }}</td>
                                    @endif

                                    @if ($data->email == NULL)
                                        <td>Not Set</td>
                                    @else
                                        <td>{{ $data->email }}</td>
                                    @endif
                                    <td>
                                        {{ $data->last_ipk['ipk'] }}
                                    </td>
                                    <td>
                                        @if ($data->cv == 1)
                                            <a href="{{ url('/') }}/previewcvs/{{ $data->nim }}" target="_blank">
                                                <div class="ui tiny button">Preview</div>
                                            </a>
                                        @else
                                            CV not available!
                                        @endif
                                    </td>
                                    <td>
                                        <!-- button show desc -->
                                        <div class="ui small button showDescriptionUnprocessed{{ $data->token }}">Show</div>
                                    </td>
                                    <td>
                                        {{ $data->duration }} month(s)
                                    </td>
                                    <td>
                                        {{ $data->expired_at }}
                                    </td>
                                    <td>
                                        {{  $data->program }}
                                    </td>
                                    <td>
                                        {{  $data->campus }}
                                    </td>
                                    @if ($data->stateAccepted == 'approved' && $data->stateStudent !== $user->userid)
                                        <td class="td">
                                            <div class="fields">
                                                <div class="field"><i class="tiny icon close"></i>Has been approved in
                                                    another company
                                                </div>
                                            </div>
                                        </td>
                                    @elseif ($data->stateAccepted == 'approved' && $data->stateStudent === $user->userid)
                                        <td class="td">
                                            <div class="fields">
                                                <div class="field"><i class="tiny icon close"></i>Has been approved in
                                                    your company
                                                </div>
                                            </div>
                                        </td>
                                    @else
                                        <td>
                                            <div class="ui tiny orange button process"
                                                 data-value="{{$data->student.'process'.$data->rToken.$data->position}}">
                                                <i class="icon check"></i>Process
                                            </div>
                                            <div class="ui tiny button negative reject"
                                                 data-value="{{$data->student.'reject'.$data->rToken.$data->position}}">
                                                <i class="icon close"></i>Reject
                                            </div>
                                        </td>
                                    @endif

                                </tr>
                                @endforeach
                        </tbody>
                    </table>
                </div>
            @endif
            <div class="ui bottom attached tab segment" data-tab="first" style="padding: 10px;">
                <div class="ui small warning message">
                    <p><strong>Student's name will be removed automatically 2 weeks after being moved to process tab,
                            but student that has been scheduled for interview will not be removed.</strong></p>
                </div>
                @if ($min_bipp_ipk = \App\Company::getBippIpk())
                    <div class="ui small green message">
                        <p>BIPP Member</p>
                    </div>
                @endif
                <table class="ui celled table" id="tableProcessed" style="width: 100%;">
                    <thead>
                    <tr>
                        <th class="three wide">Name</th>
                        <th class="one wide">Gender</th>
                        <th class="two wide">Position</th>
                        <th class="one wide">Phone</th>
                        <th class="one wide">E-mail</th>
                        <th class="one wide">Last IPK</th>
                        <th class="one wide">CV</th>
                        <th class="one wide">Job Desc</th>
                        <th class="one wide">Will be removed on</th>
                        <th class="one wide">Program</th>
                        <th class="one wide">Campus</th>
                        <th class="four wide">Status</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($datas as $data)
                        @if ($data->reStatus == 'rejected')
                            <tr class="negative">
                        @elseif ($data->stateAccepted == 'approved' && $data->stateStudent !== $user->userid)
                            <tr class="warning tr">
                        @elseif ($data->reStatus == 'approved')
                            <tr class="positive tr">
                        @else
                            <tr>
                                @endif
                                <td>
                                    <a href="create?studentid={{ $data->nim }}" data-content="Send Message to this user"
                                       class="send_message_btn"><i class="teal mail icon"></i></a>&nbsp;|&nbsp;
                                    <a href="profile/{{ $data->nim }}">{{ $data->student }}</a>
                                    @if(!empty($data->duration))
                                        <br>Internship Duration: {{ $data->duration }} month(s)
                                    @endif
                                </td>
                                <td>{{ ucfirst($data->gender) }}</td>
                                <td>{{ $data->position }}</td>

                                @if ($data->phone == NULL)
                                    <td>Not Set</td>
                                @else
                                    <td>{{ $data->phone }}</td>
                                @endif

                                @if ($data->email == NULL)
                                    <td>Not Set</td>
                                @else
                                    <td>{{ $data->email }}</td>
                                @endif
                                <td>
                                    {{ $data->last_ipk['ipk'] }}
                                </td>
                                <td>
                                    @if ($data->cv == 1)
                                        <a href="{{ url('/') }}/previewcvs/{{ $data->nim }}" target="_blank">
                                            <div class="ui tiny button">Preview</div>
                                        </a>
                                    @else
                                        CV not available!
                                    @endif
                                </td>
                                <td>
                                    <!-- button show desc -->
                                    <div class="ui small button showDescription{{ $data->token }}">Show</div>
                                </td>
                                <td>
                                    @if($data->reStatus == 'process')
                                        {{ $data->expired_at }}
                                    @endif
                                </td>
                                <td>{{ $data->program }}</td>
                                <td>{{ $data->campus }}</td>
                                @if ($data->stateAccepted == 'approved' && $data->stateStudent !== $user->userid)
                                    <td class="td">
                                        <div class="fields">
                                            <div class="field"><i class="tiny icon close"></i>Has been approved in
                                                another company
                                            </div>
                                        </div>
                                    </td>
                                @elseif ($data->stateAccepted == 'approved' && $data->stateStudent === $user->userid && $data->reStatus != 'approved')
                                    <td class="td">
                                        <div class="fields">
                                            <div class="field"><i class="tiny icon close"></i>Has been approved in your
                                                company
                                            </div>
                                        </div>
                                    </td>
                                @elseif ($data->reStatus == 'rejected')
                                    <td>
                                        <div class="fields">
                                            <div class="field">
                                                <i class="icon close"></i>Rejected

                                                @if($data->is_rejected_by_department)
                                                    <br>Rejected on: {{ $data->rejected_by_department_on }}
                                                @elseif(!empty($data->rejected_by) && $data->rejected_by == 'system')
                                                    <br>Rejected by system automatically (because the student did not
                                                    reply to the interview invitation within one week)
                                                @endif

                                                <div class="unreject_action" token="{{ $data->rToken }}"
                                                     style="display: inline-block;">
                                                    @if($data->unreject_request == null || $data->unreject_request == 0)
                                                        <span class="ui tiny button pink unreject"
                                                              nim="{{ $data->nim }}" name="{{ $data->student }}"
                                                              token="{{ $data->rToken }}"><i class="icon undo"></i>Unreject</span>
                                                    @elseif ($data->unreject_request > 0)
                                                        <span style="color: pink;"><i class="tiny icon undo"></i>Waiting unreject approval from department</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                @elseif ($data->reStatus == 'approved')
                                    <td class="td">
                                        <div class="fields">
                                            <div class="field">
                                                <i class="tiny icon check"></i>Accepted and Approved<br>
                                                <div class="unaccept_action" token="{{ $data->rToken }}"
                                                     style="display: inline-block;">
                                                    @if($data->unaccept_request == null || $data->unaccept_request == 0)
                                                        <span class="ui tiny button violet unaccept disabled"
                                                              nim="{{ $data->nim }}" name="{{ $data->student }}"
                                                              token="{{ $data->rToken }}"><i class="icon undo"></i>Unaccept</span>
                                                    @elseif ($data->unaccept_request > 0)
                                                        <span style="color: purple;"><i class="tiny icon undo"></i>Waiting unaccept approval from department</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                @elseif ($data->reStatus == 'accepted')
                                    <td class="td">
                                        <div class="fields">
                                            <div class="field">
                                                <i class="tiny icon check"></i>Accepted and Waiting Approval<br>
                                            </div>
                                        </div>
                                    </td>
                                @elseif ($data->reStatus == 'process')
                                    <td>

                                        @if ($data->inStatus == 'accept')
                                            <div class="ui tiny primary disabled button positive interviewButton"
                                                 style="pointer-events: auto;"
                                                 data-content="Accepted interview schedule"><i
                                                        class="calendar icon"></i></div>
                                        @elseif ($data->inStatus == 'waiting')
                                            <div class="ui tiny primary disabled button warning interviewButton"
                                                 style="pointer-events: auto;"
                                                 data-content="Waiting confirmation interview schedule"><i
                                                        class="calendar icon"></i></div>
                                        @elseif ($data->inStatus == 'reject')
                                            <div class="ui tiny primary disabled button negative interviewButton"
                                                 style="pointer-events: auto;"
                                                 data-content="Rejected interview schedule"><i
                                                        class="calendar icon"></i></div>
                                        @elseif ($data->interview_reschedule_request == 'yes')
                                            <div class="ui tiny brown button interview{{ $data->rToken }}"><i
                                                        class="calendar icon"></i>Reschedule Interview
                                            </div>
                                        @else
                                            <div class="ui tiny primary button interview{{ $data->rToken }}"><i
                                                        class="calendar icon"></i>Interview
                                            </div>
                                        @endif
                                        @if ($data->inStatus == 'accept')
                                            <p style="font-size: 11pt;">You can only accept after interview schedule on: {{ $data->inDate }} {{ $data->inTime }}</p>
                                            @if (\Carbon\Carbon::now()->greaterThan(\Carbon\Carbon::parse($data->inDate.' '.$data->inTime)))
                                                <div class="ui tiny button positive accept" nim="{{ $data->nim }}"
                                                     inStatus="{{ $data->inStatus }}"
                                                     data-value="{{$data->student.'accept'.$data->rToken.$data->position}}">
                                                    <i class="icon check"></i>Accept
                                                </div>
                                            @endif
                                        @endif
                                        <div class="ui tiny button negative reject" nim="{{ $data->nim }}"
                                             inStatus="{{ $data->inStatus }}"
                                             data-value="{{$data->student.'reject'.$data->rToken.$data->position}}"><i
                                                    class="icon close"></i>Reject
                                        </div>
                                    </td>
                                @else
                                    <td>
                                        <div class="ui tiny orange button process"
                                             data-value="{{$data->student.'process'.$data->rToken.$data->position}}"><i
                                                    class="icon check"></i>Process
                                        </div>
                                        <div class="ui tiny button negative reject"
                                             data-value="{{$data->student.'reject'.$data->rToken.$data->position}}"><i
                                                    class="icon close"></i>Reject
                                        </div>
                                    </td>
                                @endif

                            </tr>
                            @endforeach
                    </tbody>
                </table>
            </div>

            <div class="row">
                <div class="ui selection dropdown column">
                    <input type="hidden" name="position" id="positionStatus">
                    <i class="dropdown icon"></i>
                    <div class="default text">Position</div>
                    <div class="menu">
                        @foreach ($dataSort as $key)
                            <div class="item" data-value="{{ $key->position }}">{{ $key->position }}</div>
                        @endforeach
                    </div>
                </div>
                <div class="column">
                    <div class="ui mini green statistic">
                        <div class="value" id="valueApp">
                            0
                        </div>
                        <div class="label">
                            Approved
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @foreach ($dataUnprocessed as $data)
        <div class="ui modal showDescriptionUnprocessed{{ $data->token }}">
            <div class="header">Information</div>
            <div class="content">
                <div class="ui bulleted list">
                    <?php
                    $pieces = explode("\n", $data->description);
                    ?>
                    @foreach ($pieces as $piece)
                        <div class="item">{{ $piece }}</div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="ui modal showLink{{ $data->token }}">
            <div class="header">Information</div>
            <div class="content">
                <p>From : {{$data->companyname}}</p>
                <p>Job : {{$data->position}}</p>
                <p>Here is the<a target="_blank" href="{{$data->linktest}}"> Link </a> for Online test</p>
            </div>
        </div>
        <script>
            $('.showDescriptionUnprocessed{{ $data->token }}').click(function (e) {
                $('.ui.modal.showDescriptionUnprocessed{{ $data->token }}').modal('show');
            });
            $('.showLink{{ $data->token }}').click(function (e) {
                $('.ui.modal.showLink{{ $data->token }}').modal('show');
            });
        </script>
    @endforeach

    @foreach ($datas as $data)
        <div class="ui modal showDescription{{ $data->token }}">
            <div class="header">Information</div>
            <div class="content">
                <div class="ui bulleted list">
                    <?php
                    $pieces = explode("\n", $data->description);
                    ?>
                    @foreach ($pieces as $piece)
                        <div class="item">{{ $piece }}</div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="ui modal showLink{{ $data->token }}">
            <div class="header">Information</div>
            <div class="content">
                <p>From : {{$data->companyname}}</p>
                <p>Job : {{$data->position}}</p>
                <p>Here is the<a target="_blank" href="{{$data->linktest}}"> Link </a> for Online test</p>
            </div>
        </div>
        <script>
            $('.showDescription{{ $data->token }}').click(function (e) {
                $('.ui.modal.showDescription{{ $data->token }}').modal('show');
            });
            $('.showLink{{ $data->token }}').click(function (e) {
                $('.ui.modal.showLink{{ $data->token }}').modal('show');
            });
        </script>
    @endforeach

    @foreach ($datas as $data)
        <div class="ui modal interview {{ $data->rToken }}" style="overflow: auto;">
            <div class="header">Interview</div>
            <div class="content">
                {!! Form::open(['url' => 'setInterview', 'method' => 'post', 'id' => 'formInput', 'role' => 'form', 'class' => 'ui large interview form']) !!}
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="ui">

                    <div class="field">
                        <div class="ui left icon input">
                            <input type="text" name="txtLocation" id="idLocation" placeholder="Enter the location">
                            <i class="building icon"></i>
                        </div>
                    </div>

                    <div class="two fields">
                        <div class="field">
                            <div class="ui left icon input">
                                <input type="text" class="datepickerInterview" name="txtDate" placeholder="Date"
                                       size="30">
                                <i class="calendar icon"></i>
                            </div>
                        </div>

                        <div class="field">
                            <div class="ui left icon input">
                                <input type="text" class="timepickerTime" id="idTime" name="txtTime"
                                       placeholder="Time (HH:MM)">
                            </div>
                        </div>
                    </div>

                    <div class="two fields">
                        <div class="field">
                            <div class="ui left icon input">
                                <input type="text" id="idPIC" name="txtPIC" placeholder="Contact Person">
                                <i class="user icon"></i>
                            </div>
                        </div>

                        <div class="field">
                            <div class="ui left icon input">
                                <input type="text" id="idPhone" name="txtPhone" placeholder="Phone">
                                <i class="phone icon"></i>
                            </div>
                        </div>
                    </div>

                    <div class="field">
                        <div class="ui left icon input">
                            <input type="hidden" id="idUserid" name="txtNIM" value="{{ $data->nim }}">
                        </div>
                    </div>

                    <div class="field">
                        <div class="ui left icon input">
                            <input type="hidden" id="idToken" name="txtToken" value="{{ $data->rToken }}">
                        </div>
                    </div>

                    <button type="submit" class="ui button fluid large blue">Submit Interview Schedule</button>
                </div>
                <div class="ui error message"></div>
                {!! Form::close() !!}
            </div>
        </div>

        <script>
            $('.interview{{ $data->rToken }}').click(function (e) {
                $('.modal.interview.{{ $data->rToken }}').modal('show');
            });
        </script>

    @endforeach

    <input type="hidden" value="<?php echo str_replace('"', '&quot;', json_encode($counting)); ?>" id="dataTesting"/>

    <div class="ui small modal confirmation">
        <div class="header">Confirmation</div>
        <div class="content">
            <p id="contentApllicant">Are you sure you want to apply to xx as a xxx?</p>
        </div>
        <div class="actions">
            <div class="ui approve green button" id="yesButton">Yes</div>
            <div class="ui cancel red button" id="noButton">No</div>
        </div>
    </div>

    <div class="ui small modal unaccept_modal">
        <div class="header">Unaccept</div>
        <div class="content">
            <div class="ui form">
                {{ csrf_field() }}
                <div class="field">
                    <label>Please tell a reason why you want to unaccept <strong><span id="unaccept_studentName"></span></strong>?</label>
                    <textarea rows="2" name="unaccept_reason"></textarea>
                </div>
                <div class="ui error message" id="unaccept_errorMessage"></div>
            </div>
        </div>
        <div class="actions">
            <div class="ui approve green button" id="unaccept_yesButton">Yes</div>
            <div class="ui cancel red button" id="unaccept_noButton">No</div>
        </div>
    </div>

    <div class="ui small modal unreject_modal">
        <div class="header">Unreject</div>
        <div class="content">
            <div class="ui form">
                {{ csrf_field() }}
                <div class="field">
                    <label>Please tell a reason why you want to unreject <strong><span id="unreject_studentName"></span></strong>?</label>
                    <textarea rows="2" name="unreject_reason"></textarea>
                </div>
                <div class="ui error message" id="unreject_errorMessage"></div>
            </div>
        </div>
        <div class="actions">
            <div class="ui approve green button" id="unreject_yesButton">Yes</div>
            <div class="ui cancel red button" id="unreject_noButton">No</div>
        </div>
    </div>

    @if($old_unprocessed->count() > 0 || $old_processed->count() > 0)
        <div class="ui modal confirmation" id="modal_old">
            <div class="header">IMPORTANT</div>
            <div class="content">
                <h2>You still have old unprocessed job application.</h2>
                <p>Jumlah mahasiswa yang belum di-process: <span
                            class="ui label blue">{{ $old_unprocessed->count() }}</span></p>
                <p>Jumlah mahasiswa yang sudah di-process tapi belum di-accept / reject: <span
                            class="ui label blue">{{ $old_processed->count() }}</span></p>
                <span class="ui label red"><strong>IMPORTANT:</strong> Lihat message pada home page anda tentang peraturan unapply / cancel otomatis job application.</span>
            </div>
            <div class="actions">
                <div class="ui cancel red button">Close</div>
            </div>
        </div>
        <script>
            $('#modal_old').modal('show');
        </script>
    @endif

    <script type="text/javascript">
        $('.menu .item').tab();
        $('#tableProcessed').DataTable({"aaSorting": []});
        let sortableTableUnprocessed = $('#tableUnprocessed').DataTable({"aaSorting": []});
        @if (\App\Company::getBippIpk() != null)
        sortableTableUnprocessed.column('5').order('desc').draw();
                @endif


        var actionApplicant = "";
        var dataToken = "";
        var dataName = "";
        var dataPosition = "";

        $(document).on("click", ".ui.button.process", function () {
            actionApplicant = $(this).data("value").slice($(this).data("value").indexOf("process"), $(this).data("value").indexOf("process") + 7);
            dataToken = $(this).data("value").slice($(this).data("value").indexOf("process") + 7, $(this).data("value").indexOf("process") + 67);
            dataName = $(this).data("value").slice(0, $(this).data("value").indexOf("process"));
            dataPosition = $(this).data("value").slice($(this).data("value").indexOf("process") + 67);
            document.getElementById("contentApllicant").innerHTML = "Are you sure you want to process " + dataName + " as " + dataPosition + " ?";
        });

        $(document).on("click", ".ui.button.reject", function () {
            var nim = $(this).attr('nim');
            var inStatus = $(this).attr('inStatus');
            $('#yesButton').attr('nim', nim);
            $('#noButton').attr('nim', nim);
            $('#yesButton').attr('inStatus', inStatus);
            $('#noButton').attr('inStatus', inStatus);
            actionApplicant = $(this).data("value").slice($(this).data("value").indexOf("reject"), $(this).data("value").indexOf("reject") + 6);
            dataToken = $(this).data("value").slice($(this).data("value").indexOf("reject") + 6, $(this).data("value").indexOf("reject") + 66);
            dataName = $(this).data("value").slice(0, $(this).data("value").indexOf("reject"));
            dataPosition = $(this).data("value").slice($(this).data("value").indexOf("reject") + 66);
            if (inStatus != 'accept' && inStatus != 'waiting' && inStatus != 'reject') {
                document.getElementById("contentApllicant").innerHTML = "Do you want to <strong>create interview schedule</strong> before rejecting " + dataName + " as " + dataPosition + " ?";
            } else {
                document.getElementById("contentApllicant").innerHTML = "Are you sure you want to reject " + dataName + " as " + dataPosition + " ?";
            }
        });

        $(document).on("click", ".ui.button.accept", function () {
            var nim = $(this).attr('nim');
            var inStatus = $(this).attr('inStatus');
            $('#yesButton').attr('nim', nim);
            $('#noButton').attr('nim', nim);
            $('#yesButton').attr('inStatus', inStatus);
            $('#noButton').attr('inStatus', inStatus);
            actionApplicant = $(this).data("value").slice($(this).data("value").indexOf("accept"), $(this).data("value").indexOf("accept") + 6);
            dataToken = $(this).data("value").slice($(this).data("value").indexOf("accept") + 6, $(this).data("value").indexOf("accept") + 66);
            dataName = $(this).data("value").slice(0, $(this).data("value").indexOf("accept"));
            dataPosition = $(this).data("value").slice($(this).data("value").indexOf("accept") + 66);
            if (inStatus != 'accept' && inStatus != 'waiting' && inStatus != 'reject') {
                document.getElementById("contentApllicant").innerHTML = "Do you want to <strong>create interview schedule</strong> before accepting " + dataName + " as " + dataPosition + " ?";
            } else {
                document.getElementById("contentApllicant").innerHTML = "Are you sure you want to accept " + dataName + " as " + dataPosition + " ?";
            }
        });

        $('.ui.small.modal.unaccept_modal').modal('setting', {
            onApprove: function () {
                var token = $('#unaccept_yesButton').attr('token');
                $('#unaccept_yesButton').html('Processing...');
                $.ajax({
                    url: '/unaccept',
                    method: 'post',
                    data: {
                        rToken: token,
                        _token: $('[name="_token"]').val(),
                        reason: $('[name="unaccept_reason"]').val()
                    },
                    dataType: 'json',
                    error: function (data) {
                        $('#unaccept_errorMessage').html('<strong>' + data.message + '</strong>');
                        $('#unaccept_errorMessage').show();
                        $('#unaccept_yesButton').html('Yes');
                    },
                    success: function (data) {
                        if (data.message == 'Success') {
                            $('.ui.small.modal.unaccept_modal').modal('hide');
                            $('#unaccept_errorMessage').hide();
                            $('.unaccept_action[token="' + token + '"]').empty();
                            $('.unaccept_action[token="' + token + '"]').html('<span style="color: purple;"><i class="tiny icon undo"></i>Waiting unaccept approval from department</span>');
                        } else {
                            $('#unaccept_errorMessage').html('<strong>' + data.message + '</strong>');
                            $('#unaccept_errorMessage').show();
                        }
                        $('#unaccept_yesButton').html('Yes');
                    }
                });
                return false;
            },
            onDeny: function () {
                $('#unaccept_studentName').html('');
                $('#unaccept_yesButton').attr('token', '');
                $('[name="unaccept_reason"]').val('');
            }
        });
        $(document).on("click", ".ui.button.unaccept", function () {
            $('[name="unaccept_reason"]').val('');
            $('#unaccept_yesButton').html('Yes');
            $('#unaccept_studentName').html($(this).attr('name'));
            $('#unaccept_yesButton').attr('token', $(this).attr('token'));
            $('.ui.small.modal.unaccept_modal').modal('show');
            $('#unaccept_errorMessage>list').empty();
            $('#unaccept_errorMessage').hide();
        });

        $('.ui.small.modal.unreject_modal').modal('setting', {
            onApprove: function () {
                var token = $('#unreject_yesButton').attr('token');
                $('#unreject_yesButton').html('Processing...');
                $.ajax({
                    url: '/unreject',
                    method: 'post',
                    data: {
                        rToken: token,
                        _token: $('[name="_token"]').val(),
                        reason: $('[name="unreject_reason"]').val()
                    },
                    dataType: 'json',
                    error: function (data) {
                        $('#unreject_errorMessage').html('<strong>' + data.message + '</strong>');
                        $('#unreject_errorMessage').show();
                        $('#unreject_yesButton').html('Yes');
                    },
                    success: function (data) {
                        if (data.message == 'Success') {
                            $('.ui.small.modal.unreject_modal').modal('hide');
                            $('#unreject_errorMessage').hide();
                            $('.unreject_action[token="' + token + '"]').empty();
                            $('.unreject_action[token="' + token + '"]').html('<span style="color: pink;"><i class="tiny icon undo"></i>Waiting unreject approval from department</span>');
                        } else {
                            $('#unreject_errorMessage').html('<strong>' + data.message + '</strong>');
                            $('#unreject_errorMessage').show();
                        }
                        $('#unreject_yesButton').html('Yes');
                    }
                });
                return false;
            },
            onDeny: function () {
                $('#unreject_studentName').html('');
                $('#unreject_yesButton').attr('token', '');
                $('[name="unreject_reason"]').val('');
            }
        });
        $(document).on("click", ".ui.button.unreject", function () {
            $('[name="unreject_reason"]').val('');
            $('#unreject_yesButton').html('Yes');
            $('#unreject_studentName').html($(this).attr('name'));
            $('#unreject_yesButton').attr('token', $(this).attr('token'));
            $('.ui.small.modal.unreject_modal').modal('show');
            $('#unreject_errorMessage>list').empty();
            $('#unreject_errorMessage').hide();
        });


        $('.ui.small.modal.confirmation').modal("setting",
            {
                blurring: true,
                onApprove: function () {
                    var inS = $('#yesButton').attr('inStatus');
                    if (actionApplicant == "reject") {
                        if (inS != 'accept' && inS != 'reject' && inS != 'waiting') {
                            $('.modal.interview.' + ($('#yesButton').attr('nim'))).modal('show');
                        } else {
                            window.location.href = "{{ url('/') }}/reject/" + dataToken;
                        }
                    } else if (actionApplicant == "accept") {
                        if (inS != 'accept' && inS != 'reject' && inS != 'waiting') {
                            $('.modal.interview.' + ($('#yesButton').attr('nim'))).modal('show');
                        } else {
                            window.location.href = "{{ url('/') }}/accept/" + dataToken;
                        }
                    } else if (actionApplicant == "process") {
                        window.location.href = "{{ url('/') }}/process/" + dataToken;
                    }
                },
                onDeny: function () {
                    var inS = $('#noButton').attr('inStatus');
                    if (actionApplicant == "reject" && inS != 'accept' && inS != 'reject' && inS != 'waiting') {
                        window.location.href = "{{ url('/') }}/reject/" + dataToken;
                    } else if (actionApplicant == "accept" && inS != 'accept' && inS != 'reject' && inS != 'waiting') {
                        window.location.href = "{{ url('/') }}/accept/" + dataToken;
                    }
                }
            });

        $(document).on('click', '.ui.button.process', function (e) {
            $('.ui.small.modal.confirmation').modal('show');
        });
        $(document).on('click', '.ui.button.reject', function (e) {
            $('.ui.small.modal.confirmation').modal('show');
        });
        $(document).on('click', '.ui.button.accept', function (e) {
            $('.ui.small.modal.confirmation').modal('show');
        });
    </script>
@stop
