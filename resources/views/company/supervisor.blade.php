@extends ('layout.masterC')

@section ('title', 'BINUS')

@section('content')

@include('partial/headerCompany')

<br><br>
<div class="ui container">
  <div class="ui basic segment">
    <div class="ui grid">
      @if (session('fyi'))
      <div class="row">

        @if (session('fyi') == 'Berhasil')
          <div class="ui positive message">
            <i class="close icon"></i>
            <div class="header">
              You have successfully added supervisor.
            </div>
          </div>
        @elseif (session('fyi') == 'Berhasilupdate')
        <div class="ui positive message">
          <i class="close icon"></i>
          <div class="header">
            You have successfully updated supervisor.
          </div>
        </div>
      @elseif (session('fyi') == 'Berhasildelete')
        <div class="ui positive message">
          <i class="close icon"></i>
          <div class="header">
            You have successfully deleted supervisor.
          </div>
        </div>

        @else
          <div class="ui negative message">
            <i class="close icon"></i>
            <div class="header">
              {{ session('fyi') }}
            </div>
          </div>
        @endif

      </div>
      @endif

      <div class="row centered">
        <h3 class="ui header">List of Company Supervisor</h3>
      </div>

      <div class="row">
        <div class="ui large button insertSupervisor">Insert</div>
        <a href="{{ url('/') }}/assignStudent"><div class="ui large primary button assignSupervisor">Assign Supervisor</div></a>
      </div>

      <div class="row">
        <table class="ui striped table">
          <thead>
            <tr>
              <th>Name</th>
              <th>Position</th>
              <th>Email</th>
              <th>Phone</th>
              <th>Address</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            @foreach ($datas as $data)
              <tr>
              <td>{{ $data->name }}</td>
              <td>{{ $data->position }}</td>
              <td>{{ $data->email }}</td>
              <td>{{ $data->phone }}</td>
              <td>{{ $data->address }}</td>
              <td>
                <div class="ui small button updateSupervisor{{ $data->token }}">Update</div>
                <a class="ui button delete spv" data-value="{{'spv'.$data->token.$data->name}}">Delete</a>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>

    </div>
  </div>
</div>

@foreach ($datas as $data)
<div class="ui modal {{ $data->token }}" style="overflow: auto;">
  <div class="header">Update Supervisor</div>
  <div class="content">
    {!! Form::open(['url' => 'updateSupervisors', 'method' => 'post', 'id' => 'formInput', 'role' => 'form', 'class' => 'ui large form supervisor']) !!}
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <div class="ui">

        <div class="field">
          <div class="ui left icon input">
            <input type="text" name="txtuName" required id="idName" placeholder="Name" value="@if(old('txtuName')!=null){{old('txtuName')}}@else{{$data->name}}@endif">
            <i class="user icon"></i>
          </div>
        </div>

        <div class="field">
          <div class="ui left icon input">
            <input type="text" name="txtuPosition" required id="idPosition" placeholder="Position" value="@if(old('txtuPosition')!=null){{old('txtuPosition')}}@else{{$data->position}}@endif">
            <i class="spy icon"></i>
          </div>
        </div>

        <div class="field">
          <div class="ui left icon input">
            <input type="text" name="txtuEmail" required id="idEmail" placeholder="Email" value="@if(old('txtuEmail')!=null){{old('txtuEmail')}}@else{{$data->email}}@endif">
            <i class="mail icon"></i>
          </div>
        </div>

        <div class="field">
          <div class="ui left icon input">
            <input type="text" name="txtuPhone" required id="idPhone" placeholder="Phone" value="@if(old('txtuPhone')!=null){{old('txtuPhone')}}@else{{$data->phone}}@endif">
            <i class="phone icon"></i>
          </div>
        </div>

        <div class="field">
          <div class="ui left icon input">
            <input type="text" name="txtuAddress" required id="idAddress" placeholder="Address" value="@if(old('txtuAddress')!=null){{old('txtuAddress')}}@else{{$data->address}}@endif">
            <i class="building icon"></i>
          </div>
        </div>

        <div class="field">
          <div class="ui left icon input">
            <input type="hidden" id="idToken" name="txtuToken" placeholder="Token" value="{{ $data->token }}" >
          </div>
        </div>

        <div class="ui fluid large blue submit button">Update</div>

      </div>
      <div class="ui error message"></div>
    {!! Form::close() !!}
  </div>
</div>

<script>
  $('.updateSupervisor{{ $data->token }}').click(function(e) {
    $('.ui.modal.{{ $data->token }}').modal('show');
  });


</script>

@endforeach

<div class="ui small modal confirmation">
  <div class="header">Confirmation</div>
  <div class="content">
    <p id="contentApllicant">Are you sure you want to apply to xx as a xxx?</p>
  </div>
  <div class="actions">
    <div class="ui approve green button">Yes</div>
    <div class="ui cancel red button">No</div>
  </div>
</div>

<script type="text/javascript">
  var dataToken = "";
  var dataName = "";

  $(document).on("click", ".ui.button.delete.spv", function ()
  {
    dataToken=$(this).data("value").slice(3, 63);
    dataName=$(this).data("value").slice(63);
    document.getElementById("contentApllicant").innerHTML = "Are you sure you want to delete "+dataName+" ?";
  });

  $('.ui.small.modal.confirmation').modal("setting",
  {
    blurring: true,
    onApprove: function ()
    {
        window.location.href = "{{ url('/') }}/deleteSpv/"+dataToken;
    }
  }).modal('attach events', '.ui.button.delete.spv', 'show') ;
</script>

<div class="ui modal insertSupervisor" style="overflow: auto;">
  <div class="header">Insert Supervisor</div>
  <div class="content">
    {!! Form::open(['url' => 'insertSupervisors', 'method' => 'post', 'id' => 'formInput', 'role' => 'form', 'class' => 'ui large form supervisor']) !!}
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <div class="ui">

        <div class="field">
          <div class="ui left icon input">
            <input type="text" name="txtiName" required id="idName" placeholder="Name" value="@if(old('txtiName')!=null){{old('txtiName')}}@endif">
            <i class="user icon"></i>
          </div>
        </div>

        <div class="field">
          <div class="ui left icon input">
            <input type="text" name="txtiPosition" required id="idPosition" placeholder="Position" value="@if(old('txtiPosition')!=null){{old('txtiPosition')}}@endif">
            <i class="spy icon"></i>
          </div>
        </div>

        <div class="field">
          <div class="ui left icon input">
            <input type="text" name="txtiEmail" required id="idEmail" placeholder="Email" value="@if(old('txtiEmail')!=null){{old('txtiEmail')}}@endif">
            <i class="mail icon"></i>
          </div>
        </div>

        <div class="field">
          <div class="ui left icon input">
            <input type="text" name="txtiPhone" required id="idPhone" placeholder="Phone" value="@if(old('txtiPhone')!=null){{old('txtiPhone')}}@endif">
            <i class="phone icon"></i>
          </div>
        </div>

        <div class="field">
          <div class="ui left icon input">
            <input type="text" name="txtiAddress" required id="idAddress" placeholder="Address" value="@if(old('txtiAddress')!=null){{old('txtiAddress')}}@endif">
            <i class="building icon"></i>
          </div>
        </div>

        <div class="ui fluid large blue submit button">Insert</div>

      </div>
      <div class="ui error message"></div>
    {!! Form::close() !!}
  </div>
</div>


@stop
