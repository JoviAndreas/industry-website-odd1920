@extends ('layout.masterC')

@section ('title', 'BINUS')

@section('content')

    @include('partial/headerCompany')


    {!! Form::open(['url' => 'submitprofiles', 'method' => 'post', 'enctype' => 'multipart/form-data', 'id' => 'formInput', 'role' => 'form', 'class' => 'ui large editProfile form']) !!}
    <div class="ui vertical stripe segment">
        <div class="ui middle aligned stackable grid container">
            @if($errors->first() != null)
                <div class="ui red message">{{$errors->first()}}</div>
            @endif
            <div class="row">
                <div class="four wide column">
                    <img class="ui medium rounded image" src="{{ url('/images/'.$userProfile->companyid) }}"
                         onerror="this.src='{{ URL::asset('assets/images/noimage.jpg') }}'">
                    <br/>
                    <input type="file" id="profpic" name="profpic"/>
                </div>
                <div class="twelve wide column">
                    <div class="row">
                        <h1>{{ $userProfile->name }}</h1>
                    </div>
                    <br/>
                    <br/>

                    <div class="row">
                        <div class="ui segment" style="padding: 1%;">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="row"><h4>Contact Me</h4></div>
                            <br/>
                            <div class="ui grid" style="padding-bottom: 0 auto; padding-top: 0 auto;">
                                <div class="three wide column">Company Name</div>
                                <div class="twelve wide column">
                                    <div class="field">
                                        <input type="text" required maxlength="50" name="txtCompanyName"
                                               placeholder="Company Name"
                                               value="@if(old('txtCompanyName')==null){{$userProfile->name}}@else{{old('txtCompanyName')}}@endif">
                                    </div>
                                </div>
                            </div>
                            <div class="ui grid" style="padding-bottom: 0 auto; padding-top: 0 auto;">
                                <div class="three wide column">Business Category</div>
                                <div class="twelve wide column">
                                    <div class="field">
                                        <select name="cmbBusiness" id="idBusiness">
                                            <option value="" selected disabled>Choose</option>
                                            <option value="Global / Multinasional"
                                                    @if($userProfile->business_category == "Global / Multinasional") selected="selected" @endif>
                                                Global / Multinasional
                                            </option>
                                            <option value="Lokal"
                                                    @if($userProfile->business_category == "Lokal") selected="selected" @endif>
                                                Lokal
                                            </option>
                                            <option value="BUMN"
                                                    @if($userProfile->business_category == "BUMN") selected="selected" @endif>
                                                BUMN
                                            </option>
                                        </select>

                                    </div>
                                </div>
                            </div>
                            <div class="ui grid" style="padding-bottom: 0 auto; padding-top: 0 auto;">
                                <div class="three wide column">Contact Person</div>
                                <div class="twelve wide column">
                                    <div class="field">
                                        <input type="text" required maxlength="50" name="txtPic" id="idPic"
                                               placeholder="Contact Person"
                                               value="@if(old('txtPic')==null){{$userProfile->pic}}@else{{old('txtPic')}}@endif">
                                    </div>
                                </div>
                            </div>
                            <div class="ui grid" style="padding-bottom: 0 auto; padding-top: 0 auto;">
                                <div class="three wide column">Email</div>
                                <div class="twelve wide column">
                                    <div class="field">
                                        <input type="text" required maxlength="100" name="txtEmail" id="idEmail"
                                               placeholder="Email"
                                               value="@if(old('txtEmail')==null){{$userProfile->email}}@else{{old('txtEmail')}}@endif">
                                    </div>
                                </div>
                            </div>
                            <div class="ui grid" style="padding-bottom: 0 auto; padding-top: 0 auto;">
                                <div class="three wide column">Phone</div>
                                <div class="twelve wide column">
                                    <div class="field">
                                        <input type="text" required maxlength="100" name="txtPhone" id="idPhone"
                                               placeholder="Phone"
                                               value="@if(old('txtPhone')==null){{$userProfile->phone}}@else{{old('txtPhone')}}@endif">
                                    </div>
                                </div>
                            </div>
                            <div class="ui grid" style="padding-bottom: 0 auto; padding-top: 0 auto;">
                                <div class="three wide column">Address</div>
                                <div class="twelve wide column">
                                    <div class="field">
                                        <input type="text" required maxlength="250" name="txtAddress" id="idAddress"
                                               placeholder="Address"
                                               value="@if(old('txtAddress')==null){{$userProfile->address}}@else{{old('txtAddress')}}@endif">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="ui segment" style="padding: 1%;">
                            <div class="row"><h4>Social Network</h4></div>
                            <br/>
                            <div class="ui grid" style="padding-bottom: 0 auto; padding-top: 0 auto;">
                                <div class="three wide column">Website</div>
                                <div class="twelve wide column">
                                    <div class="field">
                                        <input type="text" maxlength="100" name="txtWebsite" id="idWebsite"
                                               placeholder="www.example.com"
                                               value="@if(old('txtWebsite')==null){{ $userProfile->website }}@else{{ old('txtWebsite') }}@endif">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <button class="ui primary button" type="submit">Submit</button>
                </div>
            </div>
        </div>

        {!! Form::close() !!}


        <script type="text/javascript">
            $("#profpic").change(function () {

                var val = $(this).val();

                switch (val.substring(val.lastIndexOf('.') + 1).toLowerCase()) {
                    case 'gif':
                    case 'jpg':
                    case 'png':
                    case 'jpeg':
                        break;
                    default:
                        $(this).val('');
                        // error message here
                        alert("You must enter an image File!");
                        break;
                }
            });

        </script>
@stop