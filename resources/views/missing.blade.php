<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>404 | Binus</title>
</head>

<body id="example">

<div class="masthead error segment">
  <div class="container">
    <h1 class="ui dividing header">
      That happens not to be a page not found
    </h1>
    <p>Rewind and try another one</p>
  </div>
</div>
</body>

</html>