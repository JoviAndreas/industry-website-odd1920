<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Standard Meta -->

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

    <!-- Site Properties -->
    <link rel="icon" type="image/png" href="{{ URL::asset('assets/images/favicon.png') }}">
    <title>@yield('title')</title>
    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/semantic/dist/semantic.min.css">
    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/semantic/dist/components/dataTables.semanticui.min.css">

    <script type="text/javascript" src="{{ url('/') }}/semantic/dist/jquery.js"></script>
    <script type="text/javascript" src="{{ url('/') }}/semantic/dist/semantic.min.js"></script>
    <script type="text/javascript" src="{{ url('/') }}/semantic/dist/components/jquery.dataTables.min.js" ></script>
    <script type="text/javascript" src="{{ url('/') }}/semantic/dist/components/dataTables.semanticui.min.js" ></script>

    <style type="text/css">
      .hidden.menu {
        display: none;
      }

      .ui.vertical.stripe {
        padding: 8em 0em;
      }
      .ui.vertical.stripe h3 {
        font-size: 2em;
      }
      .ui.vertical.stripe .button + h3,
      .ui.vertical.stripe p + h3 {
        margin-top: 3em;
      }
      .ui.vertical.stripe .floated.image {
        clear: both;
      }
      .ui.vertical.stripe p {
        font-size: 1.33em;
      }
      .ui.vertical.stripe .horizontal.divider {
        margin: 3em 0em;
      }

      .quote.stripe.segment {
        padding: 0em;
      }
      .quote.stripe.segment .grid .column {
        padding-top: 5em;
        padding-bottom: 5em;
      }

      .footer.segment {
        padding: 5em 0em;
      }

      .secondary.pointing.menu .toc.item {
        display: none;
      }

      .ui.segment{
        padding: 0em;
      }

      .ui.fullscreen.modal{
        height: 90%;
      }

      @media only screen and (max-width: 700px) {
        .ui.fixed.menu {
          display: none !important;
        }
        .secondary.pointing.menu .item,
        .secondary.pointing.menu .menu {
          display: none;
        }
        .secondary.pointing.menu .toc.item {
          display: block;
        }
      }

      #notification_li
      {
        position:relative
      }

      #notificationContainer
      {
        background-color: #fff;
        border: 1px solid rgba(100, 100, 100, .4);
        -webkit-box-shadow: 0 3px 8px rgba(0, 0, 0, .25);
        overflow: visible;
        position: absolute;
        top: 30px;
        margin-left: -170px;
        width: 400px;
        z-index: 100;
        display: none; // Enable this after jquery implementation
      }

      // Popup Arrow
      #notificationContainer:before {
        content: '';
        display: block;
        position: absolute;
        width: 0;
        height: 0;
        color: transparent;
        border: 10px solid black;
        border-color: transparent transparent white;
        margin-top: -20px;
        margin-left: 188px;
      }

      #notificationTitle
      {
        font-weight: bold;
        padding: 8px;
        font-size: 13px;
        background-color: #ffffff;
        position: fixed;
        z-index: 1000;
        width: 384px;
        border-bottom: 1px solid #dddddd;
        color: #000000;
      }

      #notificationsBody
      {
        padding: 33px 0px 0px 0px !important;
        min-height:300px;
      }

      #notificationFooter
      {
        background-color: #e9eaed;
        text-align: center;
        font-weight: bold;
        padding: 8px;
        font-size: 12px;
        border-top: 1px solid #dddddd;
      }

      #notification_count
      {
        padding: 3px 7px 3px 7px;
        background: #cc0000;
        color: #ffffff;
        font-weight: bold;
        margin-left: 35%;
        border-radius: 9px;
        -moz-border-radius: 9px;
        -webkit-border-radius: 9px;
        position: absolute;
        margin-top: -25%;
        font-size: 11px;
      }

      #mailbox_count
      {
        padding: 3px 7px 3px 7px;
        background: #cc0000;
        color: #ffffff;
        font-weight: bold;
        margin-left: 35%;
        border-radius: 9px;
        -moz-border-radius: 9px;
        -webkit-border-radius: 9px;
        position: absolute;
        margin-top: -25%;
        font-size: 11px;
      }

      #notificationLink:hover{color:#00aaff;text-decoration:none;margin-right:}

      #mailboxLink:hover{color:#00aaff;text-decoration:none}

      #notification_lis
      {
        position:relative
      }

      #notificationContainers
      {
        background-color: #fff;
        border: 1px solid rgba(100, 100, 100, .4);
        -webkit-box-shadow: 0 3px 8px rgba(0, 0, 0, .25);
        overflow: visible;
        position: absolute;
        top: 30px;
        margin-left: -170px;
        width: 400px;
        z-index: 100;
        display: none; // Enable this after jquery implementation
      }

      // Popup Arrow
      #notificationContainers:before {
        content: '';
        display: block;
        position: absolute;
        width: 0;
        height: 0;
        color: transparent;
        border: 10px solid black;
        border-color: transparent transparent white;
        margin-top: -20px;
        margin-left: 188px;
      }

      #notificationTitles
      {
        font-weight: bold;
        padding: 8px;
        font-size: 13px;
        background-color: #ffffff;
        position: fixed;
        z-index: 1000;
        width: 384px;
        border-bottom: 1px solid #dddddd;
        color: #000000;
      }

      #notificationsBodys
      {
        padding: 33px 0px 0px 0px !important;
        min-height:300px;
      }

      #notificationFooters
      {
        background-color: #e9eaed;
        text-align: center;
        font-weight: bold;
        padding: 8px;
        font-size: 12px;
        border-top: 1px solid #dddddd;
      }

      #notification_counts
      {
        padding: 3px 7px 3px 7px;
        background: #cc0000;
        color: #ffffff;
        font-weight: bold;
        margin-left: 35%;
        border-radius: 9px;
        -moz-border-radius: 9px;
        -webkit-border-radius: 9px;
        position: absolute;
        margin-top: -25%;
        font-size: 11px;
      }

      #mailbox_counts
      {
        padding: 3px 7px 3px 7px;
        background: #cc0000;
        color: #ffffff;
        font-weight: bold;
        margin-left: 35%;
        border-radius: 9px;
        -moz-border-radius: 9px;
        -webkit-border-radius: 9px;
        position: absolute;
        margin-top: -25%;
        font-size: 11px;
      }

      #notificationLinks:hover{color:#00aaff;text-decoration:none;margin-right:}

      #mailboxLinks:hover{color:#00aaff;text-decoration:none}
    </style>

    <script src="{{ url('/') }}/semantic/dist/components/visibility.js"></script>
    <script src="{{ url('/') }}/semantic/dist/components/sidebar.js"></script>
    <script src="{{ url('/') }}/semantic/dist/components/transition.js"></script>
    <script src="{{ url('/') }}/ckeditor/ckeditor.js"></script>
    <script src="{{ url('/') }}/ckeditor/adapters/jquery.js"></script>
    <script>
    $(document)
      .ready(function() {

        $("#notificationLink").click(function(){
          $("#notificationContainer").fadeToggle(300);
          $("#notification_count").fadeOut("slow");
          $.get("{{ url('/') }}/readNotif",function(data,status) {},'html');
          return false;
        });

        //Document Click hiding the popup
        $(document).click(function(){
          $("#notificationContainer").hide();
        });

        //Document Click hiding the popup
        $(document).click(function(){
          $("#notificationContainers").hide();
        });

        $("#notificationLinks").click(function(){
          $("#notificationContainers").fadeToggle(300);
          $("#notification_counts").fadeOut("slow");
          $.get("{{ url('/') }}/readNotif",function(data,status) {},'html');
          return false;
        });


      //var a = $('html:last').height();
        $('#idMessage').ckeditor();

        CKEDITOR.config.width = '100%';
        CKEDITOR.config.height = $(this).height() * 0.339;

        // fix menu when passed
        $('.invis')
          .visibility({
            once: false,
            onBottomPassed: function() {
              $('.fixed.menu').transition('fade in');
            },
            onBottomPassedReverse: function() {
              $('.fixed.menu').transition('fade out');
            }
          })
        ;

        $('.popup.td').popup();

        var data = $('#dataTesting').val();
        //var parsedData = JSON.parse(data);
        $('#positionStatus').change(function() {
          //Ubah jumlah approve
          $('#valueApp').html(parsedData[$(this).val()]);
        });

        $('#LPMode').change(function() {
          //Ubah LPMode
          if($(this).val() == "1"){
            $('#LPDesc').html("1 Learning plan for all Intern.");
          }else if ($(this).val() == "2"){
            $('#LPDesc').html("1 Learning plan for all Intern in the same position.");
          }else if ($(this).val() == "3"){
            $('#LPDesc').html("1 Learning plan for every individual Intern.");
          }
        });

        // create sidebar and attach to menu open
        $('.ui.sidebar')
          .sidebar('attach events', '.toc.item')
        ;

        $('.ui.dropdown')
          .dropdown({fullTextSearch: true})
        ;

        $('.selection.message.dropdown')
          .dropdown({
            action: 'activate',
            onChange: function(value, text, $selectedItem) {
              // custom action
              alert(value+text+$selectedItem);
            }
          })
        ;

        $('#statusTable').DataTable();

        $('.insertJob').click(function(e) {
          $('.ui.modal').modal('show');
        });




          var scntDiv = $('.jobDesc');

          $('.plusIcon').click(function(){
            $(this).transition('pulse');
            $('<div class="field"><div class="ui left icon input"><input type="text" name="txtJobDescription" placeholder="Job Description"></div></div>').appendTo(scntDiv);
          });

          $('.showModal').click(function(e) {
            $('.fullscreen.modal').modal('show');
            document.getElementById('descriptionId').style.height = '100%';
            $("html, body").animate({ scrollTop: $(document).height()-$(window).height() });
          });

          $('.ui.large.form')
            .form({
              fields: {
                txtMessage     : ['empty', 'maxLength[1000]'],
                txtSubject   : ['empty', 'maxLength[150]'],
              }
            })
          ;
          $('.ui.large.change.form')
            .form({
              fields: {
                NewPassword   : ['empty', 'minLength[6]', 'maxLength[20]', 'match[ConfirmationPassword]'],
                ConfirmationPassword   : ['empty', 'minLength[6]', 'maxLength[20]']
              }
            })
          ;

          $('.inboxsent.menu .item')
            .tab()
          ;
          $('.inboxsents.menu .item')
            .tab()
          ;

      })
    ;
    </script>
  </head>
  <body class="pushable">

    <!-- Following Menu -->
    <?php
      $user = Auth::user();
      if (strcasecmp($user->role, "company") == 0){
    ?>
    <!-- Company hidden menu -->
      <div class="ui large top fixed hidden menu">
        <div class="ui container">
          <a <?php if($_SERVER["REQUEST_URI"] == '/homec'){echo 'class="active item"';}else{echo 'class="item"';} ?> href="{{ url('/') }}/homec">Home</a>
          <a class="item" href="{{ url('/') }}/profilec/{{ $user->userid }}">Profile</a>
          <div class="ui pointing dropdown link item">
                <span class="text">Manage</span>
                <i class="dropdown icon"></i>
                <div class="menu">
                  <a href="{{ url('/') }}/job"><div class="item">Job</div></a>
                  <a href="{{ url('/') }}/supervisor"><div class="item">Supervisor</div></a>
                </div>
          </div>
          <a class="item" href="{{ url('/') }}/feedbacks">Feedback</a>
          <div class="right menu">
            <div class="item" style="padding-top:0px; padding-bottom: 0px;">
              @if ($mailboxCount>0)
                <span id="mailbox_count">{{ $mailboxCount }}</span>
              @endif
              <a href="{{ url('/') }}/mailbox">
                <i id="mailboxLink" class="middle aligned mail outline large icon"></i>
              </a>
            </div>

            <div class="item" style="padding-top:0px; padding-bottom: 0px;">
              @if($notifCount > 0)<span id="notification_counts">{{ $notifCount }}</span>@endif
              <a><i class="middle aligned info circle large icon" id="notificationLinks" style="cursor:pointer;"></i></a>
              <div id="notificationContainers">
              <div id="notificationTitles">Notifications</div>
              <div id="notificationsBodys" class="notifications ">
                <div class="ui relaxed divided list">
                  @foreach ($notif as $item)
                  <div class="item" style="padding-bottom: 0%;">
                    <div class="content">
                      <div class="header" style="text-align: center;">{{ $item->subject }}</div>
                      <div class="description" style="text-align: center;">{{ $item->message }}</div>
                    </div>
                  </div>
                  <hr/>
                  @endforeach
                </div>
              </div>
              <div id="notificationFooters"><a href="{{ url('/') }}/notif">See All</a></div>
              </div>
            </div>
            
            <a class="ui button" href="{{ url('/') }}/auth/logout">Log Out</a>
          </div>
        </div>
      </div>
    <?php
  }else if (strcasecmp($user->role, "department") == 0){
    ?>

    <!-- Department hidden menu -->
      <div class="ui large top fixed hidden menu">
        <div class="ui container">
          <a class="
          {{Request::is('homed')==1 ? 'active' : '' }} item"
          href="{{ url('/') }}/homed">Home</a>

          <div class="ui pointing dropdown link item">
            <span class="text">Others</span>
            <i class="dropdown icon"></i>
            <div class="ui menu">
               <a class="
              {{Request::is('passwords')==1 ? 'active' : '' }} item"
              href="{{ url('/') }}/passwords">Master Password</a>
              <a class="
              {{Request::is('addDepartmentAccount')==1 ? 'active' : '' }} item"
              href="{{ url('/') }}/addDepartmentAccount">Department Account</a>
              <a class="
             {{Request::is('seeFeedbacks')==1 ? 'active' : '' }} item"
             href="{{ url('/') }}/seeFeedbacks">See Feedbacks</a>
            </div>
          </div>

{{--          <div class="ui pointing dropdown link item">--}}
{{--            <span class="text">Master</span>--}}
{{--            <i class="dropdown icon"></i>--}}
{{--            <div class="ui menu">--}}
{{--            @if (\App\Department::isAdmin())--}}
{{--            <a class="--}}
{{--            {{Request::is(route('department.faculty'))==1 ? 'active' : '' }}--}}
{{--            item" href="{{ route('department.faculty') }}">Faculty & Degree</a>--}}
{{--            @endif--}}

{{--              <a class="--}}
{{--              {{Request::is('manageStudent')==1 ? 'active' : '' }}--}}
{{--              item" href="{{ url('/') }}/manageStudent">Student</a>--}}
{{--              --}}
{{--              <a class="--}}
{{--              {{Request::is('register')==1 ? 'active' : '' }}--}}
{{--              item" href="{{ url('/') }}/register">Company</a>--}}
{{--              --}}
{{--              <a class="--}}
{{--              {{Request::is('manageguider')==1 ? 'active' : '' }}--}}
{{--              item" href="{{ url('/') }}/manageguider">University Supervisor</a>--}}

{{--              <a class="--}}
{{--              {{Request::is('manageJob')==1 ? 'active' : '' }}--}}
{{--              item" href="{{ url('/') }}/manageJob">Vacancies</a>--}}
{{--              --}}
{{--              <a class="--}}
{{--              {{Request::is('manageJobPosition')==1 ? 'active' : '' }}--}}
{{--              item" href="{{ url('/') }}/manageJobPosition">Job Position</a>--}}

{{--              <a class="--}}
{{--              {{Request::is('manageDeletedJob')==1 ? 'active' : '' }}--}}
{{--              item" href="{{ url('/') }}/manageDeletedJob">Deleted Job</a>--}}

{{--            </div>--}}
{{--          </div>--}}
{{--          <div class="ui pointing dropdown link item">--}}
{{--            <span class="text">Organize</span>--}}
{{--            <i class="dropdown icon"></i>--}}
{{--            <div class="ui menu">--}}
{{--              <a class="--}}
{{--              {{Request::is('apply')==1 ? 'active' : '' }}--}}
{{--              item" href="{{ url('/') }}/apply">Apply Student to Job</a>--}}

{{--              <a class="--}}
{{--              {{Request::is('accepted')==1 ? 'active' : '' }}--}}
{{--              item" href="{{ url('/') }}/accepted">Student Placement</a>--}}

{{--              <a class="--}}
{{--              {{Request::is('editRecruitment')==1 ? 'active' : '' }}--}}
{{--              item" href="{{ url('/') }}/editRecruitment">Edit Recruitment</a>--}}
{{--              @if(\App\Department::isSuperAdmin())--}}
{{--              <a class="--}}
{{--                {{Request::is('editBipp')==1 ? 'active' : '' }}--}}
{{--                item" href="{{ url('/') }}/editBipp">Edit BIPP</a>--}}
{{--                --}}
{{--              <a class="--}}
{{--              {{Request::is('editDesc')==1 ? 'active' : '' }}--}}
{{--              item" href="{{ url('/') }}/editDesc">Edit Description</a>--}}

{{--              <a class="--}}
{{--              {{Request::is('unaccept_request')==1 ? 'active' : '' }}--}}
{{--              item" href="{{ url('/') }}/unaccept_request">Unaccept Status</a>--}}
{{--              @endif--}}
{{--              <a class="--}}
{{--              {{Request::is('unreject_request')==1 ? 'active' : '' }}--}}
{{--              item" href="{{ url('/') }}/unreject_request">Unreject Request</a>--}}

{{--              <a class="--}}
{{--              {{Request::is('approval_request')==1 ? 'active' : '' }}--}}
{{--              item" href="{{ url('/') }}/approval_request">Approval Request</a>--}}

{{--              <a class="--}}
{{--              {{Request::is('assign_supervisor')==1 ? 'active' : '' }}--}}
{{--              item" href="{{ url('/') }}/assign_supervisor">Assign Supervisor</a>--}}
{{--            </div>--}}
{{--          </div>--}}

{{--          <div class="ui pointing dropdown link item">--}}
{{--            <span class="text">User Management</span>--}}
{{--            <i class="dropdown icon"></i>--}}
{{--             <div class="ui menu">--}}
{{--               <a class="--}}
{{--                {{Request::is('accountapproval')==1 ? 'active' : '' }} item"--}}
{{--                href="{{ url('/') }}/accountapproval">Account Approval</a>--}}

{{--                 <a class="--}}
{{--                {{Request::is('activestudentlist')==1 ? 'active' : '' }} item"--}}
{{--                href="{{ url('/') }}/activestudentlist">Active Student List</a>--}}
{{--                  <a class="--}}
{{--                {{Request::is('passwords')==1 ? 'active' : '' }} item"--}}
{{--                href="{{ url('/') }}/passwords">Master Password</a>--}}
{{--                <a class="--}}
{{--              {{Request::is('addDepartmentAccount')==1 ? 'active' : '' }} item"--}}
{{--              href="{{ url('/') }}/addDepartmentAccount">Department Account</a>--}}
{{--                <a class="--}}
{{--              {{Request::is('seeFeedbacks')==1 ? 'active' : '' }} item"--}}
{{--              href="{{ url('/') }}/seeFeedbacks">See Feedbacks</a>--}}
{{--            </div>--}}
{{--          </div>--}}

          <div class="right menu">
            <a class="item" href="{{ url('/') }}/mailbox">
              <i class="mail outline icon"></i>
            </a>
            <div class="item">
              <a class="ui button" href="{{ url('/') }}/auth/logout">Log Out</a>
            </div>
          </div>
        </div>
      </div>
    <?php
  } else if (strcasecmp($user->role, "student") == 0){
    ?>
    <!-- Student hidden menu -->
      <div class="ui large top fixed hidden menu">
        <div class="ui container">
          @if($accepted == 'no')
        <a <?php if($_SERVER["REQUEST_URI"] == '/vacancy'){echo 'class="active item"';}else{echo 'class="item"';} ?> href="{{ url('/') }}/vacancy" >Vacancy</a>
        <a <?php if($_SERVER["REQUEST_URI"] == '/status'){echo 'class="active item"';}else{echo 'class="item"';} ?> href="{{ url('/') }}/status" >Status</a>
        <a <?php if($_SERVER["REQUEST_URI"] == '/profile'){echo 'class="active item"';}else{echo 'class="item"';} ?> href="{{ url('/') }}/profile" >Profile</a>
        <a <?php if($_SERVER["REQUEST_URI"] == '/feedback'){echo 'class="active item"';}else{echo 'class="item"';} ?> href="{{ url('/') }}/feedback" >Feedback</a>
      @else
        <!--<a <?php if($_SERVER["REQUEST_URI"] == '/learningplan'){echo 'class="active item"';}else{echo 'class="item"';} ?> href="{{ url('/') }}/learningplan">Learning Plan</a>-->
        <a <?php if($_SERVER["REQUEST_URI"] == '/status'){echo 'class="active item"';}else{echo 'class="item"';} ?> href="{{ url('/') }}/status" >Status</a>
        <a <?php if($_SERVER["REQUEST_URI"] == '/profile'){echo 'class="active item"';}else{echo 'class="item"';} ?> href="{{ url('/') }}/profile" >Profile</a>
        <a <?php if($_SERVER["REQUEST_URI"] == '/feedback'){echo 'class="active item"';}else{echo 'class="item"';} ?> href="{{ url('/') }}/feedback" >Feedback</a>
      @endif

        <div class="right menu">
          <div class="item" style="padding-top:0px; padding-bottom: 0px;">
            @if ($mailboxCount>0)
              <span id="mailbox_count">{{ $mailboxCount }}</span>
            @endif
            <a href="{{ url('/') }}/mailbox">
              <i id="mailboxLink" class="middle aligned mail outline large icon"></i>
            </a>
          </div>


          <div class="item" style="padding-top:0px; padding-bottom: 0px;">
            @if($notifCount > 0)<span id="notification_counts">{{ $notifCount }}</span>@endif
            <a><i class="middle aligned info circle large icon" id="notificationLinks" style="cursor:pointer;"></i></a>
            <div id="notificationContainers">
            <div id="notificationTitles">Notifications</div>
            <div id="notificationsBodys" class="notifications ">
              <div class="ui relaxed divided list">
                @foreach ($notif as $item)
                <div class="item" style="padding-bottom: 0%;">
                  <div class="content">
                    <div class="header" style="text-align: center;">{{ $item->subject }}</div>
                    <div class="description" style="text-align: center;">{{ $item->message }}</div>
                  </div>
                </div>
                <hr/>
                @endforeach
              </div>
            </div>
            <div id="notificationFooters"><a href="{{ url('/') }}/notif">See All</a></div>
            </div>
          </div>

          <a class="ui button" href="{{ url('/') }}/auth/logout">Log Out</a>
        </div>
        </div>
      </div>
    <?php
    }
    ?>
    <!-- Sidebar Menu -->
    <?php
      if (strcasecmp($user->role, "Company") == 0){
    ?>
    <!--Company sidebar-->
    <div class="ui vertical inverted sidebar menu left" style="overflow-y: scroll;">
      <a <?php if($_SERVER["REQUEST_URI"] == '/homec'){echo 'class="active item"';}else{echo 'class="item"';} ?> href="{{ url('/') }}/homec">Home</a>
      <a class="item" href="{{ url('/') }}/profilec/{{ $user->userid }}">Profile</a>
      <div class="item">
        <span class="text">Manage</span>
        <i class="dropdown icon"></i>
        <div class="menu">
          <a href="{{ url('/') }}/job"><div class="item">Job</div></a>
          <a href="{{ url('/') }}/supervisor"><div class="item">Supervisor</div></a>
        </div>
      </div>
      <a class="item" href="{{ url('/') }}/feedbacks">Feedback</a>
      <a href="{{ url('/') }}/mailbox" class="item">
        <span class="text">Mailbox</span><i class="mail outline icon"></i>
      </a>
      <div class="item">
        <a href="{{ url('/') }}/notif">Notifications</a><i class="info circle icon"></i>
      </div>
    </div>

    <?php
      } else if (strcasecmp($user->role, "Department") == 0){
    ?>
    <!--Department sidebar-->
    <div class="ui vertical inverted sidebar menu left" style="overflow-y: scroll;">
      <a class="
        {{Request::is('homed')==1 ? 'active' : '' }} item"
        href="{{ url('/') }}/homed">Home</a>

    <!-- <div class="item">
      <span class="text">Others</span>
      <i class="dropdown icon"></i>
      <div class="menu">
        </div>
    </div> -->

      <div class="item">
        <span class="text">Master</span>
        <i class="dropdown icon"></i>
        <div class="menu">
          @if (\App\Department::isAdmin())
          <a class="
          {{Request::is(route('department.faculty'))==1 ? 'active' : '' }}
          item" href="{{ route('department.faculty') }}">Faculty & Degree</a>
          @endif
          
          <a class="
          {{Request::is('manageStudent')==1 ? 'active' : '' }}
          item" href="{{ url('/') }}/manageStudent">Student</a>

          <a class="
          {{Request::is('register')==1 ? 'active' : '' }}
          item" href="{{ url('/') }}/register">Company</a>

          <a class="
          {{Request::is('manageguider')==1 ? 'active' : '' }}
          item" href="{{ url('/') }}/manageguider">University Supervisor</a>

          <a class="
          {{Request::is('manageJob')==1 ? 'active' : '' }}
          item" href="{{ url('/') }}/manageJob">Vacancies</a>
          
          <a class="
          {{Request::is('manageJobPosition')==1 ? 'active' : '' }}
          item" href="{{ url('/') }}/manageJobPosition">Job Position</a>

          <a class="
          {{Request::is('manageDeletedJob')==1 ? 'active' : '' }}
          item" href="{{ url('/') }}/manageDeletedJob">Deleted Job</a>

        </div>
      </div>
      <div class="item">
        <span class="text">Organize</span>
        <i class="dropdown icon"></i>
        <div class="menu">
          <a class="
          {{Request::is('apply')==1 ? 'active' : '' }}
          item" href="{{ url('/') }}/apply">Apply Student to Job</a>

          <a class="
          {{Request::is('accepted')==1 ? 'active' : '' }}
          item" href="{{ url('/') }}/accepted">Student Placement</a>

          <a class="
          {{Request::is('editRecruitment')==1 ? 'active' : '' }}
          item" href="{{ url('/') }}/editRecruitment">Edit Recruitment</a>
          @if(\App\Department::isSuperAdmin())
            <a class="
              {{Request::is('editBipp')==1 ? 'active' : '' }}
              item" href="{{ url('/') }}/editBipp">Edit BIPP</a>
              
            <a class="
            {{Request::is('editDesc')==1 ? 'active' : '' }}
            item" href="{{ url('/') }}/editDesc">Edit Description</a>
          
          <a class="
          {{Request::is('unaccept_request')==1 ? 'active' : '' }}
          item" href="{{ url('/') }}/unaccept_request">Unaccept Status</a>
          @endif
          <a class="
          {{Request::is('unreject_request')==1 ? 'active' : '' }}
          item" href="{{ url('/') }}/unreject_request">Unreject Request</a>

          <a class="
          {{Request::is('approval_request')==1 ? 'active' : '' }}
          item" href="{{ url('/') }}/approval_request">Approval Request</a>

          <a class="
          {{Request::is('assign_supervisor')==1 ? 'active' : '' }}
          item" href="{{ url('/') }}/assign_supervisor">Assign Supervisor</a>
        </div>
      </div>
      <div class="item">
        <span class="text">User Management</span>
        <i class="dropdown icon"></i>
         <div class="menu">
           <a class="
            {{Request::is('accountapproval')==1 ? 'active' : '' }} item"
            href="{{ url('/') }}/accountapproval">Account Approval</a>

           <a class="
            {{Request::is('activestudentlist')==1 ? 'active' : '' }} item"
            href="{{ url('/') }}/activestudentlist">Active Student List</a>  
            <a class="
              {{Request::is('passwords')==1 ? 'active' : '' }} item"
              href="{{ url('/') }}/passwords">Master Password</a>
            <a class="
              {{Request::is('seeFeedbacks')==1 ? 'active' : '' }} item"
              href="{{ url('/') }}/seeFeedbacks">See Feedbacks</a>
            <a class="
              {{Request::is('addDepartmentAccount')==1 ? 'active' : '' }} item"
              href="{{ url('/') }}/addDepartmentAccount">Department Account</a>
        </div>
      </div>

      <a class="item" href="{{ url('/') }}/mailbox">Mailbox
        <i class="mail outline icon"></i>
      </a>
    </div>

    <?php
      } else if (strcasecmp($user->role, "Student") == 0){
    ?>
    <!--Student sidebar-->
    <div class="ui vertical inverted sidebar menu left" style="overflow-y: scroll;">
      @if($accepted == 'no')
        <a <?php if($_SERVER["REQUEST_URI"] == '/vacancy'){echo 'class="active item"';}else{echo 'class="item"';} ?> href="{{ url('/') }}/vacancy" >Vacancy</a>
        <a <?php if($_SERVER["REQUEST_URI"] == '/status'){echo 'class="active item"';}else{echo 'class="item"';} ?> href="{{ url('/') }}/status" >Status</a>
        <a <?php if($_SERVER["REQUEST_URI"] == '/profile'){echo 'class="active item"';}else{echo 'class="item"';} ?> href="{{ url('/') }}/profile" >Profile</a>
        <a <?php if($_SERVER["REQUEST_URI"] == '/feedback'){echo 'class="active item"';}else{echo 'class="item"';} ?> href="{{ url('/') }}/feedback" >Feedback</a>
      @else
        <!--<a <?php if($_SERVER["REQUEST_URI"] == '/learningplan'){echo 'class="active item"';}else{echo 'class="item"';} ?> href="{{ url('/') }}/learningplan">Learning Plan</a>-->
        <a <?php if($_SERVER["REQUEST_URI"] == '/status'){echo 'class="active item"';}else{echo 'class="item"';} ?> href="{{ url('/') }}/status" >Status</a>
        <a <?php if($_SERVER["REQUEST_URI"] == '/profile'){echo 'class="active item"';}else{echo 'class="item"';} ?> href="{{ url('/') }}/profile" >Profile</a>
        <a <?php if($_SERVER["REQUEST_URI"] == '/feedback'){echo 'class="active item"';}else{echo 'class="item"';} ?> href="{{ url('/') }}/feedback" >Feedback</a>
      @endif
      <a href="{{ url('/') }}/mailbox" class="item">
        <span class="text">Mailbox</span><i class="mail outline icon"></i>
      </a>
      <div class="item">
        <a href="{{ url('/') }}/notif">Notifications</a><i class="info circle icon"></i>
      </div>
    </div>
    <?php
    }
    ?>

    <!-- Page Contents -->
    <div class="pusher">
      @yield('content')
    </div>
  </body>
</html>
