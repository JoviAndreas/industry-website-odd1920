<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Standard Meta -->

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

    <!-- Site Properties -->
    <link rel="icon" type="image/png" href="{{ URL::asset('assets/images/favicon.png') }}">
    <title>@yield('title')</title>
    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/semantic/dist/semantic.min.css">
    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/semantic/dist/components/dataTables.semanticui.min.css">
    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/jqueryui/jquery-ui.css">
    <style>
      .ui.error.message:not(:empty) {
        display: block;
      }
    </style>

    <script type="text/javascript" src="{{ url('/') }}/semantic/dist/jquery.js"></script>
    <script type="text/javascript" src="{{ url('/') }}/semantic/dist/Chart.min.js"></script>
    <script type="text/javascript" src="{{ url('/') }}/semantic/dist/semantic.min.js"></script>
    <script type="text/javascript" src="{{ url('/') }}/semantic/dist/components/accordion.js"></script>
    <script type="text/javascript" src="{{ url('/') }}/semantic/dist/components/jquery.dataTables.min.js" ></script>
    <script type="text/javascript" src="{{ url('/') }}/semantic/dist/components/dataTables.semanticui.min.js" ></script>
    <script type="text/javascript" src="{{ url('/') }}/jqueryui/jquery-ui.js" ></script>
    <style type="text/css">
    div.upload {
        width: 157px;
        height: 57px;
        overflow: hidden;
    }


.scrollToTop{
  width:100px;
  height:60px;
  padding:10px;
  text-align:center;
  background: whiteSmoke;
  font-weight: bold;
  color: #444;
  text-decoration: none;
  position:fixed;
  top:560px;
  right:40px;
  display:none;
}
.scrollToTop:hover{
  text-decoration:none;
}


div.upload input {
    display: block !important;
    width: 157px !important;
    height: 57px !important;
    opacity: 0 !important;
    overflow: hidden !important;
}

#companyTable th{ cursor: pointer; }

      .hidden.menu {
        display: none;
      }

      .ui.vertical.stripe {
        padding: 8em 0em;
      }
      .ui.vertical.stripe h3 {
        font-size: 2em;
      }
      .ui.vertical.stripe .button + h3,
      .ui.vertical.stripe p + h3 {
        margin-top: 3em;
      }
      .ui.vertical.stripe .floated.image {
        clear: both;
      }
      .ui.vertical.stripe p {
        font-size: 1.33em;
      }
      .ui.vertical.stripe .horizontal.divider {
        margin: 3em 0em;
      }

      .quote.stripe.segment {
        padding: 0em;
      }
      .quote.stripe.segment .grid .column {
        padding-top: 5em;
        padding-bottom: 5em;
      }

      .footer.segment {
        padding: 5em 0em;
      }

      .secondary.pointing.menu .toc.item {
        display: none;
      }

      .ui.segment{
        padding: 0em;
      }
      .ui.fullscreen.modal {
        height: 90%;
      }

      body > .grid {
        height: 100%;
      }
      .image {
        margin-top: -100px;
      }

      #vacancy_table div.arrow { background:transparent url("{{ URL::asset('assets/images/arrows.png') }}") no-repeat scroll 0px -16px; width:16px; height:16px; display:block;}
      #vacancy_table div.up { background-position:0px 0px;}
      #vacancy_table tr.odd { cursor: pointer; }
      .odd.selectable:hover { background-color: #eeeeee; }
      #statusTable th{ cursor: pointer; }

      @media only screen and (max-width: 700px) {
        .ui.fixed.menu {
          display: none !important;
        }
        .secondary.pointing.menu .item,
        .secondary.pointing.menu .menu {
          display: none;
        }
        .secondary.pointing.menu .toc.item {
          display: block;
        }
      }
    </style>


    <script src="{{ url('/') }}/semantic/dist/components/visibility.js"></script>
    <script src="{{ url('/') }}/semantic/dist/components/sidebar.js"></script>
    <script src="{{ url('/') }}/semantic/dist/components/transition.js"></script>
    <script src="{{ url('/') }}/semantic/dist/components/popup.min.js"></script>


    <script>

          Element.prototype.remove = function()
      {
        this.parentElement.removeChild(this);
      }
      NodeList.prototype.remove = HTMLCollection.prototype.remove = function() {
        for(var i = this.length - 1; i >= 0; i--)
        {
           if(this[i] && this[i].parentElement) {
              this [i].parentElement.removeChild(this[i]);
        }
          }
            }


    $(document)
      .ready(function() {
        $('#companyTable').DataTable();


        $('.showModal').click(function(e) {
            $('.cv.fullscreen.modal').modal('show');
            document.getElementById('descriptionId').style.height = '100%';
            $("html, body").animate({ scrollTop: $(document).height()-$(window).height() });
          });

        $('.ui.tr').popup();
        $('#statusTable1').DataTable();
        $('#statusTable2').DataTable();
        $("#vacancy_table tr.even").hide();
        $("#vacancy_table tr:first-child").show();

        $('.datepickerDea').datepicker({minDate: 0});
        $('.dobStudent').datepicker();


        $('.ui.large.rec.form')
          .form({
            fields: {
              status     : ['empty'],
            }
          })
        ;



            $('.ui.masterpassword.form')
              .form({
                fields: {
                  confirmpassword : ['minLength[6]', 'empty'],
                  newpassword : ['minLength[6]', 'empty'],
                }
              })
            ;






        $('.insertJob').click(function(e) {
          $('.ui.modal.insertJob').modal('show');
        });
        

        $('.ui.large.insert.form')
          .form({
            fields: {
              txtPosition     : ['empty'],
              txtQuantity   : ['empty', 'regExp[/^[1-9][0-9]*$/]'],
              txtLocation : 'empty',
              txtSupervisor : 'empty',
              txtSupervisorContact: 'empty',
              txtDeadline : 'empty',
              txtDuration : 'empty',
              txtJobDescription    : 'empty',
              txtAllowance: 'empty'
            }
          })
        ;

        $('.ui.large.update.form')
          .form({
            fields: {
              txtPosition     : ['empty'],
              txtQuantity   : ['empty', 'regExp[/^[1-9][0-9]*$/]'],
              txtLocation : 'empty',
              txtSupervisor : 'empty',
              txtSupervisorContact: 'empty',
              txtDeadline : 'empty',
              txtDuration : 'empty',
              txtJobDescription    : 'empty',
              txtAllowance: 'empty'
            }
          })
        ;


        $("#vacancy_table tr.odd").click(function(){
            $(this).next(".even").toggle();

            $(this).find(".caret").toggleClass("up");
        });
        $('.ui.checkbox').checkbox();

        var scntDiv = $('.regComp');

          $('.plusIcon').click(function(){
            $(this).transition('pulse');
            $('<div class="field"><div class="ui left icon input"><input type="text" name="txtRegComp" placeholder="Company Name"></div></div>').appendTo(scntDiv);
          });


        // fix menu when passed
        $('.invis')
          .visibility({
            once: false,
            onBottomPassed: function() {
              $('.fixed.menu').transition('fade in');
            },
            onBottomPassedReverse: function() {
              $('.fixed.menu').transition('fade out');
            }
          })
        ;

        $('.ui.dropdown').dropdown({ fullTextSearch: true });;

        // create sidebar and attach to menu open
        $('.ui.sidebar')
          .sidebar('attach events', '.toc.item')
        ;

        $('.ui.form')
          .form({
            fields: {
              userid: {
                identifier  : 'userid',
                rules: [
                  {
                    type   : 'empty',
                    prompt : 'Please enter your username'
                  }
                ]
              },
              password: {
                identifier  : 'password',
                rules: [
                  {
                    type   : 'empty',
                    prompt : 'Please enter your password'
                  },
                  {
                    type   : 'length[6]',
                    prompt : 'Your password must be at least 6 characters'
                  }
                ]
              }
            }
          })
        ;

  /*      $('.showJobModal').click(function(e) {
          $('.job.modal').modal('show');
        });*/

        $('.showJobModal').click(function(e) {
            $('.job.modal').modal('show');
            //document.getElementById('descriptionId').style.height = '100%';
            $("html, body").animate({ scrollTop: $(document).height()-$(window).height() });
          });

          $('.showStudentModal').click(function(e) {
            $('.student.modal').modal('show');
            $("html, body").animate({ scrollTop: $(document).height()-$(window).height() });
          });



        $('.showModal').click(function(e) {
          $('.small.modal').modal('show');
        });


          $('.message .close')
            .on('click', function() {
              $(this)
                .closest('.message')
                .transition('fade')
              ;
            })
          ;


      })
    ;
    </script>
  </head>
  <body class="pushable">

    <!-- Following Menu -->
    <div class="ui large top hidden fixed menu">
      <div class="ui container">
       <a class="
        {{Request::is('homed')==1 ? 'active' : '' }} item"
        href="{{ url('/') }}/homed">Home</a>

        @include('partial.navigation', ['localNavigationType' => 'department'])

{{--        <div class="ui pointing dropdown link item">--}}
{{--          <span class="text">Master</span>--}}
{{--          <i class="dropdown icon"></i>--}}
{{--          <div class="ui menu">--}}
{{--            --}}
{{--            @if (\App\Department::isAdmin())--}}
{{--            <a class="--}}
{{--            {{Request::is(route('department.faculty'))==1 ? 'active' : '' }}--}}
{{--            item" href="{{ route('department.faculty') }}">Faculty & Degree</a>--}}
{{--            @endif--}}

{{--            <a class="--}}
{{--            {{Request::is('manageStudent')==1 ? 'active' : '' }}--}}
{{--            item" href="{{ url('/') }}/manageStudent">Student</a>--}}

{{--            <a class="--}}
{{--            {{Request::is('register')==1 ? 'active' : '' }}--}}
{{--            item" href="{{ url('/') }}/register">Company</a>--}}

{{--            <a class="--}}
{{--            {{Request::is('manageguider')==1 ? 'active' : '' }}--}}
{{--            item" href="{{ url('/') }}/manageguider">University Supervisor</a>--}}
{{--            --}}
{{--            <a class="--}}
{{--            {{Request::is('manageJob')==1 ? 'active' : '' }}--}}
{{--            item" href="{{ url('/') }}/manageJob">Vacancies</a>--}}

{{--            <a class="--}}
{{--            {{Request::is('manageJobPosition')==1 ? 'active' : '' }}--}}
{{--            item" href="{{ url('/') }}/manageJobPosition">Job Position</a>--}}

{{--            <a class="--}}
{{--            {{Request::is('manageDeletedJob')==1 ? 'active' : '' }}--}}
{{--            item" href="{{ url('/') }}/manageDeletedJob">Deleted Job</a>--}}

{{--          </div>--}}
{{--        </div>--}}
{{--        <div class="ui pointing dropdown link item">--}}
{{--          <span class="text">Organize</span>--}}
{{--          <i class="dropdown icon"></i>--}}
{{--          <div class="ui menu">--}}
{{--            <a class="--}}
{{--            {{Request::is('apply')==1 ? 'active' : '' }}--}}
{{--            item" href="{{ url('/') }}/apply">Apply Student to Job</a>--}}

{{--            <a class="--}}
{{--            {{Request::is('accepted')==1 ? 'active' : '' }}--}}
{{--            item" href="{{ url('/') }}/accepted">Student Placement</a>--}}

{{--            <a class="--}}
{{--            {{Request::is('editRecruitment')==1 ? 'active' : '' }}--}}
{{--            item" href="{{ url('/') }}/editRecruitment">Edit Recruitment</a>--}}
{{--            @if(\App\Department::isSuperAdmin())--}}
{{--            <a class="--}}
{{--              {{Request::is('editBipp')==1 ? 'active' : '' }}--}}
{{--              item" href="{{ url('/') }}/editBipp">Edit BIPP</a>--}}
{{--              --}}
{{--            <a class="--}}
{{--            {{Request::is('editDesc')==1 ? 'active' : '' }}--}}
{{--            item" href="{{ url('/') }}/editDesc">Edit Description</a>--}}
{{--            --}}
{{--            <a class="--}}
{{--            {{Request::is('unaccept_request')==1 ? 'active' : '' }}--}}
{{--            item" href="{{ url('/') }}/unaccept_request">Unaccept Status</a>--}}
{{--            @endif--}}
{{--            <a class="--}}
{{--            {{Request::is('unreject_request')==1 ? 'active' : '' }}--}}
{{--            item" href="{{ url('/') }}/unreject_request">Unreject Request</a>--}}

{{--            <a class="--}}
{{--            {{Request::is('approval_request')==1 ? 'active' : '' }}--}}
{{--            item" href="{{ url('/') }}/approval_request">Approval Request</a>--}}

{{--            <a class="--}}
{{--            {{Request::is('assign_supervisor')==1 ? 'active' : '' }}--}}
{{--            item" href="{{ url('/') }}/assign_supervisor">Assign Supervisor</a>--}}
{{--          </div>--}}
{{--        </div>--}}

{{--        <div class="ui pointing dropdown link item">--}}
{{--          <span class="text">User Management</span>--}}
{{--          <i class="dropdown icon"></i>--}}
{{--           <div class="ui menu">--}}
{{--             <a class="--}}
{{--              {{Request::is('accountapproval')==1 ? 'active' : '' }} item"--}}
{{--              href="{{ url('/') }}/accountapproval">Account Approval</a>--}}

{{--               <a class="--}}
{{--              {{Request::is('activestudentlist')==1 ? 'active' : '' }} item"--}}
{{--              href="{{ url('/') }}/activestudentlist">Active Student List</a>--}}
{{--              @if (\App\Department::isSuperAdmin())--}}
{{--                <a class="--}}
{{--                  {{Request::is('passwords')==1 ? 'active' : '' }} item"--}}
{{--                  href="{{ url('/') }}/passwords">Master Password</a>--}}
{{--                @endif--}}
{{--                --}}
{{--                --}}{{-- @if (\App\Department::isSuperAdmin())--}}
{{--                <a class="item" href="{{ url('facultyPasswords') }}">Faculty Password</a>--}}
{{--                @endif --}}
{{--                --}}
{{--                <a class="--}}
{{--                {{Request::is('addDepartmentAccount')==1 ? 'active' : '' }} item"--}}
{{--                href="{{ url('/') }}/addDepartmentAccount">Department Account</a>--}}

{{--                <a class="--}}
{{--                {{Request::is('seeFeedbacks')==1 ? 'active' : '' }} item"--}}
{{--                href="{{ url('/') }}/seeFeedbacks">See Feedbacks</a>--}}

{{--          </div>--}}
{{--        </div>--}}

        <div class="right menu">
          <a class="item" href="{{ url('/') }}/mailbox">
            <i class="mail outline icon"></i>
          </a>
          @if (Auth::check())
          <div class="item">
            <div class="content">
              <p>{{ Auth::user()->userid }}</p>
            </div>
          </div>
          @endif
          <div class="item">
            <a class="ui button" href="{{ url('/') }}/auth/logout">Log Out</a>
          </div>
        </div>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <div class="ui vertical inverted sidebar menu left" style="overflow-y: scroll;">
      <a class="
        {{Request::is('homed')==1 ? 'active' : '' }} item"
        href="{{ url('/') }}/homed">Home</a>

      @include('partial.navigation', ['localNavigationType' => 'department', 'isLocalNavigationSidebar' => 'true'])


    <!-- <div class="item">
          <span class="text">Others</span>
          <i class="dropdown icon"></i>
          <div class="menu">
              
          </div>
      </div> -->

{{--      <div class="item">--}}
{{--        <span class="text">Master</span>--}}
{{--        <i class="dropdown icon"></i>--}}
{{--        <div class="menu">--}}
{{--          --}}
{{--          @if (\App\Department::isAdmin())--}}
{{--          <a class="--}}
{{--          {{Request::is(route('department.faculty'))==1 ? 'active' : '' }}--}}
{{--          item" href="{{ route('department.faculty') }}">Faculty & Degree</a>--}}
{{--          @endif--}}

{{--          <a class="--}}
{{--          {{Request::is('manageStudent')==1 ? 'active' : '' }}--}}
{{--          item" href="{{ url('/') }}/manageStudent">Student</a>--}}

{{--          <a class="--}}
{{--          {{Request::is('register')==1 ? 'active' : '' }}--}}
{{--          item" href="{{ url('/') }}/register">Company</a>--}}

{{--          <a class="--}}
{{--          {{Request::is('manageguider')==1 ? 'active' : '' }}--}}
{{--          item" href="{{ url('/') }}/manageguider">University Supervisor</a>--}}
{{--          --}}
{{--          <a class="--}}
{{--          {{Request::is('manageJob')==1 ? 'active' : '' }}--}}
{{--          item" href="{{ url('/') }}/manageJob">Vacancies</a>--}}
{{--            --}}
{{--          <a class="--}}
{{--          {{Request::is('manageJobPosition')==1 ? 'active' : '' }}--}}
{{--          item" href="{{ url('/') }}/manageJobPosition">Job Position</a>--}}

{{--          <a class="--}}
{{--          {{Request::is('manageDeletedJob')==1 ? 'active' : '' }}--}}
{{--          item" href="{{ url('/') }}/manageDeletedJob">Deleted Job</a>--}}

{{--        </div>--}}
{{--      </div>--}}
{{--      <div class="item">--}}
{{--        <span class="text">Organize</span>--}}
{{--        <i class="dropdown icon"></i>--}}
{{--        <div class="menu">--}}
{{--          <a class="--}}
{{--          {{Request::is('apply')==1 ? 'active' : '' }}--}}
{{--          item" href="{{ url('/') }}/apply">Apply Student to Job</a>--}}

{{--          <a class="--}}
{{--          {{Request::is('accepted')==1 ? 'active' : '' }}--}}
{{--          item" href="{{ url('/') }}/accepted">Student Placement</a>--}}

{{--          <a class="--}}
{{--          {{Request::is('editRecruitment')==1 ? 'active' : '' }}--}}
{{--          item" href="{{ url('/') }}/editRecruitment">Edit Recruitment</a>--}}
{{--          @if(\App\Department::isSuperAdmin())--}}
{{--            <a class="--}}
{{--              {{Request::is('editBipp')==1 ? 'active' : '' }}--}}
{{--              item" href="{{ url('/') }}/editBipp">Edit BIPP</a>--}}
{{--              --}}
{{--            <a class="--}}
{{--            {{Request::is('editDesc')==1 ? 'active' : '' }}--}}
{{--            item" href="{{ url('/') }}/editDesc">Edit Description</a>--}}
{{--      --}}
{{--            <a class="--}}
{{--            {{Request::is('unaccept_request')==1 ? 'active' : '' }}--}}
{{--            item" href="{{ url('/') }}/unaccept_request">Unaccept Status</a>--}}
{{--            @endif--}}
{{--          <a class="--}}
{{--          {{Request::is('unreject_request')==1 ? 'active' : '' }}--}}
{{--          item" href="{{ url('/') }}/unreject_request">Unreject Request</a>--}}

{{--          <a class="--}}
{{--          {{Request::is('approval_request')==1 ? 'active' : '' }}--}}
{{--          item" href="{{ url('/') }}/approval_request">Approval Request</a>--}}

{{--          <a class="--}}
{{--          {{Request::is('assign_supervisor')==1 ? 'active' : '' }}--}}
{{--          item" href="{{ url('/') }}/assign_supervisor">Assign Supervisor</a>--}}
{{--        </div>--}}
{{--      </div>--}}
{{--      <div class="item">--}}
{{--        <span class="text">User Management</span>--}}
{{--        <i class="dropdown icon"></i>--}}
{{--         <div class="menu">--}}
{{--           <a class="--}}
{{--            {{Request::is('accountapproval')==1 ? 'active' : '' }} item"--}}
{{--            href="{{ url('/') }}/accountapproval">Account Approval</a>--}}

{{--           <a class="--}}
{{--            {{Request::is('activestudentlist')==1 ? 'active' : '' }} item"--}}
{{--            href="{{ url('/') }}/activestudentlist">Active Student List</a>--}}
{{--            @if (\App\Department::isSuperAdmin())--}}
{{--              <a class="--}}
{{--                {{Request::is('passwords')==1 ? 'active' : '' }} item"--}}
{{--                href="{{ url('/') }}/passwords">Master Password</a>--}}
{{--              @endif--}}
{{--              --}}
{{--              --}}{{-- @if (\App\Department::isSuperAdmin())--}}
{{--              <a class="item" href="{{ url('facultyPasswords') }}">Faculty Password</a>--}}
{{--              @endif --}}
{{--              --}}
{{--              <a class="--}}
{{--              {{Request::is('addDepartmentAccount')==1 ? 'active' : '' }} item"--}}
{{--              href="{{ url('/') }}/addDepartmentAccount">Department Account</a>--}}

{{--              <a class="--}}
{{--                  {{Request::is('seeFeedbacks')==1 ? 'active' : '' }} item"--}}
{{--                  href="{{ url('/') }}/seeFeedbacks">See Feedbacks</a>--}}
{{--              --}}
{{--        </div>--}}
{{--      </div>--}}

      <a class="item" href="{{ url('/') }}/mailbox">Mailbox
        <i class="mail outline icon"></i>
      </a>
    </div>

    <!-- Page Contents -->
    <div class="pusher">
      @yield('content')
    </div>
  </body>
</html>
