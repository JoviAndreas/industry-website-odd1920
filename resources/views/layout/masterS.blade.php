<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Standard Meta -->

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

    <!-- Site Properties -->
    <link rel="icon" type="image/png" href="{{ URL::asset('assets/images/favicon.png') }}">
    <title>@yield('title')</title>
    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/semantic/dist/semantic.min.css">
    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/semantic/dist/semantics.min.css">
    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/semantic/dist/components/dataTables.semanticui.min.css">
    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/jqueryui/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/jquery-timepicker-1.3.5/jquery.timepicker.min.css">

    <script type="text/javascript" src="{{ url('/') }}/semantic/dist/jquery.js"></script>
    <script type="text/javascript" src="{{ url('/') }}/semantic/dist/semantic.min.js"></script>
    <script type="text/javascript" src="{{ url('/') }}/semantic/dist/semantics.min.js"></script>
    <script type="text/javascript" src="{{ url('/') }}/semantic/dist/components/accordion.js"></script>
    <script type="text/javascript" src="{{ url('/') }}/semantic/dist/components/jquery.dataTables.min.js" ></script>
    <script type="text/javascript" src="{{ url('/') }}/semantic/dist/components/dataTables.semanticui.min.js" ></script>
    <script type="text/javascript" src="{{ url('/') }}/jqueryui/jquery-ui.js" ></script>
    <script type="text/javascript" src="{{ url('/') }}/jquery-timepicker-1.3.5/jquery.timepicker.min.js" ></script>

    <style type="text/css">


      #vacancy_table div.arrow { background:transparent url("{{ URL::asset('assets/images/arrows.png') }}") no-repeat scroll 0px -16px; width:16px; height:16px; display:block;}
      #vacancy_table div.up { background-position:0px 0px;}
      #vacancy_table tr.odd { cursor: pointer; }

      #statusTable th{ cursor: pointer; }

      .link_underline:hover{
        text-decoration: underline;
      }

      .hidden.menu {
        display: none;
      }

      .ui.vertical.stripe {
        padding: 8em 0em;
      }
      .ui.vertical.stripe h3 {
        font-size: 2em;
      }
      .ui.vertical.stripe .button + h3,
      .ui.vertical.stripe p + h3 {
        margin-top: 3em;
      }
      .ui.vertical.stripe .floated.image {
        clear: both;
      }
      .ui.vertical.stripe p {
        font-size: 1.33em;
      }
      .ui.vertical.stripe .horizontal.divider {
        margin: 3em 0em;
      }

      .quote.stripe.segment {
        padding: 0em;
      }
      .quote.stripe.segment .grid .column {
        padding-top: 5em;
        padding-bottom: 5em;
      }

      .footer.segment {
        padding: 5em 0em;
      }

      .secondary.pointing.menu .toc.item {
        display: none;
      }

      .ui.segment{
        padding: 0em;
      }

      .ui.fullscreen.modal{
        height: 90%;
      }

      @media only screen and (max-width: 700px) {
        .ui.fixed.menu {
          display: none !important;
        }
        .secondary.pointing.menu .item,
        .secondary.pointing.menu .menu {
          display: none;
        }
        .secondary.pointing.menu .toc.item {
          display: block;
        }
      }

      #notification_li
      {
        position:relative
      }

      #notificationContainer
      {
        background-color: #fff;
        border: 1px solid rgba(100, 100, 100, .4);
        -webkit-box-shadow: 0 3px 8px rgba(0, 0, 0, .25);
        overflow: visible;
        position: absolute;
        top: 30px;
        margin-left: -170px;
        width: 400px;
        z-index: 100;
        display: none; // Enable this after jquery implementation
      }

      // Popup Arrow
      #notificationContainer:before {
        content: '';
        display: block;
        position: absolute;
        width: 0;
        height: 0;
        color: transparent;
        border: 10px solid black;
        border-color: transparent transparent white;
        margin-top: -20px;
        margin-left: 188px;
      }

      #notificationTitle
      {
        font-weight: bold;
        padding: 8px;
        font-size: 13px;
        background-color: #ffffff;
        position: fixed;
        z-index: 1000;
        width: 384px;
        border-bottom: 1px solid #dddddd;
        color: #000000;
      }

      #notificationsBody
      {
        padding: 33px 0px 0px 0px !important;
        min-height:300px;
      }

      #notificationFooter
      {
        background-color: #e9eaed;
        text-align: center;
        font-weight: bold;
        padding: 8px;
        font-size: 12px;
        border-top: 1px solid #dddddd;
      }

      #notification_count
      {
        padding: 3px 7px 3px 7px;
        background: #cc0000;
        color: #ffffff;
        font-weight: bold;
        margin-left: 35%;
        border-radius: 9px;
        -moz-border-radius: 9px;
        -webkit-border-radius: 9px;
        position: absolute;
        margin-top: -25%;
        font-size: 11px;
      }
      #mailbox_count
      {
        padding: 3px 7px 3px 7px;
        background: #cc0000;
        color: #ffffff;
        font-weight: bold;
        margin-left: 35%;
        border-radius: 9px;
        -moz-border-radius: 9px;
        -webkit-border-radius: 9px;
        position: absolute;
        margin-top: -25%;
        font-size: 11px;
      }

      #notificationLink:hover{color:#00aaff;text-decoration:none;margin-right:}

      #mailboxLink:hover{color:#00aaff;text-decoration:none}

      #notification_lis
      {
        position:relative
      }

      #notificationContainers
      {
        background-color: #fff;
        border: 1px solid rgba(100, 100, 100, .4);
        -webkit-box-shadow: 0 3px 8px rgba(0, 0, 0, .25);
        overflow: visible;
        position: absolute;
        top: 30px;
        margin-left: -170px;
        width: 400px;
        z-index: 100;
        display: none; // Enable this after jquery implementation
      }

      // Popup Arrow
      #notificationContainers:before {
        content: '';
        display: block;
        position: absolute;
        width: 0;
        height: 0;
        color: transparent;
        border: 10px solid black;
        border-color: transparent transparent white;
        margin-top: -20px;
        margin-left: 188px;
      }

      #notificationTitles
      {
        font-weight: bold;
        padding: 8px;
        font-size: 13px;
        background-color: #ffffff;
        position: fixed;
        z-index: 1000;
        width: 384px;
        border-bottom: 1px solid #dddddd;
        color: #000000;
      }

      #notificationsBodys
      {
        padding: 33px 0px 0px 0px !important;
        min-height:300px;
      }

      #notificationFooters
      {
        background-color: #e9eaed;
        text-align: center;
        font-weight: bold;
        padding: 8px;
        font-size: 12px;
        border-top: 1px solid #dddddd;
      }

      #notification_counts
      {
        padding: 3px 7px 3px 7px;
        background: #cc0000;
        color: #ffffff;
        font-weight: bold;
        margin-left: 35%;
        border-radius: 9px;
        -moz-border-radius: 9px;
        -webkit-border-radius: 9px;
        position: absolute;
        margin-top: -25%;
        font-size: 11px;
      }

      #mailbox_counts
      {
        padding: 3px 7px 3px 7px;
        background: #cc0000;
        color: #ffffff;
        font-weight: bold;
        margin-left: 35%;
        border-radius: 9px;
        -moz-border-radius: 9px;
        -webkit-border-radius: 9px;
        position: absolute;
        margin-top: -25%;
        font-size: 11px;
      }

      #notificationLinks:hover{color:#00aaff;text-decoration:none;margin-right:}

      #mailboxLinks:hover{color:#00aaff;text-decoration:none}
    </style>

    <script src="{{ url('/') }}/semantic/dist/components/visibility.js"></script>
    <script src="{{ url('/') }}/semantic/dist/components/sidebar.js"></script>
    <script src="{{ url('/') }}/semantic/dist/components/transition.js"></script>


    <script>

      Element.prototype.remove = function()
      {
        this.parentElement.removeChild(this);
      }
      NodeList.prototype.remove = HTMLCollection.prototype.remove = function() {
        for(var i = this.length - 1; i >= 0; i--)
        {
           if(this[i] && this[i].parentElement) {
              this [i].parentElement.removeChild(this[i]);
        }
          }
            }




    $(document)
      .ready(function() {
        // fix menu when passed
        $("#notificationLink").click(function(){
          $("#notificationContainer").fadeToggle(300);
          $("#notification_count").fadeOut("slow");
          $.get("{{ url('/') }}/readNotif",function(data,status) {},'html');
          return false;
        });

        //Document Click hiding the popup
        $(document).click(function(){
          $("#notificationContainer").hide();
        });

        //Document Click hiding the popup
        $(document).click(function(){
          $("#notificationContainers").hide();
        });

        $("#notificationLinks").click(function(){
          $("#notificationContainers").fadeToggle(300);
          $("#notification_counts").fadeOut("slow");
          $.get("{{ url('/') }}/readNotif",function(data,status) {},'html');
          return false;
        });


        $('.ui.basic.modal.subject')
              .modal('show') ;

        $('.interviewButton').popup();
        $('.ui.primary.button.learning.plan').popup();


        $('#statusTable').DataTable();

        $('.ui.dropdown')
          .dropdown()
        ;

        $('.message .close')
            .on('click', function() {
              $(this)
                .closest('.message')
                .transition('fade')
              ;
            })
          ;

        $('.invis')
          .visibility({
            once: false,
            onBottomPassed: function() {
              $('.fixed.menu').transition('fade in');
            },
            onBottomPassedReverse: function() {
              $('.fixed.menu').transition('fade out');
            }
          })
        ;

        // create sidebar and attach to menu open
        $('.ui.sidebar')
          .sidebar('attach events', '.toc.item')
        ;

        $("#vacancy_table tr.even").hide();
        $("#vacancy_table tr:first-child").show();

        $("#vacancy_table tr.odd").click(function(){
            $(this).next(".even").toggle();
            $(this).find("#arrow").toggleClass("up");
        });
        
            $('.ui.large.feedback.form')
            .form({
              fields: {
                txtfeed   : ['empty', 'maxLength[500]']
              }
            });


      $("#profpic").change(function() {
      var val = $(this).val();
      switch(val.substring(val.lastIndexOf('.') + 1).toLowerCase()){
          case 'jpg': case 'png': case 'jpeg':
              break;
          default:
              $(this).val('');
              // error message here
              alert("You must insert an image File!");
              break;
      }
  });


      $("#fileexcel").change(function() {
      var val = $(this).val();
      switch(val.substring(val.lastIndexOf('.') + 1).toLowerCase()){
          case 'xlsx': case 'xls':
              break;
          default:
              $(this).val('');
              // error message here
              alert("You must insert an template excel File!");
              break;
      }
  });




      });







    </script>
  </head>
  <body class="pushable">

    <!-- Following Menu -->
    <div class="ui large top fixed hidden menu">
      <div class="ui container">


      @if($accepted == 'no')
        <a <?php if($_SERVER["REQUEST_URI"] == '/vacancy'){echo 'class="active item"';}else{echo 'class="item"';} ?> href="{{ url('/') }}/vacancy" >Vacancy</a>
        <a <?php if($_SERVER["REQUEST_URI"] == '/status'){echo 'class="active item"';}else{echo 'class="item"';} ?> href="{{ url('/') }}/status" >Status</a>
        <a <?php if($_SERVER["REQUEST_URI"] == '/profile'){echo 'class="active item"';}else{echo 'class="item"';} ?> href="{{ url('/') }}/profile" >Profile</a>
        <a <?php if($_SERVER["REQUEST_URI"] == '/feedback'){echo 'class="active item"';}else{echo 'class="item"';} ?> href="{{ url('/') }}/feedback" >Feedback</a>
      @else
        <!--<a <?php if($_SERVER["REQUEST_URI"] == '/learningplan'){echo 'class="active item"';}else{echo 'class="item"';} ?> href="{{ url('/') }}/learningplan">Learning Plan</a>-->
        <a <?php if($_SERVER["REQUEST_URI"] == '/status'){echo 'class="active item"';}else{echo 'class="item"';} ?> href="{{ url('/') }}/status" >Status</a>
        <a <?php if($_SERVER["REQUEST_URI"] == '/profile'){echo 'class="active item"';}else{echo 'class="item"';} ?> href="{{ url('/') }}/profile" >Profile</a>
        <a <?php if($_SERVER["REQUEST_URI"] == '/feedback'){echo 'class="active item"';}else{echo 'class="item"';} ?> href="{{ url('/') }}/feedback" >Feedback</a>
      @endif

        <div class="right menu">
          <div class="item" style="padding-top:0px; padding-bottom: 0px;">
            @if ($mailboxCount>0)
              <span id="mailbox_count">{{ $mailboxCount }}</span>
            @endif
            <a href="{{ url('/') }}/mailbox">
              <i id="mailboxLink" class="middle aligned mail outline large icon"></i>
            </a>
          </div>


          <div class="item" style="padding-top:0px; padding-bottom: 0px;">
            @if($notifCount > 0)<span id="notification_counts">{{ $notifCount }}</span>@endif
            <a><i class="middle aligned info circle large icon" id="notificationLinks" style="cursor:pointer;"></i></a>
            <div id="notificationContainers">
            <div id="notificationTitles">Notifications</div>
            <div id="notificationsBodys" class="notifications ">
              <div class="ui relaxed divided list">
                @foreach ($notif as $item)
                <div class="item" style="padding-bottom: 0%;">
                  <div class="content">
                    <div class="header" style="text-align: center;">{{ $item->subject }}</div>
                    <div class="description" style="text-align: center;">{{ $item->message }}</div>
                  </div>
                </div>
                <hr/>
                @endforeach
              </div>
            </div>
            <div id="notificationFooters"><a href="{{ url('/') }}/notif">See All</a></div>
            </div>
          </div>

          <a class="ui button" href="{{ url('/') }}/auth/logout">Log Out</a>
        </div>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <div class="ui vertical inverted sidebar menu left" style="overflow-y: scroll;">
      @if($accepted == 'no')
        <a <?php if($_SERVER["REQUEST_URI"] == '/vacancy'){echo 'class="active item"';}else{echo 'class="item"';} ?> href="{{ url('/') }}/vacancy" >Vacancy</a>
        <a <?php if($_SERVER["REQUEST_URI"] == '/status'){echo 'class="active item"';}else{echo 'class="item"';} ?> href="{{ url('/') }}/status" >Status</a>
        <a <?php if($_SERVER["REQUEST_URI"] == '/profile'){echo 'class="active item"';}else{echo 'class="item"';} ?> href="{{ url('/') }}/profile" >Profile</a>
        <a <?php if($_SERVER["REQUEST_URI"] == '/feedback'){echo 'class="active item"';}else{echo 'class="item"';} ?> href="{{ url('/') }}/feedback" >Feedback</a>
      @else
        <!--<a <?php if($_SERVER["REQUEST_URI"] == '/learningplan'){echo 'class="active item"';}else{echo 'class="item"';} ?> href="{{ url('/') }}/learningplan">Learning Plan</a>-->
        <a <?php if($_SERVER["REQUEST_URI"] == '/status'){echo 'class="active item"';}else{echo 'class="item"';} ?> href="{{ url('/') }}/status" >Status</a>
        <a <?php if($_SERVER["REQUEST_URI"] == '/profile'){echo 'class="active item"';}else{echo 'class="item"';} ?> href="{{ url('/') }}/profile" >Profile</a>
        <a <?php if($_SERVER["REQUEST_URI"] == '/feedback'){echo 'class="active item"';}else{echo 'class="item"';} ?> href="{{ url('/') }}/feedback" >Feedback</a>
      @endif
      <a href="{{ url('/') }}/mailbox" class="item">
        <span class="text">Mailbox</span><i class="mail outline icon"></i>
      </a>
      <div class="item">
        <a href="{{ url('/') }}/notif">Notifications</a><i class="info circle icon"></i>
      </div>
    </div>


    <!-- Page Contents -->
    <div class="pusher">
      @yield('content')
    </div>
  </body>
</html>
