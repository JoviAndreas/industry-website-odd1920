  <div class="ui inverted vertical center aligned segment invis">
    <div class="ui container">
     
      <!-- Student menu -->
        <div class="ui large secondary inverted pointing menu">
            <a class="toc item">
              <i class="sidebar icon"></i>
            </a>
    
    @if($accepted == 'no')
            <a class="
            {{Request::is('vacancy')==1 ? 'active' : '' }} item" 
            href="{{ url('/') }}/vacancy">Vacancy</a>

            <a class="
            {{Request::is('status')==1 ? 'active' : '' }} item"
             href="{{ url('/') }}/status">Status</a>

            <a class="
            {{Request::is('profile')==1 || Request::is('updateprofile')==1 ? 'active' : '' }} item"
            href="{{ url('/') }}/profile">Profile</a>

            <a class="
             {{Request::is('feedback')==1 ? 'active' : '' }} item"
            href="{{ url('/') }}/feedback">Feedback</a>
    @elseif($accepted == 'company')
            @include('headerCompany')
    @else
            <a class="
            {{Request::is('status')==1 ? 'active' : '' }} item"
             href="{{ url('/') }}/status">Status</a>

            <a class="
            {{Request::is('profile')==1 || Request::is('updateprofile')==1 ? 'active' : '' }} item"
            href="{{ url('/') }}/profile">Profile</a>

            <!--
            <a class="
            {{Request::is('learningplan')==1  ? 'active' : '' }} item"
            href="{{ url('/') }}/learningplan">Learning Plan</a>
            -->

            <a class="
             {{Request::is('feedback')==1 ? 'active' : '' }} item"
            href="{{ url('/') }}/feedback">Feedback</a>
    @endif


            <div class="right item">
              <div class="item" style="padding-top:0px; padding-bottom: 0px;">
                @if ($mailboxCount>0)
                  <span id="mailbox_count">{{ $mailboxCount }}</span>
                @endif
                <a href="{{ url('/') }}/mailbox">
                  <i id="mailboxLink" class="middle aligned mail outline large icon"></i>
                </a>
              </div>
              
             
              <div class="item" style="padding-top:0px; padding-bottom: 0px;">
                @if($notifCount > 0)<span id="notification_count">{{ $notifCount }}</span>@endif
                <a><i class="middle aligned info circle large icon" id="notificationLink" style="cursor:pointer;"></i></a>
                <div id="notificationContainer">
                <div id="notificationTitle">Notifications</div>
                <div id="notificationsBody" class="notifications ">
                  <div class="ui relaxed divided list">
                    @foreach($notif as $item)
                    <div class="item">
                      <div class="content">
                        <div class="header">{{ $item->subject }}</div>
                        <div class="description">{{ $item->message }}</div>
                      </div>
                    </div>
                    @endforeach
                  </div>
                </div>
                <div id="notificationFooter"><a href="{{ url('/') }}/notif">See All</a></div>
                </div>
              </div>

              <a class="ui inverted button" href="{{ url('/') }}/auth/logout">Log Out</a>
            </div>
          </div>
    </div>
  </div>