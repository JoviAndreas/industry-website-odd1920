 <div class="ui inverted vertical center aligned segment invis">
    <div class="ui container">
      <div class="ui large secondary inverted pointing menu">
        <a class="toc item">
          <i class="sidebar icon"></i>
        </a>
        <a class="
        {{Request::is('homec')==1 ? 'active' : '' }} item"
        href="{{ url('/') }}/homec">Home</a>

        <a class="
       {{Request::is('profilec/'.Auth::user()->userid)==1 || Request::is('updateprofiles')==1  ? 'active' : '' }} item"
        href="{{ url('/') }}/profilec/{{ Auth::user()->userid }}">Profile</a>

        @if ($state == 'closed')
          <a href="{{ url('/') }}/supervisor" class="
              {{Request::is('supervisor')==1 ? 'active' : '' }} item"
              >Supervisor</a>
        @else
          <div class="ui pointing dropdown link item">
            <span class="text">Manage</span>
            <i class="dropdown icon"></i>
            <div class="ui menu">

              <a href="{{ url('/') }}/job" class="
            {{Request::is('job')==1 ? 'active' : '' }} item"
              >Job</a>

              <a href="{{ url('/') }}/supervisor" class="
              {{Request::is('supervisor')==1 ? 'active' : '' }} item"
              >Supervisor</a>
            </div>
          </div>
        @endif
        
        <a class="item 
        {{Request::is('company/report')==1 ? 'active' : '' }} item"" href="{{ url('/company/report') }}">Report</a>
        
        <a class="
       {{Request::is('feedbacks')==1 ? 'active' : '' }} item"
        href="{{ url('/') }}/feedbacks">Feedback</a>

        <div class="right item">
          <div class="item" style="padding-top:0px; padding-bottom: 0px;">
            @if ($mailboxCount>0)
              <span id="mailbox_count">{{ $mailboxCount }}</span>
            @endif
            <a href="{{ url('/') }}/mailbox">
              <i id="mailboxLink" class="middle aligned mail outline large icon"></i>
            </a>
          </div>
          
         
          <div class="item" style="padding-top:0px; padding-bottom: 0px;">
            @if($notifCount > 0)<span id="notification_count">{{ $notifCount }}</span>@endif
            <a><i class="middle aligned info circle large icon" id="notificationLink" style="cursor:pointer;"></i></a>
            <div id="notificationContainer">
            <div id="notificationTitle">Notifications</div>
            <div id="notificationsBody" class="notifications">
              <div class="ui relaxed divided list">
                @foreach ($notif as $item)
                <div class="item" style="padding-bottom: 0%;">
                  <div class="content">
                    <div class="header">{{ $item->subject }}</div>
                    <div class="description">{{ $item->message }}</div>
                  </div>
                </div>
                <hr/>
                @endforeach
              </div>
            </div>
            <div id="notificationFooter"><a href="{{ url('/') }}/notif">See All</a></div>
            </div>
          </div>

          <a class="ui inverted button" href="{{ url('/') }}/auth/logout">Log Out</a>
        </div>
      </div>
    </div>
  </div>