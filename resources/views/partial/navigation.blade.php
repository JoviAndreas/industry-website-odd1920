@php
    /*
    * Local Navigations
    * -> Navigation Type
    * -> -> Navigation
    */
    $localNavigations = [
        'department' => [
            'Master' => [
                (\App\Department::isSuperAdmin()) ? new \App\LocalNavigation('Application Configuration', null, 'automated_service.index') : null,
                (\App\Department::isSuperAdmin()) ? new \App\LocalNavigation('Data Synchronization', null, 'data_synchronization.index') : null,
                (\App\Department::isSuperAdmin()) ? new \App\LocalNavigation('Faculty & Degree', null, 'department.faculty') : null,
                new \App\LocalNavigation('Student', 'manageStudent'),
                (!\App\Department::isSSC()) ? new \App\LocalNavigation('Company', 'register') : null,
                //new LocalNavigation('University Supervisor', 'manageguider'),
                (!\App\Department::isSSC()) ? new \App\LocalNavigation('Vacancies', 'manageJob') : null,
                (\App\Department::isSuperAdmin() || \App\Department::isAdmin()) ? new \App\LocalNavigation('Job Position', 'manageJobPosition') : null,
                (\App\Department::isSuperAdmin() || \App\Department::isAdmin()) ? new \App\LocalNavigation('Deleted Job', 'manageDeletedJob') : null,
            ],
            'Organize' => [
                (!\App\Department::isSSC()) ? new \App\LocalNavigation('Apply Student to Job', 'apply') : null,
                new \App\LocalNavigation('Student Placement', 'accepted'),
                (\App\Department::isSuperAdmin() || \App\Department::isAdmin()) ? new \App\LocalNavigation('Edit Recruitment', 'editRecruitment') : null,
                (\App\Department::isSuperAdmin()) ? new \App\LocalNavigation('Edit BIPP', 'editBipp') : null,
                //(\App\Department::isSuperAdmin()) ? new \App\LocalNavigation('Edit Description', 'editDesc') : null,
                (\App\Department::isSuperAdmin() || \App\Department::isAdmin()) ? new \App\LocalNavigation('Unaccept Status', 'unaccept_request') : null,
                (\App\Department::isSuperAdmin() || \App\Department::isAdmin()) ? new \App\LocalNavigation('Unreject Request', 'unreject_request') : null,
                (\App\Department::isSuperAdmin() || \App\Department::isAdmin()) ? new \App\LocalNavigation('Approval Request', 'approval_request') : null,
                //new \App\LocalNavigation('Assign Supervisor', 'assign_supervisor'),
            ],
            'User Management' => [
                (\App\Department::isSuperAdmin()) ? new \App\LocalNavigation('Account Approval', 'accountapproval') : null,
                //new \App\LocalNavigation('Active Student List', 'activestudentlist'),
                (\App\Department::isSuperAdmin()) ? new \App\LocalNavigation('Master Password', 'passwords') : null,
                (\App\Department::isSuperAdmin()) ? new \App\LocalNavigation('Department Account', 'addDepartmentAccount') : null,
                (\App\Department::isSuperAdmin()) ? new \App\LocalNavigation('See Feedbacks', 'seeFeedbacks') : null,

            ]
        ],
        'company' => [
            ''
        ],
        'student' => [
            ''
        ],
    ];
@endphp


@foreach($localNavigations[$localNavigationType] as $navGroupKey => $navGroup)
    @php
        $navGroupCount = 0;
        foreach ($navGroup as $v)
        {
            if ($v != null)
            {
                $navGroupCount++;
            }
        }
    @endphp
    @if ($navGroupCount > 0)
    <div class="{{ ($isLocalNavigationSidebar == 'true') ? '' : 'ui pointing dropdown link' }} item">
        <span class="text">{{ $navGroupKey }}</span>
        <i class="dropdown icon"></i>
        <div class="{{ ($isLocalNavigationSidebar == 'true') ? '' : 'ui' }} menu">
            @foreach ($navGroup as $nav)
                @if ($nav)
                    @if ($nav->route)
                        <a class="{{Request::url() == route($nav->route) ? 'active' : '' }} item"
                           href="{{ route($nav->route) }}">{{ $nav->label }}</a>
                    @else
                        <a class="{{Request::is($nav->url)==1 ? 'active' : '' }} item"
                           href="{{ url($nav->url) }}">{{ $nav->label }}</a>
                    @endif
                @endif
            @endforeach
        </div>
    </div>
    @endif
@endforeach
<div class="ui pointing dropdown item current-semester" tabindex="0">
    <input type="hidden" name="semester" value="851baf40-a93d-11e9-b93f-5d5b57221483">
    <div class="text">Odd 2019/2020</div>
    <div class="menu" tabindex="-1">
        <div class="item active selected" data-value="851baf40-a93d-11e9-b93f-5d5b57221483">Odd 2019/2020</div>
    </div>
</div>

