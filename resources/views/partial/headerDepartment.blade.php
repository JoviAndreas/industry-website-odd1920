  <div class="ui inverted vertical center aligned segment invis">
    <div class="ui container">
      <div class="ui large secondary inverted pointing menu">
        <a class="toc item">
          <i class="sidebar icon"></i>
        </a>

        <a class="
        {{Request::is('homed')==1 ? 'active' : '' }} item"
        href="{{ url('/') }}/homed">Home</a>

        @include('partial.navigation', ['localNavigationType' => 'department'])

{{--        <div class="ui pointing dropdown link item">--}}
{{--          <span class="text">Master</span>--}}
{{--          <i class="dropdown icon"></i>--}}
{{--          <div class="ui menu">--}}
{{--            @if (\App\Department::isAdmin())--}}
{{--            <a class="{{Request::is(route('department.faculty'))==1 ? 'active' : '' }}--}}
{{--            item" href="{{ route('department.faculty') }}">Faculty & Degree</a>--}}
{{--            @endif--}}
{{--            <a class="--}}
{{--            {{Request::is('manageStudent')==1 ? 'active' : '' }}--}}
{{--            item" href="{{ url('/') }}/manageStudent">Student</a>--}}
{{--            <a class="--}}
{{--            {{Request::is('register')==1 ? 'active' : '' }}--}}
{{--            item" href="{{ url('/') }}/register">Company</a>--}}
{{--            <a class="--}}
{{--            {{Request::is('manageguider')==1 ? 'active' : '' }}--}}
{{--            item" href="{{ url('/') }}/manageguider">University Supervisor</a>--}}
{{--            <a class="--}}
{{--            {{Request::is('manageJob')==1 ? 'active' : '' }}--}}
{{--            item" href="{{ url('/') }}/manageJob">Vacancies</a>--}}
{{--            <a class="--}}
{{--            {{Request::is('manageJobPosition')==1 ? 'active' : '' }}--}}
{{--            item" href="{{ url('/') }}/manageJobPosition">Job Position</a>--}}
{{--            <a class="--}}
{{--            {{Request::is('manageDeletedJob')==1 ? 'active' : '' }}--}}
{{--            item" href="{{ url('/') }}/manageDeletedJob">Deleted Job</a>--}}
{{--          </div>--}}
{{--        </div>--}}

{{--        <div class="ui pointing dropdown link item">--}}
{{--          <span class="text">Organize</span>--}}
{{--          <i class="dropdown icon"></i>--}}
{{--          <div class="ui menu">--}}
{{--            <a class="--}}
{{--            {{Request::is('apply')==1 ? 'active' : '' }}--}}
{{--            item" href="{{ url('/') }}/apply">Apply Student to Job</a>--}}

{{--            <a class="--}}
{{--            {{Request::is('accepted')==1 ? 'active' : '' }}--}}
{{--            item" href="{{ url('/') }}/accepted">Student Placement</a>--}}

{{--            <a class="--}}
{{--            {{Request::is('editRecruitment')==1 ? 'active' : '' }}--}}
{{--            item" href="{{ url('/') }}/editRecruitment">Edit Recruitment</a>--}}
{{--            @if(\App\Department::isSuperAdmin())--}}
{{--            <a class="--}}
{{--              {{Request::is('editBipp')==1 ? 'active' : '' }}--}}
{{--              item" href="{{ url('/') }}/editBipp">Edit BIPP</a>--}}

{{--            <a class="--}}
{{--            {{Request::is('editDesc')==1 ? 'active' : '' }}--}}
{{--            item" href="{{ url('/') }}/editDesc">Edit Description</a>--}}

{{--            <a class="--}}
{{--            {{Request::is('unaccept_request')==1 ? 'active' : '' }}--}}
{{--            item" href="{{ url('/') }}/unaccept_request">Unaccept Status</a>--}}
{{--            @endif--}}
{{--            <a class="--}}
{{--            {{Request::is('unreject_request')==1 ? 'active' : '' }}--}}
{{--            item" href="{{ url('/') }}/unreject_request">Unreject Request</a>--}}

{{--            <a class="--}}
{{--            {{Request::is('approval_request')==1 ? 'active' : '' }}--}}
{{--            item" href="{{ url('/') }}/approval_request">Approval Request</a>--}}

{{--            <a class="--}}
{{--            {{Request::is('assign_supervisor')==1 ? 'active' : '' }}--}}
{{--            item" href="{{ url('/') }}/assign_supervisor">Assign Supervisor</a>--}}
{{--          </div>--}}
{{--        </div>--}}

{{--        <div class="ui pointing dropdown link item">--}}
{{--          <span class="text">User Management</span>--}}
{{--          <i class="dropdown icon"></i>--}}
{{--          <div class="ui menu">--}}
{{--            <a class="--}}
{{--            {{Request::is('accountapproval')==1 ? 'active' : '' }} item"--}}
{{--            href="{{ url('/') }}/accountapproval">Account Approval</a>--}}

{{--            <a class="--}}
{{--            {{Request::is('activestudentlist')==1 ? 'active' : '' }} item"--}}
{{--            href="{{ url('/') }}/activestudentlist">Active Student List</a>--}}
{{--            @if (\App\Department::isSuperAdmin())--}}
{{--            <a class="--}}
{{--              {{Request::is('passwords')==1 ? 'active' : '' }} item"--}}
{{--              href="{{ url('/') }}/passwords">Master Password</a>--}}
{{--            @endif--}}

{{--            --}}{{-- @if (\App\Department::isSuperAdmin())--}}
{{--            <a class="item" href="{{ url('facultyPasswords') }}">Faculty Password</a>--}}
{{--            @endif --}}
{{--            <a class="--}}
{{--            {{Request::is('addDepartmentAccount')==1 ? 'active' : '' }} item"--}}
{{--            href="{{ url('/') }}/addDepartmentAccount">Department Account</a>--}}
{{--            <a class="--}}
{{--            {{Request::is('seeFeedbacks')==1 ? 'active' : '' }} item"--}}
{{--            href="{{ url('/') }}/seeFeedbacks">See Feedbacks</a>--}}
{{--          </div>--}}
{{--        </div>--}}

        @if (Auth::check())
        <div class="header item">Welcome, {{ Auth::user()->userid }}</div>
        @endif
        <div class="right menu item">
          <a class="ui inverted" href="{{ url('/') }}/mailbox">
            <div class="item" style="padding-top:0px; padding-bottom: 0px;">
              <i class="middle aligned mail outline large icon"></i>
            </div>
          </a>
          <a class="ui inverted button" href="{{ url('/') }}/auth/logout">Log Out</a>
        </div>
      </div>
    </div>
  </div>
