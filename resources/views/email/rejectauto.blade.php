Dear {{ $rec['company_name'] }}, <br><br>

We would like to inform you that job application status for position <strong>{{ $rec['job_name'] }}</strong> for student <strong>{{ $rec['student_name'] }} - {{ $rec['student_id'] }}</strong> has been changed to <strong>"rejected"</strong> automatically by the system.<br>

<br>
The student do not process the interview invitation within one week.<br>

<br>
To see the student's profile you can click <a href="http://industry.socs.binus.ac.id/profile/{{ $rec['student_id'] }}">here</a><br>

<br>
Thank you and have a nice day.
