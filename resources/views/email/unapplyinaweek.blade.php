Dear {{ $data['company_name'] }}, <br><br>

We would like to inform you that there are [<strong>{{ count($data['list_student']) }}</strong>] job application that will be unapplied in the following week.

<br>
Below is the list of students that will be unapplied in the following week:<br>
<table border="1" cellspacing="2" cellpadding="2">
    <tr>
        <td>Student ID</td>
        <td>Student Name</td>
        <td>Job Name</td>
        <td>Status</td>
        <td>Expired At</td>
    </tr>
    @foreach($data['list_student'] as $s)
    <tr>
        <td>{{ $s['student_id'] }}</td>
        <td>{{ $s['student_name'] }}</td>
        <td>{{ $s['job_name'] }}</td>
        <td>{{ $s['status'] }}</td>
        <td>{{ $s['expired_at'] }}</td>
    </tr>
    @endforeach
</table>

<br>
<br>
To see the current status, you can login to <a href="http://industry.socs.binus.ac.id/">the website</a><br>

<br>
Thank you and have a nice day.
