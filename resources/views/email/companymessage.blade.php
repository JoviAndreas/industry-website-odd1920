Dear {{ $data['recipient_company_name'] }}, <br><br>

We would like to inform you that there is new <strong>Message</strong> from {{ $data['sender_student_name'] }} ({{ $data['sender_student_id'] }})<br><br>

Message Subject: <strong>{{ $data['subject'] }}</strong>

<br>
<br>
To see the message, you can login <a href="http://industry.socs.binus.ac.id/">here</a> and open your mailbox.<br>

<br>
Thank you and have a nice day.
