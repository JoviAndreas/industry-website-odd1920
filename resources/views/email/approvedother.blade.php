Dear {{ $data['company_name'] }}, <br><br>

We would like to inform you that student [<strong>{{ $data['student_name'] }} - {{ $data['student_id'] }}</strong>] has been approved to another company.<br>

<br>
To see the student's profile you can click <a href="http://industry.socs.binus.ac.id/profile/{{ $data['student_id'] }}">here</a><br>

<br>
Thank you and have a nice day.
