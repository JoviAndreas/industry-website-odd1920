Dear {{ $rec['student_name'] }} - {{ $rec['student_id'] }}, <br><br>

We would like to inform you that job application status for position <strong>{{ $rec['job_name'] }}</strong> at <strong>{{ $rec['company_name'] }}</strong> has been changed to <strong>"{{ $rec['status'] }}"</strong><br>

<br>
To see your current status you can click <a href="http://industry.socs.binus.ac.id/status/">here</a><br>

<br>
Thank you and have a nice day.
