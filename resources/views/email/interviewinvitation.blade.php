Dear {{ $interview['student_name'] }} - {{ $interview['student_id'] }}, <br><br>

We would like to inform you that there is new <strong>Interview Invitation</strong> for job position <strong>{{ $interview['job_name'] }}</strong> at <strong>{{ $interview['company_name'] }}</strong>.<br>

<br>
To reply to the invitation, you can login <a href="http://industry.socs.binus.ac.id/">here</a> and open your mailbox.<br>

<br>
Thank you and have a nice day.
