Dear {{ $data['company_name'] }}, <br><br>

We would like to inform you that there are [<strong>{{ $data['count_of_job_application'] }}</strong>] new job application since yesterday.

<br>
To see the current status, you can login to <a href="http://industry.socs.binus.ac.id/">the website</a><br>

<br>
Thank you and have a nice day.
