Dear {{ $rec['company_name'] }}, <br><br>

We would like to inform you that job application for position <strong>{{ $rec['job_name'] }}</strong> for student <strong>{{ $rec['student_name'] }} - {{ $rec['student_id'] }}</strong> has been unapplied automatically by the system.<br>

<br>
To see the student's profile you can click <a href="http://industry.socs.binus.ac.id/profile/{{ $rec['student_id'] }}">here</a><br>

<br>
If you want the student to re-apply to your company you can <strong>send message</strong> (from the application) to the student or <strong>send email</strong> to the student.<br>

<br>
Thank you and have a nice day.
