Dear {{ $data['student_name'] }} - {{ $data['student_id'] }}, <br><br>

We would like to inform you that there are [<strong>{{ $data['count_of_new_jobs'] }}</strong>] new job posted since yesterday.

<br>
To apply, you can login to <a href="http://industry.socs.binus.ac.id/">the website</a><br>

<br>
Thank you and have a nice day.
