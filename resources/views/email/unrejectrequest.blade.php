Dear {{ $data['department_name'] }}, <br><br>

We would like to inform you that there are <strong>new unreject request</strong>.

<br>
Below is the detail of the unreject request<br>
<table border="1" cellspacing="1" cellpadding="1">
    <tr>
        <td>Student ID</td>
        <td>Student Name</td>
        <td>Program</td>
        <td>Job Name</td>
        <td>Company Name</td>
        <td>Reason</td>
    </tr>
    <tr>
        <td>{{ $data['student_id'] }}</td>
        <td>{{ $data['student_name'] }}</td>
        <td>{{ $data['student_program'] }}</td>
        <td>{{ $data['job_name'] }}</td>
        <td>{{ $data['company_name'] }}</td>
        <td>{{ $data['reason'] }}</td>
    </tr>
</table>

<br>
To see the current request, you can login to <a href="http://industry.socs.binus.ac.id/">the website</a><br>

<br>
Thank you and have a nice day.
