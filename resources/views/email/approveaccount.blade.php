Dear {{ $user['name'] }}, <br><br>

We would like to inform you that your registration account request has been accepted.<br>

You can login in <a href="http://industry.socs.binus.ac.id/">http://industry.socs.binus.ac.id</a> using the following account: <br><br>

Username: {{ $user['userid'] }}<br>
Password: {{ $user['defaultpassword'] }}<br><br>

Thank you and have a nice day.