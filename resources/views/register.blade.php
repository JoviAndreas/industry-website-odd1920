<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Standard Meta -->

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

    <!-- Site Properties -->
    <title>BINUS</title>
    <link rel="icon" type="image/png" href="{{ URL::asset('assets/images/favicon.png') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/semantic/dist/semantic.min.css">
        <link rel="stylesheet" type="text/css" href="{{ url('/') }}/semantic/dist/semantics.min.css">
    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/semantic/dist/components/dataTables.semanticui.min.css">
    <script type="text/javascript" src="{{ url('/') }}/semantic/dist/jquery.js"></script>
    <script src="{{ url('/') }}/semantic/dist/semantic.min.js"></script>
    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/jqueryui/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/jquery-timepicker-1.3.5/jquery.timepicker.min.css">


     <script type="text/javascript" src="{{ url('/') }}/semantic/dist/jquery.js"></script>
    <script type="text/javascript" src="{{ url('/') }}/semantic/dist/semantic.min.js"></script>
    <script type="text/javascript" src="{{ url('/') }}/semantic/dist/semantics.min.js"></script>
    <script type="text/javascript" src="{{ url('/') }}/semantic/dist/components/accordion.js"></script>
    <script type="text/javascript" src="{{ url('/') }}/semantic/dist/components/jquery.dataTables.min.js" ></script>
    <script type="text/javascript" src="{{ url('/') }}/semantic/dist/components/dataTables.semanticui.min.js" ></script>
    <script type="text/javascript" src="{{ url('/') }}/jqueryui/jquery-ui.js" ></script>
    <script type="text/javascript" src="{{ url('/') }}/jquery-timepicker-1.3.5/jquery.timepicker.min.js" ></script>
    <style type="text/css">
     body {
        background-color: #DADADA;
      }
      body > .grid {
        height: 100%;
      }
      .image {
        margin-top: -100px;
      }
      .column {
        max-width: 450px;
      }
      .note{
        font-size:11px;
      }
      .title{
        font-weight:bold;
        text-decoration:underline;
      }
    </style>
    <script>
        $(document).ready(function(){
            $('.ui.dropdown').dropdown({
                onChange: function()
                {

                    var role = $(this).dropdown('get value')
                    if(role == "student")
                    {
                        $('#registerstudent').css("display", "block");
                        $('#registercompany').css("display", "none");

                    }
                    else if(role == 'company')
                    {
                        $('#registerstudent').css("display", "none");
                        $('#registercompany').css("display", "block");
                    }
                }
            });

        $('.ui.large.register.student.form')
            .form({
              fields: {
                nim   : ['empty', 'exactLength[10]'],
                name   : ['empty', 'maxLength[100]'],
                gender   : ['empty'],
                email   : ['empty', 'maxLength[120]','email'],
                dob   : ['empty'],
                phone   : [ 'empty','maxLength[50]'],
                school   : ['empty'],
                major   : ['empty'],
                interest   : ['empty'],
                semester   : ['empty'],
                campus : ['empty'],
              }
            })
          ;

            $('.ui.large.register.company.form')
            .form({
              fields: {

                name   : ['empty', 'maxLength[100]'],
                web   : ['empty', 'maxLength[100]','url'],
                desc   : ['empty'],
                email   : ['empty', 'maxLength[120]','email'],
                phone   : [ 'empty','maxLength[50]']
              }
            })
          ;
        });





     </script>
  </head>
  <body>


<center>
<h3 class="ui center aligned header">
    <h2 class="ui blue image header">
        <img src="{{ url('/') }}/assets/images/logo.png" class="image">
        <div class="content">Registration</div>
    </h2>
    <div class="ui text container segment">
        @if(!empty($role) && $role == 'department')
        Back to Home page <a href="{{ url('/') }}">here </a>!
        @elseif(!empty($role) && $role == 'guest')
        Back to Login page <a href="{{ url('/') }}">here </a>!
        @endif
    </div>
</h3>
</center>

<div class="ui text container">
    <div class="ui segment">
        <center>
            <label>Register as :</label>
            <div class="ui selection dropdown">
                <input type="hidden" name="role">
                <i class="dropdown icon"></i>
                <div class="default text">---</div>
                <div class="menu">
                    @if(!empty($role) && $role == 'department')
                    <div class="item" data-value="student">Student</div>
                    @elseif(!empty($role) && $role == 'guest')
                    <div class="item" data-value="student">Student</div>
                    <div class="item" data-value="company">Company</div>
                    @endif
                </div>
            </div>
            @if(count($errors) > 0)
            <div class="ui error message">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if (session('success'))
            <div class="row">
                <div class="ui positive message">
                  <div class="header">{{session('success')}}</div>
                </div>
            </div>
            @endif
            @if (session('err'))
                <div class="ui error message">{{session('err')}}</div>
            @endif
        </center>
    </div>
    @if(!empty($role) && $role == 'guest')
    {!! Form::open(['url' => 'insertUser', 'method' => 'post', 'role' => 'form', 'class' => 'ui large register student form', 'id' => 'registerstudent', 'style' => 'display:none']) !!}
    @elseif(!empty($role) && $role == 'department')
    {!! Form::open(['url' => 'insertUserByDepartment', 'method' => 'post', 'role' => 'form', 'class' => 'ui large register student form', 'id' => 'registerstudent', 'style' => 'display:none']) !!}
    @endif
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="ui stacked segment">
            <h4 class="ui dividing header">Biodata</h4>
            <div class="field">
                <label>NIM</label>
                <input type="text" name="nim" placeholder="NIM"
                value="@if(old('nim')!=null){{old('nim')}}@elseif(!empty($role) && $role == 'department' && !empty(request('nim'))){{request('nim')}}@endif">
            </div>
            <div class="field">
                <label>Name</label>
                <input type="text" name="name" placeholder="Name"
                value="@if(old('name')!=null){{old('name')}}@elseif(!empty($role) && $role == 'department' && !empty(request('name'))){{request('name')}}@endif">
            </div>
            <div class="field">
                <label>Gender</label>
                <div class="ui selection dropdown">
                    <input type="hidden" name="gender"
                    value="@if(old('gender')!=null){{old('gender')}}@endif">
                    <i class="dropdown icon"></i>
                    <div class="default text">---</div>
                    <div class="menu">
                        <div class="item" data-value="male">Male</div>
                        <div class="item" data-value="female">Female</div>
                    </div>
                </div>
            </div>
            <div class="field">
                <label>DOB</label>
                <div class="ui input left icon">
                    <input type="date" placeholder="YYYY-MM-DD" name="dob"
                     value="@if(old('dob')!=null){{old('dob')}}@endif">
                    <i class="calendar icon"></i>
                </div>
            </div>
        </div>
        <div class="ui stacked segment">
            <h4 class="ui dividing header">Contact Information</h4>
            <div class="field">
                <label>Email</label>
                <input type="text" name="email" placeholder="Email"
                 value="@if(old('email')!=null){{old('email')}}@endif">
            </div>
            <div class="field">
                <label>Phone</label>
                <input type="text" name="phone" placeholder="Phone"
                 value="@if(old('phone')!=null){{old('phone')}}@endif">
            </div>
        </div>
        <div class="ui stacked segment">
            <h4 class="ui dividing header">Academic Information</h4>
            <div class="field">
                <label>School</label>
                <div class="ui selection dropdown">
                    <input type="hidden" name="school"
                    value="@if(old('school')!=null){{old('school')}}@endif">
                    <i class="dropdown icon"></i>
                    <div class="default text">---</div>
                    <div class="menu">
                        @foreach($faculties as $f)
                            <div class="item" data-value="School of Computer Science">{{ $f->name }}</div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="field">
                <label>Program</label>
                <div class="ui selection dropdown">
                    <input type="hidden" name="major"
                      value="@if(old('major')!=null){{old('major')}}@endif">
                    <i class="dropdown icon"></i>
                    <div class="default text">---</div>
                    <div class="menu">
                        @foreach($degrees as $d)
                        <div class="item">{{ $d->name }}</div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="field">
                <label>Interest</label>
                <div class="ui selection dropdown">
                    <input type="hidden" name="interest"
                    value="@if(old('interest')!=null){{old('interest')}}@endif">
                    <i class="dropdown icon"></i>
                    <div class="default text">---</div>
                    <div class="menu">
                        <div class="item">-</div>
                        <div class="item">Global Class</div>
                        <div class="item">Database</div>
                        <div class="item">Multimedia</div>
                        <div class="item">AI</div>
                        <div class="item">Network</div>
                        <div class="item">Software Engineering</div>
                        <div class="item">Applied Database</div>
                        <div class="item">Applied Network</div>
                    </div>
                </div>
            </div>
            <div class="field">
                <label>Semester</label>
                <div class="ui selection dropdown">
                    <input type="hidden" name="semester"
                     value="@if(old('semester')!=null){{old('semester')}}@endif">
                    <i class="dropdown icon"></i>
                    <div class="default text">---</div>
                    <div class="menu">
                    @for($i=1;$i<=14;$i++)
                        <div class="item">{{$i}}</div>
                    @endfor
                    </div>
                </div>
            </div>
            <div class="field">
                <label>Campus</label>
                <div class="ui selection dropdown">
                    <input type="hidden" name="campus"
                     value="@if(old('campus')!=null){{old('campus')}}@endif">
                    <i class="dropdown icon"></i>
                    <div class="default text">---</div>
                    <div class="menu">
                    @foreach(\App\Campus::all() as $c)
                        <div class="item">{{$c->name}}</div>
                    @endforeach
                    </div>
                </div>
            </div>
        </div>
        <button class="ui fluid large blue primary button" type="submit">Register</button>
        <div class="ui error message"></div>
    {!! Form::close() !!}

    @if(!empty($role) && $role == 'guest')
    {!! Form::open(['url' => 'insertUser', 'method' => 'post', 'role' => 'form', 'class' => 'ui large register company form', 'id' => 'registercompany', 'style' => 'display:none']) !!}
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="ui stacked segment">
            <h4 class="ui dividing header">Company Information</h4>
            <div class="field">
                <label>Name</label>
                <input type="text" name="name" placeholder="Name"
                 value="@if(old('name')!=null){{old('name')}}@endif">
            </div>
            <div class="field">
                <label>Website</label>
                <input type="text" name="web" placeholder="www.company.com"
                value="@if(old('web')!=null){{old('web')}}@endif">
            </div>
            {{-- <!-- <div class="ui left icon input field">
            <div class="ui checkbox">
                <input type="checkbox" name="bipp" value="true">
                <label>BIPP</label>
            </div>
            </div> --> --}}
            <div class="field">
                <label>Company Description</label>
                <textarea rows="2" name="desc">@if(old('desc')!=null){{old('desc')}}@endif</textarea>
            </div>
            {{-- <!-- <div class="field note">
                <span>{{$desc->description}}</span>
            </div> --> --}}
        </div>
        <div class="ui stacked segment">
            <h4 class="ui dividing header">Contact Information</h4>
            <div class="field">
                <label>Email</label>
                <input type="text" name="email" placeholder="Email"
                value="@if(old('email')!=null){{old('email')}}@endif">
            </div>
            <div class="field">
                <label>Phone</label>
                <input type="text" name="phone" placeholder="Phone"
                 value="@if(old('phone')!=null){{old('phone')}}@endif">
            </div>
        </div>
        <button class="ui fluid large blue primary button" type="submit">Register</button>
        <div class="ui error message"></div>
    {!! Form::close() !!}
    @endif
<br><br>
</div>

</body>
</html>
