@extends ('layout.masterD')

@section ('title', 'BINUS')

@section('content')

@include('partial/headerDepartment')
  
<br><br>

<div class="ui container" style="overflow: auto;">
  
  <div class="row">
    @if (session('success'))
    <div class="ui positive message">
      <i class="close icon"></i>
      <div class="header">
        You have successfully {{ session('success') }}.
      </div>
    </div>
    <br>
    @endif
    @foreach ($errors->all() as $error)
    <div class="ui negative message">
      <i class="close icon"></i>
      <div class="header">
        {{ $error }}
      </div>
    </div>
    <br>
    @endforeach
  </div>
  
  <h2 class="ui header centered">Update Faculty</h2>
  <div class="content">
    {!! Form::open(['url' => route('department.faculty.update', ['faculty' => $faculty->id]), 'method' => 'post', 'id' => 'formUpdateFaculty', 'role' => 'form', 'class' => 'ui large update form']) !!}
    
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="ui">
      <div class="field">
        <div class="ui left icon input">
          {{-- Insert Faculty --}}
          <input type="text" name="name" required placeholder="Faculty Name" value="{{old('name') ? old('name') : $faculty->name}}">
          <i class="building icon"></i>
        </div>
      </div>
      <div class="field">
        <div>Programs:</div>
        @if(count($degrees)===0)-@endif
        @foreach($degrees as $degree)
        <div class="ui left icon input">
          <div class="ui checkbox">
            <input type="checkbox"  class="inputCheckboxDegree" name="degrees[]" value="{{ $degree->id }}" checked>
            <label>{{ $degree->name }}</label>
          </div>
        </div>
        @endforeach
      </div>
      <div class="field">
          <div class="red">Unmapped Programs:</div>
          @if(count($unmappedDegrees)===0)-@endif
          @foreach($unmappedDegrees as $degree)
          <div class="ui left icon input">
            <div class="ui checkbox">
              <input type="checkbox"  class="inputCheckboxDegree" name="unmappedDegrees[]" value="{{ $degree->id }}">
              <label>{{ $degree->name }}</label>
            </div>
          </div>
          @endforeach
        </div>
      <div class="ui fluid large blue submit button">Update</div>
      <br>
      <a href="{{ route('department.faculty') }}" class="ui fluid large button">Back</a>
    </div>
    <div class="ui error message" id="error message update faculty"></div>
    {!! Form::close() !!}
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function() {
    $('#formUpdateFaculty').form({
      fields: {
        name : ['empty'],
      }
    });
  });
</script>
@stop
