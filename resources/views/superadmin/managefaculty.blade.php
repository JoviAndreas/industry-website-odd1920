@extends ('layout.masterD')

@section ('title', 'BINUS')

@section('content')


@include('partial/headerDepartment')

<br><br>

<div class="ui middle aligned container">
  
  <div class="row">
    @if (session('success'))
    <div class="ui positive message">
      <i class="close icon"></i>
      <div class="header">
        You have successfully {{ session('success') }}.
      </div>
    </div>
    <br>
    @endif
    @foreach ($errors->all() as $error)
    <div class="ui negative message">
      <i class="close icon"></i>
      <div class="header">
        {{ $error }}
      </div>
    </div>
    <br>
    @endforeach
  </div>
  
  @if(session('head_of_faculty') == 'all')
  {!! Form::open(['url' => route('department.faculty'), 'method' => 'get', 'role' => 'form', 'id' => 'formFilterByFaculty', 'class' => 'ui large form']) !!}
  <div class="fields">
    <div class="field">
      <select name="filter" id="buttonFilterByFaculty">
        <option value="all">All Faculty</option>
        @foreach($filterForRole as $faculty)
        <option value="{{ $faculty->id }}"
          @if (!empty(Request::input('filter')) && Request::input('filter') == $faculty->id) selected @endif>
          {{ $faculty->name }}
        </option>
        @endforeach
      </select>
    </div>
  </div>
  {!! Form::close() !!}
        <br>
{{--  <div class="ui large button insertFaculty">Insert Faculty</div>--}}

  @else
  <form class="ui large form">
    <div class="fields">
      <div class="field">
        <select name="filter" disabled>
          <option>{{ $filterForRole->first()->name }}</option>
        </select>
      </div>
    </div>  
  </form>
  @endif
{{--  <div class="ui large button insertDegree">Insert Degree</div>--}}
</div>
<br>
<div class="ui middle aligned stackable container" style="padding: 0 0 8rem 0;">
  
  {{-- Faculty List --}}
  <h3 class="ui horizontal divider header">
    List of Faculties
  </h3>
  <div class="row">
    <table class="ui small table">
      <thead>
        <tr>
          <th class="four wide">Faculty Name</th>
          <th class="four wide">Degrees</th>
{{--          <th class="four wide"></th>--}}
        </tr>
      </thead>
      <tbody>
        @foreach ($faculties as $faculty)
        <tr>
          <td>{{ $faculty->name }}</td>
          <td>
            <div class="ui bulleted list">
              @foreach ($faculty->degrees as $d)
              <div class="item">{{ $d->name }}</div>
              @endforeach
            </div>
          </td>
{{--          <td>--}}
{{--            <a class="ui small button blue updateFaculty" href="{{ route('department.faculty.edit', ['faculty' => $faculty->id]) }}">Update Mapping</a>--}}
{{--            @if(session('head_of_faculty') == 'all')--}}
{{--            <div class="ui small button red deleteFaculty" data-id="{{ $faculty->id }}" data-name="{{ $faculty->name }}">Delete</div>--}}
{{--            <form action="{{ route('department.faculty.delete', ['id'=>$faculty->id]) }}" method="POST" id="formDeleteFaculty-{{ $faculty->id }}">{{ csrf_field() }}</form>--}}
{{--            @endif--}}
{{--          </td>--}}
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
  {{-- End of Faculty List --}}
  
  {{-- All Degrees List --}}
  <h3 class="ui horizontal divider header">
    List of Degrees
  </h3>
  <div class="row">
    <table class="ui small table">
      <thead>
        <tr>
          <th class="four wide">Degree Name</th>
          <th class="four wide">Faculty Name</th>
{{--          <th class="four wide"></th>--}}
        </tr>
      </thead>
      <tbody>
        @foreach ($degrees as $degree)
        <tr>
          <td>{{ $degree->name }}</td>
          <td>{{ ($degree->faculty != NULL) ? $degree->faculty->name : '[NOT MAPPED]' }}</td>
{{--          <td>--}}
{{--            <div class="ui small button updateDegree blue" data-id="{{ $degree->id }}" data-name="{{ $degree->name }}">Update</div>--}}
{{--            <div class="ui small button deleteDegree red" data-id="{{ $degree->id }}" data-name="{{ $degree->name }}">Delete</div>--}}
{{--            <form action="{{ route('department.degree.delete', ['id'=>$degree->id]) }}" method="POST" id="formDeleteDegree-{{ $degree->id }}">{{ csrf_field() }}</form>--}}
{{--          </td>--}}
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
  {{-- End of All Degrees List --}}
  
</div>

{{--<div class="ui modal insertFaculty" style="overflow: auto;">--}}
{{--  <div class="header">Insert Faculty</div>--}}
{{--  <div class="content">--}}
{{--    {!! Form::open(['url' => route('department.faculty.insert'), 'method' => 'post', 'id' => 'formInsertFaculty', 'role' => 'form', 'class' => 'ui large insert form']) !!}--}}
{{--    --}}
{{--    <input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
{{--    <div class="ui">--}}
{{--      <div class="field">--}}
{{--        <div class="ui left icon input">--}}
{{--          --}}{{-- Insert Faculty --}}
{{--          <input type="text" name="name" required placeholder="Faculty Name" value="{{old('name')}}">--}}
{{--          <i class="building icon"></i>--}}
{{--        </div>--}}
{{--      </div>--}}
{{--      --}}
{{--      <div class="ui fluid large blue submit button">Insert</div>--}}
{{--    </div>--}}
{{--    <div class="ui error message" id="error message insert faculty"></div>--}}
{{--    {!! Form::close() !!}--}}
{{--  </div>--}}
{{--</div>--}}

{{--<div class="ui modal insertDegree" style="overflow: auto;">--}}
{{--  <div class="header">Insert Degree</div>--}}
{{--  <div class="content">--}}
{{--    {!! Form::open(['url' => route('department.degree.insert'), 'method' => 'post', 'id' => 'formInsertDegree', 'role' => 'form', 'class' => 'ui large insert form']) !!}--}}
{{--    --}}
{{--    <input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
{{--    <div class="ui">--}}
{{--      <div class="field">--}}
{{--        <div class="ui left icon input">--}}
{{--          --}}{{-- Insert Degree --}}
{{--          <input type="text" name="name" required placeholder="Degree Name" value="{{old('name')}}">--}}
{{--          <i class="book icon"></i>--}}
{{--        </div>--}}
{{--      </div>--}}
{{--      <div class="field">--}}
{{--        <select name="faculty" required aria-placeholder="Faculty">--}}
{{--          <option value="" disabled selected>--Faculty--</option>--}}
{{--          @foreach($filterForRole as $faculty)--}}
{{--          <option value="{{ $faculty->id }}">--}}
{{--            {{ $faculty->name }}--}}
{{--          </option>--}}
{{--          @endforeach--}}
{{--        </select>--}}
{{--      </div>--}}
{{--      --}}
{{--      <div class="ui fluid large blue submit button">Insert</div>--}}
{{--    </div>--}}
{{--    <div class="ui error message" id="error message insert degree"></div>--}}
{{--    {!! Form::close() !!}--}}
{{--  </div>--}}
{{--</div>--}}

{{--<div class="ui modal updateDegree" style="overflow: auto;">--}}
{{--  <div class="header">Update Degree</div>--}}
{{--  <div class="content">--}}
{{--    {!! Form::open(['url' => route('department.degree.update'), 'method' => 'post', 'id' => 'formUpdateDegree', 'role' => 'form', 'class' => 'ui large insert form']) !!}--}}
{{--    --}}
{{--    <input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
{{--    <div class="ui">--}}
{{--      <div class="field">--}}
{{--        <div class="ui left icon input">--}}
{{--          --}}{{-- Update Degree --}}
{{--          <input type="hidden" id="contentApplicant-updateDegree-id" name="id">--}}
{{--          <input type="text" id="contentApplicant-updateDegree-name" required placeholder="Degree Name" name="name">--}}
{{--          <i class="book icon"></i>--}}
{{--        </div>        --}}
{{--      </div>--}}
{{--      <div class="field">--}}
{{--        <select  name="faculty" required placeholder="Faculty">--}}
{{--          <option value="" disabled selected>--Faculty--</option>--}}
{{--          @foreach($filterForRole as $faculty)--}}
{{--          <option value="{{ $faculty->id }}">--}}
{{--            {{ $faculty->name }}--}}
{{--          </option>--}}
{{--          @endforeach--}}
{{--        </select>--}}
{{--      </div>--}}
{{--      --}}
{{--      <div class="ui fluid large blue submit button">Update Degree</div>--}}
{{--    </div>--}}
{{--    <div class="ui error message" id="error message update degree"></div>--}}
{{--    {!! Form::close() !!}--}}
{{--  </div>--}}
{{--</div>--}}

{{--<div class="ui small modal confirmation deleteFaculty">--}}
{{--  <div class="header">Confirmation</div>--}}
{{--  <div class="content">--}}
{{--    <p id="contentApllicant-deleteFaculty-name"></p>--}}
{{--  </div>--}}
{{--  <div class="actions">--}}
{{--    <div class="ui approve green button">Yes</div>--}}
{{--    <div class="ui cancel red button">No</div>--}}
{{--  </div>--}}
{{--</div>--}}

{{--<div class="ui small modal confirmation deleteDegree">--}}
{{--  <div class="header">Confirmation</div>--}}
{{--  <div class="content">--}}
{{--    <p id="contentApllicant-deleteDegree-name"></p>--}}
{{--  </div>--}}
{{--  <div class="actions">--}}
{{--    <div class="ui approve green button">Yes</div>--}}
{{--    <div class="ui cancel red button">No</div>--}}
{{--  </div>--}}
{{--</div>--}}

<script type="text/javascript">
  $(document).ready(function() {
    let facultyId = "";
    let degreeId = "";
    
    // // Insert Faculty Modal
    // $('.ui.button.insertFaculty').click(function(e) {
    //   $('.ui.modal.insertFaculty').modal('show');
    // });
    // $('#formInsertFaculty').form({
    //   fields: {
    //     name : ['empty'],
    //   }
    // });
    //
    // //Delete Faculty Modal
    // $('.ui.button.deleteFaculty').click(function ()  {
    //   let facultyName = $(this).data("name");
    //   facultyId = $(this).data("id");
    //   document.getElementById("contentApllicant-deleteFaculty-name").innerHTML = "Are you sure you want to delete faculty " + facultyName + " ?<br>WARNING: This will also delete all the degrees within this faculty!";
    // });
    // $('.ui.small.modal.confirmation.deleteFaculty').modal("setting", {
    //   blurring: true,
    //   onApprove: function ()
    //   {
    //     $('#formDeleteFaculty-' + facultyId).submit();
    //   }
    // }).modal('attach events', '.ui.button.deleteFaculty', 'show');
    //
    // // Insert Degree Modal
    // $('.ui.button.insertDegree').click(function(e) {
    //   $('.ui.modal.insertDegree').modal('show');
    // });
    // $('#formInsertDegree').form({
    //   fields: {
    //     name : {rules: [{type: 'empty'}]},
    //     faculty : {rules: [{type: 'empty', prompt: 'Faculty must be filled'}]}
    //   }
    // });
    //
    // // Update Degree Modal
    // $('.ui.button.updateDegree').click(function(e) {
    //   let degreeId = $(this).data("id");
    //   let degreeName = $(this).data("name");
    //   $('.ui.modal.updateDegree').modal('show');
    //   document.getElementById('contentApplicant-updateDegree-id').value = degreeId;
    //   document.getElementById('contentApplicant-updateDegree-name').value = degreeName;
    // });
    // $('#formUpdateDegree').form({
    //   fields: {
    //     name : {rules: [{type: 'empty'}]},
    //     faculty : {rules: [{type: 'empty', prompt: 'Faculty must be filled'}]}
    //   }
    // });
    //
    // //Delete Faculty Modal
    // $('.ui.button.deleteDegree').click(function ()  {
    //   let degreeName = $(this).data("name");
    //   degreeId = $(this).data("id");
    //   document.getElementById("contentApllicant-deleteDegree-name").innerHTML = "Are you sure you want to delete degree " + degreeName + " ?";
    // });
    // $('.ui.small.modal.confirmation.deleteDegree').modal("setting", {
    //   blurring: true,
    //   onApprove: function ()
    //   {
    //     $('#formDeleteDegree-' + degreeId).submit();
    //   }
    // }).modal('attach events', '.ui.button.deleteDegree', 'show');
    //
    // Filter By Faculty
    $('#buttonFilterByFaculty').change(function(e) {
      $('#formFilterByFaculty').submit();
    });
  });
</script>
@stop
