@extends ('layout.masterD')

@section ('title', 'BINUS')

@section('content')


    @include('partial/headerDepartment')

    <br><br>
    <div class="ui middle aligned container">
        <div class="row">
            @if (session('success'))
                <div class="ui positive message">
                    <i class="close icon"></i>
                    <div class="header">
                        You have successfully {{ session('success') }}.
                    </div>
                </div>
                <br>
            @endif
            @foreach ($errors->all() as $error)
                <div class="ui negative message">
                    <i class="close icon"></i>
                    <div class="header">
                        {{ $error }}
                    </div>
                </div>
                <br>
            @endforeach
        </div>
        @if(session('head_of_faculty') == 'all')
            {!! Form::open(['url' => route('department.jobposition'), 'method' => 'get', 'role' => 'form', 'id' => 'formFilterByJobPosition', 'class' => 'ui large form']) !!}
            <div class="fields">
                <div class="field">
                    <select name="filter" id="buttonFilterByJobPosition">
                        <option value="all">All Job Position</option>
                        @foreach($filterForRole as $jobposition)
                            <option value="{{ $jobposition->id }}"
                                    @if (!empty(Request::input('filter')) && Request::input('filter') == $jobposition->id) selected @endif>
                                {{ $jobposition->name }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
            <span></span>
            {!! Form::close() !!}

            <div class="ui large button insertJobPosition">Insert Job Position</div>

        @else
            <form class="ui large form">
                <div class="fields">
                    <div class="field">
                        <select name="filter" disabled>
                            <option>{{ $filterForRole->first()->name }}</option>
                        </select>
                    </div>
                </div>
            </form>
        @endif
    </div>
    <br>
    <div class="ui middle aligned stackable container" style="padding: 0 0 8rem 0;">

        {{-- Job Position List --}}
        <h3 class="ui horizontal divider header">
            List of Job Position
        </h3>
        <div class="row">
            <table class="ui small table">
                <thead>
                <tr>
                    <th class="four wide">Job Position Name</th>
                    <th class="four wide">Degrees</th>
                    <th class="four wide"></th>
                </tr>
                </thead>
                <tbody>
                @foreach ($jobpositions as $jobposition)
                    <tr>
                        <td>{{ $jobposition->name }}</td>
                        <td>
                            <div class="ui bulleted list">
                                @foreach ($jobposition->jobpositiondegrees as $d)
                                    <div class="item">{{ $d->degree->name }}</div>
                                @endforeach
                            </div>
                        </td>
                        <td>
                            <a class="ui small button blue updateJobPosition"
                               href="{{ route('department.jobposition.edit', ['jobposition' => $jobposition->id]) }}">Update
                                Mapping</a>
                            @if(session('head_of_faculty') == 'all')
                                <div class="ui small button red deleteJobPosition" data-id="{{ $jobposition->id }}"
                                     data-name="{{ $jobposition->name }}">Delete
                                </div>
                                <form action="{{ route('department.jobposition.delete', ['id'=>$jobposition->id]) }}"
                                      method="POST"
                                      id="formDeleteJobPosition-{{ $jobposition->id }}">{{ csrf_field() }}</form>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        {{-- End of Job Position List --}}


    </div>

    <div class="ui modal insertJobPosition" style="overflow: auto;">
        <div class="header">Insert JobPosition</div>
        <div class="content">
            {!! Form::open(['url' => route('department.jobposition.insert'), 'method' => 'post', 'id' => 'formInsertJobPosition', 'role' => 'form', 'class' => 'ui large form']) !!}
            <div class="ui">
                <div class="field">
                    <div class="ui left icon input">
                        {{-- Insert Job Position --}}
                        <input type="text" name="name" required placeholder="Job Position Name" value="{{old('name')}}">
                        <i class="building icon"></i>
                    </div>
                </div>

                <div class="ui fluid large blue submit button">Insert</div>
            </div>
            <div class="ui error message" id="error message insert job position"></div>
            {!! Form::close() !!}
        </div>
    </div>

    <div class="ui small modal confirmation deleteJobPosition">
        <div class="header">Confirmation</div>
        <div class="content">
            <p id="contentApllicant-deleteJobPosition-name"></p>
        </div>
        <div class="actions">
            <div class="ui approve green button">Yes</div>
            <div class="ui cancel red button">No</div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            let jobPositionId = "";
            let degreeId = "";

            // Insert Job Position Modal
            $('.ui.button.insertJobPosition').click(function (e) {
                $('.ui.modal.insertJobPosition').modal('show');
            });
            $('#formInsertJobPosition').form({
                fields: {
                    name: ['empty'],
                }
            });

            //Delete Job Position Modal
            $('.ui.button.deleteJobPosition').click(function () {
                let jobPositionName = $(this).data("name");
                jobPositionId = $(this).data("id");
                document.getElementById("contentApllicant-deleteJobPosition-name").innerHTML = "Are you sure you want to delete job position " + jobPositionName + " ?<br>WARNING: This will also delete all the degrees within this job position!";
            });
            $('.ui.small.modal.confirmation.deleteJobPosition').modal("setting", {
                blurring: true,
                onApprove: function () {
                    $('#formDeleteJobPosition-' + jobPositionId).submit();
                }
            }).modal('attach events', '.ui.button.deleteJobPosition', 'show');


            //Delete Degree Modal
            $('.ui.button.deleteDegree').click(function () {
                let degreeName = $(this).data("name");
                degreeId = $(this).data("id");
                document.getElementById("contentApllicant-deleteDegree-name").innerHTML = "Are you sure you want to delete degree " + degreeName + " ?";
            });

            $('.ui.small.modal.confirmation.deleteDegree').modal("setting", {
                blurring: true,
                onApprove: function () {
                    $('#formDeleteDegree-' + degreeId).submit();
                }
            }).modal('attach events', '.ui.button.deleteDegree', 'show');

            // Filter By Job Position
            $('#buttonFilterByJobPosition').change(function (e) {
                $('#formFilterByJobPosition').submit();
            });
        });
    </script>
@stop
 