@extends ('layout.master')

@section ('title', 'BINUS')

@section('content')


@if (strcasecmp(Auth::user()->role, "Company") == 0)
	@include('partial/headerCompany')  
@elseif (strcasecmp(Auth::user()->role, "Department") == 0) 
	@include('partial/headerDepartment')   
@elseif (strcasecmp(Auth::user()->role, "Student") == 0)
	 @include('partial/headerStudent')   
@endif

  <div class="ui vertical stripe segment">
    <div class="ui center aligned stackable grid container">
      <div class="left aligned seven wide column">
      	{!! Form::open(['url' => 'submitpassword', 'method' => 'post', 'id' => 'formInput', 'role' => 'form', 'class' => 'ui large change form']) !!}
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="ui">

              <div class="field">
                <div class="ui left icon input">  
                  <input type="password" name="NewPassword" id="idNew" placeholder="Enter your new password">
                	<i class="lock icon"></i>
                </div>
              </div>
              <div class="field">
                <div class="ui left icon input">  
                  <input type="password" name="ConfirmationPassword" id="idConfirm" placeholder="Enter your confirmation password">
                  <i class="lock icon"></i>
                </div>
              </div>

              <div class="ui fluid large blue submit button">Submit</div>

            </div>
            <div class="ui error message"></div>
			@if (session('error'))
			    <div class="ui red message"> {{ session('error') }}</div>       
			@endif
			@if (session('fyi'))
			    <div class="ui green message"> {{ session('fyi') }}</div>       
			@endif
			@if($errors->first() != null)
			  <div class="ui red message">{{$errors->first()}}</div>
			@endif
          {!! Form::close() !!}
	  </div>
    </div>
  </div>
@stop