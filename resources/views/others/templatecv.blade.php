<html>
<head>
    <title>Curriculum Vitae</title>
    <style type="text/css">
        body {
            margin: 0;
            padding:25px;
            font-family: "Helvetica";
        }
        a{
            text-decoration: none;
            font-weight: bold;
            border-radius: 4px;
            border: 1px solid #cccccc;
            padding: 10px;
        }
    </style>
</head>
<body>
    <h2>Personal Information</h2>
    <table cellpadding="12">
        <tr>
            <td width="225px">
                @if(empty($path))
                <img style="max-height: 400px; max-width: 200px;" src="{{ url('/') }}/images/{{ $student->studentid }}">
                @else
                <img style="max-height: 400px; max-width: 200px;" src="{{ $path }}">
                @endif
            </td>
            <td>
                Name: {{ $student->name }}<br><br>
                Date of Birth: {{ $student->dob }}<br><br>
                Sex: {{ $student->sex }}<br><br>
                Address: {{ $student->address }}<br><br>
                Mobile: {{ $student->phone }}<br><br>
                Email: {{ $student->email }}<br><br>
                Program: {{ $student->program->name }}<br><br>
                Major: {{ $student->major }}<br><br>
            </td>
        </tr>
    </table>

    @if($formal->count() > 0)
    <br><br>
    <h2>Formal Education</h2>
    <table width="100%" border="1">
    @foreach($formal as $f)
        <tr>
            <td width="150">{{ $f->startdate }} - {{ $f->enddate }}</td>
            <td>{{ $f->description }}</td>
        </tr>
    @endforeach
    </table>
    @endif

    @if($informal->count() > 0)
    <br><br>
    <h2>Informal Education</h2>
    <table width="100%" border="1">
    @foreach($informal as $f)
        <tr>
            <td width="150">{{ $f->startdate }} - {{ $f->enddate }}</td>
            <td>{{ $f->description }}</td>
        </tr>
    @endforeach
    </table>
    @endif

    @if($organization->count() > 0)
    <br><br>
    <h2>Organization and Working Experience</h2>
    <table width="100%" border="1">
    @foreach($organization as $f)
        <tr>
            <td width="150">{{ $f->startdate }} - {{ $f->enddate }}</td>
            <td>
                At: {{ $f->description }}<br>
                As: {{ $f->role }}<br>
                <u>Job Description:</u>
                <?php $tagsdesc = explode("\n", $f->jobdesc); ?>
                @foreach($tagsdesc as $key)
					{{ $key }}<br>
				@endforeach
            </td>
        </tr>
    @endforeach
    </table>
    @endif

    @if($award->count() > 0)
    <br><br>
    <h2>Achievement / Awards</h2>
    <table width="100%" border="1">
    @foreach($award as $f)
        <tr>
            <td width="150">{{ $f->startdate }}</td>
            <td>{{ $f->description }}</td>
        </tr>
    @endforeach
    </table>
    @endif

    @if($skill->count() > 0)
    <br><br>
    <h2>Computer / Special Skill</h2>
    <img src="{{ $image }}" style="width: 100%; height: 100%;">
    @endif

    @if($language->count() > 0)
    <br><br>
    <h2>Language Skill</h2>
    <table width="100%" border="1">
    @foreach($language as $f)
        <tr>
            <td>{{ $f->name }}</td>
            <td>{{ $f->status }}</td>
        </tr>
    @endforeach
    </table>
    @endif

    @if($soft->count() > 0)
        <br><br>
        <h2>Soft Skill</h2>
        <table width="100%" border="1">
            @foreach($soft as $s)
                <tr>
                    <td>{{ $s->name }}</td>
                </tr>
            @endforeach
        </table>
    @endif

    @if($ip->count() > 0)
    <br><br>
    <h2>GPA History</h2>
    <table width="100%" border="1">
        <tr>
            <td>Semester</td>
{{--            <td>IPS</td>--}}
            <td>GPA</td>
        </tr>
    @foreach($ip as $f)
        <tr>
            <td>{{ $f->semester }}</td>
{{--            <td>{{ $f->ips }}</td>--}}
            <td>{{ $f->ipk }}</td>
        </tr>
    @endforeach
    </table>
    @endif

    @if($subs->count() > 0)
    <br><br>
    <h2>Subject Grade</h2>
    <table width="100%" border="1">
        <tr>
            <td>Course Code</td>
            <td>Course Name</td>
            <td>Grade</td>
        </tr>
    @foreach($subs as $f)
        <tr>
            <td>{{ $f->code }}</td>
            <td>{{ $f->name }}</td>
            <td>{{ $f->grade }}</td>
        </tr>
    @endforeach
    </table>
    @endif
</body>
</html>
