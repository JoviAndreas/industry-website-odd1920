@extends ('layout.master')

@section ('title', 'BINUS')

@section('content')

@if (strcasecmp(Auth::user()->role, "Company") == 0)
	@include('partial/headerCompany')
@elseif (strcasecmp(Auth::user()->role, "Department") == 0)
	@include('partial/headerDepartment')
@elseif (strcasecmp(Auth::user()->role, "Student") == 0)
	 @include('partial/headerStudent')
@endif


  <div class="ui vertical stripe segment" style="padding-bottom: 0px;">
    <div class="ui center aligned stackable grid container">
      <div class="left aligned ten wide column">
	    <div class="ui blue segment">
	      	<div class="ui grid" style="padding: 3%;">
	      		<div class="row">
	      			<div class="three wide column"><img class="ui top aligned tiny image" src="{{ url('images/'.$data->senderid) }}" onerror="this.src='{{ URL::asset('assets/images/noimage.jpg') }}'"></div>
		      		<div class="thirteen wide column">
		      			<div class="row" style="margin-top: 1%;margin-bottom: 1%;">
		      				<h3>@if (isset($inisent)) To : @endif{{ $data->name }}</h3>
		      			</div>
		      			<div class="row">
		      				@if (strlen($data->subject) > 34)
		      					<h2>{{ substr($data->subject,0,34).'...' }}</h2>
		      				@else
		      					<h2>{{ $data->subject }}</h2>
		      					@if ($data->subject == 'Interview Invitation')
		      						as {{ $job->name }}
		      					@endif
		      				@endif
		      			</div>
		      		</div>
	      		</div>
	      		@if ($data->type == 'mailbox')
		      		<div class="row" style="padding-left: 2%;padding-right: 2%">
		      			<textarea name="txtMessage" id="idMessage">
		      			<?php
		      				echo nl2br($data->message);
		      			?>
		      			</textarea>
		      		</div>
		      		@if (!isset($inisent))
		      		<div class="right floated left aligned three wide column">
					    <a href="{{ url('/') }}/reply/{{ $data->senderid }}/{{ $data->token }}"><div class="ui button">Reply</div></a>
				  	</div>
				  	@endif
	      		@else
	      			<div class="row" style="padding-left: 2%;padding-right: 2%">
		      			<h4>Dear {{ $interview->name }},
		      			<br/><br/>

						{{ $data->name }} would like to invite you for interview appointment.<br/>
						<table>
							<tr>
								<td>Place</td>
								<td>&nbsp;:&nbsp;</td>
								<td>{{ $interview->location }}</td>
							</tr>
							<tr>
								<td>Date and Time</td>
								<td>&nbsp;:&nbsp;</td>
								<td>{{ date("d M Y", strtotime($interview->date)) }},&nbsp;{{ $interview->time }}</td>
							</tr>
							<tr>
								<td>Phone</td>
								<td>&nbsp;:&nbsp;</td>
								<td>{{ $interview->phone }}</td>

							</tr>
							<tr>
								<td>Contact</td>
								<td>&nbsp;:&nbsp;</td>
								<td>{{ $interview->pic }}</td>
							</tr>
						</table>
						</h4>
						@if ($interview->status == 'waiting' && $company_user_status == 'active' && $data->status_recruitment == 'process')
		      			<div style="width: 100%;">
		      				{!! Form::open(['url' => 'acceptInterview', 'method' => 'post', 'id' => 'acceptInterviewform', 'role' => 'form', 'class' => 'ui large form', 'style' => 'display:inline-block;']) !!}
						      	<div class="ui primary submit button">
						      		<i class="checkmark icon"></i>
						      		Accept
						      	</div>
						      	<input type="hidden" name="tokenid" value="{{ $interview->token }}">
					      	{!! Form::close() !!}
					      	@if($rescheduled_count < $max_reschedule)
					      	{!! Form::open(['url' => 'resInterview', 'method' => 'post', 'id' => 'rescheduleInterviewform', 'role' => 'form', 'class' => 'ui large form', 'style' => 'display:inline-block;']) !!}
						      	<div class="ui primary submit button">
						      		<i class="move icon"></i>
						      		Reschedule
						      	</div>
					      		<input type="hidden" name="tokenid" value="{{ $interview->token }}">
				      		{!! Form::close() !!}
						    @else
						    	<div class="ui primary submit button disabled"><i class="move icon"></i>Reschedule</div>
						    	<div class="ui bottom warning message">
							  <i class="icon help"></i>
							  Cannot reschedule more than 2 times.
							</div>
						    @endif
						</div>
						@elseif(($interview->status != 'waiting' && $company_user_status == 'active') || ($data->status_recruitment != 'process'))
						<div style="width: 100%;">
					      	<div class="ui {{ ($interview->status == 'accept') ? 'green' : 'primary' }} disabled button">
					      		<i class="checkmark icon"></i>
					      		{{ ($interview->status == 'accept') ? 'Accepted' : 'Accept' }}
					      	</div>
					      	<div class="ui {{ ($interview->status == 'reschedule') ? 'brown' : 'primary' }} disabled button">
					      		<i class="move icon"></i>
					      		{{ ($interview->status == 'reschedule') ? 'Rescheduled' : 'Reschedule' }}
					      	</div>
						</div>
						@elseif($company_user_status == 'nonactive')
						<div style="width: 100%;">
							<div class="ui large orange label">This company is closed.</div>
						</div>
						@endif
						@if(!empty(session('err')))
						<div class="ui message">
							<p>{{ session('err') }}</p>
						</div>
						@endif
		      		</div>
	      		@endif
	      	</div>
	    </div>
	  </div>
    </div>
  </div>

<p>
	<input id="readOnlyOn" onclick="toggleReadOnly();" type="button" value="Make CKEditor read-only" style="display:none">
	<input id="readOnlyOff" onclick="toggleReadOnly( false );" type="button" value="Make CKEditor editable again" style="display:none">
</p>
<script type="text/javascript">
	CKEDITOR.config.readOnly = true;
    	CKEDITOR.config.removePlugins = 'undo,link,paste,';
</script>

@stop
