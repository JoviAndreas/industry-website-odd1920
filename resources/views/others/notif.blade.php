@extends ('layout.master')

@section ('title', 'BINUS')

@section('content')

	      
@if (strcasecmp(Auth::user()->role, "Company") == 0)
	@include('partial/headerCompany')  
@elseif (strcasecmp(Auth::user()->role, "Department") == 0)
	@include('partial/headerDepartment')   
@elseif (strcasecmp(Auth::user()->role, "Student") == 0)
	 @include('partial/headerStudent')   
@endif



  <br><br>
       <h4 class="ui horizontal divider header">Your Notification</h4>

  <div class="ui vertical segment">
    <div class="ui left aligned container" >

			<div class="ui list">
  
			@foreach($listnotif as $ln)
  <hr>
  <div class="item">
    <i class="{{$ln->icon}} icon"></i>
    <div class="left aligned content">
      <div class="header"><h3>{{$ln->subject}}</h3></div>
      <div class="description"><h4>{{$ln->message}}</h4></div>
     <div class="description"><h5>{{$ln->time}}</h5></div>
    </div>
  </div>

  	@endforeach
  <hr>



      </div>
  </div>
  
  </div>
 
@stop