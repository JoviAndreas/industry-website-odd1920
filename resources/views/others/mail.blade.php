@extends ('layout.master')

@section ('title', 'BINUS')

@section('content')


@if (strcasecmp(Auth::user()->role, "Company") == 0)
	@include('partial/headerCompany')  
@elseif (strcasecmp(Auth::user()->role, "Department") == 0)
	@include('partial/headerDepartment')   
@elseif (strcasecmp(Auth::user()->role, "Student") == 0)
	 @include('partial/headerStudent')   
@endif

  <div class="ui vertical stripe segment">

    <div class="ui center aligned stackable grid container">

      <div class="left aligned sixteen wide column">

      	<a href="{{ url('/') }}/create">
	      	<div class="ui primary button">
	      		<i class="add square icon"></i>
	      		Create
	      	</div>
      	</a>

      	<div class="ui top attached tabular inboxsents menu">
		  <a class="item active" data-tab="first">Inbox</a>
		  <a class="item" data-tab="second">Sent Mail</a>
		</div>

		<div class="ui bottom attached tab segment active" data-tab="first">
		  <table class="ui selectable celled table">
	      <thead>
		    <tr>
		      <th>Subject</th>
		      <th>From</th>
		      <th>Date</th>
		    </tr>
		  </thead>
		  <tbody>
		  	<?php foreach ($inboxs as $inbox) { ?>
			  	@if ($inbox->status == 'seen')
				    <tr style="background-color: #eeeeee;">
				@else
				    <tr class="positive">
				@endif
			  		<td class="seven wide"><a href="mailbox/{{ $inbox->token }}">{{ $inbox->subject}}</a></td>
			  		<!-- Maksimal 70 karakter -->
			  		<td class="six wide"><a href="mailbox/{{ $inbox->token }}">{{ $inbox->name}}</a></td>
			  		<td class="three wide"><a href="mailbox/{{ $inbox->token }}">{{ date( "d M Y H:i:s", strtotime($inbox->date) )}}</a></td>
			  	</tr>
		  	<?php } ?>
		  </tbody>
	      </table>
		</div>

		<div class="ui bottom attached tab segment" data-tab="second">
		  <table class="ui selectable celled table">
	      <thead>
		    <tr>
		      <th>Subject</th>
		      <th>To</th>
		      <th>Date</th>
		    </tr>
		  </thead>
		  <tbody>
		  	<?php foreach ($sents as $sent) { ?>
			  	@if ($sent->status == 'seen')
				    <tr>
				@else
				    <tr>
				@endif
			  		<td class="seven wide"><a href="mailboxs/{{ $sent->token }}">{{ $sent->subject }}</a></td>
			  		<!-- Maksimal 70 karakter -->
			  		<td class="six wide"><a href="mailboxs/{{ $sent->token }}">{{ $sent->name}}</a></td>
			  		<td class="three wide"><a href="mailboxs/{{ $sent->token }}">{{ date( "d M Y H:i:s", strtotime($sent->date) )}}</a></td>
			  	</tr>
		  	<?php } ?>
		  </tbody>
	      </table>
		</div>

	  </div>

    </div>

  </div>
@stop