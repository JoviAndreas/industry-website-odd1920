@extends ('layout.master')

@section ('title', 'BINUS')

@section('content')

@if (strcasecmp(Auth::user()->role, "Company") == 0)
	@include('partial/headerCompany')
@elseif (strcasecmp(Auth::user()->role, "Department") == 0)
	@include('partial/headerDepartment')
@elseif (strcasecmp(Auth::user()->role, "Student") == 0)
	 @include('partial/headerStudent')
@endif

  <div class="ui vertical stripe segment">
    <div class="ui center aligned stackable grid container">
      <div class="left aligned ten wide column">
	    <div class="ui blue segment">
	      	<div class="ui" style="padding: 3%;">
	      		{!! Form::open(['url' => 'sendMail', 'method' => 'post', 'id' => 'formInput', 'role' => 'form', 'class' => 'ui large form']) !!}
	      		<input type="hidden" name="_token" value="{{ csrf_token() }}">
				@if($for == 'reply')
				<input type="hidden" name="token_reply" value="{{ $token_reply }}">
				@endif
	      			<div class="field">
						<div class="ui @if ($for == 'reply') {{ 'disabled' }} @endif fluid search selection dropdown">
							<input type="hidden" name="recipient" @if ($for == 'reply') {{ 'value='.$data->userid.'' }} @elseif ($to_studentid != null) {{ 'value='.$to_studentid }} @endif />
							<i class="dropdown icon"></i>
							<div class="default text">Select Recipient</div>
							<div class="menu">
								@if ($for == 'reply')
									<div class="item" data-value="{{ $data->userid }}">{{ $data->name }}</div>
								@else
									@foreach ($data as $d)
										<div class="item" data-value="{{ $d->userid }}">{{ $d->name }}</div>
									@endforeach
								@endif
							</div>
						</div>
	              	</div>
	              	<div class="field">
	              		@if ($for == 'reply')
							<div class="ui input disabled">
								@if (substr($data->subject, 0, 5) != 'RE : ')
									<input type="text" name="txtSubject" id="idSubject" placeholder="Subject" value="RE : {{ $data->subject }}">
								@else
									<input type="text" name="txtSubject" id="idSubject" placeholder="Subject" value="{{ $data->subject }}">
								@endif
			                </div>
						@else
							<div class="ui input">
			                  <input type="text" name="txtSubject" id="idSubject" placeholder="Subject">
			                </div>
						@endif
	              	</div>
		      		<div class="field">
	                	<div class="ui left icon input">
	                    	<textarea rows="5" name="txtMessage" id="idMessage" placeholder="Type your message here.">@if(old('txtMessage')!=null){{ old('txtMessage') }}@endif</textarea>
	                	</div>
	              	</div>
		      		<div class="ui fluid large blue submit button">Send</div>
		      		<div class="ui error message"></div>
	      		{!! Form::close() !!}
	      		@if (count($errors) > 0)
				    <div class="ui error message">
				        <ul>
				            @foreach ($errors->all() as $error)
				                <li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>
				@endif
				@if(session('err'))
				    <div class="ui error message">
						<p>{{ session('err') }}</p>
				    </div>
				@endif
	      	</div>
	    </div>
	  </div>
    </div>
  </div>

<script>
    $('#idMessage').ckeditor();
    // $('.textarea').ckeditor(); // if class is prefered.
    CKEDITOR.config.width = '100%';	
</script>
@stop
