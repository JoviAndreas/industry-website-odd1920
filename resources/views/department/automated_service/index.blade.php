@extends ('layout.masterD')

@section ('title', 'BINUS')

@section('content')

    @include('partial/headerDepartment')

    <br><br>

    <div class="ui middle aligned stackable container" style="padding: 0 0 8rem 0;">
        <div class="row">
            @if (session('success'))
                <div class="ui positive message">
                    <i class="close icon"></i>
                    <div class="header">
                        You have successfully {{ session('success') }}.
                    </div>
                </div>
            @endif
            @foreach ($errors->all() as $error)
                <div class="ui negative message">
                    <i class="close icon"></i>
                    <div class="header">
                        {{ $error }}
                    </div>
                </div>
            @endforeach
        </div>
        <h3 class="ui horizontal divider header">Application Configuration</h3>
        <form action="{{ route('automated_service.update') }}" method="POST" class="row">
            {{ csrf_field() }}
            <table class="ui small table">
                <thead>
                <tr>
                    <th class="four wide">Configuration</th>
                    <th class="four wide">Status</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    @foreach($automatedServices as $key => $value)
                        @php
                            $automatedService = \App\AutomatedService::where('name', $key)->first();
                        @endphp
                        <td>{{ $value }}</td>
                        <td>
                            <div class="ui form">
                                <div class="fields">
                                    <div class="field">
                                        <div class="ui radio checkbox">
                                            <input type="radio" name="automated_service;{{ $key }}" value="running" {{ ($automatedService != null && $automatedService->status == 'running') ? 'checked' : '' }}>
                                            <label for="">On</label>
                                        </div>
                                    </div>
                                    <div class="field">
                                        <div class="ui radio checkbox">
                                            <input type="radio" name="automated_service;{{ $key }}" value="stopped" {{ ($automatedService == null || $automatedService->status != 'running') ? 'checked' : '' }}>
                                            <label for="">Off</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    @endforeach
                </tr>
                </tbody>
            </table>
            <button type="submit" class="ui small button blue">Save Configuration</button>
        </form>
    </div>

@endsection