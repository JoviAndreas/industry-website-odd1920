@extends ('layout.masterD')

@section ('title', 'BINUS')

@section('content')

@include('partial/headerDepartment')

<script type="text/javascript">

      function getData(jobid)
      {
        $("#apply_table").dataTable().fnDestroy();
        $.fn.dataTable.ext.search.push(
            function( settings, data, dataIndex ) {
                var min = parseFloat( $('#searchIPK').val()) || 0;
                var ipk = parseFloat( data[6] ) || 0; // use data for the ipk column

                if ( ipk >= min )
                {
                    return true;
                }
                return false;
            }
        );

        let tab = $('#apply_table').DataTable
        (
          {
            "ajax": {
              'url': '{{url("/")}}/getdata/'+jobid,
              complete: function(){
                $('.ui.apply.large.modal').modal('show');
                 $('#txtjob').val(jobid);
              }
            },
            "columns":
            [
              { "data": "NIM" },
              { "data": "Name" },
              { "data": "Sex" },
              { "data": "Phone" },
              { "data": "CV" },
              { "data": "Degree_Name" },
              { "name": "IPK", "data": "IPK" },
              { "data": "Action" }
            ]
          }
        );

        $('#searchIPK').keyup(function() {
          tab.draw();
        })
      }


      function clicked(element)
      {
        var value = element.value;
        if(element.checked == true)
        {

              $('#hidden_container').append('<input type="hidden" id="'+value+'" value="'+value+'" class="hid" name="check'+value+'" />');
        }else
        {
           document.getElementById(''+value).remove();
        }
      }

      function checkall(element)
      {

            $('.check').prop('checked', true);
            $('.check').each(function(i, obj) {

              if(document.getElementById(""+obj.value) == null)
              {
                 $('#hidden_container').append('<input type="hidden" id="'+obj.value+'" value="'+obj.value+'" class="hid" name="check'+obj.value+'" />');
              }
            });
      }

  </script>

<br><br>

<div class="ui container">

<div class="ui header">Job Apply</div>
@if (session('fyi'))
    <div class="ui positive message">
        {{ session('fyi') }}
    </div>
@endif
@if (session('err'))
    <div class="ui negative message">
        {{ session('err') }}
    </div>
@endif


{!! Form::open(['url' => 'apply', 'method' => 'get', 'enctype' => 'multipart/form-data', 'id' => 'formInput', 'role' => 'form', 'class' => 'ui large Searching form']) !!}
  <div class="field">
    <input type="text" name="Searching" id="idSearching" placeholder="Search" />
  </div>
  <button class="ui primary button" type="submit">Search</button>
{!! Form::close() !!}

@if(sizeof($company)>0)
<table class="ui striped selectable table" id="vacancy_table">
    <thead>
      <tr>
        <th class="three wide">Company</th>
        <th class="one wide"></th>
      </tr>
    </thead>
    <tbody>
    @foreach ($company as $c)
        <tr class="odd">
          <td colspan="2"><span>{{ $c->name }}</span><i class="caret down icon"></i></td>
        </tr>
        <tr class="even">
          <td colspan="4">
          <div class="ui container">
           <table class="ui basic padded table {{ $c->id }}" id="jobTable">
               <thead>
                <tr>
                  <th class="two wide">Position</th>
                  <th class="one wide">Seat</th>
                  <th class="two wide">Location</th>
                  <th class="one wide">Jobdesc</th>
                  <th class="two wide">Programs</th>
                  <th class="one wide">Deadline</th>
                  <th class="one wide">Duration</th>
                  <th class="one wide">Action</th>
              </tr>
            </thead>
            <tbody>
             @foreach($c->listJob as $job)
              <tr>
                <td>{{ $job->name }}</td>
                <td>{{ $job->quota }}</td>
                <td>{{ $job->location }}</td>
                <td>
                  @if($job->description != "")
                  <div class="ui button showModal{{$job->id}}">Desc
                  </div>
                  @else
                    -
                  @endif
                </td>
                <td>
                  <div class="ui bulleted list">
                    @foreach ($job->degrees as $d)
                    <div class="item">{{ $d->name }}</div>
                    @endforeach
                  </div>
                </td>
                <td>{{ $job->deadline }}</td>
                <td>{{ $job->duration }}</td>
                <td><div class="ui small button applyModal"
                onclick="getData({{$job->id}})">Apply</div> </td>
              </tr>
              @endforeach
             </tbody>
            </table>
              </div>
          </td>
        </tr>
    @endforeach
    </tbody>
</table>
<?php
  $search = (isset($Searching))?"&Searching=".$Searching:"";
?>

<div class="pagination">
<center>
 @if($currentPage > 1)
      <a class="ui blue circular label" href="
     {{ url('/apply').'?page=1'.$search}}"><<</a>

     <a class="ui blue circular label" href="
     {{ url('/apply').'?page='.($currentPage-1).$search}}"><</a>
 @endif

  @for($i=$currentPage-5; $i<$currentPage ;$i++)
    @if($i<=0)
      @continue;
    @endif
  <a class="ui blue circular label" href="
  {{ url('/apply').'?page='.$i.$search}}">{{$i}}</a>
  @endfor

  @for($i=$currentPage+1; $i<=$currentPage+5 ;$i++)
    @if($i>$totalPage)
      @continue;
    @endif
  <a class="ui blue circular label" href="{{ url('/apply').'?page='.$i.$search }}">{{$i}}</a>
  @endfor

  @if($currentPage< $totalPage)
     <a class="ui blue circular label" href="{{ url('/apply').'?page='.($currentPage+1).$search }}">></a>

      <a class="ui blue circular label" href="{{ url('/apply').'?page='.($totalPage).$search }}">>></a>
   @endif
</center>
 </div>

</div>
<br><br>

<!-- modal -->
@foreach ($company as $c)
  @foreach($c->listJob as $job)

<div class="ui small modal {{$job->id}}">
    <div class="header">Job Description</div>
<br><br>
        <div class="description">
        <div class="ui bulleted list">
          <?php
              $pieces = explode("\n", $job->description);
            ?>
            @foreach ($pieces as $piece)
              <div class="item" style="margin:0 20px">{{ $piece }}</div>
            @endforeach
          </div>
        </div>
 <br><br>
      <div class="actions">
    <div class="ui red deny button">
      Close
    </div>
  </div>
  </div>


<script>
    $('.showModal{{$job->id}}').click(function(e) {
        $('.small.modal.{{$job->id}}').modal('show');
        });
</script>
  @endforeach
@endforeach
@endif


<div class="ui apply large modal">
  <div class="header">Students</div>
  <div class="content">
  {!! Form::open(['url' => 'submitApplyJob', 'method' => 'post',   'class' => 'ui form']) !!}
  <input type="hidden" id="txtjob" name="txtjob" value=""/>
    <div class="ui basic vertical segment">
        <span>Min. IPK</span>
        <input type="text" id="searchIPK">
        <table class="ui single line basic padded table" id="apply_table" style="width: 100%;">
          <thead>
               <tr>
                  <td class="one wide">NIM</td>
                  <td class="two wide">Name</td>
                  <td class="one wide">Sex</td>
                  <td class="one wide">Phone</td>
                  <td class="one wide">CV</td>
                  <td class="two wide">Program</td>
                  <td class="one wide">IPK</td>
                  <td class="one wide"><a class="ui button" onclick="checkall(this)">Mark All</a></td>
              </tr>
          </thead>
        </table>
      </div>
      <div id="hidden_container"></div>
        <div class="actions">
          <button class="ui primary button" type="submit" name="applyStudent" >Apply Student(s)</button>
{{--            @if(\App\Department::isSuperAdmin())--}}
                <button class="ui secondary button" type="submit" name="applyAndApproveStudent" >Apply and Approve Student(s)</button>
{{--            @endif--}}
          <div class="ui negative button">Exit</div>
      </div>
   {!! Form::close() !!}
 </div>
</div>
@stop
