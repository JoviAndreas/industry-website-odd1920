@extends ('layout.masterD')

@section ('title', 'BINUS')

@section('content')

@include('partial/headerDepartment')

<div class="ui container">
  <div class="ui vertical stripe segment">
@if (session('success_message'))
    <div class="row">
        <div class="ui positive message">
          <i class="close icon"></i>
          <div class="header">
           {{session('fyi')}}
          </div>
    </div>
@endif

@if (!empty(session('errors')))
    <div class="row">
        <div class="ui negative message">
          <i class="close icon"></i>
          <div class="header">
           {{ session('errors') }}
          </div>
    </div>
@endif

<center><h2>Approval Request List (For Dual Program)</h2></center>
<table class="ui padded table" id="approval_request_history">
  <thead>
    <tr>
      <th>NIM</th>
      <th>Name</th>
      <th>Program</th>
      <th></th>
    </tr>
  </thead>
   <tbody>
      @foreach($approval_requests as $request)
      <tr class="odd" style="cursor: pointer;">
        <td><a href="{{url('/')}}/profile/{{ $request->nim }}" target="_blank">{{ $request->nim }}</a></td>
        <td>{{ $request->student_name }}</td>
        <td>{{ $request->degree_name }}</td>
        <td><div class="arrow" style="background:transparent url('{{ URL::asset('assets/images/arrows.png') }}') no-repeat scroll 0px -16px; width:16px; height:16px; display:block;"></div></td>
      </tr>
      <tr class="even" style="display: none;">
          <td colspan="4">
              <table class="ui table">
                    <thead>
                        <tr>
                            <th>Company Name</th>
                            <th>Job Name</th>
                            <th>Job Description</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($request->list_recruitments as $j)
                        <tr>
                            <td>{{ $j->company_name }}</td>
                            <td>
                                <a href="{{url('/')}}/job_detail/{{ $j->job_id }}" target="_blank">{{ $j->job_name }}</a>
                            </td>
                            <td>{{ $j->job_description }}</td>
                            <td>
                                <a href="{{url('/')}}/approve_approval_request/{{ $j->request_id }}" onclick="return confirm('Are you sure you want to approve this request ?')"><div class="ui primary button">Approve</div></a>
                                <a href="{{url('/')}}/reject_approval_request/{{ $j->request_id }}" onclick="return confirm('Are you sure you want to reject this request ?')"><div class="ui primary button">Reject</div></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
              </table>
          </td>
      </tr>
      @endforeach
   </tbody>
</table>

<br><br>

<center><h2>Approval Request History (For Dual Program)</h2></center>
<table class="ui padded table" id="approval_request_table">
  <thead>
    <tr>
      <th class="two wide">NIM</th>
      <th class="two wide">Name</th>
      <th class="two wide">Program</th>
      <th class="two wide">Company Name</th>
      <th class="two wide">Job</th>
      <th class="four wide">Status</th>
      <th class="two wide">Change Approval Status</th>
    </tr>
  </thead>
   <tbody>
      @foreach($approval_requests_history as $request)
      <tr>
        <td><a href="{{url('/')}}/profile/{{ $request->nim }}" target="_blank">{{ $request->nim }}</a></td>
        <td>{{ $request->student_name }}</td>
        <td>{{ $request->degree_name }}</td>
        <td>{{ $request->company_name }}</td>
        <td><a href="{{url('/')}}/job_detail/{{ $request->job_id }}" target="_blank">{{ $request->job_name }}</a></td>
        <td>
            <strong>{{ ucfirst($request->status) }}</strong><br>
            <br>
            Approved On: {{ $request->approved_on }}<br>
            Approved By: {{ $request->approved_by }}<br>
            <br>
            Rejected On: {{ $request->rejected_on }}<br>
            Rejected By: {{ $request->rejected_by }}<br>
        </td>
        <td>
          <a href="{{url('/')}}/approve_approval_request_history/{{ $request->request_id }}" onclick="return confirm('Are you sure you want to approve this request ?')"><div class="ui primary button">Approve</div></a>
          <a href="{{url('/')}}/reject_approval_request_history/{{ $request->request_id }}" onclick="return confirm('Are you sure you want to reject this request ?')"><div class="ui primary button">Reject</div></a>
          <a href="{{url('/')}}/pending_approval_request_history/{{ $request->request_id }}" onclick="return confirm('Are you sure you want to update this request to become pending ?')"><div class="ui orange button">Pending</div></a>
          <a href="{{url('/')}}/delete_approval_request_history/{{ $request->request_id }}" onclick="return confirm('Are you sure you want to delete this request ?')"><div class="ui red button">Delete</div></a>
        </td>
      </tr>
      @endforeach
   </tbody>
</table>

</div>

<br><br>

<script>
$("tr.odd").click(function(){
    $(this).next(".even").toggle();
});
</script>

@stop
