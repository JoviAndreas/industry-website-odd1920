@extends ('layout.masterD')

@section ('title', 'BINUS')

@section('content')

@include('partial/headerDepartment')

<div class="ui container">
<br><br>
<center>
@if (session('err'))
    <div class="ui red message">
        {{ session('err') }}
    </div>
    <br><br>
@endif
@if($errors->first() != null)
  	<div class="ui red message">{{ $errors->first() }}</div>
@endif
@if(session('fyi') )
	<div class="ui positive message">
		{{ session('fyi') }}
	</div>
@endif

<h2>Edit Description</h2>
<br><br>
<label>Choose Type</label>
<br>
<div class="ui selection dropdown">
	<input type="hidden" name="role">
	<i class="dropdown icon"></i>
	<div class="default text">---</div>
	<div class="menu">
	@foreach($data as $d)
		<div class="item" data-value="{!! $d->id !!}" >{{$d->type}}</div>
	@endforeach
	</div>
</div>

<br><br>
{!! Form::open(['url' => 'submitEditDesc', 'method' => 'post', 'enctype' => 'multipart/form-data', 'id' => 'formInput', 'role' => 'form', 'class' => 'ui large form']) !!}
<div class="field">
	<input type="hidden" id="descId" value="" name="id">
	<input type="text" name="desc" id="descDesc" placeholder="Set Description" />
</div>
<button class="ui primary button" type="submit">Update</button>
{!! Form::close() !!}
<script>
$(document).ready(function(){
	let oldDescriptions = {!! $data->toJson() !!};
	
	$('.ui.dropdown').dropdown({
		onChange: function()
		{
			let id = $(this).dropdown('get value');
			let oldDescription = oldDescriptions.filter((d) => {return d.id == id})[0].description;
			$('#descId').val(id);
			$("#descDesc").val(oldDescription);
		}
	});
});
</script>
@stop
