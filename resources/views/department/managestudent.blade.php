@extends ('layout.masterD')

@section ('title', 'BINUS')

@section('content')

    @include('partial/headerDepartment')

    <br><br>

    <div class="ui container">

{{--        <div class="ui segment" style="padding: 10px;">--}}
{{--            --}}{{--    {!! Form::open(['url' => 'submitByExcelStudent', 'enctype' => 'multipart/form-data','method' => 'post', 'role' => 'form', 'class' => 'ui large form']) !!}--}}
{{--            --}}{{--    <input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
{{--            --}}{{--    <div class="field">--}}
{{--            --}}{{--      <input type="file"  name="fileexcel">--}}
{{--            --}}{{--    </div>--}}
{{--            --}}{{--    <div class="field">--}}
{{--            --}}{{--      <button class="ui primary button">Submit</button>&nbsp;<a href="{{ asset('assets/template/Template_Insert_Student.xlsx') }}"/>Download Template here</a>--}}
{{--            --}}{{--    </div>--}}
{{--            --}}{{--    {!! Form::close() !!}--}}
{{--        </div>--}}

        <div class="ui middle aligned stackable grid container">
            @if (session('fyi'))
                <div class="row">
                    @if (session('fyi') == 'Berhasil')
                        <div class="ui positive  message">
                            <i class="close icon"></i>
                            <div class="header">
                                You have successfully delete.
                            </div>
                        </div>
                    @else
                        <div class="ui positive  message">
                            <i class="close icon"></i>
                            <div class="header">
                                {{ session('fyi') }}
                            </div>
                        </div>
                    @endif
                </div>
            @endif
            @if($errors->first() != null)
                <div class="row">
                    <div class="ui negative message">
                        <p>{{$errors->first()}}</p>
                    </div>
                </div>
            @endif
            @if(session('arrSame'))
                <div class="row">
                    <div class="ui negative message">
                        <p>Some Students already Exist!</p>
                        @foreach(session('arrSame') as $d)
                            <p>{{ $d}}</p>
                        @endforeach
                    </div>
                </div>
            @endif
            @if(session('err'))
                <div class="row">
                    <div class="ui negative message">
                        <p>{{ session('err') }}</p>
                    </div>
                </div>
            @endif
        </div>
        <br>
        <div class="ui basic segment">
            <div class="row">
                {!! Form::open(['url' => 'manageStudent', 'method' => 'get', 'enctype' => 'multipart/form-data', 'id' => 'formInput', 'role' => 'form', 'class' => 'ui large Searching form']) !!}
                <div class="field">
                    <input type="text" name="Searching" id="idSearching" placeholder="Search" value="{{$Searching}}"/>
                </div>
                <button class="ui primary button" type="submit">Search</button>
                {!! Form::close() !!}
            </div>
            <div class="row">
                {{--      <div class="ui primary large button insertStudent">Insert</div>--}}
                <a class="ui brown large button" href="{{ url('/') }}/exportStudentData">Export</a>
            </div>
            <br>
            <div class="row">
                <div class="column">
                    <div class="ui floating labeled icon dropdown button">
                        <input type="hidden" id="track_filter" value="">
                        <i class="filter icon"></i>
                        <span class="text">Filter By Track</span>
                        <div class="menu">
                            <div class="item" data-value="">All Track</div>
                            @foreach ($tracks as $track)
                            <div class="item" data-value="{{ ucfirst($track->track) }}">{{ ucfirst($track->track) }}</div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <table class="ui padded table" id="student_table">
                    <thead>
                    <tr>
                        <th class="one wide">NIM</th>
                        <th class="three wide">Name</th>
                        <th class="two wide">Date of Birth</th>
                        <th class="one wide">School</th>
                        <th class="three wide">Program</th>
                        <th class="one wide">Major</th>
                        <th class="one wide">Campus</th>
                        <th class="one wide">Student Track</th>
                        @if(\App\Department::isSuperAdmin())
                            <th class="two wide"></th>
                        @endif
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($Students as $student)
                        <tr>
                            <td><a href="{{url('/')}}/profile/{{ $student->studentid }}">{{ $student->studentid }}</a>
                            </td>
                            <td>{{ $student->name }}</td>
                            <td>{{ $student->dob }}</td>
                            <td>{{ $student->school }}</td>
                            <td>{{ $student->degree_name }}</td>
                            <td>{{ $student->major }}</td>
                            <td>{{ $student->campus }}</td>
                            <td>{{ ucfirst($student->track) }}</td>
                            @if(\App\Department::isSuperAdmin())
                                <td><a href="{{url('/')}}/deleteStudent/{{ $student->studentid }}"
                                       onclick="return confirm('Are you sure delete this student ?')">
                                        <div class="ui primary button">Delete</div>
                                    </a></td>
                                <td>
                                    <a href="{{url('/')}}/finishStudent/{{ $student->studentid }}" onclick="return confirm('By clicking this button, you will mark this student\'s internship status to finished, making them available to reapply to another company. Are you sure?')">
                                        <div class="ui warning button">Finish</div>
                                    </a>
                                </td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>

    {{--<div class="ui modal insertStudent" style="overflow: auto;">--}}
    {{--  <div class="header">Insert Student</div>--}}
    {{--  <div class="content">--}}
    {{--    {!! Form::open(['url' => 'insertStudent', 'method' => 'post', 'id' => 'formInput', 'role' => 'form', 'class' => 'ui large insert student form']) !!}--}}
    {{--    --}}
    {{--    <input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
    {{--    --}}
    {{--    <div class="ui">--}}
    {{--      <div class="field">--}}
    {{--        <div class="ui left icon input">--}}
    {{--          <input type="text" name="txtNIM" required id="idNIM" placeholder="NIM" value="@if(old('txtNIM')!=null){{old('txtNIM')}}@endif">--}}
    {{--          <i class="student icon"></i>--}}
    {{--        </div>--}}
    {{--      </div>--}}
    {{--      --}}
    {{--      <div class="field">--}}
    {{--        <div class="ui left icon input">--}}
    {{--          <input type="text" name="txtName" required id="idName" placeholder="Name" value="@if(old('txtName')!=null){{old('txtName')}}@endif">--}}
    {{--          <i class="user icon"></i>--}}
    {{--        </div>--}}
    {{--      </div>--}}
    {{--      --}}
    {{--      <div class="field">--}}
    {{--        <div class="ui left icon input">--}}
    {{--          <input class="dobStudent" type="text" id="idDOB" name="txtDOB" placeholder="Date of Birth" value="@if(old('txtDOB')!=null){{old('txtDOB')}}@endif">--}}
    {{--          <i class="calendar icon"></i>--}}
    {{--        </div>--}}
    {{--      </div>--}}
    {{--      --}}
    {{--      <div class="field">--}}
    {{--        <div class="ui left icon input">--}}
    {{--          <input type="text" name="txtSchool" required id="idSchool" placeholder="School [e.g School of Computer Science]" value="@if(old('txtSchool')!=null){{old('txtSchool')}}@endif">--}}
    {{--          <i class="student icon"></i>--}}
    {{--        </div>--}}
    {{--      </div>--}}
    {{--      --}}
    {{--      <div class="field">--}}
    {{--        <div class="ui left icon input">--}}
    {{--          <input type="text" name="txtDegree" required id="idDegree" placeholder="Program [e.g Computer Science]" value="@if(old('txtDegree')!=null){{old('txtDegree')}}@endif">--}}
    {{--          <i class="student icon"></i>--}}
    {{--        </div>--}}
    {{--      </div>--}}
    {{--      --}}
    {{--      <div class="field">--}}
    {{--        <div class="ui left icon input">--}}
    {{--          <input type="text" name="txtMajor" required id="idMajor" placeholder="Major [e.g Multimedia]" value="@if(old('txtMajor')!=null){{old('txtMajor')}}@endif">--}}
    {{--          <i class="student icon"></i>--}}
    {{--        </div>--}}
    {{--      </div>--}}
    {{--      --}}
    {{--      <div class="ui fluid large blue submit button">Insert</div>--}}
    {{--      --}}
    {{--    </div>--}}
    {{--    <div class="ui error message"></div>--}}
    {{--    --}}
    {{--    {!! Form::close() !!}--}}
    {{--  </div>--}}
    {{--</div>--}}
    <br><br><br>
    </div>

    <script>
        $.fn.dataTable.ext.search.push(
            function( settings, data, dataIndex) {
                let trackFilter = $('#track_filter').val();
                let track = data[9]; // use data for the track column
                console.log(trackFilter);
                if (track == trackFilter || trackFilter == '')
                {
                    return true;
                }
                return false;
            }
        );
        $(document).ready(function () {
            $('#student_table').DataTable();
            $('.ui.large.insert.student.form')
                .form({
                    fields: {
                        txtNIM: ['empty', 'length[10]', 'maxLength[10]'],
                        txtName: ['empty', 'maxLength[200]'],
                        txtDOB: ['empty']
                    }
                })
            ;


            $('.insertStudent').click(function (e) {
                $('.ui.modal.insertStudent').modal('show');
            });
        });
    </script>

@stop
