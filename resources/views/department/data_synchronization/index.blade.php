@extends ('layout.masterD')

@section ('title', 'BINUS')

@section('content')
    @include('partial/headerDepartment')
    <br><br>
    <div class="ui container">
        @if (session('success'))
            <div class="ui positive message transition">
                <i class="close icon"></i>
                <div class="header">
                    {{ session('success') }}
                </div>
            </div>
        @endif
        @if($errors->first() != null)
            <div class="ui error message">
                <i class="close icon"></i>
                <div class="header">
                    {{ $errors->first() }}
                </div>
{{--                <ul class="list">--}}
{{--                    <li></li>--}}
{{--                </ul>--}}
            </div>
        @endif
        <br>
    </div>
    <div class="ui container">
        <h3 class="ui horizontal divider header">Data Synchronization</h3>
        <div class="ui segment" style="padding: 10px;">
            <div class="ui header">Step 1 - Get Student Header from Binusmaya {{ \App\Semester::where('period', Auth::user()->active_period)->first()->description ?? '-' }}</div>
            <form action="{{ url('/department/data/student/insert/temp/header') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                {{-- <p>Temporary Student Count : {{ $studentTemp->sum('counts') }} student(s)</p> --}}
                <input type="radio" name="campus" value="kmg" id="kmgalsId">
                <label for="kmgalsId"  style="cursor:pointer" > Kemanggisan & Alam Sutera </label><br>
                <input type="radio" name="campus" value="jwc" id="jwcId">
                <label for="jwcId"  style="cursor:pointer"> JWC </label><br>
                <input type="radio" name="campus" value="bdg" id="bdgId">
                <label for="bdgId"  style="cursor:pointer"> Bandung </label><br>
                <input type="radio" name="campus" value="mlg" id="mlgId">
                <label for="mlgId"  style="cursor:pointer"> Malang </label><br>
                @if (count($studentTemp) == 0)
                    <p>No Student Header is Processed This Semester</p>
                @else
                    <p>Details:</p>
                    <ul>
                        @foreach ($studentTemp as $studentTrackDetail)
                            <li>{{ $studentTrackDetail->track }}: {{ $studentTrackDetail->counts }} student(s)</li>
                        @endforeach
                    </ul>
                @endif
                <input type="hidden" name="period" value="1910">
                <div class="field">
                    <button type="submit" class="ui primary button" onclick="this.innerHTML='<p>GETTING DATA...</p>'">Get Student Header</button>
                </div>
                <div class="ui divider"></div>
            </form>
{{--            <div class="ui header">Step 2 - Get Student Grades from Binusmaya</div>--}}
{{--            <form action="{{ url('/') }}" method="POST">--}}
{{--                <input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
{{--                <input type="hidden" name="period" value="1910">--}}
{{--                <div class="field">--}}
{{--                    <button type="submit" class="ui primary button">Get Grades</button>--}}
{{--                </div>--}}
{{--                <div class="ui divider"></div>--}}
{{--            </form>--}}
            <div class="ui header">Step 2 - Activate Student Header from Binusmaya {{ \App\Semester::where('period', Auth::user()->active_period)->first()->description ?? '-' }}</div>
            <form action="{{ url('/department/data/student/sync/temp/header') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <p>Student Count : {{ $students->sum('counts') }} student(s)</p>
                @if (count($students) == 0)
                    <p>No Student Header is Activated This Semester</p>
                @else
                    <p>Details:</p>
                    <ul>
                        @foreach ($students as $studentTrackDetail)
                            <li>{{ $studentTrackDetail->track }}: {{ $studentTrackDetail->counts }} student(s)</li>
                        @endforeach
                    </ul>
                @endif
                <input type="hidden" name="period" value="1910">
                <div class="field">
                    <button type="submit" class="ui primary button" onclick="this.innerHTML='<p>ACTIVATING...</p>'">Activate Student Header</button>
                </div>
                <div class="ui divider"></div>
            </form>
        </div>
    </div>
    <br><br>
@endsection