@extends ('layout.masterD')

@section ('title', 'BINUS')

@section('content')

@include('partial/headerDepartment')

<div class="ui container">
<br><br>
<center>
@if (session('err'))
    <div class="ui red message">
        {{ session('err') }}
    </div>
    <br><br>
@endif
@if($errors->first() != null)
  	<div class="ui red message">{{$errors->first()}}</div>
@endif
@if(session('fyi') )
	<div class="ui positive message">
		{{ session('fyi') }}
	</div>
@endif

<h2>List All Recruitment</h2>
<br><br>

<a class="ui button" href=" {{ url('/') }}/exportRecruitmentData">Export All Recruitment Data</a>

<br><br>

{!! Form::open(['url' => 'editRecruitment', 'method' => 'get', 'enctype' => 'multipart/form-data', 'id' => 'formInput', 'role' => 'form', 'class' => 'ui large Searching form']) !!}
<div class="field">
	<input type="text" name="Searching" id="idSearching" placeholder="Search by Student Name" />
</div>
<button class="ui primary button" type="submit">Search</button>
{!! Form::close() !!}

<table class="ui striped table" id="vacancy_table">
	<thead>
		<tr>
			<th class="three wide">NIM</th>
			<th class="three wide">Student Name</th>
			<th class="three wide">CV</th>
			<th class="three wide">Program</th>
			<th class="three wide"></th>
		</tr>
	</thead>
	<tbody>
		@foreach ($data as $d)
        <tr class="odd selectable">
	    	<td>{{ $d->studentid }}</td>
			<td>{{ $d->studentname }}</td>
			<td>
				@if($d->cv == 1)
					<a class="ui button" href="{{ url('/')}}/previewcvs/{{$d->studentid}}" target="_blank">Preview</a>
				@else
					CV not found!
				@endif
			</td>
			<td>
				<div>{{ $d->degree_name }}</div>
			</td>
			<td>
				<div class="arrow"></div>
			</td>
        </tr>
        <tr class="even">
			<td colspan="5">
				<div class="ui container">
               		<table class="ui line basic padded table {{ $d->studentid }}" id="jobTable">
                 		<thead>
							<tr>
								<th class="two wide">Job Name</th>
								<th class="one wide">Jobdesc</th>
								<th class="three wide">Company Name</th>
								<th class="two wide">Status</th>
								<th class="five wide">History</th>
								<th class="two wide">Action</th>
							</tr>
                		</thead>
                		<tbody>
                 		@foreach($d->listjob as $job)
                  			<tr class="
							{{$job->status=='approved' ? 'positive' :
  							($job->status=='rejected'?'negative' :
  							($job->status=='accepted'?'warning':
  							($job->status=='process'?'active':'')))}}">
                    			<td>
                                    {{ $job->jobname }}<br>
                                    Full Quota: {{ $job->full_quota }}<br>
                                    Current Quota: <span class="ui green label">{{ $job->quota }}</span>
                                </td>
								<td>
									<div class="ui button showModal{{$job->recid}}">Desc</div>
								</td>
								<td>{{ $job->companyname }}</td>
								<td>{{ $job->status }}</td>
								<td>
                                    Applied On: {{ $job->applied_on }}<br>
                                    <br>
                                    Processed On: {{ $job->processed_on }}<br>
                                    Processed By: {{ $job->processed_by }}<br>
                                    <br>
                                    Accepted On: {{ $job->accepted_on }}<br>
                                    Accepted By: {{ $job->accepted_by }}<br>
                                    <br>
                                    Approved On: {{ $job->approved_on }}<br>
                                    Approved By: {{ $job->approved_by }}<br>
                                    <br>
                                    Rejected On: {{ $job->rejected_on }}<br>
                                    Rejected By: {{ $job->rejected_by }}<br>
                                    @if($job->interview_data_count > 0)
                                    <br>
                                    <button class="ui primary button" id="btn_interview" recId="{{ $job->recid }}">See Interview Data</button>
                                    @endif
                                </td>
								<td>
	        						{!! Form::open(['url' => 'submitEditRec', 'method' => 'post', 'role' => 'form', 'class' => 'ui large rec form']) !!}
									<select class="ui dropdown" name="status">
	  									<option value="">Choose Status</option>
										@if($job->status != "waiting")
										<option value="waiting">Waiting</option>
										@endif
										@if($job->status != "process")
										<option value="process">Process</option>
										@endif
										@if($job->status != "approved")
										<option value="approved">Approved</option>
										@endif
										@if($job->status != "rejected")
										<option value="rejected">Rejected</option>
										@endif
									</select>
	         						<input type="hidden" name="recid" value="{{$job->recid}}"/>
	       							<button class="ui primary button" type="submit">Update</button>
	          						{!! Form::close() !!}
  								</td>
                  			</tr>
                  		@endforeach

                        @foreach($d->listjobdeleted as $job)
                  			<tr style="color: white; background-color: gray;">
                    			<td>
                                    {{ $job->jobname }}<br><br>
                                    @if(!empty($job->job_deleted_at))
                                    [Job Deleted]<br><br>
                                    @endif
                                    Full Quota: {{ $job->full_quota }}<br>
                                    Current Quota: <span class="ui green label">{{ $job->quota }}</span>
                                </td>
								<td>
									<div class="ui button showModal{{$job->recid}}">Desc</div>
								</td>
								<td>
                                    {{ $job->companyname }}<br><br>
                                    @if(!empty($job->company_deleted_at))
                                    [Company Deleted]
                                    @endif
                                </td>
								<td>
                                    <strong>Unapplied</strong><br><br>
                                    Old Status: {{ $job->status }}
                                </td>
								<td>
                                    Applied On: {{ $job->applied_on }}<br>
                                    <br>
                                    Processed On: {{ $job->processed_on }}<br>
                                    Processed By: {{ $job->processed_by }}<br>
                                    <br>
                                    Accepted On: {{ $job->accepted_on }}<br>
                                    Accepted By: {{ $job->accepted_by }}<br>
                                    <br>
                                    Approved On: {{ $job->approved_on }}<br>
                                    Approved By: {{ $job->approved_by }}<br>
                                    <br>
                                    Rejected On: {{ $job->rejected_on }}<br>
                                    Rejected By: {{ $job->rejected_by }}<br>
                                    <br>
                                    Unapplied On: {{ $job->unapplied_on }}<br>
                                    Unapplied By: {{ $job->unapplied_by }}<br>
                                    @if($job->interview_data_count > 0)
                                    <br>
                                    <button class="ui primary button" id="btn_interview" recId="{{ $job->recid }}">See Interview Data</button>
                                    @endif
                                </td>
								<td>

  								</td>
                  			</tr>
                  		@endforeach
                 		</tbody>
                	</table>
                  </div>
              </td>
            </tr>
			@endforeach
	</tbody>
</table>

<?php
$search = (isset($Searching))?"&Searching=".$Searching:"";
?>
<div class="pagination">
	@if($currentPage > 1)
		<a class="ui blue circular label" href="{{ url('/editRecruitment').'?page=1'.$search}}"><<</a>
		<a class="ui blue circular label" href="{{ url('/editRecruitment').'?page='.($currentPage-1).$search}}"><</a>
	@endif
	@for($i=$currentPage-5; $i<$currentPage ;$i++)
	    @if($i<=0)
			@continue;
	    @endif
		<a class="ui blue circular label" href="{{ url('/editRecruitment').'?page='.$i.$search}}">{{$i}}</a>
  	@endfor
	. . .
  	@for($i=$currentPage+1; $i<=$currentPage+5 ;$i++)
	    @if($i>$totalPage)
			@continue;
	    @endif
  		<a class="ui blue circular label" href="{{url('/editRecruitment').'?page='.$i.$search }}">{{$i}}</a>
	@endfor
  	@if($currentPage< $totalPage)
		<a class="ui blue circular label" href="{{url('/editRecruitment').'?page='.($currentPage+1).$search }}">></a>
		<a class="ui blue circular label" href="{{url('/editRecruitment').'?page='.($totalPage).$search }}">>></a>
   	@endif
</div>

</center>
<br><br>
</div>

@foreach($data as $d)
<script>
$(document).ready(function() {
    $('.ui.single.line.basic.padded.table.{{$d->studentid}}').DataTable({
        "paging": false,
        "info": false,
    	"searching": false
    });
});
</script>
@endforeach
<?php
$list = array();
foreach($data as $x)
{
	foreach($x->listjob	as $y)
	{
		array_push($list, $y);
	}
}
?>
@foreach ($list as $d)
<div class="ui small modal {{$d->recid}}">
	<div class="header">Job Description</div>
    <div class="description">
  	<div class="ui bulleted list">
    	<?php
          $pieces = explode("\n", $d->jobdesc);
        ?>
        @foreach ($pieces as $piece)
        	<div class="item">{{ $piece }}</div>
        @endforeach
    	</div>
    </div>
  	<div class="actions">
		<div class="ui red deny button">Close</div>
	</div>
</div>
<script>
$('.showModal{{$d->recid}}').click(function(e) {
	$('.small.modal.{{$d->recid}}').modal('show');
});
</script>
@endforeach

<script>
	$('#ApprovalTable').DataTable();
</script>

<div class="ui modal" id="modal_interview_history">
    <div class="header">List Interview History</div>
    <div class="content">
        <table class="ui basic padded table" id="interview_history_table">
            <thead>
                <tr>
                    <th>Location</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>PIC</th>
                    <th>Phone</th>
                    <th>Status</th>
                    <th>Created At</th>
                    <th>Updated At</th>
                </tr>
            </thead>
        <tbody>

        </tbody>
        </table>
    </div>
    <div class="actions">
        <div class="ui red deny button">Close</div>
    </div>
</div>

<script>
    var interview_history_table = $('#interview_history_table').DataTable({
            "columns": [
                { data: 'location' },
                { data: 'date' },
                { data: 'time' },
                { data: 'pic' },
                { data: 'phone' },
                { data: 'status' },
                { data: 'created_at' },
                { data: 'processed_on' }
            ],
            "ajax": "{{ url('/') }}/getRecruitmentInterviewData/0",
            "paging": false,
            "info": false,
            "searching": false
        });
    $(document).on('click', '#btn_interview', function(e) {
        var recId = e.target.attributes.recId.value;
        interview_history_table.ajax.url('{{ url("/") }}/getRecruitmentInterviewData/' + recId).load();
        $('#modal_interview_history').modal('show');
    });
</script>
@stop
