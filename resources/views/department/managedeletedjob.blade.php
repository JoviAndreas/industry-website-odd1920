@extends ('layout.masterD')

@section ('title', 'BINUS')

@section('content')

@include('partial/headerDepartment')

<br><br>
<div class="ui container">
    <div class="ui grid">
    @if (session('fyi'))
        <div class="row">
            <div class="ui positive message">
                <i class="close icon"></i>
                <div class="header">
                {{session('fyi')}}
                </div>
            </div>
        </div>
    @endif

    @if (!empty(session('errors')))
        <div class="row">
            <div class="ui negative message">
                <i class="close icon"></i>
                <div class="header">
                {{ session('errors') }}
                </div>
            </div>
        </div>
    @endif
    </div>

    <center><h2>Deleted Job</h2></center>
    <table class="ui padded table" id="deleted_job_table">
      <thead>
        <tr>
          <th class="two wide">Job ID</th>
          <th class="two wide">Company Name</th>
          <th class="four wide">Job Name</th>
          <th class="two wide">Job Status</th>
          <th class="four wide">Deleted At</th>
          <th class="two wide">Action</th>
        </tr>
      </thead>
       <tbody>
          @foreach($deleted_jobs as $d)
          <tr>
            <td>{{ $d->id }}</td>
            <td>{{ $d->company_name }}</td>
            <td>{{ $d->name }}</td>
            <td>{{ $d->job_status }}</td>
            <td>{{ $d->deleted_at }}</td>
            <td>
                @if($d->deleted_at > '2017-12-15 00:00:00')
              <a href="{{url('/')}}/undelete_job/{{ $d->id }}" onclick="return confirm('Are you sure you want to UNDELETE this job ?')"><div class="ui red button">Undelete</div></a>
              @endif
            </td>
          </tr>
          @endforeach
       </tbody>
    </table>

</div>

<script>
$(document).ready(function(){
    $('#deleted_job_table').DataTable();
});
</script>

@stop
