@extends ('layout.masterD')

@section ('title', 'BINUS')

@section('content')

@include('partial/headerDepartment')

<style>
  .ui.padded.segment{
    padding-top: 5px;
    padding-bottom: 5px;
    padding-left: 10px;
    padding-right: 10px;
  }
</style>

<br><br>

<div class="ui container">
<h2>Job Name: {{ $job->name }}</h2>
<strong>Company: {{ $company->name }}</strong> &nbsp; <a class="ui right labeled icon primary button" href="{{ url('/profilec/'.$company->companyid) }}">Go to Company Profile &nbsp; <i class="arrow right icon"></i></a>
<br><br>
<div class="ui grid">
  <div class="row">
    <div class="four wide column">
      Description :
    </div>
    <div class="twelve wide column">
      <div class="ui padded segment">
        <p>{{ $job->description }}</p>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="four wide column">
      Location :
    </div>
    <div class="twelve wide column">
      <div class="ui padded segment">
        <p>{{ $job->location }}</p>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="four wide column">
      Quota :
    </div>
    <div class="twelve wide column">
      <div class="ui padded segment">
        <p>{{ $job->quota }}</p>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="four wide column">
      Deadline :
    </div>
    <div class="twelve wide column">
      <div class="ui padded segment">
        <p>{{ Carbon\Carbon::parse($job->deadline)->format('d M Y') }}</p>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="four wide column">
      Duration :
    </div>
    <div class="twelve wide column">
      <div class="ui padded segment">
        <p>{{ $job->duration }} Months</p>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="four wide column">
      Start Date :
    </div>
    <div class="twelve wide column">
      <div class="ui padded segment">
        <p>{{ ($job->startdate == null || $job->startdate == '0000-00-00') ? '-' : Carbon\Carbon::parse($job->startdate)->format('d M Y') }}</p>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="four wide column">
      End Date :
    </div>
    <div class="twelve wide column">
      <div class="ui padded segment">
          <p>{{ ($job->enddate == null || $job->enddate == '0000-00-00') ? '-' : Carbon\Carbon::parse($job->enddate)->format('d M Y') }}</p>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="four wide column">
      Link Test :
    </div>
    <div class="twelve wide column">
      <div class="ui padded segment">
        <p>
          @if(!empty($job->linktest))
          <a href="//{{ $job->linktest }}">{{ $job->linktest }}</a>
          @else
          -
          @endif
        </p>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="four wide column">
      Programs :
    </div>
    <div class="twelve wide column">
      <div class="ui padded segment">
        <ul>
          @foreach($degrees as $d)
          <li>{{ $d->name }}</li>
          @endforeach
        </ul>
      </div>
    </div>
  </div>
  <!--
  <div class="row">
    <div class="four wide column">
      Technical Competencies :
    </div>
    <div class="twelve wide column">
      <div class="ui padded segment">
        <div class="ui checkbox" style="width: 100%;">
          <input type="checkbox" name="TC1" @if(!empty($TCarray['tc1']) && $TCarray['tc1']=='on') checked @endif>
          <label>User Requirement gathering</label>
        </div>

        <div class="ui checkbox" style="width: 100%;">
          <input type="checkbox" name="TC2" @if(!empty($TCarray['tc2']) && $TCarray['tc2']=='on') checked @endif>
          <label>Analysis of user requirements to find problems and solutions</label>
        </div>

        <div class="ui checkbox" style="width: 100%;">
          <input type="checkbox" name="TC3" @if(!empty($TCarray['tc3']) && $TCarray['tc3']=='on') checked @endif>
          <label>Application design according user requirements to solve problems</label>
        </div>

        <div class="ui  checkbox" style="width: 100%;">
          <input type="checkbox" name="TC4" @if(!empty($TCarray['tc4']) && $TCarray['tc4']=='on') checked @endif>
          <label>Interface & UX design of application</label>
        </div>

        <div class="ui checkbox" style="width: 100%;">
          <input type="checkbox" name="TC5" @if(!empty($TCarray['tc5']) && $TCarray['tc5']=='on') checked @endif>
          <label>Code and develop application according to the application design</label>
        </div>

        <div class="ui checkbox" style="width: 100%;">
          <input type="checkbox" name="TC6" @if(!empty($TCarray['tc6']) && $TCarray['tc6']=='on') checked @endif>
          <label>Database Design for the application</label>
        </div>

        <div class="ui checkbox" style="width: 100%;">
          <input type="checkbox" name="TC7" @if(!empty($TCarray['tc7']) && $TCarray['tc7']=='on') checked @endif>
          <label>Application testing (Quality Assurance)</label>
        </div>

        <div class="ui checkbox" style="width: 100%;">
          <input type="checkbox" name="TC8" @if(!empty($TCarray['tc8']) && $TCarray['tc8']=='on') checked @endif>
          <label>Application implementation / publishing</label>
        </div>

        <div class="ui checkbox" style="width: 100%;">
          <input type="checkbox" name="TC9" @if(!empty($TCarray['tc9']) && $TCarray['tc9']=='on') checked @endif>
          <label>Database administration</label>
        </div>

        <div class="ui checkbox" style="width: 100%;">
          <input type="checkbox" name="TC10" @if(!empty($TCarray['tc10']) && $TCarray['tc10']=='on') checked @endif>
          <label>Network infrastructure design</label>
        </div>

        <div class="ui checkbox" style="width: 100%;">
          <input type="checkbox" name="TC11" @if(!empty($TCarray['tc11']) && $TCarray['tc11']=='on') checked @endif>
          <label>Network administration</label>
        </div>

        <div class="ui checkbox" style="width: 100%;">
          <input type="checkbox" name="TC12" @if(!empty($TCarray['tc12']) && $TCarray['tc12']=='on') checked @endif>
          <label>Applying software development methodology / process</label>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="four wide column">
      Soft Skills :
    </div>
    <div class="twelve wide column">
      <div class="ui padded segment">
        <div class="ui checkbox" style="width: 100%;">
          <input type="checkbox" name="SS1" @if(!empty($SSarray['ss1']) && $SSarray['ss1']=='on') checked @endif>
          <label>Self Development</label>
        </div>

        <div class="ui checkbox" style="width: 100%;">
          <input type="checkbox" name="SS2" @if(!empty($SSarray['ss2']) && $SSarray['ss2']=='on') checked @endif>
          <label>Teamwork</label>
        </div>

        <div class="ui checkbox" style="width: 100%;">
          <input type="checkbox" name="SS3" @if(!empty($SSarray['ss3']) && $SSarray['ss3']=='on') checked @endif>
          <label>Communication</label>
        </div>

        <div class="ui checkbox" style="width: 100%;">
          <input type="checkbox" name="SS4" @if(!empty($SSarray['ss4']) && $SSarray['ss4']=='on') checked @endif>
          <label>Problem Solving and Decision Making</label>
        </div>

        <div class="ui checkbox" style="width: 100%;">
          <input type="checkbox" name="SS5" @if(!empty($SSarray['ss5']) && $SSarray['ss5']=='on') checked @endif>
          <label>Planning & Organizing</label>
        </div>

        <div class="ui checkbox" style="width: 100%;">
          <input type="checkbox" name="SS6" @if(!empty($SSarray['ss6']) && $SSarray['ss6']=='on') checked @endif>
          <label>Initiative & Enterprise</label>
        </div>
      </div>
    </div>
  </div>
  -->
</div>
<br><br>
</div>

@stop
