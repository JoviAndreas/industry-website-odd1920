@extends ('layout.masterD')

@section ('title', 'BINUS')

@section('content')

    @include('partial/headerDepartment')

    <br><br>

    @if (session('fyi'))
        <div class="row">
            <div class="ui positive message">
                <i class="close icon"></i>
                <div class="header">
                    {{session('fyi')}}
                </div>
            </div>
        </div>
    @endif

    @if($errors->first() != null)
        <div class="row">
            <div class="ui negative message">
                <p>{{$errors->first()}}</p>
            </div>
        </div>
    @endif

    @if(!empty(session('error')))
        <div class="row">
            <div class="ui negative message">
                <p>{{ session('error') }}</p>
            </div>
        </div>
    @endif

    <br><br>

    <div class="ui container">
        <h2>Current Account</h2>
        <table class="ui celled padded table">
            <thead>
            <tr>
                <th>UserID</th>
                <th>Name</th>
                <th>Email</th>
                <th>Default Password</th>
                <th>Campus</th>
                <th>Allowed Degrees</th>
                <th>Access Role</th>
                @if(Auth::user()->department()->first()->head_of_program == 'all')
                    <th></th>
                @endif
            </tr>
            </thead>
            <tbody>
            @foreach($departments as $d)
                <tr>
                    <td>{{ $d->userid }}</td>
                    <td>{{ $d->name }}</td>
                    <td>{{ $d->email }}</td>
                    <td><span style="background-color: black; color: #111111;">{{ $d->defaultpassword }}</span></td>
                    <td>{{ ($d->campus) ? $d->campus : 'All' }}</td>
                    <td>
                        <ul>
                            @foreach($d->access_roles as $h)
                                <li>{{ $h }}</li>
                            @endforeach
                        </ul>
                        <a class="ui primary button" href="{{ url('/') }}/changeHeadOfProgramData/{{ $d->userid }}">Change</a>
                    </td>
                    <td>{{ \App\Department::getAllAccessRoles()[$d->access_role] ?? '-' }}</td>
                    @if(Auth::user()->department()->first()->head_of_program == 'all')
                        <td>
                            @if($d->status == 'nonactive')
                                <a href="{{ url('/') }}/activeAccountDepartment/{{ $d->userid }}"
                                   class="ui green button">Active</a>
                            @else
                                <a href="{{ url('/') }}/inactiveAccountDepartment/{{ $d->userid }}"
                                   class="ui red button">Inactive</a>
                            @endif
                            <a href="{{ url('/') }}/updateDepartmentAccount/{{ $d->userid }}" class="ui blue button">Update</a>
                        </td>
                    @endif
                </tr>
            @endforeach
            </tbody>
        </table>

        <h2>Add New Account</h2>
        {!! Form::open(['url' => 'insertNewDepartmentAccount', 'method' => 'post', 'class' => 'ui form']) !!}
        <div class="field">
            <label>User ID</label>
            <input type="text" name="txt_userid" placeholder="User ID" value="{{ old('txt_userid') }}" required>
        </div>
        <div class="field">
            <label>Name</label>
            <input type="text" name="txt_name" placeholder="Name" value="{{ old('txt_name') }}" required>
        </div>
        <div class="field">
            <label>Email</label>
            <input type="email" name="txt_email" placeholder="Email" value="{{ old('txt_email') }}" required>
        </div>
        <div class="field">
            <label>Default Password</label>
            <input type="password" name="txt_password" placeholder="Default Password" value="{{ old('txt_password') }}"
                   required>
        </div>
        <div class="field">
            <label>Confirm Default Password</label>
            <input type="password" name="txt_confirm_password" placeholder="Confirm Default Password"
                   value="{{ old('txt_confirm_password') }}" required>
        </div>
        <div class="field">
            <label>Campus</label>
            <select name="cmb_campus">
                <option value="" disabled="disabled" selected="selected">-- Choose --</option>
                <option value="all" {{ old('cmb_campus') == 'all' ? 'selected' : '' }}>All</option>
                @foreach(\App\Campus::all() as $c)
                    <option value="{{ $c->name }}"  {{ old('cmb_campus') == $c->name ? 'selected' : '' }}>{{ $c->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="field">
            <label>Access Role</label>
            <select name="cmb_access_role">
                <option value="" disabled="disabled" selected="selected">-- Choose --</option>
                @foreach(\App\Department::getAllAccessRoles() as $key => $name)
                    <option value="{{ $key }}" {{ old('cmb_access_role') == $key ? 'selected' : '' }}>{{ $name }}</option>
                @endforeach
            </select>
        </div>
        <div class="field">
            <button class="ui green button">Add</button>
        </div>
        {!! Form::close() !!}
        <br><br>
    </div>

@stop
