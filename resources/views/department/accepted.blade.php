@extends ('layout.masterD')

@section ('title', 'BINUS')

@section('content')

    @include('partial/headerDepartment')

    <br><br>

    <div class="ui fluid container" style="padding-left: 1rem; padding-right: 1rem;">

        <div class="ui header">
            <center>List Approved Recruitment</center>
        </div>

        <div class="row">
            @if (session('fyi'))
                <div class="ui positive message">
                    <i class="close icon"></i>
                    <div class="header">
                        {{ session('fyi') }}
                    </div>
                </div>
            @elseif (!empty(session('err')))
                <div class="ui negative message">
                    <i class="close icon"></i>
                    <div class="header">
                        {{ session('err') }}
                    </div>
                </div>
            @endif
        </div>


        {!! Form::open(['url' => 'accepted', 'method' => 'get', 'enctype' => 'multipart/form-data', 'id' => 'formInput', 'role' => 'form', 'class' => 'ui large Searching form']) !!}
        <div class="field">
            <input type="text" name="Searching" id="idSearching" placeholder="Search" value="{{$Searching}}"/>
        </div>
        <button class="ui primary button" type="submit">Search</button>
        {!! Form::close() !!}
        <br>
        <a class="ui green button" type="submit" target="_blank" href="{{ url('/exportRecruitmentData') }}">Export to Excel</a>
        @if(sizeof($appdata)>0)
            <table class="ui celled small scrolling table" id="acceptedTable">
                <thead>
                <tr>
                    <th class="one wide">NIM</th>
                    <th class="two wide">Student Name</th>
                    <th class="one wide">School/Faculty</th>
                    <th class="one wide">Program</th>
                    <th class="one wide">Campus</th>
                    <th class="two wide">Job Name</th>
                    <th class="one wide">Jobdesc</th>
                    <th class="one wide">Start Date</th>
                    <th class="one wide">End Date</th>
                    <th class="two wide">Company Name</th>
                    <th class="two wide">Interviews</th>
                    <th class="one wide">Status</th>
                    <th class="two wide">Date Approved</th>
                    <th class="one wide">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($appdata as $d)
                    <tr class="positive">
                        <td>{{ $d->studentid }}</td>
                        <td>{{ $d->studentname }}</td>
                        <td>{{ $d->school }}</td>
                        <td>{{ $d->degree_name }}</td>
                        <td>{{ $d->campus }}</td>
                        <td>{{ $d->jobname }}</td>
                        <td>
                            @if($d->jobdesc != null)
                                <div class="ui button showModal{{$d->recid}}">Desc</div>
                            @else
                                -
                            @endif
                        </td>
                        <td>{{ \Carbon\Carbon::parse($d->startdate)->format('d M Y') }}</td>
                        <td>{{ \Carbon\Carbon::parse($d->enddate)->format('d M Y') }}</td>
                        <td>{{ $d->companyname }}</td>
                        <td>@if(count($interviews[$d->recruitmentid]) > 0)
                                <div class="ui button showInterviewModal{{ $d->recid }}">Detail</div>
                            @else
                                No Interview
                            @endif
                        </td>
                        <td>{{ $d->status }}</td>
                        <td>{{ $d->approvedate }}</td>
                        <td>
                            @if (\App\Department::isSuperAdmin())
                                <form action="{{ url('/department/unaccept_student/' . $d->recid) }}" method="POST">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <button class="ui button red" type="submit">Unaccept Student</button>
                                </form>
                                <br>
                            @endif
                            {{--                <a class="ui button" href="{{ url('/') }}/printLP/{{$d->studentid}}" target="_blank">--}}
                            {{--                  Download--}}
                            {{--                </a>--}}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <br>
{{--            <center><a class="ui primary button" href="{{ url('/') }}/exportAccData" target="_blank">Export Data</a>--}}
{{--            </center>--}}



            @foreach ($appdata as $d)
                <div class="ui small modal job {{$d->recid}}">
                    <div class="header">Job Description</div>
                    <br><br>
                    <div class="description">
                        <div class="ui bulleted list">
                            <?php
                            $pieces = explode("\n", $d->jobdesc);
                            ?>
                            <div class="item" style="margin:0 20px">
                                @foreach ($pieces as $piece)
                                    {{ $piece }}<br>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <br><br>
                    <div class="actions">
                        <div class="ui red deny button">
                            Close
                        </div>
                    </div>
                </div>

                <div class="ui small modal interview  {{$d->recid}}">
                    <div class="header">Interviews Detail</div>
                    <br><br>
                    <div class="description">
                        <div class="ui bulleted list ">
                            @foreach ($interviews[$d->recruitmentid] as $i)
                                <div class="item" style="margin:0 20px">
                                    Interviewer : {{ $i->interviewer }}
                                    <br> Location : {{ $i->location }}
                                    <br> Date : {{ $i->date }}
                                    <br> Time : {{ $i->time }}
                                    <br> Status : {{ $i->status }}
                                    <br> PIC : {{ $i->pic }}
                                    <br> Phone : {{ $i->phone }}</div>
                            @endforeach
                        </div>
                    </div>
                    <br><br>
                    <div class="actions">
                        <div class="ui red deny button">
                            Close
                        </div>
                    </div>
                </div>

                <script>
                    $('.showModal{{$d->recid}}').click(function (e) {
                        $('.small.modal.job.{{$d->recid}}').modal('show');
                    });
                    $('.showInterviewModal{{$d->recid}}').click(function (e) {
                        $('.small.modal.interview.{{$d->recid}}').modal('show');
                    });
                </script>
            @endforeach

            <?php
            $search = (isset($Searching)) ? "&Searching=" . $Searching : "";
            ?>

            <div class="pagination">
                @if($currentPage > 1)
                    <a class="ui blue circular label" href="
     {{ url('/accepted').'?page=1'.$search}}"><<</a>

                    <a class="ui blue circular label" href="
     {{ url('/accepted').'?page='.($currentPage-1).$search}}"><</a>
                @endif


                @for($i=$currentPage-5; $i<$currentPage ;$i++)
                    @if($i<=0)
                        @continue;
                    @endif
                    <a class="ui blue circular label" href="
  {{ url('/accepted').'?page='.$i.$search}}">{{$i}}</a>
                @endfor



                @for($i=$currentPage+1; $i<=$currentPage+5 ;$i++)
                    @if($i>$totalPage)
                        @continue;
                    @endif
                    <a class="ui blue circular label" href="{{ url('/accepted').'?page='.$i.$search }}">{{$i}}</a>
                @endfor



                @if($currentPage< $totalPage)
                    <a class="ui blue circular label"
                       href="{{ url('/accepted').'?page='.($currentPage+1).$search }}">></a>

                    <a class="ui blue circular label" href="{{ url('/accepted').'?page='.($totalPage).$search }}">>></a>
                @endif
            </div>

    </div>
    <br><br>
    @else
        <div class="ui error message">
            <i class="close icon"></i>
            <div class="header">
                No Student Found...
            </div>
        </div>
    @endif

@stop
