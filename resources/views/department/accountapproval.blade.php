@extends ('layout.masterD')

@section ('title', 'BINUS')

@section('content')

@include('partial/headerDepartment')
	<center>
  <div class="ui vertical stripe segment">
@if (session('fyi'))
        <div class="row">
            <div class="ui positive message">
              <i class="close icon"></i>
              <div class="header">
               {{session('fyi')}}
              </div>
        </div>
@endif

@if (!empty(session('errors')))
        <div class="row">
            <div class="ui negative message">
              <i class="close icon"></i>
              <div class="header">
               {{ session('errors') }}
              </div>
        </div>
@endif

{!! Form::open(['url' => 'updateapprovestudent', 'method' => 'post']) !!}
<input type="hidden" name="_token" value="{{ csrf_token() }}">
      <center>
        <h2>List Unapproved Students (registered in active 3+1 students list)</h2>
     </center>
     <br><br>
 <div class="ui middle aligned stackable grid container">
   <div class="row">
        <table class="ui padded table" id="listStudent1">
          <thead>
            <tr>
              <th class="two wide"></th>
              <th class="two wide">NIM</th>
              <th class="two wide">Name</th>
              <th class="two wide">Date of Birth</th>
              <th class="two wide">School</th>
              <th class="two wide">Program</th>
              <th class="two wide">Major</th>
              <th class="two wide"></th>
            </tr>
          </thead>
          <tbody>
            @foreach ($students_in_active_student as $student)
            <tr>
              <td class="collapsing">
                <div class="ui fitted slider checkbox">
                  <input type="checkbox" class="checkboxStudent1" name="checkbox1{{ $student->studentid }}"><label></label>
                </div>
              </td>
              <td>{{ $student->studentid }}</td>
              <td>{{ $student->name }}</td>
              <td>{{ $student->dob }}</td>
              <td>{{ $student->school }}</td>
              <td>{{ $student->degree_name }}</td>
              <td>{{ $student->major }}</td>
              <td><a href="{{url('/')}}/approveaccount/{{ $student->studentid }}" onclick="return confirm('Are you sure you want to approve this account ?')"><div class="ui primary button">Approve</div></a>
              <a href="{{url('/')}}/rejectaccount/{{ $student->studentid }}" onclick="return confirm('Are you sure you want to reject this account ?')"><div class="ui primary button">Reject</div></a>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
        <a class="ui primary button" id="selectAll1">Select All Students</a>
        <a class="ui primary button" id="unselectAll1">Unselect All Students</a>
        <button class="ui green button">Approve Selected Students</button>
      </div>
    </div>
{!! Form::close() !!}

<br><br><br><br>
<div class="ui divider"></div>

{!! Form::open(['url' => 'updateapprovestudentnotlisted', 'method' => 'post']) !!}
<input type="hidden" name="_token" value="{{ csrf_token() }}">
        <center>
		<h2>List Unapproved Students (not registered in active 3+1 students list)</h2>
	</center>
	<br><br>
   <div class="ui middle aligned stackable grid container">
 <div class="row">
        <table class="ui padded table" id="listStudent2">
          <thead>
            <tr>
              <th class="two wide"></th>
              <th class="two wide">NIM</th>
              <th class="two wide">Name</th>
              <th class="two wide">Date of Birth</th>
              <th class="two wide">School</th>
              <th class="two wide">Program</th>
              <th class="two wide">Major</th>
              <th class="two wide"></th>
            </tr>
          </thead>
          <tbody>
          	@foreach ($students_not_in_active_student as $student)
            <tr>
          		<td class="collapsing">
                          <div class="ui fitted slider checkbox">
                            <input type="checkbox" class="checkboxStudent2" name="checkbox2{{ $student->studentid }}"><label></label>
                          </div>
                        </td>
                        <td>{{ $student->studentid }}</td>
                    		<td>{{ $student->name }}</td>
                    		<td>{{ $student->dob }}</td>
                        <td>{{ $student->school }}</td>
                        <td>{{ $student->degree_name }}</td>
                        <td>{{ $student->major }}</td>
          		<td><a href="{{url('/')}}/approveaccount/{{ $student->studentid }}" onclick="return confirm('Are you sure you want to approve this account ?')"><div class="ui primary button">Approve</div></a>
          		<a href="{{url('/')}}/rejectaccount/{{ $student->studentid }}" onclick="return confirm('Are you sure you want to reject this account ?')"><div class="ui primary button">Reject</div></a>
          		</td>
          	</tr>
          	@endforeach
          </tbody>
        </table>
       <a class="ui primary button" id="selectAll2">Select All Students</a>
        <a class="ui primary button" id="unselectAll2">Unselect All Students</a>
        <button class="ui green button">Approve Selected Students</button>
      </div>
    </div>
{!! Form::close() !!}

<br><br><br><br>
<div class="ui divider"></div>
{!! Form::open(['url' => 'updateapprovecompany', 'method' => 'post']) !!}
<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<center>
		<h2>List Unapproved Companies Account</h2>
	</center>
	<br><br>
   <div class="ui middle aligned stackable grid container">
 <div class="row">
        <table class="ui padded table" id="listCompany1">
          <thead>
            <tr>
              <th class="two wide">Select Company</th>
              <th class="two wide">Id</th>
              <th class="three wide">Name</th>
              <th class="four wide">Email</th>
              <th class="four wide">Phone</th>
              <th class="four wide">Website</th>
              <th class="two wide">BIPP</th>
              <th class="two wide"></th>
            </tr>
          </thead>
          <tbody>
            @foreach ($Companies as $c)
            <tr>
              <td class="collapsing">
                <div class="ui fitted slider checkbox">
                  <input type="checkbox" class="checkboxCompany3" name="checkbox3{{ $c->companyid }}">
                </div>
              </td>
          		<td>{{ $c->companyid }}</td>
          		<td>{{ $c->name }}</td>
          		<td>{{ $c->email }}</td>
              <td>{{ $c->phone }}</td>
              <td>{{ $c->website }}</td>
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <td class="collapsing">
                <div class="ui fitted slider checkbox">
                @if ($c->bipp_id != null)
                  <input type="checkbox" class="checkboxBipp" id="checkboxBipp{{$c->id}}" name="bipp" checked>
                @else
                  <input type="checkbox" class="checkboxBipp" id="checkboxBipp{{$c->id}}" name="bipp">
                @endif
                </div>
              </td>
          		<td>
                <a class="ui primary button approveCompany" data-id="{{ $c->id }}" data-company="{{ $c->companyid }}">Approve</a>
                <a href="{{ url('/') }}/rejectaccount/{{ $c->companyid }}" onclick="return confirm('Are you sure you want to reject this account ?')"><div class="ui primary button">Reject</div></a>
          		</td>
            </tr>
            @endforeach
          </tbody>
        </table>
        <a class="ui primary button" id="selectAll3">Select All Companies</a>
        <a class="ui primary button" id="unselectAll3">Unselect All Companies</a>
        <button class="ui green button">Approve Selected Companies</button>
      </div>
    </div>
{!! Form::close() !!}

<br><br><br><br>
      <center>
        <h2>List Unregistered Students (registered in active 3+1 students list)</h2>
     </center>
     <br><br>
 <div class="ui middle aligned stackable grid container">
   <div class="row">
        <table class="ui padded table" id="listStudent3">
          <thead>
            <tr>
              <th class="four wide">NIM</th>
              <th class="eight wide">Name</th>
              <th class="two wide"></th>
            </tr>
          </thead>
          <tbody>
            @foreach ($unregistered_student as $student)
            <tr>
              <td>{{ $student->studentid }}</td>
              <td>{{ $student->name }}</td>
              <td><a href="{{url('/')}}/registerStudentByDepartment?nim={{ $student->studentid }}&name={{ $student->name }}" onclick="return confirm('Are you sure you want to open registration for this account ?')"><div class="ui primary button">Register</div></a>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>

<br><br><br>

     <center>
	        <h2>List Rejected Students</h2>
     </center>
	     <br><br>
	 <div class="ui middle aligned stackable grid container">
	   <div class="row">
	        <table class="ui padded table" id="listStudent4">
	          <thead>
	            <tr>
	              <th class="five wide">NIM</th>
	              <th class="eight wide">Name</th>
	              <th class="two wide"></th>
	            </tr>
	          </thead>
	          <tbody>
	            @foreach ($approval_rejected_student as $student)
	            <tr>
	              <td>{{ $student->studentid }}</td>
	              <td>{{ $student->name }}</td>
	              <td><a href="{{url('/')}}/unreject/{{ $student->studentid }}" onclick="return confirm('Are you sure you want to unreject this student ?')"><div class="ui primary button">Unreject</div></a>
	              </td>
	            </tr>
	            @endforeach
	          </tbody>
	        </table>
	      </div>
	    </div>

<br><br><br>

<center>
	   <h2>List Rejected Companies</h2>
</center>
	<br><br>
<div class="ui middle aligned stackable grid container">
  <div class="row">
	   <table class="ui padded table" id="listRejectedCompanies">
		 <thead>
		   <tr>
			 <th class="five wide">Company Name</th>
			 <th class="eight wide">Email</th>
			 <th class="two wide"></th>
		   </tr>
		 </thead>
		 <tbody>
		   @foreach ($approval_rejected_company as $company)
		   <tr>
			 <td>{{ $company->name }}</td>
			 <td>{{ $company->email }}</td>
			 <td><a href="{{url('/')}}/unrejectcompany/{{ $company->companyid }}" onclick="return confirm('Are you sure you want to unreject this company ?')"><div class="ui primary button">Unreject</div></a>
			 </td>
		   </tr>
		   @endforeach
		 </tbody>
	   </table>
	 </div>
   </div>

	<br><br><br>

  </div>

<script>
 $('.approveCompany').click(function(e) {
  confirm('Are you sure you want to approve this account ?');
  let cid = $(this).data('id');
  let companyId = $(this).data('company');
  let input = $('#checkboxBipp' + cid);
  $(location).attr('href', "{{ url('/approveaccount') }}" + '/' + companyId + '?bipp=' + input.prop( "checked" ));
 });
$(document).on('click', '#selectAll1', function(event){
  event.preventDefault();
  $('.checkboxStudent1').each(function(index, obj){
   $(obj).prop('checked', true);
  });
});
$(document).on('click', '#unselectAll1', function(event){
  event.preventDefault();
  $('.checkboxStudent1').each(function(index, obj){
    $(obj).prop('checked', false);
  });
});
$(document).on('click', '#selectAll2', function(event){
  event.preventDefault();
  $('.checkboxStudent2').each(function(index, obj){
   $(obj).prop('checked', true);
  });
});
$(document).on('click', '#unselectAll2', function(event){
  event.preventDefault();
  $('.checkboxStudent2').each(function(index, obj){
    $(obj).prop('checked', false);
  });
});
$(document).on('click', '#selectAll3', function(event){
  event.preventDefault();
  $('.checkboxCompany3').each(function(index, obj){
   $(obj).prop('checked', true);
  });
});
$(document).on('click', '#unselectAll3', function(event){
  event.preventDefault();
  $('.checkboxCompany3').each(function(index, obj){
    $(obj).prop('checked', false);
  });
});
</script>

@stop
