@php
    $facultyWhoCanSeeCompanyIdAndPassword = 'faculty:5';
@endphp

@extends ('layout.masterD')

@section ('title', 'BINUS')

@section('content')


    @include('partial/headerDepartment')


    <br><br>

    <style>
        .ui.padded.table td, .ui.padded.table th {
             padding: .5em;
        }
    </style>
    <div class="ui container">
        @if (\App\Department::isSuperAdmin() || \App\Department::isAdmin() || \App\Department::isEPC())
            <div class="ui segment" style="padding: 10px;">
                <div class="ui header">Submit New Company Data</div>
                <div class="ui divider"></div>
                {!! Form::open(['url' => 'submitByExcel', 'enctype' => 'multipart/form-data','method' => 'post', 'role' => 'form', 'class' => 'ui large form']) !!}
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="field">
                    <input type="file" name="fileexcel">
                </div>
                <div class="field">
                    <button class="ui primary button">Submit By Excel</button>&nbsp;<a
                            href="{{ asset('assets/template/Template_Insert_Company.xlsx') }}"/>Download Template
                    here </a>
                </div>
                {!! Form::close() !!}
                <div class="ui horizontal divider">OR</div>
                {!! Form::open(['url' => 'submitByForm', 'method' => 'post', 'role' => 'form', 'class' => 'ui large form']) !!}
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="field">
                    <input type="text" name="txtcompany" placeholder="Company Name (With 'CV'/'PT')">
                </div>
                <div class="ui left icon input field">
                    <div class="ui checkbox">
                        <input type="checkbox" class="inputCheckboxBipp" name="bipp" value="{{ $bipp_id }}">
                        <span>Inserted by {{Auth::user()->department->name}}</span>
                        <br>
                        <label>BIPP</label>
                    </div>
                </div>
                <div class="field">
                    <button class="ui primary button">Submit by Form</button>
                </div>
                {!! Form::close() !!}
            </div>
        @endif

        @if (session('status'))
            <div class="ui positive message">
                <p> {{ session('status') }}</p>
            </div>
        @endif

        @if($errors->first() != null)
            <div class="ui negative message">
                <p>{{$errors->first()}}</p>
            </div>
        @endif

        @if(session('err'))
            <div class="ui negative message">
                <p>{{ session('err') }}</p>
            </div>
        @endif

        @if(session('arrSame'))
            <div class="ui negative message">
                <p>Some Companies already Exist!</p>
                @foreach(session('arrSame') as $d)
                    <p>{{ $d}}</p>
                @endforeach
            </div>
        @endif

        <br><br>
        <div class="ui basic segment"/>
        <center>
            <div class="ui header">Company List</div>
            <a class="ui button" href="{{ url('/') }}/exportaccount" target="_blank">Export Account List</a>
        </center>
        <table class="ui padded table" id="companyTable">
            <thead>
            <tr>
                <th class="one wide">Company Name</th>
                <th class="one wide">Status</th>
                <th class="one wide">Business Category</th>
                <th class="two wide">Total Quota / Available Quota</th>
                <th class="one wide">Email</th>
                <th class="two wide">Phone</th>
                @if (\App\Department::isSuperAdmin() || \App\Department::isAdmin() || (\App\Department::isEPC() && \Auth::user()->department->head_of_program == $facultyWhoCanSeeCompanyIdAndPassword))
                    <th class="one wide">UserId & Default Password</th>
                @endif
                @if (\App\Department::isSuperAdmin() || \App\Department::isAdmin())
                <th class="one wide">Inserted By</th>
                <th class="two wide"></th>
                @endif
            </tr>
            </thead>
            <tbody>
            @foreach($data as $d)
                <tr>
                    <td>
                        <a href="{{ url('/profilec/' .$d->companyid) }}">{{$d->name}}</a> {{ (\App\Company::isBippByUserId($d->userid)) ? '(BIPP)' : '' }}
                        @if (\App\Department::isSuperAdmin())
                            <form action="{{ url('/department/togglecompanybipp/' . $d->companyprimaryid) }}"
                                  method="POST">
                                {{ csrf_field() }}
                                <button class="ui primary button" type="submit">
                                    Toggle BIPP Status
                                </button>
                            </form>
                        @endif
                    </td>
                    <td>{{$d->state}}</td>
                    <td>{{$d->business_category}}</td>
                    <td>{{$d->total_quota}} / <span class="ui green label">{{$d->available_quota}}</span></td>
                    <td>{{$d->email}}</td>
                    <td>{{$d->phone}}</td>
                    @if (\App\Department::isSuperAdmin() || \App\Department::isAdmin() || (\App\Department::isEPC() && \Auth::user()->department->head_of_program == $facultyWhoCanSeeCompanyIdAndPassword))
                        <td>{{$d->companyid}}<br><br><span style="background-color: black; color: #111111;">{{$d->defaultpassword}}</span></td>
                    @endif
                    @if (\App\Department::isSuperAdmin() || \App\Department::isAdmin())
                    <th>{{App\Department::where('departmentid','=',$d->created_by)->first()->name}} ({{App\Department::where('departmentid','=',$d->created_by)->first()->access_role}})</th>
                    <td>

                        @if($d->userstatus != 'active')
                                <a class="ui primary button"
                                   href="{{ url('/') }}/changeStatus/{{$d->companyid}}/active"
                                   onclick="return confirm('Are you sure you want to change this company\'s status to be ACTIVE ?')">
                                    Change status to Active
                                </a>
                            @endif

                        @if($d->userstatus != 'nonactive')
                            <a class="ui primary button"
                                href="{{ url('/') }}/changeStatus/{{$d->companyid}}/nonactive"
                                onclick="return confirm('Are you sure you want to change this company\'s status to be NONACTIVE ?')">
                                Change status to Nonactive
                            </a>
                        @endif

                        @if($d->status == 'active')
                            <a href="{{ url('/') }}/unapproveCompany/{{$d->companyid}}">
                                <div class="ui red button">Unapprove</div>
                            </a>

                            <a href="{{ url('/') }}/deleteCompany/{{$d->companyid}}">
                                <div class="ui red button">Delete</div>
                            </a>

                            <a href="{{ url('/') }}/generateNew/{{$d->companyid}}">
                                <div class="ui primary button">Generate New Password</div>
                            </a>

                            @if($d->state != 'closed')
                                <a class="ui primary button"
                                   href="{{ url('/') }}/changeState/{{$d->companyid}}/closed"
                                   onclick="return confirm('Are you sure you want to change this company\'s status to be CLOSED ?')">
                                    Change status to Closed
                                </a>
                            @endif

                            @if($d->state != 'open')
                                <a class="ui primary button"
                                   href="{{ url('/') }}/changeState/{{$d->companyid}}/open"
                                   onclick="return confirm('Are you sure you want to change this company\'s status to be OPEN ?')">
                                    Change status to Open
                                </a>
                            @endif
                        @else
                            <strong>Please approve this account first</strong>
                        @endif
                    </td>
                    @endif
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <br><br><br>
    </div>
@stop
