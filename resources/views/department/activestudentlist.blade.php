@extends ('layout.masterD')

@section ('title', 'BINUS')

@section('content')

@include('partial/headerDepartment')
<br><br>
<div class="ui container">

<h2 class="ui header">List of Active 3+1 Student</h2>

{!! Form::open(['url' => 'insertactivestudent', 'enctype' => 'multipart/form-data', 'method' => 'post']) !!}
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<div class="ui form">
	@if($errors->first() != null)
   	<div class="ui negative message">
		<p>{{$errors->first()}}</p>
	</div>
	@endif
	@if(!empty(session('status')))
   	<div class="ui positive message">
		<p>{{ session('status') }}</p>
	</div>
	@endif
	<div class="field">
		<label>Upload Excel File</label>
		<input type="file" name="excel_file">
	</div>
	<div class="ui message">
		<p>You can <a href="{{ url('/assets/template/Template_Active_Student.xlsx') }}">download template here</a></p>
	</div>
	<button>Submit</button>
</div>
{!! Form::close() !!}

<br><br>
<div class="ui basic segment">
	<table class="ui basic table" id="tableactivestudent">
	 	<thead>
			<tr>
				<td class="four wide">NIM</td>
				<td class="six wide">Name</td>
				<td class="six wide">Action</td>
			</tr>
		</thead>
		<tbody>
			@foreach ($students as $student)
			<tr>
				<td>{{ $student->studentid }}</td>
				<td>{{ $student->name }}</td>
				<td>
					{!! Form::open(['url' => 'deleteactivestudent', 'method' => 'post']) !!}
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" value="{{ $student->token }}" name="token">
					<button class="ui red button">Delete</button>
					{!! Form::close() !!}
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>
</div>

<script>
$(document).ready(function(){
	$('#tableactivestudent').DataTable({
		autoWidth: false
	});
});
</script>
@stop