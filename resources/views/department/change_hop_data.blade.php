@extends ('layout.masterD')

@section ('title', 'BINUS')

@section('content')

@include('partial/headerDepartment')

<br><br>

@if (session('fyi'))
<div class="row">
  <div class="ui positive message">
    <i class="close icon"></i>
    <div class="header">
     {{session('fyi')}}
   </div>
 </div>
</div>
@endif

@if($errors->first() != null)
<div class="row">
  <div class="ui negative message">
    <p>{{$errors->first()}}</p>
  </div>
</div>
@endif

@if(!empty(session('error')))
<div class="row">
  <div class="ui negative message">
    <p>{{ session('error') }}</p>
  </div>
</div>
@endif

<br><br>

<div class="ui container">
  <h2>Account: {{ $department->name }}</h2>

  <h4>Degrees:</h4>
  {!! Form::open(['url' => 'submitChangeHOPData', 'method' => 'post', 'class' => 'ui form']) !!}
  <input type="hidden" name="departmentid" value="{{ $department->departmentid }}">
  <div class="field">
      <div class="ui checkbox">
          <input type="checkbox" class="superCheckbox" name="roles[]" value="all" @if(in_array('all', $currentRoles)) checked @endif><label>All Faculty</label>
      </div>
  </div>

  @foreach($faculties as $faculty)
  <div class="ui styled accordion">
      <div class="title">
        <i class="dropdown icon"></i>
        {{ $faculty->name }}
      </div>
      <div class="content">
          <div class="field" style="padding-left: 2rem;">
              <div class="ui checkbox">
                  <input type="checkbox" class="facultyCheckbox" name="roles[]" value="faculty:{{ $faculty->id }}" @if(in_array('faculty:'.$faculty->id, $currentRoles)) checked @endif><label>{{ $faculty->name }}</label>
                  <br>
              </div>
              @foreach($faculty->degrees as $degree)
              <div class="field" style="margin-left: 2rem;">
                <div class="ui checkbox">
                    <input type="checkbox" class="degreeCheckbox" name="roles[]" value="{{ $degree->id }}" data-faculty="{{ $faculty->id }}" style="display: hidden;" @if(in_array($degree->id, $currentRoles)) checked @endif><label>{{ $degree->name }}</label>
                </div>
              </div>
              @endforeach
          </div>
      </div>
  </div>
  @endforeach
  <div class="field">
    <br>
      <button class="ui green button">Save</button>
  </div>
  {!! Form::close() !!}
  <br><br>
</div>

<script>
  $(document).ready(function(){
    $('.accordion').accordion();

    /* Checkbox Validation */
    $('.superCheckbox').change(function(){
      $('.facultyCheckbox').prop('checked', false);
      $('.degreeCheckbox').prop('checked', false);
    });
    $('.facultyCheckbox').change(function(){
      $('.superCheckbox').prop('checked', false);
      $('.facultyCheckbox').not(this).prop('checked', false);
      $('.degreeCheckbox').prop('checked', false);
    });
    $('.degreeCheckbox').change(function(){
      $('.superCheckbox').prop('checked', false);
      $('.facultyCheckbox').prop('checked', false);
      // let currFaculty = $(this).data('faculty');
      // $('.degreeCheckbox').filter(function(num, el) {
      //   return $(el).data('faculty') != currFaculty;
      // }).prop('checked', false);
    });
    /* End of Checkbox Validation */
  });
</script>
  
@stop
