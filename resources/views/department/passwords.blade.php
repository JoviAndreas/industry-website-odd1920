@extends ('layout.masterD')

@section ('title', 'BINUS')

@section('content')


@include('partial/headerDepartment')

<div class="ui container">
  <br><br>
  <div class="ui basic segment" style="text-align: center;">
    <p>Block Me -></p>
    <p class="ui item" style="background-color: black; display: inline-block;">{{ $passwords }}</p>
  </div>
  <br><br>
  <div class="ui basic segment" style="text-align: center;">
    <p>Login as {{ Auth::user()->department()->first()->name }}</p>
    <p><a href="{{ url('/') }}/changepassword" class="ui button">Change Current User Password</a></p>
  </div>
  <br><br>
  <div class="ui basic segment">
    <div class="ui header" style="text-align: center;">Change Master Password</div>
    <div class="ui basic segment">
      <div class="ui center aligned container">
      {!! Form::open(['url' => 'submitMasterPassword', 'method' => 'post', 'id' => 'formInput', 'role' => 'form', 'class' => 'ui masterpassword form']) !!}
        <div class="field">
          <label>New Password</label>
          <input type="password" name="newpassword" >
        </div>
        <div class="field">
            <label>Confirmation Password</label>
            <input type="password" name="confirmpassword">
        </div> 
        <button class="ui button" type="submit">Submit</button>
        <div class="ui error message"></div>
        @if (session('error'))
          <div class="ui red message"> {{ session('error') }}</div>       
        @endif
        @if (session('fyi'))
          <div class="ui green message"> {{ session('fyi') }}</div>       
        @endif
        @if($errors->first() != null)
          <div class="ui red message">{{$errors->first()}}</div>
        @endif
      {!! Form::close() !!}
      </div>
    </div>
  </div>

</div>
@stop