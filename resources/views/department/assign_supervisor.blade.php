@extends ('layout.masterD')

@section ('title', 'BINUS')

@section('content')

@include('partial/headerDepartment')

<br><br>
<div class="ui container">
    <center><h2>Assign University Supervisor</h2></center>
    <div class="ui basic segment">
        <div class="ui grid">
            @if (session('fyi'))
            <div class="row">

              @if (session('fyi') == 'Berhasil')
                <div class="ui positive message">
                  <i class="close icon"></i>
                  <div class="header">
                    You have successfully assign supervisor.
                  </div>
                </div>
              @elseif (session('fyi') == 'Berhasilupdate')
              <div class="ui positive message">
                <i class="close icon"></i>
                <div class="header">
                  You have successfully updated supervisor.
                </div>
              </div>
              @else
                <div class="ui negative message">
                  <i class="close icon"></i>
                  <div class="header">
                    {{ session('fyi') }}
                  </div>
                </div>
              @endif

            </div>
            @endif

            <div class="row">
                <div class="column">
                    Total Approved Student:<br>
                    <span class="ui large label">
                        {{ $total_approved }} student(s)
                    </span>
                </div>
            </div>

            <div class="row">
              <div class="column">
                <table class="ui table">
                  <thead>
                    <tr>
                      <th class="three wide">Lecturer Code</th>
                      <th class="three wide">Name</th>
                      <th class="ten wide">List Student</th>
                    </tr>
                  </thead>
                  <tbody>
                      @foreach($supervisors as $s)
                      @if($s->student_list->count() > 0)
                      <tr>
                          <td class="top aligned">{{ $s->lecturerid }}</td>
                          <td class="top aligned">{{ $s->name }}</td>
                          <td>
                              <ul>
                              @foreach($s->student_list as $stu)
                                <li>{{ $stu->studentid }} - {{ $stu->name }} - {{ $stu->degree_name }}</li>
                              @endforeach
                              </ul>
                              <button class="ui brown button" id="add_new_student" spv_id="{{ $s->id }}">Add New Student</button>
                          </td>
                      </tr>
                      @endif
                      @endforeach
                  </tbody>
                </table>
              </div>
            </div>

            <div class="row">
                <div class="column">
                    <button class="ui primary button" id="assign_new_spv">Assign New Supervisor</button>
                </div>
            </div>
        </div>
    </div>
</div>

<br><br>

<div class="ui large modal" id="modal_assign">
  <div class="header">Assign Supervisor</div>
  <div class="scrolling content">
      {!! Form::open(['url' => 'assign_university_supervisor', 'method' => 'post', 'role' => 'form', 'id' => 'form_assign_spv', 'class' => 'ui form']) !!}

      <div class="field">
          <label>Lecturer Name</label>
          <select name="spv_id_temp">
              @foreach($supervisors as $s)
              <option value="{{ $s->id }}">{{ $s->lecturerid }} - {{ $s->name }}</option>
              @endforeach
          </select>
          <input type="hidden" name="spv_id" value="">
      </div>
      <div class="field">
          <table class="ui compact table" id="tbl_list_student" width="100%">
              <thead>
                  <tr>
                      <td></td>
                      <td>NIM</td>
                      <td>Name</td>
                      <td>Program</td>
                      <td>Job</td>
                      <td>Company Name</td>
                  </tr>
              </thead>
          </table>
      </div>
      {!! Form::close() !!}
  </div>
  <div class="actions">
    <div class="ui approve green button">Save</div>
    <div class="ui cancel button">Cancel</div>
  </div>
</div>

<script>
$(document).ready(function(){
    $('#tbl_list_student').DataTable({
        "ajax": '{{ url('/get_list_approved_student') }}',
        "columns":
        [
          { "data": "student_id" },
          { "data": "student_id" },
          { "data": "student_name" },
          { "data": "program" },
          { "data": "job_name" },
          { "data": "company_name" }
        ],
        "columnDefs": [
            {
                "render": function ( data, type, row ) {
                    return '\
                        <input type="checkbox" name="student_'+ data +'">\
                    ';
                },
                "targets": 0
            }
        ]
    });

    $('#modal_assign').modal('setting', {
        onApprove: function(){
            $('#form_assign_spv').submit();
        }
    });

    $(document).on('click', '#add_new_student', function(e){
        var spv_id = e.target.attributes.spv_id.value;
        $('select[name="spv_id_temp"]').val(spv_id);
        $('input[name="spv_id"]').val(spv_id);
        $('select[name="spv_id_temp"]').attr('disabled', 'true');
        $('#modal_assign').modal('show');
    });

    $(document).on('click', '#assign_new_spv', function(e){
        $('#modal_assign').modal('show');
        $('input[name="spv_id"]').val('');
        $('select[name="spv_id_temp"]').removeAttr('disabled');
    });

    $(document).on('change', 'select[name="spv_id_temp"]', function(e){
        var spv_id = $('select[name="spv_id_temp"]').val();
        $('input[name="spv_id"]').val(spv_id);
    });
});
</script>

@stop
