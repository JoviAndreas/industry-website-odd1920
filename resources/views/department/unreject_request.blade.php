@extends ('layout.masterD')

@section ('title', 'BINUS')

@section('content')

@include('partial/headerDepartment')

<div class="ui container">
  <div class="ui vertical stripe segment">
@if (session('success_message'))
    <div class="row">
        <div class="ui positive message">
          <i class="close icon"></i>
          <div class="header">
           {{session('fyi')}}
          </div>
    </div>
@endif

@if (!empty(session('errors')))
    <div class="row">
        <div class="ui negative message">
          <i class="close icon"></i>
          <div class="header">
           {{ session('errors') }}
          </div>
    </div>
@endif

{!! Form::open(['url' => 'unreject_request_submit', 'method' => 'post', 'id' => 'form_unreject_request']) !!}
<input type="hidden" name="_token" value="{{ csrf_token() }}">

<center><h2>Unreject Request List</h2></center>
<table class="ui padded table" id="unreject_request_table">
  <thead>
    <tr>
      <th class="one wide"></th>
      <th class="two wide">NIM</th>
      <th class="two wide">Name</th>
      <th class="two wide">Program</th>
      <th class="two wide">Company Name</th>
      <th class="two wide">Job</th>
      <th class="three wide">Reason</th>
      <th class="two wide">Action</th>
    </tr>
  </thead>
   <tbody>
      @foreach($unreject_requests as $request)
      <tr>
        <td class="collapsing">
          <div class="ui fitted slider checkbox">
            <input type="checkbox" class="checkboxRequest1" name="checkbox1_{{ $request->request_id }}"><label></label>
          </div>
        </td>
        <td><a href="{{url('/')}}/profile/{{ $request->nim }}" target="_blank">{{ $request->nim }}</a></td>
        <td>{{ $request->student_name }}</td>
        <td>{{ $request->degree_name }}</td>
        <td>{{ $request->company_name }}</td>
        <td><a href="{{url('/')}}/job_detail/{{ $request->job_id }}" target="_blank">{{ $request->job_name }}</a></td>
        <td>{{ $request->reason }}</td>
        <td>
          <a href="{{url('/')}}/approve_unreject_request/{{ $request->request_id }}" onclick="return confirm('Are you sure you want to approve this request ?')"><div class="ui primary button">Approve</div></a>
          <a href="{{url('/')}}/reject_unreject_request/{{ $request->request_id }}" onclick="return confirm('Are you sure you want to reject this request ?')"><div class="ui primary button">Reject</div></a>
        </td>
      </tr>
      @endforeach
   </tbody>
</table>
<a class="ui primary button" id="selectAll1">Select All Requests</a>
<a class="ui primary button" id="unselectAll1">Unselect All Requests</a>
<input type="hidden" name="action_type" value="">
<button class="ui green button" id="approve_button">Approve Selected Requests</button>
<button class="ui red button" id="reject_button">Reject Selected Request</button>
{!! Form::close() !!}
<br><br>

<center><h2>Unreject Request History</h2></center>
<table class="ui padded table" id="unreject_request_history_table">
  <thead>
    <tr>
      <th class="one wide">NIM</th>
      <th class="two wide">Name</th>
      <th class="two wide">Program</th>
      <th class="two wide">Company Name</th>
      <th class="two wide">Job</th>
      <th class="three wide">Reason</th>
      <th class="five wide">Unreject Request Status</th>
    </tr>
  </thead>
   <tbody>
      @foreach($unreject_request_history as $request)
      <tr>
        <td><a href="{{url('/')}}/profile/{{ $request->nim }}" target="_blank">{{ $request->nim }}</a></td>
        <td>{{ $request->student_name }}</td>
        <td>{{ $request->degree_name }}</td>
        <td>{{ $request->company_name }}</td>
        <td><a href="{{url('/')}}/job_detail/{{ $request->job_id }}" target="_blank">{{ $request->job_name }}</a></td>
        <td>{{ $request->reason }}</td>
        <td>
            {{ $request->status }}<br>
            @if($request->status == 'approved')
            Approved By: {{ $request->approved_by }}<br>
            Approved On: {{ $request->approved_on }}
            @else
            Rejected By: {{ $request->rejected_by }}<br>
            Rejected On: {{ $request->rejected_on }}
            @endif
        </td>
      </tr>
      @endforeach
   </tbody>
</table>

<br><br>

</div>

<script>
$(document).on('click', '#approve_button', function(event){
	event.preventDefault();
	$('[name="action_type"]').val('approve');
	$('#form_unreject_request').submit();
});
$(document).on('click', '#reject_button', function(event){
	event.preventDefault();
	$('[name="action_type"]').val('reject');
	$('#form_unreject_request').submit();
});
$(document).on('click', '#selectAll1', function(event){
  	event.preventDefault();
	$('.checkboxRequest1').each(function(index, obj){
		$(obj).prop('checked', true);
	});
});
$(document).on('click', '#unselectAll1', function(event){
  	event.preventDefault();
	$('.checkboxRequest1').each(function(index, obj){
    		$(obj).prop('checked', false);
	});
});
</script>

@stop
