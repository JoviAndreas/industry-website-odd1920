@extends ('layout.masterD')

@section ('title', 'BINUS')

@section('content')

@include('partial/headerDepartment')
<br><br>
<div class="ui container">
	<div class="ui header">Feedbacks</div>
	<div class="ui basic segment">
	    <table class="ui padded table" id="listStudent1">
	      <thead>
	        <tr>
	          <th class="three wide">UserID</th>
			  <th class="three wide">Posted On</th>
	          <th class="ten wide">Message</th>
	        </tr>
	      </thead>
	      <tbody>
	        @foreach ($feedbacks as $f)
	        <tr>
	          <td>{{ $f->userid }}</td>
			  <td>{{ $f->created_at }}</td>
	          <td>{{ $f->content }}</td>
	        </tr>
	        @endforeach
	      </tbody>
	    </table>
	</div>
</div>
@stop
