@extends ('layout.masterD')

@section ('title', 'BINUS')

@section('content')

@include('partial/headerDepartment')

<div class="ui container">
<br><br>
<center>
@if (session('err'))
    <div class="ui red message">
        {{ session('err') }}
    </div>
    <br><br>
@endif
@if($errors->first() != null)
  	<div class="ui red message">{{ $errors->first() }}</div>
@endif
@if(session('fyi') )
	<div class="ui positive message">
		{{ session('fyi') }}
	</div>
@endif

<h2>BIPP MINIMUM GPA DATA</h2>
<br><br>

<span class="ui label large grey">MINIMUM GPA : {{ $data[0]->min_ipk }}</span>

<br><br>

{!! Form::open(['url' => 'submitEditBIPP', 'method' => 'post', 'enctype' => 'multipart/form-data', 'id' => 'formInput', 'role' => 'form', 'class' => 'ui large form']) !!}
<div class="field">
	<input type="number" name="bipp" min="0" max="4" value="{{ $data[0]->min_ipk ?? 0 }}" data-decimals="2" step="0.01" id="idBipp" placeholder="Set BIPP" />
</div>
<button class="ui primary button" type="submit">Update</button>
{!! Form::close() !!}
@stop
