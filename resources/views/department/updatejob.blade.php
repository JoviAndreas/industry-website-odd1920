@extends ('layout.masterD')

@section ('title', 'BINUS')

@section('content')

    @include('partial/headerDepartment')

    <br><br>

    <div class="ui container">


        @if (session('fyi'))
            <div class="row">
                @if (session('fyi') == 'Berhasil')
                    <div class="ui positive message">
                        <i class="close icon"></i>
                        <div class="header">
                            You have successfully updated job.
                        </div>
                    </div>
                @else
                    <div class="ui negative message">
                        <i class="close icon"></i>
                        <div class="header">
                            {{ session('fyi') }}
                        </div>
                    </div>
                @endif
            </div>
        @endif

        <h2 class="ui header centered">Update Job Vacancy</h2>
        <div class="ui row">
            {!! Form::open(['url' => 'updateJobDesc', 'method' => 'post', 'id' => 'formInput', 'role' => 'form', 'class' => 'ui large update form']) !!}
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="ui">
                <div class="fields">
                    <p>Set minimal IPK >= {{ \App\Bipp::first()->minimumIPK() }}: </p>
                    <div class="field">
                        <div class="ui radio checkbox">
                            <input type="radio" name="rdHasMinIPK" value="yes" id="rdHasMinIPKYes" {{ ($data->is_using_bipp_min_ipk) ? 'checked' : '' }}>
                            <label for="rdHasMinIPKYes">Yes</label>
                        </div>
                    </div>
                    <div class="field">
                        <div class="ui radio checkbox">
                            <input type="radio" name="rdHasMinIPK" value="no" id="rdHasMinIPKNo" {{ ($data->is_using_bipp_min_ipk) ? '' : 'checked' }}>
                            <label for="rdHasMinIPKNo">No</label>
                        </div>
                    </div>
                </div>
                <div class="field">
                    <div class="ui left icon input">
                        <input type="text" name="txtSupervisor" required id="idSupervisor" placeholder="PIC"
                               value="@if(old('txtSupervisor')!=null){{old('txtSupervisor')}}@else{{$data->supervisor_name}}@endif">
                        <i class="find icon"></i>
                    </div>
                </div>

                <div class="field">
                    <div class="ui left icon input">
                        <input type="text" name="txtSupervisorContact" required id="idSupervisorContact"
                               placeholder="PIC Contact"
                               value="@if(old('txtSupervisorContact')!=null){{old('txtSupervisorContact')}}@else{{$data->supervisor_contact}}@endif">
                        <i class="find icon"></i>
                    </div>
                </div>

                <div class="field">
                    <div class="ui left icon input">
                        <input type="number" name="txtAllowance" required min="1"
                               placeholder="Allowance (Rupiah)"
                               value="@if(old('txtAllowance')!=null){{old('txtAllowance')}}@else{{$data->allowance}}@endif">
                        <i class="money icon"></i>
                    </div>
                </div>

                <div class="two fields">
                    <div class="field">
                        <div class="ui left icon input">
                            <input type="number" name="txtQuantity" required id="idQuantity" placeholder="Quota" min="1"
                                   value="@if(old('txtQuantity')!=null){{old('txtQuantity')}}@else{{$data->quota}}@endif">
                            <i class="calculator icon"></i>
                        </div>
                    </div>

                    <div class="field">
                        <div class="ui left icon input">
                            <input type="text" name="txtLocation" required id="idLocation" placeholder="Location"
                                   value="@if(old('txtLocation')!=null){{old('txtLocation')}}@else{{$data->location}}@endif">
                            <i class="calculator icon"></i>
                        </div>
                    </div>
                </div>

                <div class="field">
                    <div>Programs: &nbsp;
                        {{-- <span class="ui label">To select program, please select the duration of your internship job</span> --}}
                    </div>
                    @foreach($faculties as $faculty)
                        <div class="ui accordion">
                            <div class="title">
                                <i class="dropdown icon"></i>
                                {{ $faculty->name }}
                            </div>
                            <div class="content">
                                <div class="field" style="padding-left: 2rem;">
                                    @foreach($faculty->degrees as $degree)
                                        <div class="ui left icon input">
                                            <input type="hidden" name="degreeu_{{ $data->id }}_{{ $degree->id }}"
                                                   value="off">
                                            <div class="ui checkbox checkboxDegreeU" data_id="{{ $data->id }}"
                                                 degree_id="{{ $degree->id }}">
                                                <input type="checkbox" class="inputCheckboxDegreeU"
                                                       data_id="{{ $data->id }}"
                                                       name="degreeu_{{ $data->id }}_{{ $degree->id }}" value="on"
                                                       degree_id="{{ $degree->id }}"
                                                       @if (!empty(old('degreeu_'.$data->id.'_' .$degree->id)) && old('degreeu_'.$data->id.'_' .$degree->id) == 'on') checked
                                                       @elseif(old('degreeu_'.$data->id.'_' .$degree->id) != 'off' && $jobsdegrees->where('id', $degree->id)->count() > 0) checked @endif>
                                                <label>{{ $degree->name }}</label>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="field">
                    <div class="ui left icon input">
                        <div class="ui dropdown selection" id="cmbPositionU">
                            <input type="hidden" name="cmbPositionU">
                            <i class="dropdown icon"></i>
                            <div class="default text"></div>
                            <div class="menu">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="two fields">
                    <div class="field">
                        <div class="ui left icon input">
                            <input type="text" class="datepickerDea" name="txtDeadline" id="txtDeadline"
                                   placeholder="Deadline Apply" size="30"
                                   value="@if(old('txtDeadline')!=null){{old('txtDeadline')}}@else{{date('m/d/Y',strtotime($data->deadline))}}@endif">
                            <i class="calendar icon"></i>
                        </div>
                    </div>

                    <div class="field">
                        <div class="ui left icon input">
                            <select required class="ui dropdown" name="txtDuration" id="datepickerDur"
                                    data_id="{{ $data->id }}" aria-placeholder="Duration">
                                @if(old('txtDuration')!=null)
                                    <option value="">Duration</option>
                                    <option value="4" @if(old('txtDuration')==4)selected="selected"@endif>4 Months
                                    </option>
                                    <option value="5" @if(old('txtDuration')==5)selected="selected"@endif>5 Months
                                    </option>
                                    <option value="6" id="durationOptionU6" data_id="{{ $data->id }}"
                                            {{-- availProgram="{{ $degrees_six_month }}" --}}
                                            @if(old('txtDuration')==6)selected="selected"@endif>
                                        6 Months
                                    </option>
                                    <option value="8" @if(old('txtDuration')==8)selected="selected"@endif>8 Months
                                    </option>
                                    <option value="9" @if(old('txtDuration')==9)selected="selected"@endif>9 Months
                                    </option>
                                    <option value="10" @if(old('txtDuration')==10)selected="selected"@endif>10 Months
                                    </option>
                                    <option value="11" @if(old('txtDuration')==11)selected="selected"@endif>11 Months
                                    </option>
                                    <option value="12" id="durationOptionU12" data_id="{{ $data->id }}"
                                            {{-- availProgram="{{ $degrees_twelve_month }}"  --}}
                                            @if(old('txtDuration')==12)selected="selected"@endif>
                                        12 Months
                                    </option>
                                @else
                                    <option value="">Duration</option>
                                    <option value="4" @if($data->duration==4)selected="selected"@endif>4 Months</option>
                                    <option value="5" @if($data->duration==5)selected="selected"@endif>5 Months</option>
                                    <option value="6" id="durationOptionU6" data_id="{{ $data->id }}"
                                            {{-- availProgram="{{ $degrees_six_month }}"  --}}
                                            @if($data->duration==6)selected="selected"@endif>
                                        6 Months
                                    </option>
                                    <option value="8" @if($data->duration==8)selected="selected"@endif>8 Months
                                    </option>
                                    <option value="9" @if($data->duration==9)selected="selected"@endif>9 Months
                                    </option>
                                    <option value="10" @if($data->duration==10)selected="selected"@endif>10 Months
                                    </option>
                                    <option value="11" @if($data->duration==11)selected="selected"@endif>11 Months
                                    </option>
                                    <option value="12" id="durationOptionU12" data_id="{{ $data->id }}"
                                            {{-- availProgram="{{ $degrees_twelve_month }}" --}}
                                            @if($data->duration==12)selected="selected"@endif>
                                        12 Months
                                    </option>
                                @endif
                            </select>
                        </div>
                    </div>
                </div>
                <div class="field">
                    <div class="ui left icon input">
                        <input type="text" class="datepickerDeaStartDate" name="txtStartDate" id="txtStartDate"
                               placeholder="Start Date" size="30"
                               value="@if(old('txtStartDate')!=null){{old('txtStartDate')}}@else{{date('m/d/Y',strtotime($data->startdate))}}@endif">
                        <i class="calendar icon"></i>
                    </div>
                </div>
                <div class="field">
                    <div class="ui left icon input">
                        <input type="text" id="txtEndDate" name="txtEndDate" placeholder="End Date" size="30" readonly
                               value="@if(old('txtEndDate')!=null){{old('txtEndDate')}}@else{{ date('m/d/Y',strtotime($data->enddate))}}@endif">
                        <i class="calendar icon"></i>
                    </div>
                </div>

                <div class="field">
                    <div class="ui left icon input">
                        <textarea rows="2" name="txtJobDescription" required id="idDescription"
                                  placeholder="Job Description ( Press enter to make points in the description. )">@if(old('txtJobDescription')!=null){{old('txtJobDescription')}}@else{{$data->description}}@endif</textarea>
                    </div>
                </div>

                <div class="field">
                    <div class="ui checkbox" id="checkboxLinkTestU{{ $data->token }}">
                        <input type="checkbox" id="checkboxLinkTestU" token="{{ $data->token }}"
                               name="checkboxLinkTestU"
                               @if(!empty(old('checkboxLinkTestU{{ $data->token }}')) || !empty($data->linktest)) checked @endif>
                        <label>Online Test</label>
                    </div>
                </div>

                <div class="field" id="fieldLinkTestU{{ $data->token }}"
                     style="@if(empty(old('checkboxLinkTestU{{ $data->token }}')) && empty($data->linktest)) display: none; @endif">
                    <div class="ui left icon input">
                        <input type="text" id="idLinkTest" name="txtuLinkTest" placeholder="Link Test (www.example.com)"
                               value="@if(old('txtuLinkTest')!=null){{old('txtuLinkTest')}}@else{{$data->linktest}}@endif">
                        <i class="linkify icon"></i>
                    </div>
                </div>

            <?php
            $listTC = explode(';', $data->learningobj);
            $TCarray = array();
            for ($i = 1; $i <= 12; $i++)
                $TCarray['tc' . $i] = 'off';
            foreach ($listTC as $key) {
                $TCarray['tc' . $key] = 'on';
            }


            $listSS = explode(';', $data->softskill);
            $SSarray = array();
            for ($i = 1; $i <= 6; $i++)
                $SSarray['ss' . $i] = 'off';
            foreach ($listSS as $key) {
                $SSarray['ss' . $key] = 'on';
            }
            ?>

            <!-- <div class="two fields">
        <div class="field">
          <div class="ui segment" style="padding: 1%;">
            Technical Competencies
            <div class="ui">
              <div class="ui @if($TCarray['tc1']=='on') checked @endif checkbox">
                <input type="checkbox" name="TC1" @if($TCarray['tc1']=='on') checked @endif>
                <label>User Requirement gathering</label>
              </div>
            </div>
            
            <div class="ui">
              <div class="ui @if($TCarray['tc2']=='on') checked @endif checkbox">
                <input type="checkbox" name="TC2" @if($TCarray['tc2']=='on') checked @endif>
                <label>Analysis of user requirements to find problems and solutions</label>
              </div>
            </div>
            
            <div class="ui">
              <div class="ui @if($TCarray['tc3']=='on') checked @endif checkbox">
                <input type="checkbox" name="TC3" @if($TCarray['tc3']=='on') checked @endif>
                <label>Application design according user requirements to solve problems</label>
              </div>
            </div>
            
            <div class="ui">
              <div class="ui @if($TCarray['tc4']=='on') checked @endif checkbox">
                <input type="checkbox" name="TC4" @if($TCarray['tc4']=='on') checked @endif>
                <label>Interface & UX design of application</label>
              </div>
            </div>
            
            <div class="ui">
              <div class="ui @if($TCarray['tc5']=='on') checked @endif checkbox">
                <input type="checkbox" name="TC5" @if($TCarray['tc5']=='on') checked @endif>
                <label>Code and develop application according to the application design</label>
              </div>
            </div>
            
            <div class="ui">
              <div class="ui @if($TCarray['tc6']=='on') checked @endif checkbox">
                <input type="checkbox" name="TC6" @if($TCarray['tc6']=='on') checked @endif>
                <label>Database Design for the application</label>
              </div>
            </div>
            
            <div class="ui">
              <div class="ui @if($TCarray['tc7']=='on') checked @endif checkbox">
                <input type="checkbox" name="TC7" @if($TCarray['tc7']=='on') checked @endif>
                <label>Application testing (Quality Assurance)</label>
              </div>
            </div>
            
            <div class="ui">
              <div class="ui @if($TCarray['tc8']=='on') checked @endif checkbox">
                <input type="checkbox" name="TC8" @if($TCarray['tc8']=='on') checked @endif>
                <label>Application implementation / publishing</label>
              </div>
            </div>
            
            <div class="ui">
              <div class="ui @if($TCarray['tc9']=='on') checked @endif checkbox">
                <input type="checkbox" name="TC9" @if($TCarray['tc9']=='on') checked @endif>
                <label>Database administration</label>
              </div>
            </div>
            
            <div class="ui">
              <div class="ui @if($TCarray['tc10']=='on') checked @endif checkbox">
                <input type="checkbox" name="TC10" @if($TCarray['tc10']=='on') checked @endif>
                <label>Network infrastructure design</label>
              </div>
            </div>
            
            <div class="ui">
              <div class="ui @if($TCarray['tc11']=='on') checked @endif checkbox">
                <input type="checkbox" name="TC11" @if($TCarray['tc11']=='on') checked @endif>
                <label>Network administration</label>
              </div>
            </div>
            
            <div class="ui">
              <div class="ui @if($TCarray['tc12']=='on') checked @endif checkbox">
                <input type="checkbox" name="TC12" @if($TCarray['tc12']=='on') checked @endif>
                <label>Applying software development methodology / process</label>
              </div>
            </div>
          </div>
        </div>
        <div class="field">
          <div class="ui segment" style="padding: 1%;">
            Soft Skills
            <div class="ui">
              <div class="ui @if($SSarray['ss1']=='on') checked @endif checkbox">
                <input type="checkbox" name="SS1" @if($SSarray['ss1']=='on') checked @endif>
                <label>Self Development</label>
              </div>
            </div>
            
            <div class="ui">
              <div class="ui @if($SSarray['ss2']=='on') checked @endif checkbox">
                <input type="checkbox" name="SS2" @if($SSarray['ss2']=='on') checked @endif>
                <label>Teamwork</label>
              </div>
            </div>
            
            <div class="ui">
              <div class="ui @if($SSarray['ss3']=='on') checked @endif checkbox">
                <input type="checkbox" name="SS3" @if($SSarray['ss3']=='on') checked @endif>
                <label>Communication</label>
              </div>
            </div>
            
            <div class="ui">
              <div class="ui @if($SSarray['ss4']=='on') checked @endif checkbox">
                <input type="checkbox" name="SS4" @if($SSarray['ss4']=='on') checked @endif>
                <label>Problem Solving and Decision Making</label>
              </div>
            </div>
            
            <div class="ui">
              <div class="ui @if($SSarray['ss5']=='on') checked @endif checkbox">
                <input type="checkbox" name="SS5" @if($SSarray['ss5']=='on') checked @endif>
                <label>Planning & Organizing</label>
              </div>
            </div>
            
            <div class="ui">
              <div class="ui @if($SSarray['ss6']=='on') checked @endif checkbox">
                <input type="checkbox" name="SS6" @if($SSarray['ss6']=='on') checked @endif>
                <label>Initiative & Enterprise</label>
              </div>
            </div>
            
          </div>
        </div>
      </div> -->

                <div class="field">
                    <div class="ui left icon input">
                        <input type="hidden" id="idToken" name="txtToken" placeholder="Token" value="{{ $token }}">
                    </div>
                </div>

                <div class="ui fluid large blue submit button">Update</div>

            </div>
            <div class="ui error message" id="error message update department"></div>
            {!! Form::close() !!}
            <br><br>
        </div>


    </div>
    </div>
    <script>
        /**
        * HARDCODE
        */
        $('.datepickerDeaStartDate').datepicker({minDate: new Date(2020, 1, 17), maxDate: new Date(2021, 1, 28)}); //1 itu bulan februari, idx mulai dari 0
        /**
        * ENDOF HARDCODE
        */

        ///////////     JOB POSITION AJAX   /////////
        function loadJobPosition() {
            let degrees = [];
            $('.inputCheckboxDegreeU').each(function (index, checkbox) {
                if ($(checkbox).is(':checked')) {
                    degrees.push($(checkbox).attr('degree_id'));
                }
            });
            $.ajax({
                type: 'GET',
                url: "{{ url('/ajax/job_position') }}",
                data: {'degrees': degrees},
                success: function (data) {
                    $('#cmbPositionU').dropdown('clear');
                    let positions = [];
                    data.forEach(function (position) {
                        let positionName = position.name;
                        let posObj = {
                            value: positionName,
                            text: positionName,
                            name: positionName
                        };
                        positions.push(posObj);
                    });
                    $('#cmbPositionU').dropdown('setup menu', {
                        'values': positions
                    });
                    $('#cmbPositionU').dropdown('set selected', '{{$data->name }}');
                },
                error: function () {
                    console.log("ERROR: AJAX");
                }
            });
        }

        function initJobPosition() {
            $('.inputCheckboxDegreeU').each(function (index, checkbox) {
                $(checkbox).change(function () {
                    loadJobPosition();
                });
            });
        }

        initJobPosition();
        loadJobPosition();
        /////////// END OF JOB POSITION AJAX /////////

        $('.accordion').accordion();

        function calculateEndDate(e) {
            let deadlineInput = $('#txtStartDate');
            let duration = $('#datepickerDur').val();
            if (duration != '' && deadlineInput.val() != '') {
                let date = new Date(deadlineInput.val());
                let endDate = new Date(date.setMonth(date.getMonth() + parseInt(duration) + 1));
                let formattedDate = date.getMonth() + '/' + date.getDate() + '/' + date.getFullYear();
                $('#txtEndDate').val(formattedDate);
            }
        }

        $('#txtStartDate').change(calculateEndDate);
        $('#datepickerDur').change(calculateEndDate);

        $(document).on('change', '#checkboxLinkTestU', function () {
            var token = $(this).attr('token');
            if ($('input[token="' + token + '"]').is(':checked') == true) {
                $('#fieldLinkTestU' + token).show();
            } else {
                $('#fieldLinkTestU' + token).hide();
            }
        });
        // $(document).on('change', '[name="txtDuration"]', function(e){
        //   var data_id = e.target.attributes.data_id.value;
        //   var value = $('[name="txtDuration"][data_id="'+ data_id +'"]').val();
        //   if(value == '6'){
        //     var degrees_six_month = $('#durationOptionU6[data_id="'+ data_id +'"]').attr('availProgram');
        //     var split = degrees_six_month.split(";");
        //     $('.checkboxDegreeU[data_id="'+ data_id +'"]').removeClass('disabled');
        //     $('.checkboxDegreeU[data_id="'+ data_id +'"]').addClass('disabled');
        //     $('.inputCheckboxDegreeU[data_id="'+ data_id +'"]').prop('checked', false);
        //     split.forEach(function(currentValue, index, arr){
        //       // $('[name="degreeu_'+ data_id + '_' + currentValue +'"]').prop('checked', true);
        //       $('.checkboxDegreeU[degree_id="'+ currentValue +'"][data_id="'+ data_id +'"]').removeClass('disabled');
        //     });
        //   }
        //   else if(value == '12'){
        //     var degrees_twelve_month = $('#durationOptionU12[data_id="'+ data_id +'"]').attr('availProgram');
        //     var split = degrees_twelve_month.split(";");
        //     $('.checkboxDegreeU[data_id="'+ data_id +'"]').removeClass('disabled');
        //     $('.checkboxDegreeU[data_id="'+ data_id +'"]').addClass('disabled');
        //     $('.inputCheckboxDegreeU[data_id="'+ data_id +'"]').prop('checked', false);
        //     split.forEach(function(currentValue, index, arr){
        //       // $('[name="degreeu_'+ data_id + '_' + currentValue +'"]').prop('checked', true);
        //       $('.checkboxDegreeU[degree_id="'+ currentValue +'"][data_id="'+ data_id +'"]').removeClass('disabled');
        //     });
        //   }
        //   else{
        //     $('.checkboxDegreeU[data_id="'+ data_id +'"]').removeClass('disabled');
        //     $('.checkboxDegreeU[data_id="'+ data_id +'"]').addClass('disabled');
        //     $('.inputCheckboxDegreeU[data_id="'+ data_id +'"]').prop('checked', false);
        //   }
        // });
        // $(document).ready(function(){
        //   $('[name="txtDuration"]').each(function(index, element){
        //     var data_id = $(element).attr('data_id');
        //     var value = $(element).val();
        //     if(value == '6'){
        //       var degrees_six_month = $('#durationOptionU6[data_id="'+ data_id +'"]').attr('availProgram');
        //       var split = degrees_six_month.split(";");
        //       $('.checkboxDegreeU[data_id="'+ data_id +'"]').removeClass('disabled');
        //       $('.checkboxDegreeU[data_id="'+ data_id +'"]').addClass('disabled');
        //       $('.inputCheckboxDegreeU[data_id="'+ data_id +'"]').prop('checked', false);
        //       split.forEach(function(currentValue, index, arr){
        //         // $('[name="degreeu_'+ data_id + '_' + currentValue +'"]').prop('checked', true);
        //         $('.checkboxDegreeU[degree_id="'+ currentValue +'"][data_id="'+ data_id +'"]').removeClass('disabled');
        //       });
        //     }
        //     else if(value == '12'){
        //       var degrees_twelve_month = $('#durationOptionU12[data_id="'+ data_id +'"]').attr('availProgram');
        //       var split = degrees_twelve_month.split(";");
        //       $('.checkboxDegreeU[data_id="'+ data_id +'"]').removeClass('disabled');
        //       $('.checkboxDegreeU[data_id="'+ data_id +'"]').addClass('disabled');
        //       $('.inputCheckboxDegreeU[data_id="'+ data_id +'"]').prop('checked', false);
        //       split.forEach(function(currentValue, index, arr){
        //         // $('[name="degreeu_'+ data_id + '_' + currentValue +'"]').prop('checked', true);
        //         $('.checkboxDegreeU[degree_id="'+ currentValue +'"][data_id="'+ data_id +'"]').removeClass('disabled');
        //       });
        //     }
        //     else{
        //       $('.checkboxDegreeU[data_id="'+ data_id +'"]').removeClass('disabled');
        //       $('.checkboxDegreeU[data_id="'+ data_id +'"]').addClass('disabled');
        //       $('.inputCheckboxDegreeU[data_id="'+ data_id +'"]').prop('checked', false);
        //     }
        //   });
        // });
    </script>
@stop
