@extends ('layout.masterD')

@section ('title', 'BINUS')

@section('content')


    @include('partial/headerDepartment')

    <br><br>

    <div class="ui middle aligned grid container">

        @if (session('fyi'))
            <div class="row">
                @if (session('fyi') == 'Berhasil')
                    <div class="ui positive message">
                        <i class="close icon"></i>
                        <div class="header">
                            You have successfully added job.
                        </div>
                    </div>
                @else
                    <div class="ui negative message">
                        <i class="close icon"></i>
                        <div class="header">
                            {{ session('fyi') }}
                        </div>
                    </div>
                @endif
            </div>
        @elseif (session('success'))
            <div class="row">
                <div class="ui positive message">
                    <i class="close icon"></i>
                    <div class="header">
                        {{ session('success') }}
                    </div>
                </div>
            </div>
        @endif

        @if(session('head_of_program') == 'all')
            {!! Form::open(['url' => 'managejobfiltered', 'method' => 'get', 'role' => 'form', 'class' => 'ui large form']) !!}
            <div class="fields">
                <div class="field">
                    <select name="filterDegrees">
                        <option value="">All Degrees</option>
                        @foreach(\App\Degree::all() as $d)
                            <option value="{{ $d->name }}"
                                    @if (!empty($selected_degree) && $selected_degree == $d->name) selected @endif>{{ $d->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="field">
                    <button class="ui large primary button">Filter</button>
                </div>
            </div>
            {!! Form::close() !!}
        @endif

        <div class="ui large button insertJob">Insert</div>

        <div class="row" style="max-height: 70vh; max-width: 100%; overflow-x: scroll;">
            <table class="ui small table">
                <thead>
                <tr>
                    <th class="one wide">Position</th>
                    <th class="one wide">Company</th>
                    <th class="one wide">Full Quota</th>
                    <th class="one wide">Current Quota</th>
                    <th class="two wide">Location</th>
                    <th class="one wide">PIC</th>
                    <th class="one wide">PIC Contact</th>
                    @if (\App\Department::isSuperAdmin() || \App\Department::isAdmin())
                    <th class="one wide">Allowance</th>
                    @endif
                    <th class="two wide">Job Desc</th>
                    <th class="two wide">Programs</th>
                    <th class="one wide">Deadline Apply</th>
                    <th class="one wide">Start Date</th>
                    <th class="one wide">End Date</th>
                    <th class="one wide">Duration</th>
                    <th class="one wide">Min GPA</th>
                    <th class="one wide">Approved By</th>
                    @if (\App\Department::isSuperAdmin() || \App\Department::isAdmin())
                    <th class="two wide"></th>
                    @endif
                </tr>
                </thead>
                <tbody>
                @foreach ($datas as $data)
                    <tr>
                        <td>
                            {{ $data->position }}<br><br>
                            [Status: <span
                                    style="color: {{ ($data->state == 'open') ? 'green' : (($data->state == 'waiting') ? 'orange' : 'red') }};">{{ strtoupper($data->state) }}</span>]
                        </td>
                        <td>{{ $data->companyname }} {{ (\App\Company::isBippByUserId($data->companyid)) ? '(BIPP)' : '' }}</td>
                        <td>{{ $data->full_quota }}</td>
                        <td><span class="ui green label">{{ $data->quota }}</span></td>
                        <td>{{ $data->location }}</td>
                        <td>{{ $data->supervisor_name }}</td>
                        <td>{{ $data->supervisor_contact }}</td>
                        @if (\App\Department::isSuperAdmin() || \App\Department::isAdmin())
                        <td>{{ 'Rp. ' . number_format($data->allowance,0,",",".") }}</td>
                        @endif
                        <td><div class="ui small button showDescription{{ $data->token }}">Show</div></td>
                        <td>
                            <div class="ui bulleted list">
                                @foreach ($data->degrees as $d)
                                    <div class="item">{{ $d->name }}</div>
                                @endforeach
                            </div>
                        </td>
                        <td>{{ date("d M Y", strtotime($data->deadline)) }}</td>
                        <td>{{ ($data->startdate == null || $data->startdate == '0000-00-00') ? '-' : date("d M Y", strtotime($data->startdate)) }}</td>
                        <td>{{ ($data->enddate == null || $data->enddate == '0000-00-00') ? '-' : date("d M Y", strtotime($data->enddate)) }}</td>
                        <td>{{ $data->duration }} months</td>
                        <td>{{ ($data->is_using_bipp_min_ipk) ? \App\Bipp::first()->minimumIPK() : 'No Minimum GPA' }}</td>
                        <td>{{ ($data->state == 'open') ? (($data->approved_by_user_id == null) ? 'System' : \App\User::where('id',$data->approved_by_user_id)->first()->userid) : '-' }}</td>
                        @if (\App\Department::isSuperAdmin() || \App\Department::isAdmin())
                        <td>
                            <a href="{{ url('job_detail/' . $data->id) }}" class="ui small button">View</a>
                            @if ($data->state == 'waiting')
                                <a href="{{ url('approve_job/' . $data->id) }}" class="ui small button primary"
                                   onclick="let a = confirm('Are you sure to approve?'); return a;">Approve</a>
                                <a href="{{ url('reject_job/' . $data->id) }}" class="ui small button red"
                                   onclick="let a = confirm('Are you sure to reject?'); return a;">Reject</a>
                                @if ( Auth::user()->userid == 'superadmin' )
                                <a href="{{ url('close_job/' . $data->id) }}" class="ui small button red"
                                    onclick="let a = confirm('Are you sure to close vacancy??'); return a;">Close Vacancy</a>
                                @endif
                            @endif
                            <a class="ui small button"
                               href="{{ url('departmentupdatejob/' . $data->token) }}">Update</a>
                            <div class="ui small button delete job" data-value="{{'job'.$data->token.$data->position}}">
                                Delete
                            </div>
                            @if(!empty($data->linktest))
                                <div class="ui small button showLink{{ $data->token }}">Info</div>
                            @endif
                        </td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
    </div>

    @foreach ($datas as $data)
        <div class="ui modal showDescription{{ $data->token }}">
            <div class="header">Information</div>
            <div class="content">
                <div class="ui bulleted list">
                    <?php
                    $pieces = explode("\n", $data->description);
                    ?>
                    @foreach ($pieces as $piece)
                        <div class="item">{{ $piece }}</div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="ui modal showLink{{ $data->token }}">
            <div class="header">Information</div>
            <div class="content">
                <p>From : {{$data->companyname}}</p>
                <p>Job : {{$data->position}}</p>
                <p>Here is the<a target="_blank" href="{{$data->linktest}}"> Link </a> for Online test</p>
            </div>
        </div>
        <script>
            $('.showDescription{{ $data->token }}').click(function (e) {
                $('.ui.modal.showDescription{{ $data->token }}').modal('show');
            });
            $('.showLink{{ $data->token }}').click(function (e) {
                $('.ui.modal.showLink{{ $data->token }}').modal('show');
            });
        </script>
    @endforeach

    <div class="ui modal insertJob" style="overflow: auto;">

        <div class="header">Insert Job Vacancy</div>

        <div class="content">
            {!! Form::open(['url' => 'insertJobDesc', 'method' => 'post', 'id' => 'formInput', 'role' => 'form', 'class' => 'ui large form']) !!}

            <div class="ui">
                <div class="fields">
                    <p>Set minimal IPK >= {{ \App\Bipp::first()->minimumIPK() }}: </p>
                    <div class="field">
                        <div class="ui radio checkbox">
                            <input type="radio" name="rdHasMinIPK" value="yes" id="rdHasMinIPKYes">
                            <label for="rdHasMinIPKYes">Yes</label>
                        </div>
                    </div>
                    <div class="field">
                        <div class="ui radio checkbox">
                            <input type="radio" name="rdHasMinIPK" value="no" id="rdHasMinIPKNo" checked>
                            <label for="rdHasMinIPKNo">No</label>
                        </div>
                    </div>
                </div>
                <div class="field">
                    <div class="ui fluid dropdown labeled icon button">
                        <input type="hidden" name="txtcompanyid">
                        <i class="filter icon"></i>
                        <span class="text">Company</span>
                        <div class="menu">
                            <div class="ui icon search input">
                                <i class="search icon"></i>
                                <input type="text" placeholder="Search company...">
                            </div>
                            <div class="scrolling menu">
                                    @foreach($companys as $c)
                                    <div class="item" data-value="{{ $c->companyid }}">
                                        {{ $c->name }}
                                    </div>
                                    @endforeach
                            </div>
                        </div>
                    </div>
                </div>


                <div class="field">
                    <div class="ui left icon input">
                        <input type="text" name="txtSupervisor" required id="idSupervisor" placeholder="PIC"
                               value="@if(old('txtSupervisor')!=null){{old('txtSupervisor')}}@endif">
                        <i class="find icon"></i>
                    </div>
                </div>

                <div class="field">
                    <div class="ui left icon input">
                        <input type="text" name="txtSupervisorContact" required id="idSupervisorContact"
                               placeholder="PIC Contact"
                               value="@if(old('txtSupervisorContact')!=null){{old('txtSupervisorContact')}}@endif">
                        <i class="find icon"></i>
                    </div>
                </div>

                <div class="field">
                    <div class="ui left icon input">
                        <input type="number" name="txtAllowance" required min="1"
                               placeholder="Allowance (Rupiah)"
                               value="@if(old('txtAllowance')!=null){{old('txtAllowance')}}@endif">
                        <i class="money icon"></i>
                    </div>
                </div>

                <div class="two fields">
                    <div class="field">
                        <div class="ui left icon input">
                            <input type="number" name="txtQuantity" required id="idQuantity" placeholder="Quota" min="1"
                                   value="@if(old('txtQuantity')!=null){{old('txtQuantity')}}@endif">
                            <i class="calculator icon"></i>
                        </div>
                    </div>

                    <div class="field">
                        <div class="ui left icon input">
                            <input type="text" name="txtLocation" required id="idLocation" placeholder="Location"
                                   value="@if(old('txtLocation')!=null){{old('txtLocation')}}@endif">
                            <i class="calculator icon"></i>
                        </div>
                    </div>
                </div>

                <div class="field">
                    <div>Programs: &nbsp;
                        {{-- <span class="ui label">To select program, please select the duration of your internship job first</span> --}}
                    </div>
                    @foreach($faculties as $faculty)
                        <div class="ui accordion">
                            <div class="title">
                                <i class="dropdown icon"></i>
                                {{ $faculty->name }}
                            </div>
                            <div class="content">
                                <div class="field" style="padding-left: 2rem;">
                                    @foreach($faculty->degrees as $degree)
                                        <div class="ui left icon input">
                                            <div class="ui checkbox checkboxDegree" degree_id="{{ $degree->id }}">
                                                <input type="checkbox" class="inputCheckboxDegree"
                                                       name="degreei_{{ $degree->id }}" value="on"
                                                       degree_id="{{ $degree->id }}"
                                                       @if(old('degreei_'.$degree->id) == 'on') checked @endif>
                                                <label>{{ $degree->name }}</label>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>


                <div class="field">
                    <div class="ui left icon input">
                        <div class="ui fluid dropdown labeled icon button">
                            <input type="hidden" name="cmbPosition">
                            <i class="filter icon"></i>
                            <span class="text">Job Position</span>
                            <div class="menu">
                                <div class="ui icon search input">
                                    <i class="search icon"></i>
                                    <input type="text" placeholder="Search job position...">
                                </div>
                                <div class="scrolling menu" id="cmbPosition">
                                </div>
                            </div>
                        </div>
{{--                        <select class="ui dropdown" name="cmbPosition" id="cmbPosition">--}}
{{--                            <option value="">Job Position</option>--}}
{{--                            --}}{{--                            @foreach($jobpositions as $jobposition)--}}
{{--                            --}}{{--                                <option value="{{$jobposition->name}}" @if($jobposition->id == old('cmbPosition'))--}}
{{--                            --}}{{--                                selected="selected"--}}
{{--                            --}}{{--                                        @endif>{{$jobposition->name}}</option>--}}
{{--                            --}}{{--                            @endforeach--}}
{{--                        </select>--}}
                    </div>
                </div>

                <div class="two fields">
                    <div class="field">
                        <div class="ui left icon input">
                            <input type="text" class="datepickerDea" id="txtDeadline" name="txtDeadline"
                                   placeholder="Deadline" size="30"
                                   value="@if(old('txtDeadline')!=null){{old('txtDeadline')}}@endif">
                            <i class="calendar icon"></i>
                        </div>
                    </div>

                    <div class="field">
                        <div class="ui left icon input">
                            <select required class="ui dropdown" name="txtDuration" id="datepickerDur"
                                    aria-placeholder="Duration">
                                <option value="">Duration</option>
                                <option value="4" @if(old('txtDuration')==4)selected="selected"@endif>4 Months</option>
                                <option value="5" @if(old('txtDuration')==5)selected="selected"@endif>5 Months</option>
                                <option value="6" id="durationOption6"
                                        {{-- availProgram="{{ $degrees_six_month }}" --}}
                                        @if(old('txtDuration')==6)selected="selected"@endif>
                                    6 Months
                                </option>
                                <option value="8" @if(old('txtDuration')==8)selected="selected"@endif>8 Months
                                </option>
                                <option value="9" @if(old('txtDuration')==9)selected="selected"@endif>9 Months
                                </option>
                                <option value="10" @if(old('txtDuration')==10)selected="selected"@endif>10 Months
                                </option>
                                <option value="11" @if(old('txtDuration')==11)selected="selected"@endif>11 Months
                                </option>
                                <option value="12" id="durationOption12"
                                        {{-- availProgram="{{ $degrees_twelve_month }}"  --}}
                                        @if(old('txtDuration')==12)selected="selected"@endif>
                                    12 Months
                                </option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="field">
                    <div class="ui left icon input">
                        <input type="text" class="datepickerDeaStartDate" id="txtStartDate" name="txtStartDate"
                               placeholder="Start Date" size="30"
                               value="@if(old('txtStartDate')!=null){{old('txtStartDate')}}@endif">
                        <i class="calendar icon"></i>
                    </div>
                </div>
                <div class="field">
                    <div class="ui left icon input">
                        <input type="text" id="txtEndDate" name="txtEndDate" placeholder="End Date" size="30" readonly
                               value="@if(old('txtEndDate')!=null){{old('txtEndDate')}}@endif">
                        <i class="calendar icon"></i>
                    </div>
                </div>

                <div class="field">
                    <div class="ui left icon input">
                        <textarea rows="2" name="txtJobDescription" required id="idDescription"
                                  placeholder="Job Description ( Press enter to make points in the description. )">{{old('txtJobDescription')}}</textarea>
                    </div>
                </div>

                <div class="field">
                    <div class="ui checkbox" id="checkboxLinkTest">
                        <input type="checkbox" name="checkboxLinkTest"
                               @if(!empty(old('checkboxLinkTest'))) checked; @endif>
                        <label>Online Test</label>
                    </div>
                </div>

                <div class="field" id="fieldLinkTest" style="@if(empty(old('checkboxLinkTest'))) display: none; @endif">
                    <div class="ui left icon input">
                        <input type="text" id="idLinkTest" name="txtiLinkTest" required
                               placeholder="Link Test (www.example.com)" size="30"
                               value="@if(old('txtiLinkTest')!=null){{old('txtiLinkTest')}}@endif">
                        <i class="linkify icon"></i>
                    </div>
                </div>

                <!-- <div class="two fields">
                  <div class="field">
                    <div class="ui segment" style="padding: 1%;">
                      Technical Competencies
                      <div class="ui">
                        <div class="ui checkbox">
                          <input type="checkbox" name="TC1">
                          <label>User Requirement gathering</label>
                        </div>
                      </div>

                      <div class="ui">
                        <div class="ui checkbox">
                          <input type="checkbox" name="TC2">
                          <label>Analysis of user requirements to find problems and solutions</label>
                        </div>
                      </div>

                      <div class="ui">
                        <div class="ui checkbox">
                          <input type="checkbox" name="TC3">
                          <label>Application design according user requirements to solve problems</label>
                        </div>
                      </div>

                      <div class="ui">
                        <div class="ui checkbox">
                          <input type="checkbox" name="TC4">
                          <label>Interface & UX design of application</label>
                        </div>
                      </div>

                      <div class="ui">
                        <div class="ui checkbox">
                          <input type="checkbox" name="TC5">
                          <label>Code and develop application according to the application design</label>
                        </div>
                      </div>

                      <div class="ui">
                        <div class="ui checkbox">
                          <input type="checkbox" name="TC6">
                          <label>Database Design for the application</label>
                        </div>
                      </div>

                      <div class="ui">
                        <div class="ui checkbox">
                          <input type="checkbox" name="TC7">
                          <label>Application testing (Quality Assurance)</label>
                        </div>
                      </div>

                      <div class="ui">
                        <div class="ui checkbox">
                          <input type="checkbox" name="TC8">
                          <label>Application implementation / publishing</label>
                        </div>
                      </div>

                      <div class="ui">
                        <div class="ui checkbox">
                          <input type="checkbox" name="TC9">
                          <label>Database administration</label>
                        </div>
                      </div>

                      <div class="ui">
                        <div class="ui checkbox">
                          <input type="checkbox" name="TC10">
                          <label>Network infrastructure design</label>
                        </div>
                      </div>

                      <div class="ui">
                        <div class="ui checkbox">
                          <input type="checkbox" name="TC11">
                          <label>Network administration</label>
                        </div>
                      </div>

                      <div class="ui">
                        <div class="ui checkbox">
                          <input type="checkbox" name="TC12">
                          <label>Applying software development methodology / process</label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="field">
                    <div class="ui segment" style="padding: 1%;">
                      Soft Skills
                      <div class="ui">
                        <div class="ui checkbox">
                          <input type="checkbox" name="SS1">
                          <label>Self Development</label>
                        </div>
                      </div>

                      <div class="ui">
                        <div class="ui checkbox">
                          <input type="checkbox" name="SS2">
                          <label>Teamwork</label>
                        </div>
                      </div>

                      <div class="ui">
                        <div class="ui checkbox">
                          <input type="checkbox" name="SS3">
                          <label>Communication</label>
                        </div>
                      </div>

                      <div class="ui">
                        <div class="ui checkbox">
                          <input type="checkbox" name="SS4">
                          <label>Problem Solving and Decision Making</label>
                        </div>
                      </div>

                      <div class="ui">
                        <div class="ui checkbox">
                          <input type="checkbox" name="SS5">
                          <label>Planning & Organizing</label>
                        </div>
                      </div>

                      <div class="ui">
                        <div class="ui checkbox">
                          <input type="checkbox" name="SS6">
                          <label>Initiative & Enterprise</label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div> -->
                <div class="ui fluid large blue submit button">Insert</div>
            </div>
            <div class="ui error message" id="error message insert department"></div>
            {!! Form::close() !!}
        </div>
    </div>

    <input type="hidden"
           value="<?php $a = ['Sales' => 3, 'Research' => 5, 'Accounting' => 2]; echo str_replace('"', '&quot;', json_encode($a)); ?>"
           id="dataTesting"/>

    <div class="ui small modal confirmation">
        <div class="header">Confirmation</div>
        <div class="content">
            <p id="contentApllicant">diisi via js nanti</p>
        </div>
        <div class="actions">
            <div class="ui approve green button">Yes</div>
            <div class="ui cancel red button">No</div>
        </div>
    </div>

    <script type="text/javascript">
        /**
         * HARDCODE
         */
        $('.datepickerDeaStartDate').datepicker({minDate: new Date(2020, 1, 17), maxDate: new Date(2021, 1, 28)}); //1 itu bulan februari, idx mulai dari 0
        /**
         * ENDOF HARDCODE
         */

        ///////////     JOB POSITION AJAX   /////////
        function loadJobPosition() {
            let degrees = [];
            $('.inputCheckboxDegree').each(function (index, checkbox) {
                if ($(checkbox).is(':checked')) {
                    degrees.push($(checkbox).attr('degree_id'));
                }
            });
            $.ajax({
                type: 'GET',
                url: "{{ url('/ajax/job_position') }}",
                data: {'degrees': degrees},
                success: function (data) {
                    // $('#cmbPosition').html('<option value="">Job Position</option>');
                    $('#cmbPosition').html('');
                    data.forEach(function (position) {
                        let positionName = position.name;


                        // let str = '<option value="' + positionName + '">' + positionName + '</option>';
                        let str = `<div class="item" data-value="${positionName}">${positionName}</div>`;
                        $('#cmbPosition').append(str); // CHANGE THE ID/CLASS

                    });
                },
                error: function () {
                    console.log("ERROR: AJAX");
                }
            });
        }

        function initJobPosition() {
            $('.inputCheckboxDegree').each(function (index, checkbox) {
                $(checkbox).change(function () {
                    loadJobPosition();
                });
            });
        }

        initJobPosition();
        loadJobPosition();
        /////////// END OF JOB POSITION AJAX /////////

        $('.accordion').accordion();

        function calculateEndDate(e) {
            let deadlineInput = $('#txtStartDate');
            let duration = $('#datepickerDur').val();
            if (duration != '' && deadlineInput.val() != '') {
                let date = new Date(deadlineInput.val());
                let endDate = new Date(date.setMonth(date.getMonth() + parseInt(duration)));
                let formattedDate = (date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear();
                $('#txtEndDate').val(formattedDate);
            }
        }

        $('#txtStartDate').change(calculateEndDate);
        $('#datepickerDur').change(calculateEndDate);

        var dataToken = "";
        var dataName = "";

        $(document).on("click", ".ui.button.delete.job", function () {
            dataToken = $(this).data("value").slice(3, 63);
            dataName = $(this).data("value").slice(63);
            document.getElementById("contentApllicant").innerHTML = "Are you sure you want to delete job " + dataName + " ?";
        });
        $('.ui.small.modal.confirmation').modal("setting", {
            blurring: true,
            onApprove: function () {
                window.location.href = "{{ url('/deleteJob') }}" + '/' + dataToken;
            }
        }).modal('attach events', '.ui.button.delete.job', 'show');
        $(document).on('change', '#checkboxLinkTest', function () {
            if ($('[name="checkboxLinkTest"]').is(':checked') == true) {
                $('#fieldLinkTest').show();
            } else {
                $('#fieldLinkTest').hide();
            }
        });

        // $(document).on('change', '[name="txtDuration"]', function(e){
        //   var value = $('[name="txtDuration"]').val();
        //   if(value == '6'){
        //     var degrees_six_month = $('#durationOption6').attr('availProgram');
        //     var split = degrees_six_month.split(";");
        //     $('.checkboxDegree').removeClass('disabled');
        //     $('.checkboxDegree').addClass('disabled');
        //     $('.inputCheckboxDegree').prop('checked', false);
        //     split.forEach(function(currentValue, index, arr){
        //       // $('[name="degreei_'+ currentValue +'"]').prop('checked', true);
        //       $('.checkboxDegree[degree_id="'+ currentValue +'"]').removeClass('disabled');
        //     });
        //   }
        //   else if(value == '12'){
        //     var degrees_twelve_month = $('#durationOption12').attr('availProgram');
        //     var split = degrees_twelve_month.split(";");
        //     $('.checkboxDegree').removeClass('disabled');
        //     $('.checkboxDegree').addClass('disabled');
        //     $('.inputCheckboxDegree').prop('checked', false);
        //     split.forEach(function(currentValue, index, arr){
        //       // $('[name="degreei_'+ currentValue +'"]').prop('checked', true);
        //       $('.checkboxDegree[degree_id="'+ currentValue +'"]').removeClass('disabled');
        //     });
        //   }
        // });
    </script>
@stop
  