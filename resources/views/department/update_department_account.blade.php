@extends ('layout.masterD')

@section ('title', 'BINUS')

@section('content')

    @include('partial/headerDepartment')

    <br><br>

    @if (session('fyi'))
        <div class="row">
            <div class="ui positive message">
                <i class="close icon"></i>
                <div class="header">
                    {{session('fyi')}}
                </div>
            </div>
        </div>
    @endif

    @if($errors->first() != null)
        <div class="row">
            <div class="ui negative message">
                <p>{{$errors->first()}}</p>
            </div>
        </div>
    @endif

    @if(!empty(session('error')))
        <div class="row">
            <div class="ui negative message">
                <p>{{ session('error') }}</p>
            </div>
        </div>
    @endif

    <br><br>

    <div class="ui container">
        <h2>Update Account [{{ $department['name'] }}]</h2>
        {!! Form::open(['url' => 'submitUpdateDepartmentAccount/'.$department['departmentid'], 'method' => 'post', 'class' => 'ui form']) !!}
        <div class="field">
            <label>User ID</label>
            <input type="text" value="{{ $department['departmentid'] }}" disabled>
        </div>
        <div class="field">
            <label>Name</label>
            <input type="text" name="txt_name" placeholder="Name" value="{{ $department['name'] }}" required>
        </div>
        <div class="field">
            <label>Email</label>
            <input type="email" name="txt_email" placeholder="Email" value="{{ $department['email'] }}" required>
        </div>
        <div class="field">
            <label>Default Password</label>
            <input type="password" name="txt_password" placeholder="Default Password"
                   value="{{ $department['defaultpassword'] }}" required>
        </div>
        <div class="field">
            <label>Confirm Default Password</label>
            <input type="password" name="txt_confirm_password" placeholder="Confirm Default Password"
                   value="{{ $department['defaultpassword'] }}" required>
        </div>
        <div class="field">
            <label>Campus</label>
            <select name="cmb_campus">
                <option value="" disabled="disabled" selected="selected">-- Choose --</option>
                <option value="all" @if($department['campus'] == null) selected="selected" @endif">All</option>
                @foreach(\App\Campus::all() as $c)
                    <option value="{{ $c->name }}" @if($department['campus'] == $c->name) selected="selected" @endif
                    ">{{ $c->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="field">
            <label>Access Role</label>
            <select name="cmb_access_role">
                <option value="" disabled="disabled" selected="selected">-- Choose --</option>
                @foreach(\App\Department::getAllAccessRoles() as $key => $name)
                    <option value="{{ $key }}" {{ $department['access_role'] == $key ? 'selected' : '' }}>{{ $name }}</option>
                @endforeach
            </select>
        </div>
        <div class="field">
            <button class="ui green button">Update</button>
        </div>
        {!! Form::close() !!}
        <br><br>
    </div>

@stop
