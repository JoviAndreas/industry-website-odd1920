@extends ('layout.masterD')

@section ('title', 'BINUS')

@section('content')

    @include('partial/headerDepartment')

    <script>

        $(document).ready(function () {

            // Filter By Faculty
            $('#buttonFilterByFaculty').change(function (e) {
                $('#formFilterByFaculty').submit();
            });
            $('#buttonFilterByCampus').change(function (e) {
                $('#formFilterByFaculty').submit();
            });

            $('#wait_table').DataTable();
            $('#accept_table').DataTable();
            $('#nojob_table').DataTable();
            $('#noapply_table').DataTable();
            $('#waitC_table').DataTable();
            $('#process_table').DataTable();
            $('#acceptC_table').DataTable();

            $(window).scroll(function () {
                if ($(this).scrollTop() > 100) {
                    $('.scrollToTop').fadeIn();
                } else {
                    $('.scrollToTop').fadeOut();
                }
            });

            //Click event to scroll to top
            $('.scrollToTop').click(function () {
                $('html, body').animate({scrollTop: 0}, 800);
                return false;
            });

            //Student CV Chart
            var ctxCV = document.getElementById("chartCanvasCV").getContext("2d");
            var chartCV = new Chart(ctxCV, {
                type: 'doughnut',
                data: {
                    labels: ["Has CV", "No CV"],
                    datasets: [{
                        label: '',
                        data: [{{ sizeof($studentCV['yes']) }}, {{ sizeof($studentCV['no']) }}],
                        backgroundColor: ["#6FB272", "#C7515F"]
                    }]
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: true,
                    title: {
                        display: true,
                        text: "Student CV Summary [Has CV/Total = {{ sizeof($studentCV['yes']) }}/{{ $total_students }}]",
                        fontFamily: "Verdana",
                        fontColor: "Black",
                        fontSize: 20
                    },
                    animation: {
                        animateRotate: true
                    }
                }
            });

            //Student Applied or not Chart
            var ctxCV = document.getElementById("chartCanvasApply").getContext("2d");
            var chartCV = new Chart(ctxCV, {
                type: 'doughnut',
                data: {
                    labels: ["Has Applied", "Not Applied"],
                    datasets: [{
                        label: '',
                        data: [{{ sizeof($studentApply['yes']) }}, {{ sizeof($studentApply['no']) }}],
                        backgroundColor: ["#6FB272", "#C7515F"]
                    }]
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: true,
                    title: {
                        display: true,
                        text: "Student Apply Summary [Applied/Total = {{ sizeof($studentApply['yes']) }}/{{ $total_students }}]",
                        fontFamily: "Verdana",
                        fontColor: "Black",
                        fontSize: 20
                    },
                    animation: {
                        animateRotate: true
                    }
                }
            });

{{--                    @if(\App\Department::isSuperAdmin())--}}
            @if (true)
            var ctx1 = document.getElementById("chartCanvas1").getContext("2d");
            var chart1 = new Chart(ctx1, {
                type: 'horizontalBar',
                data: {
                    labels: ["Accepted", "Not Accepted"],
                    datasets: [{
                        label: 'Number of Student(s)',
                        data: [{{ sizeof($student['accept']) }}, {{ sizeof($student['wait']) }}],
                        backgroundColor: "#62C9C3"
                    }]
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: true,
                    title: {
                        display: true,
                        text: "Student Summary  [Accepted/Total = {{ sizeof($student['accept']) }}/{{ $total_students }}]",
                        fontFamily: "Verdana",
                        fontColor: "Black",
                        fontSize: 20
                    },
                    scales: {
                        xAxes: [{
                            ticks: {
                                beginAtZero: true,
                                min: 0,
                                max: {{ sizeof($student['accept']) }} + {{ sizeof($student['wait']) }}
                            }
                        }],
                        yAxes: [{
                            ticks: {
                                fontFamily: "Verdana",
                                fontSize: "18",
                                fontColor: "black"
                            }
                        }]
                    }
                }
            });

            var ctx2 = document.getElementById("chartCanvas2").getContext("2d");
            var chart2 = new Chart(ctx2, {
                type: 'doughnut',
                data: {
                    labels: ["Student accepted by Company", "Student processed by Company (Interview/Test)", "Student waiting to be processed by Company", "Student not Applied to Company", "Company with no job"],
                    datasets: [{
                        label: '',
                        data: [
                            {{sizeof($company['accept'])}},
                            {{sizeof($company['process'])}},
                            {{sizeof($company['wait'])}},
                            {{sizeof($company['noapply'])}},
                            {{sizeof($company['nojob'])}}
                        ],
                        backgroundColor: ["#6FB272", "#97C1FF", "#FFDA71", "#B26C53", "#CC374A"]
                    }]
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: true,
                    title: {
                        display: true,
                        text: "Company Summary [Accept/Process/Wait/NoApply/NoJob/Total = {{sizeof($company['accept'])}}/{{sizeof($company['process'])}}/{{sizeof($company['wait'])}}/{{sizeof($company['noapply'])}}/{{sizeof($company['nojob'])}}/{{sizeof($company['accept'])+sizeof($company['process'])+sizeof($company['wait'])+sizeof($company['noapply'])+sizeof($company['nojob'])}}]",
                        fontFamily: "Verdana",
                        fontColor: "Black",
                        fontSize: 20
                    },
                    animation: {
                        animateRotate: true
                    }
                }
            });

            var ctx3 = document.getElementById("chartCanvas3").getContext("2d");
            var chart3 = new Chart(ctx3, {
                type: 'doughnut',
                data: {
                    labels: ["Available Quota", "Filled"],
                    datasets: [{
                        label: '',
                        data: [
                            {{ $total_quota - $filled }},
                            {{ $filled }}
                        ],
                        backgroundColor: ["#6FB272", "#CC374A"]
                    }]
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: true,
                    title: {
                        display: true,
                        text: "Jobs Quota Summary [Filled/Available Quota/Total Quota = {{$filled}}/{{$available_quota}}/{{ $total_quota }}]",
                        fontFamily: "Verdana",
                        fontColor: "Black",
                        fontSize: 20
                    },
                    animation: {
                        animateRotate: true
                    }
                }
            });

            var ctx5 = document.getElementById("chartCanvas5").getContext("2d");
            var chart5 = new Chart(ctx5, {
                type: 'bar',
                data: {
                    labels: [
                        @foreach($top_ten as $t)
                            "{{ $t->company_name }}",
                        @endforeach
                    ],
                    datasets: [{
                        label: 'Student(s) applied',
                        data: [
                            @foreach($top_ten as $t)
                            {{ $t->how_many_apply }},
                            @endforeach
                        ],
                        backgroundColor: "#62C9C3"
                    }]
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: true,
                    title: {
                        display: true,
                        text: "Top 10 Most Apply Companies",
                        fontFamily: "Verdana",
                        fontColor: "Black",
                        fontSize: 20
                    },
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true,
                                min: 0
                            }
                        }],
                        xAxes: [{
                            ticks: {
                                fontFamily: "Verdana",
                                fontSize: 12,
                                fontColor: "black",
                                stepSize: 0,
                                autoSkip: false
                            },
                        }]
                    }
                }
            });

                    @else

            var ctx4 = document.getElementById("chartCanvas4").getContext("2d");
            var chart4 = new Chart(ctx4, {
                type: 'pie',
                data: {
                    labels: ["Not Approved", "Approved"],
                    datasets: [{
                        label: '',
                        data: [
                            {{ $total_students - $total_students_approved }},
                            {{ $total_students_approved }}
                        ],
                        backgroundColor: ["#CC374A", "#6FB272"]
                    }]
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: true,
                    title: {
                        display: true,
                        text: "Approved Summary [Total Student = {{ $total_students }}]",
                        fontFamily: "Verdana",
                        fontColor: "Black",
                        fontSize: 20
                    },
                    animation: {
                        animateRotate: true
                    }
                }
            });

            @endif
        });
    </script>

    <br><br>

    <div class="ui container">

        <div class="ui basic segment">
            <p>You are currently logged in
                as {{ \App\Department::getAllAccessRoles()[Auth::user()->department->access_role] }}</p>
            {!! Form::open(['url' => route('department.home'), 'method' => 'get', 'role' => 'form', 'id' => 'formFilterByFaculty', 'class' => 'ui form']) !!}
            <input type="hidden" name="filter_change" value="true">
            <div class="fields">
                <div class="inline field">
                    @if (Auth::user()->department->campus != null)
                        <b>Campus : {{ Auth::user()->department->campus }}</b>
                    @else
                        <label>Campus Filter:</label>
                        <select id="buttonFilterByCampus" name="filterCampus">
                            <option value="" selected>All Campus</option>
                            @foreach(\App\Campus::all() as $c)
                                <option value="{{ $c->id }}" @if($filter_campus == $c->id) selected @endif>{{ $c->name }}</option>
                            @endforeach
                        </select>
                    @endif
                </div>
            </div>
            <div class="fields">
                <div class="inline field">
                    <label>Faculty Filter:</label>
                    <select id="buttonFilterByFaculty" name="filter">
                        <option value="all">All Faculty & Programs</option>
                        @foreach($faculties_can_be_chosen as $f)
                            <option value="{{ $f->id }}"
                                    @if(!empty($filter_faculty) && $filter_faculty == $f->id) selected @endif>{{ $f->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            {!! Form::close() !!}
            <br>
            @if ($filter_faculty != 'all')
            {!! Form::open(['url' => 'changeHeadOfProgram', 'method' => 'get', 'role' => 'form', 'class' => 'ui form']) !!}
            <input type="hidden" name="filterCampus" value="{{ $filter_campus }}">
            <div class="fields">
                <div class="inline field">
                    <label>Program Filter:</label>
                    <input type="hidden" name="head_of_faculty" value="{{ $filter_faculty }}">
                    <select name="head_of_program">
                        @if(Auth::user()->department()->first()->head_of_program == 'all')
                            <option value="all">All Degree
                                in {{ \App\Faculty::where('id', $filter_faculty)->first()->name }}</option>
                        @endif
                        @foreach($degrees_can_be_chosen as $d)
                            <option value="{{ $d->id }}"
                                    @if(!empty(session('head_of_program')) && session('head_of_program') == $d->id) selected @endif>{{ $d->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="fields">
                <button class="ui tiny primary button">Filter</button>
            </div>
            {!! Form::close() !!}
            @endif
        </div>

        {{-- @if(session('head_of_program') == 'all') --}}
        @if(\App\Department::isSuperAdmin())
            <h3 class="ui horizontal divider header">
                Summary
            </h3>
        @endif
        <div id="chartContainerCV">
            <canvas id="chartCanvasCV" width="800px" height="150px"></canvas>
            <center>
                <a class="ui green button" type="submit" target="_blank" href="{{ url('/department/export/studentCVSummary?filter=' . $filter_faculty . '&filterCampus=' . $filter_campus) }}">Export Student CV Summary to Excel</a>
            </center>
            <br><br>
        </div>
        <div id="chartContainerApply">
            <canvas id="chartCanvasApply" width="800px" height="150px"></canvas>
            <center>
                <a class="ui green button" type="submit" target="_blank" href="{{ url('/department/export/studentApplySummary?filter=' . $filter_faculty . '&filterCampus=' . $filter_campus) }}">Export Student Apply Summary to Excel</a>
            </center>
            <br><br>
        </div>
        {{-- @if(session('head_of_program') == 'all') --}}
        @if(\App\Department::isSuperAdmin())
            <div id="chartContainer1">
                <canvas id="chartCanvas1" width="800px" height="150px"></canvas>
            </div>
            <div id="chartContainer2">
                <canvas id="chartCanvas2" width="800px" height="200px"></canvas>
            </div>
            <div id="chartContainer3">
                <canvas id="chartCanvas3" width="800px" height="200px"></canvas>
                <center>
                    <a class="ui green button" type="submit" target="_blank" href="{{ url('/department/export/jobQuotaSummary?filter=' . $filter_faculty . '&filterCampus=' . $filter_campus) }}">Export Job Quota Summary to Excel</a>
                </center>
                <br><br>
            </div>
            <div id="chartContainer5">
                <canvas id="chartCanvas5" width="800px" height="400px"></canvas>
            </div>
        @else
            <div id="chartContainer4">
                <canvas id="chartCanvas4" width="800px" height="200px"></canvas>
            </div>
        @endif

        <br>

        <div class="ui vertical stripe segment">
            <div class="ui middle aligned stackable container">
                <h3 class="ui horizontal divider header">Student Detail</h3>

                <!--Accepted Student-->
                <h4 id="acceptStudent" class="ui green center aligned horizontal divider header">Accepted</h4>
                <div class="row">
                    <table class="ui basic padded table" id="accept_table">
                        <thead>
                        <tr>
                            <th class="two wide">NIM</th>
                            <th class="two wide">Name</th>
                            <th class="two wide">Program</th>
                            <th class="two wide">Sex</th>
                            <th class="two wide">Phone</th>
                            <th class="two wide">Email</th>
                            <th class="two wide">Company Name</th>
                            <th class="two wide">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($student['accept'] as $s)
                            <tr>
                                <td><a href="{{url('/')}}/profile/{{$s->studentid}}">{{$s->studentid}}</a></td>
                                <td>{{$s->name}}</td>
                                <td>{{$s->degree_name}}</td>
                                <td>{{$s->sex}}</td>
                                <td>{{$s->phone}}</td>
                                <td>{{$s->email}}</td>
                                <td>{{$s->company_name}}</td>
                                <td>
                                    @if($s->cv == 1)
                                        <a class="ui button" href="{{ url('/') }}/previewcvs/{{$s->studentid}}"
                                           target="_blank">
                                            Preview </a>
                                    @else
                                        CV Not Available !
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

                <!--Waiting Student-->
                <h4 id="waitStudent" class="ui yellow center aligned horizontal divider header">Waiting</h4>
                <a class="ui green button" type="submit" target="_blank" href="{{ url('/department/export/waitingStudent?filter=' . $filter_faculty . '&filterCampus=' . $filter_campus) }}">Export to Excel</a>
                <br>
                <br>
                <div class="row">
                    <table class="ui basic padded table" id="wait_table">
                        <thead>
                        <tr>
                            <th class="two wide">NIM</th>
                            <th class="two wide">Name</th>
                            <th class="two wide">Program</th>
                            <th class="two wide">Sex</th>
                            <th class="two wide">Phone</th>
                            <th class="two wide">Email</th>
                            <th class="four wide">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($student['wait'] as $s)
                            <tr>
                                <td><a href="{{url('/')}}/profile/{{$s->studentid}}">{{$s->studentid}}</a></td>
                                <td>{{$s->name}}</td>
                                <td>{{$s->degree_name}}</td>
                                <td>{{$s->sex}}</td>
                                <td>{{$s->phone}}</td>
                                <td>{{$s->email}}</td>
                                <td>
                                    @if($s->cv == 1)
                                        <a class="ui button" href="{{ url('/') }}/previewcvs/{{$s->studentid}}"
                                           target="_blank">
                                            Preview </a>
                                    @else
                                        CV Not Available
                                    @endif
                                    <a class="ui button" id="show_job_preview" studentid="{{ $s->studentid }}">Preview
                                        Job</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

                <div class="ui modal" id="modal_job_preview">
                    <div class="header">List Job</div>
                    <div class="content">
                        <table class="ui basic padded table" id="job_table">
                            <thead>
                            <tr>
                                <th class="four wide">Job Name</th>
                                <th class="four wide">Company Name</th>
                                <th class="two wide">Status</th>
                                <th class="six wide">Last Update</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                    <div class="actions">
                        <div class="ui red deny button">Close</div>
                    </div>
                </div>

                <script>
                    var job_table = $('#job_table').DataTable({
                        "columns": [
                            {data: 'jobname'},
                            {data: 'companyname'},
                            {data: 'status'},
                            {data: 'time'}
                        ],
                        "ajax": "{{ url('/') }}/getStudentJobApplicationList/0",
                        "paging": false,
                        "info": false,
                        "searching": false
                    });
                    $(document).on('click', '#show_job_preview', function (e) {
                        var studentid = e.target.attributes.studentid.value;
                        job_table.ajax.url('{{ url("/") }}/getStudentJobApplicationList/' + studentid).load();
                        $('#modal_job_preview').modal('show');
                    });
                </script>

                <br><br><br>
                <h3 class="ui horizontal divider header">
                    Company Detail
                </h3>

                @if (\App\Department::isSuperAdmin() || \App\Department::isAdmin())
                <h4 id="nojobCompany" class="ui red center aligned horizontal divider header">No Job</h4>
                <div class="row">
                    <table class="ui basic padded table" id="nojob_table">
                        <thead>
                        <tr>
                            <th class="two wide">Id</th>
                            <th class="three wide">Name</th>
                            <th class="two wide">Phone</th>
                            <th class="two wide">PIC</th>
                            <th class="three wide">Email</th>
                            <th class="four wide">Website</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($company['nojob'] as $s)
                            <tr>
                                <td>{{$s->companyid}}</td>
                                <td>{{$s->name}}</td>
                                <td>{{$s->phone}}</td>
                                <td>{{$s->pic}}</td>
                                <td>{{$s->email}}</td>
                                <td>{{$s->website}}</td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>
                @endif


                <!-- No Apply-->
                <h4 id="noapplyCompany" class="ui orange center aligned horizontal divider header">No Apply</h4>
                <div class="row">
                    <table class="ui basic padded table" id="noapply_table">
                        <thead>
                        <tr>
                            <th class="two wide">Id</th>
                            <th class="three wide">Name</th>
                            <th class="two wide">Phone</th>
                            <th class="two wide">PIC</th>
                            <th class="three wide">Email</th>
                            <th class="four wide">Website</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($company['noapply'] as $s)
                            <tr>
                                <td>{{$s->companyid}}</td>
                                <td>{{$s->name}}</td>
                                <td>{{$s->phone}}</td>
                                <td>{{$s->pic}}</td>
                                <td>{{$s->email}}</td>
                                <td>{{$s->website}}</td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>


                <!-- Wait-->
                <h4 id="waitCompany" class="ui yellow center aligned horizontal divider header">Wait</h4>
                <div class="row">
                    <table class="ui basic padded table" id="waitC_table">
                        <thead>
                        <tr>
                            <th class="two wide">Id</th>
                            <th class="three wide">Name</th>
                            <th class="two wide">Phone</th>
                            <th class="two wide">PIC</th>
                            <th class="three wide">Email</th>
                            <th class="four wide">Website</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($company['wait'] as $s)
                            <tr>
                                <td>{{$s->companyid}}</td>
                                <td>{{$s->name}}</td>
                                <td>{{$s->phone}}</td>
                                <td>{{$s->pic}}</td>
                                <td>{{$s->email}}</td>
                                <td>{{$s->website}}</td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>


                <!-- Process-->
                <h4 id="processCompany" class="ui olive center aligned horizontal divider header">Process</h4>
                <div class="row">
                    <table class="ui basic padded table" id="process_table">
                        <thead>
                        <tr>
                            <th class="two wide">Id</th>
                            <th class="three wide">Name</th>
                            <th class="two wide">Phone</th>
                            <th class="two wide">PIC</th>
                            <th class="three wide">Email</th>
                            <th class="four wide">Website</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($company['process'] as $s)
                            <tr>
                                <td>{{$s->companyid}}</td>
                                <td>{{$s->name}}</td>
                                <td>{{$s->phone}}</td>
                                <td>{{$s->pic}}</td>
                                <td>{{$s->email}}</td>
                                <td>{{$s->website}}</td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>

                <!-- Accept-->
                <h4 id="acceptCompany" class="ui green center aligned horizontal divider header">Accept</h4>
                <div class="row">
                    <table class="ui basic padded table" id="acceptC_table">
                        <thead>
                        <tr>
                            <th class="two wide">Id</th>
                            <th class="three wide">Name</th>
                            <th class="two wide">Phone</th>
                            <th class="two wide">PIC</th>
                            <th class="three wide">Email</th>
                            <th class="four wide">Website</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($company['accept'] as $s)
                            <tr>
                                <td>{{$s->companyid}}</td>
                                <td>{{$s->name}}</td>
                                <td>{{$s->phone}}</td>
                                <td>{{$s->pic}}</td>
                                <td>{{$s->email}}</td>
                                <td>{{$s->website}}</td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>

            </div>
        </div>

    </div>

    <a href="#" class="scrollToTop">
        <i class="arrow up icon"></i><br>
        Scroll To Top
    </a>
@endsection
