@extends ('layout.masterD')

@section ('title', 'BINUS')

@section('content')

@include('partial/headerDepartment')

<br><br>

<div class="ui container">

@if($errors->first() != null)
    <div class="ui red message">{{$errors->first()}}</div>
@endif
@if (session('fyi') )
    <div class="ui positive message">
        {{ session('fyi') }}
    </div>
@endif

<div class="ui segment" style="padding: 10px;">
    <div class="ui header">Insert University Supervisor</div>
    <div class="ui divider"></div>
    {!! Form::open(['url' => 'submitGuiderByExcel', 'enctype' => 'multipart/form-data','method' => 'post', 'role' => 'form', 'class' => 'ui large form']) !!}
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <div class="field">
        <input type="file"  name="fileexcel">
      </div>
      <div class="field">
          <button class="ui primary button">Submit University Supervisor</button>&nbsp;<a href="{{ url('/') }}/templateguider"/>Download Template here</a>
      </div>
    {!! Form::close() !!}
</div>

<div class="ui header">List University Supervisor</div>
    <table class="ui celled table" id="univ_supervisor_table">
      <thead>
        <tr>
          <th class="two wide">Lecturer Code</th>
          <th class="four wide">Name</th>
          <th class="four wide">Email</th>
          <th class="three wide">Phone</th>
          <th class="four wide">Action</th>
        </tr>
      </thead>
      <tbody>
        @foreach($univ_supervisor_list as $u)
        <tr>
          <td>{{ $u->lecturerid }}</td>
          <td>{{ $u->name }}</td>
          <td>{{ $u->email }}</td>
          <td>{{ $u->phone }}</td>
          <td><a href="/delete_univ_supervisor/{{ $u->id }}" class="ui red button" onclick="return confirm('Are you sure you want to delete ?')">Delete</a></td>
        </tr>
        @endforeach
      </tbody>
    </table>
</div>

<br><br>
<script>
    $(document).ready(function(){
        $('#univ_supervisor_table').DataTable();
    });
</script>
@stop
