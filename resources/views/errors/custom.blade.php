@extends('errors.layout')

@section('title', 'Not Found')
@section('content')
    <div class="code">
        ERROR
    </div>

    <div class="message">
        Unknown Error
    </div>
@endsection