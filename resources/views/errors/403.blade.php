@extends('errors.layout')

@section('title', 'Not Found')
@section('content')
    <div class="code">
        403
    </div>

    <div class="message">
        Forbidden
    </div>
@endsection