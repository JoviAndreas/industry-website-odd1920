@extends('errors.layout')

@section('title', 'Not Found')
@section('content')
    <div class="code">
        404
    </div>

    <div class="message">
        Not Found
    </div>
@endsection