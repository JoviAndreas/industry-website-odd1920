@extends('errors.layout')

@section('title', 'Not Found')
@section('content')
    <div class="code">
        500
    </div>

    <div class="message">
        Server Error
    </div>
@endsection