@extends ('layout.master')

@section ('title', 'BINUS')

@section('content')

@if (Auth::user()->role=='department')
  @include('partial/headerDepartment')
@elseif (Auth::user()->role=='company')
  @include('partial/headerCompany')
@else
  @include('partial/headerStudent')
@endif
<br><br>
    <div class="ui middle aligned stackable grid container">
        @if(session('sukses'))
        <div class="row">
          <div class="ui positive message">
            <i class="close icon"></i>
            <div class="header">
              {{session('sukses')}}
            </div>
          </div>
        </div>
      @endif

      <div class="row" style=" align-items: flex-start;">
        <div class="four wide column" style="align-self: flex-start !important;">
          <img style="max-height: 375px; max-width: 260px;" src="{{ url('images/'.$userProfile->studentid) }}" onerror="this.src='{{ URL::asset('assets/images/noimage.jpg') }}'">
        </div>
        <div class="twelve wide column">
          <div class="row">
            <h2 class="ui header">{{ $userProfile->studentid }} &nbsp;
            @if(Auth::user()->role == 'department')
            <a class="ui button" href="{{ url('/') }}/updateprofilestudent/{{ $userProfile->studentid }}" >
              Update Profile & Generate CV
            </a>
            <a class="ui brown button" href="{{ url('/') }}/resend ApprovalEmail/{{ $userProfile->studentid }}" >
              Resend Approval Email
            </a>
{{--            <a class="ui red button" href="{{ url('/') }}/resetPasswordStudent/{{ $userProfile->studentid }}" onclick="return confirm('Are you sure you want to reset this account password ?');">--}}
{{--              Reset Password--}}
{{--            </a>--}}
            @endif
            @if(Auth::user()->userid == $userProfile->studentid)
            <a class="ui button" href="{{ url('/') }}/updateprofile" >
              Update Profile & Generate CV
            </a>
            <a class="ui button" href="{{ url('/') }}/changepassword" >
              Change Password
            </a>
            <a class="ui button" href="{{ url('/') }}/student/grades/generate" >
              Generate Grades
            </a>
            @endif
            </h2>
          </div>
          <br/>
          <div class="row">
            <h2 class="ui header">{{ $userProfile->name }}</h2>
          </div>
          @if($userProfile->linkportofolio != "")
          <br/>
            <div class="row">
            <h4 class="ui header"><a target="_blank" href="{{$userProfile->linkportofolio}}">Click here for Portofolio</a></h4>
          </div>
          @endif
          <br/>
           <div class="row">
            <div class="ui segment" style="padding: 1%;">
              <div class="row"><b>Academic Information</b></div>
              <br/>
              <div class="ui grid" style="padding-bottom: 0 auto; padding-top: 0 auto;">
                <div class="three wide column">Semester</div>
                <div class="twelve wide column">{{$userProfile->semester}}</div>
              </div>
              <div class="ui grid" style="padding-bottom: 0 auto; padding-top: 0 auto;">
                <div class="three wide column">Program</div>
                <div class="twelve wide column">{{ $userDegree != null ? $userDegree->name : '' }}</div>
              </div>
              <div class="ui grid" style="padding-bottom: 0 auto; padding-top: 0 auto;">
                <div class="three wide column">Major</div>
                <div class="twelve wide column">{{ $userProfile->major }}</div>
              </div>
            </div>
           <div class="row">
            <div class="ui segment" style="padding: 1%;">
              <div class="row"><b>Personal Information</b></div>
              <br/>
              <div class="ui grid" style="padding-bottom: 0 auto; padding-top: 0 auto;">
                <div class="three wide column">Gender</div>
                <div class="twelve wide column">{{$userProfile->sex}}</div>
              </div>
              <div class="ui grid" style="padding-bottom: 0 auto; padding-top: 0 auto;">
                <div class="three wide column">DOB</div>
                <div class="twelve wide column">{{ $userProfile->dob }}</div>
              </div>
               <div class="ui grid" style="padding-bottom: 0 auto; padding-top: 0 auto;">
                <div class="three wide column">Address</div>
                <div class="twelve wide column">{{ $userProfile->address }}</div>
              </div>
            </div></div>
           <br/>
          <div class="row">
            <div class="ui segment" style="padding: 1%;">
              <div class="row"><b>Contact Me</b></div>
              <br/>
              <div class="ui grid" style="padding-bottom: 0 auto; padding-top: 0 auto;">
                <div class="three wide column">Email</div>
                <div class="twelve wide column"><a href="mailto:{{ $userProfile->email }}" target="_top">{{ $userProfile->email }}</a></div>
              </div>
              <div class="ui grid" style="padding-bottom: 0 auto; padding-top: 0 auto;">
                <div class="three wide column">Phone</div>
                <div class="twelve wide column">{{ $userProfile->phone }}</div>
              </div>
            </div>
          </div>
          <br/>
          <div class="row">
            <div class="ui segment" style="padding: 1%;">
              <div class="row"><b>Social Network</b></div>
              <br/>
              <div class="ui grid" style="padding-bottom: 0 auto; padding-top: 0 auto;">
                <div class="three wide column">Facebook</div>
                @if(!empty($userProfile->facebook) && (strpos($userProfile->facebook, 'http://') === 0 || strpos($userProfile->facebook, 'https://') === 0))
                <div class="twelve wide column"><a href="{{ $userProfile->facebook }}" target="_blank">{{ $userProfile->facebook }}</a></div>
                @else
                <div class="twelve wide column">{{ $userProfile->facebook }}</div>
                @endif
              </div>
              <div class="ui grid" style="padding-bottom: 0 auto; padding-top: 0 auto;">
                <div class="three wide column">LinkedIn</div>
                @if(!empty($userProfile->linkedin) && (strpos($userProfile->linkedin, 'http://') === 0 || strpos($userProfile->linkedin, 'https://') === 0))
                <div class="twelve wide column"><a href="{{ $userProfile->linkedin }}" target="_blank">{{ $userProfile->linkedin }}</a></div>
                @else
                <div class="twelve wide column">{{ $userProfile->linkedin }}</div>
                @endif
              </div>
              <div class="ui grid" style="padding-bottom: 0 auto; padding-top: 0 auto;">
                <div class="three wide column">Line</div>
                <div class="twelve wide column">{{ $userProfile->line }}</div>
              </div>
            </div>
          </div>
          @if(sizeof($subject)>0)
          <br>
          <div class="row">
            <div class="ui segment" style="padding: 1%;">
              <div class="row"><b>Subject Grade</b></div>
              <br>
              <div class="ui button showModalGrade">Preview</div>
            </div>
          </div>
          @endif
          @if(sizeof($semester)>0)
          <br>
          <div class="row">
            <div class="ui segment" style="padding: 1%;">
              <div class="row"><b>GPA History</b></div>
              <br>
              <table class="ui celled table">
                <thead>
                  <tr>
                    <th class="four wide">Semester</th>
                    <th class="seven wide">GPA</th>
                  </tr>
                </thead>
                <tbody>
                @foreach($semester as $s)
                  <tr>
                    <td>{{$s->semester}}</td>
                    <td>{{$s->ipk}}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
          @endif
          @if(sizeof($formal)>0)
          <br>
          <div class="row">
            <div class="ui segment" style="padding: 1%;">
              <div class="row"><b>Formal Education</b></div>
              <br>
               <table class="ui celled table">
                <thead>
                  <tr>
                    <th class="five wide">Start Date</th>
                    <th class="five wide">End Date</th>
                    <th class="six wide">Description</th>
                  </tr>
                </thead>
                <tbody>
                @foreach($formal as $s)
                  <tr>
                    <td>{{$s->startdate}}</td>
                    <td>{{$s->enddate}}</td>
                    <td>{{$s->description}}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
          @endif
          @if(sizeof($informal)>0)
          <br>
          <div class="row">
            <div class="ui segment" style="padding: 1%;">
              <div class="row"><b>Informal Education</b></div>
              <br>
                <table class="ui celled table">
                  <thead>
                    <tr>
                      <th class="five wide">Start Date</th>
                      <th class="five wide">End Date</th>
                      <th class="six wide">Description</th>
                    </tr>
                  </thead>
                  <tbody>
                  @foreach($informal as $s)
                    <tr>
                      <td>{{$s->startdate}}</td>
                      <td>{{$s->enddate}}</td>
                      <td>{{$s->description}}</td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
            </div>
          </div>
          @endif
          @if(sizeof($org)>0)
          <br>
          <div class="row">
            <div class="ui segment" style="padding: 1%;">
              <div class="row"><b>Organizational/Working Experiences</b></div>
              <br>
                <table class="ui celled table">
                  <thead>
                    <tr>
                      <th class="three wide">Start Date</th>
                      <th class="three wide">End Date</th>
                      <th class="three wide">Place/Instition</th>
                      <th class="three wide">role/Position</th>
                       <th class="four wide">Job Description</th>
                    </tr>
                  </thead>
                  <tbody>
                  @foreach($org as $s)
                    <tr>
                      <td>{{$s->startdate}}</td>
                      <td>{{$s->enddate}}</td>
                      <td>{{$s->description}}</td>
                      <td>{{$s->role}}</td>
                      <td>{{$s->jobdesc}}</td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
            </div>
          </div>
          @endif
          @if(sizeof($award)>0)
          <br>
          <div class="row">
            <div class="ui segment" style="padding: 1%;">
              <div class="row"><b>Achievements / Awards</b></div>
              <br>
                <table class="ui celled table">
                  <thead>
                    <tr>
                      <th class="five wide">Date</th>
                      <th class="seven wide">Description</th>
                    </tr>
                  </thead>
                  <tbody>
                  @foreach($award as $s)
                    <tr>
                      <td>{{$s->startdate}}</td>
                      <td>{{$s->description}}</td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
            </div>
          </div>
          @endif
          @if(sizeof($skill)>0)
          <br>
          <div class="row">
            <div class="ui segment" style="padding: 1%;">
              <div class="row"><b>Computer / Special Skills</b></div>
              <br>
               <table class="ui celled table">
                <thead>
                  <tr>
                    <th class="three wide">Skills</th>
                    <th class="three wide">Level</th>
                  </tr>
                </thead>
                <tbody>
                @foreach($skill as $s)
                  <tr>
                    <td>{{$s->name}}</td>
                    <td>{{$s->status}}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            <img src="{{ url('/chart/' .$userProfile->studentid) }}" style="width: 100%; height: 100%;">
            </div>
          </div>
          @endif
          @if(sizeof($lang)>0)
          <br>
          <div class="row">
            <div class="ui segment" style="padding: 1%;">
              <div class="row"><b>Language Skills</b></div>
              <br>
               <table class="ui celled table">
                  <thead>
                    <tr>
                      <th class="three wide">Skills</th>
                      <th class="three wide">Level</th>
                    </tr>
                  </thead>
                  <tbody>
                  @foreach($lang as $s)
                    <tr>
                      <td>{{$s->name}}</td>
                      <td>{{$s->status}}</td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
            </div>
          </div>
          @endif

            @if(sizeof($softs) > 0)
               <div class="ui segment" style="padding: 1%;">
                 <center><strong style="color: navy;">Soft Skill</strong></center>
                 <div class="ui divider"></div>
                 <input type="hidden" id="counter5" name="counter5" value="{{ (old('counter5') > 0 ? old('counter5') : ( $userProfile->countlanguage > 0 ? $userProfile->countlanguage : 0 ) ) }}">
                 <table class="ui celled table" id="tableSoftSkill">
                   <thead>
                   <tr>
                     <th class="eight wide column">Skill</th>
                   </tr>
                   </thead>
                   <tbody>
                   @foreach($softs as $s)
                     <tr>
                       <td>{{$s->name}}</td>
                     </tr>
                     @endforeach
                   </tbody>
                 </table>
               </div>
            @endif
             <br>
          <div class="row">
            <div class="ui segment" style="padding: 1%;">
              @if($userProfile->cv == 1)
                @if (Auth::user()->role == 'student')
                <center>
                    <a class="ui button" href="{{ url('/') }}/previewcv" target="_blank">Preview CV</a>
                </center>
                @elseif (Auth::user()->role == 'company' || Auth::user()->role == 'department')
                <center>
                    <a class="ui button" href="{{ url('/') }}/previewcvs/{{$userProfile->studentid}}" target="_blank">Preview CV</a>
                </center>
                @endif
              @endif
            </div>
          </div>
        </div>
      </div>

<script>
      $('.showModalGrade').click(function(e) {
          $('.ui.modal.subject').modal('show');
        });
</script>

</div>
<br><br>


<div class="ui modal subject">
  <div class="header">
    Subject Grade
  </div>
  <div class="content">
   <table class="ui celled table">
          <thead>
            <tr>
              <th class="four wide">Course Code</th>
              <th class="four wide">Course Name</th>
              <th class="four wide">Course Grade</th>
            </tr>
          </thead>
          <tbody>
          @foreach($subject as $s)
            <tr>
              <td>{{$s->code}}</td>
              <td>{{$s->name}}</td>
             <td>{{$s->grade}}</td>
            </tr>
            @endforeach
          </tbody>
        </table>


  </div>
  <div class="actions">
    <div class="ui red deny button">
      Close
    </div>
  </div>
</div>
@stop
