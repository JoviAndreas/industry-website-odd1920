@extends ('layout.masterS')

@section ('title', 'BINUS')

@section('content')

@include('partial/headerStudent')

<br><br>

@if(Auth::user()->student()->first()->cv != 1)
<div class="ui container">
	<div class="ui warning message">
		<div class="header">
			Thank you for registering
		</div>
		<p>
			You just have to complete <strong>one more step</strong>, before applying jobs.<br>
			You have to complete your <strong>CV</strong> first.<br>
			<strong>Please fill in the following fields in your profile:</strong><br>
			@foreach($student->returnEmptyProfileInfo() as $arr)
	        - {{ $arr }}<br>
	        @endforeach
	        You can generate your grades manually on this page <a href="{{ url('/profile') }}">Here</a><br>
			(Go to Profile menu and Update your profile, or click <a href="{{ url('/updateprofile') }}">here</a> Then Submit to Generate your CV)
		</p>
	</div>
</div>
@else
<div class="ui middle aligned stackable grid container">
	@if (session('gagalNih'))
	  <div class="row">
	    <div class="ui negative message">
	      <i class="close icon"></i>
	      <div class="header">
	        {{session('gagalNih')}}
	      </div>
	    </div>
	  </div>
	  @elseif(session('suksesprioritas'))
	  <div class="row">
	    <div class="ui positive message">
	      <i class="close icon"></i>
	      <div class="header">
	        {{session('suksesprioritas')}}
	      </div>
	    </div>
	  </div>
	@endif

	@if($accepted == 'accepted')
	<div class="row">
		<div class="sixteen wide column">
		    <div class="ui positive message">
		      	<div class="header">
		      		Congratulations! You have been accepted to a company.
		      	</div>
		      	<div class="content">
		      		Please wait for the next announcement to download your learning plan.
		      	</div>
		    </div>
		</div>
  	</div>
	@endif
	<div class="row">
		<div class="sixteen wide column">
			<center><h3>Active Job Application</h3></center>
			<table class="ui celled table" id="statusTable">
				<thead>
					<tr>
						<th class="two wide">Company</th>
						<th class="two wide">Position</th>
						<th class="two wide">Duration</th>
						<th class="two wide">Start Date</th>
						<th class="two wide">End Date</th>
						<th class="one wide">Status</th>
						<th class="three wide">Notes</th>
					</tr>
				</thead>
				<tbody>
					@foreach($statuss as $status)
					<tr class="{{$status->status=='approved' ? 'positive' : ($status->status=='rejected'?'negative' : ($status->status=='accepted'?'warning': ($status->status=='process'?'active':'')))}}">
						<td><a href="{{ url('/') }}/profilec/{{$status->companyid}}" >{{ $status->companyname }}</a></td>
						<td>{{ $status->jobname }}</td>
						<td>{{ $status->duration }} Month(s)</td>
						<td>{{ \Carbon\Carbon::parse($status->startdate)->format('d M Y') }}</td>
						<td>{{ \Carbon\Carbon::parse($status->enddate)->format('d M Y') }}</td>
						<td>
							@if($accepted=='accepted' && $status->status != 'approved')
							Closed
							@else
							{{ ucfirst($status->status) }}
							@endif
						</td>
						<td>
						@if($accepted == 'no' && $status->company_status == 'nonactive' && $status->status != 'approved')
						<strong>This company is closed.</strong>
						@else

						@if($accepted=='accepted' && $status->status != 'approved')
							Has been approved to another job
						@endif
						@if($status->linktest != "" && $status->status != "rejected" && $accepted=='no')
							<div class="ui tiny blue button showInfoModal{{$status->rToken}}">Online Test Info</div><br>To be processed by the company, <strong>please attend online test first</strong>
							<br><br>
						@endif
						@if ($status->status != 'rejected' && $status->iStatus == 'accept' && $accepted == 'no')
			 				<div class="ui tiny primary button positive interviewButton{{$status->iToken}}" data-content="Interview schedule"><i class="calendar icon"></i>Interview Schedule</div>

			 				<div class="ui small modal interviewButton{{$status->iToken}}">
							    	<div class="header">Confirmed Interview Schedule</div>
							       	<div class="content">
								        	<table class="ui celled table">
										<tr>
											<td>Place</td>
											<td><strong>{{ $status->iPlace }}</strong></td>
										</tr>
										<tr>
											<td>Date and Time</td>
											<td><strong>{{ date("d M Y", strtotime($status->iDate)) }},&nbsp;{{ $status->iTime }}</strong></td>
										</tr>
										<tr>
											<td>Phone</td>
											<td><strong>{{ $status->iPhone }}</strong></td>

										</tr>
										<tr>
											<td>Contact</td>
											<td><strong>{{ $status->iContact }}</strong></td>
										</tr>
									</table>
							       	 </div>
							 </div>
							 <br><br>
						  	<script>
						        $('.interviewButton{{$status->iToken}}').click(function(e) {
						          	$('.small.modal.interviewButton{{$status->iToken}}').modal('show');
						        });
						  	</script>
			 			@elseif ($status->status != 'rejected' && $status->iStatus == 'waiting' && $accepted=='no')
			 				<a href="{{ url('/') }}/mailbox/{{ $status->iToken }}"><div class="ui tiny primary button positive interviewButton" data-content="You have waiting confirmation interview schedule"><i class="calendar icon"></i></div></a>
			 			@endif
					  	@if($status->status=='waiting' && $accepted=='no')
							<div class="ui tiny button unapply job" data-value="{{'unapply'.$status->jToken.$status->jobname}}">Unapply</div>
						@elseif($status->status=='accepted' && $accepted=='no')
							Accepted and Waiting School of Computer Science Approval
						@elseif($status->status=='approved')
							Approved on: <strong>{{  (!empty($status->approved_on)) ? $status->approved_on : '(No Data)' }}</strong>
						@elseif($status->status=='rejected' && $status->is_rejected_by_department)
							Rejected by School Of Computer Science on: <strong>{{ (!empty($status->rejected_by_department_on) ?  $status->rejected_by_department_on : '(No Data)') }}</strong>
						@elseif($status->status=='rejected' && !empty($status->rejected_by) && $status->rejected_by == 'system')
							Rejected automatically because you have not replied to the interview invitation within one week
						@endif

						@endif
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
	<div class="row">
		<div class="sixteen wide column">
			<center><h3>Inactive Job Application</h3></center>
			<table class="ui celled table" id="statusUnappliedTable">
				<thead>
					<tr>
						<th class="four wide">Company</th>
						<th class="two wide">Position</th>
						<th class="two wide">Duration</th>
						<th class="two wide">Status</th>
						<th class="six wide">Notes</th>
					</tr>
				</thead>
				<tbody>
					@foreach($unapplied_recruitments as $u)
					<tr>
						<td><a href="{{ url('/') }}/profilec/{{$u->company_id}}" >{{ $u->company_name }}</a></td>
						<td>{{ $u->job_name }}</td>
						<td>{{ $u->duration }} Month(s)</td>
						<td>
							Unapplied
						</td>
						<td>
							@if ($u->unapplied_by != 'department' && $u->unapplied_by != 'system' && $u->unapplied_by != 'deleted_job' && $u->unapplied_by != 'department_close')
								Unapplied by User<br>
								On: {{ empty($u->unapplied_on) ? $u->deleted_at : $u->unapplied_on }}
							@elseif ($u->unapplied_by == 'department')
								Unapplied by School of Computer Science<br>
								On: {{ $u->unapplied_on }}
							@elseif ($u->unapplied_by == 'system')
								Unapplied automatically (because there is no process / answer from companies in 30 days or the company has closed the application period). <strong>Please reapply if you still want to apply to the company.</strong><br>
								On: {{ $u->unapplied_on }}
							@elseif ($u->unapplied_by == 'deleted_job')
								Unapplied automatically because the job has been deleted<br>
								On: {{ $u->unapplied_on }}
							@else
								Unapplied automatically<br>
								On: {{ $u->unapplied_on }}
							@endif
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
<br><br>

@foreach ($statuss as $status)
<div class="ui small modal info{{$status->rToken}}">
	<div class="header">Information</div>
	<div class="content">
    		<p>From : {{$status->companyname}}</p>
    		<p>Job : {{$status->jobname}}</p>
    		<p>Here is the<a target="_blank" href="{{$status->linktest}}"> Link </a> for Online test</p>
    	</div>
      	<div class="actions">
		<div class="ui red deny button">Close</div>
	</div>
</div>
<script>
        	$('.showInfoModal{{$status->rToken}}').click(function(e) {
          		$('.small.modal.info{{$status->rToken}}').modal('show');
        	});
</script>
@endforeach

@if($have_old_invitation)
<div class="ui modal confirmation" id="modal_old_invitation">
  <div class="header">IMPORTANT</div>
  <div class="content">
    <h2>You have interview invitation in your mailbox.</h2>
    <span class="ui label red"><strong>IMPORTANT:</strong> Your application will be rejected if you don't reply to the invitation within one week after you get the invitation.</span>
  </div>
  <div class="actions">
    <div class="ui cancel red button">Close</div>
  </div>
</div>
<script>
    $('#modal_old_invitation').modal('show');
</script>
@endif

<div class="ui small modal confirmation">
  <div class="header">Confirmation</div>
  <div class="content">
    <p id="contentApllicant">Are you sure you want to apply to xx as a xxx?</p>
  </div>
  <div class="actions">
    <div class="ui approve green button">Yes</div>
    <div class="ui cancel red button">No</div>
  </div>
</div>

<script type="text/javascript">
	$('#statusUnappliedTable').DataTable();

    var dataToken = "";
    var dataJob = "";
    var actionApplicant = "";

    $(document).on("click", ".ui.button.unapply.job", function ()
    {
      dataToken=$(this).data("value").slice(7, 67);
      actionApplicant=$(this).data("value").slice(0, 7);
      dataJob=$(this).data("value").slice(67);
      document.getElementById("contentApllicant").innerHTML = "Are you sure you want to unapply as "+dataJob;
    });

   $('.ui.small.modal.confirmation').modal("setting",
      {
        blurring: true,
        onApprove: function ()
        {
           window.location.href = "{{ url('/') }}/unapply/"+dataToken;
        }
      })
      .modal('attach events', '.ui.button.unapply.job', 'show');
</script>
@endif

@stop
