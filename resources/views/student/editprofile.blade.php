@extends ('layout.master')

@section ('title', 'BINUS')

@section('content')

    @if(Auth::user()->role == 'department')
        @include('partial/headerDepartment')
    @elseif(Auth::user()->role == 'student')
        @include('partial/headerStudent')
    @endif

    <br><br>

    @if (session('error'))
        <div class="ui red message"> {{ session('error') }}</div>
    @endif
    @if($errors->first() != null)
        <div class="ui red message">{{$errors->first()}}</div>
    @endif

    <div class="ui container">
        <center><div class="ui primary button showInsertGuide">Guidelines <span style="color: yellow;">(Please read this first !)</span></div></center>

        @if(Auth::user()->role == 'department')
            {!! Form::open(['url' => 'submitprofilebydepartment', 'enctype' => 'multipart/form-data', 'method' => 'post', 'role' => 'form', 'class' => 'ui large editprofile form']) !!}
        @else
            {!! Form::open(['url' => 'submitprofile', 'enctype' => 'multipart/form-data', 'method' => 'post', 'role' => 'form', 'class' => 'ui large editprofile form']) !!}
        @endif
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="studentid" value="{{ $userProfile->studentid }}">

        <div class="ui basic segment"  style="padding-top: 0px;">
            <div class="ui top aligned stackable grid">
                <div class="row">
                    <div class="sixteen wide column">
                        <div class="row">
                            <h2 class="ui header">{{ $userProfile->studentid }}</h2>
                            <br>
                        </div>
                        <div class="row">
                            <div class="ui segment" style="padding: 1%;">
                                <center><strong style="color: navy;">Basic Information</strong></center>
                                <div class="ui divider"></div>
                                <div class="field">
                                    <label>Change Profile Picture</label>
                                    <img style="max-height: 375px; max-width: 260px;" src="{{ url('images/'.$userProfile->studentid) }}"
                                         id="profimage" onerror="this.src='{{ URL::asset('assets/images/noimage.jpg') }}'"><br>
                                    <input type="file" id="profpic" name="profpic"/>
                                </div>
                                <div class="field">
                                    <label>Name</label>
                                    <input type="text" name="name" value="@if(old('name')==null){{ $userProfile->name }}@else{{ old('name') }}@endif" placeholder="Name" required maxlength="100">
                                </div>
                                <div class="field">
                                    <label>Portofolio Link</label>
                                    <input type="text" name="link" value="@if(old('link')==null){{ $userProfile->linkportofolio }}@else{{ old('link') }}@endif" placeholder="www.example.com or www.example.com" maxlength="150">
                                </div>
                            </div>
                            <div class="ui segment" style="padding: 1%;">
                                <center><strong style="color: navy;">Academic Information</strong></center>
                                <div class="ui divider"></div>
                                <div class="field">
                                    <label>Semester</label>
                                    <div class="ui selection dropdown">
                                        <input type="hidden" name="semester" value="@if(old('semester')!=null){{old('semester')}}@elseif(isset($userProfile)){{$userProfile->semester}}@endif">
                                        <i class="dropdown icon"></i>
                                        <div class="default text"> --Choose Semester--</div>
                                        <div class="menu">
                                            @for($i=1;$i<=14;$i++)
                                                <div class="item" data-value="{{$i}}" >{{$i}}</div>
                                            @endfor
                                        </div>
                                    </div>
                                </div>
                                @if(Auth::user()->role == 'department')
                                    <div class="field">
                                        <label>Program</label>
                                        <p>{{ $userProfile->degree_name }}</p>
                                        {{--                <div class="ui selection dropdown">--}}
                                        {{--                  <input type="hidden" name="program" value="@if(old('program')!=null){{old('program')}}@elseif(isset($userProfile)){{$userProfile->degree_name}}@endif">--}}
                                        {{--                  <i class="dropdown icon"></i>--}}
                                        {{--                  <div class="default text"> --Choose Program--</div>--}}
                                        {{--                  <div class="menu">--}}
                                        {{--                      @foreach($degrees as $d)--}}
                                        {{--                      <div class="item">{{ $d->name }}</div>--}}
                                        {{--                      @endforeach--}}
                                        {{--                  </div>--}}
                                        {{--                </div>--}}
                                    </div>
                                    <div class="field">
                                        <label>Interest</label>
                                        <p>{{ $userProfile->major }}</p>
                                        {{--                <div class="ui selection dropdown">--}}
                                        {{--                  <input type="hidden" name="interest" value="@if(old('interest')!=null){{old('interest')}}@elseif(isset($userProfile)){{$userProfile->major}}@endif">--}}
                                        {{--                  <i class="dropdown icon"></i>--}}
                                        {{--                  <div class="default text"> --Choose Program--</div>--}}
                                        {{--                  <div class="menu">--}}
                                        {{--                      <div class="item">-</div>--}}
                                        {{--                      <div class="item">Global Class</div>--}}
                                        {{--                      <div class="item">Database</div>--}}
                                        {{--                      <div class="item">Multimedia</div>--}}
                                        {{--                      <div class="item">AI</div>--}}
                                        {{--                      <div class="item">Network</div>--}}
                                        {{--                      <div class="item">Software Engineering</div>--}}
                                        {{--                      <div class="item">Applied Database</div>--}}
                                        {{--                      <div class="item">Applied Network</div>--}}
                                        {{--                  </div>--}}
                                        {{--                </div>--}}
                                    </div>
                                @endif
                            </div>
                            <div class="ui segment" style="padding: 1%;">
                                <center><strong style="color: navy;">Personal Information</strong></center>
                                <div class="ui divider"></div>
                                <div class="field">
                                    <label>Gender</label>
                                    <div class="ui selection dropdown">
                                        <input type="hidden" name="gender" value="@if(old('gender')!=null){{old('gender')}}@elseif(isset($userProfile)){{$userProfile->sex}}@endif">
                                        <i class="dropdown icon"></i>
                                        <div class="default text">--------</div>
                                        <div class="menu">
                                            <div class="item" data-value="male" >Male</div>
                                            <div class="item" data-value="female">Female</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="field">
                                    <label>Date of Birth</label>
                                    <input type="date" placeholder="YYYY-MM-DD" name="dob" style="width:230px;" @if(Auth::user()->role == 'student') required @endif value="@if(old('dob')==null){{ $userProfile->dob }}@else{{ old('dob') }}@endif">
                                </div>
                                <div class="field">
                                    <label>Address</label>
                                    <textarea rows="3" name="address" placeholder="Address" maxlength="120" @if(Auth::user()->role == 'student') required @endif>@if(old('address')==null){{ isset($userProfile) ? $userProfile->address : '' }}@else{{ old('address') }}@endif</textarea>
                                </div>
                            </div>
                            <div class="ui segment" style="padding: 1%;">
                                <center><strong style="color: navy;">Contact Me</strong></center>
                                <div class="ui divider"></div>
                                <div class="field">
                                    <label>Email</label>
                                    <input type="text" name="email" value="@if(old('email')==null){{ $userProfile->email }}@else{{ old('email') }}@endif" placeholder="Email" maxlength="150" required>
                                </div>
                                <div class="field">
                                    <label>Phone</label>
                                    <input type="text" name="phone" value="@if(old('phone')==null){{ $userProfile->phone }}@else{{ old('phone') }}@endif" placeholder="Phone" maxlength="15" required>
                                </div>
                            </div>
                            <div class="ui segment" style="padding: 1%;">
                                <center><strong style="color: navy;">Social Network</strong></center>
                                <div class="ui divider"></div>
                                <div class="field">
                                    <label>Facebook</label>
                                    <input type="text" name="facebook" value="@if(old('facebook')==null){{ $userProfile->facebook }}@else{{ old('facebook') }}@endif" placeholder="Facebook" maxlength="150">
                                </div>
                                <div class="field">
                                    <label>LinkedIn</label>
                                    <input type="text" name="linkedin" value="@if(old('linkedin')==null){{ $userProfile->linkedin }}@else{{ old('linkedin') }}@endif" placeholder="Linkedin" maxlength="150">
                                </div>
                                <div class="field">
                                    <label>Line</label>
                                    <input type="text" name="line" value="@if(old('line')==null){{ $userProfile->line }}@else{{ old('line') }}@endif" placeholder="Line" maxlength="100">
                                </div>
                            </div>
                            {{--            <div class="ui segment" style="padding: 1%;">--}}
                            {{--              <center><strong style="color: navy;">Semester Scores</strong></center>--}}
                            {{--              <div class="ui divider"></div>--}}
                            {{--              <input type="hidden" name="countersemester" id="countersemester" value="{{ (old('countersemester') > 0 ? old('countersemester') : ( $semester['list'] != null ? $semester['list']->count() : 0 ) ) }}"/>--}}
                            {{--              <table class="ui celled table" id="tableSemester">--}}
                            {{--                <thead>--}}
                            {{--                  <tr>--}}
                            {{--                    <th class="six wide column">Semester</th>--}}
                            {{--                    <th class="four wide column">IPS</th>--}}
                            {{--                    <th class="four wide column">IPK</th>--}}
                            {{--                    <th class="two wide column"></th>--}}
                            {{--                  </tr>--}}
                            {{--                </thead>--}}
                            {{--                <tbody>--}}
                            {{--                  @if(old('countersemester')>0)--}}
                            {{--                    @for($i=1;$i <= old('countersemester');$i++)--}}
                            {{--                    <tr>--}}
                            {{--                      <td>Semester {{ $i }}</td>--}}
                            {{--                      <td><input type="number" min="0" max="4" step="0.01" id="ips{{ $i }}" name="ips{{ $i }}" placeholder="IPS" value="{{ old('ips'.$i) }}" disabled></td>--}}
                            {{--                      <td><input type="number" min="0" max="4" step="0.01" id="ipk{{ $i }}" name="ipk{{ $i }}" placeholder="IPK" value="{{ old('ipk'.$i) }}" disabled></td>--}}
                            {{--                      <td></td>--}}
                            {{--                    </tr>--}}
                            {{--                    @endfor--}}
                            {{--                  @elseif($semester['list'] != null)--}}
                            {{--                    @foreach($semester['list'] as $s)--}}
                            {{--                    <tr>--}}
                            {{--                      <td>Semester {{ $s->semester }}</td>--}}
                            {{--                      <td><input type="number" min="0" max="4" step="0.01" id="ips{{ $s->semester }}" name="ips{{ $s->semester }}" placeholder="IPS" value="{{ $s->ips }}" disabled></td>--}}
                            {{--                      <td><input type="number" min="0" max="4" step="0.01" id="ipk{{ $s->semester }}" name="ipk{{ $s->semester }}" placeholder="IPK" value="{{ $s->ipk }}" disabled></td>--}}
                            {{--                      <td></td>--}}
                            {{--                    </tr>--}}
                            {{--                    @endforeach--}}
                            {{--                  @endif--}}
                            {{--                </tbody>--}}
                            {{--                <tfoot>--}}
                            {{--                  <tr>--}}
                            {{--                    <th colspan="3"></th>--}}
                            {{--                    <th>--}}
                            {{--                      <div class="ui icon green compact button" id="add_semester_score_row"><i class="plus icon" ></i></div>--}}
                            {{--                      <div class="ui icon red compact button" id="reduce_semester_score_row"><i class="minus icon"></i></div>--}}
                            {{--                    </th>--}}
                            {{--                  </tr>--}}
                            {{--                </tfoot>--}}
                            {{--              </table>--}}
                            {{--            </div>--}}
                            {{--            <div class="ui segment" style="padding: 1%;">--}}
                            {{--              <center><strong style="color: navy;">Subject Grade</strong></center>--}}
                            {{--              <div class="ui divider"></div>--}}
                            {{--              <ol>--}}
                            {{--                <li>Login at <a href="https://binusmaya.binus.ac.id">https://binusmaya.binus.ac.id</a></li>--}}
                            {{--                <li>Open menu Learning > Grades > View Grade</li>--}}
                            {{--                <li>--}}
                            {{--                  <a href="{{ asset('assets/template/Template_Insert_Nilai_Subject.xlsx') }}" target="_blank">--}}
                            {{--                    Download Template here</a>--}}
                            {{--                </li>--}}
                            {{--                <li>Copy the Table In Binusmaya to the Excel</li>--}}
                            {{--                <li>Don't replace / edit the Template's Header !</li>--}}
                            {{--                <li>Upload the excel below</li>--}}
                            {{--              </ol>--}}
                            {{--              <div class="ui divider"></div>--}}
                            {{--              <div class="field">--}}
                            {{--                <label>Upload Here</label>--}}
                            {{--                <input type="file"  name="fileexcel" ID="fileexcel">--}}
                            {{--              </div>--}}
                            {{--            </div>--}}
                            <div class="ui segment" style="padding: 1%;">
                                <center><strong style="color: navy;">Formal Education</strong></center>
                                <div class="ui divider"></div>
                                <input type="hidden" id="counter1" name="counter1" value="{{ (old('counter1') > 0 ? old('counter1') : ( $userProfile->countformal > 0 ? $userProfile->countformal : 0 ) ) }}">
                                <table class="ui celled table" id="tableFormalEducation">
                                    <thead>
                                    <tr>
                                        <th class="three wide column">Start Date</th>
                                        <th class="three wide column">End Date</th>
                                        <th class="eight wide column">Description</th>
                                        <th class="two wide column"><th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(old('counter1')>0)
                                        @for($i=1;$i <= old('counter1');$i++)
                                            <tr>
                                                <td><div class="ui calendar" id="SFormal{{ $i }}"><div class="ui input left icon"><i class="calendar icon"></i><input type="text" placeholder="ex:October 2017" name="FormalStart{{ $i }}" value="{{ old('FormalStart'.$i) }}" required></div></div></td>
                                                <td><div class="ui calendar" id="EFormal{{ $i }}"><div class="ui input left icon"><i class="calendar icon"></i><input type="text"  placeholder="ex:October 2017" name="FormalEnd{{ $i }}" value="{{ old('FormalEnd'.$i) }}" required></div></div></td>
                                                <td><textarea rows="4" maxlength="200" required name="FormalDesc{{ $i }}" placeholder="Description">{{ old('FormalDesc'.$i) }}</textarea></td>
                                            </tr>
                                        @endfor
                                    @elseif($userProfile->countformal > 0)
                                        @foreach($userProfile->formal as $i => $u)
                                            <tr>
                                                <td><div class="ui calendar" id="SFormal{{ $i+1 }}"><div class="ui input left icon"><i class="calendar icon"></i><input type="text"  placeholder="ex:October 2017" name="FormalStart{{ $i+1 }}" value="{{ $u->startdate }}" required></div></div></td>
                                                <td><div class="ui calendar" id="EFormal{{ $i+1 }}"><div class="ui input left icon"><i class="calendar icon"></i><input type="text"  placeholder="ex:October 2017" name="FormalEnd{{ $i+1 }}" value="{{ $u->enddate }}" required></div></div></td>
                                                <td><textarea rows="4" maxlength="200" required name="FormalDesc{{ $i+1 }}" placeholder="Description">{{ $u->description }}</textarea></td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th colspan="3"></th>
                                        <th>
                                            <div class="ui icon green compact button" id="add_formal_education_row"><i class="plus icon" ></i></div>
                                            <div class="ui icon red compact button" id="reduce_formal_education_row"><i class="minus icon"></i></div>
                                        </th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <div class="ui segment" style="padding: 1%;">
                                <center><strong style="color: navy;">Informal Education</strong></center>
                                <div class="ui divider"></div>
                                <input type="hidden" id="counter2" name="counter2" value="{{ (old('counter2') > 0 ? old('counter2') : ( $userProfile->countinformal > 0 ? $userProfile->countinformal : 0 ) ) }}">
                                <table class="ui celled table" id="tableInformalEducation">
                                    <thead>
                                    <tr>
                                        <th class="three wide column">Start Date</th>
                                        <th class="three wide column">End Date</th>
                                        <th class="eight wide column">Description</th>
                                        <th class="two wide column"><th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(old('counter2')>0)
                                        @for($i=1;$i <= old('counter2');$i++)
                                            <tr>
                                                <td><div class="ui calendar" id="SInformal{{ $i }}"><div class="ui input left icon"><i class="calendar icon"></i><input type="text"  placeholder="ex:October 2017" name="InformalStart{{ $i }}" value="{{ old('InformalStart'.$i) }}" required></div></div></td>
                                                <td><div class="ui calendar" id="EInformal{{ $i }}"><div class="ui input left icon"><i class="calendar icon"></i><input type="text" placeholder="ex:October 2017" name="InformalEnd{{ $i }}" value="{{ old('InformalEnd'.$i) }}" required></div></div></td>
                                                <td><textarea rows="4" maxlength="200" required name="InformalDesc{{ $i }}" placeholder="Description">{{ old('InformalDesc'.$i) }}</textarea></td>
                                            </tr>
                                        @endfor
                                    @elseif($userProfile->countinformal > 0)
                                        @foreach($userProfile->informal as $i => $u)
                                            <tr>
                                                <td><div class="ui calendar" id="SInformal{{ $i+1 }}"><div class="ui input left icon"><i class="calendar icon"></i><input type="text"  placeholder="ex:October 2017" name="InformalStart{{ $i+1 }}" value="{{ $u->startdate }}" required></div></div></td>
                                                <td><div class="ui calendar" id="EInformal{{ $i+1 }}"><div class="ui input left icon"><i class="calendar icon"></i><input type="text" placeholder="ex:October 2017" name="InformalEnd{{ $i+1 }}" value="{{ $u->enddate }}" required></div></div></td>
                                                <td><textarea rows="4" maxlength="200" required name="InformalDesc{{ $i+1 }}" placeholder="Description">{{ $u->description }}</textarea></td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th colspan="3"></th>
                                        <th>
                                            <div class="ui icon green compact button" id="add_informal_education_row"><i class="plus icon" ></i></div>
                                            <div class="ui icon red compact button" id="reduce_informal_education_row"><i class="minus icon"></i></div>
                                        </th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <div class="ui segment" style="padding: 1%;">
                                <center><strong style="color: navy;">Organization & Working Experience</strong></center>
                                <div class="ui divider"></div>
                                <input type="hidden" id="counter3" name="counter3" value="{{ (old('counter3') > 0 ? old('counter3') : ( $userProfile->countorganization > 0 ? $userProfile->countorganization : 0 ) ) }}">
                                <table class="ui celled table" id="tableOrganizationAndWorking">
                                    <thead>
                                    <tr>
                                        <th class="three wide column">Start Date</th>
                                        <th class="three wide column">End Date</th>
                                        <th class="two wide column">At</th>
                                        <th class="two wide column">As</th>
                                        <th class="four wide column">Job Description</th>
                                        <th class="two wide column"><th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(old('counter3')>0)
                                        @for($i=1;$i <= old('counter3');$i++)
                                            <tr>
                                                <td><div class="ui calendar" id="SOrg{{ $i }}"><div class="ui input left icon"><i class="calendar icon"></i><input type="text" placeholder="ex:October 2017" name="OrgStart{{ $i }}" value="{{ old('OrgStart'.$i) }}" required></div></div></td>
                                                <td><div class="ui calendar" id="EOrg{{ $i }}"><div class="ui input left icon"><i class="calendar icon"></i><input type="text" placeholder="ex:October 2017" name="OrgEnd{{ $i }}" value="{{ old('OrgEnd'.$i) }}" required></div></div></td>
                                                <td><div class="ui input"><input type="text" placeholder="Institution" name="OrgName{{ $i }}" value="{{ old('OrgName'.$i) }}" required maxlength="100"></div></td>
                                                <td><div class="ui input"><input type="text" placeholder="Role" name="OrgRole{{ $i }}" value="{{ old('OrgRole'.$i) }}" required maxlength="100"></div></td>
                                                <td><textarea rows="4" name="OrgJobdesc{{ $i }}" required maxlength="1000" placeholder="Job Description">{{ old('OrgJobdesc'.$i) }}</textarea></td>
                                            </tr>
                                        @endfor
                                    @elseif($userProfile->countorganization > 0)
                                        @foreach($userProfile->organization as $i => $u)
                                            <tr>
                                                <td><div class="ui calendar" id="SOrg{{ $i+1 }}"><div class="ui input left icon"><i class="calendar icon"></i><input type="text" placeholder="ex:October 2017" name="OrgStart{{ $i+1 }}" value="{{ $u->startdate }}" required></div></div></td>
                                                <td><div class="ui calendar" id="EOrg{{ $i+1 }}"><div class="ui input left icon"><i class="calendar icon"></i><input type="text" placeholder="ex:October 2017" name="OrgEnd{{ $i+1 }}" value="{{ $u->enddate }}" required></div></div></td>
                                                <td><div class="ui input"><input type="text" placeholder="Institution" name="OrgName{{ $i+1 }}" value="{{ $u->description }}" required maxlength="100"></div></td>
                                                <td><div class="ui input"><input type="text" placeholder="Role" name="OrgRole{{ $i+1 }}" value="{{ $u->role }}" required maxlength="100"></div></td>
                                                <td><textarea rows="4" name="OrgJobdesc{{ $i+1 }}" required maxlength="1000" placeholder="Job Description">{{ $u->jobdesc }}</textarea></td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th colspan="5"></th>
                                        <th>
                                            <div class="ui icon green compact button" id="add_organization_row"><i class="plus icon" ></i></div>
                                            <div class="ui icon red compact button" id="reduce_organization_row"><i class="minus icon"></i></div>
                                        </th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <div class="ui segment" style="padding: 1%;">
                                <center><strong style="color: navy;">Achievement / Award</strong></center>
                                <div class="ui divider"></div>
                                <input type="hidden" id="counter6" name="counter6" value="{{ (old('counter6') > 0 ? old('counter6') : ( $userProfile->countaward > 0 ? $userProfile->countaward : 0 ) ) }}">
                                <table class="ui celled table" id="tableAchievement">
                                    <thead>
                                    <tr>
                                        <th class="six wide column">Date</th>
                                        <th class="eight wide column">Description</th>
                                        <th class="two wide column"><th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(old('counter6')>0)
                                        @for($i=1;$i <= old('counter6');$i++)
                                            <tr>
                                                <td><div class="ui calendar" id="Date{{ $i }}"><div class="ui input left icon"><i class="calendar icon"></i><input type="text" placeholder="ex:October 2017" name="AcvDate{{ $i }}" value="{{ old('AcvDate'.$i) }}" required></div></div></td>
                                                <td><textarea rows="4" name="AcvDesc{{ $i }}" maxlength="200" required placeholder="Competition, Rank, etc">{{ old('AcvDesc'.$i) }}</textarea></td>
                                            </tr>
                                        @endfor
                                    @elseif($userProfile->countaward > 0)
                                        @foreach($userProfile->award as $i => $u)
                                            <tr>
                                                <td><div class="ui calendar" id="Date{{ $i+1 }}"><div class="ui input left icon"><i class="calendar icon"></i><input type="text" placeholder="ex:October 2017" name="AcvDate{{ $i+1 }}" value="{{ $u->startdate }}" required></div></div></td>
                                                <td><textarea rows="4" name="AcvDesc{{ $i+1 }}" maxlength="200" required placeholder="Competition, Rank, etc">{{ $u->description }}</textarea></td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th colspan="2"></th>
                                        <th>
                                            <div class="ui icon green compact button" id="add_achievement_row"><i class="plus icon" ></i></div>
                                            <div class="ui icon red compact button" id="reduce_achievement_row"><i class="minus icon"></i></div>
                                        </th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <div class="ui segment" style="padding: 1%;">
                                <center><strong style="color: navy;">Hard Skill / Soft Skill</strong></center>
                                <div class="ui divider"></div>
                                <input type="hidden" id="counter4" name="counter4" value="{{ (old('counter4') > 0 ? old('counter4') : ( $userProfile->countskill > 0 ? $userProfile->countskill : 0 ) ) }}">
                                <table class="ui celled table" id="tableComputerSkill">
                                    <thead>
                                    <tr>
                                        <th class="eight wide column">Skill</th>
                                        <th class="six wide column">Level</th>
                                        <th class="two wide column"><th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(old('counter4')>0)
                                        @for($i=1;$i <= old('counter4');$i++)
                                            <tr>
                                                <td><input type="text" placeholder="Skill" name="CompName{{ $i }}" required maxlength="100" value="{{ old('CompName'.$i) }}"></td>
                                                <td><input type="number" required min="1" max="10" step="0.25" placeholder="Level [1-10]" name="CompSkill{{ $i }}" value="{{ old('CompSkill'.$i) }}"></td>
                                            </tr>
                                        @endfor
                                    @elseif($userProfile->countskill > 0)
                                        @foreach($userProfile->skill as $i => $u)
                                            <tr>
                                                <td><input type="text" placeholder="Skill" name="CompName{{ $i+1 }}" required maxlength="100" value="{{ $u->name }}"></td>
                                                <td><input type="number" required min="1" max="10" step="0.25" placeholder="Level [1-10]" name="CompSkill{{ $i+1 }}" value="{{ $u->status }}"></td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th colspan="2"></th>
                                        <th>
                                            <div class="ui icon green compact button" id="add_computer_skill_row"><i class="plus icon" ></i></div>
                                            <div class="ui icon red compact button" id="reduce_computer_skill_row"><i class="minus icon"></i></div>
                                        </th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>

                            {{--SOFTSKILL--}}
                            {{-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------}}
                            <div class="ui segment" style="padding: 1%;">
                                <center><strong style="color: navy;">Soft Skill</strong></center>
                                <div class="ui divider"></div>
{{--                                <input type="hidden" id="counter5" name="counter5" value="{{ (old('counter5') > 0 ? old('counter5') : ( $userProfile->countlanguage > 0 ? $userProfile->countlanguage : 0 ) ) }}">--}}
                                <table class="ui celled table" id="tableSoftSkill">
                                    <thead>
                                    <tr>
                                        <th class="eight wide column">Skill</th>
                                        <th class="six wide column">Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Self Development</td>
                                        <td><input type="checkbox" name="softSkill1" id="" value="Self Development"></td>
                                    </tr>
                                    <tr>
                                        <td>Teamwork</td>
                                        <td><input type="checkbox" name="softSkill2" id="" value="Teamwork"></td>
                                    </tr>
                                    <tr>
                                        <td>Communication</td>
                                        <td><input type="checkbox" name="softSkill3" id="" value="Communication"></td>
                                    </tr>
                                    <tr>
                                        <td>Planning and Organizing</td>
                                        <td><input type="checkbox" name="softSkill4" id="" value="Planning and Organizing"></td>
                                    </tr>

                                    <tr>
                                        <td>Problem Solving and Decision Making</td>
                                        <td><input type="checkbox" name="softSkill5" id="" value="Problem Solving and Decision Making"></td>
                                    </tr>

                                    <tr>
                                        <td>Initiative and Enterprise </td>
                                        <td><input type="checkbox" name="softSkill6" id="" value="Initiative and Enterprise"></td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                            {{-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------}}

                            <div class="ui segment" style="padding: 1%;">
                                <center><strong style="color: navy;">Language Skill</strong></center>
                                <div class="ui divider"></div>
                                <input type="hidden" id="counter5" name="counter5" value="{{ (old('counter5') > 0 ? old('counter5') : ( $userProfile->countlanguage > 0 ? $userProfile->countlanguage : 0 ) ) }}">
                                <table class="ui celled table" id="tableLanguageSkill">
                                    <thead>
                                    <tr>
                                        <th class="eight wide column">Skill</th>
                                        <th class="six wide column">Level</th>
                                        <th class="two wide column"><th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($userProfile->countlanguage > 0)
                                        @foreach($userProfile->language as $i => $u)
                                            <tr>
                                                <td><input type="text" placeholder="Language" name="LangName{{ $i+1 }}" required maxlength="100" value="{{ $u->name }}"></td>
                                                <td><input type="text" placeholder="Native, Good, Okay, Etc" name="LangLevel{{ $i+1 }}" required maxlength="20" value="{{ $u->status }}"></td>
                                            </tr>
                                        @endforeach
                                    @elseif(old('counter5')>0)
                                        @for($i=1;$i <= old('counter5');$i++)
                                            <tr>
                                                <td><input type="text" placeholder="Language" name="LangName{{ $i }}" required maxlength="100" value="{{ old('LangName'.$i) }}"></td>
                                                <td><input type="text" placeholder="Native, Good, Okay, Etc" name="LangLevel{{ $i }}" required maxlength="20" value="{{ old('LangLevel'.$i) }}"></td>
                                            </tr>
                                        @endfor
                                    @endif
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th colspan="2"></th>
                                        <th>
                                            <div class="ui icon green compact button" id="add_language_skill_row"><i class="plus icon" ></i></div>
                                            <div class="ui icon red compact button" id="reduce_language_skill_row"><i class="minus icon"></i></div>
                                        </th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>



                            <div class="ui segment" style="padding: 1%;">
                                <center><div class="ui blue submit button">Submit Data And Generate CV</div></center>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>


    <div class="ui modal InsertGuide">
        <div class="header">
            Guidelines
        </div>
        <div class="scrollable content">
            <div class="ui bulleted list">
                <div class="item">
                    To complete your CV please fill in the following fields:<br>
                    - Name<br>
                    - Address<br>
                    - Gender<br>
                    - Date of Birth<br>
                    - Semester<br>
                    - Email<br>
                    - Phone<br>
                    - Profile Picture<br>
                    - Semester Score<br>
                    - Subject Grade<br>
                    - Formal Education<br>
                    - Computer Skills
                </div>
                <div class="item">
                    Below is the requirements of each fields:
                    <table class="ui celled padded table">
                        <tr>
                            <td>Profile Picture</td>
                            <td>Cannot be larger than 2 MB</td>
                        </tr>
                        <tr>
                            <td>Name</td>
                            <td>Maximum 100 characters</td>
                        </tr>
                        <tr>
                            <td>Portofolio Link</td>
                            <td>Maximum 150 characters</td>
                        </tr>
                        <tr>
                            <td>Semester</td>
                            <td>Must be selected</td>
                        </tr>
                        <tr>
                            <td>Gender</td>
                            <td>Must be selected</td>
                        </tr>
                        <tr>
                            <td>Date of Birth</td>
                            <td>Must be filled</td>
                        </tr>
                        <tr>
                            <td>Address</td>
                            <td>Maximum 120 characters</td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>Maximum 150 characters</td>
                        </tr>
                        <tr>
                            <td>Phone</td>
                            <td>Maximum 15 characters</td>
                        </tr>
                        <tr>
                            <td>Facebook</td>
                            <td>Maximum 150 characters</td>
                        </tr>
                        <tr>
                            <td>LinkedIn</td>
                            <td>Maximum 150 characters</td>
                        </tr>
                        <tr>
                            <td>Line</td>
                            <td>Maximum 100 characters</td>
                        </tr>
                        <tr>
                            <td>Semester Scores</td>
                            <td>IPS and IPK must be filled</td>
                        </tr>
                        <tr>
                            <td>Subject Grade</td>
                            <td>Must be uploaded</td>
                        </tr>
                        <tr>
                            <td>Formal Education</td>
                            <td>
                                Start Date: Maximum 50 characters<br>
                                End Date: Maximum 50 characters<br>
                                Description: Maximum 200 characters
                            </td>
                        </tr>
                        <tr>
                            <td>Informal Education</td>
                            <td>
                                Start Date: Maximum 50 characters<br>
                                End Date: Maximum 50 characters<br>
                                Description: Maximum 200 characters
                            </td>
                        </tr>
                        <tr>
                            <td>Organization and Working Experience</td>
                            <td>
                                Start Date: Maximum 50 characters<br>
                                End Date: Maximum 50 characters<br>
                                At: Maximum 100 characters<br>
                                As: Maximum 100 characters<br>
                                Description: Maximum 1000 characters
                            </td>
                        </tr>
                        <tr>
                            <td>Achievement / Award</td>
                            <td>
                                Date: Maximum 50 characters<br>
                                Description: Maximum 200 characters
                            </td>
                        </tr>
                        <tr>
                            <td>Technical Skill</td>
                            <td>
                                Number of Skills: Maximum 8 skills<br>
                                <strong>Please only input the most relevant and important skills</strong><br>
                                Skill: Maximum 100 characters<br>
                                Level: Must be filled from range 1 - 10
                            </td>
                        </tr>
                        <tr>
                            <td>Language Skill</td>
                            <td>
                                Skill: Maximum 100 characters<br>
                                Level: Maximum 20 characters
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

        <script>
            var countersemester, counter1, counter2, counter3, counter4, counter5, counter6;
            $(document).ready(function(){

                $('.showInsertGuide').click(function(e) {
                    $('.ui.modal.InsertGuide').modal('show');
                });

                $('#add_semester_score_row').click(function(){
                    counterSemester = document.getElementById('countersemester').value;
                    counterSemester++;
                    var tbody = $('#tableSemester>tbody');
                    tbody.append('<tr><td>Semester '+ counterSemester +'</td><td><input type="number" min="0" max="4" step="0.01" id="ips'+counterSemester+'" name="ips'+counterSemester+'" placeholder="IPS" required></td><td><input type="number" min="0" max="4" step="0.01" id="ipk'+counterSemester+'" name="ipk'+counterSemester+'" placeholder="IPK" required></td></tr>');
                    document.getElementById('countersemester').value = counterSemester;
                });

                $('#reduce_semester_score_row').click(function(){
                    counterSemester = document.getElementById('countersemester').value;
                    if(counterSemester <= 0) {
                        counterSemester = 0;
                        return;
                    }
                    counterSemester--;
                    var lastChild = $('#tableSemester>tbody>tr:last-child').remove();
                    document.getElementById('countersemester').value = counterSemester;
                });

                $('#add_formal_education_row').click(function(){
                    counter1 = document.getElementById('counter1').value;
                    counter1++;
                    var tbody = $('#tableFormalEducation>tbody');
                    tbody.append('<tr><td><div class="ui calendar" id="SFormal'+counter1+'"><div class="ui input left icon"><i class="calendar icon"></i><input type="text"  placeholder="ex:October 2017" name="FormalStart'+counter1+'" required></div></div></td><td><div class="ui calendar" id="EFormal'+counter1+'"><div class="ui input left icon"><i class="calendar icon"></i><input type="text"  placeholder="ex:October 2017" name="FormalEnd'+counter1+'" required></div></div></td><td><textarea rows="4" maxlength="200" required name="FormalDesc'+counter1+'" placeholder="Description"></textarea></td></tr>');
                    document.getElementById('counter1').value = counter1;
                });

                $('#reduce_formal_education_row').click(function(){
                    counter1 = document.getElementById('counter1').value;
                    if(counter1 <= 0) {
                        counter1 = 0;
                        return;
                    }
                    counter1--;
                    var lastChild = $('#tableFormalEducation>tbody>tr:last-child').remove();
                    document.getElementById('counter1').value = counter1;
                });

                $('#add_informal_education_row').click(function(){
                    counter2 = document.getElementById('counter2').value;
                    counter2++;
                    var tbody = $('#tableInformalEducation>tbody');
                    tbody.append('<tr><td><div class="ui calendar" id="SInformal'+counter2+'"><div class="ui input left icon"><i class="calendar icon"></i><input type="text"  placeholder="ex:October 2017" name="InformalStart'+counter2+'" required></div></div></td><td><div class="ui calendar" id="EInformal'+counter2+'"><div class="ui input left icon"><i class="calendar icon"></i><input type="text"  placeholder="ex:October 2017" name="InformalEnd'+counter2+'" required></div></div></td><td><textarea rows="4" maxlength="200" required name="InformalDesc'+counter2+'" placeholder="Description"></textarea></td></tr>');
                    document.getElementById('counter2').value = counter2;
                });

                $('#reduce_informal_education_row').click(function(){
                    counter2 = document.getElementById('counter2').value;
                    if(counter2 <= 0) {
                        counter2 = 0;
                        return;
                    }
                    counter2--;
                    var lastChild = $('#tableInformalEducation>tbody>tr:last-child').remove();
                    document.getElementById('counter2').value = counter2;
                });


                $('#add_organization_row').click(function(){
                    counter3 = document.getElementById('counter3').value;
                    counter3++;
                    var tbody = $('#tableOrganizationAndWorking>tbody');
                    tbody.append('<tr><td><div class="ui calendar" id="SOrg'+counter3+'"><div class="ui input left icon"><i class="calendar icon"></i><input type="text"  placeholder="ex:October 2017" name="OrgStart'+counter3+'" required></div></div></td><td><div class="ui calendar" id="EOrg'+counter3+'"><div class="ui input left icon"><i class="calendar icon"></i><input type="text"  placeholder="ex:October 2017" name="OrgEnd'+counter3+'" required></div></div></td><td><div class="ui input"><input type="text" placeholder="Institution" name="OrgName'+counter3+'" required maxlength="100"></div></td><td><div class="ui input"><input type="text" placeholder="Role" name="OrgRole'+counter3+'" required maxlength="100"></div></td><td><textarea rows="4" name="OrgJobdesc'+counter3+'" required maxlength="1000" placeholder="Job Description"></textarea></td></tr>');
                    document.getElementById('counter3').value = counter3;
                });

                $('#reduce_organization_row').click(function(){
                    counter3 = document.getElementById('counter3').value;
                    if(counter3 <= 0) {
                        counter3 = 0;
                        return;
                    }
                    counter3--;
                    var lastChild = $('#tableOrganizationAndWorking>tbody>tr:last-child').remove();
                    document.getElementById('counter3').value = counter3;
                });

                $('#add_achievement_row').click(function(){
                    counter6 = document.getElementById('counter6').value;
                    counter6++;
                    var tbody = $('#tableAchievement>tbody');
                    tbody.append('<tr><td><div class="ui calendar" id="Date'+counter6+'"><div class="ui input left icon"><i class="calendar icon"></i><input type="text"  placeholder="ex:October 2017" name="AcvDate'+counter6+'" required></div></div></td><td><textarea rows="4" name="AcvDesc'+counter6+'" maxlength="200" required placeholder="Competition, Rank, etc"></textarea></td></tr>');
                    document.getElementById('counter6').value = counter6;
                });

                $('#reduce_achievement_row').click(function(){
                    counter6 = document.getElementById('counter6').value;
                    if(counter6 <= 0) {
                        counter6 = 0;
                        return;
                    }
                    counter6--;
                    var lastChild = $('#tableAchievement>tbody>tr:last-child').remove();
                    document.getElementById('counter6').value = counter6;
                });

                $('#add_computer_skill_row').click(function(){
                    counter4 = document.getElementById('counter4').value;
                    counter4++;
                    var tbody = $('#tableComputerSkill>tbody');
                    tbody.append('<tr><td><input type="text" placeholder="Skill" name="CompName'+counter4+'" required maxlength="100"></td><td><input type="number" required min="1" max="10" step="0.25" placeholder="Level [1-10]" name="CompSkill'+counter4+'"></td></tr>');
                    document.getElementById('counter4').value = counter4;
                });

                $('#reduce_computer_skill_row').click(function(){
                    counter4 = document.getElementById('counter4').value;
                    if(counter4 <= 0) {
                        counter4 = 0;
                        return;
                    }
                    counter4--;
                    var lastChild = $('#tableComputerSkill>tbody>tr:last-child').remove();
                    document.getElementById('counter4').value = counter4;
                });

                $('#add_language_skill_row').click(function(){
                    console.log(document.getElementById('counter5').value);
                    counter5 = document.getElementById('counter5').value;
                    counter5++;
                    var tbody = $('#tableLanguageSkill>tbody');
                    tbody.append('<tr><td><input type="text" placeholder="Language" name="LangName'+counter5+'" required maxlength="100"></td><td><input type="text" placeholder="Native, Good, Okay, Etc" name="LangLevel'+counter5+'" required maxlength="20"></td></tr>');
                    document.getElementById('counter5').value = counter5;
                });

                $('#reduce_language_skill_row').click(function(){
                    counter5 = document.getElementById('counter5').value;
                    if(counter5 <= 0) {
                        counter5 = 0;
                        return;
                    }
                    counter5--;
                    var lastChild = $('#tableLanguageSkill>tbody>tr:last-child').remove();
                    document.getElementById('counter5').value = counter5;
                });

            });//end of document ready
        </script>
@stop
