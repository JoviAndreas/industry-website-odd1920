@extends('layout.masterS')

@section ('title', 'BINUS')

@section('content')

    @include('partial/headerStudent')
    <div class="ui container" style="margin-bottom: 10rem;">
        <br>
        @if (session('success'))
            <div class="row">
                <div class="ui positive message">
                    <i class="close icon"></i>
                    <div class="header">
                        {{session('success')}}
                    </div>
                </div>
            </div>
        @endif
        @if (session('err'))
            <div class="row">
                <div class="ui negative message">
                    <i class="close icon"></i>
                    <div class="header">
                        {{session('err')}}
                    </div>
                </div>
            </div>
        @endif
        <br>
        @if($student->cv == 1)
            {{-- <div id="studentWarningModal" class="ui modal">
                <div class="header">
                    Pengumuman
                </div>
                <div class="content">
                    <p>Saat ini proses apply otomatis sudah dilakukan, bagi mahasiswa yang mendapat undangan interview dan tes wajib memberikan konfirmasi kehadiran ke perusahaan, tanpa terkecuali. Mohon memperhatikan deadline penempatan magang sebelum hari perkuliahan Semester Genap Februari 2020.</p>
                    <p>Apabila  masih terkendala dengan penempatan dapat berkonsultasi ke EPC (Enrichment Program Coordinator) untuk kemungkinan pindah track atau mengambil cuti.</p>
                </div>
                <div class="actions">
                    <div class="ui positive icon button">OK</div>
                </div>
            </div>
            <script>
                $('#studentWarningModal').modal('show');
            </script> --}}
            <br>
            <div class="ui container">
                <div class="ui message">
                    <ul>
                        <li>To see your apply status, see Status menu.</li>
                        <li>
                            There are <span cla ss="ui blue label">{{ $pending_interview_invitation_count }}</span>
                            interview invitation that you have not replied yet.
                            @if($pending_interview_invitation_count > 0)
                                <a href="{{ url('/mailbox') }}">Go to interview invitation mailbox</a>
                            @endif
                        </li>
                        <li>To see interview invitation, open your Mailbox.</li>
                        <li>
                            There are <span class="ui green label">{{ $new_job_count }}</span> job(s) posted by
                            companies yesterday.
                            @if($new_job_count > 0)
                                <a href="" id="btn_new_job" onclick="return false;">See list of companies with new jobs
                                    here</a>
                                <div class="ui small modal" id="modal_new_job">
                                    <div class="header">Companies with New Jobs</div>
                                    <div class="scrolling content">
                                        <table class="ui compact padded table">
                                            <thead>
                                            <tr>
                                                <th class="sixteen wide">Company Name</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($new_job_company_name_only as $n)
                                                <tr>
                                                    <td>{{ $n->company_name }}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="actions">
                                        <div class="ui red deny button">
                                            Close
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </li>
                    </ul>
                </div>
                <div class="ui segment">
                    <table class="ui striped selectable table" id="vacancy_table">
                        <thead>
                        <tr>
                            <th class="three wide">Company</th>
                            <th class="one wide"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($companys as $company)
                            <tr class="odd">
                                <td colspan="5">
                                    {{ $company->name }}
                                    <i class="caret down icon" id="arrow"></i>
                                    @if($company->countOfNewJobs > 0)
                                        <span class="ui grey circular label">New Jobs: {{ $company->countOfNewJobs }}</span>
                                    @endif
                                    <a href="{{ url('/') }}/profilec/{{$company->companyid}}" class="link_underline"
                                       target="_blank" style="float: right;">Go to Company Profile</a>
                                </td>
                            </tr>
                            <tr class="even">
                                <td colspan="5">
                                    <table class="ui celled table">
                                        <thead>
                                        <tr>
                                            <th class="six wide">Job</th>
                                            <th class="two wide">Location</th>
                                            <th class="two wide">Duration</th>
                                            <th class="two wide">Status</th>
                                            <th class="one wide">Quota</th>
                                            <th class="three wide">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($company->jobs as $job)
                                            <tr>
                                                <td>{{ $job->name }}@if($job->isNew) &nbsp; <span style=""
                                                                                                  class="ui circular grey label">New</span> @endif
                                                </td>
                                                <td>{{ $job->location }}</td>
                                                <td>{{ $job->duration }} Month(s)</td>
                                                <td>
                                                    <div class="ui {{ $job->status == 'rejected' ? 'red' : ($job->status == 'accepted' || $job->status == 'approved' ? 'green' : ($job->status=='process'? 'blue': ($job->status=='waiting'?'orange':''))) }} label">{{ ucwords($job->status) }}</div>
                                                </td>
                                                <td>{{ $job->quota }}</td>
                                                <td>
                                                    @if($job->description != '')
                                                        <div class="ui button showModal{{$job->token}}">Desc</div>
                                                    @endif

                                                    @if($job->status=='waiting')
                                                        <div class="ui button unapply job"
                                                             data-value="{{'unapply'.$job->token.$job->name}}">Unapply
                                                        </div>
                                                    @elseif($job->status =='not applied' && $status=='available')
                                                        <div class="ui button apply job"
                                                             data-value="{{'apply'.$job->token.$job->name}}">Apply
                                                        </div>
                                                    @elseif($job->status =='not applied' && $status=='maxcompany')
                                                        <div class="ui button apply_with_confirmation job"
                                                             data-value="{{'apply'.$job->token.$job->name}}">Apply
                                                        </div>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

    </div>
    </div>
    </div>


    @foreach ($companys as $company)
        @foreach($company->jobs as $job)
            <div class="ui small modal {{$job->token}}">
                <div class="header">Job Description</div>
                <div class="image content">
                    <img class="image">
                    <div class="description">
                        <div class="ui bulleted list">
                            <?php
                            $pieces = explode("\n", $job->description);
                            ?>
                            @foreach ($pieces as $piece)
                                <div class="item">{{ $piece }}</div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="actions">
                    <div class="ui red deny button">
                        Close
                    </div>
                </div>
            </div>

            <script>
                $('.showModal{{$job->token}}').click(function (e) {
                    console.log(e);
                    $('.small.modal.{{$job->token}}').modal('show');
                });
            </script>
        @endforeach
    @endforeach

    @else
        <div class="ui warning message">
            <div class="header">
                Thank you for registering
            </div>
            <p>
                You just have to complete <strong>one more step</strong>, before applying jobs.<br>
                You have to complete your <strong>CV</strong> first.<br>
                <strong>Please fill in the following fields in your profile:</strong><br>
                @foreach($student->returnEmptyProfileInfo() as $arr)
                    - {{ $arr }}<br>
                @endforeach
                You can generate your grades manually on this page <a href="{{ url('/profile') }}">Here</a><br>
                (Go to Profile menu and Update your profile, or click <a href="{{ url('/updateprofile') }}">here</a> Then Submit to Generate your CV)
            </p>
        </div>
        @endif

        </div>

        @if($have_old_invitation)
            <div class="ui modal confirmation" id="modal_old_invitation">
                <div class="header">IMPORTANT</div>
                <div class="content">
                    <h2>You have interview invitation in your mailbox.</h2>
                    <span class="ui label red"><strong>IMPORTANT:</strong> Your application will be rejected if you don't reply to the invitation within one week after you get the invitation.</span>
                </div>
                <div class="actions">
                    <div class="ui cancel red button">Close</div>
                </div>
            </div>
            <script>
                $('#modal_old_invitation').modal('show');
            </script>
        @endif

        <div class="ui small modal confirmation">
            <div class="header">Confirmation</div>
            <div class="content">
                <p id="contentApllicant">Are you sure you want to apply to xx as a xxx?</p>
            </div>
            <div class="actions">
                <div class="ui approve green button">Yes</div>
                <div class="ui cancel red button">No</div>
            </div>
        </div>
        <script type="text/javascript">
            var dataToken = "";
            var dataJob = "";
            var actionApplicant = "";

            $(document).on("click", ".ui.button.apply_with_confirmation.job", function () {
                dataToken = $(this).data("value").slice(5, 65);
                actionApplicant = $(this).data("value").slice(0, 5);
                dataJob = $(this).data("value").slice(65);
                document.getElementById("contentApllicant").innerHTML = '<div class="ui red label">You have already applied to 5 companies</div><br>Are you sure you want to apply as ' + dataJob;
            });

            $(document).on("click", ".ui.button.apply.job", function () {
                dataToken = $(this).data("value").slice(5, 65);
                actionApplicant = $(this).data("value").slice(0, 5);
                dataJob = $(this).data("value").slice(65);
                document.getElementById("contentApllicant").innerHTML = "Are you sure you want to apply as " + dataJob;
            });

            $(document).on("click", ".ui.button.unapply.job", function () {
                dataToken = $(this).data("value").slice(7, 67);
                actionApplicant = $(this).data("value").slice(0, 7);
                dataJob = $(this).data("value").slice(67);
                document.getElementById("contentApllicant").innerHTML = "Are you sure you want to unapply as " + dataJob;
            });

            $('.ui.small.modal.confirmation').modal("setting",
                {
                    blurring: true,
                    onApprove: function () {
                        if (actionApplicant == "apply") {
                            window.location.href = "{{ url('/') }}/applyjob/" + dataToken;
                        } else if (actionApplicant == "unapply") {
                            window.location.href = "{{ url('/') }}/unapply/" + dataToken;
                        }
                    }
                })
                .modal('attach events', '.ui.button.apply.job', 'show')
                .modal('attach events', '.ui.button.unapply.job', 'show')
                .modal('attach events', '.ui.button.apply_with_confirmation.job', 'show')
            ;

            $('#modal_new_job').modal()
                .modal('attach events', '#btn_new_job', 'show');
        </script>
@stop
