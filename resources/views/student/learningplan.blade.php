@extends ('layout.masterS')

@section ('title', 'BINUS')

@section('content')

@include('partial/headerStudent')

<br><br>
<center>
<h3 class="ui horizontal large header">You have been accepted as {{$data->jobname}} at {{$data->companyname}}</h3>
</center>

<br><br>
<h4 class="ui horizontal divider header">Learning Plan - Student Information</h4>
@if ($statusLP)
      @if($profile->semester == null || $profile->semester == "" || $profile->semester == 0)
        <div class="ui primary disabled button learning plan"  style="pointer-events: auto;" data-content="Your company has not fill all the data.">
          <i class="download icon"></i>
          Download
        </div>
      @else
        <a href="{{ url('/') }}/downloadLearningPlan">
          <div class="ui primary button learning plan"  style="pointer-events: auto;" data-content="Your company and department has fill all the data.">
            <i class="download icon"></i>
            Download
          </div>
        </a>
      @endif
@else
  <div class="ui primary disabled button learning plan"  style="pointer-events: auto;" data-content="Your company has not fill all the data.">
    <i class="download icon"></i>
    Download
  </div>
@endif

<br><br>
	<center>

<table class="ui definition table">
  <tbody>
    <tr>
      <td>Name</td>
      <td>{{$profile->name}}</td>
    </tr>
    <tr>
      <td>Binusian ID</td>
      <td>{{$profile->studentid}}</td>
    </tr>
    <tr>
      <td>E-Mail</td>
      <td>{{$profile->email}}</td>
    </tr>
    <tr>
      <td>Phone</td>
      <td>{{$profile->phone}}</td>
    </tr>
    <tr>
      <td>Address</td>
      <td>{{$profile->address}}</td>
    </tr>
    <tr>
      <td>Study Program / Degree</td>
      <td>{{$profile->degree}}</td>
    </tr>
     <tr>
      <td>Faculty / School</td>
      <td>{{$profile->school}}</td>
    </tr>
     <tr>
      <td>Semester</td>
      <td>{{$profile->semester}}</td>
    </tr> 
</tbody></table>
</center>

@if($profile->email == null || $profile->email == "")
		<div class="ui error message">Please fill your E-Mail in menu profile</div>
@endif

@if($profile->phone == null || $profile->phone == "")
		<div class="ui error message">Please fill your Phone in menu profile</div>
@endif

@if($profile->address == null || $profile->address == "")
		<div class="ui error message">Please fill your Address in menu profile</div>
@endif

@if(($profile->semester == null || $profile->semester == "" || $profile->semester == 0) && !$statusLP)
  <div class="ui error message">* Please fill your Semester in menu profile<br/>* Your company or department has not fill all the data</div>
@elseif($profile->semester == null || $profile->semester == "" || $profile->semester == 0)
  <div class="ui error message">* Please fill your Semester in menu profile</div>
@elseif(!$statusLP)
  <div class="ui error message">* Your company or department has not fill all the data</div>
@endif

@stop