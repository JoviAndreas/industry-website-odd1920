<?php

use Illuminate\Database\Seeder;

class JobPositionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('job_positions')->delete();
        
        \DB::table('job_positions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Developers',
                'created_at' => '2019-05-13 12:18:00',
                'updated_at' => '2019-05-13 13:37:30',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'System Analyst',
                'created_at' => '2019-05-13 12:18:12',
                'updated_at' => '2019-05-13 12:18:12',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Business Analyst',
                'created_at' => '2019-05-13 12:18:21',
                'updated_at' => '2019-05-13 12:18:21',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Business Management',
                'created_at' => '2019-05-13 12:18:28',
                'updated_at' => '2019-05-13 12:18:28',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'General Manager',
                'created_at' => '2019-05-13 12:18:35',
                'updated_at' => '2019-05-13 12:18:35',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'UI/UX Designer',
                'created_at' => '2019-05-13 12:18:42',
                'updated_at' => '2019-05-13 12:18:42',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'UI/UX Tester',
                'created_at' => '2019-05-13 12:18:50',
                'updated_at' => '2019-05-13 12:18:50',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Animation Ilustrator',
                'created_at' => '2019-05-13 12:18:56',
                'updated_at' => '2019-05-13 12:18:56',
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'Project Management',
                'created_at' => '2019-05-13 12:19:03',
                'updated_at' => '2019-05-13 12:19:03',
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'Mobile Dev',
                'created_at' => '2019-05-13 12:19:13',
                'updated_at' => '2019-05-13 12:19:13',
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'marketing',
                'created_at' => '2019-05-13 12:19:19',
                'updated_at' => '2019-05-13 12:19:19',
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'System Core',
                'created_at' => '2019-05-13 12:19:25',
                'updated_at' => '2019-05-13 12:19:25',
            ),
        ));
        
        
    }
}