<?php

use Illuminate\Database\Seeder;

class BippTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('bipp')->delete();
        
        \DB::table('bipp')->insert(array (
            0 => 
            array (
                'id' => 1,
                'min_ipk' => 3.0,
                'created_at' => '2019-04-01 00:00:00',
                'updated_at' => '2019-04-01 00:00:00',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}