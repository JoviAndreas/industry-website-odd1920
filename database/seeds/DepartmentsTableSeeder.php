<?php

use Illuminate\Database\Seeder;

class DepartmentsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        \DB::table('departments')->insert(array (
            0 => 
            array (
                'id' => 1,
                'departmentid' => 'superadmin',
                'name' => 'Superadmin',
                'email' => 'superadmin@gmail.com',
                'photo' => '',
                'head_of_program' => 'all',
                'access_role' => 'superadmin',
                'campus' => NULL,
                'created_at' => '2019-04-01 00:00:00',
                'updated_at' => '2019-04-01 00:00:00',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}