<?php

use Illuminate\Database\Seeder;

class CvskillinformationsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('cvskillinformations')->delete();
        
        \DB::table('cvskillinformations')->insert(array (
            0 => 
            array (
                'id' => 1,
                'studentid' => '2001234567',
                'name' => 'C++',
                'status' => '7',
                'type' => 'Skills',
                'created_at' => '2019-04-01 10:35:06',
                'updated_at' => '2019-04-01 10:35:06',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'studentid' => '2001234567',
                'name' => 'PHP',
                'status' => '6',
                'type' => 'Skills',
                'created_at' => '2019-04-01 10:35:06',
                'updated_at' => '2019-04-01 10:35:06',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'studentid' => '2001234567',
                'name' => 'Indonesia',
                'status' => 'Native',
                'type' => 'Languages',
                'created_at' => '2019-04-01 10:35:06',
                'updated_at' => '2019-04-01 10:35:06',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'studentid' => '2001234567',
                'name' => 'English',
                'status' => 'Good',
                'type' => 'Languages',
                'created_at' => '2019-04-01 10:35:06',
                'updated_at' => '2019-04-01 10:35:06',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'studentid' => '2001567890',
                'name' => 'Business Analyst',
                'status' => '10',
                'type' => 'Skills',
                'created_at' => '2019-04-06 15:49:29',
                'updated_at' => '2019-04-06 15:49:29',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'studentid' => '2001567890',
                'name' => 'english',
                'status' => 'ok',
                'type' => 'Languages',
                'created_at' => '2019-04-06 15:49:29',
                'updated_at' => '2019-04-06 15:49:29',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'studentid' => '2001345780',
                'name' => 'Programming',
                'status' => '8',
                'type' => 'Skills',
                'created_at' => '2019-04-13 00:13:07',
                'updated_at' => '2019-04-13 00:13:51',
                'deleted_at' => '2019-04-13 00:13:51',
            ),
            7 => 
            array (
                'id' => 8,
                'studentid' => '2001345780',
                'name' => 'English',
                'status' => '8',
                'type' => 'Languages',
                'created_at' => '2019-04-13 00:13:07',
                'updated_at' => '2019-04-13 00:13:51',
                'deleted_at' => '2019-04-13 00:13:51',
            ),
            8 => 
            array (
                'id' => 9,
                'studentid' => '2001345780',
                'name' => 'Programming',
                'status' => '8',
                'type' => 'Skills',
                'created_at' => '2019-04-13 00:13:51',
                'updated_at' => '2019-04-13 00:13:51',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'studentid' => '2001345780',
                'name' => 'English',
                'status' => '8',
                'type' => 'Languages',
                'created_at' => '2019-04-13 00:13:51',
                'updated_at' => '2019-04-13 00:13:51',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'studentid' => '2001123456',
                'name' => 'Critical Thinking',
                'status' => '7',
                'type' => 'Skills',
                'created_at' => '2019-04-13 00:17:36',
                'updated_at' => '2019-04-13 00:17:36',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'studentid' => '2001123456',
                'name' => 'English',
                'status' => '7',
                'type' => 'Languages',
                'created_at' => '2019-04-13 00:17:36',
                'updated_at' => '2019-04-13 00:17:36',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'studentid' => '2003345345',
                'name' => 'Teknik ',
                'status' => '9',
                'type' => 'Skills',
                'created_at' => '2019-04-13 00:27:10',
                'updated_at' => '2019-04-13 00:27:10',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'studentid' => '2003345345',
                'name' => 'English',
                'status' => '9',
                'type' => 'Languages',
                'created_at' => '2019-04-13 00:27:10',
                'updated_at' => '2019-04-13 00:27:10',
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'studentid' => '2003890890',
                'name' => 'teknik abc',
                'status' => '10',
                'type' => 'Skills',
                'created_at' => '2019-04-13 00:42:55',
                'updated_at' => '2019-04-13 00:42:55',
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'studentid' => '2003890890',
                'name' => 'english',
                'status' => 'good',
                'type' => 'Languages',
                'created_at' => '2019-04-13 00:42:55',
                'updated_at' => '2019-04-13 00:42:55',
                'deleted_at' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'studentid' => '2000789789',
                'name' => 'Design Thinking',
                'status' => '8',
                'type' => 'Skills',
                'created_at' => '2019-04-13 00:47:26',
                'updated_at' => '2019-04-13 00:47:26',
                'deleted_at' => NULL,
            ),
            17 => 
            array (
                'id' => 18,
                'studentid' => '2000789789',
                'name' => 'English',
                'status' => 'good',
                'type' => 'Languages',
                'created_at' => '2019-04-13 00:47:26',
                'updated_at' => '2019-04-13 00:47:26',
                'deleted_at' => NULL,
            ),
            18 => 
            array (
                'id' => 19,
                'studentid' => '2000134134',
                'name' => 'laporan keuangan',
                'status' => '8',
                'type' => 'Skills',
                'created_at' => '2019-04-13 00:50:43',
                'updated_at' => '2019-04-13 00:50:43',
                'deleted_at' => NULL,
            ),
            19 => 
            array (
                'id' => 20,
                'studentid' => '2000134134',
                'name' => 'English',
                'status' => 'Native',
                'type' => 'Languages',
                'created_at' => '2019-04-13 00:50:43',
                'updated_at' => '2019-04-13 00:50:43',
                'deleted_at' => NULL,
            ),
            20 => 
            array (
                'id' => 21,
                'studentid' => '2001898902',
                'name' => 'IOS',
                'status' => '9',
                'type' => 'Skills',
                'created_at' => '2019-04-13 00:54:30',
                'updated_at' => '2019-04-13 00:54:30',
                'deleted_at' => NULL,
            ),
            21 => 
            array (
                'id' => 22,
                'studentid' => '2001898902',
                'name' => 'English',
                'status' => 'good',
                'type' => 'Languages',
                'created_at' => '2019-04-13 00:54:30',
                'updated_at' => '2019-04-13 00:54:30',
                'deleted_at' => NULL,
            ),
            22 => 
            array (
                'id' => 23,
                'studentid' => '2001098789',
                'name' => 'Animation Design',
                'status' => '9',
                'type' => 'Skills',
                'created_at' => '2019-04-13 07:54:12',
                'updated_at' => '2019-04-13 07:58:57',
                'deleted_at' => '2019-04-13 07:58:57',
            ),
            23 => 
            array (
                'id' => 24,
                'studentid' => '2001098789',
                'name' => 'English',
                'status' => 'good',
                'type' => 'Languages',
                'created_at' => '2019-04-13 07:54:12',
                'updated_at' => '2019-04-13 07:58:57',
                'deleted_at' => '2019-04-13 07:58:57',
            ),
            24 => 
            array (
                'id' => 25,
                'studentid' => '2001098789',
                'name' => 'Animation Design',
                'status' => '9',
                'type' => 'Skills',
                'created_at' => '2019-04-13 07:58:57',
                'updated_at' => '2019-04-13 07:58:57',
                'deleted_at' => NULL,
            ),
            25 => 
            array (
                'id' => 26,
                'studentid' => '2001098789',
                'name' => 'English',
                'status' => 'good',
                'type' => 'Languages',
                'created_at' => '2019-04-13 07:58:57',
                'updated_at' => '2019-04-13 07:58:57',
                'deleted_at' => NULL,
            ),
            26 => 
            array (
                'id' => 27,
                'studentid' => '2000111222',
                'name' => 'Design',
                'status' => '9',
                'type' => 'Skills',
                'created_at' => '2019-04-13 08:02:28',
                'updated_at' => '2019-04-13 08:02:28',
                'deleted_at' => NULL,
            ),
            27 => 
            array (
                'id' => 28,
                'studentid' => '2000111222',
                'name' => 'English',
                'status' => 'Native',
                'type' => 'Languages',
                'created_at' => '2019-04-13 08:02:28',
                'updated_at' => '2019-04-13 08:02:28',
                'deleted_at' => NULL,
            ),
            28 => 
            array (
                'id' => 29,
                'studentid' => '2001544322',
                'name' => 'C and C++',
                'status' => '6',
                'type' => 'Skills',
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:02:26',
                'deleted_at' => '2019-05-02 10:02:26',
            ),
            29 => 
            array (
                'id' => 30,
                'studentid' => '2001544322',
                'name' => 'Java',
                'status' => '7',
                'type' => 'Skills',
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:02:26',
                'deleted_at' => '2019-05-02 10:02:26',
            ),
            30 => 
            array (
                'id' => 31,
                'studentid' => '2001544322',
                'name' => 'Laravel ',
                'status' => '7',
                'type' => 'Skills',
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:02:26',
                'deleted_at' => '2019-05-02 10:02:26',
            ),
            31 => 
            array (
                'id' => 32,
                'studentid' => '2001544322',
                'name' => 'Bootstrap',
                'status' => '8',
                'type' => 'Skills',
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:02:26',
                'deleted_at' => '2019-05-02 10:02:26',
            ),
            32 => 
            array (
                'id' => 33,
                'studentid' => '2001544322',
                'name' => 'ASP.NET & C#',
                'status' => '6',
                'type' => 'Skills',
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:02:26',
                'deleted_at' => '2019-05-02 10:02:26',
            ),
            33 => 
            array (
                'id' => 34,
                'studentid' => '2001544322',
                'name' => 'Indonesian',
                'status' => 'Native',
                'type' => 'Languages',
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:02:26',
                'deleted_at' => '2019-05-02 10:02:26',
            ),
            34 => 
            array (
                'id' => 35,
                'studentid' => '2001544322',
                'name' => 'English',
                'status' => 'Good',
                'type' => 'Languages',
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:02:26',
                'deleted_at' => '2019-05-02 10:02:26',
            ),
            35 => 
            array (
                'id' => 36,
                'studentid' => '2001544322',
                'name' => 'C and C++',
                'status' => '6',
                'type' => 'Skills',
                'created_at' => '2019-05-02 10:02:26',
                'updated_at' => '2019-05-02 19:06:33',
                'deleted_at' => '2019-05-02 19:06:33',
            ),
            36 => 
            array (
                'id' => 37,
                'studentid' => '2001544322',
                'name' => 'Java',
                'status' => '7',
                'type' => 'Skills',
                'created_at' => '2019-05-02 10:02:26',
                'updated_at' => '2019-05-02 19:06:33',
                'deleted_at' => '2019-05-02 19:06:33',
            ),
            37 => 
            array (
                'id' => 38,
                'studentid' => '2001544322',
                'name' => 'Laravel ',
                'status' => '7',
                'type' => 'Skills',
                'created_at' => '2019-05-02 10:02:26',
                'updated_at' => '2019-05-02 19:06:33',
                'deleted_at' => '2019-05-02 19:06:33',
            ),
            38 => 
            array (
                'id' => 39,
                'studentid' => '2001544322',
                'name' => 'Bootstrap',
                'status' => '8',
                'type' => 'Skills',
                'created_at' => '2019-05-02 10:02:26',
                'updated_at' => '2019-05-02 19:06:33',
                'deleted_at' => '2019-05-02 19:06:33',
            ),
            39 => 
            array (
                'id' => 40,
                'studentid' => '2001544322',
                'name' => 'ASP.NET & C#',
                'status' => '6',
                'type' => 'Skills',
                'created_at' => '2019-05-02 10:02:26',
                'updated_at' => '2019-05-02 19:06:33',
                'deleted_at' => '2019-05-02 19:06:33',
            ),
            40 => 
            array (
                'id' => 41,
                'studentid' => '2001544322',
                'name' => 'Indonesian',
                'status' => 'Native',
                'type' => 'Languages',
                'created_at' => '2019-05-02 10:02:26',
                'updated_at' => '2019-05-02 19:06:33',
                'deleted_at' => '2019-05-02 19:06:33',
            ),
            41 => 
            array (
                'id' => 42,
                'studentid' => '2001544322',
                'name' => 'English',
                'status' => 'Good',
                'type' => 'Languages',
                'created_at' => '2019-05-02 10:02:26',
                'updated_at' => '2019-05-02 19:06:33',
                'deleted_at' => '2019-05-02 19:06:33',
            ),
            42 => 
            array (
                'id' => 43,
                'studentid' => '2001544322',
                'name' => 'C and C++',
                'status' => '6',
                'type' => 'Skills',
                'created_at' => '2019-05-02 19:06:33',
                'updated_at' => '2019-05-02 19:06:33',
                'deleted_at' => NULL,
            ),
            43 => 
            array (
                'id' => 44,
                'studentid' => '2001544322',
                'name' => 'Java',
                'status' => '7',
                'type' => 'Skills',
                'created_at' => '2019-05-02 19:06:33',
                'updated_at' => '2019-05-02 19:06:33',
                'deleted_at' => NULL,
            ),
            44 => 
            array (
                'id' => 45,
                'studentid' => '2001544322',
                'name' => 'Laravel ',
                'status' => '7',
                'type' => 'Skills',
                'created_at' => '2019-05-02 19:06:33',
                'updated_at' => '2019-05-02 19:06:33',
                'deleted_at' => NULL,
            ),
            45 => 
            array (
                'id' => 46,
                'studentid' => '2001544322',
                'name' => 'Bootstrap',
                'status' => '8',
                'type' => 'Skills',
                'created_at' => '2019-05-02 19:06:33',
                'updated_at' => '2019-05-02 19:06:33',
                'deleted_at' => NULL,
            ),
            46 => 
            array (
                'id' => 47,
                'studentid' => '2001544322',
                'name' => 'ASP.NET & C#',
                'status' => '6',
                'type' => 'Skills',
                'created_at' => '2019-05-02 19:06:33',
                'updated_at' => '2019-05-02 19:06:33',
                'deleted_at' => NULL,
            ),
            47 => 
            array (
                'id' => 48,
                'studentid' => '2001544322',
                'name' => 'Indonesian',
                'status' => 'Native',
                'type' => 'Languages',
                'created_at' => '2019-05-02 19:06:33',
                'updated_at' => '2019-05-02 19:06:33',
                'deleted_at' => NULL,
            ),
            48 => 
            array (
                'id' => 49,
                'studentid' => '2001544322',
                'name' => 'English',
                'status' => 'Good',
                'type' => 'Languages',
                'created_at' => '2019-05-02 19:06:33',
                'updated_at' => '2019-05-02 19:06:33',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}