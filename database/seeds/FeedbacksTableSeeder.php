<?php

use Illuminate\Database\Seeder;

class FeedbacksTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('feedbacks')->delete();
        
        \DB::table('feedbacks')->insert(array (
            0 => 
            array (
                'id' => 1,
                'userid' => '2001234567',
                'content' => 'Mantap',
                'created_at' => '2019-04-01 10:23:08',
                'updated_at' => '2019-04-01 10:23:08',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'userid' => '2000111222',
                'content' => 'oke',
                'created_at' => '2019-04-24 11:20:57',
                'updated_at' => '2019-04-24 11:20:57',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}