<?php

use Illuminate\Database\Seeder;

class DegreesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('degrees')->delete();
        
        \DB::table('degrees')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Computer Science',
                'faculty_id' => 1,
                'created_at' => '2019-04-01 10:12:06',
                'updated_at' => '2019-06-25 10:07:01',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Information Systems',
                'faculty_id' => 2,
                'created_at' => '2019-04-01 11:32:18',
                'updated_at' => '2019-04-01 12:41:39',
                'deleted_at' => '2019-04-01 12:41:39',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Master Track MTI',
                'faculty_id' => NULL,
                'created_at' => '2019-04-01 11:32:38',
                'updated_at' => '2019-04-01 12:41:50',
                'deleted_at' => '2019-04-01 12:41:50',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Information Systems',
                'faculty_id' => 5,
                'created_at' => '2019-04-01 19:46:03',
                'updated_at' => '2019-04-01 19:46:15',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Information Systems',
                'faculty_id' => NULL,
                'created_at' => '2019-04-01 19:46:03',
                'updated_at' => '2019-04-01 19:46:23',
                'deleted_at' => '2019-04-01 19:46:23',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Information Systems',
                'faculty_id' => NULL,
                'created_at' => '2019-04-01 19:46:03',
                'updated_at' => '2019-04-01 19:46:27',
                'deleted_at' => '2019-04-01 19:46:27',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Animation',
                'faculty_id' => 6,
                'created_at' => '2019-04-01 19:48:21',
                'updated_at' => '2019-04-01 19:49:08',
                'deleted_at' => '2019-04-01 19:49:08',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Animation',
                'faculty_id' => 6,
                'created_at' => '2019-04-01 19:48:21',
                'updated_at' => '2019-04-01 19:49:12',
                'deleted_at' => '2019-04-01 19:49:12',
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'Animation',
                'faculty_id' => 6,
                'created_at' => '2019-04-01 19:48:21',
                'updated_at' => '2019-04-01 19:48:21',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'test',
                'faculty_id' => 10,
                'created_at' => '2019-04-01 21:24:11',
                'updated_at' => '2019-04-01 21:24:11',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'Animation',
                'faculty_id' => NULL,
                'created_at' => '2019-04-12 23:58:37',
                'updated_at' => '2019-04-13 00:02:13',
                'deleted_at' => '2019-04-13 00:02:13',
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'Industrial Engineering',
                'faculty_id' => 12,
                'created_at' => '2019-04-12 23:59:36',
                'updated_at' => '2019-04-12 23:59:36',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'name' => 'Accounting Information Systems',
                'faculty_id' => 5,
                'created_at' => '2019-04-13 00:00:42',
                'updated_at' => '2019-04-13 00:00:42',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'name' => 'Mobile Applications',
                'faculty_id' => 1,
                'created_at' => '2019-04-13 00:01:11',
                'updated_at' => '2019-06-25 10:07:01',
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'name' => 'Management',
                'faculty_id' => 11,
                'created_at' => '2019-04-13 00:01:53',
                'updated_at' => '2019-04-13 00:01:53',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}