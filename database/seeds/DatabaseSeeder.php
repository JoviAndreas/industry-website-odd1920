<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(InsertUserCVData::class);
        $this->call(UsersTableSeeder::class);
        $this->call(DevUsersTableSeeder::class);
        $this->call(BippTableSeeder::class);
        $this->call(CampusTableSeeder::class);
        $this->call(CompaniesTableSeeder::class);
        $this->call(StudentsTableSeeder::class);
        $this->call(StudentscoresTableSeeder::class);
        $this->call(StudentsemestersTableSeeder::class);
        $this->call(CvdateinformationsTableSeeder::class);
        $this->call(CvskillinformationsTableSeeder::class);
        $this->call(DegreesTableSeeder::class);
        $this->call(DepartmentsTableSeeder::class);
        $this->call(FacultiesTableSeeder::class);
        $this->call(FeedbacksTableSeeder::class);
        $this->call(JobPositionsTableSeeder::class);
        $this->call(JobPositionsDegreesTableSeeder::class);
        $this->call(JobsTableSeeder::class);
        $this->call(JobsdegreesTableSeeder::class);
        $this->call(MailboxesTableSeeder::class);
        $this->call(RecruitmentsTableSeeder::class);
        $this->call(InterviewsTableSeeder::class);
    }
}
