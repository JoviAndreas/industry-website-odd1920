<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        \DB::table('semester')->insert([
            [
                'period' => '1910',
                'description' => 'Odd 2019/2020',
                'is_active' => '1',
                'is_current' => '1',
            ],
            [
                'period' => '1920',
                'description' => 'Even 2019/2020',
                'is_active' => '0',
                'is_current' => '0',
            ],
            [
                'period' => '2010',
                'description' => 'Odd 2020/2021',
                'is_active' => '0',
                'is_current' => '0',
            ],
            [
                'period' => '2020',
                'description' => 'Even 2020/2021',
                'is_active' => '0',
                'is_current' => '0',
            ],
            [
                'period' => '2110',
                'description' => 'Odd 2021/2022',
                'is_active' => '0',
                'is_current' => '0',
            ],
            [
                'period' => '2120',
                'description' => 'Even 2021/2022',
                'is_active' => '0',
                'is_current' => '0',
            ],
            [
                'period' => '2210',
                'description' => 'Odd 2022/2023',
                'is_active' => '0',
                'is_current' => '0',
            ],
            [
                'period' => '2220',
                'description' => 'Even 2022/2023',
                'is_active' => '0',
                'is_current' => '0',
            ],
        ]);
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'userid' => 'superadmin',
                'password' => '$2y$12$m23RS4l32V9yqS/f1Ln47ekRyWqboNauLU2qL5JWKfoRd7yLslFOy',
                'role' => 'department',
                'status' => 'active',
                'defaultpassword' => 'superadmin',
                'remember_token' => 'gUV1Q7Jhhy4HQHd4W3BZVrEEsOTodSSrYOOusOH3dSGd0qB1VbQpvkrzzoGx',
                'created_at' => '2019-04-01 00:00:00',
                'updated_at' => '2019-09-09 13:06:25',
                'deleted_at' => NULL,
            )
        ));
        
        
    }
}