<?php

use Illuminate\Database\Seeder;

class CvdateinformationsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('cvdateinformations')->delete();
        
        \DB::table('cvdateinformations')->insert(array (
            0 => 
            array (
                'id' => 1,
                'studentid' => '2001234567',
                'startdate' => 'October 2004',
                'enddate' => 'October 2010',
                'description' => 'SD Negeri 1 Jakarta',
                'role' => '',
                'jobdesc' => '',
                'type' => 'Formal',
                'created_at' => '2019-04-01 10:35:06',
                'updated_at' => '2019-04-01 10:35:06',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'studentid' => '2001234567',
                'startdate' => 'October 2010',
                'enddate' => 'October 2013',
                'description' => 'SMP Negeri 1 Jakarta',
                'role' => '',
                'jobdesc' => '',
                'type' => 'Formal',
                'created_at' => '2019-04-01 10:35:06',
                'updated_at' => '2019-04-01 10:35:06',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'studentid' => '2001234567',
                'startdate' => 'October 2013',
                'enddate' => 'October 2016',
                'description' => 'SMA Negeri 1 Jakarta',
                'role' => '',
                'jobdesc' => '',
                'type' => 'Formal',
                'created_at' => '2019-04-01 10:35:06',
                'updated_at' => '2019-04-01 10:35:06',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'studentid' => '2001234567',
                'startdate' => 'October 2016',
                'enddate' => 'Now',
                'description' => 'Bina Nusantara',
                'role' => '',
                'jobdesc' => '',
                'type' => 'Formal',
                'created_at' => '2019-04-01 10:35:06',
                'updated_at' => '2019-04-01 10:35:06',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'studentid' => '2001234567',
                'startdate' => 'October 2015',
                'enddate' => 'October 2016',
                'description' => 'Sertifikasi Binus Center',
                'role' => '',
                'jobdesc' => '',
                'type' => 'Informal',
                'created_at' => '2019-04-01 10:35:06',
                'updated_at' => '2019-04-01 10:35:06',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'studentid' => '2001234567',
                'startdate' => 'Maret 2013',
                'enddate' => '',
                'description' => 'Juara 2 Public Speaking',
                'role' => '',
                'jobdesc' => '',
                'type' => 'Award',
                'created_at' => '2019-04-01 10:35:06',
                'updated_at' => '2019-04-01 10:35:06',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'studentid' => '2001567890',
                'startdate' => 'sept 2015',
                'enddate' => 'sept 2019',
                'description' => 'English Course',
                'role' => '',
                'jobdesc' => '',
                'type' => 'Formal',
                'created_at' => '2019-04-06 14:48:11',
                'updated_at' => '2019-04-06 15:48:42',
                'deleted_at' => '2019-04-06 15:48:42',
            ),
            7 => 
            array (
                'id' => 8,
                'studentid' => '2001567890',
                'startdate' => 'sept 2015',
                'enddate' => 'sept 2019',
                'description' => 'English Course',
                'role' => '',
                'jobdesc' => '',
                'type' => 'Formal',
                'created_at' => '2019-04-06 15:48:42',
                'updated_at' => '2019-04-06 15:49:29',
                'deleted_at' => '2019-04-06 15:49:29',
            ),
            8 => 
            array (
                'id' => 9,
                'studentid' => '2001567890',
                'startdate' => 'sept 2015',
                'enddate' => 'sept 2019',
                'description' => 'English Course',
                'role' => '',
                'jobdesc' => '',
                'type' => 'Formal',
                'created_at' => '2019-04-06 15:49:29',
                'updated_at' => '2019-04-06 15:49:29',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'studentid' => '2001345780',
                'startdate' => 'sept 2015',
                'enddate' => 'sept 2019',
                'description' => 'Kursus Programming',
                'role' => '',
                'jobdesc' => '',
                'type' => 'Formal',
                'created_at' => '2019-04-13 00:13:07',
                'updated_at' => '2019-04-13 00:13:51',
                'deleted_at' => '2019-04-13 00:13:51',
            ),
            10 => 
            array (
                'id' => 11,
                'studentid' => '2001345780',
                'startdate' => 'Agustus 2016',
                'enddate' => 'Agustus 2017',
                'description' => 'Kursus Kepribadian',
                'role' => '',
                'jobdesc' => '',
                'type' => 'Informal',
                'created_at' => '2019-04-13 00:13:07',
                'updated_at' => '2019-04-13 00:13:51',
                'deleted_at' => '2019-04-13 00:13:51',
            ),
            11 => 
            array (
                'id' => 12,
                'studentid' => '2001345780',
                'startdate' => 'sept 2015',
                'enddate' => 'sept 2019',
                'description' => 'Kursus Programming',
                'role' => '',
                'jobdesc' => '',
                'type' => 'Formal',
                'created_at' => '2019-04-13 00:13:51',
                'updated_at' => '2019-04-13 00:13:51',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'studentid' => '2001345780',
                'startdate' => 'Agustus 2016',
                'enddate' => 'Agustus 2017',
                'description' => 'Kursus Kepribadian',
                'role' => '',
                'jobdesc' => '',
                'type' => 'Informal',
                'created_at' => '2019-04-13 00:13:51',
                'updated_at' => '2019-04-13 00:13:51',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'studentid' => '2001123456',
                'startdate' => 'sept 2016',
                'enddate' => 'sept 2019',
                'description' => 'BBS - Management',
                'role' => '',
                'jobdesc' => '',
                'type' => 'Formal',
                'created_at' => '2019-04-13 00:17:36',
                'updated_at' => '2019-04-13 00:17:36',
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'studentid' => '2001123456',
                'startdate' => 'Januari 2017',
                'enddate' => 'December 2017',
                'description' => 'Wu Shu',
                'role' => '',
                'jobdesc' => '',
                'type' => 'Informal',
                'created_at' => '2019-04-13 00:17:36',
                'updated_at' => '2019-04-13 00:17:36',
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'studentid' => '2003345345',
                'startdate' => 'sept 2016',
                'enddate' => 'sept 2019',
                'description' => 'BINUS',
                'role' => '',
                'jobdesc' => '',
                'type' => 'Formal',
                'created_at' => '2019-04-13 00:27:10',
                'updated_at' => '2019-04-13 00:27:10',
                'deleted_at' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'studentid' => '2003345345',
                'startdate' => 'januari 2019',
                'enddate' => 'Maret 2019',
                'description' => 'Kursus X',
                'role' => '',
                'jobdesc' => '',
                'type' => 'Informal',
                'created_at' => '2019-04-13 00:27:10',
                'updated_at' => '2019-04-13 00:27:10',
                'deleted_at' => NULL,
            ),
            17 => 
            array (
                'id' => 18,
                'studentid' => '2003890890',
                'startdate' => 'sept 2016',
                'enddate' => 'sept 2019',
                'description' => 'binus',
                'role' => '',
                'jobdesc' => '',
                'type' => 'Formal',
                'created_at' => '2019-04-13 00:42:55',
                'updated_at' => '2019-04-13 00:42:55',
                'deleted_at' => NULL,
            ),
            18 => 
            array (
                'id' => 19,
                'studentid' => '2003890890',
                'startdate' => 'jan 2019',
                'enddate' => 'april 2019',
                'description' => 'teknik abc',
                'role' => '',
                'jobdesc' => '',
                'type' => 'Informal',
                'created_at' => '2019-04-13 00:42:55',
                'updated_at' => '2019-04-13 00:42:55',
                'deleted_at' => NULL,
            ),
            19 => 
            array (
                'id' => 20,
                'studentid' => '2000789789',
                'startdate' => 'sept 2016',
                'enddate' => 'sept 2019',
                'description' => 'BINUS',
                'role' => '',
                'jobdesc' => '',
                'type' => 'Formal',
                'created_at' => '2019-04-13 00:47:26',
                'updated_at' => '2019-04-13 00:47:26',
                'deleted_at' => NULL,
            ),
            20 => 
            array (
                'id' => 21,
                'studentid' => '2000789789',
                'startdate' => 'Januari 2019',
                'enddate' => 'Maret 2019',
                'description' => 'perpajakan',
                'role' => '',
                'jobdesc' => '',
                'type' => 'Informal',
                'created_at' => '2019-04-13 00:47:26',
                'updated_at' => '2019-04-13 00:47:26',
                'deleted_at' => NULL,
            ),
            21 => 
            array (
                'id' => 22,
                'studentid' => '2000134134',
                'startdate' => 'sept 2016',
                'enddate' => 'sept 2019',
                'description' => 'binus',
                'role' => '',
                'jobdesc' => '',
                'type' => 'Formal',
                'created_at' => '2019-04-13 00:50:43',
                'updated_at' => '2019-04-13 00:50:43',
                'deleted_at' => NULL,
            ),
            22 => 
            array (
                'id' => 23,
                'studentid' => '2000134134',
                'startdate' => 'sept 2018',
                'enddate' => 'juli 2018',
                'description' => 'laporan keuangan',
                'role' => '',
                'jobdesc' => '',
                'type' => 'Informal',
                'created_at' => '2019-04-13 00:50:43',
                'updated_at' => '2019-04-13 00:50:43',
                'deleted_at' => NULL,
            ),
            23 => 
            array (
                'id' => 24,
                'studentid' => '2001898902',
                'startdate' => 'sept 2016',
                'enddate' => 'sept 2019',
                'description' => 'binus',
                'role' => '',
                'jobdesc' => '',
                'type' => 'Formal',
                'created_at' => '2019-04-13 00:54:30',
                'updated_at' => '2019-04-13 00:54:30',
                'deleted_at' => NULL,
            ),
            24 => 
            array (
                'id' => 25,
                'studentid' => '2001898902',
                'startdate' => 'april 2018',
                'enddate' => 'desember 2018',
                'description' => 'IOS ',
                'role' => '',
                'jobdesc' => '',
                'type' => 'Informal',
                'created_at' => '2019-04-13 00:54:30',
                'updated_at' => '2019-04-13 00:54:30',
                'deleted_at' => NULL,
            ),
            25 => 
            array (
                'id' => 26,
                'studentid' => '2001098789',
                'startdate' => 'sept 2016',
                'enddate' => 'sept 2019',
                'description' => 'BINUS',
                'role' => '',
                'jobdesc' => '',
                'type' => 'Formal',
                'created_at' => '2019-04-13 07:58:57',
                'updated_at' => '2019-04-13 07:58:57',
                'deleted_at' => NULL,
            ),
            26 => 
            array (
                'id' => 27,
                'studentid' => '2001098789',
                'startdate' => 'Januari 2019',
                'enddate' => 'April 2019',
                'description' => 'Teknik Design',
                'role' => '',
                'jobdesc' => '',
                'type' => 'Informal',
                'created_at' => '2019-04-13 07:58:57',
                'updated_at' => '2019-04-13 07:58:57',
                'deleted_at' => NULL,
            ),
            27 => 
            array (
                'id' => 28,
                'studentid' => '2000111222',
                'startdate' => 'sept 2016',
                'enddate' => 'sept 2019',
                'description' => 'BINUS',
                'role' => '',
                'jobdesc' => '',
                'type' => 'Formal',
                'created_at' => '2019-04-13 08:02:28',
                'updated_at' => '2019-04-13 08:02:28',
                'deleted_at' => NULL,
            ),
            28 => 
            array (
                'id' => 29,
                'studentid' => '2000111222',
                'startdate' => 'Januari 2019',
                'enddate' => 'April 2019',
                'description' => 'Animation Critical Design',
                'role' => '',
                'jobdesc' => '',
                'type' => 'Informal',
                'created_at' => '2019-04-13 08:02:28',
                'updated_at' => '2019-04-13 08:02:28',
                'deleted_at' => NULL,
            ),
            29 => 
            array (
                'id' => 30,
                'studentid' => '2001544322',
                'startdate' => 'Juli 2004',
                'enddate' => 'Juni 2010',
                'description' => 'SD Tarakanita Citra Raya',
                'role' => '',
                'jobdesc' => '',
                'type' => 'Formal',
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:02:26',
                'deleted_at' => '2019-05-02 10:02:26',
            ),
            30 => 
            array (
                'id' => 31,
                'studentid' => '2001544322',
                'startdate' => 'Juli 2010',
                'enddate' => 'Juni 2013',
                'description' => 'SMP Tarakanita Citra Raya',
                'role' => '',
                'jobdesc' => '',
                'type' => 'Formal',
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:02:26',
                'deleted_at' => '2019-05-02 10:02:26',
            ),
            31 => 
            array (
                'id' => 32,
                'studentid' => '2001544322',
                'startdate' => 'Juli 2013',
                'enddate' => 'Juni 2016',
                'description' => 'SMA Negeri 1 Kabupaten Tangerang
',
                'role' => '',
                'jobdesc' => '',
                'type' => 'Formal',
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:02:26',
                'deleted_at' => '2019-05-02 10:02:26',
            ),
            32 => 
            array (
                'id' => 33,
                'studentid' => '2001544322',
                'startdate' => 'September 2016',
                'enddate' => 'Present',
                'description' => 'Universitas Bina Nusantara',
                'role' => '',
                'jobdesc' => '',
                'type' => 'Formal',
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:02:26',
                'deleted_at' => '2019-05-02 10:02:26',
            ),
            33 => 
            array (
                'id' => 34,
                'studentid' => '2001544322',
                'startdate' => 'April 2014',
                'enddate' => 'Juni 2015 ',
                'description' => 'English Course at LBPP LIA Tangeran',
                'role' => '',
                'jobdesc' => '',
                'type' => 'Informal',
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:02:26',
                'deleted_at' => '2019-05-02 10:02:26',
            ),
            34 => 
            array (
                'id' => 35,
                'studentid' => '2001544322',
                'startdate' => '24 Oktober 2016',
                'enddate' => ' 4 Januari 2017',
            'description' => 'Training NAR 17-1 BINUS ( Pre Training - Core Training )',
                'role' => '',
                'jobdesc' => '',
                'type' => 'Informal',
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:02:26',
                'deleted_at' => '2019-05-02 10:02:26',
            ),
            35 => 
            array (
                'id' => 36,
                'studentid' => '2001544322',
                'startdate' => '16 Oktober 2018',
                'enddate' => '18 Oktober 2018',
                'description' => 'ITB Cloud Computing Training by AWS Educate',
                'role' => '',
                'jobdesc' => '',
                'type' => 'Informal',
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:02:26',
                'deleted_at' => '2019-05-02 10:02:26',
            ),
            36 => 
            array (
                'id' => 37,
                'studentid' => '2001544322',
                'startdate' => 'Oktober 2016',
                'enddate' => 'Januari 2017',
                'description' => 'Keluarga Mahasiswa Katolik Binus',
                'role' => 'Communication Media Activist',
                'jobdesc' => 'Make poster and handle event documentation.',
                'type' => 'Organization',
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:02:26',
                'deleted_at' => '2019-05-02 10:02:26',
            ),
            37 => 
            array (
                'id' => 38,
                'studentid' => '2001544322',
                'startdate' => 'Januari 2017',
                'enddate' => 'Januari 2018',
                'description' => 'Keluarga Mahasiswa Katolik Binus',
                'role' => 'Communication Media Committe',
                'jobdesc' => 'Make poster, banner and handle event documentation.',
                'type' => 'Organization',
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:02:26',
                'deleted_at' => '2019-05-02 10:02:26',
            ),
            38 => 
            array (
                'id' => 39,
                'studentid' => '2001544322',
                'startdate' => 'Juli 2004',
                'enddate' => 'Juni 2010',
                'description' => 'SD Tarakanita Citra Raya',
                'role' => '',
                'jobdesc' => '',
                'type' => 'Formal',
                'created_at' => '2019-05-02 10:02:26',
                'updated_at' => '2019-05-02 19:06:33',
                'deleted_at' => '2019-05-02 19:06:33',
            ),
            39 => 
            array (
                'id' => 40,
                'studentid' => '2001544322',
                'startdate' => 'Juli 2010',
                'enddate' => 'Juni 2013',
                'description' => 'SMP Tarakanita Citra Raya',
                'role' => '',
                'jobdesc' => '',
                'type' => 'Formal',
                'created_at' => '2019-05-02 10:02:26',
                'updated_at' => '2019-05-02 19:06:33',
                'deleted_at' => '2019-05-02 19:06:33',
            ),
            40 => 
            array (
                'id' => 41,
                'studentid' => '2001544322',
                'startdate' => 'Juli 2013',
                'enddate' => 'Juni 2016',
                'description' => 'SMA Negeri 1 Kabupaten Tangerang
',
                'role' => '',
                'jobdesc' => '',
                'type' => 'Formal',
                'created_at' => '2019-05-02 10:02:26',
                'updated_at' => '2019-05-02 19:06:33',
                'deleted_at' => '2019-05-02 19:06:33',
            ),
            41 => 
            array (
                'id' => 42,
                'studentid' => '2001544322',
                'startdate' => 'September 2016',
                'enddate' => 'Present',
                'description' => 'Universitas Bina Nusantara',
                'role' => '',
                'jobdesc' => '',
                'type' => 'Formal',
                'created_at' => '2019-05-02 10:02:26',
                'updated_at' => '2019-05-02 19:06:33',
                'deleted_at' => '2019-05-02 19:06:33',
            ),
            42 => 
            array (
                'id' => 43,
                'studentid' => '2001544322',
                'startdate' => 'April 2014',
                'enddate' => 'Juni 2015 ',
                'description' => 'English Course at LBPP LIA Tangerang',
                'role' => '',
                'jobdesc' => '',
                'type' => 'Informal',
                'created_at' => '2019-05-02 10:02:26',
                'updated_at' => '2019-05-02 19:06:33',
                'deleted_at' => '2019-05-02 19:06:33',
            ),
            43 => 
            array (
                'id' => 44,
                'studentid' => '2001544322',
                'startdate' => '24 Oktober 2016',
                'enddate' => ' 4 Januari 2017',
            'description' => 'Training NAR 17-1 BINUS ( Pre Training - Core Training )',
                'role' => '',
                'jobdesc' => '',
                'type' => 'Informal',
                'created_at' => '2019-05-02 10:02:26',
                'updated_at' => '2019-05-02 19:06:33',
                'deleted_at' => '2019-05-02 19:06:33',
            ),
            44 => 
            array (
                'id' => 45,
                'studentid' => '2001544322',
                'startdate' => '16 Oktober 2018',
                'enddate' => '18 Oktober 2018',
                'description' => 'ITB Cloud Computing Training by AWS Educate',
                'role' => '',
                'jobdesc' => '',
                'type' => 'Informal',
                'created_at' => '2019-05-02 10:02:26',
                'updated_at' => '2019-05-02 19:06:33',
                'deleted_at' => '2019-05-02 19:06:33',
            ),
            45 => 
            array (
                'id' => 46,
                'studentid' => '2001544322',
                'startdate' => 'Oktober 2016',
                'enddate' => 'Januari 2017',
                'description' => 'Keluarga Mahasiswa Katolik Binus',
                'role' => 'Communication Media Activist',
                'jobdesc' => 'Make poster and handle event documentation.',
                'type' => 'Organization',
                'created_at' => '2019-05-02 10:02:26',
                'updated_at' => '2019-05-02 19:06:33',
                'deleted_at' => '2019-05-02 19:06:33',
            ),
            46 => 
            array (
                'id' => 47,
                'studentid' => '2001544322',
                'startdate' => 'Januari 2017',
                'enddate' => 'Januari 2018',
                'description' => 'Keluarga Mahasiswa Katolik Binus',
                'role' => 'Communication Media Committe',
                'jobdesc' => 'Make poster, banner and handle event documentation.',
                'type' => 'Organization',
                'created_at' => '2019-05-02 10:02:26',
                'updated_at' => '2019-05-02 19:06:33',
                'deleted_at' => '2019-05-02 19:06:33',
            ),
            47 => 
            array (
                'id' => 48,
                'studentid' => '2001544322',
                'startdate' => 'Juli 2004',
                'enddate' => 'Juni 2010',
                'description' => 'SD Tarakanita Citra Raya',
                'role' => '',
                'jobdesc' => '',
                'type' => 'Formal',
                'created_at' => '2019-05-02 19:06:33',
                'updated_at' => '2019-05-02 19:06:33',
                'deleted_at' => NULL,
            ),
            48 => 
            array (
                'id' => 49,
                'studentid' => '2001544322',
                'startdate' => 'Juli 2010',
                'enddate' => 'Juni 2013',
                'description' => 'SMP Tarakanita Citra Raya',
                'role' => '',
                'jobdesc' => '',
                'type' => 'Formal',
                'created_at' => '2019-05-02 19:06:33',
                'updated_at' => '2019-05-02 19:06:33',
                'deleted_at' => NULL,
            ),
            49 => 
            array (
                'id' => 50,
                'studentid' => '2001544322',
                'startdate' => 'Juli 2013',
                'enddate' => 'Juni 2016',
                'description' => 'SMA Negeri 1 Kabupaten Tangerang
',
                'role' => '',
                'jobdesc' => '',
                'type' => 'Formal',
                'created_at' => '2019-05-02 19:06:33',
                'updated_at' => '2019-05-02 19:06:33',
                'deleted_at' => NULL,
            ),
            50 => 
            array (
                'id' => 51,
                'studentid' => '2001544322',
                'startdate' => 'September 2016',
                'enddate' => 'Present',
                'description' => 'Universitas Bina Nusantara',
                'role' => '',
                'jobdesc' => '',
                'type' => 'Formal',
                'created_at' => '2019-05-02 19:06:33',
                'updated_at' => '2019-05-02 19:06:33',
                'deleted_at' => NULL,
            ),
            51 => 
            array (
                'id' => 52,
                'studentid' => '2001544322',
                'startdate' => 'April 2014',
                'enddate' => 'Juni 2015 ',
                'description' => 'English Course at LBPP LIA Tangerang',
                'role' => '',
                'jobdesc' => '',
                'type' => 'Informal',
                'created_at' => '2019-05-02 19:06:33',
                'updated_at' => '2019-05-02 19:06:33',
                'deleted_at' => NULL,
            ),
            52 => 
            array (
                'id' => 53,
                'studentid' => '2001544322',
                'startdate' => '24 Oktober 2016',
                'enddate' => ' 4 Januari 2017',
            'description' => 'Training NAR 17-1 BINUS ( Pre Training - Core Training )',
                'role' => '',
                'jobdesc' => '',
                'type' => 'Informal',
                'created_at' => '2019-05-02 19:06:33',
                'updated_at' => '2019-05-02 19:06:33',
                'deleted_at' => NULL,
            ),
            53 => 
            array (
                'id' => 54,
                'studentid' => '2001544322',
                'startdate' => '16 Oktober 2018',
                'enddate' => '18 Oktober 2018',
                'description' => 'ITB Cloud Computing Training by AWS Educate',
                'role' => '',
                'jobdesc' => '',
                'type' => 'Informal',
                'created_at' => '2019-05-02 19:06:33',
                'updated_at' => '2019-05-02 19:06:33',
                'deleted_at' => NULL,
            ),
            54 => 
            array (
                'id' => 55,
                'studentid' => '2001544322',
                'startdate' => 'Oktober 2016',
                'enddate' => 'Januari 2017',
                'description' => 'Keluarga Mahasiswa Katolik Binus',
                'role' => 'Communication Media Activist',
                'jobdesc' => 'Make poster and handle event documentation.',
                'type' => 'Organization',
                'created_at' => '2019-05-02 19:06:33',
                'updated_at' => '2019-05-02 19:06:33',
                'deleted_at' => NULL,
            ),
            55 => 
            array (
                'id' => 56,
                'studentid' => '2001544322',
                'startdate' => 'Januari 2017',
                'enddate' => 'Januari 2018',
                'description' => 'Keluarga Mahasiswa Katolik Binus',
                'role' => 'Communication Media Committe',
                'jobdesc' => 'Make poster, banner and handle event documentation.',
                'type' => 'Organization',
                'created_at' => '2019-05-02 19:06:33',
                'updated_at' => '2019-05-02 19:06:33',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}