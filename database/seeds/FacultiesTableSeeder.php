<?php

use Illuminate\Database\Seeder;

class FacultiesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('faculties')->delete();
        
        \DB::table('faculties')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'School of Computer Science',
                'password_master' => NULL,
                'created_at' => '2019-04-01 00:00:00',
                'updated_at' => '2019-04-01 10:11:36',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'School of Information Systems',
                'password_master' => NULL,
                'created_at' => '2019-04-01 11:32:07',
                'updated_at' => '2019-04-01 12:41:39',
                'deleted_at' => '2019-04-01 12:41:39',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'School of Information Systems',
                'password_master' => NULL,
                'created_at' => '2019-04-01 19:45:33',
                'updated_at' => '2019-04-01 19:45:42',
                'deleted_at' => '2019-04-01 19:45:42',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'School of Information Systems',
                'password_master' => NULL,
                'created_at' => '2019-04-01 19:45:33',
                'updated_at' => '2019-04-01 19:45:40',
                'deleted_at' => '2019-04-01 19:45:40',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'School of Information Systems',
                'password_master' => NULL,
                'created_at' => '2019-04-01 19:45:34',
                'updated_at' => '2019-04-01 19:45:34',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'School of Design',
                'password_master' => NULL,
                'created_at' => '2019-04-01 19:46:37',
                'updated_at' => '2019-04-01 19:46:37',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'School of Design',
                'password_master' => NULL,
                'created_at' => '2019-04-01 19:46:37',
                'updated_at' => '2019-04-01 19:48:06',
                'deleted_at' => '2019-04-01 19:48:06',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'School of Design',
                'password_master' => NULL,
                'created_at' => '2019-04-01 19:46:37',
                'updated_at' => '2019-04-01 19:48:09',
                'deleted_at' => '2019-04-01 19:48:09',
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'School of 123',
                'password_master' => NULL,
                'created_at' => '2019-04-01 21:17:26',
                'updated_at' => '2019-04-01 21:17:26',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'test123',
                'password_master' => NULL,
                'created_at' => '2019-04-01 21:17:47',
                'updated_at' => '2019-04-01 21:17:47',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'BINUS Business School',
                'password_master' => NULL,
                'created_at' => '2019-04-12 23:58:24',
                'updated_at' => '2019-04-12 23:58:24',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'Faculty of Engineering',
                'password_master' => NULL,
                'created_at' => '2019-04-12 23:59:21',
                'updated_at' => '2019-04-12 23:59:21',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}