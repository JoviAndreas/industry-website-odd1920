<?php

use Illuminate\Database\Seeder;

class CompaniesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('companies')->delete();
        
        \DB::table('companies')->insert(array (
            0 => 
            array (
                'id' => 1,
                'companyid' => 'djarum',
                'bipp_id' => 1,
                'faculties' => '',
                'name' => 'Djarum',
                'description' => '',
                'photo' => 'photo.jpg',
                'email' => 'susi@jarum.co.id',
                'phone' => '0814567890',
                'website' => 'http://www.jarum.co.id',
                'pic' => 'Susi ',
                'address' => 'Jl. Kebayoran Baru No. 19, Jakarta Barat ',
                'state' => 'open',
                'business_category' => NULL,
                'position' => NULL,
                'city' => NULL,
                'postal_code' => NULL,
                'ext' => NULL,
                'fax' => NULL,
                'cellphone' => NULL,
                'created_at' => '2019-04-01 10:52:00',
                'updated_at' => '2019-06-21 01:03:44',
                'deleted_at' => '2019-06-21 01:03:44',
            ),
            1 => 
            array (
                'id' => 2,
                'companyid' => 'blibli',
                'bipp_id' => NULL,
                'faculties' => '',
                'name' => 'Blibli',
                'description' => 'Blibli Company',
                'photo' => '',
                'email' => 'blibli@gmail.com',
                'phone' => '0897867564',
                'website' => 'https://www.blibli.com/',
                'pic' => '',
                'address' => '',
                'state' => 'open',
                'business_category' => NULL,
                'position' => NULL,
                'city' => NULL,
                'postal_code' => NULL,
                'ext' => NULL,
                'fax' => NULL,
                'cellphone' => NULL,
                'created_at' => '2019-04-01 11:09:49',
                'updated_at' => '2019-04-01 11:09:49',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'companyid' => 'xx',
                'bipp_id' => NULL,
                'faculties' => '',
                'name' => 'PT. X',
                'description' => 'XYZ',
                'photo' => '',
                'email' => 'companyx@mail.com',
                'phone' => '12345678',
                'website' => 'http://www.x.com',
                'pic' => '',
                'address' => '',
                'state' => 'open',
                'business_category' => NULL,
                'position' => NULL,
                'city' => NULL,
                'postal_code' => NULL,
                'ext' => NULL,
                'fax' => NULL,
                'cellphone' => NULL,
                'created_at' => '2019-04-06 15:50:27',
                'updated_at' => '2019-04-06 15:50:27',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'companyid' => 'abcabc',
                'bipp_id' => 1,
                'faculties' => '',
                'name' => 'PT. ABC',
                'description' => 'PT. ABC',
                'photo' => 'photo.png',
                'email' => 'abc@mail.com',
                'phone' => '1234567',
                'website' => 'http://www.abc.com',
                'pic' => 'Susi',
                'address' => 'Jl. Besar Raya 8',
                'state' => 'open',
                'business_category' => NULL,
                'position' => NULL,
                'city' => NULL,
                'postal_code' => NULL,
                'ext' => NULL,
                'fax' => NULL,
                'cellphone' => NULL,
                'created_at' => '2019-04-08 12:56:19',
                'updated_at' => '2019-04-13 14:04:12',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'companyid' => 'cvcv',
                'bipp_id' => NULL,
                'faculties' => '',
                'name' => 'PT. CV',
                'description' => 'PT. CV jaya selalu',
                'photo' => '',
                'email' => 'cv@mail.com',
                'phone' => '021-567890',
                'website' => 'http://www.cv.com',
                'pic' => '',
                'address' => '',
                'state' => 'open',
                'business_category' => NULL,
                'position' => NULL,
                'city' => NULL,
                'postal_code' => NULL,
                'ext' => NULL,
                'fax' => NULL,
                'cellphone' => NULL,
                'created_at' => '2019-04-24 11:10:26',
                'updated_at' => '2019-04-24 11:10:26',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'companyid' => 'konoha',
                'bipp_id' => NULL,
                'faculties' => '',
                'name' => 'Konoha',
                'description' => 'konoha adalah sebuah perusahaan multinasional',
                'photo' => '',
                'email' => 'konoha@gmail.com',
                'phone' => '0211111321',
                'website' => 'http://www.konoha.com',
                'pic' => '',
                'address' => '',
                'state' => 'open',
                'business_category' => NULL,
                'position' => NULL,
                'city' => NULL,
                'postal_code' => NULL,
                'ext' => NULL,
                'fax' => NULL,
                'cellphone' => NULL,
                'created_at' => '2019-04-26 15:46:25',
                'updated_at' => '2019-06-21 01:38:32',
                'deleted_at' => '2019-06-21 01:38:32',
            ),
            6 => 
            array (
                'id' => 7,
                'companyid' => 'testtest',
                'bipp_id' => NULL,
                'faculties' => '',
                'name' => 'test',
                'description' => 'test',
                'photo' => '',
                'email' => 'test@gmail.com',
                'phone' => '08123123123123',
                'website' => 'http://www.test.com',
                'pic' => '',
                'address' => '',
                'state' => 'open',
                'business_category' => NULL,
                'position' => NULL,
                'city' => NULL,
                'postal_code' => NULL,
                'ext' => NULL,
                'fax' => NULL,
                'cellphone' => NULL,
                'created_at' => '2019-04-30 13:54:35',
                'updated_at' => '2019-04-30 13:54:35',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'companyid' => 'sukasuka',
                'bipp_id' => NULL,
                'faculties' => '',
                'name' => 'sukasuka',
                'description' => 'suka suka',
                'photo' => '',
                'email' => 'suka@gmail.com',
                'phone' => '123456789',
                'website' => 'http://www.suka.com',
                'pic' => '',
                'address' => '',
                'state' => 'open',
                'business_category' => NULL,
                'position' => NULL,
                'city' => NULL,
                'postal_code' => NULL,
                'ext' => NULL,
                'fax' => NULL,
                'cellphone' => NULL,
                'created_at' => '2019-05-09 23:38:51',
                'updated_at' => '2019-05-09 23:38:51',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}