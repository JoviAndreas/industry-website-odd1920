<?php

use Illuminate\Database\Seeder;

class CampusTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('campus')->delete();
        
        \DB::table('campus')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Kemanggisan',
                'created_at' => '2019-06-12 12:38:17',
                'updated_at' => '0000-00-00 00:00:00',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Alam Sutera',
                'created_at' => '2019-06-12 12:38:17',
                'updated_at' => '0000-00-00 00:00:00',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Bekasi',
                'created_at' => '2019-06-12 12:38:34',
                'updated_at' => '0000-00-00 00:00:00',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Malang',
                'created_at' => '2019-06-12 12:38:34',
                'updated_at' => '0000-00-00 00:00:00',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Bandung',
                'created_at' => '2019-06-12 12:38:52',
                'updated_at' => '0000-00-00 00:00:00',
            ),
        ));
        
        
    }
}