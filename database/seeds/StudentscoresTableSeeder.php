<?php

use Illuminate\Database\Seeder;

class StudentscoresTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        \DB::table('studentscores')->insert(array (
            0 => 
            array (
                'id' => 1,
                'studentid' => '2001234567',
                'code' => 'CHAR6013',
                'name' => 'Character Building: Pancasila',
                'grade' => 'A-',
                'created_at' => '2019-04-01 10:35:06',
                'updated_at' => '2019-04-01 10:35:06',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'studentid' => '2001234567',
                'code' => 'COMP6047',
                'name' => 'Algorithm and Programming',
                'grade' => 'A',
                'created_at' => '2019-04-01 10:35:06',
                'updated_at' => '2019-04-01 10:35:06',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'studentid' => '2001234567',
                'code' => 'COMP6060',
                'name' => 'Programming Language Concepts',
                'grade' => 'C',
                'created_at' => '2019-04-01 10:35:06',
                'updated_at' => '2019-04-01 10:35:06',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'studentid' => '2001234567',
                'code' => 'ENGL6130',
                'name' => 'English for Business Presentation',
                'grade' => 'B+',
                'created_at' => '2019-04-01 10:35:06',
                'updated_at' => '2019-04-01 10:35:06',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'studentid' => '2001234567',
                'code' => 'MATH6025',
                'name' => 'Discrete Mathematics',
                'grade' => 'B-',
                'created_at' => '2019-04-01 10:35:06',
                'updated_at' => '2019-04-01 10:35:06',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'studentid' => '2001234567',
                'code' => 'MATH6031',
                'name' => 'Calculus',
                'grade' => 'D',
                'created_at' => '2019-04-01 10:35:06',
                'updated_at' => '2019-04-01 10:35:06',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'studentid' => '2001234567',
                'code' => 'CHAR6014',
                'name' => 'Character Building: Kewarganegaraan',
                'grade' => 'A',
                'created_at' => '2019-04-01 10:35:06',
                'updated_at' => '2019-04-01 10:35:06',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'studentid' => '2001234567',
                'code' => 'COMP6048',
                'name' => 'Data Structures',
                'grade' => 'B+',
                'created_at' => '2019-04-01 10:35:06',
                'updated_at' => '2019-04-01 10:35:06',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'studentid' => '2001234567',
                'code' => 'COMP6056',
                'name' => 'Program Design Methods',
                'grade' => 'A-',
                'created_at' => '2019-04-01 10:35:06',
                'updated_at' => '2019-04-01 10:35:06',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'studentid' => '2001234567',
                'code' => 'COMP6175',
                'name' => 'Object Oriented Programming',
                'grade' => 'B',
                'created_at' => '2019-04-01 10:35:06',
                'updated_at' => '2019-04-01 10:35:06',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'studentid' => '2001234567',
                'code' => 'ENGL6131',
                'name' => 'English for Written Business Communication',
                'grade' => 'B+',
                'created_at' => '2019-04-01 10:35:06',
                'updated_at' => '2019-04-01 10:35:06',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'studentid' => '2001234567',
                'code' => 'LANG6061',
                'name' => 'Indonesian',
                'grade' => 'B',
                'created_at' => '2019-04-01 10:35:06',
                'updated_at' => '2019-04-01 10:35:06',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'studentid' => '2001234567',
                'code' => 'MATH6030',
                'name' => 'Linear Algebra',
                'grade' => 'B',
                'created_at' => '2019-04-01 10:35:06',
                'updated_at' => '2019-04-01 10:35:06',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'studentid' => '2001234567',
                'code' => 'CHAR6015',
                'name' => 'Character Building: Agama',
                'grade' => 'A-',
                'created_at' => '2019-04-01 10:35:06',
                'updated_at' => '2019-04-01 10:35:06',
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'studentid' => '2001234567',
                'code' => 'COMP6049',
                'name' => 'Algorithm Design and Analysis',
                'grade' => 'B',
                'created_at' => '2019-04-01 10:35:06',
                'updated_at' => '2019-04-01 10:35:06',
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'studentid' => '2001234567',
                'code' => 'COMP6065',
                'name' => 'Artificial Intelligence',
                'grade' => 'B',
                'created_at' => '2019-04-01 10:35:06',
                'updated_at' => '2019-04-01 10:35:06',
                'deleted_at' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'studentid' => '2001234567',
                'code' => 'CPEN6098',
                'name' => 'Computer Networks',
                'grade' => 'A',
                'created_at' => '2019-04-01 10:35:06',
                'updated_at' => '2019-04-01 10:35:06',
                'deleted_at' => NULL,
            ),
            17 => 
            array (
                'id' => 18,
                'studentid' => '2001234567',
                'code' => 'ENTR6003',
                'name' => 'Entrepreneurship I',
                'grade' => 'B',
                'created_at' => '2019-04-01 10:35:06',
                'updated_at' => '2019-04-01 10:35:06',
                'deleted_at' => NULL,
            ),
            18 => 
            array (
                'id' => 19,
                'studentid' => '2001234567',
                'code' => 'ISYS6169',
                'name' => 'Database Systems',
                'grade' => 'A-',
                'created_at' => '2019-04-01 10:35:06',
                'updated_at' => '2019-04-01 10:35:06',
                'deleted_at' => NULL,
            ),
            19 => 
            array (
                'id' => 20,
                'studentid' => '2001234567',
                'code' => 'COMP6100',
                'name' => 'Software Engineering',
                'grade' => 'A-',
                'created_at' => '2019-04-01 10:35:06',
                'updated_at' => '2019-04-01 10:35:06',
                'deleted_at' => NULL,
            ),
            20 => 
            array (
                'id' => 21,
                'studentid' => '2001234567',
                'code' => 'COMP6176',
                'name' => 'Human and Computer Interaction',
                'grade' => 'A-',
                'created_at' => '2019-04-01 10:35:06',
                'updated_at' => '2019-04-01 10:35:06',
                'deleted_at' => NULL,
            ),
            21 => 
            array (
                'id' => 22,
                'studentid' => '2001234567',
                'code' => 'COMP7084',
                'name' => 'Multimedia Systems',
                'grade' => 'B+',
                'created_at' => '2019-04-01 10:35:06',
                'updated_at' => '2019-04-01 10:35:06',
                'deleted_at' => NULL,
            ),
            22 => 
            array (
                'id' => 23,
                'studentid' => '2001234567',
                'code' => 'COMP7094',
                'name' => 'Multimedia Programming Foundation',
                'grade' => 'B+',
                'created_at' => '2019-04-01 10:35:06',
                'updated_at' => '2019-04-01 10:35:06',
                'deleted_at' => NULL,
            ),
            23 => 
            array (
                'id' => 24,
                'studentid' => '2001234567',
                'code' => 'COMP7110',
                'name' => 'Computer Graphic',
                'grade' => 'A',
                'created_at' => '2019-04-01 10:35:06',
                'updated_at' => '2019-04-01 10:35:06',
                'deleted_at' => NULL,
            ),
            24 => 
            array (
                'id' => 25,
                'studentid' => '2001234567',
                'code' => 'COMP7128',
                'name' => 'Game Design',
                'grade' => 'B',
                'created_at' => '2019-04-01 10:35:06',
                'updated_at' => '2019-04-01 10:35:06',
                'deleted_at' => NULL,
            ),
            25 => 
            array (
                'id' => 26,
                'studentid' => '2001234567',
                'code' => 'STAT6021',
                'name' => 'Research Methodology',
                'grade' => 'D',
                'created_at' => '2019-04-01 10:35:06',
                'updated_at' => '2019-04-01 10:35:06',
                'deleted_at' => NULL,
            ),
            26 => 
            array (
                'id' => 27,
                'studentid' => '2001234567',
                'code' => 'COMP6062',
                'name' => 'Compilation Techniques',
                'grade' => 'B+',
                'created_at' => '2019-04-01 10:35:06',
                'updated_at' => '2019-04-01 10:35:06',
                'deleted_at' => NULL,
            ),
            27 => 
            array (
                'id' => 28,
                'studentid' => '2001234567',
                'code' => 'COMP6099',
                'name' => 'Advanced Object Oriented Programming',
                'grade' => 'A',
                'created_at' => '2019-04-01 10:35:06',
                'updated_at' => '2019-04-01 10:35:06',
                'deleted_at' => NULL,
            ),
            28 => 
            array (
                'id' => 29,
                'studentid' => '2001234567',
                'code' => 'COMP6144',
                'name' => 'Web Programming',
                'grade' => 'A-',
                'created_at' => '2019-04-01 10:35:06',
                'updated_at' => '2019-04-01 10:35:06',
                'deleted_at' => NULL,
            ),
            29 => 
            array (
                'id' => 30,
                'studentid' => '2001234567',
                'code' => 'COMP6153',
                'name' => 'Operating System',
                'grade' => 'B+',
                'created_at' => '2019-04-01 10:35:06',
                'updated_at' => '2019-04-01 10:35:06',
                'deleted_at' => NULL,
            ),
            30 => 
            array (
                'id' => 31,
                'studentid' => '2001234567',
                'code' => 'COMP7139',
                'name' => 'Game Programming',
                'grade' => 'A',
                'created_at' => '2019-04-01 10:35:06',
                'updated_at' => '2019-04-01 10:35:06',
                'deleted_at' => NULL,
            ),
            31 => 
            array (
                'id' => 32,
                'studentid' => '2001234567',
                'code' => 'COMP8129',
                'name' => 'User Experience',
                'grade' => 'A',
                'created_at' => '2019-04-01 10:35:06',
                'updated_at' => '2019-04-01 10:35:06',
                'deleted_at' => NULL,
            ),
            32 => 
            array (
                'id' => 33,
                'studentid' => '2001234567',
                'code' => 'ENTR6004',
                'name' => 'Entrepreneurship II',
                'grade' => 'F',
                'created_at' => '2019-04-01 10:35:06',
                'updated_at' => '2019-04-01 10:35:06',
                'deleted_at' => NULL,
            ),
            33 => 
            array (
                'id' => 34,
                'studentid' => '2001567890',
                'code' => '1234',
                'name' => 'xxx',
                'grade' => 'A',
                'created_at' => '2019-04-06 15:48:42',
                'updated_at' => '2019-04-06 15:48:42',
                'deleted_at' => NULL,
            ),
            34 => 
            array (
                'id' => 35,
                'studentid' => '2001345780',
                'code' => '1234',
                'name' => 'xxx',
                'grade' => 'A',
                'created_at' => '2019-04-13 00:13:07',
                'updated_at' => '2019-04-13 00:13:07',
                'deleted_at' => NULL,
            ),
            35 => 
            array (
                'id' => 36,
                'studentid' => '2001123456',
                'code' => '1234',
                'name' => 'xxx',
                'grade' => 'A',
                'created_at' => '2019-04-13 00:17:36',
                'updated_at' => '2019-04-13 00:17:36',
                'deleted_at' => NULL,
            ),
            36 => 
            array (
                'id' => 37,
                'studentid' => '2003345345',
                'code' => '1234',
                'name' => 'xxx',
                'grade' => 'A',
                'created_at' => '2019-04-13 00:27:10',
                'updated_at' => '2019-04-13 00:27:10',
                'deleted_at' => NULL,
            ),
            37 => 
            array (
                'id' => 38,
                'studentid' => '2003890890',
                'code' => '1234',
                'name' => 'xxx',
                'grade' => 'A',
                'created_at' => '2019-04-13 00:42:55',
                'updated_at' => '2019-04-13 00:42:55',
                'deleted_at' => NULL,
            ),
            38 => 
            array (
                'id' => 39,
                'studentid' => '2000789789',
                'code' => '1234',
                'name' => 'xxx',
                'grade' => 'A',
                'created_at' => '2019-04-13 00:47:26',
                'updated_at' => '2019-04-13 00:47:26',
                'deleted_at' => NULL,
            ),
            39 => 
            array (
                'id' => 40,
                'studentid' => '2000134134',
                'code' => '1234',
                'name' => 'xxx',
                'grade' => 'A',
                'created_at' => '2019-04-13 00:50:43',
                'updated_at' => '2019-04-13 00:50:43',
                'deleted_at' => NULL,
            ),
            40 => 
            array (
                'id' => 41,
                'studentid' => '2001898902',
                'code' => '1234',
                'name' => 'xxx',
                'grade' => 'A',
                'created_at' => '2019-04-13 00:54:29',
                'updated_at' => '2019-04-13 00:54:29',
                'deleted_at' => NULL,
            ),
            41 => 
            array (
                'id' => 42,
                'studentid' => '2001098789',
                'code' => '1234',
                'name' => 'xxx',
                'grade' => 'A',
                'created_at' => '2019-04-13 07:58:57',
                'updated_at' => '2019-04-13 07:58:57',
                'deleted_at' => NULL,
            ),
            42 => 
            array (
                'id' => 43,
                'studentid' => '2000111222',
                'code' => '1234',
                'name' => 'xxx',
                'grade' => 'A',
                'created_at' => '2019-04-13 08:02:28',
                'updated_at' => '2019-04-13 08:02:28',
                'deleted_at' => NULL,
            ),
            43 => 
            array (
                'id' => 44,
                'studentid' => '2001544322',
                'code' => 'CHAR6013',
                'name' => 'Character Building: Pancasila',
                'grade' => 'A',
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:00:26',
                'deleted_at' => NULL,
            ),
            44 => 
            array (
                'id' => 45,
                'studentid' => '2001544322',
                'code' => 'COMP6047',
                'name' => 'Algorithm and Programming',
                'grade' => 'B',
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:00:26',
                'deleted_at' => NULL,
            ),
            45 => 
            array (
                'id' => 46,
                'studentid' => '2001544322',
                'code' => 'COMP6060',
                'name' => 'Programming Language Concepts',
                'grade' => 'D',
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:00:26',
                'deleted_at' => NULL,
            ),
            46 => 
            array (
                'id' => 47,
                'studentid' => '2001544322',
                'code' => 'ENGL6128',
                'name' => 'English in Focus',
                'grade' => 'B',
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:00:26',
                'deleted_at' => NULL,
            ),
            47 => 
            array (
                'id' => 48,
                'studentid' => '2001544322',
                'code' => 'MATH6025',
                'name' => 'Discrete Mathematics',
                'grade' => 'E',
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:00:26',
                'deleted_at' => NULL,
            ),
            48 => 
            array (
                'id' => 49,
                'studentid' => '2001544322',
                'code' => 'MATH6031',
                'name' => 'Calculus',
                'grade' => 'D',
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:00:26',
                'deleted_at' => NULL,
            ),
            49 => 
            array (
                'id' => 50,
                'studentid' => '2001544322',
                'code' => 'CHAR6014',
                'name' => 'Character Building: Kewarganegaraan',
                'grade' => 'B',
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:00:26',
                'deleted_at' => NULL,
            ),
            50 => 
            array (
                'id' => 51,
                'studentid' => '2001544322',
                'code' => 'COMP6048',
                'name' => 'Data Structures',
                'grade' => 'B',
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:00:26',
                'deleted_at' => NULL,
            ),
            51 => 
            array (
                'id' => 52,
                'studentid' => '2001544322',
                'code' => 'COMP6056',
                'name' => 'Program Design Methods',
                'grade' => 'B-',
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:00:26',
                'deleted_at' => NULL,
            ),
            52 => 
            array (
                'id' => 53,
                'studentid' => '2001544322',
                'code' => 'COMP6175',
                'name' => 'Object Oriented Programming',
                'grade' => 'B-',
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:00:26',
                'deleted_at' => NULL,
            ),
            53 => 
            array (
                'id' => 54,
                'studentid' => '2001544322',
                'code' => 'ENGL6129',
                'name' => 'English Savvy',
                'grade' => 'B-',
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:00:26',
                'deleted_at' => NULL,
            ),
            54 => 
            array (
                'id' => 55,
                'studentid' => '2001544322',
                'code' => 'LANG6061',
                'name' => 'Indonesian',
                'grade' => 'B+',
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:00:26',
                'deleted_at' => NULL,
            ),
            55 => 
            array (
                'id' => 56,
                'studentid' => '2001544322',
                'code' => 'MATH6030',
                'name' => 'Linear Algebra',
                'grade' => 'C',
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:00:26',
                'deleted_at' => NULL,
            ),
            56 => 
            array (
                'id' => 57,
                'studentid' => '2001544322',
                'code' => 'MATH6025',
                'name' => 'Discrete Mathematics',
                'grade' => 'B',
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:00:26',
                'deleted_at' => NULL,
            ),
            57 => 
            array (
                'id' => 58,
                'studentid' => '2001544322',
                'code' => 'CHAR6015',
                'name' => 'Character Building: Agama',
                'grade' => 'B',
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:00:26',
                'deleted_at' => NULL,
            ),
            58 => 
            array (
                'id' => 59,
                'studentid' => '2001544322',
                'code' => 'COMP6049',
                'name' => 'Algorithm Design and Analysis',
                'grade' => 'D',
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:00:26',
                'deleted_at' => NULL,
            ),
            59 => 
            array (
                'id' => 60,
                'studentid' => '2001544322',
                'code' => 'COMP6065',
                'name' => 'Artificial Intelligence',
                'grade' => 'D',
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:00:26',
                'deleted_at' => NULL,
            ),
            60 => 
            array (
                'id' => 61,
                'studentid' => '2001544322',
                'code' => 'CPEN6098',
                'name' => 'Computer Networks',
                'grade' => 'B+',
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:00:26',
                'deleted_at' => NULL,
            ),
            61 => 
            array (
                'id' => 62,
                'studentid' => '2001544322',
                'code' => 'ENTR6003',
                'name' => 'Entrepreneurship I',
                'grade' => 'B',
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:00:26',
                'deleted_at' => NULL,
            ),
            62 => 
            array (
                'id' => 63,
                'studentid' => '2001544322',
                'code' => 'ISYS6169',
                'name' => 'Database Systems',
                'grade' => 'C',
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:00:26',
                'deleted_at' => NULL,
            ),
            63 => 
            array (
                'id' => 64,
                'studentid' => '2001544322',
                'code' => 'COMP6100',
                'name' => 'Software Engineering',
                'grade' => 'B+',
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:00:26',
                'deleted_at' => NULL,
            ),
            64 => 
            array (
                'id' => 65,
                'studentid' => '2001544322',
                'code' => 'COMP6106',
                'name' => 'Code Reengineering',
                'grade' => 'C',
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:00:26',
                'deleted_at' => NULL,
            ),
            65 => 
            array (
                'id' => 66,
                'studentid' => '2001544322',
                'code' => 'COMP6107',
                'name' => 'Agile Software Development',
                'grade' => 'D',
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:00:26',
                'deleted_at' => NULL,
            ),
            66 => 
            array (
                'id' => 67,
                'studentid' => '2001544322',
                'code' => 'COMP6114',
                'name' => 'Pattern Software Design',
                'grade' => 'D',
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:00:26',
                'deleted_at' => NULL,
            ),
            67 => 
            array (
                'id' => 68,
                'studentid' => '2001544322',
                'code' => 'COMP6176',
                'name' => 'Human and Computer Interaction',
                'grade' => 'B-',
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:00:26',
                'deleted_at' => NULL,
            ),
            68 => 
            array (
                'id' => 69,
                'studentid' => '2001544322',
                'code' => 'COMP7084',
                'name' => 'Multimedia Systems',
                'grade' => 'B-',
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:00:26',
                'deleted_at' => NULL,
            ),
            69 => 
            array (
                'id' => 70,
                'studentid' => '2001544322',
                'code' => 'STAT6021',
                'name' => 'Research Methodology',
                'grade' => 'D',
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:00:26',
                'deleted_at' => NULL,
            ),
            70 => 
            array (
                'id' => 71,
                'studentid' => '2001544322',
                'code' => 'COMP6062',
                'name' => 'Compilation Techniques',
                'grade' => 'B',
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:00:26',
                'deleted_at' => NULL,
            ),
            71 => 
            array (
                'id' => 72,
                'studentid' => '2001544322',
                'code' => 'COMP6099',
                'name' => 'Advanced Object Oriented Programming',
                'grade' => 'D',
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:00:26',
                'deleted_at' => NULL,
            ),
            72 => 
            array (
                'id' => 73,
                'studentid' => '2001544322',
                'code' => 'COMP6115',
                'name' => 'Object Oriented Analysis & Design',
                'grade' => 'D',
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:00:26',
                'deleted_at' => NULL,
            ),
            73 => 
            array (
                'id' => 74,
                'studentid' => '2001544322',
                'code' => 'COMP6122',
                'name' => 'Framework Layer Architecture',
                'grade' => 'D',
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:00:26',
                'deleted_at' => NULL,
            ),
            74 => 
            array (
                'id' => 75,
                'studentid' => '2001544322',
                'code' => 'COMP6144',
                'name' => 'Web Programming',
                'grade' => 'A',
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:00:26',
                'deleted_at' => NULL,
            ),
            75 => 
            array (
                'id' => 76,
                'studentid' => '2001544322',
                'code' => 'COMP6153',
                'name' => 'Operating System',
                'grade' => 'D',
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:00:26',
                'deleted_at' => NULL,
            ),
            76 => 
            array (
                'id' => 77,
                'studentid' => '2001544322',
                'code' => 'ENTR6004',
                'name' => 'Entrepreneurship II',
                'grade' => 'B',
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:00:26',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}