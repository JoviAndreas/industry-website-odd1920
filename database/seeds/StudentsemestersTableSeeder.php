<?php

use Illuminate\Database\Seeder;

class StudentsemestersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('studentsemesters')->delete();
        
        \DB::table('studentsemesters')->insert(array (
            0 => 
            array (
                'id' => 1,
                'studentid' => '2001234567',
                'semester' => 1,
                'ips' => 3.2,
                'ipk' => 3.2,
                'created_at' => '2019-04-01 10:35:06',
                'updated_at' => '2019-04-01 10:35:06',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'studentid' => '2001234567',
                'semester' => 2,
                'ips' => 3.4,
                'ipk' => 3.3,
                'created_at' => '2019-04-01 10:35:06',
                'updated_at' => '2019-04-01 10:35:06',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'studentid' => '2001234567',
                'semester' => 3,
                'ips' => 3.7,
                'ipk' => 3.5,
                'created_at' => '2019-04-01 10:35:06',
                'updated_at' => '2019-04-01 10:35:06',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'studentid' => '2001234567',
                'semester' => 4,
                'ips' => 3.6,
                'ipk' => 3.55,
                'created_at' => '2019-04-01 10:35:06',
                'updated_at' => '2019-04-01 10:35:06',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'studentid' => '2001234567',
                'semester' => 5,
                'ips' => 3.67,
                'ipk' => 3.6,
                'created_at' => '2019-04-01 10:35:06',
                'updated_at' => '2019-04-01 10:35:06',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'studentid' => '2001234567',
                'semester' => 6,
                'ips' => 3.33,
                'ipk' => 3.57,
                'created_at' => '2019-04-01 10:35:06',
                'updated_at' => '2019-04-01 10:35:06',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'studentid' => '2001567890',
                'semester' => 1,
                'ips' => 3.0,
                'ipk' => 3.0,
                'created_at' => '2019-04-06 14:47:06',
                'updated_at' => '2019-04-06 14:47:25',
                'deleted_at' => '2019-04-06 14:47:25',
            ),
            7 => 
            array (
                'id' => 8,
                'studentid' => '2001567890',
                'semester' => 2,
                'ips' => 3.0,
                'ipk' => 3.0,
                'created_at' => '2019-04-06 14:47:06',
                'updated_at' => '2019-04-06 14:47:25',
                'deleted_at' => '2019-04-06 14:47:25',
            ),
            8 => 
            array (
                'id' => 9,
                'studentid' => '2001567890',
                'semester' => 3,
                'ips' => 3.0,
                'ipk' => 3.0,
                'created_at' => '2019-04-06 14:47:06',
                'updated_at' => '2019-04-06 14:47:25',
                'deleted_at' => '2019-04-06 14:47:25',
            ),
            9 => 
            array (
                'id' => 10,
                'studentid' => '2001567890',
                'semester' => 4,
                'ips' => 3.0,
                'ipk' => 3.0,
                'created_at' => '2019-04-06 14:47:06',
                'updated_at' => '2019-04-06 14:47:25',
                'deleted_at' => '2019-04-06 14:47:25',
            ),
            10 => 
            array (
                'id' => 11,
                'studentid' => '2001567890',
                'semester' => 5,
                'ips' => 3.0,
                'ipk' => 3.0,
                'created_at' => '2019-04-06 14:47:06',
                'updated_at' => '2019-04-06 14:47:25',
                'deleted_at' => '2019-04-06 14:47:25',
            ),
            11 => 
            array (
                'id' => 12,
                'studentid' => '2001567890',
                'semester' => 1,
                'ips' => 3.0,
                'ipk' => 3.0,
                'created_at' => '2019-04-06 14:47:25',
                'updated_at' => '2019-04-06 14:48:11',
                'deleted_at' => '2019-04-06 14:48:11',
            ),
            12 => 
            array (
                'id' => 13,
                'studentid' => '2001567890',
                'semester' => 2,
                'ips' => 3.0,
                'ipk' => 3.0,
                'created_at' => '2019-04-06 14:47:25',
                'updated_at' => '2019-04-06 14:48:11',
                'deleted_at' => '2019-04-06 14:48:11',
            ),
            13 => 
            array (
                'id' => 14,
                'studentid' => '2001567890',
                'semester' => 3,
                'ips' => 3.0,
                'ipk' => 3.0,
                'created_at' => '2019-04-06 14:47:25',
                'updated_at' => '2019-04-06 14:48:11',
                'deleted_at' => '2019-04-06 14:48:11',
            ),
            14 => 
            array (
                'id' => 15,
                'studentid' => '2001567890',
                'semester' => 4,
                'ips' => 3.0,
                'ipk' => 3.0,
                'created_at' => '2019-04-06 14:47:25',
                'updated_at' => '2019-04-06 14:48:11',
                'deleted_at' => '2019-04-06 14:48:11',
            ),
            15 => 
            array (
                'id' => 16,
                'studentid' => '2001567890',
                'semester' => 5,
                'ips' => 3.0,
                'ipk' => 3.0,
                'created_at' => '2019-04-06 14:47:25',
                'updated_at' => '2019-04-06 14:48:11',
                'deleted_at' => '2019-04-06 14:48:11',
            ),
            16 => 
            array (
                'id' => 17,
                'studentid' => '2001567890',
                'semester' => 1,
                'ips' => 3.0,
                'ipk' => 3.0,
                'created_at' => '2019-04-06 14:48:11',
                'updated_at' => '2019-04-06 15:48:42',
                'deleted_at' => '2019-04-06 15:48:42',
            ),
            17 => 
            array (
                'id' => 18,
                'studentid' => '2001567890',
                'semester' => 2,
                'ips' => 3.0,
                'ipk' => 3.0,
                'created_at' => '2019-04-06 14:48:11',
                'updated_at' => '2019-04-06 15:48:42',
                'deleted_at' => '2019-04-06 15:48:42',
            ),
            18 => 
            array (
                'id' => 19,
                'studentid' => '2001567890',
                'semester' => 3,
                'ips' => 3.0,
                'ipk' => 3.0,
                'created_at' => '2019-04-06 14:48:11',
                'updated_at' => '2019-04-06 15:48:42',
                'deleted_at' => '2019-04-06 15:48:42',
            ),
            19 => 
            array (
                'id' => 20,
                'studentid' => '2001567890',
                'semester' => 4,
                'ips' => 3.0,
                'ipk' => 3.0,
                'created_at' => '2019-04-06 14:48:11',
                'updated_at' => '2019-04-06 15:48:42',
                'deleted_at' => '2019-04-06 15:48:42',
            ),
            20 => 
            array (
                'id' => 21,
                'studentid' => '2001567890',
                'semester' => 5,
                'ips' => 3.0,
                'ipk' => 3.0,
                'created_at' => '2019-04-06 14:48:11',
                'updated_at' => '2019-04-06 15:48:42',
                'deleted_at' => '2019-04-06 15:48:42',
            ),
            21 => 
            array (
                'id' => 22,
                'studentid' => '2001567890',
                'semester' => 1,
                'ips' => 3.0,
                'ipk' => 3.0,
                'created_at' => '2019-04-06 15:48:42',
                'updated_at' => '2019-04-06 15:49:29',
                'deleted_at' => '2019-04-06 15:49:29',
            ),
            22 => 
            array (
                'id' => 23,
                'studentid' => '2001567890',
                'semester' => 2,
                'ips' => 3.0,
                'ipk' => 3.0,
                'created_at' => '2019-04-06 15:48:42',
                'updated_at' => '2019-04-06 15:49:29',
                'deleted_at' => '2019-04-06 15:49:29',
            ),
            23 => 
            array (
                'id' => 24,
                'studentid' => '2001567890',
                'semester' => 3,
                'ips' => 3.0,
                'ipk' => 3.0,
                'created_at' => '2019-04-06 15:48:42',
                'updated_at' => '2019-04-06 15:49:29',
                'deleted_at' => '2019-04-06 15:49:29',
            ),
            24 => 
            array (
                'id' => 25,
                'studentid' => '2001567890',
                'semester' => 4,
                'ips' => 3.0,
                'ipk' => 3.0,
                'created_at' => '2019-04-06 15:48:42',
                'updated_at' => '2019-04-06 15:49:29',
                'deleted_at' => '2019-04-06 15:49:29',
            ),
            25 => 
            array (
                'id' => 26,
                'studentid' => '2001567890',
                'semester' => 5,
                'ips' => 3.0,
                'ipk' => 3.0,
                'created_at' => '2019-04-06 15:48:42',
                'updated_at' => '2019-04-06 15:49:29',
                'deleted_at' => '2019-04-06 15:49:29',
            ),
            26 => 
            array (
                'id' => 27,
                'studentid' => '2001567890',
                'semester' => 1,
                'ips' => 3.0,
                'ipk' => 3.0,
                'created_at' => '2019-04-06 15:49:29',
                'updated_at' => '2019-04-06 15:49:29',
                'deleted_at' => NULL,
            ),
            27 => 
            array (
                'id' => 28,
                'studentid' => '2001567890',
                'semester' => 2,
                'ips' => 3.0,
                'ipk' => 3.0,
                'created_at' => '2019-04-06 15:49:29',
                'updated_at' => '2019-04-06 15:49:29',
                'deleted_at' => NULL,
            ),
            28 => 
            array (
                'id' => 29,
                'studentid' => '2001567890',
                'semester' => 3,
                'ips' => 3.0,
                'ipk' => 3.0,
                'created_at' => '2019-04-06 15:49:29',
                'updated_at' => '2019-04-06 15:49:29',
                'deleted_at' => NULL,
            ),
            29 => 
            array (
                'id' => 30,
                'studentid' => '2001567890',
                'semester' => 4,
                'ips' => 3.0,
                'ipk' => 3.0,
                'created_at' => '2019-04-06 15:49:29',
                'updated_at' => '2019-04-06 15:49:29',
                'deleted_at' => NULL,
            ),
            30 => 
            array (
                'id' => 31,
                'studentid' => '2001567890',
                'semester' => 5,
                'ips' => 3.0,
                'ipk' => 3.0,
                'created_at' => '2019-04-06 15:49:29',
                'updated_at' => '2019-04-06 15:49:29',
                'deleted_at' => NULL,
            ),
            31 => 
            array (
                'id' => 32,
                'studentid' => '2001345780',
                'semester' => 1,
                'ips' => 3.0,
                'ipk' => 3.0,
                'created_at' => '2019-04-13 00:13:07',
                'updated_at' => '2019-04-13 00:13:51',
                'deleted_at' => '2019-04-13 00:13:51',
            ),
            32 => 
            array (
                'id' => 33,
                'studentid' => '2001345780',
                'semester' => 1,
                'ips' => 3.0,
                'ipk' => 3.0,
                'created_at' => '2019-04-13 00:13:51',
                'updated_at' => '2019-04-13 00:13:51',
                'deleted_at' => NULL,
            ),
            33 => 
            array (
                'id' => 34,
                'studentid' => '2001123456',
                'semester' => 1,
                'ips' => 2.9,
                'ipk' => 2.9,
                'created_at' => '2019-04-13 00:17:36',
                'updated_at' => '2019-04-13 00:17:36',
                'deleted_at' => NULL,
            ),
            34 => 
            array (
                'id' => 35,
                'studentid' => '2003345345',
                'semester' => 1,
                'ips' => 3.5,
                'ipk' => 3.5,
                'created_at' => '2019-04-13 00:27:10',
                'updated_at' => '2019-04-13 00:27:10',
                'deleted_at' => NULL,
            ),
            35 => 
            array (
                'id' => 36,
                'studentid' => '2003890890',
                'semester' => 1,
                'ips' => 3.01,
                'ipk' => 3.01,
                'created_at' => '2019-04-13 00:42:55',
                'updated_at' => '2019-04-13 00:42:55',
                'deleted_at' => NULL,
            ),
            36 => 
            array (
                'id' => 37,
                'studentid' => '2000789789',
                'semester' => 1,
                'ips' => 3.24,
                'ipk' => 3.24,
                'created_at' => '2019-04-13 00:47:26',
                'updated_at' => '2019-04-13 00:47:26',
                'deleted_at' => NULL,
            ),
            37 => 
            array (
                'id' => 38,
                'studentid' => '2000134134',
                'semester' => 1,
                'ips' => 3.89,
                'ipk' => 3.89,
                'created_at' => '2019-04-13 00:50:43',
                'updated_at' => '2019-04-13 00:50:43',
                'deleted_at' => NULL,
            ),
            38 => 
            array (
                'id' => 39,
                'studentid' => '2001898902',
                'semester' => 1,
                'ips' => 3.76,
                'ipk' => 3.76,
                'created_at' => '2019-04-13 00:54:29',
                'updated_at' => '2019-04-13 00:54:29',
                'deleted_at' => NULL,
            ),
            39 => 
            array (
                'id' => 40,
                'studentid' => '2001098789',
                'semester' => 1,
                'ips' => 3.2,
                'ipk' => 3.2,
                'created_at' => '2019-04-13 07:54:12',
                'updated_at' => '2019-04-13 07:58:57',
                'deleted_at' => '2019-04-13 07:58:57',
            ),
            40 => 
            array (
                'id' => 41,
                'studentid' => '2001098789',
                'semester' => 1,
                'ips' => 3.2,
                'ipk' => 3.2,
                'created_at' => '2019-04-13 07:58:57',
                'updated_at' => '2019-04-13 07:58:57',
                'deleted_at' => NULL,
            ),
            41 => 
            array (
                'id' => 42,
                'studentid' => '2000111222',
                'semester' => 1,
                'ips' => 3.1,
                'ipk' => 3.1,
                'created_at' => '2019-04-13 08:02:28',
                'updated_at' => '2019-04-13 08:02:28',
                'deleted_at' => NULL,
            ),
            42 => 
            array (
                'id' => 43,
                'studentid' => '2001544322',
                'semester' => 1,
                'ips' => 2.2,
                'ipk' => 2.38,
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:02:26',
                'deleted_at' => '2019-05-02 10:02:26',
            ),
            43 => 
            array (
                'id' => 44,
                'studentid' => '2001544322',
                'semester' => 2,
                'ips' => 2.68,
                'ipk' => 2.55,
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:02:26',
                'deleted_at' => '2019-05-02 10:02:26',
            ),
            44 => 
            array (
                'id' => 45,
                'studentid' => '2001544322',
                'semester' => 3,
                'ips' => 2.06,
                'ipk' => 2.41,
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:02:26',
                'deleted_at' => '2019-05-02 10:02:26',
            ),
            45 => 
            array (
                'id' => 46,
                'studentid' => '2001544322',
                'semester' => 4,
                'ips' => 2.04,
                'ipk' => 2.31,
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:02:26',
                'deleted_at' => '2019-05-02 10:02:26',
            ),
            46 => 
            array (
                'id' => 47,
                'studentid' => '2001544322',
                'semester' => 5,
                'ips' => 1.91,
                'ipk' => 2.22,
                'created_at' => '2019-05-02 10:00:26',
                'updated_at' => '2019-05-02 10:02:26',
                'deleted_at' => '2019-05-02 10:02:26',
            ),
            47 => 
            array (
                'id' => 48,
                'studentid' => '2001544322',
                'semester' => 1,
                'ips' => 2.2,
                'ipk' => 2.38,
                'created_at' => '2019-05-02 10:02:26',
                'updated_at' => '2019-05-02 19:06:33',
                'deleted_at' => '2019-05-02 19:06:33',
            ),
            48 => 
            array (
                'id' => 49,
                'studentid' => '2001544322',
                'semester' => 2,
                'ips' => 2.68,
                'ipk' => 2.55,
                'created_at' => '2019-05-02 10:02:26',
                'updated_at' => '2019-05-02 19:06:33',
                'deleted_at' => '2019-05-02 19:06:33',
            ),
            49 => 
            array (
                'id' => 50,
                'studentid' => '2001544322',
                'semester' => 3,
                'ips' => 2.06,
                'ipk' => 2.41,
                'created_at' => '2019-05-02 10:02:26',
                'updated_at' => '2019-05-02 19:06:33',
                'deleted_at' => '2019-05-02 19:06:33',
            ),
            50 => 
            array (
                'id' => 51,
                'studentid' => '2001544322',
                'semester' => 4,
                'ips' => 2.04,
                'ipk' => 2.31,
                'created_at' => '2019-05-02 10:02:26',
                'updated_at' => '2019-05-02 19:06:33',
                'deleted_at' => '2019-05-02 19:06:33',
            ),
            51 => 
            array (
                'id' => 52,
                'studentid' => '2001544322',
                'semester' => 5,
                'ips' => 1.91,
                'ipk' => 2.22,
                'created_at' => '2019-05-02 10:02:26',
                'updated_at' => '2019-05-02 19:06:33',
                'deleted_at' => '2019-05-02 19:06:33',
            ),
            52 => 
            array (
                'id' => 53,
                'studentid' => '2001544322',
                'semester' => 1,
                'ips' => 2.2,
                'ipk' => 2.38,
                'created_at' => '2019-05-02 19:06:33',
                'updated_at' => '2019-05-02 19:06:33',
                'deleted_at' => NULL,
            ),
            53 => 
            array (
                'id' => 54,
                'studentid' => '2001544322',
                'semester' => 2,
                'ips' => 2.68,
                'ipk' => 2.55,
                'created_at' => '2019-05-02 19:06:33',
                'updated_at' => '2019-05-02 19:06:33',
                'deleted_at' => NULL,
            ),
            54 => 
            array (
                'id' => 55,
                'studentid' => '2001544322',
                'semester' => 3,
                'ips' => 2.06,
                'ipk' => 2.41,
                'created_at' => '2019-05-02 19:06:33',
                'updated_at' => '2019-05-02 19:06:33',
                'deleted_at' => NULL,
            ),
            55 => 
            array (
                'id' => 56,
                'studentid' => '2001544322',
                'semester' => 4,
                'ips' => 2.04,
                'ipk' => 2.31,
                'created_at' => '2019-05-02 19:06:33',
                'updated_at' => '2019-05-02 19:06:33',
                'deleted_at' => NULL,
            ),
            56 => 
            array (
                'id' => 57,
                'studentid' => '2001544322',
                'semester' => 5,
                'ips' => 1.91,
                'ipk' => 2.22,
                'created_at' => '2019-05-02 19:06:33',
                'updated_at' => '2019-05-02 19:06:33',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}