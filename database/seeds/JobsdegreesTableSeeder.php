<?php

use Illuminate\Database\Seeder;

class JobsdegreesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('jobsdegrees')->delete();
        
        \DB::table('jobsdegrees')->insert(array (
            0 => 
            array (
                'jobid' => 1,
                'degreeid' => 14,
                'approval_status' => 'pending',
                'note' => NULL,
                'created_at' => '2019-06-20 10:43:41',
                'updated_at' => '2019-06-21 01:03:44',
                'deleted_at' => '2019-06-21 01:03:44',
            ),
            1 => 
            array (
                'jobid' => 2,
                'degreeid' => 1,
                'approval_status' => 'pending',
                'note' => NULL,
                'created_at' => '2019-04-01 11:13:44',
                'updated_at' => '2019-04-01 11:13:44',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'jobid' => 3,
                'degreeid' => 4,
                'approval_status' => 'pending',
                'note' => NULL,
                'created_at' => '2019-04-06 16:04:54',
                'updated_at' => '2019-04-06 16:04:54',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'jobid' => 4,
                'degreeid' => 4,
                'approval_status' => 'pending',
                'note' => NULL,
                'created_at' => '2019-04-06 16:04:58',
                'updated_at' => '2019-04-06 16:04:58',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'jobid' => 5,
                'degreeid' => 1,
                'approval_status' => 'pending',
                'note' => NULL,
                'created_at' => '2019-04-08 13:08:08',
                'updated_at' => '2019-04-08 13:08:08',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'jobid' => 5,
                'degreeid' => 4,
                'approval_status' => 'pending',
                'note' => NULL,
                'created_at' => '2019-04-08 13:08:08',
                'updated_at' => '2019-04-08 13:08:08',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'jobid' => 6,
                'degreeid' => 1,
                'approval_status' => 'pending',
                'note' => NULL,
                'created_at' => '2019-04-11 23:48:12',
                'updated_at' => '2019-04-11 23:48:12',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'jobid' => 6,
                'degreeid' => 4,
                'approval_status' => 'pending',
                'note' => NULL,
                'created_at' => '2019-04-11 23:48:12',
                'updated_at' => '2019-04-11 23:48:12',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'jobid' => 7,
                'degreeid' => 4,
                'approval_status' => 'pending',
                'note' => NULL,
                'created_at' => '2019-07-29 11:48:02',
                'updated_at' => '2019-07-29 11:48:02',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'jobid' => 7,
                'degreeid' => 13,
                'approval_status' => 'pending',
                'note' => NULL,
                'created_at' => '2019-07-29 11:48:02',
                'updated_at' => '2019-07-29 11:48:02',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'jobid' => 8,
                'degreeid' => 9,
                'approval_status' => 'pending',
                'note' => NULL,
                'created_at' => '2019-04-13 00:56:16',
                'updated_at' => '2019-04-13 00:56:16',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'jobid' => 9,
                'degreeid' => 12,
                'approval_status' => 'pending',
                'note' => NULL,
                'created_at' => '2019-04-13 00:57:15',
                'updated_at' => '2019-04-13 00:57:15',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'jobid' => 10,
                'degreeid' => 1,
                'approval_status' => 'pending',
                'note' => NULL,
                'created_at' => '2019-04-13 00:58:46',
                'updated_at' => '2019-04-13 00:58:46',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'jobid' => 10,
                'degreeid' => 14,
                'approval_status' => 'pending',
                'note' => NULL,
                'created_at' => '2019-04-13 00:58:46',
                'updated_at' => '2019-04-13 00:58:46',
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'jobid' => 11,
                'degreeid' => 15,
                'approval_status' => 'pending',
                'note' => NULL,
                'created_at' => '2019-04-13 14:11:55',
                'updated_at' => '2019-04-13 14:11:55',
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'jobid' => 12,
                'degreeid' => 4,
                'approval_status' => 'pending',
                'note' => NULL,
                'created_at' => '2019-04-24 10:41:47',
                'updated_at' => '2019-04-24 10:41:47',
                'deleted_at' => NULL,
            ),
            16 => 
            array (
                'jobid' => 12,
                'degreeid' => 13,
                'approval_status' => 'pending',
                'note' => NULL,
                'created_at' => '2019-04-24 10:41:47',
                'updated_at' => '2019-04-24 10:41:47',
                'deleted_at' => NULL,
            ),
            17 => 
            array (
                'jobid' => 13,
                'degreeid' => 1,
                'approval_status' => 'pending',
                'note' => NULL,
                'created_at' => '2019-05-09 23:50:46',
                'updated_at' => '2019-05-09 23:50:46',
                'deleted_at' => NULL,
            ),
            18 => 
            array (
                'jobid' => 13,
                'degreeid' => 4,
                'approval_status' => 'pending',
                'note' => NULL,
                'created_at' => '2019-05-09 23:50:46',
                'updated_at' => '2019-05-09 23:50:46',
                'deleted_at' => NULL,
            ),
            19 => 
            array (
                'jobid' => 13,
                'degreeid' => 14,
                'approval_status' => 'pending',
                'note' => NULL,
                'created_at' => '2019-05-09 23:50:46',
                'updated_at' => '2019-05-09 23:50:46',
                'deleted_at' => NULL,
            ),
            20 => 
            array (
                'jobid' => 15,
                'degreeid' => 1,
                'approval_status' => 'pending',
                'note' => NULL,
                'created_at' => '2019-05-13 13:10:14',
                'updated_at' => '2019-05-13 13:10:14',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}