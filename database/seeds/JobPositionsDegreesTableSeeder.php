<?php

use Illuminate\Database\Seeder;

class JobPositionsDegreesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('job_positions_degrees')->delete();
        
        \DB::table('job_positions_degrees')->insert(array (
            0 => 
            array (
                'id' => 2,
                'job_position_id' => 2,
                'degree_id' => 4,
                'created_at' => '2019-05-13 12:29:43',
                'updated_at' => '2019-05-13 12:29:43',
            ),
            1 => 
            array (
                'id' => 3,
                'job_position_id' => 3,
                'degree_id' => 9,
                'created_at' => '2019-05-13 12:29:51',
                'updated_at' => '2019-05-13 12:29:51',
            ),
            2 => 
            array (
                'id' => 4,
                'job_position_id' => 4,
                'degree_id' => 12,
                'created_at' => '2019-05-13 12:29:56',
                'updated_at' => '2019-05-13 12:29:56',
            ),
            3 => 
            array (
                'id' => 5,
                'job_position_id' => 5,
                'degree_id' => 1,
                'created_at' => '2019-05-13 12:30:05',
                'updated_at' => '2019-05-13 12:30:05',
            ),
            4 => 
            array (
                'id' => 6,
                'job_position_id' => 5,
                'degree_id' => 4,
                'created_at' => '2019-05-13 14:53:41',
                'updated_at' => '2019-05-13 12:30:10',
            ),
            5 => 
            array (
                'id' => 7,
                'job_position_id' => 7,
                'degree_id' => 4,
                'created_at' => '2019-05-13 12:30:16',
                'updated_at' => '2019-05-13 12:30:16',
            ),
            6 => 
            array (
                'id' => 8,
                'job_position_id' => 8,
                'degree_id' => 9,
                'created_at' => '2019-05-13 12:30:23',
                'updated_at' => '2019-05-13 12:30:23',
            ),
            7 => 
            array (
                'id' => 9,
                'job_position_id' => 9,
                'degree_id' => 15,
                'created_at' => '2019-05-13 12:30:30',
                'updated_at' => '2019-05-13 12:30:30',
            ),
            8 => 
            array (
                'id' => 10,
                'job_position_id' => 10,
                'degree_id' => 14,
                'created_at' => '2019-05-13 12:30:35',
                'updated_at' => '2019-05-13 12:30:35',
            ),
            9 => 
            array (
                'id' => 11,
                'job_position_id' => 11,
                'degree_id' => 15,
                'created_at' => '2019-05-13 12:30:41',
                'updated_at' => '2019-05-13 12:30:41',
            ),
            10 => 
            array (
                'id' => 12,
                'job_position_id' => 12,
                'degree_id' => 13,
                'created_at' => '2019-05-13 12:30:45',
                'updated_at' => '2019-05-13 12:30:45',
            ),
            11 => 
            array (
                'id' => 19,
                'job_position_id' => 1,
                'degree_id' => 1,
                'created_at' => '2019-05-13 15:52:04',
                'updated_at' => '2019-05-13 15:52:04',
            ),
        ));
        
        
    }
}