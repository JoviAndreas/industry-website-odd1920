<?php

use Illuminate\Database\Seeder;

class InterviewsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('interviews')->delete();
        
        \DB::table('interviews')->insert(array (
            0 => 
            array (
                'id' => 1,
                'recruitment_id' => 2,
                'interviewer' => 'blibli',
                'interviewee' => '2001234567',
                'location' => 'Kuningan',
                'date' => '2019-04-05',
                'time' => '09:00',
                'token' => 'S2yS10SKPiR6QJ4WKvzuYAo3vpEEOIzKP6RsUIDjKV4wdnoeh0YhG2zfVXFm',
                'status' => 'accept',
                'pic' => 'Yudi',
                'phone' => '08575552123',
                'created_at' => '2019-04-01 11:17:21',
                'updated_at' => '2019-04-01 11:19:22',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'recruitment_id' => 4,
                'interviewer' => 'xx',
                'interviewee' => '2001567890',
                'location' => 'PT. X',
                'date' => '2019-04-19',
                'time' => '08:00',
                'token' => 'S2yS10SFHKh1CprP8aqzA47jalKuOOpMXJHHNj0byJpaylzMIdF6WrtCrjty',
                'status' => 'accept',
                'pic' => 'Susi',
                'phone' => '12345678',
                'created_at' => '2019-04-06 16:30:35',
                'updated_at' => '2019-04-06 16:37:32',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'recruitment_id' => 5,
                'interviewer' => 'abcabc',
                'interviewee' => '2000134134',
                'location' => 'Jakarta',
                'date' => '2019-04-23',
                'time' => '13:00',
                'token' => 'S2yS10SzUr2UX1GXIK8OaELhUYExHHWtxiZB3rudaqWk8iBBgs8ZMHXJQd96',
                'status' => 'waiting',
                'pic' => 'Susi',
                'phone' => '081317888503',
                'created_at' => '2019-04-13 14:17:01',
                'updated_at' => '2019-04-13 14:17:01',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'recruitment_id' => 10,
                'interviewer' => 'abcabc',
                'interviewee' => '2001898902',
                'location' => 'jakarta',
                'date' => '2019-04-23',
                'time' => '08:00',
                'token' => 'S2yS10SosVkC3CXTz2kcFYq9HCEWuocTQiCg24A4CjOnKgkxWlxOPCTV1R6u',
                'status' => 'waiting',
                'pic' => 'Susi',
                'phone' => '081317888503',
                'created_at' => '2019-04-13 14:17:31',
                'updated_at' => '2019-04-13 14:17:31',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'recruitment_id' => 6,
                'interviewer' => 'abcabc',
                'interviewee' => '2000111222',
                'location' => 'Crossroad',
                'date' => '2019-04-26',
                'time' => '11:00',
                'token' => 'S2yS10SX2OVLbZUhuHze9ZoOG3sceLYIVSWoolrJFuz9MyJuJP1izA5KuNjG',
                'status' => 'reschedule',
                'pic' => 'Susi',
                'phone' => '081317888503',
                'created_at' => '2019-04-24 10:49:14',
                'updated_at' => '2019-04-24 10:57:50',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'recruitment_id' => 6,
                'interviewer' => 'abcabc',
                'interviewee' => '2000111222',
                'location' => 'Crossroad',
                'date' => '2019-04-29',
                'time' => '11:00',
                'token' => 'S2yS10SDlIK7XvUecfezMUXgvs0XuEeWX9xRD7Aozha72TXK0BUAMgXz3P8a',
                'status' => 'accept',
                'pic' => 'Susi',
                'phone' => '081317888503',
                'created_at' => '2019-04-24 11:00:59',
                'updated_at' => '2019-04-24 11:03:56',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}