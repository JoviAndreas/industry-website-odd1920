<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInterviewsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('interviews', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('recruitment_id')->nullable();
			$table->string('interviewer', 100)->index('interviewer');
			$table->string('interviewee', 100)->index('interviewee');
			$table->string('location', 150);
			$table->date('date');
			$table->string('time', 5);
			$table->string('token', 60);
			$table->string('status', 15);
			$table->string('pic', 100);
			$table->string('phone', 12);
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('interviews');
	}

}
