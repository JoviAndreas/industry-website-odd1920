<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInstitutionToStudentsAndStudentsTempTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('students', function(Blueprint $table)
        {
            $table->string('institution')->nullable();
            $table->string('acad_career')->nullable();
        });
        Schema::table('students_temp', function(Blueprint $table)
        {
            $table->string('institution')->nullable();
            $table->string('acad_career')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('students', function(Blueprint $table)
        {
            $table->dropColumn('institution');
            $table->dropColumn('acad_career');
        });
        Schema::table('students_temp', function(Blueprint $table)
        {
            $table->dropColumn('institution');
            $table->dropColumn('acad_career');
        });
    }
}
