<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStudentscoresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('studentscores', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('studentid', 100)->index('studentid');
			$table->string('code', 100);
			$table->string('name', 100);
			$table->string('grade', 5);
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('studentscores');
	}

}
