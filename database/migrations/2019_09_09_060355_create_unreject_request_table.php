<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUnrejectRequestTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('unreject_request', function(Blueprint $table)
		{
			$table->integer('request_id', true);
			$table->integer('recruitment_id');
			$table->text('reason', 16777215);
			$table->string('status', 10);
			$table->string('approved_by', 100)->nullable();
			$table->dateTime('approved_on')->nullable();
			$table->string('rejected_by', 100)->nullable();
			$table->dateTime('rejected_on')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('unreject_request');
	}

}
