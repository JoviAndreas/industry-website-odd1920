<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLearningPlanTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('learning_plan', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('students_done_id');
			$table->string('learning_competencies', 100)->nullable();
			$table->string('soft_skills', 100)->nullable();
			$table->string('approved_by_univ', 10)->nullable()->default('no');
			$table->string('company_name', 200)->nullable();
			$table->string('field_spv_name', 200)->nullable();
			$table->string('field_spv_position', 100)->nullable();
			$table->string('field_spv_email', 100)->nullable();
			$table->string('field_spv_phone', 100)->nullable();
			$table->string('field_spv_address', 150)->nullable();
			$table->date('start_date')->nullable();
			$table->date('end_date')->nullable();
			$table->string('working_hours', 100)->nullable();
			$table->string('job_description', 500)->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('learning_plan');
	}

}
