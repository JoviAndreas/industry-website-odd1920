<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTempTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students_temp', function(Blueprint $table) {
            $table->string('period');
            $table->string('studentid');
            $table->string('email');
            $table->string('name');
            $table->string('school');
            $table->string('program');
            $table->string('major');
            $table->string('campus');
            $table->string('track');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('students_temp');
    }
}
