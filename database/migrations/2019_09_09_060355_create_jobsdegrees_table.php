<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateJobsdegreesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('jobsdegrees', function(Blueprint $table)
		{
			$table->integer('jobid');
			$table->integer('degreeid');
			$table->string('approval_status', 20)->default('pending');
			$table->string('note', 200)->nullable();
			$table->timestamps();
			$table->softDeletes();
			$table->primary(['jobid','degreeid']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('jobsdegrees');
	}

}
