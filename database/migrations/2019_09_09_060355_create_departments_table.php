<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDepartmentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('departments', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('departmentid', 100)->index('departmentid');
			$table->string('name', 200);
			$table->string('email', 150)->nullable();
			$table->string('photo', 100);
			$table->string('head_of_program', 100)->nullable();
			$table->string('campus', 100)->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('departments');
	}

}
