<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSupervisorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('supervisors', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('companyid', 100)->index('companyid');
			$table->string('name', 100);
			$table->string('position', 100);
			$table->string('email', 100);
			$table->string('phone', 100);
			$table->string('address', 150);
			$table->string('token', 60);
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('supervisors');
	}

}
