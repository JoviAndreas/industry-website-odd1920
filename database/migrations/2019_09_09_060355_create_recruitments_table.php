<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRecruitmentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('recruitments', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('jobid')->index('jobid');
			$table->string('studentid', 100)->index('studentid');
			$table->string('status', 20);
			$table->string('token', 60);
			$table->string('applied_by', 100)->default('student');
			$table->string('accepted_by', 100)->nullable();
			$table->dateTime('accepted_on')->nullable();
			$table->string('processed_by', 100)->nullable();
			$table->dateTime('processed_on')->nullable();
			$table->string('unapplied_by', 100)->nullable();
			$table->dateTime('unapplied_on')->nullable();
			$table->string('rejected_by', 100)->nullable();
			$table->dateTime('rejected_on')->nullable();
			$table->string('approved_by', 100)->nullable();
			$table->dateTime('approved_on')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('recruitments');
	}

}
