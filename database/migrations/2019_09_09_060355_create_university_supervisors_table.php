<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUniversitySupervisorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('university_supervisors', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('lecturerid', 100)->nullable();
			$table->string('name', 100);
			$table->string('email', 100)->nullable();
			$table->string('phone', 100)->nullable();
			$table->string('line_id', 100)->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('university_supervisors');
	}

}
