<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMailboxesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mailboxes', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('interview_id')->nullable();
			$table->string('senderid', 100)->index('froms');
			$table->string('recipientid', 100)->index('recipientid');
			$table->string('subject', 150);
			$table->dateTime('date');
			$table->text('message', 65535);
			$table->string('status', 10);
			$table->string('type', 100);
			$table->string('token', 60);
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mailboxes');
	}

}
