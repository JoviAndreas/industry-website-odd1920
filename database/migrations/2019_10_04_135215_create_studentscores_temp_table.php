<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentscoresTempTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('studentscores_temp', function(Blueprint $table)
        {
            $table->string('studentid', 100);
            $table->string('code', 100);
            $table->string('name', 100);
            $table->string('grade', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('studentscores_temp');
    }
}
