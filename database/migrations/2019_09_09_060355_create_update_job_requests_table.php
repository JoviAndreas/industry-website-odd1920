<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUpdateJobRequestsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('update_job_requests', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('job_id');
			$table->string('companyid', 100);
			$table->string('name', 200);
			$table->string('description', 500);
			$table->string('location', 100);
			$table->date('deadline');
			$table->integer('duration');
			$table->integer('quota');
			$table->string('linktest', 300)->nullable();
			$table->date('startdate');
			$table->date('enddate');
			$table->text('qualification', 65535)->nullable();
			$table->string('allowance', 100)->nullable();
			$table->string('status', 60);
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('update_job_requests');
	}

}
