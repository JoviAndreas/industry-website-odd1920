<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStudentsDoneTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('students_done', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('studentid', 100)->index('studentid');
			$table->integer('univ_supervisor_id')->nullable();
			$table->string('name', 200)->nullable();
			$table->string('email', 150)->nullable();
			$table->string('phone', 15)->nullable();
			$table->string('address', 120)->nullable();
			$table->string('program', 150)->nullable();
			$table->string('faculty', 150)->nullable();
			$table->string('semester', 100)->nullable();
			$table->string('track', 100);
			$table->string('company_name', 200)->nullable();
			$table->string('job_name', 200)->nullable();
			$table->date('date_of_birth')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('students_done');
	}

}
