<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStudentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('students', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('period');
			$table->string('studentid', 100)->index('studentid');
			$table->integer('supervisorid')->nullable();
			$table->integer('univ_supervisor_id')->nullable();
			$table->string('name', 200);
			$table->date('dob')->nullable();
			$table->string('sex', 20)->nullable();
			$table->string('school', 100)->nullable();
			$table->integer('degreeid')->nullable();
			$table->string('major', 100)->nullable();
			$table->integer('semester')->nullable();
			$table->string('address', 120)->nullable();
			$table->boolean('cv');
			$table->string('photo', 100)->nullable();
			$table->string('email', 150)->nullable();
			$table->string('phone', 15)->nullable();
			$table->string('facebook', 150)->nullable();
			$table->string('linkedin', 150)->nullable();
			$table->string('line', 100)->nullable();
			$table->string('linkportofolio', 150)->nullable();
			$table->integer('active_semester_id')->nullable();
			$table->integer('duration')->nullable();
			$table->string('track', 100)->nullable();
			$table->string('campus', 100)->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('students');
	}

}
