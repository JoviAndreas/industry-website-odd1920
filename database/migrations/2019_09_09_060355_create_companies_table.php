<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCompaniesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('companies', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('companyid', 100)->index('companyid');
			$table->integer('bipp_id')->nullable();
			$table->string('faculties', 100);
			$table->string('name', 200);
			$table->string('description', 100);
			$table->string('photo', 100);
			$table->string('email', 100);
			$table->string('phone', 100);
			$table->string('website', 100);
			$table->string('pic', 50);
			$table->string('address', 250);
			$table->string('state', 100)->default('open');
			$table->string('business_category', 100)->nullable();
			$table->string('position', 200)->nullable();
			$table->string('city', 200)->nullable();
			$table->string('postal_code', 32)->nullable();
			$table->string('ext', 10)->nullable();
			$table->string('fax', 32)->nullable();
			$table->string('cellphone', 32)->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('companies');
	}

}
