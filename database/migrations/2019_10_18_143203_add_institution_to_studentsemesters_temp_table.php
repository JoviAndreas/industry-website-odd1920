<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInstitutionToStudentsemestersTempTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('studentsemesters_temp', function(Blueprint $table)
        {
            $table->string('period')->nullable();
            $table->string('institution')->nullable();
            $table->string('acad_career')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('studentsemester_temp', function(Blueprint $table)
        {
            $table->dropColumn('period');
            $table->dropColumn('institution');
            $table->dropColumn('acad_career');
        });
    }
}
