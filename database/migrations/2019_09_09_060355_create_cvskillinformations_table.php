<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCvskillinformationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cvskillinformations', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('studentid', 100)->index('studentid');
			$table->string('name', 100);
			$table->string('status', 20);
			$table->string('type', 100);
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cvskillinformations');
	}

}
