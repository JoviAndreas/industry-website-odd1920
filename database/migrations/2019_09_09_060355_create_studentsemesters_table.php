<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStudentsemestersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('studentsemesters', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('studentid', 100)->index('studentid');
			$table->integer('semester');
			$table->float('ips', 10, 0);
			$table->float('ipk', 10, 0);
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('studentsemesters');
	}

}
