<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateJobPositionsDegreesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('job_positions_degrees', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('job_position_id')->index('job_positions_degrees_fk1');
			$table->integer('degree_id');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('job_positions_degrees');
	}

}
