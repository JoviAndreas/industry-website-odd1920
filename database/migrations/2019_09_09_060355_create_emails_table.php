<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('emails', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('to_email', 512);
			$table->string('to_name', 512);
			$table->text('subject', 65535);
			$table->string('from_email', 512);
			$table->string('from_name', 512);
			$table->text('message', 65535)->nullable();
			$table->timestamps();
			$table->softDeletes();
			$table->string('status', 10);
			$table->string('to_userid', 100)->nullable();
			$table->string('type', 50);
			$table->text('data', 65535)->nullable();
			$table->string('view', 100)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('emails');
	}

}
