<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateApprovalRequestTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('approval_request', function(Blueprint $table)
		{
			$table->integer('approval_request_id', true);
			$table->integer('recruitment_id');
			$table->string('status', 10);
			$table->string('approved_by', 100)->nullable();
			$table->dateTime('approved_on')->nullable();
			$table->string('rejected_by', 100)->nullable();
			$table->dateTime('rejected_on')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('approval_request');
	}

}
