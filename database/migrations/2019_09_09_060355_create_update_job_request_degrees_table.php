<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUpdateJobRequestDegreesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('update_job_request_degrees', function(Blueprint $table)
		{
			$table->integer('update_job_request_id');
			$table->integer('degree_id');
			$table->timestamps();
			$table->softDeletes();
			$table->primary(['update_job_request_id','degree_id'], 'update_job_request_degrees_pk');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('update_job_request_degrees');
	}

}
