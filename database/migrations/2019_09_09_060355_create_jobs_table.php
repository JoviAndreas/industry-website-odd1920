<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateJobsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('jobs', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('period');
			$table->string('companyid', 100)->index('companyid');
			$table->string('name', 100)->nullable()->index('name');
			$table->string('supervisor_name', 100)->nullable();
			$table->string('supervisor_contact', 100);
			$table->text('description', 65535);
			$table->string('location', 100);
			$table->date('deadline');
			$table->integer('duration');
			$table->integer('quota');
			$table->integer('quota_apply');
			$table->string('linktest', 300)->nullable();
			$table->string('learningobj', 100);
			$table->string('softskill', 100);
			$table->date('startdate');
			$table->date('enddate');
			$table->string('workinghour', 100);
			$table->string('token', 60);
			$table->string('job_state', 100)->default('open');
			$table->text('qualification', 65535)->nullable();
			$table->string('allowance', 100)->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('jobs');
	}

}
