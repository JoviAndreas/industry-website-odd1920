<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCvdateinformationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cvdateinformations', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('studentid', 100)->index('studentid');
			$table->string('startdate', 100);
			$table->string('enddate', 100);
			$table->string('description', 200);
			$table->string('role', 100);
			$table->string('jobdesc', 1000);
			$table->string('type', 100);
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cvdateinformations');
	}

}
