<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateActivestudentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('activestudents', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('studentid', 100);
			$table->string('name', 200);
			$table->string('token', 60);
			$table->integer('active_semester_id')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('activestudents');
	}

}
