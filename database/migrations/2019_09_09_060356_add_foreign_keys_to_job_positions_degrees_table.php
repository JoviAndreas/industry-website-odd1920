<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToJobPositionsDegreesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('job_positions_degrees', function(Blueprint $table)
		{
			$table->foreign('job_position_id', 'job_positions_degrees_fk1')->references('id')->on('job_positions')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('job_positions_degrees', function(Blueprint $table)
		{
			$table->dropForeign('job_positions_degrees_fk1');
		});
	}

}
