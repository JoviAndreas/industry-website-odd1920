<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Degree extends Model
{

    use SoftDeletes;

    protected $table = 'degrees';
    protected $dates = ['deleted_at'];

     //eloquent relationship

    public function faculty() {
        return $this->belongsTo('App\Faculty', 'faculty_id', 'id');
    }

    public function jobsdegrees(){
        return $this->hasMany('App\JobsDegrees', 'degreeid', 'id');
    }

    public function jobpositiondegrees()
    {
        return $this->hasMany('App\JobsPositionDegrees', 'degree_id', 'id');
    }

    public static function getDurationSixMonth(){
        $degrees_six_month = Degree::whereRaw('LOWER(name) = "computer science and mathematics"')
            ->orWhereRaw('LOWER(name) = "computer science and statistics"')
            ->orWhereRaw('LOWER(name) = "master track mti"')
            ->select('id')
            ->get();
        $arr_deg = [];
        foreach ($degrees_six_month as $d) {
            array_push($arr_deg, $d->id);
        }
        $implode = implode(';', $arr_deg);
        return $implode;
    }

    public static function getDurationTwelveMonth(){
        $degrees_twelve_month = Degree::whereRaw('LOWER(name) <> "computer science and mathematics"')
            ->whereRaw('LOWER(name) <> "computer science and statistics"')
            ->whereRaw('LOWER(name) <> "master track mti"')
            ->select('id')
            ->get();
        $arr_deg = [];
        foreach ($degrees_twelve_month as $d) {
            array_push($arr_deg, $d->id);
        }
        $implode = implode(';', $arr_deg);
        return $implode;
    }

}

?>
