<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cvskillinformation extends Model
{
	use SoftDeletes;

	protected $table = 'cvskillinformations';
    	protected $dates = ['deleted_at'];

    	//eloquent relationship
    	public function student(){
    		return $this->belongsTo('App\Student', 'studentid', 'studentid');
    	}

    	//Dapetin list skills dari student dan tipe tertentu (Skills or language)
	public function getCvSkills($type, $studentid)
	{
		return $this->where('studentid', '=', $studentid)->where('type','=',$type)->get();
	}

	//Hapus skills dari studentid tertentu
	public function deleteCvSkills($studentid)
	{
		$this->where('studentid', $studentid)->delete();
	}
}

?>