<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class LearningPlan extends Model
{
     use SoftDeletes;

     protected $table = 'learning_plan';
     protected $dates = ['deleted_at'];

     //eloquent relationship
     public function recruitment(){
         return $this->hasOne('App\Recruitment', 'id', 'recruitment_id');
     }
}
?>
