<?php

//IGNORE COUNT ERROR FOR PHP 7.2 keatas
use Illuminate\Support\Facades\Route;

if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
    error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
}

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/**
 * API
 */

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Route::get("/testing",'StudentController@testing');
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Route::group(['prefix' => 'api'], function() {
   Route::get('getStudentPlacement', 'APIController@getStudentPlacement');
});

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 * 1. Untuk mereset status cv pada mahasiswa yang gagal generate pdf (student profile tidak lengkap)
 * 2. Untuk generate otomatis cv mahasiswa yang gagal ke generate (student profile lengkap)
 * */
Route::get("/resetCV","StudentController@resetCVStatus");
Route::get("/generateCV","StudentController@generateStudentCV");
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// deploy
Route::post('/api/deploy', 'AppServiceController@deploy');

// can be opened for all
Route::get('/auth/logout', 'AuthController@logout');

// this middleware will check whether the user is student or not
// if the user is student then the routes mentioned below is restricted
// the routes can be opened for company and department only
Route::group(['middleware' => ['prestige']], function () {
    // get user profile page
    Route::get('profile/{id}', ['as' => 'profile', 'uses' => 'StudentController@profileOther']);

    // get student's CV
    Route::get('/previewcvs/{id}', ['as' => 'previewcv', 'uses' => 'StudentController@previewCvOther']);
});

// basic middleware for non logged in routes
//
Route::group(['middleware' => ['web']], function () {
    // home
    Route::get('/', ['as' => 'login', 'uses' => 'AuthController@index']);

    // login page
    Route::get('/login', ['as' => 'login', 'uses' => 'AuthController@index']);

    // login page
    Route::get('/auth/login', ['as' => 'login', 'uses' => 'AuthController@index']);

    // login submit
    Route::post('/auth/login', ['as' => 'login', 'uses' => 'AuthController@login']);

    // register page
//    Route::get('signin', ['as' => 'login', 'uses' => 'UserController@register']);

    //register page submit
    Route::post('insertUser', ['as' => 'login', 'uses' => 'UserController@insertUser']);
});

// this middleware is used to check whether the users is authenticated or not
// this doesn't check the role
// use this only when making routes for all role
Route::group(['middleware' => ['users']], function () {
    // get job positions
    Route::get('ajax/job_position', 'JobPositionController@getJobPositionAJAX');

    // get profile picture image
    Route::get('images/{id}', ['uses' => 'ImageController@getProfilePicture']);

    // get chart image
    Route::get('chart/{id}', ['uses' => 'ImageController@getChart']);

    // Open notif page
    Route::get('/notif', ['as' => 'mail', 'uses' => 'MailboxController@viewNotification']);

    // Update all notif status to be seen
    Route::get('/readNotif', ['as' => 'mail', 'uses' => 'MailboxController@updateReadNotification']);

    // mailbox page
    Route::get('mailbox', ['as' => 'mail', 'uses' => 'MailboxController@indexMessage']);

    // detail mailbox page
    Route::get('mailbox/{id}', ['as' => 'mail', 'uses' => 'MailboxController@detailInboxMessage']);

    // detail send message mailbox page
    Route::get('mailboxs/{id}', ['as' => 'mail', 'uses' => 'MailboxController@detailSentMessage']);

    // create new message page
    Route::get('/create', ['as' => 'mail', 'uses' => 'MailboxController@sendMessage']);

    // create new message with the recipient information
    Route::get('/reply/{id}/{token}', ['as' => 'mail', 'uses' => 'MailboxController@replyMessage']);

    // post / submit new message
    Route::post('/sendMail', ['as' => 'mail', 'uses' => 'MailboxController@insertSendMessage']);

    // company profile page
    Route::get('/profilec/{id}', ['as' => 'mail', 'uses' => 'CompanyController@profile']);

    Route::get('profile', ['as' => 'profile', 'uses' => 'StudentController@profileStudent']);
    Route::get('changepassword', ['as' => 'mail', 'uses' => 'UserController@changePassword']);
    Route::post('submitpassword', ['as' => 'mail', 'uses' => 'UserController@updateChangePassword']);
});

// this middleware is used to check the whether the user is student or not
Route::group(['middleware' => ['student']], function () {
    // Home page
    Route::get('/userdata', ['as' => 'student', 'uses' => 'RecruitmentController@userdata']);
    Route::get('/vacancy', ['as' => 'student', 'uses' => 'RecruitmentController@applyStudent']);

    // Home page
    // for student to apply job
    Route::get('/applyjob/{tokenid}', ['as' => 'student', 'uses' => 'RecruitmentController@insertApplyStudent']);

    Route::get('/student/grades/generate', 'StudentActivationController@generateStudentGrades');

    // Status page
    Route::get('/status', ['as' => 'student', 'uses' => 'RecruitmentController@priorityStudent']);

    // Home page | Status page
    // to unapply job
    Route::get('/unapply/{tokenid}', ['as' => 'student', 'uses' => 'RecruitmentController@deleteApplyStudent']);

    // Profile > Change Profile page
    Route::get('/updateprofile', ['as' => 'student', 'uses' => 'StudentController@editProfileStudent']);

    // Profile > Change Profile
    // submit change profile
    Route::post('/submitprofile', ['as' => 'student', 'uses' => 'StudentController@updateEditProfileStudent']);

    // Feedback
    // open feedback page
    Route::get('/feedback', ['as' => 'student', 'uses' => 'FeedbackController@indexStudent']);

    // Feedback
    // submit feedback
    Route::post('/submitFeedback', ['as' => 'student', 'uses' => 'FeedbackController@insertStudent']);

    // Mailbox Interview invitation
    // to accept interview invitation
    Route::post('/acceptInterview', ['as' => 'student', 'uses' => 'InterviewController@updateAcceptInterviewStudent']);

    // Mailbox Interview invitation
    // to reschedule interview invitation
    Route::post('/resInterview', ['as' => 'student', 'uses' => 'InterviewController@updateRescheduleInterviewStudent']);

    // Preview CV as pdf
    // to open current student's cv as PDF file
    Route::get('/previewcv', ['as' => 'student', 'uses' => 'StudentController@previewCvStudent']);


    // below is the inactive routes

    //Route::get('/learningplan', ['as' => 'student', 'uses' => 'StudentController@learningPlanStudent']);
    Route::get('/learningplan', ['as' => 'student', 'uses' => 'StudentController@tempLearningPlanBlock']);
    //Route::get('/downloadLearningPlan', ['as' => 'student', 'uses' => 'StudentController@generateLearningPlanStudent']);
    Route::get('/downloadLearningPlan', ['as' => 'student', 'uses' => 'StudentController@tempLearningPlanBlock']);
});

// this middleware is used to check the whether the user is company or not
Route::group(['middleware' => ['company']], function () {
    // Home page
    Route::get('/homec', ['as' => 'company', 'uses' => 'RecruitmentController@indexCompany']);

    // Report page
    Route::get('/company/report', ['as' => 'company', 'uses' => 'RecruitmentController@reportCompany']);

    // Report page export
    // Export company report to excel
    Route::get('/company/exportRecruitmentData', ['as' => 'company', 'uses' => 'RecruitmentController@exportData']);

    // Home
    // for company to accept a student
    Route::get('/accept/{id}', ['as' => 'company', 'uses' => 'RecruitmentController@updateAcceptRecruitmentCompany']);

    // Home
    // for company to process a student
    Route::get('/process/{id}', ['as' => 'company', 'uses' => 'RecruitmentController@updateProcessRecruitmentCompany']);

    // Home
    // for company to reject a student
    Route::get('/reject/{id}', ['as' => 'company', 'uses' => 'RecruitmentController@updateRejectRecruitmentCompany']);

    // Home
    // for company to submit new interview data
    Route::post('/setInterview', ['as' => 'company', 'uses' => 'InterviewController@insertInterviewCompany']);

    // Home
    // for company to export the accepted interview data to excel file
    Route::get('/interviewD', ['as' => 'company', 'uses' => 'InterviewController@exportAcceptedCompany']);

    // Home
    // for company to send unaccept request
    Route::post('/unaccept', ['as' => 'company', 'uses' => 'RecruitmentController@unacceptStudentByCompany']);

    // Home
    // for company to send unreject request
    Route::post('/unreject', ['as' => 'company', 'uses' => 'RecruitmentController@unrejectStudentByCompany']);

    // Profile
    // update profile page
    Route::get('/updateprofiles', ['as' => 'company', 'uses' => 'CompanyController@editProfileCompany']);

    // Profile
    // submit update profile
    Route::post('/submitprofiles', ['as' => 'company', 'uses' => 'CompanyController@updateEditProfileCompany']);

    // Manage > Job page
    Route::get('/job', ['as' => 'company', 'uses' => 'JobController@indexCompany']);

    // Manage > Job
    // insert job for company
    Route::post('/insertJobDescs', ['as' => 'company', 'uses' => 'JobController@insertCompany']);

    // Manage > Job
    // update job for company
    Route::post('/updateJobDescs', ['as' => 'company', 'uses' => 'JobController@updateCompany']);

    // Manage > Job
    // delete job for company
    Route::get('/deleteJobs/{token}', ['as' => 'company', 'uses' => 'JobController@deleteCompany']);

    // Manage > Job
    // close job for company
    Route::get('/close_job/{job_token}', ['as' => 'company', 'uses' => 'JobController@closeByCompany']);

    // Manage > Job
    // open job for company
    Route::get('/open_job/{job_token}', ['as' => 'company', 'uses' => 'JobController@openByCompany']);

    // Manage > Job
    // close all jobs for company
    Route::get('/close_jobs', ['as' => 'company', 'uses' => 'JobController@closeAllByCompany']);

    // Manage > Job
    // open all jobs for company
    Route::get('/open_jobs', ['as' => 'company', 'uses' => 'JobController@openAllByCompany']);

    // Manage > Supervisor page
    // Open manage supervisor page
    Route::get('/supervisor', ['as' => 'company', 'uses' => 'SupervisorController@indexCompany']);

    // Manage > Supervisor
    // assign supervisor to the student
    Route::get('/assignStudent', ['as' => 'company', 'uses' => 'SupervisorController@assignCompany']);

    // Manage > Supervisor
    // submit assign supervisor to the student
    Route::post('/submitAssignSPV', ['as' => 'company', 'uses' => 'SupervisorController@updateAssignCompany']);

    // Manage > Supervisor
    // remove student from his/her spv
    Route::get('/removeStudentFromSpv/{spvid}/{userid}', ['as' => 'company', 'uses' => 'SupervisorController@removeStudentFromSpv']);

    // Feedback page
    // open insert feedback page
    Route::get('/feedbacks', ['as' => 'company', 'uses' => 'FeedbackController@indexCompany']);

    // Feedback
    // submit feedback
    Route::post('/submitFeedbacks', ['as' => 'company', 'uses' => 'FeedbackController@insertCompany']);

    // Close Company
    Route::get('/closeCompany', ['as' => 'company', 'uses' => 'CompanyController@changeStateClosedCompany']);

    // Open Company
    Route::get('/openCompany', ['as' => 'company', 'uses' => 'CompanyController@changeStateOpenCompany']);

    Route::get('/listSupervisor', ['as' => 'company', 'uses' => 'SupervisorController@exportCompany']);

    Route::post('/insertSupervisors', ['as' => 'company', 'uses' => 'SupervisorController@insertCompany']);

    Route::post('/updateSupervisors', ['as' => 'company', 'uses' => 'SupervisorController@updateCompany']);
    Route::get('/deleteSpv/{token}', ['as' => 'company', 'uses' => 'SupervisorController@deleteCompany']);
});

// this middleware is used to check the whether the user is department or not
Route::group(['middleware' => ['department']], function () {


    Route::group(['prefix' => '/department/export'], function() {
        Route::get('waitingStudent', 'ExportController@waitingStudent');
        Route::get('studentCVSummary', 'ExportController@studentCVSummary');
        Route::get('studentApplySummary', 'ExportController@studentApplySummary');
        Route::get('jobQuotaSummary', 'ExportController@jobQuotaSummary');


    });

    // Home
    Route::get('/homed', 'StudentController@homeDepartment')->name('department.home');

    // Home
    // submit change head of program selection
    Route::get('/changeHeadOfProgram', ['as' => 'department', 'uses' => 'DepartmentController@changeHeadOfProgram']);

    Route::get('automated/service/config', 'AutomatedServiceController@index')->name('automated_service.index');
    Route::post('automated/service/update', 'AutomatedServiceController@update')->name('automated_service.update');

    // Home > table waiting student
    // Get Student's job application list for department's home page
    // Ajax request data table
    Route::get('/getStudentJobApplicationList/{studentid}', ['as' => 'department', 'uses' => 'StudentController@getStudentJobApplicationList']);

    // Others > Master Password page
    // open master password page to configure master password
    Route::get('/passwords', ['as' => 'department', 'uses' => 'DepartmentController@masterPasswordDepartment']);

    // Others > Faculty Password page
    // open master password page to configure faculty password
    Route::get('/facultyPasswords', ['as' => 'department', 'uses' => 'DepartmentController@masterPasswordDepartmentFaculty']);

    // Others > See Feedbacks page
    // see the feedbacks posted by all user
    Route::get('/seeFeedbacks', ['as' => 'department', 'uses' => 'FeedbackController@seeFeedbacks']);

    // Others > Department Account page
    Route::get('/addDepartmentAccount', ['as' => 'department', 'uses' => 'UserController@addDepartmentAccount']);

    // Others > Department Account
    // open change department account's head of program data page
    Route::get('/changeHeadOfProgramData/{departmentid}', ['as' => 'department', 'uses' => 'DepartmentController@changeHeadOfProgramData']);

    // Others > Department Account
    // submit to change the HOP data from /changeHeadOfProgramData page
    Route::post('/submitChangeHOPData', ['as' => 'department', 'uses' => 'DepartmentController@submitChangeHOPData']);

    // Others > Department Account
    // submit post new department account
    Route::post('/insertNewDepartmentAccount', ['as' => 'department', 'uses' => 'UserController@insertNewDepartmentAccount']);

    // Others > Department Account
    // change status of department account to be inactive
    Route::get('/inactiveAccountDepartment/{id}', ['as' => 'department', 'uses' => 'UserController@inactiveAccountDepartment']);

    // Others > Department Account
    // change status of department account to be active
    Route::get('/activeAccountDepartment/{id}', ['as' => 'department', 'uses' => 'UserController@activeAccountDepartment']);

    // Others > Department Account
    // open update page for department account
    Route::get('/updateDepartmentAccount/{id}', ['as' => 'department', 'uses' => 'UserController@updateDepartmentAccount']);

    // Others > Department Account
    // submit update for department account
    Route::post('/submitUpdateDepartmentAccount/{id}', ['as' => 'department', 'uses' => 'UserController@submitUpdateDepartmentAccount']);

    // Manage > Company page
    Route::get('/register', ['as' => 'department', 'uses' => 'CompanyController@indexDepartment']);

    // Manage > Company
    // unapprove company button
    Route::get('/unapproveCompany/{id}', ['as' => 'department', 'uses' => 'CompanyController@unapproveCompany']);

    // Manage > Company
    // submit new company by form
    Route::post('/submitByForm', ['as' => 'department', 'uses' => 'CompanyController@insertDepartment']);

    // Manage > Company
    // generate new password for company
    Route::get('/generateNew/{id}', ['as' => 'department', 'uses' => 'CompanyController@generateNewPasswordDepartment']);

    // Manage > Company
    // change company's state either to close or to open
    Route::get('/changeState/{id}/{state}', ['as' => 'department', 'uses' => 'CompanyController@changeStateDepartment']);

    // Manage > Company
    // change company's status either to active or to inactive
    Route::get('/changeStatus/{id}/{status}', ['as' => 'department', 'uses' => 'CompanyController@changeDepartmentStatus']);

    // Manage > Company
    // delete company
    Route::get('/deleteCompany/{id}', ['as' => 'department', 'uses' => 'CompanyController@deleteDepartment']);

    // Manage > Company
    // export all companies data to excel file
    Route::get('/exportaccount', ['as' => 'department', 'uses' => 'CompanyController@exportDepartment']);

    // Manage > Job page
    // (NOT filtered based on program)
    Route::get('/manageJob', ['as' => 'department', 'uses' => 'JobController@indexDepartment']);

    // Manage > Job page
    // (filtered based on program)
    Route::get('/managejobfiltered', ['as' => 'department', 'uses' => 'JobController@indexDepartmentFiltered']);

    // Manage > job
    // open update job page
    Route::get('/departmentupdatejob/{token}', ['as' => 'department', 'uses' => 'JobController@departmentupdatejob']);

    // Manage > Job
    // submit update job for department account
    Route::post('/updateJobDesc', ['as' => 'department', 'uses' => 'JobController@updateDepartment']);

    // Manage > job
    // submit new job for department
    Route::post('/insertJobDesc', ['as' => 'department', 'uses' => 'JobController@insertDepartment']);

    // Manage > job
    // delete job for department
    Route::get('/deleteJob/{token}', ['as' => 'department', 'uses' => 'JobController@deleteDepartment']);

    // Manage > job
    // approve job for department
    Route::get('/approve_job/{job}', 'JobController@approveByDepartment');

    // Manage > job
    // reject job for department
    Route::get('/reject_job/{job}', 'JobController@rejectByDepartment');

    // Manage > job
    // close job vacancy for department
    Route::get('/close_job/{job}', 'JobController@closeVacancyDepartment');

    // Manage > Job Position Page
    Route::get('/manageJobPosition', ['as' => 'department', 'uses' => 'JobPositionController@index']);

    // Manage > Deleted Job page
    Route::get('/manageDeletedJob', ['as' => 'department', 'uses' => 'JobController@manage_deleted_job']);

    // Manage > Deleted Job page
    Route::get('/undelete_job/{job_id}', ['as' => 'department', 'uses' => 'JobController@undelete_job']);

    // Manage > Student
    Route::get('/manageStudent', ['as' => 'department', 'uses' => 'StudentController@indexDepartment']);

    // Manage > Student
    // Export students list to excel
    Route::get('/exportStudentData', ['as' => 'department', 'uses' => 'StudentController@exportStudentData']);

    // Manage > Student
    // upload student data by excel
    Route::post('/submitByExcelStudent', ['as' => 'department', 'uses' => 'StudentController@uploadDepartment']);

    // Manage > University Supervisor page
    Route::get('/manageguider', ['as' => 'department', 'uses' => 'StudentController@universitySupervisorDepartment']);

    // Manage > University Supervisor
    // download template for inserting student's guider
    // return excel
    Route::get('/templateguider', ['as' => 'department', 'uses' => 'StudentController@templateUniversitySupervisorDepartment']);

    // Manage > University Supervisor
    // submit the guider data by excel file
    Route::post('/submitGuiderByExcel', ['as' => 'department', 'uses' => 'StudentController@uploadUniversitySupervisorDepartment']);

    // Manage > University Supervisor
    // delete the guider data
    Route::get('/delete_univ_supervisor/{id}', ['as' => 'department', 'uses' => 'StudentController@deleteUniversitySupervisorDepartment']);

    // Organize > Apply page
    Route::get('/apply', ['as' => 'department', 'uses' => 'RecruitmentController@applyDepartment']);

    // Organize > Apply page
    // get student's list that could apply to the specific job id
    Route::get('/getdata/{jobid}', ['as' => 'department', 'uses' => 'RecruitmentController@getStudentApplyDepartment']);

    // Organize > Apply Page
    // submit student's list that the department user want to apply to a job
    Route::post('/submitApplyJob', ['as' => 'department', 'uses' => 'RecruitmentController@insertApplyDepartment']);

    // Organize > Approved page
    Route::get('/accepted', ['as' => 'department', 'uses' => 'RecruitmentController@acceptedDepartment']);

    // Organize > Approved
    // export approved data to excel
    // return excel file
    Route::get('/exportAccData', ['as' => 'department', 'uses' => 'RecruitmentController@exportAcceptedDepartment']);

    // Organize > Approved
    // Unapply Student
    Route::post('/department/unaccept_student/{recruitment_id}', 'RecruitmentController@unacceptDepartment');

    // Organize > Edit Recruitment page
    Route::get('/editRecruitment', ['as' => 'department', 'uses' => 'RecruitmentController@indexDepartment']);

    // Organize > Edit Recruitment
    // submit edit recruitment (button update for each student)
    Route::post('/submitEditRec', ['as' => 'department', 'uses' => 'RecruitmentController@updateDepartment']);

    // Organize > Edit Recruitment
    // Get job application's interview data
    // Ajax request data table
    Route::get('/getRecruitmentInterviewData/{recruitment_id}', ['as' => 'department', 'uses' => 'RecruitmentController@getRecruitmentInterviewData']);

    // Organize > Edit Recruitment
    // get all recruitment data as excel file (Accepted Recruitment Data)
    Route::get('/exportRecruitmentData', ['as' => 'department', 'uses' => 'RecruitmentController@exportRecruitmentData']);

    // Organize > Edit BIPP page
    Route::get('/editBipp', ['as' => 'department', 'uses' => 'BippController@indexBipp']);

    // Organize > Edit BIPP
    // submit edit BIPP (button update for BIPP's GPA)
    Route::post('/submitEditBIPP', ['as' => 'department', 'uses' => 'BippController@updateBipp']);

    // Organize > Edit Description page
    Route::get('/editDesc', ['as' => 'department', 'uses' => 'DescriptionController@indexDesc']);

    // Organize > Edit Description
    // submit edit Description (button update for Edit Description)
    Route::post('/submitEditDesc', ['as' => 'department', 'uses' => 'DescriptionController@updateDesc']);

    // Organize > Unaccept Request page
    Route::get('/unaccept_request', ['as' => 'department', 'uses' => 'RecruitmentController@unaccept_request']);

    // Organize > Unaccept Request
    // submit unaccept request update (approve all / reject all)
    Route::post('/unaccept_request_submit', ['as' => 'department', 'uses' => 'RecruitmentController@unaccept_request_submit']);

    // Organize > Unaccept Request
    // submit unaccept request update (approve each)
    Route::get('/approve_unaccept_request/{id}', ['as' => 'department', 'uses' => 'RecruitmentController@approve_unaccept_request']);

    // Organize > Unaccept Request
    // submit unaccept request update (reject each)
    Route::get('/reject_unaccept_request/{id}', ['as' => 'department', 'uses' => 'RecruitmentController@reject_unaccept_request']);

    // Organize > Unreject Request page
    Route::get('/unreject_request', ['as' => 'department', 'uses' => 'RecruitmentController@unreject_request']);

    // Organize > Unreject Request
    // submit unreject request update (approve all / reject all)
    Route::post('/unreject_request_submit', ['as' => 'department', 'uses' => 'RecruitmentController@unreject_request_submit']);

    // Organize > Unreject Request
    // submit unreject request update (approve each)
    Route::get('/approve_unreject_request/{id}', ['as' => 'department', 'uses' => 'RecruitmentController@approve_unreject_request']);

    // Organize > Unreject Request
    // submit unreject request update (reject each)
    Route::get('/reject_unreject_request/{id}', ['as' => 'department', 'uses' => 'RecruitmentController@reject_unreject_request']);

    // Organize > Approval Request page
    Route::get('/approval_request', ['as' => 'department', 'uses' => 'RecruitmentController@approval_request']);

    // Organize > Approval Request
    // submit approval request update (approve all / reject all)
    Route::post('/approval_request_submit', ['as' => 'department', 'uses' => 'RecruitmentController@approval_request_submit']);

    // Organize > Approval Request
    // submit approval request update (approve each)
    Route::get('/approve_approval_request/{id}', ['as' => 'department', 'uses' => 'RecruitmentController@approve_approval_request']);

    // Organize > Approval Request
    // submit approval request update (reject each)
    Route::get('/reject_approval_request/{id}', ['as' => 'department', 'uses' => 'RecruitmentController@reject_approval_request']);

    // Organize > Approval Request
    // submit approval request update (approve each)
    Route::get('/approve_approval_request_history/{id}', ['as' => 'department', 'uses' => 'RecruitmentController@approve_approval_request_history']);

    // Organize > Approval Request
    // submit approval request update (reject each)
    Route::get('/reject_approval_request_history/{id}', ['as' => 'department', 'uses' => 'RecruitmentController@reject_approval_request_history']);

    // Organize > Approval Request
    // submit approval request update (delete each)
    Route::get('/delete_approval_request_history/{id}', ['as' => 'department', 'uses' => 'RecruitmentController@delete_approval_request_history']);

    // Organize > Approval Request
    // submit approval request update (pending each)
    Route::get('/pending_approval_request_history/{id}', ['as' => 'department', 'uses' => 'RecruitmentController@pending_approval_request_history']);

    // Organize > Assign Supervisor page
    // open assign university supervisor page
    Route::get('/assign_supervisor', ['as' => 'department', 'uses' => 'StudentController@assign_supervisor']);

    // Organize > Assign Supervisor
    // get list approved student
    Route::get('/get_list_approved_student', ['as' => 'department', 'uses' => 'StudentController@get_list_approved_student']);

    // Organize > Assign Supervisor
    // submit assign university supervisor
    Route::post('/assign_university_supervisor', ['as' => 'department', 'uses' => 'StudentController@assign_university_supervisor']);

    // Account Manager > Account Approval page
    Route::get('/accountapproval', ['as' => 'department', 'uses' => 'UserController@accountapproval']);

    // Account > Manager > Account Approval
    // unreject approval student
    Route::get('/unreject/{studentid}', ['as' => 'department', 'uses' => 'UserController@unrejectapproval']);

    // Account > Manager > Account Approval
    // unreject approval company
    Route::get('/unrejectcompany/{companyid}', ['as' => 'department', 'uses' => 'UserController@unrejectapprovalcompany']);

    // Account Manager > Account Approval
    // submit approve selected students (approve all)
    Route::post('/updateapprovestudent', ['as' => 'department', 'uses' => 'UserController@updateapprovestudent']);

    // Account Manager > Account Approval
    // submit approve selected student (approve each)
    Route::get('/approveaccount/{id}', ['as' => 'department', 'uses' => 'UserController@approveaccount']);

    // Account Manager > Account Approval
    // submit reject selected student (reject each)
    Route::get('/rejectaccount/{id}', ['as' => 'department', 'uses' => 'UserController@rejectaccount']);

    // Account Manager > Account Approval
    // submit approve selected students (approve all)
    // this is for the not-listed students
    Route::post('/updateapprovestudentnotlisted', ['as' => 'department', 'uses' => 'UserController@updateapprovestudentnotlisted']);

    // Account Manager > Account Approval
    // submit approve selected companies (approve all)
    Route::post('/updateapprovecompany', ['as' => 'department', 'uses' => 'UserController@updateapprovecompany']);

    // Account Manager > Account Approval
    // Open student registration page (for department role)
    Route::get('/registerStudentByDepartment', ['as' => 'profile', 'uses' => 'UserController@registerByDepartment']);

    // Account Manager > Account Approval
    // submit student registration (for department role)
    Route::post('/insertUserByDepartment', ['as' => 'login', 'uses' => 'UserController@insertUserByDepartment']);

    // Account Manager > Active Student List page
    Route::get('/activestudentlist', ['as' => 'department', 'uses' => 'UserController@activestudentlist']);

    // Account Manager > Active Student List
    // submit insert active student list by excel file
    Route::post('/insertactivestudent', ['as' => 'department', 'uses' => 'UserController@insertactivestudent']);

    // Account Manager > Active Student List
    // submit delete active student list (delete each)
    Route::post('/deleteactivestudent', ['as' => 'department', 'uses' => 'UserController@deleteactivestudent']);

    // Job Detail page
    Route::get('/job_detail/{id}', ['as' => 'department', 'uses' => 'JobController@job_detail_view_by_department']);

    Route::post('/submitByExcel', ['as' => 'department', 'uses' => 'CompanyController@uploadDepartment']);

    Route::post('/insertStudent', ['as' => 'department', 'uses' => 'StudentController@insertDepartment']);

    Route::get('/deleteStudent/{id}', ['as' => 'department', 'uses' => 'StudentController@deleteDepartment']);
    Route::get('/finishStudent/{id}', ['as' => 'department', 'uses' => 'StudentController@finishDepartment']);
    Route::post('/submitMasterPassword', ['as' => 'department', 'uses' => 'DepartmentController@updateMasterPasswordDepartment']);

    Route::get('/updateprofilestudent/{id}', ['as' => 'department', 'uses' => 'StudentController@editProfileStudentByDepartment']);


    Route::post('/submitprofilebydepartment', ['as' => 'department', 'uses' => 'StudentController@updateEditProfileStudentByDepartment']);



    Route::get('/resendApprovalEmail/{id}', ['as' => 'department', 'uses' => 'UserController@resendApprovalEmail']);
    Route::get('/resetPasswordStudent/{id}', ['as' => 'department', 'uses' => 'UserController@resetPasswordStudent']);

    //Route::get('/printLP/{studentid}', ['as' => 'department', 'uses' => 'StudentController@generateLearningPlanDepartment']);
    Route::get('/printLP/{studentid}', ['as' => 'department', 'uses' => 'StudentController@tempLearningPlanBlock']);

    // Faculty Management
    Route::get('/department/faculty/manage', 'FacultyController@index')->name('department.faculty'); //filter by faculty
//    Route::post('/department/faculty/insert', 'FacultyController@insertFaculty')->name('department.faculty.insert');
//    Route::get('/department/faculty/edit/{faculty}', 'FacultyController@editFaculty')->name('department.faculty.edit');
//    Route::post('/department/faculty/update/{faculty}', 'FacultyController@updateFaculty')->name('department.faculty.update');
//    Route::post('/department/faculty/delete/{faculty}', 'FacultyController@deleteFaculty')->name('department.faculty.delete');


    // Job Position Management
    Route::get('/department/jobposition/manage', 'JobPositionController@index')->name('department.jobposition'); //filter by faculty
    Route::post('/department/jobposition/insert', 'JobPositionController@insertJobPosition')->name('department.jobposition.insert');
    Route::get('/department/jobposition/edit/{jobposition}', 'JobPositionController@editJobPosition')->name('department.jobposition.edit');
    Route::post('/department/jobposition/update/{jobposition}', 'JobPositionController@updateJobPosition')->name('department.jobposition.update');
    Route::post('/department/jobposition/delete/{jobposition}', 'JobPositionController@deleteJobPosition')->name('department.jobposition.delete');

    // Degree Management
//    Route::post('/degree/insert/', 'FacultyController@insertDegree')->name('department.degree.insert');
//    Route::post('/degree/update/', 'FacultyController@updateDegree')->name('department.degree.update');
//    Route::post('/degree/delete/{degree}', 'FacultyController@deleteDegree')->name('department.degree.delete');

    // API
    Route::get('/department/data/sync', 'APIController@index')->name('data_synchronization.index');
    Route::post('/department/data/student/insert/temp/header', 'APIController@insertTemporaryStudentHeader');
    Route::post('/department/data/student/insert/score', 'APIController@insertStudentsGradesByAPI');

    // Student Activation after API
    Route::post('/department/data/student/sync/temp/header', 'StudentActivationController@syncHeader');
//    Route::get('/department/data/student/initStudentPassword', 'StudentActivationController@initStudentPassword');

    Route::post('/department/togglecompanybipp/{companyId}', 'BippController@toggleCompanyBippStatus');
    Route::get('/department/job/generateToken/{jobid}', 'JobController@generateToken');
});
?>