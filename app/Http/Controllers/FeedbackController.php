<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Http\Request;
use Dompdf\Dompdf;
use Storage;
use Response;
use Auth;
use DB;
use Validator;
use Hash;
use App\Recruitment;
use App\Mailbox;
use App\Company;
use App\Student;
use App\User;
use App\Interview;
use App\Feedback;
use App\Job;

class FeedbackController extends BaseController{

	use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

		//function untuk cek apakah student sudah di accept atau belum
	public function checkAccepted()
	{
		$rec = Recruitment::where('studentid','=', Auth::user()->userid)
			->where('status','=','approved')
			->first();
		if($rec != null){
			return 'accepted';
		}
		else{
			return "no";
		}
	}

	// Feedback page
	// open insert feedback page for company
	// routes: /feedbacks
	public function indexCompany()
	{
		$getState = Company::where('companyid', '=', Auth::user()->userid)
			->first();

		$user = Auth::user();

		$mailboxCount = Mailbox::where('type', '=', 'mailbox')
			->where('status', '=', 'unseen')
			->where('recipientid', '=', Auth::user()->userid)
			->count();

		return view("company.feedback",
		[
			'user' => $user,
			'mailboxCount' => $mailboxCount,
			'state' => $getState->state,
			'notifCount' => app('App\Http\Controllers\MailboxController')->getCountNotification(),
			'notif' => app('App\Http\Controllers\MailboxController')->getLatestNotification()
		]);
	}

	// Feedback
	// submit feedback for company
	// routes: /submitFeedbacks
	public function insertCompany(Request $req)
	{
		//validasi feedbacknya
		$user = Auth::user();
		$messages =
			[
    			'txtfeed.required' => 'Field must be Filled!',
    			'txtfeed.max' => 'Maximum length is 90 Characters!'
			];

		$rules =
			[
    			'txtfeed' => 'required|max:500'
       		];

		$validator = Validator::make($req->all(), $rules, $messages);

		if ($validator->fails())
		{
	    	return redirect('/feedbacks')->withErrors($validator)->withInput();
		}
		else
		{
			//insert data feedback
			$feedback = new Feedback;
			$feedback->userid = $user->userid;
			$feedback->content = $req->txtfeed;
			$feedback->save();
			return redirect('/feedbacks')->with('fyi', 'Berhasil');
		}
	}

	// Feedback page
	// get insert feedback page for student
	// routes: /feedback
	public function indexStudent()
	{
		$mailboxCount = Mailbox::whereIn('type', array('interview', 'mailbox'))
			->where('status', '=', 'unseen')
			->where('recipientid', '=', Auth::user()->userid)
			->count();

		return view('student.feedback',
		[
			'accepted' => $this->checkAccepted(),
			'mailboxCount' => $mailboxCount,
			'notifCount' => app('App\Http\Controllers\MailboxController')->getCountNotification(),
			'notif' => app('App\Http\Controllers\MailboxController')->getLatestNotification()
		]);
	}

	// Feedback
	// submit feedback for student
	// routes: /submitFeedback
	public function insertStudent(Request $req)
	{
		//validasi kosong dan max using validator
		$user = Auth::user();
		$messages =
			[
    			'txtfeed.required' => 'Field must be Filled!',
    			'txtfeed.max' => 'Maximum length is 500 Characters!'
			];

		$rules =
			[
           		'txtfeed' => 'required|max:500'
			];

		$validator = Validator::make($req->all(), $rules, $messages);

		if ($validator->fails())
		{
		    	return redirect('/feedback')->withErrors($validator)->withInput();
		}
		else
		{
			//insert feedback
			$insertFb = new Feedback;
			$insertFb->userid = $user->userid;
			$insertFb->content = $req->txtfeed;
			$insertFb->save();
			return redirect('feedback')->with('fyi', 'Berhasil');
		}
	}

	// Others > See Feedbacks page
	// for department to see all feedbacks posted by all user
	// routes: /seeFeedbacks
	public function seeFeedbacks(){
		return view('department.seefeedbacks',
		[
			'feedbacks' => Feedback::get()
		]);
	}
}
?>
