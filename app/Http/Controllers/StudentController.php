<?php

namespace App\Http\Controllers;

use App\Company;
use App\Cvdateinformation;
use App\Cvskillinformation;
use App\Degree;
use App\Department;
use App\Faculty;
use App\Job;
use App\Mailbox;
use App\Recruitment;
use App\Student;
use App\Studentscore;
use App\Studentsemester;
use App\UniversitySupervisor;
use App\User;
use Auth;
use Carbon\Carbon;
use DB;
use Dompdf\Dompdf;
use Excel;
use File;
use Hash;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Image;
use PhpOffice\PhpWord\Style\Font;
use PhpOffice\PhpWord\TemplateProcessor;
use Response;
use route;
use Storage;
use URL;
use Validator;
use View;

class StudentController extends BaseController{

    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    //function untuk cek apakah student sudah di accept atau belum
    public function checkAccepted()
    {
        $rec = Recruitment::where('studentid','=', Auth::user()->userid)->where('status','=','approved')->first();
        if($rec != null){
            return 'accepted';
        }
        else{
            return "no";
        }
    }

    // Manage > Student page
    // routes: /manageStudent
    public function indexDepartment(Request $request)
    {
        $allowed_degree_id = DepartmentController::get_allowed_degree_list();

        // Display All EVEN AND ODD active students
        $Students = Student::filterCampus()->leftJoin('degrees', 'degrees.id', '=', 'students.degreeid')
            ->whereNull('degrees.deleted_at')
            ->whereNull('students.deleted_at')
            ->whereIn('students.degreeid', $allowed_degree_id)
            // ->where('students.period','2010')
            ->select('students.*', 'degrees.name as degree_name')
            ->get();

        $tracks = Student::filterCampus()->leftJoin('degrees', 'degrees.id', '=', 'students.degreeid')
            ->whereNull('degrees.deleted_at')
            ->whereNull('students.deleted_at')
            ->whereIn('students.degreeid', $allowed_degree_id)
            // ->where('students.period','2010')
            ->select('track')
            ->distinct()
            ->get();
        
        // Searching
        if( $request->Searching != null ){
            $Students = Student::filterCampus()->leftJoin('degrees', 'degrees.id', '=', 'students.degreeid')
            ->whereNull('degrees.deleted_at')
            ->whereNull('students.deleted_at')
            ->whereIn('students.degreeid', $allowed_degree_id)
            ->where(function ($query) use($request){
                $query->where('students.studentid','like','%'.$request->Searching.'%')
                    ->orWhere('students.name','like','%'.$request->Searching.'%');
            })
            // ->where('students.period','2010')
            ->select('students.*', 
                    'degrees.name as degree_name')
            ->get();

            $tracks = Student::filterCampus()->leftJoin('degrees', 'degrees.id', '=', 'students.degreeid')
            ->whereNull('degrees.deleted_at')
            ->whereNull('students.deleted_at')
            ->whereIn('students.degreeid', $allowed_degree_id)
            ->where(function ($query) use($request){
                $query->where('students.studentid','like','%'.$request->Searching.'%')
                    ->orWhere('students.name','like','%'.$request->Searching.'%');
            })
            // ->where('students.period','2010')
            ->select('track')
            ->distinct()
            ->get();

            $Searching = $request->Searching;
        }else{
            $Searching = "";
        }

        return view('department.managestudent', 
        ['Students' => $Students,
         'tracks' => $tracks,
         'Seaching' => $Searching
        ]);
    }

    // exporting students data to excel
    // routes: /exportStudentData
    public function exportStudentData()
    {
        $allowed_degree_id = DepartmentController::get_allowed_degree_list();

        $data = Student::filterCampus()
            ->with(['degree' => function($query) {
                $query->select('id', 'name');
            }, 'user' => function($query) {
                $query->select('userid', 'status');
            }, 'studentsemester' => function ($query) {
                $query->select('studentid', 'period', 'semester', 'ipk');
            }])
            ->whereIn('students.degreeid', $allowed_degree_id)
            ->where('students.period','2010')
            ->select(
                'students.studentid',
                'students.name',
                'students.dob',
                'students.major',
                'students.semester',
                'students.address',
                'students.email',
                'students.school',
                'students.track',
                'students.degreeid',
                'students.campus'
            )
            ->get();

//		return response()->json($data[0]);
        ob_end_clean();
        ob_start();
        $excel = Excel::create('Students', function ($excel) use ($data)
        {
            $excel->sheet('Sheet1', function($sheet) use($data)
            {
                $sheet->cell('A1', function($cell)
                {
                    $cell->setValue('NIM');
                });
                $sheet->cell('B1', function($cell)
                {
                    $cell->setValue('Name');
                });
                $sheet->cell('C1', function($cell)
                {
                    $cell->setValue('Date of Birth');
                });
                $sheet->cell('D1', function($cell)
                {
                    $cell->setValue('Program');
                });
                $sheet->cell('E1', function($cell)
                {
                    $cell->setValue('Major');
                });
                $sheet->cell('F1', function($cell)
                {
                    $cell->setValue('Semester');
                });
                $sheet->cell('G1', function($cell)
                {
                    $cell->setValue('Address');
                });
                $sheet->cell('H1', function($cell)
                {
                    $cell->setValue('Email');
                });
                $sheet->cell('I1', function($cell)
                {
                    $cell->setValue('Registration Status');
                });
                $sheet->cell('J1', function($cell)
                {
                    $cell->setValue('School');
                });
                $sheet->cell('K1', function($cell)
                {
                    $cell->setValue('Track');
                });
                $sheet->cell('L1', function($cell)
                {
                    $cell->setValue('IPK');
                });
                $sheet->cell('M1', function($cell)
                {
                    $cell->setValue('Campus');
                });

                $counter=2;
                foreach($data as $record)
                {
                    $maxSemester = $record->studentsemester->max('semester');

                    $tbody = [
                        'J' => $record->school,
                        'K' => $record->track,
                        'L' => $record->studentsemester->where('semester', $maxSemester)->first()->ipk,
                        'M' => $record->campus
                    ];
                    $sheet->cell('A'.$counter, function($cell)use($record)
                    {
                        $cell->setValue($record->studentid);
                    });
                    $sheet->cell('B'.$counter, function($cell)use($record)
                    {
                        $cell->setValue($record->name);
                    });
                    $sheet->cell('C'.$counter, function($cell)use($record)
                    {
                        $cell->setValue($record->dob);
                    });
                    $sheet->cell('D'.$counter, function($cell)use($record)
                    {
                        $cell->setValue($record->degree->name);
                    });
                    $sheet->cell('E'.$counter, function($cell)use($record)
                    {
                        $cell->setValue($record->major);
                    });
                    $sheet->cell('F'.$counter, function($cell)use($record)
                    {
                        $cell->setValue($record->semester);
                    });
                    $sheet->cell('G'.$counter, function($cell)use($record)
                    {
                        $cell->setValue($record->address);
                    });
                    $sheet->cell('H'.$counter, function($cell)use($record)
                    {
                        $cell->setValue($record->email);
                    });
                    $sheet->cell('I'.$counter, function($cell)use($record)
                    {
                        $cell->setValue($record->user->status);
                    });
                    $sheet->cell('J'.$counter, function($cell)use($record)
                    {
                        $cell->setValue($record->school);
                    });
                    $sheet->cell('K'.$counter, function($cell)use($record)
                    {
                        $cell->setValue($record->track);
                    });
                    $sheet->cell('L'.$counter, function($cell)use($record)
                    {
                        $cell->setValue($record->studentsemester->where('semester', $maxSemester)->first()->ipk);
                    });
                    $sheet->cell('M'.$counter, function($cell)use($record)
                    {
                        $cell->setValue($record->campus);
                    });
                    $counter++;
                }
            });
        })->download('csv');
    }

    //for insert student
    public function insertDepartment(Request $request)
    {
        $messages =
            [
                'txtNIM.unique' => 'This Student already registered!'
            ];

        $rules =
            [
                'txtNIM' => 'required|unique:students,studentid'
            ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails())
        {
            return redirect('/manageStudent')->withErrors($validator)->withInput();
        }
        else
        {
            $degree = Degree::where('name', '=', $request->txtDegree)->first();
            if($degree == null){
                return redirect('manageStudent')->with('err', "Invalid Program Input, Program must be in valid format !");
            }

            $school = $request->txtSchool;
            if($school != "School of Computer Science"){
                return redirect('manageStudent')->with('err', "Invalid School Input, School must be School of Computer Science !");
            }

            $valid_major = ['-', 'Global Class', 'Database', 'Multimedia', 'AI', 'Network', 'Software Engineering', 'Applied Database', 'Applied Network'];
            if(array_search($request->txtMajor, $valid_major, TRUE) === FALSE){
                return redirect('manageStudent')->with('err', "Invalid Major Input !");
            }

            //mm/dd/yyyy
            $date = date('Y-m-d', strtotime($request->txtDOB));
            $dobPass = str_replace("-","",substr($date,8,2).substr($date,5,2).substr($date,0,4)); //DDMMYYYY
            $user = new User;
            $user->userid =  $request->txtNIM;
            $user->role =  'student';
            $user->defaultpassword =  $dobPass;
            $user->password =  bcrypt($dobPass);
            $user->save();

            $stud = new Student;
            $stud->studentid = $request->txtNIM;
            $stud->name = $request->txtName;
            $stud->dob = $date;
            $stud->school = $request->txtSchool;
            $stud->degreeid = $degree->id;
            $stud->major = $request->txtMajor;
            $stud->save();
            return redirect('manageStudent')->with('fyi', "Inserted");
        }
    }

    // for delete student
    public function deleteDepartment($request)
    {
        //Delete every student information
        Cvdateinformation::where('studentid', $request)->delete();
        Cvskillinformation::where('studentid', $request)->delete();
        Studentscore::where('studentid', $request)->delete();
        Studentsemester::where('studentid', $request)->delete();
        $rec = Recruitment::where('studentid', $request)->get();
        foreach($rec as $r){
            // delete approval_request dan unaccept_request
            $r->approval_request()->delete();
            $r->unaccept_request()->delete();
            $r->unreject_request()->delete();

            // delete interview
            $r->interview()->delete();

            $r->unapplied_by = 'system';
            $r->unapplied_on = Carbon::now();
            $r->save();
            $r->delete();
        }
        Student::filterCampus()->filterInternship()->where('studentid', $request)->delete();
        User::where('userid', $request)->delete();

        return redirect('manageStudent')->with('fyi', "Berhasil");
    }

    // for sacked students
    public function finishDepartment($request) {
        $recruitments = Recruitment::where('studentid', $request)->where('status', 'approved')->get();

        if (!$recruitments) redirect("manageStudent", 'fyi', "This student is not joining any company at the moment.");

        foreach ($recruitments as $recruitment) {
            $recruitment->status = "cancelled";
            $recruitment->save();
        }

        return redirect("manageStudent")->with('fyi', "You have successfully allowed the student to reapply to new companies.");
    }

    // submit excel for inserting new student data
    // routes: /uploadDepartment
    public function uploadDepartment(Request $request)
    {
        $allowed_degree_id = DepartmentController::get_allowed_degree_list();

        //validasi dasar validator
        $messages =
            [
                'fileexcel.required' => 'File Not Found!'
            ];

        $rules = array
        (
            'fileexcel' => 'required'
        );

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails())
        {
            return redirect('/manageStudent')->withErrors($validator)->withInput();
        }
        else
        {
            //load excel
            $arrList = array();
            $arr_nim = [];
            Excel::load($request['fileexcel'], function ($reader) use (&$arrList, &$arr_nim, $allowed_degree_id)
            {
                //loop setiap baris
                foreach ($reader->toArray() as $row)
                {
                    //data dummy di skip
                    if(strcmp($row['nim'],'17XXXXXXXXX')==0)
                    {
                        continue;
                    }
                    else
                    {
                        //check untuk nim yang udah ada di db
                        //di push ke array buat pesan error
                        $student = Student::filterCampus()->filterInternship()->where('studentid', $row['nim'])->first();

                        if($student != null)
                        {
                            Array_push($arrList, $row['nim'].' - '.$row['name']);
                        }
                        else
                        {
                            //manipulate DOB to specified format in SQL
                            //1995-08-06 00:00:00.000000 => From Excel

                            //$dob = $row['dob'];
                            $dob = substr($row['dob'],0,10); //YYYY-MM-DD
                            $dobPass =
                                str_replace("-","",substr($dob,8,2).substr($dob,5,2).substr($dob,0,4)); //DDMMYYYY

                            $degree = Degree::where('name', '=', $row['program'])->first();
                            if($degree == null || array_search($degree->id, $allowed_degree_id) === FALSE){
                                array_push($arr_nim, (string)(int)$row['nim']);
                            }
                            else{
                                //insert ke user dan student
                                $user = new User;
                                $user->userid =  $row['nim'];
                                $user->role =  'student';
                                $user->defaultpassword =  $dobPass;
                                $user->password =  bcrypt($dobPass);
                                $user->save();

                                $stud = new Student;
                                $stud->studentid =$row['nim'];
                                $stud->name =$row['name'];
                                $stud->dob =$dob;
                                $stud->school =$row['school'];
                                $stud->degreeid = $degree->id;
                                $stud->major =$row['major'];
                                $stud->save();
                            }
                        }
                    }
                }
            });
            if(count($arrList) > 0){
                return redirect('/manageStudent')->with('arrSame',$arrList);
            }
            if(count($arr_nim) > 0){//create error message with all nim who is not saved
                $message = 'Invalid Program Input, Students with NIM: ';
                for($i = 0 ; $i < count($arr_nim); $i++){
                    $message = $message.$arr_nim[$i];
                    if($i < count($arr_nim)-1) $message = $message.', ';
                }
                $message = $message.' are not saved';
                return redirect('/manageStudent')->with('err', $message);
            }
            else{
                return redirect('/manageStudent');
            }
        }
    }

    // Manage > University Supervisor
    // routes: /manageguider
    // return: University Supervisor page
    public function universitySupervisorDepartment(Request $request)
    {
        $univ_supervisor_list = UniversitySupervisor::get();

        return view('department.manageguider',
            [
                'univ_supervisor_list' => $univ_supervisor_list
            ]);
    }

    // submit guider data using excel file
    // routes: /submitGuiderByExcel
    public function uploadUniversitySupervisorDepartment(Request $request)
    {
        $messages =
            [
                'fileexcel.required' => 'File Not Found!'
            ];

        $rules = array
        (
            'fileexcel' => 'required'
        );

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails())
        {
            return redirect('/manageguider')
                ->withErrors($validator)
                ->withInput();
        }
        else
        {
            Excel::load($request['fileexcel'], function ($reader)
            {
                foreach ($reader->toArray() as $row)
                {
                    if($row['lecturer_code'] == null || $row['name']=="" || $row['email'] == null || $row['phone']=="")
                    {
                        continue;
                    }

                    $univ_spv = UniversitySupervisor::where('lecturerid', '=', $row['lecturer_code'])->first();

                    if($univ_spv == null){
                        $u = new UniversitySupervisor;
                        $u->lecturerid = $row['lecturer_code'];
                        $u->name = $row['name'];
                        $u->email = $row['email'];
                        $u->phone = $row['phone'];
                        $u->save();
                    }
                }
            });
            return redirect('/manageguider')->with('fyi','Guider successfully assigned!');
        }
    }

    // Manage > Supervisor
    // delete university supervisor
    public function deleteUniversitySupervisorDepartment($univ_spv_id){
        $univ_spv = UniversitySupervisor::where('id', '=', $univ_spv_id)->first();

        if($univ_spv == null){
            return redirect('manageguider')->with('fyi', 'Error Delete: University Supervisor not found');
        }
        else{
            $students = Student::filterCampus()->filterInternship()->where('univ_supervisor_id', '=', $univ_spv->id)->get();

            foreach($students as $s){
                $s->univ_supervisor_id = null;
                $s->save();
            }

            $univ_spv->delete();

            return redirect('manageguider')->with('fyi', 'Success Delete');
        }
    }

    // siapin data untuk ditampilin di halaman profile student punya sendiri
    public function profileStudent()
    {
        $user = Auth::user();

        $userProfile = User::join('students', 'users.userid', '=', 'students.studentid')
            ->whereNull('students.deleted_at')
            ->where('students.studentid', '=', $user->userid)
            ->first();

        $userDegree = null;
        if(!empty($userProfile->degreeid)){
            $userDegree = Degree::where('id', '=', $userProfile->degreeid)
                ->first();
        }

        $ScoreSemester = Studentsemester::where('studentid', '=', $user->userid)
            ->orderBy('semester', 'asc')
            ->get();

        $ScoreSubject = Studentscore::where('studentid', '=', $user->userid)
            ->get();

        //ambil semua data cv yang dates + skills
        $listFormal = Cvdateinformation::where('studentid', '=', $user->userid)
            ->where('type','=','Formal')
            ->get();

        $listInformal = Cvdateinformation::where('studentid', '=', $user->userid)
            ->where('type','=','Informal')
            ->get();

        $listOrg = Cvdateinformation::where('studentid', '=', $user->userid)
            ->where('type','=','Organization')
            ->get();

        $listAward = Cvdateinformation::where('studentid', '=', $user->userid)
            ->where('type','=','Award')
            ->get();

        $listSkill = Cvskillinformation::where('studentid', '=', $user->userid)
            ->where('type','=','Skills')
            ->get();

        $listLang = Cvskillinformation::where('studentid', '=', $user->userid)
            ->where('type','=','Languages')
            ->get();

        $mailboxCount = Mailbox::whereIn('type', array('interview', 'mailbox'))
            ->where('status', '=', 'unseen')
            ->where('recipientid', '=', Auth::user()->userid)
            ->count();

        return view("student.profile",
            [
                'accepted' => $this->checkAccepted(),
                'userProfile' => $userProfile,
                'userDegree' => $userDegree,
                'semester'=>$ScoreSemester,
                'subject'=>$ScoreSubject,
                'formal' => $listFormal,
                'informal' => $listInformal,
                'org' => $listOrg,
                'skill' => $listSkill,
                'lang' => $listLang,
                'award' => $listAward,
                'mailboxCount' => $mailboxCount,
                'notifCount' => app('App\Http\Controllers\MailboxController')->getCountNotification(),
                'notif' => app('App\Http\Controllers\MailboxController')->getLatestNotification()
            ]);
    }

    //buka halaman change profile
    public function editProfileStudent()
    {
        $id = Auth::user()->userid;

        $userProfile = User::join('students', 'users.userid', '=', 'students.studentid')
            ->whereNull('students.deleted_at')
            ->where('students.studentid', '=', $id)
            ->first();

        $semester['list']  = Studentsemester::where('studentid', '=', $id)
            ->orderBy('semester', 'asc')
            ->get();
        $semester['countsemester'] = sizeof($semester['list']);

        $cvdate = new Cvdateinformation;
        $cvskill = new Cvskillinformation;

        $userProfile->formal = Cvdateinformation::where('studentid', '=', $id)
            ->where('type','=','Formal')
            ->get();

        $userProfile->informal = Cvdateinformation::where('studentid', '=', $id)
            ->where('type','=','Informal')
            ->get();

        $userProfile->organization = Cvdateinformation::where('studentid', '=', $id)
            ->where('type','=','Organization')
            ->get();

        $userProfile->award = Cvdateinformation::where('studentid', '=', $id)
            ->where('type','=','Award')
            ->get();

        $userProfile->skill = Cvskillinformation::where('studentid', '=', $id)
            ->where('type','=','Skills')
            ->get();

        $userProfile->language = Cvskillinformation::where('studentid', '=', $id)
            ->where('type','=','Languages')
            ->get();

        $userProfile->countformal = sizeof($userProfile->formal);
        $userProfile->countinformal = sizeof($userProfile->informal);
        $userProfile->countorganization = sizeof($userProfile->organization);
        $userProfile->countaward = sizeof($userProfile->award);
        $userProfile->countskill = sizeof($userProfile->skill);
        $userProfile->countlanguage = sizeof($userProfile->language);

        $mailboxCount = Mailbox::whereIn('type', array('interview', 'mailbox'))
            ->where('status', '=', 'unseen')
            ->where('recipientid', '=', Auth::user()->userid)
            ->count();

        return view("student.editprofile",
            [
                'accepted' => $this->checkAccepted(),
                'userProfile' => $userProfile,
                'semester'=>$semester,
                'mailboxCount' => $mailboxCount,
                'notifCount' => app('App\Http\Controllers\MailboxController')->getCountNotification(),
                'notif' => app('App\Http\Controllers\MailboxController')->getLatestNotification()
            ]);
    }

    //buka halaman change profile
    public function editProfileStudentByDepartment($id)
    {
        $userProfile = User::join('students', 'users.userid', '=', 'students.studentid')
            ->join('degrees', 'degrees.id', '=', 'students.degreeid')
            ->whereNull('students.deleted_at')
            ->whereNull('degrees.deleted_at')
            ->where('students.studentid', '=', $id)
            ->select('students.*', 'degrees.name as degree_name')
            ->first();
        $semester['list']  = Studentsemester::where('studentid', '=', $id)
            ->orderBy('semester', 'asc')
            ->get();
        $semester['countsemester'] = sizeof($semester['list']);
        $cvdate = new Cvdateinformation;
        $cvskill = new Cvskillinformation;
        $userProfile->formal = Cvdateinformation::where('studentid', '=', $id)->where('type','=','Formal')->get();
        $userProfile->informal = Cvdateinformation::where('studentid', '=', $id)->where('type','=','Informal')->get();
        $userProfile->organization = Cvdateinformation::where('studentid', '=', $id)->where('type','=','Organization')->get();
        $userProfile->award = Cvdateinformation::where('studentid', '=', $id)->where('type','=','Award')->get();
        $userProfile->skill = Cvskillinformation::where('studentid', '=', $id)->where('type','=','Skills')->get();
        $userProfile->language = Cvskillinformation::where('studentid', '=', $id)->where('type','=','Languages')->get();
        $userProfile->countformal = sizeof($userProfile->formal);
        $userProfile->countinformal = sizeof($userProfile->informal);
        $userProfile->countorganization = sizeof($userProfile->organization);
        $userProfile->countaward = sizeof($userProfile->award);
        $userProfile->countskill = sizeof($userProfile->skill);
        $userProfile->countlanguage = sizeof($userProfile->language);

        $mailboxCount = Mailbox::whereIn('type', array('interview', 'mailbox'))
            ->where('status', '=', 'unseen')
            ->where('recipientid', '=', Auth::user()->userid)
            ->count();

        return view("student.editprofile",
            [
                'accepted' => $this->checkAccepted(),
                'userProfile' => $userProfile,
                'semester'=>$semester,
                'mailboxCount' => $mailboxCount,
                'notifCount' => app('App\Http\Controllers\MailboxController')->getCountNotification(),
                'notif' => app('App\Http\Controllers\MailboxController')->getLatestNotification(),
                'degrees' => Degree::all()
            ]);
    }

    public function updateEditProfileStudentByDepartment(Request $request){
        //validasi dasar validator
        $id = $request->input('studentid');
        $user = User::where('userid', '=', $id)->first();
        if($user == null){
            return redirect('/homed');
        }
        $messages =
            [
                'name.required' => 'Name must be Filled!',
                'email.required' => 'Email must be Filled!',
                'phone.required' => 'Phone must be Filled!',
                'semester.required' => 'Semester must be Filled!',
                'address.required' => 'Address must be Filled!',
                'dob.required' => 'Birth Date must be Filled!',
                'gender.required' => 'Gender must be Filled!',
                'profpic.max' => 'Profile picture must not be greater than 2 Megabytes'
            ];

        $rules =
            [
                'name' => 'required|max:100',
                'email' => 'required|max:150',
                'phone' => 'required|max:15',
                'address' => 'max:120',
                'facebook'   => 'max:150',
                'linkedin'   => 'max:150',
                'line'   => 'max:100',
                'link' => 'max:150',
                'profpic' => 'image|max:2000',
                'program' => 'required',
                'interest' => 'required'
            ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails())
        {
            return redirect('/updateprofilestudent/'.$id)->withErrors($validator)->withInput();
        }
        else
        {
            //validation program
            $degree = Degree::where('name', '=', $request->program)->first();
            if($degree == null){
                return redirect('/updateprofilestudent/'.$id)->with('error', 'Program Invalid!')->withInput();
            }

            //validation interest
            $valid_interest = ['-', 'global class', 'database', 'multimedia', 'ai', 'network', 'software engineering', 'applied database', 'applied network'];
            if(array_search(strtolower($request->interest), $valid_interest, TRUE) === FALSE){
                return redirect('/updateprofilestudent/'.$id)->with('error', "Invalid Interest Input !")->withInput();
            }

            //Validation semester
            for($i =1;$i<=$request->countersemester;$i++)
            {
                $ips = $request['ips'.$i];
                $ipk = $request['ipk'.$i];
                if($ips <= 0 || $ipk <= 0)
                {
                    return redirect('/updateprofilestudent/'.$id)->with('error', 'Semester IPS/IPK Must be Filled!')->withInput();
                }
            }

            //Validation Formal
            for($i =1;$i<=$request->counter1;$i++)
            {
                $desc = $request['FormalDesc'.$i];
                $start = $request['FormalStart'.$i];
                $end =  $request['FormalEnd'.$i];

                if(strlen($start) < 1 || strlen($start) > 50 || strlen($end) < 1 || strlen($end) > 50){
                    return redirect('/updateprofilestudent/'.$id)
                        ->with('error', 'Date must be between 1-50 characters!')
                        ->withInput();
                }
                else if(strlen($desc) > 200)
                {
                    return redirect('/updateprofilestudent/'.$id)
                        ->with('error', 'Maximum Formal Education Description is 200 Characters!')
                        ->withInput();
                }
                else if(strlen($desc) < 1 || strlen($start) < 1 || strlen($end) < 1)
                {
                    return redirect('/updateprofilestudent/'.$id)
                        ->with('error', 'Formal Education field must be filled!')
                        ->withInput();
                }
            }

            //Validation Informal
            for($i =1;$i<=$request->counter2;$i++)
            {
                $desc = $request['InformalDesc'.$i];
                $start = $request['InformalStart'.$i];
                $end = $request['InformalEnd'.$i];

                if(strlen($start) < 1 || strlen($start) > 50 || strlen($end) < 1 || strlen($end) > 50){
                    return redirect('/updateprofilestudent/'.$id)
                        ->with('error', 'Date must be between 1-50 characters!')
                        ->withInput();
                }
                else if(strlen($desc) > 200)
                {
                    return redirect('/updateprofilestudent/'.$id)
                        ->with('error', 'Maximal Informal Education Description is 200 Characters!')
                        ->withInput();
                }
                else if(strlen($desc) < 1 || strlen($start) < 1 || strlen($end) < 1)
                {
                    return redirect('/updateprofilestudent/'.$id)
                        ->with('error', 'Informal Education field must be filled!')
                        ->withInput();
                }
            }

            //Validation Org
            for($i =1;$i<=$request->counter3;$i++)
            {
                $start = $request['OrgStart'.$i];
                $end = $request['OrgEnd'.$i];
                $name = $request['OrgName'.$i];
                $role = $request['OrgRole'.$i];
                $job = $request['OrgJobdesc'.$i];

                if(strlen($start) < 1 || strlen($start) > 50 || strlen($end) < 1 || strlen($end) > 50){
                    return redirect('/updateprofilestudent/'.$id)
                        ->with('error', 'Date must be between 1-50 characters!')
                        ->withInput();
                }
                else if(strlen($name) < 1 || strlen($role) < 1 || strlen($job) < 1)
                {
                    return redirect('/updateprofilestudent/'.$id)
                        ->with('error', 'Organizational & Work field must be filled!')
                        ->withInput();
                }
                else if(strlen($name) > 100 || strlen($role) > 100)
                {
                    return redirect('/updateprofilestudent/'.$id)
                        ->with('error', 'Maximum length of Organizational & Work Institution Name and Role is 100 Characters!')
                        ->withInput();
                }
                else if(strlen($job) > 1000)
                {
                    return redirect('/updateprofilestudent/'.$id)
                        ->with('error', 'Maximum length of Job Description is 1000 characters')
                        ->withInput();
                }
            }

            //Validation Achievement
            for($i =1;$i<=$request->counter6;$i++)
            {
                $date = $request['AcvDate'.$i];
                $desc = $request['AcvDesc'.$i];

                if(strlen($date) < 1 || strlen($date) > 50){
                    return redirect('/updateprofilestudent/'.$id)
                        ->with('error', 'Date must be between 1-50 characters!')
                        ->withInput();
                }
                else if(strlen($date) < 1 || strlen($desc) < 1)
                {
                    return redirect('/updateprofilestudent/'.$id)
                        ->with('error', 'Ahievement field must be filled!')
                        ->withInput();
                }
                else if(strlen($desc) > 200)
                {
                    return redirect('/updateprofilestudent/'.$id)
                        ->with('error', 'Maximum Achievement description 200 Characters!')
                        ->withInput();
                }
            }

            //Validation Computer Skill
            if($request->counter4 > 8){
                return redirect('/updateprofilestudent/'.$id)
                    ->with('error', 'You only can input maximum 8 Computer / Special Skill(s)!')
                    ->withInput();
            }
            for($i =1;$i<=$request->counter4;$i++)
            {
                $name = $request['CompName'.$i];
                $status = $request['CompSkill'.$i];

                if(strlen($name) < 1 || strlen($status) < 1 || $status < 1 || $status > 10)
                {
                    return redirect('/updateprofilestudent/'.$id)
                        ->with('error', 'Computer/Special Skill Field must be filled!')
                        ->withInput();
                }
                else if(strlen($name) > 100)
                {
                    return redirect('/updateprofilestudent/'.$id)
                        ->with('error', 'Maximum Computer/Special Skill name 100 Characters!')
                        ->withInput();
                }
            }

            //Validation languages
            for($i =1;$i<=$request->counter5;$i++)
            {
                $name = $request['LangName'.$i];
                $status  =$request['LangLevel'.$i];

                if(strlen($name) < 1 || strlen($status) < 1)
                {
                    return redirect('/updateprofilestudent/'.$id)
                        ->with('error', 'Languages Skill Field must be filled!')
                        ->withInput();
                }
                else if(strlen($name) > 100)
                {
                    return redirect('/updateprofilestudent/'.$id)
                        ->with('error', 'Maximum Languages Name Field 100 Characters')
                        ->withInput();
                }
                else if(strlen($status) > 20)
                {
                    return redirect('/updateprofilestudent/'.$id)
                        ->with('error', 'Maximum Languages Skill Field 20 Characters')
                        ->withInput();
                }
            }

            $errorExcel = null;
            //If Grade is Submitted
//    		if(isset($request['fileexcel']))
//    		{
//    			Studentscore::where('studentid', $id)->delete();
//
//				Excel::load($request['fileexcel'], function ($reader) use (&$errorExcel)
//    			{
//            		foreach ($reader->toArray() as $row)
//            		{
//            			if(empty($row['code']) || empty($row['name'] || empty($row['grade']))){
//            				$errorExcel = true;
//            				break;
//            			}
//
//            			$score = new Studentscore;
//            			$score->studentid = $id;
//            			$score->code = $row['code'];
//            			$score->name = $row['name'];
//            			$score->grade = $row['grade'];
//          				$score->save();
//            		}
//        		});
//    		}

            if($errorExcel != null){
                Studentscore::where('studentid', '=', $id)->delete();
                return redirect('/updateprofilestudent/'.$id)
                    ->with('error', 'Saved with error. Excel Student score file is not complete.')
                    ->withInput();
            }

            //website: always have http:// or https:// in front
            $link = null;
            if(!empty($request->link) && strpos($request->link, 'http://') === false && strpos($request->link, 'https://') === false){
                $link = 'http://'.$request->link;
            }
            else{
                $link = $request->link;
            }

            //update informasi yang menempel di table student
            Student::where('studentid', $id)->update(
                [
                    'facebook' => $request->facebook,
                    'linkedin' => $request->linkedin,
                    'line' => $request->line,
                    'linkportofolio' => $link,
                    'semester' => $request->semester,
                    'name' => $request->name,
                    'email' => $request->email,
                    'phone' => $request->phone,
                    'address' => $request->address,
                    'dob' => date('Y-m-d', strtotime($request->dob)),
                    'sex' => $request->gender,
//					'degreeid' => $degree->id,
//					'major' => $request->interest
                ]);

            //jika update profpic
            $iniekstensinya = '';
            if(!is_null($request->file('profpic')))
            {
                $destinationPath = storage_path().'/assets/images/'.$id.'/';
                if(!File::exists($destinationPath)){
                    File::makeDirectory($destinationPath, 0777, true, true);
                }
                $extension = $request->file('profpic')->getClientOriginalExtension();
                $fileName = 'photo.'.$extension;
                $iniekstensinya = $extension;
                $request->file('profpic')->move($destinationPath, $fileName);
                Student::where('studentid', $id)->update(
                    [
                        'photo' => $fileName
                    ]);
            }

            //For Semester
            Studentsemester::where('studentid', $id)->delete();
            //$semester->deleteSemesterScores( Auth::user()->userid);
            for($i =1;$i<=$request->countersemester;$i++)
            {
                $semester = new Studentsemester;
                $semester->studentid = $id;
                $semester->ips = $request['ips'.$i];
                $semester->semester = $i;
                $semester->ipk = $request['ipk'.$i];
                $semester->save();
            }

            //For Other Information
            Cvdateinformation::where('studentid', $id)->delete();

            Cvskillinformation::where('studentid', $id)->delete();

            //Insert Formal education
            for($i =1;$i<=$request->counter1;$i++)
            {
                $insertFormal = new Cvdateinformation;
                $insertFormal->studentid = $id;
                $insertFormal->startdate = $request['FormalStart'.$i];
                $insertFormal->enddate = $request['FormalEnd'.$i];
                $insertFormal->description = $request['FormalDesc'.$i];
                $insertFormal->type = 'Formal';
                $insertFormal->save();
            }

            //Insert Informal education
            for($i =1;$i<=$request->counter2;$i++)
            {
                $insertInformal = new Cvdateinformation;
                $insertInformal->studentid = $id;
                $insertInformal->startdate = $request['InformalStart'.$i];
                $insertInformal->enddate = $request['InformalEnd'.$i];
                $insertInformal->description = $request['InformalDesc'.$i];
                $insertInformal->type = 'Informal';
                $insertInformal->save();
            }

            //Insert Softskill
            for($i =1;$i<=6;$i++) {
                if ($request['softSkill' . $i] === null) {
                    continue;
                } else {
                    $soft = new Cvskillinformation();
                    $soft->studentid = Auth::user()->userid;
                    $soft->status = 'Soft Skills';
                    $soft->name = $request['softSkill' . $i];
                    $soft->type = 'Skills';
                    $soft->save();
                }
            }

            //Insert Organizational
            for($i =1;$i<=$request->counter3;$i++)
            {
                $insertOrganization = new Cvdateinformation;
                $insertOrganization->studentid = $id;
                $insertOrganization->startdate = $request['OrgStart'.$i];
                $insertOrganization->enddate = $request['OrgEnd'.$i];
                $insertOrganization->description = $request['OrgName'.$i];
                $insertOrganization->Role = $request['OrgRole'.$i];
                $insertOrganization->Jobdesc = $request['OrgJobdesc'.$i];
                $insertOrganization->type = 'Organization';
                $insertOrganization->save();
            }

            $date = $request['AcvDate'.$i];
            $desc = $request['AcvDesc'.$i];

            //Insert Achievement
            for($i =1;$i<=$request->counter6;$i++)
            {
                $insertAward = new Cvdateinformation;
                $insertAward->studentid = $id;
                $insertAward->startdate = $request['AcvDate'.$i];
                $insertAward->description = $request['AcvDesc'.$i];
                $insertAward->type = 'Award';
                $insertAward->save();
            }

            //Insert Skills
            for($i =1;$i<=$request->counter4;$i++)
            {
                $insertSkill = new Cvskillinformation;
                $insertSkill->studentid = $id;
                $insertSkill->name = $request['CompName'.$i];
                $insertSkill->status = $request['CompSkill'.$i];
                $insertSkill->type = 'Skills';
                $insertSkill->save();
            }

            //Insert languages
            for($i =1;$i<=$request->counter5;$i++)
            {
                $insertLanguage = new Cvskillinformation;
                $insertLanguage->studentid = $id;
                $insertLanguage->name = $request['LangName'.$i];
                $insertLanguage->status = $request['LangLevel'.$i];
                $insertLanguage->type = 'Languages';
                $insertLanguage->save();
            }

            $field_yang_kosong = Student::where('studentid', $id)->first()->returnEmptyProfileInfo();

            if(count($field_yang_kosong) == 0){
                //panggil function makecv untuk men-generate CV
                Student::filterCampus()->filterInternship()->where('studentid', $id)->update(
                    ['cv' => '1']
                );
                $this->generateCv($id);
            }
            else{
                Student::filterCampus()->filterInternship()->where('studentid', $id)->update(
                    ['cv' => '0']
                );
            }

            return redirect('/profile/'.$id)->with('sukses','Success update profile!');
        }
    }

    //untuk submit hasil edit profile
    public function updateEditProfileStudent(Request $request)
    {
        //validasi dasar validator
        $user = Auth::user();
        $messages =
            [
                'name.required' => 'Name must be Filled!',
                'email.required' => 'Email must be Filled!',
                'phone.required' => 'Phone must be Filled!',
                'semester.required' => 'Semester must be Filled!',
                'address.required' => 'Address must be Filled!',
                'dob.required' => 'Birth Date must be Filled!',
                'gender.required' => 'Gender must be Filled!',
                'profpic.max' => 'Profile picture must not be greater than 2 Megabytes'
            ];

        $rules =
            [
                'name' => 'required|max:100',
                'email' => 'required|max:150',
                'phone' => 'required|max:15',
                'address' => 'required|max:120',
                'dob' => 'required',
                'semester' => 'required',
                'gender' => 'required',
                'facebook'   => 'max:150',
                'linkedin'   => 'max:150',
                'line'   => 'max:100',
                'link' => 'max:150',
                'profpic' => 'image|max:2000'
            ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails())
            return redirect('/updateprofile')->withErrors($validator)->withInput();
        else
        {
            //Validation semester
            for($i =1;$i<=$request->countersemester;$i++)
            {
                $ips = $request['ips'.$i];
                $ipk = $request['ipk'.$i];
                if($ips <= 0 || $ipk <= 0)
                    return redirect('/updateprofile')->with('error', 'Semester IPS/IPK Must be Filled!')->withInput();
            }

            //Validation Formal
            for($i =1;$i<=$request->counter1;$i++)
            {
                $desc = $request['FormalDesc'.$i];
                $start = $request['FormalStart'.$i];
                $end =  $request['FormalEnd'.$i];

                if(strlen($start) < 1 || strlen($start) > 50 || strlen($end) < 1 || strlen($end) > 50){
                    return redirect('/updateprofile')
                        ->with('error', 'Date must be between 1-50 characters!')
                        ->withInput();
                }
                else if(strlen($desc) > 200)
                {
                    return redirect('/updateprofile')
                        ->with('error', 'Maximum Formal Education Description is 200 Characters!')
                        ->withInput();
                }
                else if(strlen($desc) < 1 || strlen($start) < 1 || strlen($end) < 1)
                {
                    return redirect('/updateprofile')
                        ->with('error', 'Formal Education field must be filled!')
                        ->withInput();
                }
            }

            //Validation Informal
            for($i =1;$i<=$request->counter2;$i++)
            {
                $desc = $request['InformalDesc'.$i];
                $start = $request['InformalStart'.$i];
                $end = $request['InformalEnd'.$i];

                if(strlen($start) < 1 || strlen($start) > 50 || strlen($end) < 1 || strlen($end) > 50){
                    return redirect('/updateprofile')
                        ->with('error', 'Date must be between 1-50 characters!')
                        ->withInput();
                }
                else if(strlen($desc) > 200)
                {
                    return redirect('/updateprofile')
                        ->with('error', 'Maximal Informal Education Description is 200 Characters!')
                        ->withInput();
                }
                else if(strlen($desc) < 1 || strlen($start) < 1 || strlen($end) < 1)
                {
                    return redirect('/updateprofile')
                        ->with('error', 'Informal Education field must be filled!')
                        ->withInput();
                }
            }

            //Validation Org
            for($i =1;$i<=$request->counter3;$i++)
            {
                $start = $request['OrgStart'.$i];
                $end = $request['OrgEnd'.$i];
                $name = $request['OrgName'.$i];
                $role = $request['OrgRole'.$i];
                $job = $request['OrgJobdesc'.$i];

                if(strlen($start) < 1 || strlen($start) > 50 || strlen($end) < 1 || strlen($end) > 50){
                    return redirect('/updateprofile')
                        ->with('error', 'Date must be between 1-50 characters!')
                        ->withInput();
                }
                else if(strlen($name) < 1 || strlen($role) < 1 || strlen($job) < 1)
                {
                    return redirect('/updateprofile')
                        ->with('error', 'Organizational & Work field must be filled!')
                        ->withInput();
                }
                else if(strlen($name) > 100 || strlen($role) > 100)
                {
                    return redirect('/updateprofile')
                        ->with('error', 'Maximum length of Organizational & Work Institution Name and Role is 100 Characters!')
                        ->withInput();
                }
                else if(strlen($job) > 1000)
                {
                    return redirect('/updateprofile')
                        ->with('error', 'Maximum length of Job Description is 1000 characters')
                        ->withInput();
                }
            }

            //Validation Achievement
            for($i =1;$i<=$request->counter6;$i++)
            {
                $date = $request['AcvDate'.$i];
                $desc = $request['AcvDesc'.$i];

                if(strlen($date) < 1 || strlen($date) > 50){
                    return redirect('/updateprofile')
                        ->with('error', 'Date must be between 1-50 characters!')
                        ->withInput();
                }
                else if(strlen($date) < 1 || strlen($desc) < 1)
                {
                    return redirect('/updateprofile')
                        ->with('error', 'Ahievement field must be filled!')
                        ->withInput();
                }
                else if(strlen($desc) > 200)
                {
                    return redirect('/updateprofile')
                        ->with('error', 'Maximum Achievement description 200 Characters!')
                        ->withInput();
                }
            }

            //Validation Computer Skill
            if($request->counter4 > 8){
                return redirect('/updateprofile')
                    ->with('error', 'You only can input maximum 8 Computer / Special Skill(s)!')
                    ->withInput();
            }

            for($i =1;$i<=$request->counter4;$i++)
            {
                $name = $request['CompName'.$i];
                $status = $request['CompSkill'.$i];

                if(strlen($name) < 1 || strlen($status) < 1 || $status < 1 || $status > 10)
                {
                    return redirect('/updateprofile')
                        ->with('error', 'Computer/Special Skill Field must be filled!')
                        ->withInput();
                }
                else if(strlen($name) > 100)
                {
                    return redirect('/updateprofile')
                        ->with('error', 'Maximum Computer/Special Skill name 100 Characters!')
                        ->withInput();
                }
            }

            //Validation Soft Skill
            $softCounter = 0;
            for($i =1;$i<=6;$i++){
                $softSkill = $request['softSkill'.$i];
                if($softSkill !== null){
                    $softCounter++;
                }
            }
            if($softCounter === 0 ){
                return redirect('/updateprofile')
                    ->with('error', 'Minimum 1 softskill selected')
                    ->withInput();
            }

            //Validation languages
            /**
             * languages not mandatory
             */
            if($request->counter5 > 0)
                for($i =1;$i<=$request->counter5;$i++)
                {
                    $name = $request['LangName'.$i];
                    $status  =$request['LangLevel'.$i];

                    if(strlen($name) < 1 || strlen($status) < 1)
                    {
                        return redirect('/updateprofile')
                            ->with('error', 'Languages Skill Field must be filled!')
                            ->withInput();
                    }
                    else if(strlen($name) > 100)
                    {
                        return redirect('/updateprofile')
                            ->with('error', 'Maximum Languages Name Field 100 Characters')
                            ->withInput();
                    }
                    else if(strlen($status) > 20)
                    {
                        return redirect('/updateprofile')
                            ->with('error', 'Maximum Languages Skill Field 20 Characters')
                            ->withInput();
                    }
                }

            $errorExcel = null;
//    		//If Grade is Submitted
//    		if(isset($request['fileexcel']))
//    		{
//    			Studentscore::where('studentid', Auth::user()->userid)->delete();
//
//				Excel::load($request['fileexcel'], function ($reader) use (&$errorExcel)
//    			{
//            		foreach ($reader->toArray() as $row)
//            		{
//            			if(empty($row['code']) || empty($row['name'] || empty($row['grade']))){
//            				$errorExcel = true;
//            				break;
//            			}
//
//            			$score = new Studentscore;
//            			$score->studentid = Auth::user()->userid;
//            			$score->code = $row['code'];
//            			$score->name = $row['name'];
//            			$score->grade = $row['grade'];
//          				$score->save();
//            		}
//        		});
//    		}

//    		if($errorExcel != null){
//    			Studentscore::where('studentid', '=', Auth::user()->userid)->delete();
//    			return redirect('/updateprofile')
//					->with('error', 'Saved with error. Excel Student score file is not complete.')
//					->withInput();
//    		}

            //website: always have http:// or https:// in front
            $link = null;
            if(!empty($request->link) && strpos($request->link, 'http://') === false && strpos($request->link, 'https://') === false){
                $link = 'http://'.$request->link;
            }
            else{
                $link = $request->link;
            }

            //update informasi yang menempel di table student
            Student::where('studentid', Auth::user()->userid)->update(
                [
                    'facebook' => $request->facebook,
                    'linkedin' => $request->linkedin,
                    'line' => $request->line,
                    'linkportofolio' => $link,
                    'semester' => $request->semester,
                    'name' => $request->name,
                    'email' => $request->email,
                    'phone' => $request->phone,
                    'address' => $request->address,
                    'dob' => date('Y-m-d', strtotime($request->dob)),
                    'sex' => $request->gender
                ]);

            //jika update profpic
            $iniekstensinya = '';
            if(!is_null($request->file('profpic')))
            {
                $destinationPath = storage_path().'/assets/images/'.Auth::user()->userid.'/';
                if(!File::exists($destinationPath)){
                    File::makeDirectory($destinationPath, 0775, true, true);
                }
                $extension = $request->file('profpic')->getClientOriginalExtension();
                $fileName = 'photo.'.$extension;
                $iniekstensinya = $extension;
                $request->file('profpic')->move($destinationPath, $fileName);
                Student::where('studentid', Auth::user()->userid)->update(
                    [
                        'photo' => $fileName
                    ]);
            }

//    		//For Semester
//    		Studentsemester::where('studentid', Auth::user()->userid)->delete();
//        	for($i =1;$i<=$request->countersemester;$i++)
//        	{
//	        	$semester = new Studentsemester;
//	        	$semester->studentid = Auth::user()->userid;
//	        	$semester->ips = $request['ips'.$i];
//	        	$semester->semester = $i;
//	        	$semester->ipk = $request['ipk'.$i];
//	        	$semester->save();
//        	}

            //For Other Information
            Cvdateinformation::where('studentid', Auth::user()->userid)->delete();

            Cvskillinformation::where('studentid', Auth::user()->userid)->delete();

            //Insert Formal education
            for($i =1;$i<=$request->counter1;$i++)
            {
                $insertFormal = new Cvdateinformation;
                $insertFormal->studentid = Auth::user()->userid;
                $insertFormal->startdate = $request['FormalStart'.$i];
                $insertFormal->enddate = $request['FormalEnd'.$i];
                $insertFormal->description = $request['FormalDesc'.$i];
                $insertFormal->type = 'Formal';
                $insertFormal->save();
            }

            //Insert Informal education
            for($i =1;$i<=$request->counter2;$i++)
            {
                $insertInformal = new Cvdateinformation;
                $insertInformal->studentid = Auth::user()->userid;
                $insertInformal->startdate = $request['InformalStart'.$i];
                $insertInformal->enddate = $request['InformalEnd'.$i];
                $insertInformal->description = $request['InformalDesc'.$i];
                $insertInformal->type = 'Informal';
                $insertInformal->save();
            }

            //Insert Organizational
            for($i =1;$i<=$request->counter3;$i++)
            {
                $insertOrganization = new Cvdateinformation;
                $insertOrganization->studentid = Auth::user()->userid;
                $insertOrganization->startdate = $request['OrgStart'.$i];
                $insertOrganization->enddate = $request['OrgEnd'.$i];
                $insertOrganization->description = $request['OrgName'.$i];
                $insertOrganization->Role = $request['OrgRole'.$i];
                $insertOrganization->Jobdesc = $request['OrgJobdesc'.$i];
                $insertOrganization->type = 'Organization';
                $insertOrganization->save();
            }

            $date = $request['AcvDate'.$i];
            $desc = $request['AcvDesc'.$i];

            //Insert Achievement
            for($i =1;$i<=$request->counter6;$i++)
            {
                $insertAward = new Cvdateinformation;
                $insertAward->studentid = Auth::user()->userid;
                $insertAward->startdate = $request['AcvDate'.$i];
                $insertAward->description = $request['AcvDesc'.$i];
                $insertAward->type = 'Award';
                $insertAward->save();
            }

            //Insert Skills
            for($i =1;$i<=$request->counter4;$i++)
            {
                $insertSkill = new Cvskillinformation;
                $insertSkill->studentid = Auth::user()->userid;
                $insertSkill->name = $request['CompName'.$i];
                $insertSkill->status = $request['CompSkill'.$i];
                $insertSkill->type = 'Skills';
                $insertSkill->save();
            }

            //Insert languages
            /**
             * insert languages not mandatory
             */
            if ($request->counter5 > 0 ) {
                for($i =1;$i<=$request->counter5;$i++)
                {
                    $insertLanguage = new Cvskillinformation;
                    $insertLanguage->studentid = Auth::user()->userid;
                    $insertLanguage->name = $request['LangName'.$i];
                    $insertLanguage->status = $request['LangLevel'.$i];
                    $insertLanguage->type = 'Languages';
                    $insertLanguage->save();
                }
            }
            $field_yang_kosong = Student::where('studentid', Auth::user()->userid)->first()->returnEmptyProfileInfo();
            //Insert Soft Skill do re mi fa sol
            for($i =1;$i<=6;$i++) {
                if ($request['softSkill' . $i] === null) {
                    continue;
                } else {
                    $soft = new Cvskillinformation();
                    $soft->studentid = Auth::user()->userid;
                    $soft->status = 'Soft Skills';
                    $soft->name = $request['softSkill' . $i];
                    $soft->type = 'Soft Skills';
                    $soft->save();
                }
            }

            $stud = Student::where('studentid', Auth::user()->userid)->first();
            $field_yang_kosong = $stud->returnEmptyProfileInfo();

            if(count($field_yang_kosong) == 0){
                //panggil function makecv untuk men-generate CV
                Student::where('studentid', Auth::user()->userid)->update(
                    ['cv' => '1']
                );
                $this->generateCv(Auth::user()->userid);
            }
            else{
                Student::where('studentid', Auth::user()->userid)->update(
                    ['cv' => '0']
                );
            }

            return redirect('/profile')->with('sukses','Success update your profile!');
        }
    }

    public function generateLearningPlanStudent(){
        $checkDegree = Student::filterCampus()->filterInternship()->where('studentid', '=', Auth::user()->userid)
            ->select('degree')
            ->first();
        $studentProfile = Student::filterCampus()->filterInternship()->leftJoin('supervisors', 'supervisors.id', '=', 'students.supervisorid')
            ->join('degrees', 'degrees.id', '=', 'students.degreeid')
            ->whereNull('supervisors.deleted_at')
            ->whereNull('degrees.deleted_at')
            ->where('studentid', Auth::user()->userid)
            ->select('students.studentid as nim', 'students.name as studentName', 'students.email as studentEmail', 'students.phone as studentPhone', 'students.address as studentAddress', 'degrees.name as studentDegree', 'students.school as studentSchool', 'students.semester as studentSemester', 'supervisors.name as supervisorName', 'supervisors.position as supervisorPosition', 'supervisors.email as supervisorEmail', 'supervisors.phone as supervisorPhone', 'supervisors.address as supervisorAddress', 'students.guider as guider')
            ->first();

        $companyProfile = Recruitment::join('jobs', 'recruitments.jobid', '=', 'jobs.id')
            ->join('companies', 'jobs.companyid', '=', 'companies.companyid')
            ->whereNull('jobs.deleted_at')
            ->whereNull('companies.deleted_at')
            ->where('recruitments.studentid', Auth::user()->userid)
            ->where('recruitments.status', 'approved')
            ->select('companies.name', 'jobs.description', 'jobs.learningobj', 'jobs.softskill')
            ->first();

        $date_now = date("d F Y");

        if (strpos(strtolower($checkDegree), 'computer science') !== false){
            $url = public_path().'\\assets\\LP-CompScience-S6-S7-B2017.docx';
        }
        else if (strpos(strtolower($checkDegree), 'game application') !== false){
            $url = public_path().'\\assets\\LP-GAT-S6-S7-B2018.docx';
        }
        else if (strpos(strtolower($checkDegree), 'mobile  application') !== false){
            $url = public_path().'\\assets\\LP-MAT-S6-B2017.docx';
        }
        else{
            $url = public_path().'\\assets\\LP-CompScience-S6-S7-B2017.docx';
        }

        $description = preg_replace('~\R~u', '</w:t><w:br/><w:t>', $companyProfile->description);
        $document = new TemplateProcessor($url);
        $fontStyle = new Font();
        $fontStyle->setName('Wingdingss 2');
        $fontStyle->setSize(18);
        $tc1='';$tc2='';$tc3='';$tc4='';$tc5='';$tc6='';$tc7='';$tc8='';$tc9='';$tc10='';$tc11='';$tc12='';
        $ss1='';$ss2='';$ss3='';$ss4='';$ss5='';$ss6='';

        $technicalCompetencies = explode(';', $companyProfile->learningobj);
        $softSkill = explode(';', $companyProfile->softskill);

        foreach ($technicalCompetencies as $key) {
            switch ($key) {
                case '1':
                    $tc1=htmlspecialchars_decode('&#10003;');
                    break;
                case '2':
                    $tc2=htmlspecialchars_decode('&#10003;');
                    break;
                case '3':
                    $tc3=htmlspecialchars_decode('&#10003;');
                    break;
                case '4':
                    $tc4=htmlspecialchars_decode('&#10003;');
                    break;
                case '5':
                    $tc5=htmlspecialchars_decode('&#10003;');
                    break;
                case '6':
                    $tc6=htmlspecialchars_decode('&#10003;');
                    break;
                case '7':
                    $tc7=htmlspecialchars_decode('&#10003;');
                    break;
                case '8':
                    $tc8=htmlspecialchars_decode('&#10003;');
                    break;
                case '9':
                    $tc9=htmlspecialchars_decode('&#10003;');
                    break;
                case '10':
                    $tc10=htmlspecialchars_decode('&#10003;');
                    break;
                case '11':
                    $tc11=htmlspecialchars_decode('&#10003;');
                    break;
                case '12':
                    $tc12=htmlspecialchars_decode('&#10003;');
                    break;
            }
        }

        foreach ($softSkill as $key) {
            switch ($key) {
                case '1':
                    $ss1=htmlspecialchars_decode('&#10003;');
                    break;
                case '2':
                    $ss2=htmlspecialchars_decode('&#10003;');
                    break;
                case '3':
                    $ss3=htmlspecialchars_decode('&#10003;');
                    break;
                case '4':
                    $ss4=htmlspecialchars_decode('&#10003;');
                    break;
                case '5':
                    $ss5=htmlspecialchars_decode('&#10003;');
                    break;
                case '6':
                    $ss6=htmlspecialchars_decode('&#10003;');
            }
        }

        $studentNames = explode(' ', $studentProfile->studentName);
        $studentName = '';
        foreach ($studentNames as $key) {
            $studentName .= substr(strtoupper($key), 0, 1).substr(strtolower($key), 1).' ';
        }

        $universitySupervisorBefore = substr( $studentProfile->guider, strpos($studentProfile->guider, '-')+1);
        $guiders = explode(' ', $universitySupervisorBefore);
        $guider = '';
        foreach ($guiders as $key) {
            $guider .= substr(strtoupper($key), 0, 1).substr(strtolower($key), 1).' ';
        }

        $spvnames = explode(' ', $studentProfile->supervisorName);
        $spvname = '';
        foreach ($spvnames as $key) {
            $spvname .= substr(strtoupper($key), 0, 1).substr(strtolower($key), 1).' ';
        }

        $document->setValue(array('name', 'studentid', 'email', 'phone', 'address', 'degree', 'school', 'semester', 'company_name', 'supervisor_name', 'supervisor_position', 'supervisor_email', 'supervisor_phone', 'supervisor_address', 'job_description', 'university_supervisor', 'date_now', 'tc1', 'tc2', 'tc3', 'tc4', 'tc5', 'tc6', 'tc7', 'tc8', 'tc9', 'tc10', 'tc11', 'tc12', 'ss1', 'ss2', 'ss3', 'ss4', 'ss5', 'ss6'), array($studentName, $studentProfile->nim, $studentProfile->studentEmail, $studentProfile->studentPhone, $studentProfile->studentAddress, $studentProfile->studentDegree, $studentProfile->studentSchool, $studentProfile->studentSemester, $companyProfile->name, $spvname, $studentProfile->supervisorPosition, $studentProfile->supervisorEmail, $studentProfile->supervisorPhone, $studentProfile->supervisorAddress, $description, $guider, $date_now, $tc1, $tc2, $tc3, $tc4, $tc5, $tc6, $tc7, $tc8, $tc9, $tc10, $tc11, $tc12, $ss1, $ss2, $ss3, $ss4, $ss5, $ss6));

        $file = '/Learning Plan - '.Auth::user()->userid.'.docx';

        if(!File::exists( public_path('/assets/ELP/').Auth::user()->userid))
        {
            File::makeDirectory(public_path('/assets/ELP/').Auth::user()->userid.'/', 0777, true, true);
        }
        $document->saveAs(public_path('/assets/ELP/').Auth::user()->userid.'/'.$file);
        $contents = public_path('/assets/ELP/').Auth::user()->userid.$file;

        $headers = array(
            'Content-Type: application/msword',
        );

        return Response::download($contents, 'Learning Plan - '.Auth::user()->userid.'.docx', $headers);
    }

    public function generateLearningPlanDepartment($studentid)
    {
        $checkDegree = Student::filterCampus()->filterInternship()->where('studentid', '=', $studentid)->select('degree')->first();

        $studentProfile = Student::filterCampus()->filterInternship()->leftJoin('supervisors', 'supervisors.id', '=', 'students.supervisorid')->join('degrees', 'degrees.id', '=', 'students.degreeid')->where('studentid', $studentid)->select('students.studentid as nim', 'students.name as studentName', 'students.email as studentEmail', 'students.phone as studentPhone', 'students.address as studentAddress', 'degrees.name as studentDegree', 'students.school as studentSchool', 'students.semester as studentSemester', 'supervisors.name as supervisorName', 'supervisors.position as supervisorPosition', 'supervisors.email as supervisorEmail', 'supervisors.phone as supervisorPhone', 'supervisors.address as supervisorAddress', 'students.guider as guider')->first();
        $companyProfile = Recruitment::join('jobs', 'recruitments.jobid', '=', 'jobs.id')->join('companies', 'jobs.companyid', '=', 'companies.companyid')->where('recruitments.studentid', $studentid)->where('recruitments.status', 'approved')->select('companies.name', 'jobs.description', 'learningobj', 'softskill')->first();
        $date_now = date("d F Y");

        if (strpos($checkDegree, 'Computer Science') !== false)
        {
            $url = asset('assets/LP-CompScience-S6-S7-B2017.docx');
        }
        else if (strpos($checkDegree, 'Game Application') !== false)
        {
            $url = asset('assets/LP-GAT-S6-S7-B2018.docx');
        }
        else if (strpos($checkDegree, 'Mobile  Application') !== false)
        {
            $url = asset('assets/LP-MAT-S6-B2017.docx');
        }
        else
        {
            $url = asset('assets/LP-CompScience-S6-S7-B2017.docx');
        }

        $description = preg_replace('~\R~u', '</w:t><w:br/><w:t>', $companyProfile->description);
        $document = new TemplateProcessor($url);
        $fontStyle = new Font();
        $fontStyle->setName('Wingdingss 2');
        $fontStyle->setSize(18);

        $tc1='';$tc2='';$tc3='';$tc4='';$tc5='';$tc6='';$tc7='';$tc8='';$tc9='';$tc10='';$tc11='';$tc12='';
        $ss1='';$ss2='';$ss3='';$ss4='';$ss5='';$ss6='';

        $technicalCompetencies = explode(';', $companyProfile->learningobj);
        $softSkill = explode(';', $companyProfile->softskill);

        foreach ($technicalCompetencies as $key) {
            switch ($key) {
                case '1':
                    $tc1=htmlspecialchars_decode('&#10003;');
                    break;
                case '2':
                    $tc2=htmlspecialchars_decode('&#10003;');
                    break;
                case '3':
                    $tc3=htmlspecialchars_decode('&#10003;');
                    break;
                case '4':
                    $tc4=htmlspecialchars_decode('&#10003;');
                    break;
                case '5':
                    $tc5=htmlspecialchars_decode('&#10003;');
                    break;
                case '6':
                    $tc6=htmlspecialchars_decode('&#10003;');
                    break;
                case '7':
                    $tc7=htmlspecialchars_decode('&#10003;');
                    break;
                case '8':
                    $tc8=htmlspecialchars_decode('&#10003;');
                    break;
                case '9':
                    $tc9=htmlspecialchars_decode('&#10003;');
                    break;
                case '10':
                    $tc10=htmlspecialchars_decode('&#10003;');
                    break;
                case '11':
                    $tc11=htmlspecialchars_decode('&#10003;');
                    break;
                case '12':
                    $tc12=htmlspecialchars_decode('&#10003;');
                    break;
            }
        }

        foreach ($softSkill as $key) {
            switch ($key) {
                case '1':
                    $ss1=htmlspecialchars_decode('&#10003;');
                    break;
                case '2':
                    $ss2=htmlspecialchars_decode('&#10003;');
                    break;
                case '3':
                    $ss3=htmlspecialchars_decode('&#10003;');
                    break;
                case '4':
                    $ss4=htmlspecialchars_decode('&#10003;');
                    break;
                case '5':
                    $ss5=htmlspecialchars_decode('&#10003;');
                    break;
                case '6':
                    $ss6=htmlspecialchars_decode('&#10003;');
            }
        }

        $studentNames = explode(' ', $studentProfile->studentName);
        $studentName = '';
        foreach ($studentNames as $key) {
            $studentName .= substr(strtoupper($key), 0, 1).substr(strtolower($key), 1).' ';
        }

        $universitySupervisorBefore = substr( $studentProfile->guider, strpos($studentProfile->guider, '-')+1);
        $guiders = explode(' ', $universitySupervisorBefore);
        $guider = '';
        foreach ($guiders as $key) {
            $guider .= substr(strtoupper($key), 0, 1).substr(strtolower($key), 1).' ';
        }

        $spvnames = explode(' ', $studentProfile->supervisorName);
        $spvname = '';
        foreach ($spvnames as $key) {
            $spvname .= substr(strtoupper($key), 0, 1).substr(strtolower($key), 1).' ';
        }

        $document->setValue(array('name', 'studentid', 'email', 'phone', 'address', 'degree', 'school', 'semester', 'company_name', 'supervisor_name', 'supervisor_position', 'supervisor_email', 'supervisor_phone', 'supervisor_address', 'job_description', 'university_supervisor', 'date_now', 'tc1', 'tc2', 'tc3', 'tc4', 'tc5', 'tc6', 'tc7', 'tc8', 'tc9', 'tc10', 'tc11', 'tc12', 'ss1', 'ss2', 'ss3', 'ss4', 'ss5', 'ss6'), array($studentName, $studentProfile->nim, $studentProfile->studentEmail, $studentProfile->studentPhone, $studentProfile->studentAddress, $studentProfile->studentDegree, $studentProfile->studentSchool, $studentProfile->studentSemester, $companyProfile->name, $spvname, $studentProfile->supervisorPosition, $studentProfile->supervisorEmail, $studentProfile->supervisorPhone, $studentProfile->supervisorAddress, $description, $guider, $date_now, $tc1, $tc2, $tc3, $tc4, $tc5, $tc6, $tc7, $tc8, $tc9, $tc10, $tc11, $tc12, $ss1, $ss2, $ss3, $ss4, $ss5, $ss6));

        $file = '/Learning Plan - '.$studentid.'.docx';

        if(!File::exists( public_path('/assets/ELP/').$studentid))
        {
            File::makeDirectory(public_path('/assets/ELP/').$studentid.'/', 0777, true, true);
        }
        $document->saveAs(public_path('/assets/ELP/').$studentid.'/'.$file);
        $contents = public_path('/assets/ELP/').$studentid.$file;

        $headers = array(
            'Content-Type: application/msword',
        );

        return Response::download($contents, 'Learning Plan - '.$studentid.'.docx', $headers);
    }

    // Department's home page
    // routes: /homed
    public function homeDepartment(Request $request)
    {
        // get session
        $generatedSession = Department::generateRoleSession();
        $head_of_faculty = $generatedSession['head_of_faculty'];
        $head_of_program = $generatedSession['head_of_program'];
        $faculties_can_be_chosen = $generatedSession['faculties_can_be_chosen'];
        $degrees_can_be_chosen = $generatedSession['degrees_can_be_chosen'];

        $filter = NULL;
        $filterCampus = \App\Campus::where('id', $request->filterCampus)->first()->name ?? null;

        $allowed_degree_id = [];
        if ($request->filter == 'all') {
            $filter = 'all';
            $allowed_degree_id = collect($generatedSession['degrees_can_be_chosen'])->pluck('id');
        } else {
            $checkFaculty = Faculty::all();
            if (count($checkFaculty) > 0) {
                if ($request->filter !== NULL) {
                    $filter = $request->filter;
                    if (Faculty::where('id', $filter)->first() == null) { // if out of bounds
                        return redirect()->route('department.home');
                    }
                    else if ($faculties_can_be_chosen->whereLoose('id', $filter)->first() == null) { //accssing another faculty is illegal for admin
                        return redirect()->route('department.home');
                    }
                }
                if ($filter === NULL) { //default filter
                    return redirect()->route('department.home', ['filter' => $faculties_can_be_chosen[0]->id]);
                }
            }

            //filter
            $degrees_can_be_chosen = $degrees_can_be_chosen->whereLoose('faculty_id', $filter);
//			return response()->json($degrees_can_be_chosen);

            // SET DEFAULT HEAD OF FACULTY
            if(empty(session('head_of_faculty'))){
                session([
                    'head_of_faculty' => $head_of_faculty,
                ]);
            }
            // SET DEFAULT HEAD OF PROGRAM
            if(empty(session('head_of_program')) || $request->filter_change != null){
                session([
                    'head_of_program' => $degrees_can_be_chosen->first()->id ?? $head_of_program
                ]);
            }

            if(session('head_of_program') == 'all'){
                foreach($degrees_can_be_chosen as $d){
                    array_push($allowed_degree_id, $d->id);
                }
            }
            else{
                array_push($allowed_degree_id, session('head_of_program'));
            }
        }


        // dd($allowed_degree_id);die();

        // students which are approved
        $AcceptedStudent = Student::filterCampus()->filterInternship()->join('recruitments', 'students.studentid', '=', 'recruitments.studentid')
            ->join('jobs', 'jobs.id', '=', 'recruitments.jobid')
            ->join('companies', 'jobs.companyid', '=', 'companies.companyid')
            ->join('degrees', 'degrees.id', '=', 'students.degreeid')
            ->whereNull('recruitments.deleted_at')
            ->whereNull('jobs.deleted_at')
            ->whereNull('companies.deleted_at')
            ->whereNull('degrees.deleted_at')
            ->whereIn('students.degreeid', $allowed_degree_id)
            ->where('status','=',"approved")
            ->where(function($query) use ($filterCampus) {
                if ($filterCampus) {
                    $query->where('students.campus', $filterCampus);
                } else {
                    $query;
                }
            })
            ->select('students.studentid as studentid', 'students.name as name', 'students.cv as cv', 'students.phone as phone', 'students.sex as sex', 'students.email as email', 'companies.name as company_name', 'degrees.name as degree_name')
            ->distinct()
            ->get();

        // students which have not approved
        $WaitingStudent = Student::filterCampus()->filterInternship()->whereNotIn('students.studentid',
            function ($query)
            {
                $query
                    ->select('studentid')
                    ->from('recruitments')
                    ->whereRaw('deleted_at IS NULL')
                    ->whereRaw('status = "approved"');
            })
            ->join('degrees', 'degrees.id', '=', 'students.degreeid')
            ->whereRaw('degrees.deleted_at IS NULL')
            ->whereIn('degrees.id', $allowed_degree_id)
            ->where(function($query) use ($filterCampus) {
                if ($filterCampus) {
                    $query->where('students.campus', $filterCampus);
                } else {
                    $query;
                }
            })
            ->select('students.studentid as studentid', 'students.name', 'students.cv', 'students.phone', 'students.sex', 'students.email', 'degrees.name as degree_name')
            ->distinct()
            ->get();

        // companies which jobs have approved student
        $AcceptedCompany = Company::join('jobs','jobs.companyid','=','companies.companyid')
            ->join('recruitments','jobs.id','=','recruitments.jobid')
            ->join('jobsdegrees', 'jobsdegrees.jobid', '=', 'jobs.id')
            ->whereRaw('jobs.deleted_at IS NULL')
            ->whereRaw('recruitments.deleted_at IS NULL')
            ->whereRaw('jobsdegrees.deleted_at IS NULL')
            ->where('status','=',"approved")
            ->whereIn('jobsdegrees.degreeid', $allowed_degree_id)
            ->select('companies.companyid','pic','address','companies.name','email','phone','website')
            ->distinct()
            ->get();

        // companies which have currently processed / rejected / accepted (but not approved) the job application
        $ProcessCompany = Company::join('jobs','jobs.companyid','=','companies.companyid')
            ->join('recruitments','jobs.id','=','recruitments.jobid')
            ->join('jobsdegrees', 'jobsdegrees.jobid', '=', 'jobs.id')
            ->whereIn('status', ["process", "reject"])
            ->whereIn('jobsdegrees.degreeid', $allowed_degree_id)
            ->whereRaw('jobs.deleted_at IS NULL')
            ->whereRaw('recruitments.deleted_at IS NULL')
            ->whereRaw('jobsdegrees.deleted_at IS NULL')
            ->whereNotIn('jobs.id',
                function ($query)
                {
                    $query
                        ->select('jobid')
                        ->from('recruitments')
                        ->whereRaw('recruitments.deleted_at IS NULL')
                        ->whereRaw('status = "approved"');
                })
            ->select('companies.companyid','pic','address','companies.name','email','phone','website')
            ->distinct()
            ->get();

        // companies who have done nothing
        $WaitCompany = Company::join('jobs','jobs.companyid','=','companies.companyid')
            ->join('recruitments','jobs.id','=','recruitments.jobid')
            ->join('jobsdegrees', 'jobsdegrees.jobid', '=', 'jobs.id')
            ->whereRaw('jobs.deleted_at IS NULL')
            ->whereRaw('recruitments.deleted_at IS NULL')
            ->whereRaw('jobsdegrees.deleted_at IS NULL')
            ->where('status','=',"waiting")
            ->whereIn('jobsdegrees.degreeid', $allowed_degree_id)
            ->whereNotIn('companies.companyid',
                function ($query)
                {
                    $query
                        ->select('companyid')
                        ->from('jobs as j')
                        ->join('recruitments as r','r.jobid','=','j.id')
                        ->whereRaw('j.deleted_at IS NULL')
                        ->whereRaw('r.deleted_at IS NULL')
                        ->whereRaw('status = "approved" or status = "accepted" or status = "process"');
                })
            ->select('companies.companyid','pic','address','companies.name','email','phone','website')
            ->distinct()
            ->get();

        // companies without a job
        $NoJobCompany = Company::leftJoin('jobs','jobs.companyid','=','companies.companyid')
            ->whereRaw('jobs.companyid is NULL')
            ->whereRaw('jobs.deleted_at is NULL')
            ->select('companies.companyid','pic','address','companies.name','email','phone','website')
            ->distinct()
            ->get();

        // companies with job(s) but no one apply
        $NoApplyCompany = Company::join('jobs','jobs.companyid','=','companies.companyid')
            ->join('jobsdegrees', 'jobsdegrees.jobid', '=', 'jobs.id')
            ->whereRaw('jobs.deleted_at is NULL')
            ->whereRaw('jobsdegrees.deleted_at is NULL')
            ->whereNotIn('companies.companyid',
                function ($query)
                {
                    $query
                        ->select('companyid')
                        ->from('jobs as j')
                        ->join('recruitments as r','r.jobid','=','j.id')
                        ->whereRaw('r.deleted_at IS NULL')
                        ->whereRaw('j.deleted_at IS NULL');
                })
            ->whereIn('jobsdegrees.degreeid', $allowed_degree_id)
            ->select('companies.companyid','pic','address','companies.name','email','phone','website')
            ->distinct()
            ->get();

        //tampung ke satu array semua datanya
        $student = array();
        $student['accept'] = $AcceptedStudent;
        $student['wait'] = $WaitingStudent;

        $studentCV = array();
        $studentCV['yes'] = Student::filterCampus()->filterInternship()->where('cv', 1)->where(function($query) use ($filterCampus) {
            if ($filterCampus) {
                $query->where('students.campus', $filterCampus);
            } else {
                $query;
            }
        })->whereIn('students.degreeid', $allowed_degree_id)->get();
        $studentCV['no'] = Student::filterCampus()->filterInternship()->where('cv', '<>', 1)->where(function($query) use ($filterCampus) {
            if ($filterCampus) {
                $query->where('students.campus', $filterCampus);
            } else {
                $query;
            }
        })->whereIn('students.degreeid', $allowed_degree_id)->get();

        //TODO : student yang sudah register dan belum

        $studentApply = array();
        $studentApply['yes'] = Student::filterCampus()->filterInternship()->where(function($query) use ($filterCampus) {
            if ($filterCampus) {
                $query->where('students.campus', $filterCampus);
            } else {
                $query;
            }
        })->has('recruitment')->whereIn('students.degreeid', $allowed_degree_id)->get();
        $studentApply['no'] = Student::filterCampus()->filterInternship()->where(function($query) use ($filterCampus) {
            if ($filterCampus) {
                $query->where('students.campus', $filterCampus);
            } else {
                $query;
            }
        })->doesnthave('recruitment')->whereIn('students.degreeid', $allowed_degree_id)->get();

        $company = array();
        $company['accept'] = $AcceptedCompany;
        $company['process'] = $ProcessCompany;
        $company['wait'] = $WaitCompany;
        $company['nojob'] = $NoJobCompany;
        $company['noapply'] = $NoApplyCompany;
        // get count of all quota and quota filled
        $total_quota = 0;
        $available_quota = 0;
        $filled = 0;
        $acceptedCompanyIds = collect($AcceptedCompany)->pluck('companyid');
        $acceptedStudentIds = collect($AcceptedStudent)->pluck('studentid');
//		dd($acceptedCompanyIds);
        $all_companies = Company::whereIn('companyid', $acceptedCompanyIds)->get();
        foreach ($all_companies as $c) {
            // get companies' quota count
            $jobs = Job::where('companyid', $c->companyid)->whereHas('jobsdegrees', function($query) use ($allowed_degree_id) {
                $query->whereIn('degreeid', $allowed_degree_id);
            })->get();
            foreach ($jobs as $job) {
                // get each job approved count
                $approved_count =
                    Recruitment::where('jobid', '=', $job->id)
                        ->whereIn('studentid', $acceptedStudentIds)
                        ->whereIn('status', ['approved', 'accepted'])
                        ->get()
                        ->count();

                //store the approved count to a var
                $filled += $approved_count;
                $available_quota += $job->quota;

                // store the total quota of each job
                $total_quota += $approved_count + $job->quota;
            }
        }

        // get total student based on degree
        $total_students = Student::filterCampus()->filterInternship()->
        join('degrees', 'degrees.id', '=', 'students.degreeid')
            ->whereNull('degrees.deleted_at')
            ->whereIn('students.degreeid', $allowed_degree_id)
            ->where(function($query) use ($filterCampus) {
                if ($filterCampus) {
                    $query->where('students.campus', $filterCampus);
                } else {
                    $query;
                }
            })
            ->get()
            ->count();

        $total_students_approved = $AcceptedStudent->count();

        // top 10 most apply companies
        $top_ten = Company::join('jobs', 'jobs.companyid', '=', 'companies.companyid')
            ->join('recruitments', 'recruitments.jobid', '=', 'jobs.id')
            ->whereNull('jobs.deleted_at')
            ->whereNull('recruitments.deleted_at')
            ->selectRaw(
                'COUNT(recruitments.id) as how_many_apply, companies.name as company_name, companies.companyid as company_id'
            )
            ->groupBy('companies.companyid')
            ->orderBy('how_many_apply', 'desc')
            ->take(10)
            ->get();

        return view('department.home', [
            // charts
            'studentCV' => $studentCV,
            'studentApply' => $studentApply,
            'student' => $student,
            'company' => $company,
            'total_quota' => $total_quota,
            'available_quota' => $available_quota,
            'filled' => $filled,
            'total_students' => $total_students,
            'total_students_approved' => $total_students_approved,
            'top_ten' => $top_ten,
            // end of charts
            'filter_faculty' => $filter,
            'filter_campus' => $request->filterCampus,
            'faculties_can_be_chosen' => $faculties_can_be_chosen,
            'degrees_can_be_chosen' => $degrees_can_be_chosen,
        ]);
    }

    // get student's current job application list
    // route: /getStudentJobApplicationList
    // return: json (for datatable)
    public function getStudentJobApplicationList($studentid){
        if (Student::filterCampus()->filterInternship()->where('studentid', '=', $studentid)->first() == null) {
            return ['data' => null];
        }
        else{
            $data = Recruitment::join('students', 'students.studentid', '=', 'recruitments.studentid')
                ->join('jobs','jobs.id','=','recruitments.jobid')
                ->join('companies', 'jobs.companyid', '=', 'companies.companyid')
                ->whereNull('students.deleted_at')
                ->whereNull('jobs.deleted_at')
                ->whereNull('companies.deleted_at')
                ->where('students.studentid', $studentid)
                ->select(
                    'companies.name as companyname',
                    'jobs.name as jobname',
                    'recruitments.updated_at as time',
                    'recruitments.status as status'
                )
                ->get()
                ->all();
            return [
                'data' => $data
            ];
        }
    }

    //function untuk preview CV oleh dirinya sendiri / Student
    public function previewCvStudent()
    {
        if(Auth::user()->student()->first()->cv == 1){
            if(!\Illuminate\Support\Facades\Storage::exists('/cv/'.Auth::user()->userid.'/cv.pdf')){
                return 'Belum ada CV';
            }
            $contents = Storage::get('/cv/'.Auth::user()->userid.'/cv.pdf');
            if (!isset($contents))
            {
                return 'Belum ada CV';
            }
            return Response::make($contents, 200, ['Content-Type' => 'application/pdf']);
        }
        else{
            abort(404);
        }
    }

    //untuk preview cv mahasiswa
    public function previewCvOther($id)
    {
        $student = Student::filterCampus()->filterInternship()->where('studentid', '=', $id)->first();
        if($student != null && $student->cv == 1){
            if(!\Illuminate\Support\Facades\Storage::exists('/cv/'.$student->studentid.'/cv.pdf')){
                return 'Belum ada CV';
            }
            $contents = Storage::get('/cv/'.$id.'/cv.pdf');
            //cek bila cv gada
            if (!isset($contents))
            {
                return 'Belum ada CV';
//                abort(404);
            }
            return Response::make($contents, 200, ['Content-Type' => 'application/pdf']);
        }
        else{
            abort(404);
        }
    }

    // Get excel template for inserting guider data
    // return: excel file (template for inserting guider data)
    public function templateUniversitySupervisorDepartment()
    {
        Excel::create('Guider', function ($excel)
        {
            $excel->sheet('Sheet1', function($sheet)
            {
                $sheet->cell('A1', function($cell)
                {
                    $cell->setValue('Lecturer_Code');
                });
                $sheet->cell('B1', function($cell)
                {
                    $cell->setValue('Name');
                });
                $sheet->cell('C1', function($cell)
                {
                    $cell->setValue('Email');
                });
                $sheet->cell('D1', function($cell)
                {
                    $cell->setValue('Phone');
                });
            });
        })->export('xlsx');
    }

    //function untuk generate CV
    public function generateCv($id)
    {
        $student = Student::where('studentid', '=', $id)->first();
        $formal = Cvdateinformation::where('studentid', '=', $id)->where('type','=','Formal')->get();
        $informal = Cvdateinformation::where('studentid', '=', $id)->where('type','=','Informal')->get();
        $organization = Cvdateinformation::where('studentid', '=', $id)->where('type','=','Organization')->get();
        $award = Cvdateinformation::where('studentid', '=', $id)->where('type','=','Award')->get();
        $skill = Cvskillinformation::where('studentid', '=', $id)->where('type','=','Skills')->get();
        $language = Cvskillinformation::where('studentid', '=', $id)->where('type','=','Languages')->get();
        $soft = Cvskillinformation::where('studentid', '=', $id)->where('type','=','Soft Skills')->get();
        if($soft){

        }
        $subs = Studentscore::where('studentid', '=', $id)->get();
        $ip = Studentsemester::where('studentid', '=', $id)->orderBy('semester', 'asc')->get();
        $path = storage_path().'/assets/images/'.$id.'/'.$student->photo;
        $dir = null;


        if($skill->count() > 0){
            $stringskill = array();
            $stringlevel = array();
            foreach($skill as $f)
            {
                array_push($stringskill, $f->name);
                array_push($stringlevel, $f->status);
            }
            $skill1 = implode("|", array_reverse($stringskill));
            $skill2 = implode(",", $stringlevel);
            $skill1 = urlencode($skill1);

//			$url = 'https://chart.googleapis.com/chart?'.
//				'cht=bvs&'.
//				'chbh=50&'.
//				'chds=a&'.
//				'chd=t:'.$skill2.'&'.
//				'chco=4286f4&'.
//				'chs=700x300&'.
//				'chxt=y&'.
//				'chm=N,000000,0,-1,11&'.
//				'chl='.$skill1.'&'.
//				'chbh=60,20';

            $url = 'https://chart.googleapis.com/chart?'.
                'cht=bhs&'.
                'chbh=a&'.
                'chds=0,10&'.
                'chd=t:'.$skill2.'&'.
                'chco=4286f4&'.
                'chs=700x300&'.
                'chxt=x,y&'.
                'chm=N,000000,0,-1,11&'.
                'chxl=0:|0|2|4|6|8|10|1:|'.$skill1.'|&'.
                'chbh=a';

            if(!File::exists(storage_path().'/app/cv/'.$student->studentid.'/'))
            {
                File::makeDirectory(storage_path().'/app/cv/'.$student->studentid.'/', 0777, true, true);
            }
            $dir = storage_path().'/app/cv/'.$student->studentid.'/chart.png';
            $image = Image::make($url)->save($dir);
        }

        //tutup HTML + Load dompdf + atur paper dan simpen ke storage hasil generatenya
        $data = View::make('others.templatecv', [
            'path' => $path,
            'formal' => $formal,
            'informal' => $informal,
            'organization' => $organization,
            'award' => $award,
            'skill' => $skill,
            'language' => $language,
            'subs' => $subs,
            'ip' => $ip,
            'student' => $student,
            'image' => $dir,
            "soft" => $soft
        ])->render();

        $dompdf = new Dompdf();
        $dompdf->loadHtml($data);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();
        $output = $dompdf->output();
        Storage::disk('local')->put('/cv/'.$id.'/cv.pdf', $output);
    }

    //siapin data untuk ditampilin di halaman profile student
    public function profileOther($id)
    {
        $state = null;
        if(Auth::user()->role == 'company'){
            $getState = Company::where('companyid', '=', Auth::user()->userid)
                ->first();
            $state = $getState->state;
        }

        $user = User::where('userid', '=', $id)->first();

        //ambil data student dan semester
        $userProfile = User::join('students', 'users.userid', '=', 'students.studentid')
            ->whereNull('students.deleted_at')
            ->where('students.studentid', '=', $user->userid)
            ->first();

        if($userProfile == null){
            return redirect('/');
        }

        $userDegree = null;
        if(!empty($userProfile->degreeid)){
            $userDegree = Degree::where('id', '=', $userProfile->degreeid)
                ->first();
        }

        $ScoreSemester = Studentsemester::where('studentid', '=', $user->userid)
            ->orderBy('semester', 'asc')
            ->get();

        $ScoreSubject = Studentscore::where('studentid', '=', $user->userid)->get();

        //ambil semua data cv yang dates + skills
        $listFormal = Cvdateinformation::where('studentid', '=', $user->userid)->where('type','=','Formal')->get();

        $listInformal = Cvdateinformation::where('studentid', '=', $user->userid)->where('type','=','Informal')->get();

        $listOrg = Cvdateinformation::where('studentid', '=', $user->userid)->where('type','=','Organization')->get();

        $listAward = Cvdateinformation::where('studentid', '=', $user->userid)->where('type','=','Award')->get();

        $listSkill = Cvskillinformation::where('studentid', '=', $user->userid)->where('type','=','Skills')->get();

        $listLang = Cvskillinformation::where('studentid', '=', $user->userid)->where('type','=','Languages')->get();

        $mailboxCount = Mailbox::whereIn('type', array('interview', 'mailbox'))
            ->where('status', '=', 'unseen')
            ->where('recipientid', '=', Auth::user()->userid)
            ->count();

        return view("student.profile",
            [
                'userDegree' => $userDegree,
                'userProfile' => $userProfile,
                'semester'=>$ScoreSemester,
                'subject'=>$ScoreSubject,
                'formal' => $listFormal,
                'informal' => $listInformal,
                'org' => $listOrg,
                'award' => $listAward,
                'skill' => $listSkill,
                'lang' => $listLang,
                'mailboxCount' => $mailboxCount,
                'accepted' => 'no',
                'state' => $state,
                'notifCount' => app('App\Http\Controllers\MailboxController')->getCountNotification(),
                'notif' => app('App\Http\Controllers\MailboxController')->getLatestNotification()
            ]);
    }

    //get data untuk halaman learningplan
    public function learningPlanStudent()
    {
        if($this->checkAccepted() == 'no'){
            return redirect('/');
        }

        $prof = Student::filterCampus()->filterInternship()->where('studentid', '=', Auth::user()->userid)->first();
        $prof->degree = $prof->degree()->first()->name;

        $data = Recruitment::join('jobs','jobs.id','=','recruitments.jobid')
            ->join('companies','companies.companyid','=','jobs.companyid')
            ->whereNull('jobs.deleted_at')
            ->whereNull('companies.deleted_at')
            ->where('studentid','=',Auth::user()->userid)
            ->where('status','approved')
            ->select('jobs.name as jobname','companies.name as companyname')
            ->first();

        $mailboxCount = Mailbox::whereIn('type', array('interview', 'mailbox'))->where('status', '=', 'unseen')->where('recipientid', '=', Auth::user()->userid)->count();
        $statusLP = null;

        $checkData = Student::filterCampus()->filterInternship()->leftJoin('supervisors', 'students.supervisorid', '=', 'supervisors.id')->where('students.studentid', Auth::user()->userid)->select('supervisors.name as spvname', 'students.guider as guider')->first();

        if ($checkData->spvname && $checkData->guider){
            $statusLP = 'ada isi nya';
        }

        return view('student.learningplan',
            [
                'data' => $data,
                'mailboxCount' => $mailboxCount,
                'notifCount' => app('App\Http\Controllers\MailboxController')->getCountNotification(),
                'notif' => app('App\Http\Controllers\MailboxController')->getLatestNotification(),
                'profile' => $prof,
                'statusLP' => $statusLP,
                'accepted' => $this->checkAccepted()
            ]);
    }

    public function tempLearningPlanBlock(){
        $mailboxCount = Mailbox::whereIn('type', array('interview', 'mailbox'))
            ->where('status', '=', 'unseen')
            ->where('recipientid', '=', Auth::user()->userid)
            ->count();

        return view('student.learningplanblock', [
            'accepted' => $this->checkAccepted(),
            'mailboxCount' => $mailboxCount,
            'notifCount' => app('App\Http\Controllers\MailboxController')->getCountNotification(),
            'notif' => app('App\Http\Controllers\MailboxController')->getLatestNotification(),
        ]);
    }

    // Organize > Assign Supervisor
    // for department account to open assign university supervisor page
    public function assign_supervisor(){
        $allowed_degree_id = DepartmentController::get_allowed_degree_list();

        $total_approved = Recruitment::where('status', '=','approved')
            ->join('students', 'students.studentid', '=', 'recruitments.studentid')
            ->whereNull('students.deleted_at')
            ->whereIn('students.degreeid', $allowed_degree_id)
            ->get()
            ->count();

        $university_supervisor = UniversitySupervisor::get();

        foreach($university_supervisor as $u){
            $u->student_list = $u->student()->join('degrees', 'degrees.id', '=', 'students.degreeid')->select('students.*', 'degrees.name as degree_name')->get();
        }

        return view('department.assign_supervisor', [
            'total_approved' => $total_approved,
            'supervisors' => $university_supervisor
        ]);
    }

    // Organize > Assign Supervisor
    // get list approved student
    public function get_list_approved_student(){
        $allowed_degree_id = DepartmentController::get_allowed_degree_list();

        $data = Student::filterCampus()->filterInternship()->join('recruitments', 'recruitments.studentid', '=', 'students.studentid')
            ->join('jobs', 'jobs.id', '=', 'recruitments.jobid')
            ->join('degrees', 'students.degreeid', '=', 'degrees.id')
            ->join('companies', 'companies.companyid', '=', 'jobs.companyid')
            ->whereNull('recruitments.deleted_at')
            ->whereNull('jobs.deleted_at')
            ->whereNull('companies.deleted_at')
            ->where('recruitments.status', '=', 'approved')
            ->whereIn('students.degreeid', $allowed_degree_id)
            ->whereNull('students.univ_supervisor_id')
            ->select(
                'students.studentid as student_id',
                'students.name as student_name',
                'degrees.name as program',
                'jobs.name as job_name',
                'companies.name as company_name'
            )
            ->distinct()
            ->get()
            ->toArray();

        return json_encode(array('data' => $data));
    }

    public function assign_university_supervisor(Request $req){
        $allowed_degree_id = DepartmentController::get_allowed_degree_list();

        $univ_spv = UniversitySupervisor::where('id', '=', $req->spv_id)
            ->first();

        if(empty($univ_spv)){
            return redirect('/assign_supervisor')->with('fyi', 'Cannot find supervisor ID');
        }

        $errors = null;
        $arrayStudent = [];

        foreach($req->all() as $key => $value){
            if(substr($key, 0, 8) === "student_"){
                $student_id = substr($key, 8);
                $student = Student::filterCampus()->filterInternship()->where('studentid', '=', $student_id)->first();
                if(empty($student)){
                    $errors = 'Invalid inputs !';
                }
                else if($student->recruitment()->where('recruitments.status', '=', 'approved')->get()->count() == 0){
                    $errors = 'Student has not approved yet!';
                }
                else if(!empty($student->univ_supervisor_id)){
                    $errors = 'Student has not been assigned before!';
                }
                else{
                    array_push($arrayStudent, $student);
                }
            }
        }

        if(!empty($errors)){
            return redirect('/assign_supervisor')->with('fyi', $errors);
        }

        foreach($arrayStudent as $s){
            $s->univ_supervisor_id = $univ_spv->id;
            $s->save();
        }

        return redirect('/assign_supervisor')->with('fyi', 'Success assigning the student');
    }

    public function generateStudentCV(){
        $student = Student::filterCampus()->filterInternship()->where('cv',1)->limit(100)->get();
        foreach ($student as $s){
            $contents = Storage::exists('/cv/'.$s->id.'/cv.pdf');
            $this->generateCv($s->studentid);
        }
        return "CV successfully generated";
    }



    public function resetCVStatus(){
//        if(Auth::user()->userid){
//            dd("asd");
//        }
        $student = Student::filterCampus()->filterInternship()->where('cv',1)->where("period","2010")->get();
        foreach ($student as $s){
            $contents = Storage::exists('/cv/'.$s->id.'/cv.pdf');
            if($contents === false){
                $s->cv = 0;
                $s->save();
                $insertMailbox = new Mailbox;
                $insertMailbox->senderid = Auth::user()->userid;
                $insertMailbox->recipientid = $s->studentid;
                $insertMailbox->subject = "Generate CV";
                $insertMailbox->date = date("Y-m-d H:i:s");
                $insertMailbox->message = "Dear student please re-generate your cv";
                $insertMailbox->status = 'unseen';
                $insertMailbox->type = 'mailbox';
                $insertMailbox->token = '';
                $insertMailbox->save();

                $token =  Hash::make($insertMailbox->id.$insertMailbox->senderid.$insertMailbox->recipientid);
                $resToken = str_replace('.', 'H', str_replace("$", "S", str_replace("/", "X", $token)));

                Mailbox::where('id', '=', $insertMailbox->id)
                    ->update(['token' => $resToken]);

            }
        }
        return "Student CV reseted";
    }
}

?>