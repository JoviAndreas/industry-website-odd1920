<?php

namespace App\Http\Controllers;

use App\Department;
use App\Recruitment;
use App\Student;
use App\StudentSemesterTemp;
use App\StudentTemp;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;
use Spatie\ArrayToXml\ArrayToXml;
use XmlResponse\XmlResponse;

class APIController extends BaseController
{
    public function __construct()
    {
        if (!Department::isSuperAdmin()) {
            return redirect()->back()->withErrors(['error' => 'You are not eligible to see that page']);
        }
    }

    public function index() {
        // $studentTemp = StudentTemp::selectRaw('COUNT(students_temp.studentid) counts, students_temp.track')
        //                     ->groupBy('students_temp.track')
        //                     ->where('period', Auth::user()->active_period)
        //                     ->get();
        $students = Student::selectRaw('COUNT(students.studentid) counts, students.track')
                            ->groupBy('students.track')
                            ->where('period', Auth::user()->active_period)
                            ->get();
        // $studentGPATemp = StudentSemesterTemp::all();
        return view('department.data_synchronization.index', compact('studentTemp', 'studentGPATemp', 'students'));
    }

    public function insertTemporaryStudentHeaderAction($campus, $period) {
        $campusMapping = [
            'kmg' => [
                'institution' => 'BNS01',
                'acad_career' => 'RS1',
            ],
            'jwc' => [
                'institution' => 'BNS01',
                'acad_career' => 'IS1',
            ],
            'mlg' => [
                'institution' => 'BNS02',
                'acad_career' => 'MLS1',
            ],
            'bdg' => [
                'institution' => 'BNS03',
                'acad_career' => 'BDS1',
            ],
        ];

            $client = new \SoapClient('http://bcs.binus.ac.id:8200/PSIGW/PeopleSoftServiceListeningConnector/N_TPON_SERVICES.1.wsdl', [
                'exceptions' => true,
                'trace' => 1,
                'cache_wsdl' => WSDL_CACHE_NONE
            ]);

        $institution = $campusMapping[$campus]['institution'];
        $acadCareer = $campusMapping[$campus]['acad_career'];

        $result = null;
        try {
            $parameters = [
                'parameters' => [
                    'Institution' => $institution,
                    'AcadCareer' => $acadCareer,
                    'Strm' => $period,
                ]
            ];
            $result = $client->__soapCall('N_TPON_GET_STUDENT_DATA', $parameters);
        } catch (\SoapFault $fault) {
            trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);
            return -1;
        }
        $responses = collect($result->N_GET_DATA_RESP_DTL);

        if ($responses->count() > 0) {
            StudentTemp::where('institution' , $institution)->where('acad_career', $acadCareer)->where('period', $period)->delete();
            StudentSemesterTemp::where('institution' , $institution)->where('acad_career', $acadCareer)->where('period', $period)->delete();

            foreach ($responses as $response) {
                $studentTemp = new StudentTemp;
                $studentTemp->period = $period;
                $studentTemp->studentid = $response->StudentID;
                $studentTemp->name = $response->StudentName;
                $studentTemp->email = $response->Email ?? '';
                $studentTemp->school = $response->School;
                $studentTemp->program = $response->Program ?? '';
                $studentTemp->major = $response->Major;
                $studentTemp->campus = $response->Campus;
                $studentTemp->track = $response->TrackRegistration;
                $studentTemp->institution = $institution;
                $studentTemp->acad_career = $acadCareer;
                $studentTemp->save();

                if (count($response->N_GET_DATA_GPA_HIST) != 0) {
                    foreach ($response->N_GET_DATA_GPA_HIST as $gpa) {
                        $studentSemesterTemp = new StudentSemesterTemp;
                        $studentSemesterTemp->studentid = $response->StudentID;
                        $studentSemesterTemp->semester = (int) $gpa->Term;
                        $studentSemesterTemp->ipk = $gpa->CumGPA;
                        $studentSemesterTemp->ips = $gpa->SemGPA;
                        $studentSemesterTemp->period = $period;
                        $studentSemesterTemp->institution = $institution;
                        $studentSemesterTemp->acad_career = $acadCareer;
                        $studentSemesterTemp->save();
                    }
                }
            }
            return 1;
        } else {
            return 0;
        }
    }

    public function insertTemporaryStudentHeader(Request $request) {
        if ($request->campus == null || $request->campus == '') {
            return redirect()->back()->withErrors(['error' => 'Please fill campus location']);
        }
        set_time_limit(3600);
        $campus = $request->campus;
        $period = Auth::user()->active_period;

        $result = $this->insertTemporaryStudentHeaderAction($campus, $period);
        if ($result == 0) {
            return redirect()->back()->withErrors(['error' => 'There is no Student Data']);
        } else if ($result == 1) {
            return redirect()->back()->with(['fyi' => 'Insert Temporary Student Header Success']);
        } else {
            return redirect()->back()->withErrors(['error' => 'Unknown Error']);
        }
    }

    public function getStudentPlacement(Request $request) {
        $strm = $request->strm;
        $response = [];

        if ($strm == '2010') {
            $recruitments = Recruitment::where('status', 'approved')->get();
            foreach ($recruitments as $recruitment) {
                $response []= [
                    'acad_career' => $recruitment->student->acad_career,
                    'nim' => $recruitment->studentid,
                    'penempatan' => $recruitment->job->company->name,
                    'position' => $recruitment->job->name,
                    'term' => '2010',
                    'track' => $recruitment->student->track
                ];
            }
        }
        return response()->json($response);
    }

   public function insertStudentsGradesByAPI(Request $request) {
       $period = Auth::user()->active_period;
       $client = new \SoapClient('http://bcs.binus.ac.id:8200/PSIGW/PeopleSoftServiceListeningConnector/N_TPON_SERVICES.1.wsdl', [
           'exceptions'=>true,
           'trace'=>1,
           'cache_wsdl'=>WSDL_CACHE_NONE
       ]);

       return StudentTemp::where('');

       $result = null;
       try {
           $parameters = [
               'parameters' => [
                   'Institution' => 'BNS01',
                   'AcadCareer' => 'RS1',
                   'Strm' => $period,
                   'StudentId' => '2001539732'
               ]
           ];
           $result = $client->__soapCall('N_TPON_GET_CRSE_DATA', $parameters);
       } catch (\SoapFault $fault) {
           trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);
       }
       $score = new class{};
       $score->student_id = $result->N_TPON_GET_CRSE_DATA_RESP_DTL;
       return response()->json($result);
   }
}
?>