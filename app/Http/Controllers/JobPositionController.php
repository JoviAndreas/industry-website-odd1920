<?php

namespace App\Http\Controllers;

use App\Degree;
use App\Department;
use App\JobPosition;
use App\JobPositionDegrees;
use Exception;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Validator;

class JobPositionController extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    // Roles : user
    public function getJobPositionAJAX(Request $request)
    {
        $degrees = $request->degrees;
        if ($degrees == null || count($degrees) == 0) {
            return response()->json([]);
        }
        $query = JobPosition::with('jobpositiondegrees');
        foreach ($degrees as $degree_id) {
            $query->whereHas('jobpositiondegrees', function ($q) use ($degree_id) {
                $q->where('degree_id', $degree_id);
            });
        }
        $job_positions = $query->get();
        return response()->json($job_positions);
    }

    // Roles : superadmin
    // Manage > Job Position page
    // routes: /manageJobPosition
    public function index(Request $request)
    {
        if (!Department::isSuperAdmin()) {
            return redirect('/');
        }
        $filter = NULL;
        if ($request->filter !== NULL) {
            $filter = $request->filter;
            if (JobPosition::where('id', $filter)->first() == null && $filter != 'all') // if out of bounds
            {
                return redirect()->route('department.faculty');
            }
        }

        if ($checkRole = session('head_of_faculty')) {
            if ($checkRole === 'all') {
                $roleJobPosition = 'all';
                $filterForRole = JobPosition::all();
            } else {
                //admin faculty
                $roleJobPosition = $checkRole;
                if ($filter !== NULL && $filter !== $roleJobPosition) { // if accessing another job position, then is illegal
                    return redirect()->route('department.jobposition', ['filter' => NULL]);
                }
                $filterForRole = JobPosition::where('id', $roleJobPosition)->get();
            }
        } else {
            // HOP
            return redirect('/');
        }

        if ($filter === NULL) { //default filter
            return redirect()->route('department.jobposition', ['filter' => $roleJobPosition]);
        }

        if ($filter === 'all') {
            return view('superadmin.managejobposition', [
                    'filterForRole' => $filterForRole,
                    'jobpositions' => JobPosition::all(),
                    'degrees' => JobPositionDegrees::all(),
                ]
            );
        } else {
            return view('superadmin.managejobposition', [
                    'filterForRole' => $filterForRole,
                    'jobpositions' => JobPosition::where('id', $filter)->get(),
                    'degrees' => JobPositionDegrees::where('job_position_id', $filter)->get(),
                ]
            );
        }
    }

    // POST
    public function insertJobPosition(Request $request)
    {
        if (!Department::isSuperAdmin()) {
            return redirect('/');
        }
        $validator = Validator::make($request->all(), [
                'name' => 'required|unique:job_positions,name,NULL,id,deleted_at,NULL',
            ]
        );
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        $deletedJobPosition = JobPosition::onlyTrashed()->where('name', $request->name)->first();
        if (!$deletedJobPosition) {
            $newJobPosition = new JobPosition();
            $newJobPosition->name = $request->name;
            $newJobPosition->save();
        } else {
            $deletedJobPosition->deleted_at = null;
            $deletedJobPosition->save();
        }

        return redirect()->back()->with(['success' => 'add job position']);
    }

    // GET
    public function editJobPosition(JobPosition $jobposition)
    {
        if (!Department::isSuperAdmin()) {
            return redirect('/');
        }
        $jobpositiondegrees = JobPositionDegrees::where('job_position_id', $jobposition->id)->get();
        $degrees = array();
        $count = 0;
        $degrees_id[] = "";
        foreach ($jobpositiondegrees as $jpd) {
            $degrees[$count] = Degree::where('id', $jpd->degree_id)->first();
            $degrees_id[$count] = $degrees[$count]->id;
            $count++;
        }
        return view('superadmin.editjobposition', [
                'jobposition' => $jobposition,
                'degrees' => $degrees,
                'unmappedDegrees' => Degree::whereNotIn('id', $degrees_id)->get(),
            ]
        );
    }

    // POST
    public function updateJobPosition(Request $request, JobPosition $jobposition)
    {
        if (!Department::isSuperAdmin()) {
            return redirect('/');
        }
        $validator = Validator::make($request->all(), [
                'name' => 'required|unique:job_positions,name,' . $jobposition->id,
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $jobposition->name = $request->name;
        $jobposition->save();

        $jobpositiondegrees = JobPositionDegrees::where('job_position_id', $jobposition->id)->get();

        if ($jobpositiondegrees) {
            foreach ($jobpositiondegrees as $jpd) {
                $jpd->delete();
            }
        }
        if ($request->degrees) {
            $degreeIds = collect($request->degrees)->unique();
            foreach ($degreeIds as $degreeId) {
                $newjobpositiondegree = new JobPositionDegrees();
                $newjobpositiondegree->job_position_id = $jobposition->id;
                $newjobpositiondegree->degree_id = $degreeId;
                $newjobpositiondegree->save();
            }
        }
        if ($request->unmappedDegrees) {
            $degreeIds = collect($request->unmappedDegrees)->unique();
            foreach ($degreeIds as $degreeId) {
                $newjobpositiondegree = new JobPositionDegrees();
                $newjobpositiondegree->job_position_id = $jobposition->id;
                $newjobpositiondegree->degree_id = $degreeId;
                $newjobpositiondegree->save();
            }
        }
        return redirect()->back()->with(['success' => 'update faculty']);
    }

    // POST
    public function deleteJobPosition(JobPosition $jobposition)
    {
        if (!Department::isSuperAdmin()) {
            return redirect('/');
        }
        $jobpositionname = $jobposition->name;
        JobPositionDegrees::where('job_position_id', $jobposition->id)->each(function ($jobpositiondegree) {
            $jobpositiondegree->delete();
        });
        try {
            $jobposition->delete();
        } catch (Exception $e) {
            return redirect()->back()->with(['errors' => 'delete job position ' . $jobpositionname]);
        }
        return redirect()->back()->with(['success' => 'delete job position ' . $jobpositionname]);
    }

}
?>