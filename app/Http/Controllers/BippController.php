<?php

namespace App\Http\Controllers;

use App\Company;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Hash;
use Auth;
use DB;
use Validator;
use Storage;
use Response;
use route;
use URL;
use Image;
use File;
use App\Bipp;
use Carbon\Carbon;

class BippController extends BaseController
{
 
    public function indexBipp(Request $request){
        if (!\App\Department::isSuperAdmin()) {
            return redirect('/');
        }
        $data = Bipp::all();
        return view('department.editbipp',
	       	[
				'data' => $data
	      	]);
    }
    public function updateBipp(Request $request){
        if (!\App\Department::isSuperAdmin()) {
            return redirect('/');
        }
        $validator = Validator::make($request->all(), ['bipp' => 'required|numeric|min:0|max:4,min_ipk']);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        $data = Bipp::first();
        if ($data == null) {
            $data = new Bipp;
        }
        $data->min_ipk = $request->bipp;
        $data->save();
        return redirect('/editBipp')->with('fyi','Successfully change BIPP!');
    }
    public function toggleCompanyBippStatus($companyId) {
        if (!\App\Department::isSuperAdmin()) {
            return redirect('/');
        }
        $company = Company::where('id', $companyId)->first();
        if ($company != null) {
            $company->bipp_id = ($company->bipp_id == '1') ? null : '1';
            $company->save();
            return redirect()->back()->with('success','Successfully toggled BIPP!');
        }
        return redirect()->back()->withErrors(['toggle' => 'Toggle Failed']);
    }
}
?>