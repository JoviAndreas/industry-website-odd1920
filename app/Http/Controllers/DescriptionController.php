<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Hash;
use Auth;
use DB;
use Validator;
use Dompdf\Dompdf;
use Storage;
use Response;
use maatwebsite\excel;
use route;
use Symfony\Component\DomCrawler\Crawler;
use URL;
use Image;
use File;
use App\Recruitment;
use App\Mailbox;
use App\Feedback;
use App\Studentscore;
use App\Studentsemester;
use App\Company;
use App\Job;
use App\Student;
use App\Cvdateinformation;
use App\Cvskillinformation;
use App\Interview;
use App\User;
use App\JobsDegrees;
use App\Bipp;
use App\Description;
use App\UnacceptRequest;
use App\UnrejectRequest;
use App\ApprovalRequest;
use App\Services\EmailService;
use Carbon\Carbon;

class DescriptionController extends BaseController
{
	use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;
 
    public function indexDesc(Request $request){
        if (!\App\Department::isSuperAdmin()) {
            return redirect('/');
        }
        $data = Description::all();
        return view('department.editdescription',
	       	[
				'data' => $data
	      	]);
    }
    public function updateDesc(Request $request){
        if (!\App\Department::isSuperAdmin()) {
            return redirect('/');
        }
        $data = Description::where('id','=',$request->id)->first();
        $data->description = $request->desc;
        $data->save();
        return redirect('/editDesc')->with('fyi','Successfully change Description!');
    }
}
?>