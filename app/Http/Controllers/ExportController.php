<?php

namespace App\Http\Controllers;

use App\Company;
use App\Department;
use App\Faculty;
use App\Job;
use App\Student;
use Illuminate\Http\Request;
use Excel;

use App\Http\Requests;
use Illuminate\Routing\Controller as BaseController;

class ExportController extends BaseController
{
    /**
     * @param Request $request
     * - Integer filter
     * - Integer filterCampus
     * @return array['allowed_degere_id', 'filterCampus']
     */
    private function getHomepageAllowedDegreesAndFilter(Request $request) {
        // get session
        $generatedSession = Department::generateRoleSession();
        $head_of_faculty = $generatedSession['head_of_faculty'];
        $head_of_program = $generatedSession['head_of_program'];
        $faculties_can_be_chosen = $generatedSession['faculties_can_be_chosen'];
        $degrees_can_be_chosen = $generatedSession['degrees_can_be_chosen'];

        $filter = NULL;
        $filterCampus = \App\Campus::where('id', $request->filterCampus)->first()->name ?? null;

        $allowed_degree_id = [];
        if ($request->filter == 'all') {
            $filter = 'all';
            $allowed_degree_id = collect($generatedSession['degrees_can_be_chosen'])->pluck('id');
        } else {
            $checkFaculty = Faculty::all();
            if (count($checkFaculty) > 0) {
                if ($request->filter !== NULL) {
                    $filter = $request->filter;
                    if (Faculty::where('id', $filter)->first() == null) { // if out of bounds
                        return redirect()->route('department.home');
                    }
                    else if ($faculties_can_be_chosen->whereLoose('id', $filter)->first() == null) { //accssing another faculty is illegal for admin
                        return redirect()->route('department.home');
                    }
                }
                if ($filter === NULL) { //default filter
                    return redirect()->route('department.home', ['filter' => $faculties_can_be_chosen[0]->id]);
                }
            }

            //filter
            $degrees_can_be_chosen = $degrees_can_be_chosen->whereLoose('faculty_id', $filter);
//			return response()->json($degrees_can_be_chosen);

            // SET DEFAULT HEAD OF FACULTY
            if(empty(session('head_of_faculty'))){
                session([
                    'head_of_faculty' => $head_of_faculty,
                ]);
            }
            // SET DEFAULT HEAD OF PROGRAM
            if(empty(session('head_of_program')) || $request->filter_change != null){
                session([
                    'head_of_program' => $degrees_can_be_chosen->first()->id ?? $head_of_program
                ]);
            }

            if(session('head_of_program') == 'all'){
                foreach($degrees_can_be_chosen as $d){
                    array_push($allowed_degree_id, $d->id);
                }
            }
            else{
                array_push($allowed_degree_id, session('head_of_program'));
            }
        }
        return [
            'allowed_degree_id' => $allowed_degree_id,
            'filterCampus' => $filterCampus
        ];
    }
    /**
     * @param Request $request
     * - Integer filter
     * - Integer filterCampus
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function waitingStudent(Request $request) {
        $homepageAllowedDegreesAndFilter = $this->getHomepageAllowedDegreesAndFilter($request);
        $allowed_degree_id = $homepageAllowedDegreesAndFilter['allowed_degree_id'];
        $filterCampus = $homepageAllowedDegreesAndFilter['filterCampus'];

        $waitingStudent = Student::filterCampus()->filterInternship()->whereNotIn('students.studentid',
            function ($query)
            {
                $query
                    ->select('studentid')
                    ->from('recruitments')
                    ->whereRaw('deleted_at IS NULL')
                    ->whereRaw('status = "approved"');
            })
            ->join('degrees', 'degrees.id', '=', 'students.degreeid')
            ->whereRaw('degrees.deleted_at IS NULL')
            ->whereIn('degrees.id', $allowed_degree_id)
            ->where(function($query) use ($filterCampus) {
                if ($filterCampus) {
                    $query->where('students.campus', $filterCampus);
                } else {
                    $query;
                }
            })
            ->select('students.studentid as studentid', 'students.name', 'students.cv', 'students.phone', 'students.sex', 'students.email', 'degrees.name as degree_name')
            ->distinct()
            ->get();

        // flush data
        ob_end_clean();
        ob_start();

        // export excel
        $excelFileName = 'list_waiting_student';
        $excel = Excel::create($excelFileName, function ($excel) use ($waitingStudent)
        {
            $excel->sheet('ListWaitingStudent', function($sheet) use($waitingStudent)
            {
                $sheet->cell('A1', function($cell)
                {
                    $cell->setValue('NIM');
                });
                $sheet->cell('B1', function($cell)
                {
                    $cell->setValue('Student Name');
                });
                $sheet->cell('C1', function($cell)
                {
                    $cell->setValue('Program');
                });
                $sheet->cell('D1', function($cell)
                {
                    $cell->setValue('Sex');
                });
                $sheet->cell('E1', function($cell)
                {
                    $cell->setValue('Phone');
                });
                $sheet->cell('F1', function($cell)
                {
                    $cell->setValue('Email');
                });
                $sheet->cell('G1', function($cell)
                {
                    $cell->setValue('Has CV');
                });

                $counter=2;
                foreach($waitingStudent as $record)
                {
                    $sheet->cell('A'.$counter, function($cell)use($record)
                    {
                        $cell->setValue($record->studentid);
                    });
                    $sheet->cell('B'.$counter, function($cell)use($record)
                    {
                        $cell->setValue($record->name);
                    });
                    $sheet->cell('C'.$counter, function($cell)use($record)
                    {
                        $cell->setValue($record->degree_name);
                    });
                    $sheet->cell('D'.$counter, function($cell)use($record)
                    {
                        $cell->setValue($record->sex);
                    });
                    $sheet->cell('E'.$counter, function($cell)use($record)
                    {
                        $cell->setValue($record->phone);
                    });
                    $sheet->cell('F'.$counter, function($cell)use($record)
                    {
                        $cell->setValue($record->email);
                    });
                    $sheet->cell('G'.$counter, function($cell)use($record)
                    {
                        $cell->setValue(($record->cv == 1) ? 'Yes' : 'No' );
                    });
                    $counter++;
                }
            });
        })->export('xlsx');
    }

    /**
     * @param Request $request
     * - Integer filter
     * - Integer filterCampus
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function studentCVSummary(Request $request) {
        $homepageAllowedDegreesAndFilter = $this->getHomepageAllowedDegreesAndFilter($request);
        $allowed_degree_id = $homepageAllowedDegreesAndFilter['allowed_degree_id'];
        $filterCampus = $homepageAllowedDegreesAndFilter['filterCampus'];

        $students = Student::filterCampus()->filterInternship()->where(function($query) use ($filterCampus) {
            if ($filterCampus) {
                $query->where('students.campus', $filterCampus);
            } else {
                $query;
            }
        })->whereIn('students.degreeid', $allowed_degree_id)->get();

        // export excel
        $excelFileName = 'list_student_cv_summary';
        $excel = Excel::create($excelFileName, function ($excel) use ($students)
        {
            $excel->sheet('ListStudentCVSummary', function($sheet) use($students)
            {
                $sheet->cell('A1', function($cell)
                {
                    $cell->setValue('NIM');
                });
                $sheet->cell('B1', function($cell)
                {
                    $cell->setValue('Student Name');
                });
                $sheet->cell('C1', function($cell)
                {
                    $cell->setValue('Has CV');
                });

                $counter=2;
                foreach($students as $record)
                {
                    $sheet->cell('A'.$counter, function($cell)use($record)
                    {
                        $cell->setValue($record->studentid);
                    });
                    $sheet->cell('B'.$counter, function($cell)use($record)
                    {
                        $cell->setValue($record->name);
                    });
                    $sheet->cell('C'.$counter, function($cell)use($record)
                    {
                        $cell->setValue(($record->cv == 1) ? 'Yes' : 'No' );
                    });
                    $counter++;
                }
            });
        })->export('xlsx');
    }

    /**
     * @param Request $request
     * - Integer filter
     * - Integer filterCampus
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function studentApplySummary(Request $request) {
        $homepageAllowedDegreesAndFilter = $this->getHomepageAllowedDegreesAndFilter($request);
        $allowed_degree_id = $homepageAllowedDegreesAndFilter['allowed_degree_id'];
        $filterCampus = $homepageAllowedDegreesAndFilter['filterCampus'];

        $appliedStudent = Student::filterCampus()->filterInternship()->where(function($query) use ($filterCampus) {
            if ($filterCampus) {
                $query->where('students.campus', $filterCampus);
            } else {
                $query;
            }
        })->has('recruitment')
            ->with('recruitment',
                'recruitment.job',
                'recruitment.job.company',
                'recruitment.interview')
            ->whereIn('students.degreeid', $allowed_degree_id)
            ->select(
                'students.studentid',
                'students.name'
            )
            ->get();

        // export excel
        $excelFileName = 'list_student_apply_summary';
        $excel = Excel::create($excelFileName, function ($excel) use ($appliedStudent)
        {
            $excel->sheet('ListStudentApplySummary', function($sheet) use($appliedStudent)
            {
                $sheet->cell('A1', function($cell)
                {
                    $cell->setValue('NIM');
                });
                $sheet->cell('B1', function($cell)
                {
                    $cell->setValue('Student Name');
                });
                $sheet->cell('C1', function($cell)
                {
                    $cell->setValue('Position');
                });
                $sheet->cell('D1', function($cell)
                {
                    $cell->setValue('Company');
                });
                $sheet->cell('E1', function($cell)
                {
                    $cell->setValue('Status');
                });


                $counter=2;
                foreach($appliedStudent as $records)
                {
                    foreach ($records->recruitment as $record) {
                        $sheet->cell('A'.$counter, function($cell)use($records, $record)
                        {
                            $cell->setValue($records->studentid);
                        });
                        $sheet->cell('B'.$counter, function($cell)use($records, $record)
                        {
                            $cell->setValue($records->name);
                        });
                        $sheet->cell('C'.$counter, function($cell)use($records, $record)
                        {
                            $cell->setValue($record->job->name);
                        });
                        $sheet->cell('D'.$counter, function($cell)use($records, $record)
                        {
                            $cell->setValue($record->job->company->name);
                        });
                        $sheet->cell('E'.$counter, function($cell)use($records, $record)
                        {
                            $status = $record->status;
                            if ($status == 'process' && count($record->interview) > 0) {
                                $status = 'interview';
                            }
                            $cell->setValue($status);
                        });
                        $counter++;
                    }
                }
            });
        })->export('xlsx');
    }

    /**
     * @param Request $request
     * - Integer filter
     * - Integer filterCampus
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function jobQuotaSummary(Request $request) {
        // Not Used
        $homepageAllowedDegreesAndFilter = $this->getHomepageAllowedDegreesAndFilter($request);
        $allowed_degree_id = $homepageAllowedDegreesAndFilter['allowed_degree_id'];
        $filterCampus = $homepageAllowedDegreesAndFilter['filterCampus'];
        // End Of Not Used
        $AcceptedCompany = Company::join('jobs','jobs.companyid','=','companies.companyid')
            ->join('recruitments','jobs.id','=','recruitments.jobid')
            ->join('jobsdegrees', 'jobsdegrees.jobid', '=', 'jobs.id')
            ->whereRaw('jobs.deleted_at IS NULL')
            ->whereRaw('recruitments.deleted_at IS NULL')
            ->whereRaw('jobsdegrees.deleted_at IS NULL')
            ->where('status','=',"approved")
            ->whereIn('jobsdegrees.degreeid', $allowed_degree_id)
            ->select('companies.companyid','pic','address','companies.name','email','phone','website')
            ->distinct()
            ->get();
        $AcceptedStudent = Student::filterCampus()->filterInternship()->join('recruitments', 'students.studentid', '=', 'recruitments.studentid')
            ->join('jobs', 'jobs.id', '=', 'recruitments.jobid')
            ->join('companies', 'jobs.companyid', '=', 'companies.companyid')
            ->join('degrees', 'degrees.id', '=', 'students.degreeid')
            ->whereNull('recruitments.deleted_at')
            ->whereNull('jobs.deleted_at')
            ->whereNull('companies.deleted_at')
            ->whereNull('degrees.deleted_at')
            ->whereIn('students.degreeid', $allowed_degree_id)
            ->where('status','=',"approved")
            ->where(function($query) use ($filterCampus) {
                if ($filterCampus) {
                    $query->where('students.campus', $filterCampus);
                } else {
                    $query;
                }
            })
            ->select('students.studentid as studentid', 'students.name as name', 'students.cv as cv', 'students.phone as phone', 'students.sex as sex', 'students.email as email', 'companies.name as company_name', 'degrees.name as degree_name')
            ->distinct()
            ->get();
        $acceptedCompanyIds = collect($AcceptedCompany)->pluck('companyid');

        $companies = Job::with([
                'company',
                'jobsdegrees',
                'jobsdegrees.degree'
        ])
            ->whereHas('company', function ($query) use ($acceptedCompanyIds) {
                $query->whereIn('companyid', $acceptedCompanyIds);
            })
            ->whereHas('jobsdegrees', function($query) use ($allowed_degree_id) {
                $query->whereIn('degreeid', $allowed_degree_id);
            })
            ->select(
                'jobs.id',
                'jobs.companyid',
                'jobs.name',
                'jobs.quota',
                'jobs.location',
                'jobs.allowance',
                'jobs.duration'
            )
            ->get();

//        return response()->json($companies);

        // export excel
        $excelFileName = 'list_job_quota_summary';
        $excel = Excel::create($excelFileName, function ($excel) use ($companies)
        {
            $excel->sheet('ListJobQuotaSummary', function($sheet) use($companies)
            {
                $headers = [
                    'A1' => 'Position',
                    'B1' => 'Company',
                    'C1' => 'Quota',
                    'D1' => 'Location',
                    'E1' => 'Allowance',
                    'F1' => 'Programs',
                    'G1' => 'Duration'
                ];

                foreach ($headers as $key => $val) {
                    $sheet->cell($key, function($cell) use ($val)
                    {
                        $cell->setValue($val);
                    });
                }

                $counter=2;
                foreach($companies as $record)
                {
                    $programs = [];
                    foreach($record->jobsdegrees as $degree) {
                        $programs []= $degree->degree->name;
                    }
                    $datas = [
                        'A' => $record->name,
                        'B' => $record->company->name,
                        'C' => $record->quota,
                        'D' => $record->location,
                        'E' => $record->allowance,
                        'F' => implode(',', $programs),
                        'G' => $record->duration . ' month(s)'
                    ];

                    foreach ($datas as $key => $val) {
                        $sheet->cell($key . $counter, function ($cell) use ($record, $val) {
                            $cell->setValue($val);
                        });
                    }
                    $counter++;
                }
            });
        })->export('xlsx');
    }
}
?>