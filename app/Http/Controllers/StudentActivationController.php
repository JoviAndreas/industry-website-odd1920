<?php

namespace App\Http\Controllers;

use App\Student;
use App\Studentscore;
use App\StudentTemp;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class StudentActivationController extends BaseController
{
    public function syncHeader() {
        set_time_limit(3600);
        $period = '2010';

        $checkTemporaryStudents = StudentTemp::where('period', $period)->get();
        if (count($checkTemporaryStudents) == 0) {
            return redirect()->back()->withErrors(['error' => 'There is no new student header']);
        }

        DB::table('students as s')
            ->join('students_temp as st', function($join) {
                $join->on('s.period', '=', 'st.period')
                    ->on('s.studentid', '=', 'st.studentid');
            })
            ->update([
                's.name' => DB::raw("`st`.`name`"),
                's.school' => DB::raw("`st`.`school`"),
                's.major' => DB::raw("`st`.`major`"),
                's.campus' => DB::raw("`st`.`campus`"),
                's.track' => DB::raw("`st`.`track`"),
                's.email' => DB::raw("`st`.`email`"),
                's.institution' => DB::raw("`st`.`institution`"),
                's.acad_career' => DB::raw("`st`.`acad_career`"),
                's.updated_at' => Carbon::now(),
            ]);

        $insertStudents = DB::table('students_temp as st')
            ->leftJoin('students as s', function($join) {
                $join->on('s.period', '=', 'st.period')
                    ->on('s.studentid', '=', 'st.studentid');
            })
            ->whereNull('s.studentid')
            ->select(DB::raw('st.period, st.studentid, st.name, st.school, st.major, st.campus, st.track, st.institution, st.acad_career, now() created_at'))
            ->get();

        foreach ($insertStudents as $i) {
            DB::table('students')->insert((array) $i);
        }

        $insertUsers = DB::table('students_temp as st')
            ->leftJoin('users as u', function($join) {
                $join->on('u.userid', '=', 'st.studentid');
            })
            ->whereNull('u.userid')
            ->select(DB::raw("st.studentid userid, case when st.track = 'Internship' then 'active' else 'nonactive' end status, '" . '$2y$12$fB9SfUXMMIEuhlPVAs1Yb.HBBOvIN.EZXpBD6uvzwNJYbT8gLptxa' . "' password, 'student' role, 'f012B9SfUX' defaultpassword"))
            ->get();

        foreach ($insertUsers as $i) {
            DB::table('users')->insert((array) $i);
        }

        DB::statement("insert into campus (name, created_at, updated_at) select distinct campus, now(), now() FROM students_temp WHERE campus not in (select name from campus)");
        DB::statement("insert into faculties (name, created_at, updated_at) SELECT distinct school, now(), now() FROM students_temp WHERE school not in (select name from faculties)");
        DB::statement("insert into degrees (name, faculty_id, created_at, updated_at) SELECT distinct program, f.id, now(), now() FROM students_temp st join faculties f on f.name = st.school left join degrees d on d.name = st.program and d.faculty_id = f.id where d.id is null");
        DB::statement("update students ss inner join (SELECT studentid, program, school FROM students_temp) s on ss.studentid = s.studentid inner join faculties f on f.name = s.school inner join degrees d on s.program = d.name and d.faculty_id = f.id set ss.degreeid = d.id");
        DB::statement("update users u inner join students s on s.studentid = u.userid set u.status = 'active' where s.track = 'Internship' and u.status = 'nonactive'");

        StudentTemp::where('period', $period)->delete();

        return redirect()->back()->with(['success' => 'Student synced']);
    }

//    public function initStudentPassword() {
//        set_time_limit(3600);
//        $students = User::where('role', 'student')->get();
//        foreach ($students as $student) {
//            $student->password = bcrypt("intern" . substr($student->userid, -5));
//            $student->save();
//        }
//        echo 'PASSWORD GENERATED';
//    }

    public function generateStudentGrades() {
        if (Auth::user()->role != 'student') {
            return redirect()->back()->withErrors(['error' => 'You are not a student']);
        }
        $studentProfile = Student::where('studentid', Auth::user()->userid)->first();
        if ($studentProfile == null) {
            return redirect()->back()->withErrors(['error' => 'You are not a student']);
        }

        $period = $studentProfile->period;
        $client = new \SoapClient('http://bcs.binus.ac.id:8200/PSIGW/PeopleSoftServiceListeningConnector/N_TPON_SERVICES.1.wsdl', [
            'exceptions'=>true,
            'trace'=>1,
            'cache_wsdl'=>WSDL_CACHE_NONE
        ]);

        $result = null;
        try {
            $parameters = [
                'parameters' => [
                    'Institution' => $studentProfile->institution,
                    'AcadCareer' => $studentProfile->acad_career,
                    'Strm' => $period,
                    'StudentId' => $studentProfile->studentid
                ]
            ];
            $result = $client->__soapCall('N_TPON_GET_CRSE_DATA', $parameters);
        } catch (\SoapFault $fault) {
            trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);
        }
        $data = $result->N_TPON_GET_CRSE_DATA_RESP_DTL;
        $grades = $data->N_GET_DATA_SCORE_HIST;
        $nim = $studentProfile->studentid;
        foreach ($grades as $grade) {
            Studentscore::updateOrCreate([
                'studentid' => $nim,
                'code' => $grade->CourseCode
            ], [
                'name' => $grade->CourseName,
                'grade' => $grade->Grade
            ]);
        }
        return redirect()->back()->with(['sukses' => 'Your grades synced, see grades in the bottom of this page.']);
    }
}
?>