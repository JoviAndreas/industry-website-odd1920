<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Http\Request;
use Dompdf\Dompdf;
use Storage;
use Response;
use Auth;
use DB;
use Validator;
use Hash;
use App\Recruitment;
use App\Mailbox;
use App\Company;
use App\Student;
use App\User;
use App\Interview;
use App\Feedback;
use App\Job;
use App\Supervisor;


class SupervisorController extends BaseController{

	use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

	// Manage > Supervisor page
	// open manage supervisor page for company
	// routes: /supervisor
	public function indexCompany()
	{
		$getState = Company::where('companyid', '=', Auth::user()->userid)->first();
		$user = Auth::user();
		$datas = Supervisor::where('companyid', Auth::user()->userid)->get();
 		$mailboxCount = Mailbox::where('type', '=', 'mailbox')
			->where('status', '=', 'unseen')
			->where('recipientid', '=',Auth::user()->userid)
			->count();

		$listJob = Job::where('companyid', '=', Auth::user()->userid)->get();

		return view("company.supervisor",
			[
				'user' => $user,
				'mailboxCount' => $mailboxCount,
				'state' => $getState->state,
				'jobs' => $listJob,
				'datas' => $datas,
				'notifCount' => app('App\Http\Controllers\MailboxController')->getCountNotification(),
				'notif' => app('App\Http\Controllers\MailboxController')->getLatestNotification()
			]);
	}

	//Insert supervisor
	public function insertCompany(Request $request)
	{
		//validasi kosong
		if($request->input('txtiName') =='' || $request->input('txtiPosition') =='' || $request->input('txtiEmail') =='' || $request->input('txtiPhone') =='' || $request->input('txtiAddress') =='')
		{
			return redirect('supervisor')->with('fyi', 'All field must be filled.')->withInput();
		}
		else
		{
			//insert jobnya
			$objSpv = new Supervisor;
			$objSpv->companyid = Auth::user()->userid;
			$objSpv->name = $request->input('txtiName');
			$objSpv->position = $request->input('txtiPosition');
			$objSpv->email = $request->input('txtiEmail');
			$objSpv->phone = $request->input('txtiPhone');
			$objSpv->address = $request->input('txtiAddress');
			$objSpv->token = str_replace('.', 'H', str_replace("$", "S", str_replace("/", "X", Hash::make(Auth::user()->userid.rand(1, 1000).$request->input('txtiName').$request->input('txtiPosition')))));
			$objSpv->save();
			return redirect('supervisor')->with('fyi', 'Berhasil');
		}
	}

	//Update Supervisor
	public function updateCompany(Request $request)
	{
		//validasi kosong
		if($request->input('txtuName') =='' || $request->input('txtuPosition') =='' || $request->input('txtuEmail') =='' || $request->input('txtuPhone') =='' || $request->input('txtuAddress') =='')
		{
				return redirect('supervisor')->with('fyi', 'All field must be filled.');
		}
		else
		{
			//update job
			Supervisor::where('token', '=', $request->input('txtuToken'))->update(
				[
					'name' => $request->input('txtuName'),
					'position' => $request->input('txtuPosition'),
					'email' => $request->input('txtuEmail'),
					'phone' => $request->input('txtuPhone'),
					'address' => $request->input('txtuAddress')
				]
			);
			return redirect('supervisor')->with('fyi', 'Berhasilupdate');
		}
	}

	//delete supervisor
	public function deleteCompany($token)
	{

		$spv = Supervisor::where('token', $token)->first();
		$student = Student::where('supervisorid',$spv->id)->get();
		if(sizeof($student) != 0)
		{
			Student::where('supervisorid',$spv->id)->update(['supervisorid' => null]);
		}
		$spv->delete();
		return redirect('/supervisor')->with('fyi', 'Berhasildelete');
	}

	// Manage > Supervisor
	// Open assign supervisor for each student page
	// routes: /assignStudent
	public function assignCompany(){
		$getState = Company::where('companyid', '=', Auth::user()->userid)
			->first();
		$user = Auth::user();

 		$mailboxCount = Mailbox::where('type', '=', 'mailbox')
		 	->where('status', '=', 'unseen')
			->where('recipientid', '=',Auth::user()->userid)
			->count();

		$listSupervisor = Supervisor::where('companyid', Auth::user()->userid)->get();

		foreach ($listSupervisor as $spv) {
			$spv->list_student = Student::where('supervisorid', '=', $spv->id)->get();
		}

		$listAccepted = Recruitment::join('jobs', 'jobs.id', '=', 'recruitments.jobid')
			->join('students', 'students.studentid', '=', 'recruitments.studentid')
			->join('degrees', 'degrees.id', '=', 'students.degreeid')
			->whereNull('jobs.deleted_at')
			->whereNull('students.deleted_at')
			->whereNull('degrees.deleted_at')
			->where('status', 'approved')
			->where('companyid', Auth::user()->userid)
			->whereNull('students.supervisorid')
			->select(
				'students.studentid',
				'students.name',
				'degrees.name as degree',
				'students.major',
				'students.supervisorid'
			)
			->orderBy('students.studentid', 'asc')
			->get();

		$total_approved = Recruitment::join('jobs', 'jobs.id', '=', 'recruitments.jobid')
			->join('students', 'students.studentid', '=', 'recruitments.studentid')
			->join('degrees', 'degrees.id', '=', 'students.degreeid')
			->whereNull('jobs.deleted_at')
			->whereNull('students.deleted_at')
			->whereNull('degrees.deleted_at')
			->where('status', 'approved')
			->where('companyid', Auth::user()->userid)
			->select(
				'students.studentid',
				'students.name',
				'degrees.name as degree',
				'students.major',
				'students.supervisorid'
			)
			->orderBy('students.studentid', 'asc')
			->get()
			->count();

		return view("company.assign",
			[
				'user' => $user,
				'mailboxCount' => $mailboxCount,
				'state' => $getState->state,
				'listStudent' => $listAccepted,
				'listspv' => $listSupervisor,
				'total_approved' => $total_approved,
				'notifCount' => app('App\Http\Controllers\MailboxController')->getCountNotification(),
				'notif' => app('App\Http\Controllers\MailboxController')->getLatestNotification()
			]);
	}

	// Manage > Supervisor
	// remove student from his/her supervisor
	// routes: /removeStudentFromSpv/
	public function removeStudentFromSpv($spvid, $studentid){
		$supervisor = Supervisor::where('id', '=', $spvid)->first();
		if($supervisor == null){
			return redirect('/assignStudent')->with('fyi', 'Error supervisor ID is invalid');
		}
		else if($supervisor->companyid != Auth::user()->userid){
			return redirect('/assignStudent')->with('fyi', "Error cannor edit the supervisor's students list");
		}

		$student = Student::where('studentid', '=', $studentid)->first();
		if($student == null){
			return redirect('/assignStudent')->with('fyi', 'Error student ID is invalid');
		}
		else if($student->supervisorid != $supervisor->id){
			return redirect('/assignStudent')->with('fyi', "Error student's supervisor not match");
		}
		else{
			$student->supervisorid = null;
			$student->save();
		}
		return redirect('/assignStudent')->with('fyi', "Berhasilupdate");
	}

	// Manage > Supervisor
	// submit assign supervisor to the student
	// routes: /submitAssignSPV
	public function updateAssignCompany(Request $request){
		$array_update = [];

		$supervisorID = $request->spv_id;
		$supervisor = Supervisor::where('id', '=', $supervisorID)->first();
		if($supervisor == null){
			return redirect('/assignStudent')->with('fyi', 'Error supervisor data is invalid');
		}
		else if($supervisor->companyid != Auth::user()->userid){
			return redirect('/assignStudent')->with('fyi', 'Error supervisor data is invalid');
		}
		else{
			foreach($request->all() as $key => $value)
			{
				//cek keseluruhan request yang namenya diawali 'spv_'
				if(substr($key, 0, 9) === "checkbox_")
				{
					$student_id = substr($key, 9);
					$student = Student::where('studentid', '=', $student_id)->first();
					if($student == null){
						return redirect('/assignStudent')->with('fyi', 'Invalid student ID');
					}
					else if(!empty($student->supervisorid)){
						return redirect('/assignStudent')->with('fyi', 'Student you choose has already assigned to another person');
					}
					array_push($array_update, [
						'studentid' => $student_id,
						'supervisor' => $supervisorID
					]);
				}
			}
		}
		if(empty($array_update)){
			return redirect('/assignStudent')->with('fyi', 'No data saved');
		}
		else{
			foreach ($array_update as $value) {
				Student::where('studentid', '=', $value['studentid'])->update(
					['supervisorid' => $value['supervisor']]
				);
			}
		}
		return redirect('/assignStudent')->with('fyi', 'Berhasil');
	}

	public function exportCompany()
	{
		$data = Student::join('supervisors', 'students.supervisorid', '=', 'supervisors.id')
			->join('degrees', 'degrees.id', '=', 'students.degreeid')
			->whereNull('supervisors.deleted_at')
			->whereNull('degrees.deleted_at')
			->where('supervisors.companyid', Auth::user()->userid)
			->where('students.supervisorid', '!=', '0')
			->select(
				'studentid', 'students.name as name', 'degrees.name as degree', 'major', 'supervisors.name as spvname')->orderBy('studentid', 'asc'
				)
			->get();
		\Excel::create('List Student with Supervisor', function($excel) use($data)
			{
				$excel->sheet('List Student', function($sheet) use($data)
					{
						// first row styling and writing content
				        		$sheet->mergeCells('A1:E1');
				        		$sheet->row(1, function ($row) {
						            $row->setFontFamily('Comic Sans MS');
						            $row->setFontSize(18);
						            $row->setAlignment('center');
						            $row->setValignment('center');
				        		});

				       		$sheet->row(1, array('List Student with Supervisor'));

				        		$sheet->cell('A3', function($cell)
							{
			   					$cell->setValue('NIM');
			   					$cell->setAlignment('center');
			   					$cell->setBorder('thin', 'thin', 'thin', 'thin');
			   				});
						$sheet->cell('B3', function($cell)
							{
			   					$cell->setValue('Name');
			   					$cell->setAlignment('center');
			   					$cell->setBorder('thin', 'thin', 'thin', 'thin');
			   				});
						$sheet->cell('C3', function($cell)
							{
			   					$cell->setValue('Degree');
			   					$cell->setAlignment('center');
			   					$cell->setBorder('thin', 'thin', 'thin', 'thin');
			   				});
						$sheet->cell('D3', function($cell)
							{
			   					$cell->setValue('Major');
			   					$cell->setAlignment('center');
			   					$cell->setBorder('thin', 'thin', 'thin', 'thin');
			   				});
						$sheet->cell('E3', function($cell)
							{
			   					$cell->setValue('Supervisor');
			   					$cell->setAlignment('center');
			   					$cell->setBorder('thin', 'thin', 'thin', 'thin');
			   				});

				       		// second row styling and writing content
				        		$sheet->row(3, function ($row)
							{
								// call cell manipulation methods
								$row->setFontFamily('Comic Sans MS');
								$row->setFontSize(12);
								$row->setAlignment('center');
							});

				        		$counter=4;
					        	foreach($data as $record)
			        			{
							$sheet->cell('A'.$counter, function($cell)use($record)
								{
									 $cell->setValue($record->studentid);
									 $cell->setAlignment('center');
									 $cell->setBorder('thin', 'thin', 'thin', 'thin');
								});
							$sheet->cell('B'.$counter, function($cell)use($record)
								{
									 $cell->setValue($record->name);
									 $cell->setAlignment('center');
									 $cell->setBorder('thin', 'thin', 'thin', 'thin');
								});
							$sheet->cell('C'.$counter, function($cell)use($record)
								{
									 $cell->setValue($record->degree);
									 $cell->setAlignment('center');
									 $cell->setBorder('thin', 'thin', 'thin', 'thin');
								});
							$sheet->cell('D'.$counter, function($cell)use($record)
								{
									 $cell->setValue($record->major);
									 $cell->setAlignment('center');
									 $cell->setBorder('thin', 'thin', 'thin', 'thin');
								});
							$sheet->cell('E'.$counter, function($cell)use($record)
								{
									 $cell->setValue($record->spvname);
									 $cell->setAlignment('center');
									 $cell->setBorder('thin', 'thin', 'thin', 'thin');
								});
							$counter++;
						}
		        			});
			})->export('xlsx');
	}
}
?>
