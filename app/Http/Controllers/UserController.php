<?php

namespace App\Http\Controllers;

use App\ActiveStudent;
use App\Bipp;
use App\Company;
use App\Degree;
use App\Department;
use App\Description;
use App\Faculty;
use App\Mailbox;
use App\Recruitment;
use App\Services\EmailService;
use App\Student;
use App\User;
use Auth;
use Carbon\Carbon;
use DB;
use Excel;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Mail;
use Validator;

class UserController extends BaseController
{

	// Others > Department Account page
	// routes: /addDepartmentAccount
	public function addDepartmentAccount(){
		$departments = Department::join('users', 'users.userid', '=', 'departments.departmentid')
			->whereNull('users.deleted_at')
			->select(
				'users.userid as userid',
				'departments.name as name',
				'users.defaultpassword as defaultpassword',
				'users.status as status',
				'departments.email as email',
				'departments.campus as campus',
				'departments.head_of_program as head_of_program',
				'departments.access_role as access_role'
				)
			->get();
		foreach($departments as $d){
            $roles = Department::getRoleFor($d);

			$head_of_faculty = $roles['head_of_faculty'];
			$head_of_program = $roles['head_of_program'];
			$degrees_can_be_chosen = $roles['degrees_can_be_chosen'];

			$arr = [];
			if ($head_of_program == NULL) { // New User
				$d->access_roles = [];
				continue;
			}
			else if ($head_of_faculty === 'all') { // Superadmin
				array_push($arr, 'All');
			} else if ($head_of_faculty !== NULL) { // Admin Faculty
                array_push($arr, Faculty::where('id', $head_of_faculty)->first()->name);
			} else { // HOP
				foreach($degrees_can_be_chosen as $degree){
					array_push($arr, $degree->name);
				}
			}
			
			$d->access_roles = $arr;
			
			/*for filter departments that can be seen (1)*/
			if ($head_of_faculty === 'all') {
				$d->role = 'all';
				$d->head_of_faculty = 'all';
			} else if ($head_of_faculty != NULL) {
				$d->role = $head_of_faculty; //facultyId
				$d->head_of_faculty = $head_of_faculty;
			} else {
				$facultyId = Degree::where('id', $head_of_program)->first()->faculty_id;
				$d->head_of_faculty = null;
				$d->role = $facultyId; //hop -> id fakultas
				$d->head_of_program = $head_of_program;
			}
			/* */
			
		}

		/* or filter departments that can be seen (2) */
		$currentUserRole = Department::generateRoleSession();
		$filteredDepartments = $departments->filter(function ($department) use ($currentUserRole) {
			// if (empty($department->access_roles)) {
			// 	return true;
			// }
			if ($currentUserRole['head_of_faculty'] === 'all') {
				return true;
			} else if ($currentUserRole['head_of_faculty'] !== NULL) {
				return $currentUserRole['head_of_faculty'] == $department->role;
			} else {
				return $currentUserRole['head_of_program'] == $department->head_of_program;
			}
		})->values();
		/* */

		return view('department.add_department_account')->with([
			'departments' => $filteredDepartments
		]);
	}

	public function inactiveAccountDepartment($userid){
		$dep = User::where('userid', '=', $userid)->first();
		$dep->status = 'nonactive';
		$dep->save();

		return redirect('/addDepartmentAccount');
	}

	public function activeAccountDepartment($userid){
		$dep = User::where('userid', '=', $userid)->first();
		$dep->status = 'active';
		$dep->save();

		return redirect('/addDepartmentAccount');
	}

	public function insertNewDepartmentAccount(Request $request){
		$messages =
			[
				'txt_userid.unique' => 'User ID is used!'
			];

		$rules =
			[
				'txt_userid' => 'required|unique:users,userid',
				'txt_name' => 'required',
				'txt_email' => 'required|email',
				'txt_password'=> 'required|min:6',
				'txt_confirm_password'=> 'required|same:txt_password',
				'cmb_campus' => 'required',
				'cmb_access_role' => 'required'
			];

		$validator = Validator::make($request->all(), $rules, $messages);

		if ($validator->fails())
		{
			return redirect('/addDepartmentAccount')
				->withErrors($validator)
				->withInput();
		}
		else{
			$new_user = new User;
			$new_user->userid = $request->input('txt_userid');
			$new_user->password = bcrypt($request->input('txt_password'));
			$new_user->role = 'department';
			$new_user->status = 'active';
			$new_user->defaultpassword = $request->input('txt_password');
			$new_user->save();

			$new_dep = new Department;
			$new_dep->departmentid = $request->input('txt_userid');
			$new_dep->name = $request->input('txt_name');
			$new_dep->email = $request->input('txt_email');
			$new_dep->campus = ($request->cmb_campus == 'all') ? null : $request->input('cmb_campus');
			$new_dep->access_role = $request->input('cmb_access_role');
			$new_dep->photo = '';
			$new_dep->save();

			return redirect('/addDepartmentAccount')->with([
				'fyi' => 'Success add account'
			]);
		}
	}

	// Others > Department Account
	// open page for updating department account
	public function updateDepartmentAccount($userid){
		$department = Department::join('users', 'users.userid', '=', 'departments.departmentid')
			->whereNull('users.deleted_at')
			->where('departmentid', '=', $userid)
			->select(
				'departments.departmentid as departmentid',
				'departments.name as name',
				'departments.email as email',
				'departments.head_of_program as head_of_program',
				'departments.access_role',
				'departments.campus as campus',
				'users.defaultpassword as defaultpassword'
				)
			->first();
		if($department == null){
			return redirect('/addDepartmentAccount')->with([
				'error' => 'update error: department account not found'
			]);
		}
		else{
			return view('department.update_department_account')->with([
				'department' => $department
			]);
		}
	}

	// Others > Department Account
	// submit update for department account
	public function submitUpdateDepartmentAccount($userid, Request $request){
		$department = Department::where('departmentid', '=', $userid)->first();
		if($department == null){
			return redirect('/addDepartmentAccount')->with([
				'error' => 'update error: department account not found'
			]);
		}
		else{
			$user = $department->user()->first();
			$rules =
				[
					'txt_name' => 'required',
					'txt_email' => 'required|email',
					'txt_password'=> 'required|min:6',
					'txt_confirm_password'=> 'required|same:txt_password',
					'cmb_campus' => 'required',
					'cmb_access_role' => 'required'
				];

			$validator = Validator::make($request->all(), $rules);

			if ($validator->fails())
			{
				return redirect('/updateDepartmentAccount/'.$userid)
					->withErrors($validator)
					->withInput();
			}
			else{
				$user->defaultpassword = $request->input('txt_password');
				$user->save();

				$department->name = $request->input('txt_name');
				$department->email = $request->input('txt_email');
				$department->photo = '';
				if ($request->cmb_campus == 'all') {
					$department->campus = null;
				} else {
					$department->campus = $request->input('cmb_campus');
				}
				$department->access_role = $request->input('cmb_access_role');

				$department->save();

				return redirect('/addDepartmentAccount')->with([
					'fyi' => 'Success update account'
				]);
			}
		}
	}

	/**
	 * view register form (role guest)
	 * @return html views/register.blade.php
	 */
	public function register()
	{
		
		$degrees = Degree::all();
		$desc = Description::where('type','=','bipp')->first();
		
		return view('register')->with(
			[
				'desc'=>$desc,
                'faculties' => Faculty::all(),
				'degrees' => $degrees,
				'role' => 'guest'
			]
		);
	}

	/**
	 * view register form (role department)
	 * routes: /registerStudentByDepartment
	 * @return html views/register.blade.php
	 */
	public function registerByDepartment()
	{
		$degrees = Degree::all();
		return view('register')->with(
			[
				'degrees' => $degrees,
				'role' => 'department'
			]
		);
	}

	/**
	 * generate random string consisting of upper + lower alphabets + numeric characters
	 * @param  integer $length length of the string (default = 10)
	 * @return string result string
	 */
	private function generateRandomString($length = 10)
	{
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++)
		{
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}

	// User Management > Account Approval page
	// view account approval page
	//routes: /accountapproval
	public function accountapproval()
	{
		$allowed_degree_id = DepartmentController::get_allowed_degree_list();

		$students_in_active_student = Student::join('degrees', 'degrees.id', '=', 'students.degreeid')
			->join('users', 'users.userid', '=', 'students.studentid')
			->whereNull('degrees.deleted_at')
			->whereNull('users.deleted_at')
			->where('users.status', '=', 'nonactive')
			->whereExists(function ($query){
				$query->select(DB::raw(1))
					->from('activestudents')
					->whereNull('activestudents.deleted_at')
					->whereRaw('students.studentid = activestudents.studentid');
			})
			->whereIn('students.degreeid', $allowed_degree_id)
			->select('students.*', 'degrees.name as degree_name')
			->distinct()
			->get();

		$students_not_in_active_student = Student::join('users', 'users.userid', '=', 'students.studentid')
			->join('degrees', 'degrees.id', '=', 'students.degreeid')
			->whereNull('users.deleted_at')
			->whereNull('degrees.deleted_at')
			->where('users.status', '=', 'nonactive')
			->whereNotExists(function($query){
				$query->select(DB::raw(1))
					->from('activestudents')
					->whereNull('activestudents.deleted_at')
					->whereRaw('students.studentid = activestudents.studentid');
			})
			->whereIn('students.degreeid', $allowed_degree_id)
			->select('students.*', 'degrees.name as degree_name')
			->distinct()
			->get();

		$Companies = Company::join('users','users.userid','=','companies.companyid')
			->whereNull('users.deleted_at')
			->select('companies.*')
			->where('users.status','=','nonactive')
			->distinct()
			->get();

		$unregistered_student = DB::select(DB::raw('select activestudents.studentid, activestudents.name from activestudents where activestudents.deleted_at IS NULL and activestudents.studentid NOT IN (select students.studentid from students where students.deleted_at IS NULL)'));

		$approval_rejected_student = Student::join('users', 'users.userid', '=', 'students.studentid')
			->whereNotNull('users.deleted_at')
			->where('users.status', '=', 'nonactive')
			->select('students.name as name', 'students.studentid as studentid')
			->onlyTrashed()
			->get();

		$approval_rejected_company = Company::join('users', 'users.userid', '=', 'companies.companyid')
				->whereNotNull('users.deleted_at')
				->where('users.status', '=', 'nonactive')
				->select('companies.name as name', 'companies.companyid as companyid', 'companies.email as email')
				->onlyTrashed()
				->get();

		return view('department.accountapproval')->with(
			[
				'students_in_active_student' => $students_in_active_student,
				'students_not_in_active_student' => $students_not_in_active_student,
				'unregistered_student' => $unregistered_student,
				'Companies' => $Companies,
				'approval_rejected_student' => $approval_rejected_student,
				'approval_rejected_company' => $approval_rejected_company
			]);
	}

	// User Management > Account Approval
	// unreject the student which is rejected
	// routes: /unreject/
	public function unrejectapproval($id){
		$usr = User::where('userid', '=', $id)->onlyTrashed()->first();
		$student = Student::where('studentid', '=', $id)
			->onlyTrashed()
			->first();
		if($usr == null || $student == null){
			return redirect('/accountapproval')->with([
				'errors' => 'User is not found'
			]);
		}
		if($usr->status != 'nonactive'){
			return redirect('/accountapproval')->with([
				'errors' => 'User is deleted, not rejected'
			]);
		}
		$usr->restore();
		$student->restore();
		return redirect('/accountapproval')->with([
			'fyi' => 'Success Unreject Student'
		]);
	}

	// User Management > Account Approval
	// unreject the company which is rejected
	// routes: /unrejectcompany/
	public function unrejectapprovalcompany($id){
		$usr = User::where('userid', '=', $id)->onlyTrashed()->first();
		$company = Company::where('companyid', '=', $id)
			->onlyTrashed()
			->first();
		if($usr == null || $company == null){
			return redirect('/accountapproval')->with([
				'errors' => 'User is not found'
			]);
		}
		if($usr->status != 'nonactive'){
			return redirect('/accountapproval')->with([
				'errors' => 'User is deleted, not rejected'
			]);
		}
		$usr->restore();
		$company->restore();
		return redirect('/accountapproval')->with([
			'fyi' => 'Success Unreject Company'
		]);
	}

	/**
	 * approve students who are registered in 3+1 active students list
	 * (approve all)
	 * routes: /updateapprovestudent
	 */
	public function updateapprovestudent(Request $request)
	{
		$allowed_degree_id = DepartmentController::get_allowed_degree_list();

		$inputs = $request->all();

		$arrayStudent = [];

		foreach ($inputs as $key => $value) {
			if(substr($key, 0, 9) === "checkbox1"){
				$nim = substr($key, 9);
				$student = User::where('userid', '=', $nim)->first();
				if(empty($student)){
					$errors = 'User with NIM '.$nim.' not found!';
					break;
				}
				else{
					if(array_search($student->degreeid, $allowed_degree_id) === FALSE){
						$errors = 'You cannot approve this student';
						break;
					}
					else{
						array_push($arrayStudent, $student);
					}
				}
			}
		}
		if(!empty($errors)){
			return redirect('/accountapproval')->with('errors', $errors);
		}
		else{
			foreach ($arrayStudent as $s) {
				$s->status = 'active';
				$s->save();

				$usr = User::where('userid', '=', $s->userid)
					->join('students','users.userid','=','students.studentid')
					->whereNull('students.deleted_at')
					->first();

				EmailService::createNewAccountApprovalEmail($usr);
			}
		  	return redirect('/accountapproval')->with('fyi', "Successfully approved!");
		}
	}

	// User Management > Account Approval
    // submit approve selected students (approve all)
    // this is for the not-listed students
	// routes: /updateapprovestudentnotlisted
	public function updateapprovestudentnotlisted(Request $request)
	{
		$allowed_degree_id = DepartmentController::get_allowed_degree_list();

		$inputs = $request->all();

		$arrayStudent = [];

		foreach ($inputs as $key => $value) {
			if(substr($key, 0, 9) === "checkbox2"){
				$nim = substr($key, 9);
				$student = User::where('userid', '=', $nim)->first();
				if(empty($student)){
					$errors = 'User with NIM '.$nim.' not found!';
					break;
				}
				else{
					if(array_search($student->degreeid, $allowed_degree_id) === FALSE){
						$errors = 'You cannot approve this student';
						break;
					}
					else{
						array_push($arrayStudent, $student);
					}
				}
			}
		}
		if(!empty($errors)){
			return redirect('/accountapproval')->with('errors', $errors);
		}
		else{
			foreach ($arrayStudent as $s) {
				$s->status = 'active';
				$s->save();

				$usr = User::where('userid', '=', $s->userid)
					->join('students','users.userid','=','students.studentid')
					->whereNull('students.deleted_at')
					->first();
				EmailService::createNewAccountApprovalEmail($usr);
			}
		  	return redirect('/accountapproval')->with('fyi',"Successfully approved!");
		}
	}

	/**
	 * approve companies
	 * Notes: multi select
	 * Role: department
	 * routes: /updateapprovecompany
	 * @param  Request $request post request from account approval page
	 * @return html redirect with errors or fyi
	 */
	public function updateapprovecompany(Request $request)
	{
		$inputs = $request->all();
		
		$arrayCompany = [];
		$arrayUserCompany = [];
		$arrayIsBippCompany = [];
		
		foreach ($inputs as $key => $value) {
			if(substr($key, 0, 9) === "checkbox3"){
				$companyid = substr($key, 9);
				$company = Company::where('companyid','=', $companyid)->first();
				$userCompany = User::where('userid', '=', $companyid)->first();
				if(empty($userCompany)){
					$errors = 'Invalid inputs !';
				}
				else{
					array_push($arrayUserCompany, $userCompany);
					array_push($arrayCompany, $company);
				}
			}else if(substr($key,0,12) === "checkboxBipp"){
				array_push($arrayIsBippCompany,substr($key,12));
			}
		}
		if(!empty($errors)){
			return redirect('/accountapproval')->with('errors', $errors);
		}
		else{
			foreach($arrayCompany as $c){
				foreach($arrayIsBippCompany as $ic){
					if($c->id == $ic){
						$c->bipp_id = 1;
						$c->save();
					}
				}
			}
			foreach ($arrayUserCompany as $uc) {
				$uc->status = 'active';
				$uc->save();
				
				//untuk role company
				$usr = User::where('userid', '=', $uc->userid)
					->join('companies','users.userid','=','companies.companyid')
					->whereNull('companies.deleted_at')
					->first();

				EmailService::createNewAccountApprovalEmail($usr);
			}
		  	return redirect('/accountapproval')->with('fyi',"Successfully approved!");
		}
	}

	/**
	 * active student list page
	 * Role: department
	 * routes: /activestudentlist
	 * @return html views/department/activestudentlist.blade.php
	 */
	public function activestudentlist()
	{
		$students = ActiveStudent::get();
		return view('department.activestudentlist')->with(
			[
				'students' => $students
			]);
	}

	/**
	 * insert active students list using excel file
	 * Role: department
	 * routes: /insertactivestudent
	 * @param  Request $request post request from active student list page
	 * @return html redirect with errors or status
	 */
	public function insertactivestudent(Request $request)
	{
		$messages = array
			(
    			'excel_file.required' => 'Excel File Not Found!'
			);

		$rules = array
			(
       			'excel_file' => 'required'
    		);

   		$validator = Validator::make($request->all(), $rules, $messages);

    	if ($validator->fails())
    	{
    		return redirect('/activestudentlist')->withErrors($validator)->withInput();
    	}
   		else
		{
			$arrList = array();
            Excel::load(
				$request['excel_file'],
				function ($reader) use ($arrList)
				{
	        		$arrList = array();
	            	foreach ($reader->toArray() as $row)
	            	{
	            		if(strcmp($row['nim'],'17XXXXXXXXX') == 0)
	            		{
	            			continue;
	            		}
	            		else
	            		{
	            			$nim = $row['nim'];
        					$name = $row['name'];

			        		//check jika sudah ada studentnya di table students dan table activestudents
				        	if(ActiveStudent::where('studentid', '=', $nim)->get()->count() > 0){
				        		//jika sudah ada
				        		continue;
				        	}

				        	//create token
				        	$token = Hash::make(Auth::user()->userid.$nim.date('Y-m-d H:i:s'));
							$token = str_replace('.', 'H', str_replace("$", "S", str_replace("/", "X", $token)));

	        				//insert data
				        	$new_active_student = new ActiveStudent;
				        	$new_active_student->studentid = $nim;
				        	$new_active_student->name = $name;
				        	$new_active_student->token = $token;
				        	$new_active_student->save();
	            		}
	            	}
	       		}
	       	);
		  	return redirect('/activestudentlist')->with('status',"All Active Student successfully inserted!");
   		}
	}

	/**
	 * delete active student from the list
	 * Role: department
	 * routes: /deleteactivestudent
	 * @param  Request $request post request from active student list page
	 * @return redirect with errors or status
	 */
	public function deleteactivestudent(Request $request)
	{
		$messages = array
			(
    			'token.required' => 'Input not complete!'
			);

		$rules = array
			(
       			'token' => 'required'
    		);

   		$validator = Validator::make($request->all(), $rules, $messages);

    	if ($validator->fails())
    	{
    		return redirect('/activestudentlist')->withErrors($validator)->withInput();
    	}
   		else
		{
			$token = $request->input('token');

			$activestudent = ActiveStudent::where('token', '=', $token)->first();
			$activestudent->delete();

		  	return redirect('/activestudentlist')->with('status',"Successfully deleted!");
   		}
	}

	// User Management > Account Approval
    // submit approve selected student (approve each)
	// routes: /approveaccount/
	public function approveaccount(Request $request, $id)
	{
		$allowed_degree_id = DepartmentController::get_allowed_degree_list();

		$usr = User::where('userid', '=', $id)->first();
		if($usr == null){
			return redirect('/accountapproval')->with([
				'errors' => 'User is not found'
			]);
		}
		else{
			if($usr->role == 'student'){
				$student = $usr->student()->first();
				if(array_search($student->degreeid, $allowed_degree_id) === FALSE){
					return redirect('/accountapproval')->with([
						'errors' => 'You cannot approve this student'
					]);
				}
			}
		}

		//update status jadi active
   	$usr = User::where('userid', '=', $id)->first();
		// User::where('userid', '=', $id)->update([ 'status' => 'active']);
		if ($request->bipp == 'true') {
            $companyBIPP = Company::where('companyid', $id)->first();
            $companyBIPP->bipp_id = Bipp::first()->id;
			$companyBIPP->save();
		} else if ($request->bipp == 'false') {
            $companyBIPP = Company::where('companyid', $id)->first();
			$companyBIPP->bipp_id = NULL;
			$companyBIPP->save();
		}
		$usr->status = 'active';
		$usr->save();

		//kirim email ke email yg terdaftar
		if($usr->role == 'student')
		{
			//untuk role student
			$usr = User::where('userid', '=', $id)
				->join('students','users.userid','=','students.studentid')
				->whereNull('students.deleted_at')
				->first();

			EmailService::createNewAccountApprovalEmail($usr);
		}
		else
		{
			//untuk role company
			$usr = User::where('userid', '=', $id)
				->join('companies','users.userid','=','companies.companyid')
				->whereNull('companies.deleted_at')
				->first();

			EmailService::createNewAccountApprovalEmail($usr);
		}
		return back()->with('fyi','Success Approved Account!');
	}

	// User Management > Account Approval
    // submit reject selected student (reject each)
	// routes: /rejectaccount/
	public function rejectaccount($id)
	{
		$allowed_degree_id = DepartmentController::get_allowed_degree_list();

		$usr = User::where('userid', '=', $id)->first();
		if($usr == null){
			return redirect('/accountapproval')->with([
				'errors' => 'User is not found'
			]);
		}
		else{
			if($usr->role == 'student'){
				$student = $usr->student()->first();
				if(array_search($student->degreeid, $allowed_degree_id) === FALSE){
					return redirect('/accountapproval')->with([
						'errors' => 'You cannot approve this student'
					]);
				}
			}
		}

		//kirim email ke email yg terdaftar
		if($usr->role == 'student')
		{
			//untuk role student
			$usr = User::where('userid', '=', $id)
				->join('students','users.userid','=','students.studentid')
				->whereNull('students.deleted_at')
				->first();

			EmailService::createNewAccountRejectionEmail($usr);

			Student::where('studentid', $id)->delete();
		}
		else
		{
			//untuk role company
			$usr = User::where('userid', '=', $id)
				->join('companies','users.userid','=','companies.companyid')
				->whereNull('companies.deleted_at')
				->first();

			EmailService::createNewAccountRejectionEmail($usr);

			Company::where('companyid', $id)->delete();
		}
		//user data is deleted if rejected
		User::where('userid', $id)->delete();
		return back()->with('fyi','Success Reject Account!');
	}

	//Registration via halaman depan
	public function insertUser(Request $req)
	{
		if($req->nim == null)
		{
			//Register as Company
			$rules =
				[
			        'name' => 'required',
			        'web'=> 'required',
			        'email'=> 'required|email',
			        'desc'=> 'required',
			        'phone'=> 'required'
				];

			$validator = Validator::make($req->all(), $rules);

        	if ($validator->fails())
        	{
        		return redirect('signin')
                    ->withErrors($validator)
                    ->withInput();
            }
            else
            {
				//generate userid
				$name = $req->name;
				$name = trim($name);
				$userid = strtolower($name);
				$userid = str_replace('pt.','',$userid);
				$userid = str_replace('cv.','',$userid);
				$userid = str_replace('pt ','',$userid);
				$userid = str_replace('cv ','',$userid);
				$userid = trim($userid);
				$userid = str_replace(' ','',$userid);
				$userid = preg_replace('/[^A-Za-z0-9\-]/', '', $userid);
				if(strlen($userid) <6)
				{
					$userid .= $userid;
				}
				else if(strlen($userid) >10)
				{
					$userid= substr($userid,0,10);
				}
				$checkIfExist = Company::where('companyid', 'like', $userid.'%')->get();
				if(sizeof($checkIfExist)>0)
				{
					$userid = $userid.sizeof($checkIfExist);
				}

				//website: always have http:// or https:// in front
				$web = null;
				if(strpos($req->web, 'http://') === false && strpos($req->web, 'https://') === false){
					$web = 'http://'.$req->web;
				}
				else{
					$web = $req->web;
				}

				//insert data
				$defpassword = $this->generateRandomString(10);
				$password = bcrypt($defpassword);

				if(User::where('userid', $userid)->first() != null){
					return redirect('signin')->with('err', "User has already registered!")->withInput();
				}

				$user = new User;
				$user->userid = $userid;
				$user->role ='company';
				$user->defaultpassword = $defpassword;
				$user->password = $password;
				$user->status = "nonactive";
				$user->save();

				$comp = new Company;
				$comp->companyid =  $userid;
				$comp->email = $req->email;
				$comp->phone = $req->phone;
				$comp->website = $web;
				$comp->description =  $req->desc;
				$comp->name =  $name;
				// if($req->bipp != null){
				// 	$comp->bipp_id = Bipp::first()->id;
				// }
				$comp->save();

            	return redirect('signin')->with('success', "Registration already sent! Login information will be sent to your email after your account is approved.");
            }
		}
		else
		{
			//Register as Student
			$messages =
				[
					'nim.unique' => 'Student already registered!',
					'dob.date_format' => 'The date of birth format is invalid. It must be YYYY-MM-DD'
				];

			$rules =
				[
					'nim' => 'required|unique:users,userid|size:10',
					'name' => 'required',
					'gender'=> 'required',
					'email'=> 'required|email',
					'dob'=> 'required|date_format:"Y-m-d"',
					'phone'=> 'required',
					'school'=> 'required',
					'major'=> 'required',
					'interest'=> 'required',
					'semester'=> 'required',
					'campus' => 'required'
				];

			$validator = Validator::make($req->all(), $rules, $messages);

        	if ($validator->fails())
        	{
        		return redirect('signin')->withErrors($validator)
                    ->withInput();

			}
			else{
        		$degreeid = Degree::where('name', '=', $req->major)->first();
        		if(empty($degreeid)){
					return redirect('signin')->with('err','Invalid Program')->withInput();
        		}

        		$school = $req->school;
				if(strtolower($school) != "school of computer science"){
					return redirect('signin')->with('err', "Invalid School Input, School must be School of Computer Science !")->withInput();
				}

				$valid_major = ['-', 'global class', 'database', 'multimedia', 'ai', 'network', 'software engineering', 'applied database', 'applied network'];
				if(array_search(strtolower($req->interest), $valid_major, TRUE) === FALSE){
					return redirect('signin')->with('err', "Invalid Interest Input !")->withInput();
				}

				//mm/dd/yyyy
				$date = Carbon::createFromFormat('Y-m-d', $req->dob);
				$dobPass = $date->format('d').$date->format('m').$date->format('Y');
				$user = new User;
				$user->userid =  $req->nim;
				$user->role =  'student';
				$user->defaultpassword =  $dobPass;
				$user->password =  bcrypt($dobPass);
				$user->status = "nonactive";
				$user->save();

				$stud = new Student;
				$stud->studentid = $req->nim;
				$stud->name = $req->name;
				$stud->sex = $req->gender;
				$stud->phone = $req->phone;
				$stud->email = $req->email;
				$stud->dob = $date;
				$stud->semester = $req->semester;
				$stud->school = $req->school;
				$stud->degreeid = $degreeid->id;
				$stud->major = $req->interest;
				$stud->campus = ucwords($req->campus);
				$stud->save();
				return redirect('signin')->with('success', "Registration already sent! Login information will be sent to your email after your account is approved.");
			}
		}
	}

	//Registration via register by department
	public function insertUserByDepartment(Request $req)
	{
		//Register as Student

		$messages =
			[
				'nim.unique' => 'Student already registered!',
				'dob.date_format' => 'The date of birth format is invalid. It must be YYYY-MM-DD'
			];

		$rules =
			[
				'nim' => 'required|unique:users,userid|size:10',
				'name' => 'required',
				'gender'=> 'required',
				'email'=> 'required|email',
				'dob'=> 'required|date_format:"Y-m-d"',
				'phone'=> 'required',
				'school'=> 'required',
				'major'=> 'required',
				'interest'=> 'required',
				'semester'=> 'required'
			];

		$validator = Validator::make($req->all(), $rules, $messages);

    	if ($validator->fails())
    	{
    		return redirect('registerStudentByDepartment')->withErrors($validator)->withInput();

		}
		else{
    		$degreeid = Degree::where('name', '=', $req->major)->first();
    		if(empty($degreeid)){
				return redirect('registerStudentByDepartment')->with('err','Invalid Program')->withInput();
    		}

    		$school = $req->school;
			if(strtolower($school) != "school of computer science"){
				return redirect('registerStudentByDepartment')->with('err', "Invalid School Input, School must be School of Computer Science !")->withInput();
			}

			$valid_major = ['-', 'global class', 'database', 'multimedia', 'ai', 'network', 'software engineering', 'applied database', 'applied network'];
			if(array_search(strtolower($req->interest), $valid_major, TRUE) === FALSE){
				return redirect('registerStudentByDepartment')->with('err', "Invalid Interest Input !")->withInput();
			}

			$date = Carbon::createFromFormat('Y-m-d', $req->dob);
			$dobPass = $date->format('d').$date->format('m').$date->format('Y');
			$user = new User;
			$user->userid =  $req->nim;
			$user->role =  'student';
			$user->defaultpassword =  $dobPass;
			$user->password =  bcrypt($dobPass);
			$user->status = "nonactive";
			$user->save();

			$stud = new Student;
			$stud->studentid = $req->nim;
			$stud->name = $req->name;
			$stud->sex = $req->gender;
			$stud->phone = $req->phone;
			$stud->email = $req->email;
			$stud->dob = $date;
			$stud->semester = $req->semester;
			$stud->school = $req->school;
			$stud->degreeid = $degreeid->id;
			$stud->major = $req->interest;
			$stud->save();
			return redirect('registerStudentByDepartment')->with('success', "Registration already sent! Login information will be sent to your email after your account is approved.");
		}
	}


	//function untuk cek apakah student sudah di accept atau belum
	public function checkAccepted()
	{
		$rec = Recruitment::where('studentid','=', Auth::user()->userid)->where('status','=','approved')->first();
		if($rec != null){
			return 'accepted';
		}
		else{
			return "no";
		}
	}

	//Redirect ke halaman ganti password
	public function changePassword()
	{
		if (Auth::user()->role == 'company'){
			$getState = Company::where('companyid', '=', Auth::user()->userid)->first();
			$getState = $getState->state;
		}
		else{
			$getState = 'bebas';
		}

		$mailboxCount = Mailbox::whereIn('type', array('interview', 'mailbox'))
			->where('status', '=', 'unseen')
			->where('recipientid', '=', Auth::user()->userid)
			->count();

		//Cek apakah bila student, udah di approve atau belum
		$accepted = "";
		if(Auth::user()->role == 'student')
		{
			$accepted = $this->checkAccepted();
		}

		return view('others.change',
			[
				'accepted' => $accepted,
				'mailboxCount' => $mailboxCount,
				'state' => $getState,
				'notifCount' => app('App\Http\Controllers\MailboxController')->getCountNotification(),
				'notif' => app('App\Http\Controllers\MailboxController')->getLatestNotification()
			]);
	}

	//function untuk ganti password
	public function updateChangePassword(Request $request)
	{
		$user = Auth::user();
		//Validasi pake validator cekin semua input yang ke send
		$messages =
			[
    			'NewPassword.required' => 'Field must be Filled!',
    			'ConfirmationPassword.required' => 'Field must be Filled!',

    			'NewPassword.max' => 'Maximum length is 20 Characters!',
    			'NewPassword.max' => 'Maximum length is 20 Characters!',

    			'ConfirmationPassword.min' => 'Minimum length is 6 Characters!',
    			'ConfirmationPassword.min' => 'Minimum length is 6 Characters!'
			];

		$rules =
			[
				'NewPassword' => 'required|max:20|min:6',
				'ConfirmationPassword' => 'required|max:20|min:6'
			];

		$validator = Validator::make($request->all(), $rules, $messages);

		if ($validator->fails())
		{
			return redirect('/changepassword')->withErrors($validator)->withInput();
		}
		else if($request->NewPassword != $request->ConfirmationPassword)
		{
			return redirect('/changepassword')->with('error','Password And Confirmation Password must be the same !');
		}
		else
		{
			//Update password si user
			User::where('userid', '=', Auth::user()->userid)->update(['password' => bcrypt($request->NewPassword)]);
			return redirect('changepassword')->with('fyi', 'Success Change Password!!');
		}
	}

	public function resendApprovalEmail($id)
	{
		$student = Student::where('studentid', '=', $id)->first();
		if(empty($student) && $student->user()->first()->status == 'nonactive'){
			$errors = 'Invalid inputs !';
		}
		if(!empty($errors)){
			return redirect('/manageStudent')->with('errors', $errors);
		}
		else{
			$usr = User::where('userid', '=', $student->studentid)
				->join('students','users.userid','=','students.studentid')
				->whereNull('students.deleted_at')
				->first();

			EmailService::createNewAccountApprovalEmail($usr);

		  	return redirect('/manageStudent')->with('fyi', "Successfully resend emails!");
		}
	}

	public function resetPasswordStudent($id)
	{
		$student = Student::where('studentid', '=', $id)->first();
		if(empty($student) && $student->user()->first()->status == 'nonactive'){
			$errors = 'Invalid inputs !';
		}
		if(!empty($errors)){
			return redirect('/manageStudent')->with('errors', $errors);
		}
		else{
			$usr = User::where('userid', '=', $student->studentid)
				->first();
			$usr->password = bcrypt($usr->defaultpassword);
			$usr->save();

		  	return redirect('/manageStudent')->with('fyi', "Successfully reset password!");
		}
	}
}
?>
