<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Auth;
use App\Student;
use App\Company;
use Image;

class ImageController extends BaseController{

    // to get the profile picture for each user
    // return: Image
    public function getProfilePicture($id) {
        if(Auth::user() == null){ // if user is not authenticated
            abort(404);
        }
        if(Auth::user()->role != 'student'){ //if user is not student
            if(Student::where('studentid', '=', $id)->first() != null){
                //if accessing student's profile picture
                $storagePath = storage_path().'/assets/images/' . $id . '/' . Student::where('studentid', '=', $id)->first()->photo;
                return Image::make($storagePath)
                    ->response();
            }
            else if(Company::where('companyid', '=', $id)->first() != null){
                //if accessing company's profile picture
                $storagePath = storage_path().'/assets/images/' . $id . '/' . Company::where('companyid', '=', $id)->first()->photo;
                return Image::make($storagePath)
                    ->response();
            }
        }
        if(Auth::user()->role == 'student' && Auth::user()->userid == $id){
            //if the user is student and the user access his / her own image
            $storagePath = storage_path().'/assets/images/' . $id . '/' . Student::where('studentid', '=', $id)->first()->photo;
            return Image::make($storagePath)
                ->response();
        }
        abort(404);
    }

    // to get the chart image stored for each user
    // return: Image
    public function getChart($id) {
        if(Auth::user() == null){// if the user is not authenticated
            abort(404);
        }
        if(Auth::user()->role != 'student'){ // if the user is not student
            if(Student::where('studentid', '=', $id)->first() != null){
                $storagePath = storage_path().'/app/cv/'.$id.'/chart.png';
                return Image::make($storagePath)->response();
            }
        }
        if(Auth::user()->role == 'student' && Auth::user()->userid == $id){
            // if the user is student and accessing his/her own chart
            $storagePath = storage_path().'/app/cv/'.$id.'/chart.png';
            return Image::make($storagePath)->response();
        }
        abort(404);
    }

}

?>
