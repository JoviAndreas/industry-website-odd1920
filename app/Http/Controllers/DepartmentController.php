<?php

namespace App\Http\Controllers;


use App\Degree;
use App\Department;
use App\Faculty;
use asset;
use Auth;
use DB;
use File;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use PHPWord;
use Response;
use Storage;
use Validator;

class DepartmentController extends BaseController{

	// return array of degree id
	public static function get_allowed_degree_list(){
        $generatedSession = Department::generateRoleSession();
		$head_of_faculty = $generatedSession['head_of_faculty'];
		$head_of_program = $generatedSession['head_of_program'];
		$degrees_can_be_chosen = $generatedSession['degrees_can_be_chosen'];
		
		$allowed_degree_id = [];
		if(session('head_of_program') == 'all'){
			foreach($degrees_can_be_chosen as $d){
				array_push($allowed_degree_id, $d->id);
			}
		}
		else{
			$allowed_degree_id = collect($degrees_can_be_chosen)->pluck('id');
		}


		return $allowed_degree_id;
	}

	// open change HOP data page
	// routes: /changeHeadOfProgramData/
	public function changeHeadOfProgramData($departmentid){
		if(Auth::user()->department()->first()->head_of_program != 'all'){
			return redirect('/addDepartmentAccount');
		}

		$dep = Department::where('departmentid', '=', $departmentid)->first();

		if($dep == null){
			return redirect('/addDepartmentAccount');
		}

		$currentRoles = explode(';', $dep->head_of_program);

		return view('department.change_hop_data', [
			'department' => $dep,
			'faculties' => Faculty::all(),
			'currentRoles' => $currentRoles,
		]);
	}

	// submit change HOP data
	// routes: /submitChangeHOPData
	public function submitChangeHOPData(Request $request){
        //dd(['test' => 'test']);
		if(Auth::user()->department()->first()->head_of_program != 'all'){
			return redirect('/addDepartmentAccount');
		}

		$dep = Department::where('departmentid', '=', $request->departmentid)->first();

		if($dep == null){
			return redirect('/addDepartmentAccount')->with([
				'error' => 'Department Account not found'
			]);
		}

		$arr_hop = [];

		if (empty($request->roles)) {
			return redirect('/changeHeadOfProgramData/'.$dep->departmentid)
			->with(['error' => 'Please input access role!']);
		}

		foreach($request->roles as $value){
			array_push($arr_hop, $value);
			
		}

		if($arr_hop[0] == 'all'){
			$dep->head_of_program = 'all';
		}
		else if (strpos($arr_hop[0], 'faculty') !== false) {
			$dep->head_of_program = $arr_hop[0];
		}
		else{
			$dep->head_of_program = implode(';', $arr_hop);
		}

		$dep->save();

		return redirect('/addDepartmentAccount/')
			->with(['fyi' => 'Successfully change access role data']);
	}

	// Others > Master Password page
    // open master password page to configure master password
	// routes: /passwords
	public function masterPasswordDepartment()
	{
		try
		{
			// get the master password
    		$masterpassword = Storage::get('/master/code');
		}
		catch (Illuminate\Filesystem\FileNotFoundException $exception)
		{
    		die("The file doesn't exist");
		}
		return view('department.passwords', ['passwords' => $masterpassword]);
	}

	// Others > Faculty Password page
    // open faculty password page to configure faculty password
	// routes: /facultyPasswords
	public function masterPasswordDepartmentFaculty()
	{
		dd("TODO");
		try
		{
			// get the master password
    		$masterpassword = Storage::get('/master/code');
		}
		catch (Illuminate\Filesystem\FileNotFoundException $exception)
		{
    		die("The file doesn't exist");
		}
		return view('department.passwords', ['passwords' => $masterpassword]);
	}

	// changing the head of program data in session for filter
	// routes: /changeHeadOfProgram
	public function changeHeadOfProgram(Request $request){
		if(empty($request->head_of_program)){
			return redirect('/homed');
		}
		else{
			//check if the head_of_program selection is in the list of head_of_program for the logged in user
			$hop = Auth::user()->department()->first()->head_of_program;
			
			if($hop == 'all' || strpos($hop,'faculty') !== false){ //SUPERADMIN OR ADMIN
				$degrees = null;
				if($request->head_of_program == 'all'){
					$degrees = Degree::where('faculty_id', $request->head_of_faculty)->get();
				}
				else{
					$degrees = Degree::where('faculty_id', $request->head_of_faculty)->where('id', '=', $request->head_of_program)->first();
				}
				if($degrees == null){
					return redirect()->route('department.home', ['filterCampus' => $request->filterCampus]);;
				}
				else{
					session([
						'head_of_program' => $request->head_of_program
					]);
					return redirect()->route('department.home', ['filter' => $request->head_of_faculty, 'filterCampus' => $request->filterCampus]);
				}
			} else {
				$arr_hop = explode(';', $hop);
				if(array_search($request->head_of_program, $arr_hop) === false){
					return redirect('/homed');
				}
				else{
					// change the session information
					session([
						'head_of_program' => $request->head_of_program
					]);
					return redirect()->route('department.home', ['filter' => $request->head_of_faculty, 'filterCampus' => $request->filterCampus]);
				}
			}
		}
	}

	//Submit new master password
	public function updateMasterPasswordDepartment(Request $request)
	{
		$validator = Validator::make($request->all(),
			[
				'newpassword' => 'required|min:6',
				'confirmpassword' => 'required|min:6'
	   		]);
		//validation kosong dan minimal
		if ($validator->fails())
		{
			return redirect('/passwords')->withErrors($validator);
        		}
        		else if($request->newpassword != $request->confirmpassword)
		{
			return redirect('/passwords')->with('error', 'Password and Confirmation Password must be the same');
		}
		else
		{
			//put in storage
			Storage::put('/master/code', $request->newpassword);
			return redirect('/passwords')->with('fyi', 'Password successfully changed!');
		}
	}
}
?>
