<?php

namespace App\Http\Controllers;

use App\ApprovalRequest;
use App\Company;
use App\Department;
use App\Interview;
use App\Job;
use App\JobsDegrees;
use App\Mailbox;
use App\Recruitment;
use App\Services\EmailService;
use App\Student;
use App\Studentsemester;
use App\UnacceptRequest;
use App\UnrejectRequest;
use Auth;
use Carbon\Carbon;
use DB;
use Excel;
use File;
use Hash;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Image;
use Response;
use route;
use Storage;
use URL;
use Validator;


class RecruitmentController extends BaseController{

	use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

	// function untuk cek apakah student sudah di accept atau belum
	public function checkAccepted()
	{
		$rec = Recruitment::where('studentid','=', Auth::user()->userid)
			->where('status','=','approved')
			->first();
		if($rec != null){
			return 'accepted';
		}
		else{
			return "no";
		}
	}

	// Organize > Edit Recruitment page
	// routes: /editRecruitment
	// return: Organize > Edit Recruitment page
	public function indexDepartment(Request $request)
	{
		$allowed_degree_id = DepartmentController::get_allowed_degree_list();

		$perpage = 20;

		$data = Recruitment::join('students', 'students.studentid', '=', 'recruitments.studentid')
			->join('degrees', 'degrees.id', '=', 'students.degreeid')
			->whereNull('students.deleted_at')
			->whereNull('degrees.deleted_at')
			->whereIn('students.degreeid', $allowed_degree_id)
			->select('students.studentid as studentid', 'students.name as studentname', 'students.cv as cv', 'degrees.name as degree_name')
			->distinct()
			->withTrashed()
			->get();

		if ($request->Searching != null)
		{
			$data = Recruitment::join('students', 'students.studentid', '=', 'recruitments.studentid')
				->join('degrees', 'degrees.id', '=', 'students.degreeid')
				->whereNull('students.deleted_at')
				->whereNull('degrees.deleted_at')
				->whereIn('students.degreeid', $allowed_degree_id)
				->where('students.name', 'like', '%'.$request->Searching.'%')
				->select('students.studentid as studentid','students.name as studentname','students.cv as cv', 'degrees.name as degree_name')
				->distinct()
				->withTrashed()
				->get();
			$Searching = $request->Searching;
		}
		else
		{
			$Searching = "";
		}

  		$totalPage = ceil(sizeof($data) / $perpage);
  		$curpage = (isset($request->page)) ? $request->page : 1;
  		$offset = ($curpage-1) * $perpage;

  		$data = $data->slice($offset, $perpage);
		foreach($data as $stud)
		{
			$stud->listjob = Recruitment::join('students', 'students.studentid', '=', 'recruitments.studentid')
				->join('jobs','jobs.id','=','recruitments.jobid')
				->join('companies','jobs.companyid','=','companies.companyid')
				->whereNull('students.deleted_at')
				->whereNull('jobs.deleted_at')
				->whereNull('companies.deleted_at')
				->where('students.studentid', $stud->studentid)
				->where('companies.deleted_at', null)
				->select(
					'companies.name as companyname',
					'jobs.name as jobname',
					'recruitments.status as status',
					'cv',
					'jobs.id as jobid',
					'jobs.description as jobdesc',
					'jobs.quota as quota',
					'recruitments.id as recid',
					'recruitments.created_at as applied_on',
					'recruitments.processed_by as processed_by',
					'recruitments.processed_on as processed_on',
					'recruitments.accepted_by as accepted_by',
					'recruitments.accepted_on as accepted_on',
					'recruitments.approved_by as approved_by',
					'recruitments.approved_on as approved_on',
					'recruitments.rejected_by as rejected_by',
					'recruitments.rejected_on as rejected_on')
				->get();

			foreach($stud->listjob as $one_rec){
				$one_rec->interview_data_count = Interview::where('recruitment_id', '=', $one_rec->recid)
					->orderBy('created_at', 'desc')
					->get()
					->count();

				// get all quota available for the job
				// current quota + approved
				$approved_count = Recruitment::where('jobid', '=', $one_rec->jobid)
					->where('status', '=', 'approved')
					->get()
					->count();

				$one_rec->{'full_quota'} = $approved_count + $one_rec->quota;
			}

			$stud->listjobdeleted = Recruitment::join('students', 'students.studentid', '=', 'recruitments.studentid')
				->join('jobs','jobs.id','=','recruitments.jobid')
				->join('companies','jobs.companyid','=','companies.companyid')
				->whereNull('students.deleted_at')
				->where('students.studentid', $stud->studentid)
				->select(
					'companies.name as companyname',
					'companies.deleted_at as company_deleted_at',
					'jobs.name as jobname',
					'recruitments.status as status',
					'cv',
					'jobs.id as jobid',
					'jobs.deleted_at as job_deleted_at',
					'jobs.description as jobdesc',
					'jobs.quota as quota',
					'recruitments.id as recid',
					'recruitments.created_at as applied_on',
					'recruitments.processed_by as processed_by',
					'recruitments.processed_on as processed_on',
					'recruitments.accepted_by as accepted_by',
					'recruitments.accepted_on as accepted_on',
					'recruitments.approved_by as approved_by',
					'recruitments.approved_on as approved_on',
					'recruitments.rejected_by as rejected_by',
					'recruitments.rejected_on as rejected_on',
					'recruitments.unapplied_on as unapplied_on',
					'recruitments.unapplied_by as unapplied_by')
				->onlyTrashed()
				->get();

			foreach($stud->listjobdeleted as $one_rec){
				$one_rec->interview_data_count = Interview::where('recruitment_id', '=', $one_rec->recid)
					->orderBy('created_at', 'desc')
					->withTrashed()
					->get()
					->count();

				// get all quota available for the job
				// current quota + approved
				$approved_count = Recruitment::where('jobid', '=', $one_rec->jobid)
					->where('status', '=', 'approved')
					->get()
					->count();

				$one_rec->{'full_quota'} = $approved_count + $one_rec->quota;
			}
		}

       	return view('department.editrecruitment',
	       	[
				'data' => $data,
				'totalPage' => $totalPage,
				'currentPage' => $curpage,
				'Searching' => $Searching
	      	]);
	}

	// get job application's interview data
	// route: /getRecruitmentInterviewData/
	// return: json (for datatable)
	public function getRecruitmentInterviewData($recruitment_id){
		if(Recruitment::where('id', '=', $recruitment_id)->withTrashed()->first() == null){
			return ['data' => null];
		}
		else{
			$data = Recruitment::join('interviews', 'recruitments.id', '=', 'interviews.recruitment_id')
				->join('jobs','jobs.id','=','recruitments.jobid')
				->join('companies', 'jobs.companyid', '=', 'companies.companyid')
				->join('students', 'students.studentid', '=', 'interviews.interviewee')
				->whereNull('interviews.deleted_at')
				->whereNull('jobs.deleted_at')
				->whereNull('companies.deleted_at')
				->whereNull('students.deleted_at')
				->where('recruitments.id', '=', $recruitment_id)
				->select(
					'interviews.location as location',
					'interviews.date as date',
					'interviews.time as time',
					'interviews.pic as pic',
					'interviews.phone as phone',
					'interviews.status as status',
					'interviews.created_at as created_at',
					'interviews.updated_at as updated_at'
				)
				->withTrashed()
				->get()
				->all();

			foreach($data as $d){
				if($d['status'] == 'waiting'){
					$d['processed_on'] = '-';
				}
				else{
					$d['processed_on'] = ''.$d['updated_at'];
				}
			}

			return [
				'data' => $data
			];
		}
	}

	// Organize > Edit Recruitment
	// submit recruitment edit
	// routes: /submitEditRec
	public function updateDepartment(Request $request)
	{
		$allowed_degree_id = DepartmentController::get_allowed_degree_list();

		$messages =
			[
				'status.required' => 'Status must be filled!'
			];

		$rules =
			[
        		'status' => 'required'
			];

		$validator = Validator::make($request->all(), $rules, $messages);

		if ($validator->fails())
		{
   			return redirect('/editRecruitment')->with('err','Status must be filled!');
		}
		else
		{
			if($request->status != 'process' && $request->status != 'approved' && $request->status != 'rejected' && $request->status != 'waiting'){
				return redirect('/editRecruitment')->with('err','Incorrect Status input !');
			}
			else{
				$rec = Recruitment::where('id', '=', $request->recid)->first();
				$student = $rec->student()->first();
				$job = $rec->job()->first();

				if(array_search($student->degreeid, $allowed_degree_id) === FALSE){
					return redirect('/editRecruitment')->with('err', "You cannot change this student's recruitment data !");
				}

				//update data
				if($request->status == 'approved'){
					if($job->quota <= 0){
						return redirect('/editRecruitment')->with('err', "The job quota is zero (0) !");
					}

					if($rec->status == 'rejected' || $rec->status == 'process' || $rec->status == 'waiting'){
						$job->quota -= 1;
						$job->save();
					}

					if($rec->status == 'rejected' && !empty($rec->rejected_by) && $rec->rejected_by == 'system'){
						//currently rejected by system
						$rec->interview()->delete();
					}

					$arr_update =  [
						'status' => $request->status,
						'approved_on' => Carbon::now(),
						'approved_by' => Auth::user()->userid,
						'rejected_by' => null,
						'rejected_on' => null
					];

					if($student->requiringDepartmentApproval()){
						ApprovalRequest::where('recruitment_id', '=', $rec->id)->delete();//delete old approval request data

						$arr_update['accepted_on'] = Carbon::now();
						$arr_update['accepted_by'] = Auth::user()->userid;

						$approval_request = new ApprovalRequest();
						$approval_request->status = 'approved';
						$approval_request->recruitment_id = $rec->id;
						$approval_request->approved_by = Auth::user()->userid;
						$approval_request->approved_on = Carbon::now();
						$approval_request->save();
					}

					Recruitment::where('id', '=', $request->recid)->update($arr_update);

					// send email to another company that the student has been approved
					EmailService::createNewApprovedOtherEmail(
						$rec->student()->first(), $rec->job()->first()->company()->first()->companyid
					);

					// send email to student that the status has been Changed
					EmailService::createNewChangeStatusEmail($rec, "Approved");
				}
				else{
					if($rec->status == 'rejected' && !empty($rec->rejected_by) && $rec->rejected_by == 'system'){
						//currently rejected by system
						$rec->interview()->delete();
					}

					if($rec->status == 'accepted' || $rec->status == 'approved'){
						$job->quota += 1;
						$job->save();
					}

					$arr_update = [
						'status' => $request->status
					];

					if($student->requiringDepartmentApproval()){
						$approval_request = $rec->approval_request()->first();
						if($approval_request != null){
							$approval_request->rejected_by = Auth::user()->userid;
							$approval_request->rejected_on = Carbon::now();
							$approval_request->save();

							$approval_request->delete();

							$arr_update['accepted_by'] = null;
							$arr_update['accepted_on'] = null;
						}
					}

					if($request->status == 'process'){
						$arr_update['processed_by'] = Auth::user()->userid;
						$arr_update['processed_on'] = Carbon::now();
						$arr_update['approved_by'] = null;
						$arr_update['approved_on'] = null;
						$arr_update['accepted_by'] = null;
						$arr_update['accepted_on'] = null;
						$arr_update['rejected_by'] = null;
						$arr_update['rejected_on'] = null;

						// send email to student that the status has been Changed
						EmailService::createNewChangeStatusEmail($rec, "Process");
					}
					else if($request->status == 'waiting'){
						$arr_update['processed_by'] = null;
						$arr_update['processed_on'] = null;
						$arr_update['rejected_by'] = null;
						$arr_update['rejected_on'] = null;
						$arr_update['accepted_by'] = null;
						$arr_update['accepted_on'] = null;
						$arr_update['approved_by'] = null;
						$arr_update['approved_on'] = null;

						// send email to student that the status has been Changed
						EmailService::createNewChangeStatusEmail($rec, "Waiting");
					}
					else if($request->status == 'rejected'){
						$arr_update['rejected_by'] = Auth::user()->userid;
						$arr_update['rejected_on'] = Carbon::now();
						$arr_update['approved_by'] = null;
						$arr_update['approved_on'] = null;

						// send email to student that the status has been Changed
						EmailService::createNewChangeStatusEmail($rec, "Rejected");
					}

   					Recruitment::where('id', '=', $request->recid)->update($arr_update);
   				}

				$company = $rec->job()->first()->company()->first();

				// send new message to user that the status is changed
				$mailInsert = new Mailbox;
				$mailInsert->senderid = Auth::user()->userid;
				$mailInsert->recipientid = $student->studentid;
				$mailInsert->subject = 'Job Application Status Changed';
				$mailInsert->date = date("Y-m-d");
				$mailInsert->message = 'Your job application as '. $rec->job()->first()->name .' to '. $company->name .' has been changed by School of Computer Science';
				$mailInsert->status = 'unseen';
				$mailInsert->type = 'notif';
				$mailInsert->token = '';
				$mailInsert->save();

				$mailInsert = new Mailbox;
				$mailInsert->senderid = Auth::user()->userid;
				$mailInsert->recipientid = $company->companyid;
				$mailInsert->subject = 'Job Application Status Changed';
				$mailInsert->date = date("Y-m-d");
				$mailInsert->message = 'Job application of '. $student->name .' as '. $rec->job()->first()->name .' has been changed by School of Computer Science';
				$mailInsert->status = 'unseen';
				$mailInsert->type = 'notif';
				$mailInsert->token = '';
				$mailInsert->save();

    			return redirect('/editRecruitment')->with('fyi','Successfully change Status!');
			}
		}
	}

	// Organize > Apply page
	// routes: /apply
	public function applyDepartment(Request $request)
	{
		$allowed_degree_id = DepartmentController::get_allowed_degree_list();

		// check searching
		if ($request->Searching != null)
		{
			$company = Company::where('companies.name', 'like', '%'.$request->Searching.'%')
				->whereIn('companyid', function($query) use ($allowed_degree_id){
					$query->select('companyid')
						->from('jobs')
						->join('jobsdegrees', 'jobsdegrees.jobid', '=', 'jobs.id')
						->whereIn('jobsdegrees.degreeid', $allowed_degree_id);
				})
				->where('state','=','open')
				->get();
			$Searching = $request->Searching;
		}
		else
		{
			$company =  Company::whereIn('companyid', function($query) use ($allowed_degree_id){
					$query->select('companyid')
						->from('jobs')
						->join('jobsdegrees', 'jobsdegrees.jobid', '=', 'jobs.id')
						->whereIn('jobsdegrees.degreeid', $allowed_degree_id);
				})
				->where('state','=','open')
				->get();
			$Searching = "";
		}

		//check list company tanpa jobs
		$listCompanyWithoutJobs = array();
		$counter = 0;

		//loop percompany
		//push counternya untuk digunakan untuk unset company without job
		foreach($company as $c)
		{
			$c->listJob = Job::where('companyid', '=', $c->companyid)
				->where('quota','>','0')
				->where('job_state', '=', 'open')
				->whereHas('jobsdegrees', function($query) use ($allowed_degree_id) {
					$query->whereIn('degreeid', $allowed_degree_id);
				})

				->get();

			foreach ($c->listJob as $key => $value) {
				$value->degrees = JobsDegrees::join('degrees', 'degrees.id', '=', 'jobsdegrees.degreeid')
					->whereNull('degrees.deleted_at')
					->where('jobid', '=', $value->id)
					->select('degrees.id as id', 'degrees.name as name')
					->get();
			}

			if(sizeof($c->listJob)==0)
			{
				array_push($listCompanyWithoutJobs,$counter);
			}
			$counter++;
		}
		foreach($listCompanyWithoutJobs as $id)
		{
			unset($company[$id]);
		}

		//simple paging
		$perpage = 10;
  		$totalPage = ceil(sizeof($company) / $perpage);
  		$curpage = (isset($request->page)) ? $request->page : 1;
  		$offset = ($curpage - 1) * $perpage;

  		$company = $company->slice($offset, $perpage);

		return view('department.apply',
			[
				'company' => $company,
				'totalPage' => $totalPage,
				'currentPage' => $curpage,
				'Searching' => $Searching
			]);
	}

	// routes: /submitApplyJob
	public function insertApplyDepartment(Request $request)
	{
		if(isset($request->applyStudent)){
			// submit apply to job by department
			// TODO Send error messages
			$allowed_degree_id = DepartmentController::get_allowed_degree_list();

			$jobid = $request->txtjob;
			$job = Job::where('id', '=', $jobid)->first();

			$company = $job->company()->first();

			if($company->state != 'open'){
				return redirect('/apply')->with([
					'err' => 'You cannot apply to the job, because the company is closed.'
				]);
			}

			if($job == null || $job->jobsdegrees()->whereIn('degreeid', $allowed_degree_id)->get()->count() == 0){
				// if the job id is Invalid
				// or if the job's programs is not in the allowed list
				return redirect('/apply')->with([
					'err' => 'You cannot apply to the job.'
				]);
			}

			if($job->job_state != 'open'){
				return redirect('/apply')->with([
					'err' => 'You cannot apply to the job because the job is closed'
				]);
			}

			foreach($request->all() as $key => $value)
			{
				//check request yang diawali dengan 'check'
				if(substr( $key, 0, 5 ) === "check")
				{
					$NIM = $value;

					$student = Student::filterCampus()->where('studentid', '=', $NIM)->first();
					if($student == null){
						// if student is not found
						continue;
					}

					// if(Recruitment::where('jobid', '=', $jobid)->where('studentid', '=', $NIM)->get()->count() > 0){
					// 	// if the selected student has applied to the job before
					// 	continue;
					// }

					$student_degree_id = $student->degreeid;
					if(array_search($student_degree_id, $allowed_degree_id) === FALSE){
						// the student's program is not in the list of allowed program
						continue;
					}

					//insert data apply
					$rec = new Recruitment;
					$rec->jobid = $jobid;
					$rec->studentid = $NIM;
					$rec->status = 'waiting';
					$rec->processed_on = null;
					$rec->save();

					$resToken = Hash::make($rec->id.$rec->jobid.$rec->studentid);
					$resToken = str_replace('.', 'H', str_replace("$", "S", str_replace("/", "X", $resToken)));

					$rec->token = $resToken;
					$rec->save();

					// send email to student that the status has been Changed
					EmailService::createNewChangeStatusEmail($rec, "Waiting");
				}
				else
				{
					continue;
				}
			}
			return redirect('/apply')->with('fyi','Success Apply Job!');
		}else{
			
			// submit apply and approve to job by department
			$allowed_degree_id = DepartmentController::get_allowed_degree_list();

			$jobid = $request->txtjob;
			$job = Job::where('id', '=', $jobid)->first();

			$company = $job->company()->first();

			if($company->state != 'open'){
				return redirect('/apply')->with([
					'err' => 'You cannot apply to the job, because the company is closed.'
				]);
			}

			if($job == null || $job->jobsdegrees()->whereIn('degreeid', $allowed_degree_id)->get()->count() == 0){
				// if the job id is Invalid
				// or if the job's programs is not in the allowed list
				return redirect('/apply')->with([
					'err' => 'You cannot apply to the job.'
				]);
			}

			if($job->job_state != 'open'){
				return redirect('/apply')->with([
					'err' => 'You cannot apply to the job because the job is closed'
				]);
			}

			foreach($request->all() as $key => $value)
			{
				//check request yang diawali dengan 'check'
				if(substr( $key, 0, 5 ) === "check")
				{
					$NIM = $value;

					$student = Student::filterCampus()->where('studentid', '=', $NIM)->first();
					if($student == null){
						// if student is not found
						continue;
					}

					// if(Recruitment::where('jobid', '=', $jobid)->where('studentid', '=', $NIM)->get()->count() > 0){
					// 	// if the selected student has applied to the job before
					// 	continue;
					// }

					$student_degree_id = $student->degreeid;
					if(array_search($student_degree_id, $allowed_degree_id) === FALSE){
						// the student's program is not in the list of allowed program
						continue;
					}

					//insert data apply
					$rec = new Recruitment;
					$rec->jobid = $jobid;
					$rec->studentid = $NIM;
					$rec->status = 'approved';
					$rec->approved_by = Auth::user()->userid;
					$rec->approved_on =  Carbon::now();
					$rec->save();

					$resToken = Hash::make($rec->id.$rec->jobid.$rec->studentid);
					$resToken = str_replace('.', 'H', str_replace("$", "S", str_replace("/", "X", $resToken)));

					$rec->token = $resToken;
					$rec->save();

					// send email to student that the status has been Changed
					EmailService::createNewChangeStatusEmail($rec, "Approved");
				}
				else
				{
					continue;
				}
			}
			return redirect('/apply')->with('fyi','Success Apply and Approve to the Job!');
		}
	}

	// Get students list that could apply to the specific job id
	// the student is the one who is not yet apply to the job
	// returns json data
	// routes: /getdata/
	public function getStudentApplyDepartment($jobid)
	{
		$allowed_degree_id = DepartmentController::get_allowed_degree_list();

        $data = Student::filterCampus()->join('degrees', 'degrees.id', '=', 'students.degreeid')
			->whereNull('degrees.deleted_at')
			->where('track', 'Internship')
			->where('cv', '1')
			->whereNotIn('students.studentid', function ($query)
				{
					$query
						->select('studentid')
						->from('recruitments')
						->whereNull('recruitments.deleted_at')
						->whereRaw('status = "approved"');
				})
			->whereNotIn('students.studentid', function ($query) use ($jobid)
				{
					$query
						->select('studentid')
						->from('recruitments as r')
						->whereNull('r.deleted_at')
						->whereRaw('jobid ='.$jobid)
						->whereRaw('status <> "rejected"');
				})
			->whereIn('students.degreeid', function ($query) use ($jobid)
				{
					$query
						->select('j.degreeid')
						->from('jobsdegrees as j')
						->whereNull('j.deleted_at')
						->whereRaw('jobid = '.$jobid);
				}
			)
			->whereIn('students.degreeid', $allowed_degree_id)
			->select('studentid as NIM','students.name as Name','students.sex as Sex','students.phone as Phone','students.email as Email','students.cv as CV', 'degrees.name as Degree_Name')
			->get();

		foreach($data as $d)
		{
			$last_semester = StudentSemester::where('studentid', '=', $d->NIM)->select(DB::raw('MAX(semester) as max_semester'))->first();
			$ipk = Studentsemester::where('studentid', '=', $d->NIM)->where('semester', '=', $last_semester['max_semester'])->first();
			if ($ipk) {
				$d->IPK = $ipk->ipk;
			} else {
				$d->IPK = 0;
			}

			$link = "href='".url('/previewcvs/')."/".$d->NIM."'";
			if($d->CV == 1)
			{
				$d->CV ="<a class='ui button'".$link." target='_blank'>Preview</a>";
			}
			else
			{
				$d->CV = "CV not Found!";
			}
			$d->Action = "<input type='checkbox' class='check' value='".$d->NIM."'  onchange='clicked(this)'/>";
		}
		$x = array("aaData" => $data);
		//kirim by json
		return json_encode($x);
	}

	// Organize > Unaccept Request page
	// routes: /unaccept_request
	// return: Organize > Unaccept Request page
	public function unaccept_request()
	{
        if (!Department::isSuperAdmin()) {
            return redirect('/');
        }
		$allowed_degree_id = DepartmentController::get_allowed_degree_list();

		$unaccept_requests = UnacceptRequest::join('recruitments', 'recruitments.id', '=', 'unaccept_request.recruitment_id')
			->join('jobs', 'jobs.id', '=', 'recruitments.jobid')
			->join('students', 'students.studentid', '=', 'recruitments.studentid')
			->join('degrees', 'degrees.id', '=', 'students.degreeid')
			->join('companies', 'companies.companyid', '=', 'jobs.companyid')
			->whereNull('recruitments.deleted_at')
			->whereNull('jobs.deleted_at')
			->whereNull('students.deleted_at')
			->whereNull('degrees.deleted_at')
			->whereNull('companies.deleted_at')
			->whereIn('students.degreeid', $allowed_degree_id)
			->where('unaccept_request.status', '=', 'pending')
			->select(
				'students.name as student_name',
				'students.studentid as nim',
				'jobs.id as job_id',
				'jobs.name as job_name',
				'unaccept_request.request_id as request_id',
				'unaccept_request.reason as reason',
				'degrees.name as degree_name',
				'companies.name as company_name'
				)
			->distinct()
			->get();

		$unaccept_requests_history = UnacceptRequest::join('recruitments', 'recruitments.id', '=', 'unaccept_request.recruitment_id')
			->join('jobs', 'jobs.id', '=', 'recruitments.jobid')
			->join('students', 'students.studentid', '=', 'recruitments.studentid')
			->join('degrees', 'degrees.id', '=', 'students.degreeid')
			->join('companies', 'companies.companyid', '=', 'jobs.companyid')
			->whereNull('recruitments.deleted_at')
			->whereNull('jobs.deleted_at')
			->whereNull('students.deleted_at')
			->whereNull('degrees.deleted_at')
			->whereNull('companies.deleted_at')
			->whereIn('students.degreeid', $allowed_degree_id)
			->where('unaccept_request.status', '<>', 'pending')
			->select(
				'students.name as student_name',
				'students.studentid as nim',
				'jobs.id as job_id',
				'jobs.name as job_name',
				'unaccept_request.request_id as request_id',
				'unaccept_request.reason as reason',
				'degrees.name as degree_name',
				'companies.name as company_name',
				'unaccept_request.status as status',
				'unaccept_request.approved_by as approved_by',
				'unaccept_request.approved_on as approved_on',
				'unaccept_request.rejected_by as rejected_by',
				'unaccept_request.rejected_on as rejected_on'
				)
			->distinct()
			->get();

		return view('department.unaccept_request')->with(
			[
				'unaccept_requests' => $unaccept_requests,
				'unaccept_request_history' => $unaccept_requests_history
			]);
	}

	// Organize > Unreject Request page
	// routes: /unreject_request
	// return: Organize > Unreject Request page
	public function unreject_request()
	{
		$allowed_degree_id = DepartmentController::get_allowed_degree_list();

		$unreject_requests = UnrejectRequest::join('recruitments', 'recruitments.id', '=', 'unreject_request.recruitment_id')
			->join('jobs', 'jobs.id', '=', 'recruitments.jobid')
			->join('students', 'students.studentid', '=', 'recruitments.studentid')
			->join('degrees', 'degrees.id', '=', 'students.degreeid')
			->join('companies', 'companies.companyid', '=', 'jobs.companyid')
			->whereNull('recruitments.deleted_at')
			->whereNull('jobs.deleted_at')
			->whereNull('students.deleted_at')
			->whereNull('degrees.deleted_at')
			->whereNull('companies.deleted_at')
			->whereIn('students.degreeid', $allowed_degree_id)
			->where('unreject_request.status', '=', 'pending')
			->select(
				'students.name as student_name',
				'students.studentid as nim',
				'jobs.id as job_id',
				'jobs.name as job_name',
				'unreject_request.request_id as request_id',
				'unreject_request.reason as reason',
				'degrees.name as degree_name',
				'companies.name as company_name'
				)
			->distinct()
			->get();

		$unreject_requests_history = UnrejectRequest::join('recruitments', 'recruitments.id', '=', 'unreject_request.recruitment_id')
			->join('jobs', 'jobs.id', '=', 'recruitments.jobid')
			->join('students', 'students.studentid', '=', 'recruitments.studentid')
			->join('degrees', 'degrees.id', '=', 'students.degreeid')
			->join('companies', 'companies.companyid', '=', 'jobs.companyid')
			->whereNull('recruitments.deleted_at')
			->whereNull('jobs.deleted_at')
			->whereNull('students.deleted_at')
			->whereNull('degrees.deleted_at')
			->whereNull('companies.deleted_at')
			->whereIn('students.degreeid', $allowed_degree_id)
			->where('unreject_request.status', '<>', 'pending')
			->select(
				'students.name as student_name',
				'students.studentid as nim',
				'jobs.id as job_id',
				'jobs.name as job_name',
				'unreject_request.request_id as request_id',
				'unreject_request.reason as reason',
				'degrees.name as degree_name',
				'companies.name as company_name',
				'unreject_request.status as status',
				'unreject_request.approved_by as approved_by',
				'unreject_request.approved_on as approved_on',
				'unreject_request.rejected_by as rejected_by',
				'unreject_request.rejected_on as rejected_on'
				)
			->distinct()
			->get();

		return view('department.unreject_request')->with(
			[
				'unreject_requests' => $unreject_requests,
				'unreject_request_history' => $unreject_requests_history
			]);
	}

	// unaccept student
	// routes: /department/unaccept_student
	public function unacceptDepartment($recruitment_id)
	{
		$allowed_degree_id = DepartmentController::get_allowed_degree_list();
		$recruitment = Recruitment::where('id', $recruitment_id)->first();

		$student = $recruitment->student()->first();

		if(array_search($student->degreeid, $allowed_degree_id) === FALSE){
			return redirect()->back()->with('err',"You cannot access this student's unaccept request");
		}

		$job = $recruitment->job()->first();
		$job->quota = $job->quota + 1;
		$job->save();

		$recruitment->status = 'process';
		$recruitment->accepted_on = null;
		$recruitment->accepted_by = null;
		$recruitment->approved_on = null;
		$recruitment->approved_by = null;
		$recruitment->processed_by = Auth::user()->userid;
		$recruitment->processed_on = Carbon::now();
		$recruitment->save();

		// send notif to student
		$recipientid = $recruitment->studentid;
		$subject = 'Your job application has been unaccepted';
		$message = $job->company()->first()->name." has unaccept your job application as ".$job->name;
		$type = "notif";
		NotificationController::createNewNotification(Auth::user()->userid, $recipientid, $subject, $message, $type);

		// send email to student that the status has been Changed
		EmailService::createNewChangeStatusEmail($recruitment, "Process");

		return redirect()->back()->with('fyi','Success Unaccept Student!');
	}

	// submit update to unaccept request (approve each)
	// routes: /approve_unaccept_request/
	public function approve_unaccept_request($id)
	{
		$allowed_degree_id = DepartmentController::get_allowed_degree_list();

   		$request = UnacceptRequest::where('request_id', '=', $id)->first();

		if($request == null){
			return redirect('/unaccept_request')->with('errors','Cannot find request data!');
		}
		else{
			if($request->status != 'pending'){
				return redirect('/unaccept_request')->with('errors','Request has been approved / rejected before!');
			}
			else{
				$recruitment = $request->recruitment()->first();
				$student = $recruitment->student()->first();

				if(array_search($student->degreeid, $allowed_degree_id) === FALSE){
					return redirect('/unaccept_request')->with('errors',"You cannot approve this student's unaccept request");
				}

				$job = $recruitment->job()->first();
				$job->quota = $job->quota + 1;
				$job->save();

				$recruitment->status = 'process';
				$recruitment->accepted_on = null;
				$recruitment->accepted_by = null;
				$recruitment->approved_on = null;
				$recruitment->approved_by = null;
				$recruitment->processed_by = Auth::user()->userid;
				$recruitment->processed_on = Carbon::now();
				$recruitment->save();

				$request->approved_by = Auth::user()->userid;
				$request->approved_on = Carbon::now();
				$request->status = 'approved';
				$request->save();

				// send notif
				NotificationController::createApproveUnacceptRequestNotif($request, Auth::user()->userid);

				// send email to student that the status has been Changed
				EmailService::createNewChangeStatusEmail($recruitment, "Process");

				return redirect('/unaccept_request')->with('fyi','Success Approve Request!');
			}
		}
	}

	// submit update to unreject request (approve each)
	// routes: /approve_unreject_request/
	public function approve_unreject_request($id)
	{
		$allowed_degree_id = DepartmentController::get_allowed_degree_list();

   		$request = UnrejectRequest::where('request_id', '=', $id)->first();

		if($request == null){
			return redirect('/unreject_request')->with('errors','Cannot find request data!');
		}
		else{
			if($request->status != 'pending'){
				return redirect('/unreject_request')->with('errors','Request has been approved / rejected before!');
			}
			else{
				$recruitment = $request->recruitment()->first();
				$student = $recruitment->student()->first();

				if(array_search($student->degreeid, $allowed_degree_id) === FALSE){
					return redirect('/unreject_request')->with('errors',"You cannot approve this student's unreject request");
				}

				$job = $recruitment->job()->first();
				$job->quota = $job->quota + 1;
				$job->save();

				$recruitment->status = 'process';
				$recruitment->processed_by = Auth::user()->userid;
				$recruitment->processed_on = Carbon::now();
				$recruitment->rejected_on = null;
				$recruitment->rejected_by = null;
				$recruitment->save();

				$request->approved_by = Auth::user()->userid;
				$request->approved_on = Carbon::now();
				$request->status = 'approved';
				$request->save();

				// send notif
				NotificationController::createApproveUnrejectRequestNotif($request, Auth::user()->userid);

				// send email to student that the status has been Changed
				EmailService::createNewChangeStatusEmail($recruitment, "Process");

				return redirect('/unreject_request')->with('fyi','Success Approve Request!');
			}
		}
	}

	// submit update to unaccept request (reject each)
	// routes: /reject_unaccept_request
	public function reject_unaccept_request($id)
	{
		$allowed_degree_id = DepartmentController::get_allowed_degree_list();

		$request = UnacceptRequest::where('request_id', '=', $id)->first();

		if($request == null){
			return redirect('/unaccept_request')->with('errors','Cannot find request data!');
		}
		else{
			$rec = $request->recruitment()->first();
			$student = $rec->student()->first();

			if(array_search($student->degreeid, $allowed_degree_id) === FALSE){
				return redirect('/unaccept_request')->with('errors', "You cannot reject this student's unaccept request");
			}

			if($request->status != 'pending'){
				return redirect('/unaccept_request')->with('errors','Request has been approved / rejected before!');
			}
			else{
				$request->rejected_by = Auth::user()->userid;
				$request->rejected_on = Carbon::now();
				$request->status = 'rejected';
				$request->save();

				// send notif
				NotificationController::createRejectUnacceptRequestNotif($request, Auth::user()->userid);

				return redirect('/unaccept_request')->with('fyi','Success Reject Request!');
			}
		}
	}

	// submit update to unreject request (reject each)
	// routes: /reject_unreject_request
	public function reject_unreject_request($id)
	{
		$allowed_degree_id = DepartmentController::get_allowed_degree_list();

		$request = UnrejectRequest::where('request_id', '=', $id)->first();

		if($request == null){
			return redirect('/unreject_request')->with('errors','Cannot find request data!');
		}
		else{
			$rec = $request->recruitment()->first();
			$student = $rec->student()->first();

			if(array_search($student->degreeid, $allowed_degree_id) === FALSE){
				return redirect('/unreject_request')->with('errors', "You cannot reject this student's unaccept request");
			}

			if($request->status != 'pending'){
				return redirect('/unreject_request')->with('errors','Request has been approved / rejected before!');
			}
			else{
				$request->rejected_by = Auth::user()->userid;
				$request->rejected_on = Carbon::now();
				$request->status = 'rejected';
				$request->save();

				// send notif
				NotificationController::createRejectUnrejectRequestNotif($request, Auth::user()->userid);

				return redirect('/unreject_request')->with('fyi','Success Reject Request!');
			}
		}
	}

	// submit update to unaccept request (approve all / reject all)
	// routes: /unaccept_request_submit
	public function unaccept_request_submit(Request $request)
	{
		$allowed_degree_id = DepartmentController::get_allowed_degree_list();

		$inputs = $request->all();

		$arrayRequest = [];

		foreach ($inputs as $key => $value) {
			if(substr($key, 0, 10) === "checkbox1_"){
				$req_id = substr($key, 10);
				$req = UnacceptRequest::where('request_id', '=', $req_id)->first();
				if(empty($req)){
					$errors = 'Invalid inputs !';
				}
				else{
					array_push($arrayRequest, $req);
				}
			}
		}
		foreach ($arrayRequest as $req) {
			if($req->status != 'pending'){
				$errors = 'Some requests have been approved / rejected before !';
				break;
			}
			$rec = $req->recruitment()->first();
			$student = $rec->student()->first();

			if(array_search($student->degreeid, $allowed_degree_id) === FALSE){
				$errors = "You cannot approve / reject the selected students' unaccept request";
				break;
			}
		}
		if(!empty($errors)){
			return redirect('/unaccept_request')->with('errors', $errors);
		}
		else{
			if($request->input('action_type') == 'approve'){
				foreach ($arrayRequest as $key => $req) {
					$recruitment = $req->recruitment()->first();

					$job = $recruitment->job()->first();
					$job->quota = $job->quota + 1;
					$job->save();

					$recruitment->status = 'process';
					$recruitment->accepted_on = null;
					$recruitment->accepted_by = null;
					$recruitment->approved_on = null;
					$recruitment->approved_by = null;
					$recruitment->processed_by = Auth::user()->userid;
					$recruitment->processed_on = Carbon::now();
					$recruitment->save();

					$req->approved_by = Auth::user()->userid;
					$req->approved_on = Carbon::now();
					$req->status = 'approved';
					$req->save();

					// send notif
					NotificationController::createApproveUnacceptRequestNotif($req, Auth::user()->userid);

					// send email to student that the status has been Changed
					EmailService::createNewChangeStatusEmail($rec, "Process");
				}
			}
			else if($request->input('action_type') == 'reject'){
				foreach ($arrayRequest as $key => $req) {
					$req->rejected_by = Auth::user()->userid;
					$req->rejected_on = Carbon::now();
					$req->status = 'rejected';
					$req->save();

					// send notif
					NotificationController::createRejectUnacceptRequestNotif($req, Auth::user()->userid);
				}
			}
			else{
				return redirect('/unaccept_request')->with('errors', 'Invalid inputs');
			}
		  	return redirect('/unaccept_request')->with('fyi','Success Request!');
		}
	}

	// submit update to unreject request (approve all / reject all)
	// routes: /unreject_request_submit
	public function unreject_request_submit(Request $request)
	{
		$allowed_degree_id = DepartmentController::get_allowed_degree_list();

		$inputs = $request->all();

		$arrayRequest = [];

		foreach ($inputs as $key => $value) {
			if(substr($key, 0, 10) === "checkbox1_"){
				$req_id = substr($key, 10);
				$req = UnrejectRequest::where('request_id', '=', $req_id)->first();
				if(empty($req)){
					$errors = 'Invalid inputs !';
				}
				else{
					array_push($arrayRequest, $req);
				}
			}
		}
		foreach ($arrayRequest as $req) {
			if($req->status != 'pending'){
				$errors = 'Some requests have been approved / rejected before !';
				break;
			}
			$rec = $req->recruitment()->first();
			$student = $rec->student()->first();

			if(array_search($student->degreeid, $allowed_degree_id) === FALSE){
				$errors = "You cannot approve / reject the selected students' unreject request";
				break;
			}
		}
		if(!empty($errors)){
			return redirect('/unreject_request')->with('errors', $errors);
		}
		else{
			if($request->input('action_type') == 'approve'){
				foreach ($arrayRequest as $key => $req) {
					$recruitment = $req->recruitment()->first();

					$recruitment->status = 'process';
					$recruitment->processed_by = Auth::user()->userid;
					$recruitment->processed_on = Carbon::now();
					$recruitment->rejected_on = null;
					$recruitment->rejected_by = null;
					$recruitment->save();

					$req->approved_by = Auth::user()->userid;
					$req->approved_on = Carbon::now();
					$req->status = 'approved';
					$req->save();

					// send notif
					NotificationController::createApproveUnrejectRequestNotif($req, Auth::user()->userid);

					// send email to student that the status has been Changed
					EmailService::createNewChangeStatusEmail($rec, "Process");
				}
			}
			else if($request->input('action_type') == 'reject'){
				foreach ($arrayRequest as $key => $req) {
					$req->rejected_by = Auth::user()->userid;
					$req->rejected_on = Carbon::now();
					$req->status = 'rejected';
					$req->save();

					// send notif
					NotificationController::createRejectUnrejectRequestNotif($req, Auth::user()->userid);
				}
			}
			else{
				return redirect('/unreject_request')->with('errors', 'Invalid inputs');
			}
		  	return redirect('/unreject_request')->with('fyi','Success Request!');
		}
	}

	// Organize > Edit Recruitment
	// export all recruitment data to excel file
	// routes: /exportRecruitmentData
	public function exportRecruitmentData(){
		$allowed_degree_id = DepartmentController::get_allowed_degree_list();

		$appdata = Recruitment::join('students', 'students.studentid', '=', 'recruitments.studentid')
			->join('jobs','jobs.id','=','recruitments.jobid')
			->join('companies','jobs.companyid','=','companies.companyid')
			->join('degrees', 'degrees.id', '=', 'students.degreeid')
			->whereNull('students.deleted_at')
			->whereNull('jobs.deleted_at')
			->whereNull('companies.deleted_at')
			->whereNull('degrees.deleted_at')
			->whereIn('students.degreeid', $allowed_degree_id)
			->select(
				'recruitments.studentid as studentid',
				'students.name as studentname',
				'companies.name as companyname',
				'jobs.name as jobname',
				'recruitments.status as status',
				'degrees.name as degree_name',
				'students.email as email',
				'students.phone as phone',
				'jobs.startdate',
				'jobs.enddate',
				'jobs.duration'
			)
			->get();

		ob_end_clean();
		ob_start();
        $excel = Excel::create('list_recruitment_data', function ($excel) use ($appdata)
			{
				$excel->sheet('Sheet1', function($sheet) use($appdata)
				{
					$sheet->cell('A1', function($cell)
					{
	   					$cell->setValue('NIM');
	   				});
					$sheet->cell('B1', function($cell)
					{
	   					$cell->setValue('Student Name');
	   				});
					$sheet->cell('C1', function($cell)
					{
	   					$cell->setValue('Program');
	   				});
					$sheet->cell('D1', function($cell)
					{
	   					$cell->setValue('Company Name');
	   				});
					$sheet->cell('E1', function($cell)
					{
	   					$cell->setValue('Job Name');
	   				});
					$sheet->cell('F1', function($cell)
					{
	   					$cell->setValue('Status');
	   				});
					$sheet->cell('G1', function($cell)
					{
	   					$cell->setValue('Email');
	   				});
					$sheet->cell('H1', function($cell)
					{
	   					$cell->setValue('Phone');
					   });
					$sheet->cell('I1', function($cell)
					{
	   					$cell->setValue('Start Date');
					});
					$sheet->cell('J1', function($cell)
					{
	   					$cell->setValue('End Date');
	   				});
					$sheet->cell('K1', function($cell)
					{
						$cell->setValue('Duration');
					});
					$counter=2;
    				foreach($appdata as $record)
    				{
						$sheet->cell('A'.$counter, function($cell)use($record)
						{
							 $cell->setValue($record->studentid);
						});
						$sheet->cell('B'.$counter, function($cell)use($record)
						{
							 $cell->setValue($record->studentname);
						});
						$sheet->cell('C'.$counter, function($cell)use($record)
						{
							 $cell->setValue($record->degree_name);
						});
						$sheet->cell('D'.$counter, function($cell)use($record)
						{
							 $cell->setValue($record->companyname);
						});
						$sheet->cell('E'.$counter, function($cell)use($record)
						{
							 $cell->setValue($record->jobname);
						});
						$sheet->cell('F'.$counter, function($cell)use($record)
						{
							 $cell->setValue($record->status);
						});
						$sheet->cell('G'.$counter, function($cell)use($record)
						{
							 $cell->setValue($record->email);
						});
						$sheet->cell('H'.$counter, function($cell)use($record)
						{
							 $cell->setValue($record->phone);
						});
						$sheet->cell('I'.$counter, function($cell)use($record)
						{
							 $cell->setValue($record->startdate);
						});
						$sheet->cell('J'.$counter, function($cell)use($record)
						{
							 $cell->setValue($record->enddate);
						});
						$sheet->cell('K'.$counter, function($cell)use($record)
						{
							 $cell->setValue($record->duration);
						});
						$counter++;
					}
    			});
			})->export('xlsx');

//		$identifier = app('excel.identifier');
//		$format = $identifier->getFormatByExtension('xls');
//		$contentType = $identifier->getContentTypeByFormat($format);
//
//		return response($excel->string('xls'), 200, [
//			'Content-Type'        => $contentType,
//			'Content-Disposition' => 'attachment; filename="' . $excel->getFileName() . '.xls"',
//			'Expires'             => 'Mon, 26 Jul 1997 05:00:00 GMT', // Date in the past
//			'Last-Modified'       => Carbon::now()->format('D, d M Y H:i:s'),
//			'Cache-Control'       => 'cache, must-revalidate',
//			'Pragma'              => 'public',
//		]);
	}

	// export approved student's list to excel
	// routes: /exportAccData
	public function exportAcceptedDepartment()
	{
		$allowed_degree_id = DepartmentController::get_allowed_degree_list();

		$appdata = Recruitment::join('students', 'students.studentid', '=', 'recruitments.studentid')
			->join('jobs','jobs.id','=','recruitments.jobid')
			->join('companies','jobs.companyid','=','companies.companyid')
			->join('degrees', 'degrees.id', '=', 'students.degreeid')
			->whereNull('students.deleted_at')
			->whereNull('jobs.deleted_at')
			->whereNull('companies.deleted_at')
			->whereNull('degrees.deleted_at')
			->where('status','=', 'approved')
			->whereIn('students.degreeid', $allowed_degree_id)
			->select('recruitments.studentid as studentid','students.name as studentname','companies.name as companyname','jobs.name as jobname','recruitments.updated_at as time','recruitments.status as status','cv','jobs.id as jobid','jobs.description as jobdesc','recruitments.id as recid', 'degrees.name as degree_name')
			->get();
			ob_end_clean();
			ob_start();
        Excel::create('list_approval_data', function ($excel) use ($appdata)
			{
				$excel->sheet('Sheet1', function($sheet) use($appdata)
				{
					$sheet->cell('A1', function($cell)
					{
	   					$cell->setValue('NIM');
	   				});
					$sheet->cell('B1', function($cell)
					{
	   					$cell->setValue('Student Name');
	   				});
					$sheet->cell('C1', function($cell)
					{
	   					$cell->setValue('Program');
	   				});
					$sheet->cell('D1', function($cell)
					{
	   					$cell->setValue('Job Name');
	   				});
					$sheet->cell('E1', function($cell)
					{
	   					$cell->setValue('Jobdesc');
	   				});
					$sheet->cell('F1', function($cell)
					{
	   					$cell->setValue('Company Name');
	   				});
					$sheet->cell('G1', function($cell)
					{
	   					$cell->setValue('Status');
	   				});
	 				$sheet->cell('H1', function($cell)
					{
	   					$cell->setValue('Last Update');
	   				});

					$counter=2;
    				foreach($appdata as $record)
    				{
						$sheet->cell('A'.$counter, function($cell)use($record)
						{
							 $cell->setValue($record->studentid);
						});
						$sheet->cell('B'.$counter, function($cell)use($record)
						{
							 $cell->setValue($record->studentname);
						});
						$sheet->cell('C'.$counter, function($cell)use($record)
						{
							 $cell->setValue($record->degree_name);
						});
						$sheet->cell('D'.$counter, function($cell)use($record)
						{
							 $cell->setValue($record->jobname);
						});
						$sheet->cell('E'.$counter, function($cell)use($record)
						{
							 $cell->setValue($record->jobdesc);
						});

						$sheet->cell('F'.$counter, function($cell)use($record)
						{
							 $cell->setValue($record->companyname);
						});
						$sheet->cell('G'.$counter, function($cell)use($record)
						{
							 $cell->setValue($record->status);
						});
						$sheet->cell('H'.$counter, function($cell)use($record)
						{
							 $cell->setValue($record->time);
						});
						$counter++;
					}
    			});
			})->export('xlsx');
	}

	// Organize > Approved page
	// routes: /accepted
	public function acceptedDepartment(Request $request)
	{
		$allowed_degree_id = DepartmentController::get_allowed_degree_list();
//		return response()->json($allowed_degree_id);
		//simple paging + searching
		$perpage = 10;

		$appdata = Recruitment::join('students', 'students.studentid', '=', 'recruitments.studentid')
			->join('jobs','jobs.id','=','recruitments.jobid')
			->join('companies','jobs.companyid','=','companies.companyid')
			->join('degrees', 'degrees.id', '=', 'students.degreeid')
			->whereNull('students.deleted_at')
			->whereNull('jobs.deleted_at')
			->whereNull('companies.deleted_at')
			->whereNull('degrees.deleted_at')
			->where('status','=', 'approved')
			->whereIn('students.degreeid', $allowed_degree_id)
			->select('recruitments.id as recruitmentid','recruitments.studentid as studentid','recruitments.approved_on as approvedate','students.name as studentname','students.school as school','students.campus as campus','companies.name as companyname','jobs.name as jobname','recruitments.updated_at as time','recruitments.status as status','cv','jobs.id as jobid','jobs.description as jobdesc','recruitments.id as recid', 'degrees.name as degree_name','jobs.startdate as startdate','jobs.enddate as enddate')
			->get();
		if ($request->Searching != null)
		{
			// this query should be the same, but add where clause
			$appdata = Recruitment::join('students', 'students.studentid', '=', 'recruitments.studentid')
		        ->join('jobs','jobs.id','=','recruitments.jobid')
		        ->join('companies','jobs.companyid','=','companies.companyid')
				->join('degrees', 'degrees.id', '=', 'students.degreeid')
				->whereNull('students.deleted_at')
				->whereNull('jobs.deleted_at')
				->whereNull('companies.deleted_at')
				->whereNull('degrees.deleted_at')
				->where('status','=', 'approved')
		        ->where(function ($query) use ($request) {
		        	$query->where('students.name', 'like', '%'.$request->Searching.'%')
						->orWhere('students.studentid', 'like', '%'.$request->Searching.'%')
						->orWhere('students.school', 'like', '%'.$request->Searching.'%')
						->orWhere('students.campus', 'like', '%'.$request->Searching.'%')
						->orWhere('companies.name', 'like', '%'.$request->Searching.'%')
						->orWhere('degrees.name', 'like', '%'.$request->Searching.'%')
						->orWhere('jobs.name', 'like', '%'.$request->Searching.'%');
				})
				->whereIn('students.degreeid', $allowed_degree_id)
				->select('recruitments.id as recruitmentid','recruitments.studentid as studentid','recruitments.approved_on as approvedate','students.name as studentname','students.school as school','students.campus as campus','companies.name as companyname','jobs.name as jobname','recruitments.updated_at as time','recruitments.status as status','cv','jobs.id as jobid','jobs.description as jobdesc','recruitments.id as recid', 'degrees.name as degree_name','jobs.startdate as startdate','jobs.enddate as enddate')
		        ->get();
			$Searching = $request->Searching;
		}
		else
		{
			$Searching = "";
		}

		if (Auth::user()->role == 'department' && Auth::user()->department->campus != null) {
			$appdata = $appdata->where('campus', Auth::user()->department->campus);
		}

		$interviews = [];
		foreach($appdata as $d){
			 $interviews[$d->recruitmentid] = Interview::where('recruitment_id',$d->recruitmentid)->where('status', 'accept')->get();
		}
		$totalPage = ceil(sizeof($appdata)/$perpage);
  		$curpage = (isset($request->page))?$request->page:1;
  		$offset = ($curpage-1)*$perpage;

  		$appdata = $appdata->slice($offset,$perpage);
		
		return view('department.accepted',
		[
			'appdata' => $appdata,
			'totalPage' => $totalPage,
			'currentPage' => $curpage,
			'Searching' => $Searching,
			'interviews' => $interviews
		]);
	}

	// Student's home page
	// routes: /vacancy
	public function applyStudent()
	{
		//kalau sudah diapprove gakan bisa kesini
		if($this->checkAccepted() == "accepted")
		{
			//return redirect('/learningplan'); -> sementara
			return redirect('/status');
		}

		$counter = 0;
		$status = "available";
		$student = Auth::user()->student()->first();
		
		$studentSemester = new Studentsemester();
		if($latestSemester = $studentSemester->getLatestSemesterScore($student->studentid)){
			$gpa = $latestSemester->ipk;
		}else{
			$gpa = 0;
		}
		//get semua company
		$company = Company::join('jobs', function ($join) {
			$join->where('jobs.job_state', '=', 'open');
			$join->on('jobs.companyid', '=', 'companies.companyid');
		})->whereIn('jobs.id', JobsDegrees::where('degreeid', '=', $student->degreeid)->pluck('jobid'))
			->join('users', 'users.userid', '=', 'companies.companyid')
			->whereNull('jobs.deleted_at')
			->whereNull('users.deleted_at')
			->where('jobs.quota', '>', 0)
		  ->whereRaw('(jobs.is_using_bipp_min_ipk = 0 or ' . \App\Bipp::first()->minimumIPK() . ' <= '.$gpa.')')
			->where('jobs.job_state', '=', 'open')
			->where('users.status', '=', 'active')
			->where('users.active_period','=','2010')
			->select('companies.name as name', 'companies.companyid')
			->groupBy('companies.companyid')
			->orderBy('companies.id')
			->get();

			$test = Company::join('jobs', function ($join) {
				$join->where('jobs.job_state', '=', 'open');
				$join->on('jobs.companyid', '=', 'companies.companyid');
			})->get();

		//dapetin all companies yang sudah dilamar student
		$listCompanyAppliedByUser = Company::join('jobs', 'jobs.companyid', '=', 'companies.companyid')
            ->join('recruitments','jobs.id','=','recruitments.jobid')
            ->join('students','recruitments.studentid','=','students.studentid')
            ->whereNull('jobs.deleted_at')
            ->whereNull('recruitments.deleted_at')
            ->whereNull('students.deleted_at')
            ->where('students.studentid','=', $student->studentid)
            ->whereIn('recruitments.status', array('waiting','accepted','process'))
            ->where('recruitments.deleted_at', null)
            ->get();;

		//apabila jumlah company yang dilamar sudah >= 5,D
		//gakan boleh apply lagi kecuali di company yang sama
		if($listCompanyAppliedByUser->count() >= 5)
		{
			$status = "maxcompany";
		}

		//iterate setiap company
		foreach($company as $c)
		{
			$c->jobs = Job::where('companyid', '=', $c->companyid)
	            ->where('quota','>','0')
				->where('job_state','=','open')
	            ->whereIn('jobs.id',
	                JobsDegrees::where('degreeid', '=', $student->degreeid)
	                ->select('jobid')
	                ->get()
	                ->toArray())
	            ->get();

			$c->countOfNewJobs = $c->jobs->filter(function ($j){
				return $j->created_at >= Carbon::yesterday();
			})->count();

			foreach($c->jobs as $job)
			{
				if($job->created_at >= Carbon::yesterday()){
					$job->isNew = true;
				}

				//get data recruitment apabila student yang login pernah apply ke job tersebut
				$data = $job->recruitment()->where('studentid', '=', Auth::user()->userid)->first();

				//jika gada, tandanya belum pernah di apply
				if($data == null)
				{
					$job->status = 'not applied';
				}
				else
				{
					//jika student sudah pernah apply ke job tersebut, maka disimpan statusnya
					$job->status = $data->status;
					if($job->status == 'approved')
					{
						//bila student sudah diaccept untuk job yang bersangkutan, status student itu menjadi accepted
						$status = "approved";
					}
				}
			}
		}

		//set to array values, untuk pengecekan di view nya (menggunakan in_array)
		$data = $listCompanyAppliedByUser;
		$listCompanyAppliedByUser = array();
		foreach($data as $d)
		{
			array_push($listCompanyAppliedByUser, $d->companyid);
		}

		$student = Auth::user()->student()->first();

		$mailboxCount = Mailbox::whereIn('type', array('interview', 'mailbox'))
			->where('status', '=', 'unseen')
			->where('recipientid', '=', Auth::user()->userid)
			->count();

		//get count of interview schedule invitation by student
		$all_interview_schedule = Interview::join('recruitments', 'recruitments.id', '=', 'interviews.recruitment_id')
			->whereNull('recruitments.deleted_at')
			->where('recruitments.status', '=', 'process')
			->where('interviews.interviewee', '=', Auth::user()->userid)
			->where('interviews.status', '=', 'waiting')
			->get();

		$new_job = Job::join('companies', 'companies.companyid', '=', 'jobs.companyid')
			->join('jobsdegrees', 'jobsdegrees.jobid', '=', 'jobs.id')
			->whereNull('companies.deleted_at')
			->whereNull('jobsdegrees.deleted_at')
			->where('jobs.created_at', '>=', Carbon::yesterday())
			->where('jobs.job_state', 'open')
			->where('jobsdegrees.degreeid', '=', Auth::user()->student()->first()->degreeid)
			->get();

		$new_job_company_name_only =
			Job::join('companies', 'companies.companyid', '=', 'jobs.companyid')
			->join('jobsdegrees', 'jobsdegrees.jobid', '=', 'jobs.id')
			->whereNull('companies.deleted_at')
			->whereNull('jobsdegrees.deleted_at')
			->where('jobs.created_at', '>=', Carbon::yesterday())
			->where('jobsdegrees.degreeid', '=', Auth::user()->student()->first()->degreeid)
			->where('jobs.job_state', 'open')
			->select('companies.name as company_name')
			->orderBy('jobs.created_at', 'desc')
			->distinct()
			->get();

		// check if student has interview invitation that has not been replied
		// for 4 days
		$have_old_invitation = false;
		$oldestDate = Carbon::now()->subDays(4);
		$old_interview_schedule = Interview::join('recruitments', 'interviews.recruitment_id', '=', 'recruitments.id')
			->whereNull('recruitments.deleted_at')
			->whereIn('recruitments.status', ['process'])
			->whereIn('interviews.status', ['waiting'])
			->whereDate('interviews.created_at', '<', $oldestDate)
			->where('interviews.interviewee', '=', Auth::user()->userid)
			->select('recruitments.id as recruitment_id')
			->get();
		if($old_interview_schedule->count() > 0){
			$have_old_invitation = true;
		}

		return view('student.vacancy',
			[ 
				'accepted' => $this->checkAccepted(),
				'companys' => $company,
				'listcompany'=> $listCompanyAppliedByUser,
				'pending_interview_invitation_count' => $all_interview_schedule->count(),
				'new_job_count' => $new_job->count(),
				'new_job_company_name_only' => $new_job_company_name_only,
				'status'=> $status,
				'student'=> $student,
			 	'mailboxCount' => $mailboxCount,
			 	'notifCount' => app('App\Http\Controllers\MailboxController')->getCountNotification(),
			 	'notif' => app('App\Http\Controllers\MailboxController')->getLatestNotification(),
				'have_old_invitation' => $have_old_invitation
			]);
	}

	// student apply job
	// routes: /applyjob/
	public function insertApplyStudent($tokenid)
	{
		$user = Auth::user();
		$userid = $user->userid;

		//dapetin all companies yang sudah dilamar student
		$listCompanyAppliedByUser = Company::join('jobs', 'jobs.companyid', '=', 'companies.companyid')
			->join('recruitments','jobs.id','=','recruitments.jobid')
			->join('students','recruitments.studentid','=','students.studentid')
			->whereNull('jobs.deleted_at')
			->whereNull('recruitments.deleted_at')
			->whereNull('students.deleted_at')
			->where('students.studentid','=', $user->userid)
			->whereIn('recruitments.status', array('waiting','accepted','process'))
			->where('recruitments.deleted_at', null)
			->get();

		//apabila jumlah company yang dilamar sudah >= 5,D
		//gakan boleh apply lagi kecuali di company yang sama
		if($listCompanyAppliedByUser->count() >= 5)
		{
			return redirect()->back()->with('err', 'You cannot apply to this job, because you have already applied to 5 other jobs.');
		}

		$student = $user->student()->first();

		$job = Job::where('token', '=', $tokenid)->first();

		if($job == null){
			return redirect('/vacancy')->with('err', 'Job not found');
		}
		if($job->job_state != 'open'){
			return redirect('/vacancy')->with('err', 'You cannot apply to this job, because the job has been closed.');
		}

		$company = $job->company()->first();
		if($company->user()->first()->status == 'nonactive'){
			//if the company is unapproved / not active
			return redirect('/vacancy')->with('err', 'You cannot apply to this job.');
		}

		if($company->state != 'open'){
			// if the company's state = closed

			// company state could be 'open' or 'closed'
			return redirect('/vacancy')->with('err', 'You cannot apply to this job, because the company has close the application.');
		}

		if($job->jobsdegrees()->where('degreeid', '=', $student->degreeid)->first() == null){
			//jika job itu tidak dibuka untuk degree milik student
			//mis: job dari Binus hanya untuk Computer Science, maka student dari major Cyber Security tidak bisa apply
			return redirect('/vacancy')->with('err', 'You cannot apply to this job.');
		}

		if(Recruitment::where('jobid', '=', $job->id)->where('studentid', '=', $student->studentid)->get()->count() > 0){
			return redirect('/vacancy')->with('err', 'You cannot apply to this job more than once.');
		}

		//insert ke table recruitment
		$rec = new Recruitment;
		$rec->jobid = $job->id;
		$rec->studentid = $userid;
		$rec->processed_on = null;
		$rec->status = 'waiting';
		$rec->save();

		//$token = Recruitment::orderBy('id', 'desc')->first();
		$resToken = Hash::make($rec->id.$rec->jobid.$rec->studentid);
		$resToken = str_replace('.', 'H', str_replace("$", "S", str_replace("/", "X", $resToken)));

		Recruitment::where('id', $rec->id)->update(['token' => $resToken]);

		$profilesender = $user->student()->first();

		$mailboxAppJob = new Mailbox;
		$mailboxAppJob->senderid = Auth::user()->userid;
		$mailboxAppJob->recipientid = $job->companyid;
		$mailboxAppJob->subject = 'You have new applicant';
		$mailboxAppJob->date = date("Y-m-d H:i:s");
		$mailboxAppJob->message = $profilesender->name.' apply as '.$job->name.'.';
		$mailboxAppJob->status = 'unseen';
		$mailboxAppJob->type = 'notif';
		$mailboxAppJob->token = '';
		$mailboxAppJob->save();

		return redirect('/vacancy')->with('success', 'You have successfully applied for a job.');
	}

	// for student to unapply job
	// routes: /unapply
	public function deleteApplyStudent($tokenid)
	{
		$jobid = Job::where('token', '=', $tokenid)->first();
		$profilesender = Auth::user()->student()->first();

		$recruitment = Recruitment::where('jobid', $jobid->id)
	        ->where('studentid', Auth::user()->userid)
			->first();

		if($recruitment->status != 'waiting'){
			return redirect('/status')->with('gagalNih', 'You cannot unapply this job.');
		}

		// delete semua jenis request terkait job application ini
		$recruitment->approval_request()->delete();
		$recruitment->unaccept_request()->delete();
		$recruitment->unreject_request()->delete();

		// delete interview
		$recruitment->interview()->delete();

		$recruitment->unapplied_by = Auth::user()->userid;
		$recruitment->unapplied_on = Carbon::now();
		$recruitment->save();

		// delete all mailbox related to this recruitment
		Mailbox::where('senderid', '=', Auth::user()->userid)
			->where('type', '=', 'notif')
			->where('message', 'like', '%'.$jobid->name.'%')
			->delete();

		$mailboxUn = new Mailbox;
		$mailboxUn->senderid = Auth::user()->userid;
		$mailboxUn->recipientid = $jobid->companyid;
		$mailboxUn->subject = 'You have new job application cancelation';
		$mailboxUn->date = date("Y-m-d H:i:s");
		$mailboxUn->message = $profilesender->name.' unapply as '.$jobid->name.'.';
		$mailboxUn->status = 'unseen';
		$mailboxUn->type = 'notif';
		$mailboxUn->token = '';
		$mailboxUn->save();

		$recruitment->delete();

		return redirect('/status')->with('gagalNih', 'You have successfully unapplied for a job.');
	}

	// Status page
	// routes: /status
	public function priorityStudent()
	{
		$user = Auth::user();

		//dapetin data recruitment milik user yang login untuk ditampilin
		$status = Recruitment::join('jobs', 'jobs.id', '=', 'recruitments.jobid')
	        ->join('companies', 'companies.companyid', '=', 'jobs.companyid')
			->join('users', 'users.userid', '=', 'companies.companyid')
			->whereNull('jobs.deleted_at')
			->whereNull('companies.deleted_at')
			->whereNull('users.deleted_at')
			->where('studentid', '=', $user->userid)
			->where('companies.deleted_at', null)
	        ->select('recruitments.id as recruitment_id',
				'recruitments.status',
				'recruitments.approved_on',
				'jobs.name as jobname',
				'jobs.duration',
				'jobs.startdate',
				'jobs.enddate',
				'companies.name as companyname',
				'linktest',
				'jobs.token as jToken',
				'recruitments.token as rToken',
				'companies.companyid as companyid',
				'recruitments.rejected_by as rejected_by',
				'users.status as company_status')
	        ->orderBy('recruitments.status', 'asc')
	        ->get();

    	//search all interviews for each recruitments data
		foreach ($status as $s) {
			$interview = Interview::where('recruitment_id', '=', $s->recruitment_id)
				->orderBy('created_at', 'desc')
				->get();
			if(count($interview) > 0){
				$i = $interview->first();
				$s->iStatus = $i->status;
				$s->iToken = $i->token;
				$s->iPlace = $i->location;
				$s->iContact = $i->pic;
				$s->iPhone = $i->phone;
				$s->iDate = $i->date;
				$s->iTime = $i->time;
			}
			else{
				$s->iStatus = null;
			}

			if($s->status == 'rejected'){
				if($user->student()->first()->requiringDepartmentApproval()){
					$approval_request = ApprovalRequest::where('recruitment_id', '=', $s->recruitment_id)
						->first();
					if($approval_request != null){
						$s->is_rejected_by_department = true;
						$s->rejected_by_department_on = $approval_request->rejected_on;
					}
				}
				else{
					$s->is_rejected_by_department = false;
				}
			}
		}

		$mailboxCount = Mailbox::whereIn('type', array('interview', 'mailbox'))
			->where('status', '=', 'unseen')
			->where('recipientid', '=', Auth::user()->userid)
			->count();

		$unapplied_recruitments = Recruitment::join('jobs', 'jobs.id', '=', 'recruitments.jobid')
			->where('studentid', '=', Auth::user()->userid)
			->select(
				'recruitments.deleted_at as deleted_at', 'recruitments.unapplied_on as unapplied_on',
				'recruitments.unapplied_by as unapplied_by',
				'jobs.name as job_name',
				'recruitments.created_at as applied_on',
				'jobs.companyid as company_id',
				'jobs.duration'
				)
			->onlyTrashed()
			->get();

		foreach($unapplied_recruitments as $u){
			$company = Company::where('companyid', '=', $u->company_id)->first();

			$u->company_name = $company->name;
		}

		// check if student has interview invitation that has not been replied
		// for 4 days
		$have_old_invitation = false;
		$oldestDate = Carbon::now()->subDays(4);
		$old_interview_schedule = Interview::join('recruitments', 'interviews.recruitment_id', '=', 'recruitments.id')
			->whereNull('recruitments.deleted_at')
			->whereIn('recruitments.status', ['process'])
			->whereIn('interviews.status', ['waiting'])
			->whereDate('interviews.created_at', '<', $oldestDate)
			->where('interviews.interviewee', '=', Auth::user()->userid)
			->select('recruitments.id as recruitment_id')
			->get();

		if($old_interview_schedule->count() > 0){
			$have_old_invitation = true;
		}

		return view('student.status',
			[
				'student' => Auth::user()->student()->first(),
				'accepted' => $this->checkAccepted(),
				'statuss' => $status,
				'unapplied_recruitments' => $unapplied_recruitments,
				'mailboxCount' => $mailboxCount,
				'notifCount' => app('App\Http\Controllers\MailboxController')->getCountNotification(),
				'notif' => app('App\Http\Controllers\MailboxController')->getLatestNotification(),
				'have_old_invitation' => $have_old_invitation
			]);
	}

	// company's home page
	// routes: /homec
	public function indexCompany()
	{
		$dataU = null;
		$user = Auth::user();
		$company = Company::where('companyid', '=', Auth::user()->userid)
			->first();
		if ($company->state == 'closed'){
			$data = Recruitment::join('jobs', 'jobs.id', '=', 'recruitments.jobid')
				->join('companies', 'companies.companyid', '=', 'jobs.companyid')
				->join('students', 'students.studentid', '=', 'recruitments.studentid')
				->leftJoin(DB::Raw('(select recruitment_id as rec_id, COUNT(*) as unaccept_request_count from unaccept_request where unaccept_request.deleted_at IS NULL and unaccept_request.status = "pending" group by recruitment_id) as unaccept_request_data'), 'rec_id', '=', 'recruitments.id')
				->leftJoin(DB::Raw('(select * from interviews where interviews.deleted_at IS NULL and interviews.status <> "reschedule" and interviews.deleted_at is null) as interviewData'), 'interviewData.recruitment_id', '=', 'recruitments.id')
				->whereNull('jobs.deleted_at')
				->whereNull('companies.deleted_at')
				->whereNull('students.deleted_at')
				->where('jobs.companyid', '=', Auth::user()->userid)
				->whereIn('recruitments.status', ['approved', 'accepted'])
				->select(
					'unaccept_request_data.unaccept_request_count as unaccept_request',
					'recruitments.id as recId',
					'jobs.name as position',
					'jobs.token as token',
					'jobs.duration as duration',
					'jobs.description as description',
					'jobs.linktest as linktest',
					'recruitments.status as reStatus',
					'companies.name as company',
					'students.name as student',
					'recruitments.token as rToken',
					'interviewData.date as inDate',
					'interviewData.time as inTime',
					'interviewData.status as inStatus',
					'students.studentid as nim',
					'students.cv as cv',
					'students.email as email',
					'students.phone as phone',
					DB::Raw('(select jobs.companyid from jobs join recruitments on jobs.id = recruitments.jobid where jobs.deleted_at IS NULL and recruitments.deleted_at IS NULL and status = "approved" and recruitments.studentid = nim limit 1) as stateStudent'),
					DB::Raw('(select recruitments.status from jobs join recruitments on jobs.id = recruitments.jobid where jobs.deleted_at IS NULL and recruitments.deleted_at IS NULL and status = "approved" and recruitments.studentid = nim limit 1) as stateAccepted'),
					'students.sex as gender',
					'recruitments.rejected_by as rejected_by',
					'students.major as program',
					'students.campus',
					DB::Raw('NULL as expired_at')
					)
				->orderBy('recruitments.approved_on', 'asc')
				->distinct()
				->get();
			$state = 'closed';
		}
		else
		{
			$data0 = Recruitment::join('jobs', 'jobs.id', '=', 'recruitments.jobid')
				->join('companies', 'companies.companyid', '=', 'jobs.companyid')
				->join('students', 'students.studentid', '=', 'recruitments.studentid')
				->leftJoin(DB::Raw('(select recruitment_id as rec_id, COUNT(*) as unaccept_request_count from unaccept_request where unaccept_request.deleted_at IS NULL and unaccept_request.status = "pending" group by recruitment_id) as unaccept_request_data'), 'rec_id', '=', 'recruitments.id')
				->leftJoin(DB::Raw('(select * from interviews where interviews.deleted_at IS NULL and interviews.status <> "reschedule" and interviews.deleted_at is null) as interviewData'), 'interviewData.recruitment_id', '=', 'recruitments.id')
				->whereNull('jobs.deleted_at')
				->whereNull('companies.deleted_at')
				->whereNull('students.deleted_at')
				->where('jobs.companyid', '=', Auth::user()->userid)
				->where('recruitments.status', '=', 'approved')
				->select(
					'unaccept_request_data.unaccept_request_count as unaccept_request',
					'recruitments.id as recId',
					'jobs.name as position',
					'jobs.token as token',
					'jobs.duration as duration',
					'jobs.description as description',
					'jobs.linktest as linktest',
					'recruitments.status as reStatus',
					'companies.name as company',
					'students.name as student',
					'recruitments.token as rToken',
					'interviewData.date as inDate',
					'interviewData.time as inTime',
					'interviewData.status as inStatus',
					'students.studentid as nim',
					'students.cv as cv',
					'students.email as email',
					'students.phone as phone',
					DB::Raw('(select jobs.companyid from jobs join recruitments on jobs.id = recruitments.jobid where jobs.deleted_at IS NULL and recruitments.deleted_at IS NULL and status = "approved" and recruitments.studentid = nim limit 1) as stateStudent'),
					DB::Raw('(select recruitments.status from jobs join recruitments on jobs.id = recruitments.jobid where jobs.deleted_at IS NULL and recruitments.deleted_at IS NULL and status = "approved" and recruitments.studentid = nim limit 1) as stateAccepted'),
					'students.sex as gender',
					'recruitments.rejected_by as rejected_by',
					'students.major as program',
					'students.campus',
					DB::Raw('NULL as expired_at')
					)
				->distinct();
			$data1 = Recruitment::join('jobs', 'jobs.id', '=', 'recruitments.jobid')
				->join('companies', 'companies.companyid', '=', 'jobs.companyid')
				->join('students', 'students.studentid', '=', 'recruitments.studentid')
				->leftJoin(DB::Raw('(select recruitment_id as rec_id, COUNT(*) as unaccept_request_count from unaccept_request where unaccept_request.deleted_at IS NULL and unaccept_request.status = "pending" group by recruitment_id) as unaccept_request_data'), 'rec_id', '=', 'recruitments.id')
				->leftJoin(DB::Raw('(select * from interviews where interviews.deleted_at IS NULL and interviews.status <> "reschedule" and interviews.deleted_at is null) as interviewData'), 'interviewData.recruitment_id', '=', 'recruitments.id')
				->whereNull('jobs.deleted_at')
				->whereNull('companies.deleted_at')
				->whereNull('students.deleted_at')
				->where('jobs.companyid', '=', Auth::user()->userid)
				->where('recruitments.status', '=', 'accepted')
				->select(
					'unaccept_request_data.unaccept_request_count as unaccept_request',
					'recruitments.id as recId',
					'jobs.name as position',
					'jobs.token as token',
					'jobs.duration as duration',
					'jobs.description as description',
					'jobs.linktest as linktest',
					'recruitments.status as reStatus',
					'companies.name as company',
					'students.name as student',
					'recruitments.token as rToken',
					'interviewData.date as inDate',
					'interviewData.time as inTime',
					'interviewData.status as inStatus',
					'students.studentid as nim',
					'students.cv as cv',
					'students.email as email',
					'students.phone as phone',
					DB::Raw('(select jobs.companyid from jobs join recruitments on jobs.id = recruitments.jobid where jobs.deleted_at IS NULL and recruitments.deleted_at IS NULL and status = "approved" and recruitments.studentid = nim limit 1) as stateStudent'),
					DB::Raw('(select recruitments.status from jobs join recruitments on jobs.id = recruitments.jobid where jobs.deleted_at IS NULL and recruitments.deleted_at and status = "approved" and recruitments.studentid = nim limit 1) as stateAccepted'),
					'students.sex as gender',
					'recruitments.rejected_by as rejected_by',
					'students.major as program',
					'students.campus',
					DB::Raw('NULL as expired_at')
					)
				->distinct();
			$data2 = Recruitment::join('jobs', 'jobs.id', '=', 'recruitments.jobid')
				->join('companies', 'companies.companyid', '=', 'jobs.companyid')
				->join('students', 'students.studentid', '=', 'recruitments.studentid')
				->leftJoin(DB::Raw('(select recruitment_id as rec_id, COUNT(*) as unaccept_request_count from unaccept_request where unaccept_request.deleted_at IS NULL and unaccept_request.status = "pending" group by recruitment_id) as unaccept_request_data'), 'rec_id', '=', 'recruitments.id')
				->leftJoin(DB::Raw('(select * from interviews where interviews.deleted_at IS NULL and interviews.status <> "reschedule" and interviews.deleted_at is null) as interviewData'), 'interviewData.recruitment_id', '=', 'recruitments.id')
				->whereNull('jobs.deleted_at')
				->whereNull('companies.deleted_at')
				->whereNull('students.deleted_at')
				->where('jobs.companyid', '=', Auth::user()->userid)
				->where('recruitments.status', '=', 'rejected')
				->select(
					'unaccept_request_data.unaccept_request_count as unaccept_request',
					'recruitments.id as recId',
					'jobs.name as position',
					'jobs.token as token',
					'jobs.duration as duration',
					'jobs.description as description',
					'jobs.linktest as linktest',
					'recruitments.status as reStatus',
					'companies.name as company',
					'students.name as student',
					'recruitments.token as rToken',
					'interviewData.date as inDate',
					'interviewData.time as inTime',
					'interviewData.status as inStatus',
					'students.studentid as nim',
					'students.cv as cv',
					'students.email as email',
					'students.phone as phone',
					DB::Raw('(select jobs.companyid from jobs join recruitments on jobs.id = recruitments.jobid where jobs.deleted_at IS NULL and recruitments.deleted_at IS NULL and status = "approved" and recruitments.studentid = nim limit 1) as stateStudent'),
					DB::Raw('(select recruitments.status from jobs join recruitments on jobs.id = recruitments.jobid where jobs.deleted_at IS NULL and recruitments.deleted_at IS NULL and status = "approved" and recruitments.studentid = nim limit 1) as stateAccepted'),
					'students.sex as gender',
					'recruitments.rejected_by as rejected_by',
					'students.major as program',
					'students.campus',
					DB::Raw('NULL as expired_at')
				)
				->distinct();
			$data = Recruitment::join('jobs', 'jobs.id', '=', 'recruitments.jobid')
				->join('companies', 'companies.companyid', '=', 'jobs.companyid')
				->join('students', 'students.studentid', '=', 'recruitments.studentid')
				->leftJoin(DB::Raw('(select recruitment_id as rec_id, COUNT(*) as unaccept_request_count from unaccept_request where unaccept_request.deleted_at IS NULL and unaccept_request.status = "pending" group by recruitment_id) as unaccept_request_data'), 'rec_id', '=', 'recruitments.id')
				->leftJoin(DB::Raw('(select * from interviews where interviews.deleted_at IS NULL and interviews.status <> "reschedule" and interviews.deleted_at is null) as interviewData'), 'interviewData.recruitment_id', '=', 'recruitments.id')
				->whereNull('jobs.deleted_at')
				->whereNull('companies.deleted_at')
				->whereNull('students.deleted_at')
				->where('jobs.companyid', '=', Auth::user()->userid)
				->where('recruitments.status', '=', 'process')
				->select(
					'unaccept_request_data.unaccept_request_count as unaccept_request',
					'recruitments.id as recId',
					'jobs.name as position',
					'jobs.token as token',
					'jobs.duration as duration',
					'jobs.description as description',
					'jobs.linktest as linktest',
					'recruitments.status as reStatus',
					'companies.name as company',
					'students.name as student',
					'recruitments.token as rToken',
					'interviewData.date as inDate',
					'interviewData.time as inTime',
					'interviewData.status as inStatus',
					'students.studentid as nim',
					'students.cv as cv',
					'students.email as email',
					'students.phone as phone',
					DB::Raw('(select jobs.companyid from jobs join recruitments on jobs.id = recruitments.jobid where jobs.deleted_at IS NULL and recruitments.deleted_at IS NULL and status = "approved" and recruitments.studentid = nim limit 1) as stateStudent'),
					DB::Raw('(select recruitments.status from jobs join recruitments on jobs.id = recruitments.jobid where jobs.deleted_at IS NULL and recruitments.deleted_at IS NULL and status = "approved" and recruitments.studentid = nim limit 1) as stateAccepted'),
					'students.sex as gender',
					'recruitments.rejected_by as rejected_by',
					'students.major as program',
					'students.campus',
					DB::Raw('DATE_ADD(recruitments.processed_on, INTERVAL 14 DAY) as expired_at')
				)
				->distinct()
				->union($data0)
				->union($data1)
				->union($data2)
				->orderBy(DB::raw('-expired_at'), 'desc')
				->get();
			$dataU = Recruitment::join('jobs', 'jobs.id', '=', 'recruitments.jobid')
				->join('companies', 'companies.companyid', '=', 'jobs.companyid')
				->join('students', 'students.studentid', '=', 'recruitments.studentid')
				->leftJoin(DB::Raw('(select * from interviews where interviews.deleted_at IS NULL and interviews.status <> "reschedule" and interviews.deleted_at is null) as interviewData'), 'interviewData.recruitment_id', '=', 'recruitments.id')
				->whereNull('jobs.deleted_at')
				->whereNull('companies.deleted_at')
				->whereNull('students.deleted_at')
				->where('jobs.companyid', '=', Auth::user()->userid)
				->where('recruitments.status', '=', 'waiting')
				->select(
					'recruitments.id as recId',
					'jobs.name as position',
					'jobs.token as token',
					'jobs.duration as duration',
					'jobs.description as description',
					'jobs.linktest as linktest',
					'recruitments.status as reStatus',
					'companies.name as company',
					'students.name as student',
					'recruitments.token as rToken',
					'interviewData.date as inDate',
					'interviewData.time as inTime',
					'interviewData.status as inStatus',
					'students.studentid as nim',
					'students.cv as cv',
					'students.email as email',
					'students.phone as phone',
					DB::Raw('(select jobs.companyid from jobs join recruitments on jobs.id = recruitments.jobid where jobs.deleted_at IS NULL and recruitments.deleted_at IS NULL and status = "approved" and recruitments.studentid = nim limit 1) as stateStudent'),
					DB::Raw('(select recruitments.status from jobs join recruitments on jobs.id = recruitments.jobid where jobs.deleted_at IS NULL and recruitments.deleted_at IS NULL and status = "approved" and recruitments.studentid = nim limit 1) as stateAccepted'),
					'students.sex as gender',
					'recruitments.rejected_by as rejected_by',
					'students.major as program',
					'students.campus',
					DB::Raw('DATE_ADD(recruitments.created_at, INTERVAL 14 DAY) as expired_at')
					)
				->orderBy('expired_at', 'asc')
				->distinct()
				->get();
			$state = 'open';
		}

		//get count of interview reschedule request by student
		$count = 0;
		$all_recruitments = Recruitment::join('jobs', 'jobs.id', '=', 'recruitments.jobid')
			->whereNull('jobs.deleted_at')
			->where('jobs.companyid', '=', Auth::user()->userid)
			->select('recruitments.id as id')
			->get();
		foreach ($all_recruitments as $rec) {
			$last_interview = Interview::where('recruitment_id', '=', $rec->id)
				->orderBy('created_at', 'desc')
				->first();
			if(!empty($last_interview) && $last_interview->status == 'reschedule'){
				$count++;
			}
		}

		$dataSort = Recruitment::join('jobs', 'jobs.id', '=', 'recruitments.jobid')
			->join('companies', 'companies.companyid', '=', 'jobs.companyid')
			->join('students', 'students.studentid', '=', 'recruitments.studentid')
			->whereNull('jobs.deleted_at')
			->whereNull('companies.deleted_at')
			->whereNull('students.deleted_at')
			->whereIn('recruitments.status', ['approved'])
			->where('jobs.companyid', '=', Auth::user()->userid)
			->select('jobs.name as position', DB::raw('count(*) as counting'))
			->groupBy('position')
			->get();
		$counting = array();

		foreach($dataSort as $value)
		{
			$counting[$value->position] = $value->counting;
		}

		$mailboxCount = Mailbox::where('type', '=', 'mailbox')
			->where('status', '=', 'unseen')
			->where('recipientid', '=', Auth::user()->userid)
			->count();

		if(!empty($dataU)){
			foreach($dataU as $du){
                $student = Student::where('studentid', '=', $du->nim)->first();

				if($student->isDurationSixMonth()){
					$du->duration = 6;
				}

				$last_semester = Studentsemester::where('studentid', '=', $du->nim)->select(DB::raw('MAX(semester) as max_semester'))->first();

				$du->last_ipk = Studentsemester::where('studentid', '=', $du->nim)->where('semester', '=', $last_semester['max_semester'])->first();

//				$du->validBIPP = false;
//                $minimum_ipk = Company::getBippIpk();
//				if ($minimum_ipk == null) {
//					$du->validBIPP = true;
//				} else if ($du->last_ipk['ipk'] >= $minimum_ipk) {
//					$du->validBIPP = true;
//				}
			}
//			$dataU = $dataU->where('validBIPP', true);
		}

		foreach($data as $d){
			$last_interview = Interview::where('recruitment_id', '=', $d->recId)
				->orderBy('created_at', 'desc')
				->first();
			if(!empty($last_interview) && $last_interview->status == 'reschedule'){
				$d->interview_reschedule_request = 'yes';
			}
			else{
				$d->interview_reschedule_request = 'no';
			}

            $student = Student::where('studentid', '=', $d->nim)->first();

			if($d->reStatus == 'rejected'){
				//check approval status
				if($student->requiringDepartmentApproval()){
					$approval_request = Recruitment::where('id', '=', $d->recId)->first()->approval_request()->orderBy('created_at', 'desc')->first();
					if($approval_request != null){
						$d->is_rejected_by_department = true;
						$d->rejected_by_department_on = $approval_request->updated_at;
					}
				}
			}

			if($student->isDurationSixMonth()){
				$d->duration = 6;
			}

			$unreject_request_count = UnrejectRequest::where('recruitment_id', '=', $d->recId)->get()->count();

			$d->unreject_request = $unreject_request_count;

			$last_semester = Studentsemester::where('studentid', '=', $d->nim)->select(DB::raw('MAX(semester) as max_semester'))->first();

			$d->last_ipk = Studentsemester::where('studentid', '=', $d->nim)->where('semester', '=', $last_semester['max_semester'])->first();
		}

		// check if company has old unprocessed application or old processed application (more than 7 days not processed)

		$oldestDate = Carbon::now()->subDays(7);
		$old_unprocessed = Recruitment::join('jobs', 'jobs.id', '=', 'recruitments.jobid')
			->join('companies', 'companies.companyid', '=', 'jobs.companyid')
			->join('students', 'students.studentid', '=', 'recruitments.studentid')
			->whereNull('jobs.deleted_at')
			->whereNull('companies.deleted_at')
			->whereNull('students.deleted_at')
			->whereDate('recruitments.created_at', '<', $oldestDate)
			->where('jobs.companyid', '=', Auth::user()->userid)
			->where('recruitments.status', '=', 'waiting')
			->select('students.name as student_name', 'students.studentid as student_nim')
			->get();

		$old_processed = Recruitment::join('jobs', 'jobs.id', '=', 'recruitments.jobid')
			->join('companies', 'companies.companyid', '=', 'jobs.companyid')
			->join('students', 'students.studentid', '=', 'recruitments.studentid')
			->whereNull('jobs.deleted_at')
			->whereNull('companies.deleted_at')
			->whereNull('students.deleted_at')
			->whereDate('processed_on', '<', $oldestDate)
			->where('jobs.companyid', '=', Auth::user()->userid)
			->where('recruitments.status', '=', 'process')
			->select('students.name as student_name', 'students.studentid as student_nim')
			->get();

		return view('company.home',
			[
				'datas' => $data,
				'dataUnprocessed' => $dataU,
				'user' => $user,
				'counting' => $counting,
				'dataSort' => $dataSort,
				'mailboxCount' => $mailboxCount,
				'state' => $state,
				'interview_reschedule_count' => $count,
				'notifCount' => app('App\Http\Controllers\MailboxController')->getCountNotification(),
				'notif' => app('App\Http\Controllers\MailboxController')->getLatestNotification(),
				'old_unprocessed' => $old_unprocessed,
				'old_processed' => $old_processed
			]);
	}

	// UTILITY FUNCTION
	public function getReportData() {
		$data = null;
		$state = null;

		$company = Company::where('companyid', '=', Auth::user()->userid)
			->first();
		if ($company->state == 'closed'){
			$data = Recruitment::join('jobs', 'jobs.id', '=', 'recruitments.jobid')
				->join('companies', 'companies.companyid', '=', 'jobs.companyid')
				->join('students', 'students.studentid', '=', 'recruitments.studentid')
				->leftJoin(DB::Raw('(select recruitment_id as rec_id, COUNT(*) as unaccept_request_count from unaccept_request where unaccept_request.deleted_at IS NULL and unaccept_request.status = "pending" group by recruitment_id) as unaccept_request_data'), 'rec_id', '=', 'recruitments.id')
				->leftJoin(DB::Raw('(select * from interviews where interviews.deleted_at IS NULL and interviews.status <> "reschedule" and interviews.deleted_at is null) as interviewData'), 'interviewData.recruitment_id', '=', 'recruitments.id')
				->whereNull('jobs.deleted_at')
				->whereNull('companies.deleted_at')
				->whereNull('students.deleted_at')
				->where('jobs.companyid', '=', Auth::user()->userid)
				->whereIn('recruitments.status', ['approved', 'accepted'])
				->select(
					'unaccept_request_data.unaccept_request_count as unaccept_request',
					'recruitments.id as recId',
					'jobs.name as position',
					'recruitments.status as reStatus',
					'companies.name as company',
					'students.name as student',
					'students.school as faculty',
					'recruitments.token as rToken',
					'interviewData.date as inDate',
					'interviewData.time as inTime',
					'interviewData.status as inStatus',
					'students.studentid as nim',
					'students.cv as cv',
					'students.email as email',
					'students.phone as phone',
					DB::Raw('(select jobs.companyid from jobs join recruitments on jobs.id = recruitments.jobid where jobs.deleted_at IS NULL and recruitments.deleted_at IS NULL and status = "approved" and recruitments.studentid = nim limit 1) as stateStudent'),
					DB::Raw('(select recruitments.status from jobs join recruitments on jobs.id = recruitments.jobid where jobs.deleted_at IS NULL and recruitments.deleted_at IS NULL and status = "approved" and recruitments.studentid = nim limit 1) as stateAccepted'),
					'students.sex as gender',
					'recruitments.rejected_by as rejected_by',
					DB::Raw('NULL as expired_at')
					)
				->orderBy('recruitments.approved_on', 'asc')
				->distinct()
				->get();
			$state = 'closed';
		}
		else
		{
			$data0 = Recruitment::join('jobs', 'jobs.id', '=', 'recruitments.jobid')
				->join('companies', 'companies.companyid', '=', 'jobs.companyid')
				->join('students', 'students.studentid', '=', 'recruitments.studentid')
				->leftJoin(DB::Raw('(select recruitment_id as rec_id, COUNT(*) as unaccept_request_count from unaccept_request where unaccept_request.deleted_at IS NULL and unaccept_request.status = "pending" group by recruitment_id) as unaccept_request_data'), 'rec_id', '=', 'recruitments.id')
				->leftJoin(DB::Raw('(select * from interviews where interviews.deleted_at IS NULL and interviews.status <> "reschedule" and interviews.deleted_at is null) as interviewData'), 'interviewData.recruitment_id', '=', 'recruitments.id')
				->whereNull('jobs.deleted_at')
				->whereNull('companies.deleted_at')
				->whereNull('students.deleted_at')
				->where('jobs.companyid', '=', Auth::user()->userid)
				->where('recruitments.status', '=', 'approved')
				->select(
					'unaccept_request_data.unaccept_request_count as unaccept_request',
					'recruitments.id as recId',
					'jobs.name as position',
					'recruitments.status as reStatus',
					'companies.name as company',
					'students.name as student',
					'students.school as faculty',
					'recruitments.token as rToken',
					'interviewData.date as inDate',
					'interviewData.time as inTime',
					'interviewData.status as inStatus',
					'students.studentid as nim',
					'students.cv as cv',
					'students.email as email',
					'students.phone as phone',
					DB::Raw('(select jobs.companyid from jobs join recruitments on jobs.id = recruitments.jobid where jobs.deleted_at IS NULL and recruitments.deleted_at IS NULL and status = "approved" and recruitments.studentid = nim limit 1) as stateStudent'),
					DB::Raw('(select recruitments.status from jobs join recruitments on jobs.id = recruitments.jobid where jobs.deleted_at IS NULL and recruitments.deleted_at IS NULL and status = "approved" and recruitments.studentid = nim limit 1) as stateAccepted'),
					'students.sex as gender',
					'recruitments.rejected_by as rejected_by',
					DB::Raw('NULL as expired_at')
					)
				->distinct();
			$data1 = Recruitment::join('jobs', 'jobs.id', '=', 'recruitments.jobid')
				->join('companies', 'companies.companyid', '=', 'jobs.companyid')
				->join('students', 'students.studentid', '=', 'recruitments.studentid')
				->leftJoin(DB::Raw('(select recruitment_id as rec_id, COUNT(*) as unaccept_request_count from unaccept_request where unaccept_request.deleted_at IS NULL and unaccept_request.status = "pending" group by recruitment_id) as unaccept_request_data'), 'rec_id', '=', 'recruitments.id')
				->leftJoin(DB::Raw('(select * from interviews where interviews.deleted_at IS NULL and interviews.status <> "reschedule" and interviews.deleted_at is null) as interviewData'), 'interviewData.recruitment_id', '=', 'recruitments.id')
				->whereNull('jobs.deleted_at')
				->whereNull('companies.deleted_at')
				->whereNull('students.deleted_at')
				->where('jobs.companyid', '=', Auth::user()->userid)
				->where('recruitments.status', '=', 'accepted')
				->select(
					'unaccept_request_data.unaccept_request_count as unaccept_request',
					'recruitments.id as recId',
					'jobs.name as position',
					'recruitments.status as reStatus',
					'companies.name as company',
					'students.name as student',
					'students.school as faculty',
					'recruitments.token as rToken',
					'interviewData.date as inDate',
					'interviewData.time as inTime',
					'interviewData.status as inStatus',
					'students.studentid as nim',
					'students.cv as cv',
					'students.email as email',
					'students.phone as phone',
					DB::Raw('(select jobs.companyid from jobs join recruitments on jobs.id = recruitments.jobid where jobs.deleted_at IS NULL and recruitments.deleted_at IS NULL and status = "approved" and recruitments.studentid = nim limit 1) as stateStudent'),
					DB::Raw('(select recruitments.status from jobs join recruitments on jobs.id = recruitments.jobid where jobs.deleted_at IS NULL and recruitments.deleted_at and status = "approved" and recruitments.studentid = nim limit 1) as stateAccepted'),
					'students.sex as gender',
					'recruitments.rejected_by as rejected_by',
					DB::Raw('NULL as expired_at')
					)
				->distinct();
			$data2 = Recruitment::join('jobs', 'jobs.id', '=', 'recruitments.jobid')
				->join('companies', 'companies.companyid', '=', 'jobs.companyid')
				->join('students', 'students.studentid', '=', 'recruitments.studentid')
				->leftJoin(DB::Raw('(select recruitment_id as rec_id, COUNT(*) as unaccept_request_count from unaccept_request where unaccept_request.deleted_at IS NULL and unaccept_request.status = "pending" group by recruitment_id) as unaccept_request_data'), 'rec_id', '=', 'recruitments.id')
				->leftJoin(DB::Raw('(select * from interviews where interviews.deleted_at IS NULL and interviews.status <> "reschedule" and interviews.deleted_at is null) as interviewData'), 'interviewData.recruitment_id', '=', 'recruitments.id')
				->whereNull('jobs.deleted_at')
				->whereNull('companies.deleted_at')
				->whereNull('students.deleted_at')
				->where('jobs.companyid', '=', Auth::user()->userid)
				->where('recruitments.status', '=', 'rejected')
				->select(
					'unaccept_request_data.unaccept_request_count as unaccept_request',
					'recruitments.id as recId',
					'jobs.name as position',
					'recruitments.status as reStatus',
					'companies.name as company',
					'students.name as student',
					'students.school as faculty',
					'recruitments.token as rToken',
					'interviewData.date as inDate',
					'interviewData.time as inTime',
					'interviewData.status as inStatus',
					'students.studentid as nim',
					'students.cv as cv',
					'students.email as email',
					'students.phone as phone',
					DB::Raw('(select jobs.companyid from jobs join recruitments on jobs.id = recruitments.jobid where jobs.deleted_at IS NULL and recruitments.deleted_at IS NULL and status = "approved" and recruitments.studentid = nim limit 1) as stateStudent'),
					DB::Raw('(select recruitments.status from jobs join recruitments on jobs.id = recruitments.jobid where jobs.deleted_at IS NULL and recruitments.deleted_at IS NULL and status = "approved" and recruitments.studentid = nim limit 1) as stateAccepted'),
					'students.sex as gender',
					'recruitments.rejected_by as rejected_by',
					DB::Raw('NULL as expired_at')
				)
				->distinct();
			$data = Recruitment::join('jobs', 'jobs.id', '=', 'recruitments.jobid')
				->join('companies', 'companies.companyid', '=', 'jobs.companyid')
				->join('students', 'students.studentid', '=', 'recruitments.studentid')
				->leftJoin(DB::Raw('(select recruitment_id as rec_id, COUNT(*) as unaccept_request_count from unaccept_request where unaccept_request.deleted_at IS NULL and unaccept_request.status = "pending" group by recruitment_id) as unaccept_request_data'), 'rec_id', '=', 'recruitments.id')
				->leftJoin(DB::Raw('(select * from interviews where interviews.deleted_at IS NULL and interviews.status <> "reschedule" and interviews.deleted_at is null) as interviewData'), 'interviewData.recruitment_id', '=', 'recruitments.id')
				->whereNull('jobs.deleted_at')
				->whereNull('companies.deleted_at')
				->whereNull('students.deleted_at')
				->where('jobs.companyid', '=', Auth::user()->userid)
				->where('recruitments.status', '=', 'process')
				->select(
					'unaccept_request_data.unaccept_request_count as unaccept_request',
					'recruitments.id as recId',
					'jobs.name as position',
					'recruitments.status as reStatus',
					'companies.name as company',
					'students.name as student',
					'students.school as faculty',
					'recruitments.token as rToken',
					'interviewData.date as inDate',
					'interviewData.time as inTime',
					'interviewData.status as inStatus',
					'students.studentid as nim',
					'students.cv as cv',
					'students.email as email',
					'students.phone as phone',
					DB::Raw('(select jobs.companyid from jobs join recruitments on jobs.id = recruitments.jobid where jobs.deleted_at IS NULL and recruitments.deleted_at IS NULL and status = "approved" and recruitments.studentid = nim limit 1) as stateStudent'),
					DB::Raw('(select recruitments.status from jobs join recruitments on jobs.id = recruitments.jobid where jobs.deleted_at IS NULL and recruitments.deleted_at IS NULL and status = "approved" and recruitments.studentid = nim limit 1) as stateAccepted'),
					'students.sex as gender',
					'recruitments.rejected_by as rejected_by',
					DB::Raw('DATE_ADD(recruitments.processed_on, INTERVAL 14 DAY) as expired_at')
				)
				->distinct()
				->union($data0)
				->union($data1)
				->union($data2)
				->orderBy(DB::raw('-expired_at'), 'desc')
				->get();
			$state = 'open';
		}
		return ['data' => $data, 'state' => $state];
	}

	// company's report page
	// routes: /company/report
	public function reportCompany()
	{
		$user = Auth::user();
		$reportData = $this->getReportData();
		$data = $reportData['data'];
		$state = $reportData['state'];

		//get count of interview reschedule request by student
		$count = 0;
		$all_recruitments = Recruitment::join('jobs', 'jobs.id', '=', 'recruitments.jobid')
			->whereNull('jobs.deleted_at')
			->where('jobs.companyid', '=', Auth::user()->userid)
			->select('recruitments.id as id')
			->get();
		foreach ($all_recruitments as $rec) {
			$last_interview = Interview::where('recruitment_id', '=', $rec->id)
				->orderBy('created_at', 'desc')
				->first();
			if(!empty($last_interview) && $last_interview->status == 'reschedule'){
				$count++;
			}
		}

		$faculties = Recruitment::join('students', 'students.studentid', '=', 'recruitments.studentid')
			->join('jobs', 'jobs.id', '=', 'recruitments.jobid')
			->whereNotIn('recruitments.status', ['waiting'])
			->where('jobs.companyid', '=', Auth::user()->userid)
			->select('students.school as faculty')
			->distinct()
			->get();
			
		$jobPositions = Recruitment::join('jobs', 'jobs.id', '=', 'recruitments.jobid')
			->whereNotIn('recruitments.status', ['waiting'])
			->where('jobs.companyid', '=', Auth::user()->userid)
			->select('jobs.name as position', DB::raw('count(*) as counting'))
			->groupBy('position')
			->get();

		$mailboxCount = Mailbox::where('type', '=', 'mailbox')
			->where('status', '=', 'unseen')
			->where('recipientid', '=', Auth::user()->userid)
			->count();

		foreach($data as $d){
            $student = Student::where('studentid', '=', $d->nim)->first();

			if($d->reStatus == 'rejected'){
				//check approval status
				if($student->requiringDepartmentApproval()){
					$approval_request = Recruitment::where('id', '=', $d->recId)->first()->approval_request()->orderBy('created_at', 'desc')->first();
					if($approval_request != null){
						$d->is_rejected_by_department = true;
						$d->rejected_by_department_on = $approval_request->updated_at;
					}
				}
			}

			if($student->isDurationSixMonth()){
				$d->duration = 6;
			}

			$unreject_request_count = UnrejectRequest::where('recruitment_id', '=', $d->recId)->get()->count();

			$d->unreject_request = $unreject_request_count;

			$last_semester = Studentsemester::where('studentid', '=', $d->nim)->select(DB::raw('MAX(semester) as max_semester'))->first();

			$d->last_ipk = Studentsemester::where('studentid', '=', $d->nim)->where('semester', '=', $last_semester['max_semester'])->first();
		}

		return view('company.report',
			[
				'datas' => $data,
				'user' => $user,
				'faculties' => $faculties,
				'jobPositions' => $jobPositions,
				'mailboxCount' => $mailboxCount,
				'state' => $state,
				'notifCount' => app('App\Http\Controllers\MailboxController')->getCountNotification(),
				'notif' => app('App\Http\Controllers\MailboxController')->getLatestNotification(),
			]);
	}

	// exporting company report data to excel
	// routes: /exportRecruitmentData
	public function exportData()
	{
		$data = $this->getReportData()['data'];
		foreach($data as $d){
            $student = Student::where('studentid', '=', $d->nim)->first();
			$last_semester = Studentsemester::where('studentid', '=', $d->nim)->select(DB::raw('MAX(semester) as max_semester'))->first();
			$d->last_ipk = Studentsemester::where('studentid', '=', $d->nim)->where('semester', '=', $last_semester['max_semester'])->first();
		}

        Excel::create('Company Report', function ($excel) use ($data)
		{
			$excel->sheet('Report', function($sheet) use($data)
			{
				$sheet->cell('A1', function($cell)
					{
	   					$cell->setValue('Name');
	   				});
				$sheet->cell('B1', function($cell)
					{
	   					$cell->setValue('Gender');
	   				});
				$sheet->cell('C1', function($cell)
					{
	   					$cell->setValue('Position');
	   				});
				$sheet->cell('D1', function($cell)
					{
						$cell->setValue('Phone');
					});
				$sheet->cell('E1', function($cell)
					{
	   					$cell->setValue('E-mail');
	   				});
				$sheet->cell('F1', function($cell)
					{
						$cell->setValue('Last IPK');
					});
				$sheet->cell('G1', function($cell)
					{
						$cell->setValue('Faculty');
					});
				$sheet->cell('H1', function($cell)
					{
						$cell->setValue('Status');
					});

				$counter=2;
				foreach($data as $record)
				{
					$sheet->cell('A'.$counter, function($cell)use($record)
					{
						 $cell->setValue($record->student);
					});
					$sheet->cell('B'.$counter, function($cell)use($record)
					{
						 $cell->setValue(ucfirst($record->gender));
					});
					$sheet->cell('C'.$counter, function($cell)use($record)
					{
						 $cell->setValue($record->position);
					});
					$sheet->cell('D'.$counter, function($cell)use($record)
					{
						 $cell->setValue(($record->phone != NULL) ? $record->phone : 'Not Set');
					});
					$sheet->cell('E'.$counter, function($cell)use($record)
					{
						 $cell->setValue(($record->email != NULL) ? $record->email : 'Not Set');
					});
					$sheet->cell('F'.$counter, function($cell)use($record)
					{
						 $cell->setValue($record->last_ipk['ipk']);
					});
					$sheet->cell('G'.$counter, function($cell)use($record)
					{
						 $cell->setValue($record->faculty);
					});
					$sheet->cell('H'.$counter, function($cell)use($record)
					{
						 $cell->setValue(ucfirst($record->reStatus));
					});
					$counter++;
				}
				$sheet->setAutoFilter();
			});
		})->export('xlsx');
	}

	// Home
	// For company to send unaccept request
	public function unacceptStudentByCompany(Request $request)
	{
		return array('message' => 'You cannot unaccept for now');
		// fitur ini disabled sementara

		$msg = '';

		$rec = Recruitment::where('token', '=', $request->input('rToken'))
			->first();
		$job = $rec->job()->first();
		if($rec->status != 'approved'){
			$msg = 'You cannot unaccept this student';
		}
		else{
			$current_unaccept_request = $rec->unaccept_request()->where('unaccept_request.status', '=', 'pending')->get();
			if($current_unaccept_request->count() == 0){
				$reason = $request->input('reason');
				if(empty($reason)){
					$msg = 'You must input reason';
				}
				else{
					$unaccept_request = new UnacceptRequest;
					$unaccept_request->reason = $reason;
					$unaccept_request->status = 'pending';
					$unaccept_request->recruitment_id = $rec->id;
					$unaccept_request->save();

					$msg = 'Success';

					// create email notification for the department account
					EmailService::createNewUnacceptRequestEmail($unaccept_request);
				}
			}
			else{
				$msg = 'You cannot unaccept this student';
			}
		}

		return array('message' => $msg);
	}

	// Home
	// For company to send unreject request
	public function unrejectStudentByCompany(Request $request)
	{
		$msg = '';

		$rec = Recruitment::where('token', '=', $request->input('rToken'))
			->first();
		$job = $rec->job()->first();

		$stu = $rec->student()->first();
		if($stu->recruitment()->where('status', '=', 'approved')->get()->count() > 0){
			$msg = 'The student has already been approved in another company';
		}
		else if($rec->status != 'rejected'){
			$msg = 'You cannot unreject this student';
		}
		else{
			$current_unreject_request = $rec->unreject_request()->where('unreject_request.status', '=', 'pending')->get();
			if($current_unreject_request->count() == 0){
				$reason = $request->input('reason');
				if(empty($reason)){
					$msg = 'You must input reason';
				}
				else{
					$unreject_request = new UnrejectRequest;
					$unreject_request->reason = $reason;
					$unreject_request->status = 'pending';
					$unreject_request->recruitment_id = $rec->id;
					$unreject_request->save();

					$msg = 'Success';

					// create email notification for the department account
					EmailService::createNewUnrejectRequestEmail($unreject_request);
				}
			}
			else{
				$msg = 'You cannot unreject this student';
			}
		}

		return array('message' => $msg);
	}

	//Untuk mengubah status mahasiswa menjadi diterima
	public function updateAcceptRecruitmentCompany($id)
	{
		$msg = '';

		$rec = Recruitment::where('token', '=', $id)->first();
		$job = $rec->job()->first();
		if($rec->status != 'process'){
			$msg = 'Gagal! Anda sudah memproses student tersebut sebelumnya.';
			return redirect('homec')->with('err', $msg);
		}
		else if($job->quota <= 0){
			$msg = 'Gagal! Quota Job yang dipilih sudah full. Silahkan update job anda di menu Manage > Job';
			return redirect('homec')->with('err', $msg);
		}
		else if($rec->student()->first()->recruitment()->where('status', '=', 'approved')->first() != null){
			$msg = 'Gagal! Student yang ingin diaccept, sudah diaccept oleh company lain';
			return redirect('homec')->with('err', $msg);
		}
		else{
			$job->quota = $job->quota - 1;
			$job->save();

			if($rec->student()->first()->requiringDepartmentApproval()){
				$approval_request = new ApprovalRequest();
				$approval_request->status = 'pending';
				$approval_request->recruitment_id = $rec->id;
				$approval_request->save();

				$rec->accepted_by = Auth::user()->userid;
				$rec->accepted_on = Carbon::now();
				$rec->status = 'accepted';

				// send email to HOP that there is new approval request
				EmailService::createNewApprovalRequestEmail($approval_request);

				// send email to student that the status has been Changed
				EmailService::createNewChangeStatusEmail($rec, "Accepted");
			}
			else{
				$rec->accepted_by = Auth::user()->userid;
				$rec->accepted_on = Carbon::now();
				$rec->approved_by = Auth::user()->userid;
				$rec->approved_on = Carbon::now();
				$rec->status = 'approved';

				// send email to another company that the student has been approved
				EmailService::createNewApprovedOtherEmail($rec->student()->first(), Auth::user()->userid);

				// send email to student that the status has been Changed
				EmailService::createNewChangeStatusEmail($rec, "Approved");
			}

			$rec->save();
			$msg = 'Berhasil';

			$company = Company::where('companyid', '=', Auth::user()->userid)
				->first();
			$mailInsert = new Mailbox;
			$mailInsert->senderid = Auth::user()->userid;
			$mailInsert->recipientid =$rec->studentid;
			$mailInsert->subject = $company->name.' has accepted your application.';
			$mailInsert->date = date("Y-m-d");
			$mailInsert->message = 'You have been accepted as '.$rec->job()->first()->name.'.';
			$mailInsert->status = 'unseen';
			$mailInsert->type = 'notif';
			$mailInsert->token = '';
			$mailInsert->save();
		}

		return redirect('homec')->with('fyi', $msg);
	}

	//Untuk mengubah status mahasiswa menjadi proses
	public function updateProcessRecruitmentCompany($id)
	{
		$msg = '';
		$rec = Recruitment::where('token', '=', $id)->first();
		if($rec->status == 'accepted'){
			$msg = 'Gagal';
		}
		else{
			$rec->status = 'process';
			$rec->processed_on = Carbon::now();
			$rec->processed_by = Auth::user()->userid;
			$rec->save();
			$msg = 'BerhasilProcess';

			// send email to student that the status has been Changed
			EmailService::createNewChangeStatusEmail($rec, "Process");

			$company = Company::where('companyid', '=', Auth::user()->userid)->first();
			$mailInsert = new Mailbox;
			$mailInsert->senderid = Auth::user()->userid;
			$mailInsert->recipientid =$rec->studentid;
			$mailInsert->subject = $company->name.' has processed your application.';
			$mailInsert->date = date("Y-m-d");
			$mailInsert->message = 'You have been processed as '.$rec->job()->first()->name.'.';
			$mailInsert->status = 'unseen';
			$mailInsert->type = 'notif';
			$mailInsert->token = '';
			$mailInsert->save();
		}

		//return redirect('homec')->with('fyi', 'BerhasilProcess');
		return redirect('homec')->with('fyi', $msg);
	}

	//Untuk mengubah status mahasiswa menjadi ditolak
	public function updateRejectRecruitmentCompany($id)
	{
		$msg = '';
		$rec = Recruitment::where('token', '=', $id)->first();
		if($rec->status == 'accepted' || $rec->status == 'approved'){
			$msg = 'Gagal';
		}
		else{
			$rec->status = 'rejected';
			$rec->rejected_on = Carbon::now();
			$rec->rejected_by = Auth::user()->userid;
			$rec->save();
			$msg = 'BerhasilReject';

			// send email to student that the status has been Changed
			EmailService::createNewChangeStatusEmail($rec, "Rejected");

			$company = Company::where('companyid', '=', Auth::user()->userid)->first();
			$mailInsert = new Mailbox;
			$mailInsert->senderid = Auth::user()->userid;
			$mailInsert->recipientid =$rec->studentid;
			$mailInsert->subject = $company->name.' has rejected your application.';
			$mailInsert->date = date("Y-m-d");
			$mailInsert->message = 'You have been rejected as '.$rec->job()->first()->name.'.';
			$mailInsert->status = 'unseen';
			$mailInsert->type = 'notif';
			$mailInsert->token = '';
			$mailInsert->save();
		}

		return redirect('homec')->with('fyi', $msg);
	}

	// Organize > Approval Request page
	// routes: /approval_request
	// return: Organize > Approval Request page
	public function approval_request(){
		$allowed_degree_id = DepartmentController::get_allowed_degree_list();

		$approval_requests = ApprovalRequest::join('recruitments', 'recruitments.id', '=', 'approval_request.recruitment_id')
			->join('students', 'students.studentid', '=', 'recruitments.studentid')
			->join('degrees', 'degrees.id', '=', 'students.degreeid')
			->whereNull('recruitments.deleted_at')
			->whereNull('students.deleted_at')
			->whereNull('degrees.deleted_at')
			->whereIn('students.degreeid', $allowed_degree_id)
			->where('approval_request.status', '=', 'pending')
			->select(
				'students.name as student_name',
				'students.studentid as nim',
				'degrees.name as degree_name'
				)
			->orderBy('student_name', 'asc')
			->distinct()
			->get();

		foreach($approval_requests as $req){
			$req->list_recruitments = ApprovalRequest::join('recruitments', 'recruitments.id', '=', 'approval_request.recruitment_id')
				->join('jobs', 'jobs.id', '=', 'recruitments.jobid')
				->join('companies', 'companies.companyid', '=', 'jobs.companyid')
				->whereNull('recruitments.deleted_at')
				->whereNull('jobs.deleted_at')
				->whereNull('companies.deleted_at')
				->where('studentid', '=', $req->nim)
				->where('approval_request.status', '=', 'pending')
				->select(
					'companies.name as company_name',
					'jobs.name as job_name',
					'jobs.id as job_id',
					'jobs.description as job_description',
					'approval_request.approval_request_id as request_id'
					)
				->get();
		}

		$approval_requests_history = ApprovalRequest::join('recruitments', 'recruitments.id', '=', 'approval_request.recruitment_id')
			->join('jobs', 'jobs.id', '=', 'recruitments.jobid')
			->join('students', 'students.studentid', '=', 'recruitments.studentid')
			->join('degrees', 'degrees.id', '=', 'students.degreeid')
			->join('companies', 'companies.companyid', '=', 'jobs.companyid')
			->whereNull('recruitments.deleted_at')
			->whereNull('jobs.deleted_at')
			->whereNull('students.deleted_at')
			->whereNull('degrees.deleted_at')
			->whereNull('companies.deleted_at')
			->whereIn('students.degreeid', $allowed_degree_id)
			->where('approval_request.status', '<>', 'pending')
			->select(
				'students.name as student_name',
				'students.studentid as nim',
				'jobs.id as job_id',
				'jobs.name as job_name',
				'approval_request.approval_request_id as request_id',
				'approval_request.status as status',
				'degrees.name as degree_name',
				'approval_request.rejected_by as rejected_by',
				'approval_request.rejected_on as rejected_on',
				'approval_request.approved_by as approved_by',
				'approval_request.approved_on as approved_on',
				'companies.name as company_name'
				)
			->orderBy('student_name', 'asc')
			->distinct()
			->get();

		return view('department.approval_request')->with(
			[
				'approval_requests' => $approval_requests,
				'approval_requests_history' => $approval_requests_history
			]);
	}

	// submit update approval request (approve each)
	// routes: /approve_approval_request/
	public function approve_approval_request($id){
		$allowed_degree_id = DepartmentController::get_allowed_degree_list();

		$request = ApprovalRequest::where('approval_request_id', '=', $id)->first();

		if($request == null){
			return redirect('/approval_request')->with('errors','Cannot find request data!');
		}
		else{
			$rec = $request->recruitment()->first();
			$student = $rec->student()->first();

			if(array_search($student->degreeid, $allowed_degree_id) === FALSE){
				return redirect('/approval_request')->with('errors','You cannot approve this student');
			}

			$student_recruitment_approved = Recruitment::where('studentid', '=', $student->studentid)
				->where('status', '=', 'approved')
				->get()
				->count();
			if($student_recruitment_approved > 0){
				return redirect('/approval_request')->with('errors','You cannot approve this student, because the student has already been approved in another company');
			}

			if($request->status != 'pending'){
				return redirect('/approval_request')->with('errors','Request has been approved / rejected before!');
			}
			else{
				$request->approved_by = Auth::user()->userid;
				$request->approved_on = Carbon::now();
				$request->status = 'approved';
				$request->save();

				$rec->approved_by = Auth::user()->userid;
				$rec->approved_on = Carbon::now();
				$rec->status = 'approved';
				$rec->save();

				// send notif
				NotificationController::createApproveApprovalRequestNotif($request, Auth::user()->userid);

				// send email to another company that the student has been approved
				EmailService::createNewApprovedOtherEmail(
					$rec->student()->first(), $rec->job()->first()->company()->first()->companyid
				);

				// send email to student that the status has been Changed
				EmailService::createNewChangeStatusEmail($rec, "Approved");

				// reject another approval_request for the student
				$another_recruitment = $student->recruitment()->where('id', '<>', $rec->id)->get();

				$another_approval_request = [];
				foreach($another_recruitment as $ar){
					array_push(
						$another_approval_request,
						$ar->approval_request()->where('status', '=', 'pending')->first()
					);
				}

				foreach($another_approval_request as $aar){
					if($aar != null){
						$aar_rec = $aar->recruitment()->first();

						$aar_job = $aar_rec->job()->first();
						$aar_job->quota += 1;
						$aar_job->save();

						$aar_rec->rejected_by = Auth::user()->userid;
						$aar_rec->rejected_on = Carbon::now();
						$aar_rec->status = 'rejected';
						$aar_rec->save();

						$aar->rejected_by = Auth::user()->userid;
						$aar->rejected_on = Carbon::now();
						$aar->status = 'rejected';
						$aar->save();

						// send notif
						NotificationController::createRejectApprovalRequestNotif($aar, Auth::user()->userid);

						// send email to student that the status has been Changed
						EmailService::createNewChangeStatusEmail($aar_rec, "Rejected");
					}
				}

				return redirect('/approval_request')->with('fyi','Success Approve Request!');
			}
		}
	}

	// submit update approval request history (approve each)
	// routes: /approve_approval_request_history/
	public function approve_approval_request_history($id){
		$allowed_degree_id = DepartmentController::get_allowed_degree_list();

		$request = ApprovalRequest::where('approval_request_id', '=', $id)->first();

		if($request == null){
			return redirect('/approval_request')->with('errors','Cannot find request data!');
		}
		else{
			$rec = $request->recruitment()->first();
			$student = $rec->student()->first();

			if(array_search($student->degreeid, $allowed_degree_id) === FALSE){
				return redirect('/approval_request')->with('errors','You cannot approve this student');
			}

			$student_recruitment_approved = Recruitment::where('studentid', '=', $student->studentid)
				->where('status', '=', 'approved')
				->get()
				->count();
			if($student_recruitment_approved > 0){
				return redirect('/approval_request')->with('errors','You cannot approve this student, because the student has already been approved in another company');
			}

			if($request->status == 'pending'){
				return redirect('/approval_request')->with('errors','Request has not been approved / rejected before!');
			}
			else{
				if($request->status == 'rejected'){
					$rec->rejected_by = null;
					$rec->rejected_on = null;
					$rec->approved_by = Auth::user()->userid;
					$rec->approved_on = Carbon::now();
					$rec->status = 'approved';
					$rec->save();

					$request->rejected_by = null;
					$request->rejected_on = null;
					$request->approved_by = Auth::user()->userid;
					$request->approved_on = Carbon::now();
					$request->status = 'approved';
					$request->save();

					// send notif
					NotificationController::createApproveApprovalRequestNotif($request, Auth::user()->userid);

					// send email to another company that the student has been approved
					EmailService::createNewApprovedOtherEmail(
						$rec->student()->first(), $rec->job()->first()->company()->first()->companyid
					);

					// send email to student that the status has been Changed
					EmailService::createNewChangeStatusEmail($rec, "Approved");
				}
				return redirect('/approval_request')->with('fyi','Success Approve Request!');
			}
		}
	}

	// submit update approval request (reject each)
	// routes: /reject_approval_request/
	public function reject_approval_request($id)
	{
		$allowed_degree_id = DepartmentController::get_allowed_degree_list();

		$request = ApprovalRequest::where('approval_request_id', '=', $id)
			->first();

		if($request == null){
			return redirect('/approval_request')->with('errors','Cannot find request data!');
		}
		else{
			$rec = $request->recruitment()->first();
			$student = $rec->student()->first();

			if(array_search($student->degreeid, $allowed_degree_id) === FALSE){
				return redirect('/approval_request')->with('errors','You cannot reject this student');
			}

			if($request->status != 'pending'){
				return redirect('/approval_request')->with('errors','Request has been approved / rejected before!');
			}
			else{
				$job = $rec->job()->first();
				$job->quota += 1;
				$job->save();

				$rec->rejected_by = Auth::user()->userid;
				$rec->rejected_on = Carbon::now();
				$rec->status = 'rejected';
				$rec->save();

				$request->rejected_by = Auth::user()->userid;
				$request->rejected_on = Carbon::now();
				$request->status = 'rejected';
				$request->save();

				// send notif
				NotificationController::createRejectApprovalRequestNotif($request, Auth::user()->userid);

				// send email to student that the status has been Changed
				EmailService::createNewChangeStatusEmail($rec, "Rejected");

				return redirect('/approval_request')->with('fyi','Success Reject Request!');
			}
		}
	}

	// submit update approval request history (reject each)
	// routes: /reject_approval_request_history/
	public function reject_approval_request_history($id)
	{
		$allowed_degree_id = DepartmentController::get_allowed_degree_list();

		$request = ApprovalRequest::where('approval_request_id', '=', $id)
			->first();

		if($request == null){
			return redirect('/approval_request')->with('errors','Cannot find request data!');
		}
		else{
			$rec = $request->recruitment()->first();
			$student = $rec->student()->first();

			if(array_search($student->degreeid, $allowed_degree_id) === FALSE){
				return redirect('/approval_request')->with('errors','You cannot reject this student');
			}

			if($request->status == 'pending'){
				return redirect('/approval_request')->with('errors','Request has not been approved / rejected before!');
			}
			else{
				if($request->status == 'approved'){
					$job = $rec->job()->first();
					$job->quota += 1;
					$job->save();

					$rec->approved_by = null;
					$rec->approved_on = null;
					$rec->rejected_by = Auth::user()->userid;
					$rec->rejected_on = Carbon::now();
					$rec->status = 'rejected';
    				$rec->save();

					$request->approved_by = null;
					$request->approved_on = null;
					$request->rejected_by = Auth::user()->userid;
					$request->rejected_on = Carbon::now();
					$request->status = 'rejected';
					$request->save();

					// send notif
					NotificationController::createRejectApprovalRequestNotif($request, Auth::user()->userid);

					// send email to student that the status has been Changed
					EmailService::createNewChangeStatusEmail($rec, "Rejected");
				}

				return redirect('/approval_request')->with('fyi','Success Reject Request!');
			}
		}
	}

	// submit update approval request history (delete each)
	// routes: /delete_approval_request_history/
	public function delete_approval_request_history($id)
	{
		$allowed_degree_id = DepartmentController::get_allowed_degree_list();

		$request = ApprovalRequest::where('approval_request_id', '=', $id)
			->first();

		if($request == null){
			return redirect('/approval_request')->with('errors','Cannot find request data!');
		}
		else{
			$rec = $request->recruitment()->first();
			$student = $rec->student()->first();

			if(array_search($student->degreeid, $allowed_degree_id) === FALSE){
				return redirect('/approval_request')->with('errors', "You cannot delete this student's approval history");
			}

			if($student->requiringDepartmentApproval()){
				if($rec->status == 'accepted' || $rec->status == 'approved'){
					return redirect('/approval_request')->with('errors','Cannot delete request data, because the recruitment data has "accepted" or "approved" status. Please edit the recruitment status to be "process" or "waiting" first!');
				}
				else if($rec->status == 'rejected'){
					return redirect('/approval_request')->with('errors','Cannot delete request data, because the recruitment data has "rejected" status. Please edit the recruitment status to be "process" or "waiting" first!');
				}
				else{
					$request->delete();
					return redirect('/approval_request')->with('fyi','Success Delete Request!');
				}
			}
		}
	}

	// submit update approval request history (pending each)
	// routes: /pending_approval_request_history/
	public function pending_approval_request_history($id)
	{
		$allowed_degree_id = DepartmentController::get_allowed_degree_list();

		$request = ApprovalRequest::where('approval_request_id', '=', $id)->first();

		if($request == null){
			return redirect('/approval_request')->with('errors','Cannot find request data!');
		}
		else{
			$rec = $request->recruitment()->first();
			$student = $rec->student()->first();

			if(array_search($student->degreeid, $allowed_degree_id) === FALSE){
				return redirect('/approval_request')->with('errors', "You cannot update this student's approval history to become pending");
			}

			if($request->status == 'rejected'){
				$job = $rec->job()->first();
				$job->quota -= 1;
				$job->save();
			}

			$request->approved_by = null;
			$request->approved_on = null;
			$request->rejected_by = null;
			$request->rejected_on = null;
			$request->status = 'pending';
			$request->save();

			$rec->approved_by = null;
			$rec->approved_on = null;
			$rec->rejected_by = null;
			$rec->rejected_on = null;
			$rec->status = 'accepted';
			$rec->save();

			// send notif
			NotificationController::createPendingApprovalRequestNotif($request, Auth::user()->userid);

			// send email to student that the status has been Changed
			EmailService::createNewChangeStatusEmail($rec, "Accepted");

			return redirect('/approval_request')->with('fyi','Success Update Request Status!');
		}
	}

	public function userdata() {
		$student = Auth::user()->student()->get();
		dd($student);
	}

	// submit approval request update (approve all / reject all)
	// routes: /approval_request_submit
	// this handler is deprecated (not used anymore)
	// TODO: delete this handler
	public function approval_request_submit(Request $request){
		$allowed_degree_id = DepartmentController::get_allowed_degree_list();

		$inputs = $request->all();

		$arrayRequest = [];

		foreach ($inputs as $key => $value) {
			if(substr($key, 0, 10) === "checkbox1_"){
				$req_id = substr($key, 10);
				$req = ApprovalRequest::where('approval_request_id', '=', $req_id)
					->first();
				if(empty($req)){
					$errors = 'Invalid inputs !';
				}
				else{
					array_push($arrayRequest, $req);
				}
			}
		}
		foreach ($arrayRequest as $req) {
			if($req->status != 'pending'){
				$errors = 'Some requests have been approved / rejected before !';
				break;
			}
			$recruitment = $req->recruitment()->first();
			$student = $recruitment->student()->first();
			if(array_search($student->degreeid, $allowed_degree_id) === FALSE){
				$errors = "You cannot approve / reject the selected user's approval data";
				break;
			}
		}
		if(!empty($errors)){
			return redirect('/approval_request')->with('errors', $errors);
		}
		else{
			if($request->input('action_type') == 'approve'){
				foreach ($arrayRequest as $key => $req) {
					$rec = $req->recruitment()->first();
					$student = $rec->student()->first();

					$student_recruitment_approved = Recruitment::where('studentid', '=', $student->studentid)
						->where('status', '=', 'approved')
						->get()
						->count();
					if($student_recruitment_approved > 0){
						return redirect('/approval_request')->with('errors','You cannot approve this student, because the student has already been approved in another company');
					}

					$req->approved_by = Auth::user()->userid;
					$req->approved_on = Carbon::now();
					$req->status = 'approved';
					$req->save();

					$rec->approved_by = Auth::user()->userid;
					$rec->approved_on = Carbon::now();
					$rec->status = 'approved';
					$rec->save();

					// send notif
					NotificationController::createApproveApprovalRequestNotif($req, Auth::user()->userid);

					// send email to another company that the student has been approved
					EmailService::createNewApprovedOtherEmail(
						$rec->student()->first(), $rec->job()->first()->company()->first()->companyid
					);

					// send email to student that the status has been Changed
					EmailService::createNewChangeStatusEmail($rec, "Approved");
				}
			}
			else if($request->input('action_type') == 'reject'){
				foreach ($arrayRequest as $key => $req) {
					$rec = $req->recruitment()->first();
    				$rec->status = 'rejected';
					$rec->rejected_by = Auth::user()->userid;
					$rec->rejected_on = Carbon::now();
    				$rec->save();

					$job = $rec->job()->first();
					$job->quota += 1;
					$job->save();

					$req->status = 'rejected';
					$req->rejected_by = Auth::user()->userid;
					$req->rejected_on = Carbon::now();
					$req->save();

					// send notif
					NotificationController::createRejectApprovalRequestNotif($req, Auth::user()->userid);

					// send email to student that the status has been Changed
					EmailService::createNewChangeStatusEmail($rec, "Rejected");
				}
			}
			else{
				return redirect('/approval_request')->with('errors', 'Invalid inputs');
			}
		  	return redirect('/approval_request')->with('fyi','Success!');
		}
	}
}
?>
