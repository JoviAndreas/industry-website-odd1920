<?php

namespace App\Http\Controllers;

use App\AutomatedService;
use App\Company;
use App\Degree;
use App\Department;
use App\Faculty;
use App\Job;
use App\JobPosition;
use App\JobPositionDegrees;
use App\JobsDegrees;
use App\Mailbox;
use App\Recruitment;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class JobController extends BaseController
{

    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    // Manage > Deleted Job page
    // routes: /manageDeletedJob
    public function manage_deleted_job()
    {
        $allowed_degree_id = DepartmentController::get_allowed_degree_list();

        $datas = Job::join('companies', 'jobs.companyid', '=', 'companies.companyid')
            ->join('jobsdegrees', 'jobsdegrees.jobid', '=', 'jobs.id')
            ->whereNull('companies.deleted_at')
            ->whereIn('jobsdegrees.degreeid', $allowed_degree_id)
            ->select(
                'jobs.name as name',
                'jobs.id as id',
                'companies.name as company_name',
                'jobs.deleted_at as deleted_at',
                'jobs.job_state as job_status'
            )
            ->distinct()
            ->onlyTrashed()
            ->get();

        return view('department.managedeletedjob',
            [
                'deleted_jobs' => $datas,
            ]
        );
    }

    // Manage > Deleted Job
    // routes: /undelete_job/
    public function undelete_job($job_id)
    {
        $job = Job::where('id', '=', $job_id)->onlyTrashed()->first();

        if ($job == null) {
            return redirect('/manageDeletedJob')->with([
                'errors' => 'Job not found',
            ]);
        }

        $rec = Recruitment::where('jobid', '=', $job->id)
            ->where('unapplied_by', '=', 'deleted_job')
            ->onlyTrashed()
            ->get();

        foreach ($rec as $r) {
            $r->restore();

            // delete approval_request dan unaccept_request
            $r->approval_request()->onlyTrashed()->restore();
            $r->unaccept_request()->onlyTrashed()->restore();
            $r->unreject_request()->onlyTrashed()->restore();

            // delete interview
            $r->interview()->onlyTrashed()->restore();

            $r->unapplied_by = null;
            $r->unapplied_on = null;
            $r->save();
        }

        $job->jobsdegrees()->onlyTrashed()->restore();
        $job->restore();

        return redirect('/manageDeletedJob')->with(['fyi' => 'success undelete']);
    }

    // Manage > Job page (not filtered based on program)
    // routes: /manageJob
    public function indexDepartment()
    {
        $allowed_degree_id = DepartmentController::get_allowed_degree_list();

        $datas = Job::join('companies', 'jobs.companyid', '=', 'companies.companyid')
            ->join('jobsdegrees', 'jobsdegrees.jobid', '=', 'jobs.id')
            ->whereNull('companies.deleted_at')
            ->whereNull('jobsdegrees.deleted_at')
            ->whereIn('jobsdegrees.degreeid', $allowed_degree_id)
//            ->where()
            ->select(
                'jobs.companyid',
                'jobs.name as position',
                'jobs.supervisor_name as supervisor_name',
                'jobs.supervisor_contact as supervisor_contact',
                'jobs.quota as quota',
                'jobs.description as description',
                'deadline',
                'startdate',
                'enddate',
                'duration',
                'location',
                'jobs.linktest as linktest',
                'jobs.token as token',
                'companies.name as companyname',
                'jobs.id as id',
                'jobs.job_state as state',
                'jobs.allowance',
                'jobs.is_using_bipp_min_ipk',
                'jobs.approved_by_user_id'
            )
            ->orderBy('jobs.job_state', 'desc')
            ->distinct()
            ->get();

        foreach ($datas as $key => $value) {
            // get programs data from each job
            $degrees = JobsDegrees::join('degrees', 'degrees.id', '=', 'jobsdegrees.degreeid')
                ->whereNull('degrees.deleted_at')
                ->where('jobid', '=', $value->id)
                ->select('degrees.name as name', 'degrees.id as id')
                ->get();
            $datas[$key]->{'degrees'} = $degrees;

            // get all quota available
            // current quota + approved
            $approved_count = Recruitment::where('jobid', '=', $value->id)
                ->whereIn('status', ['approved', 'accepted'])
                ->get()
                ->count();

            $datas[$key]->{'full_quota'} = $approved_count + $value->quota;
         }

        $listcompany = Company::all();

        return view('department.managejob',
            [
                'datas' => $datas,
                'companys' => $listcompany,
                'faculties' => Faculty::all(),
                'jobpositions' => JobPosition::all(),
                // 'degrees_six_month' => Degree::getDurationSixMonth(),
                // 'degrees_twelve_month' => Degree::getDurationTwelveMonth()
            ]
        );
    }

    // Manage > Job
    // open update job page for department
    // routes: /departmentupdatejob/
    public function departmentupdatejob($token = null)
    {
        $job = Job::where('token', '=', $token)->first();

        $jobsdegrees = JobsDegrees::join('degrees', 'degrees.id', '=', 'jobsdegrees.degreeid')
            ->whereNull('degrees.deleted_at')
            ->where('jobsdegrees.jobid', '=', $job->id)
            ->select('degrees.name as name', 'degrees.id as id')
            ->get();

        // $degrees_six_month = Degree::getDurationSixMonth();
        // $degrees_twelve_month = Degree::getDurationTwelveMonth();

        return view('department.updatejob',
            [
                'token' => $token,
                'data' => $job,
                'faculties' => Faculty::all(),
                'jobsdegrees' => $jobsdegrees,
                'jobpositions' => JobPosition::all(),
                // 'degrees_six_month' => $degrees_six_month,
                // 'degrees_twelve_month' => $degrees_twelve_month
            ]
        );
    }

    // Manage > Job page (filtered based on program)
    // routes: /managejobfiltered
    public function indexDepartmentFiltered(Request $request)
    {
        if (empty($request->input('filterDegrees'))) {
            return redirect('manageJob');
        }

        if (session('head_of_program') != 'all') {
            return redirect('manageJob');
        }

        $degree_name = $request->input('filterDegrees');
        $degree = Degree::where('name', '=', $degree_name)->first();
        if (empty($degree)) {
            return redirect('manageJob');
        }

        $datas = Job::join('companies', 'jobs.companyid', '=', 'companies.companyid')
            ->whereIn('jobs.id',
                JobsDegrees::where('degreeid', '=', $degree->id)
                    ->select('jobid')
                    ->get()
                    ->toArray()
            )
            ->whereNull('companies.deleted_at')
        //->select('jobs.name as position', 'jobs.quota as quota', 'jobs.description as description', 'deadline', 'startdate', 'enddate', 'duration', 'location', 'jobs.linktest as linktest', 'jobs.token as token','companies.name as companyname', 'learningobj', 'softskill', 'jobs.id as id')
            ->select(
                'jobs.name as position',
                'jobs.supervisor_name as supervisor_name',
                'jobs.supervisor_contact as supervisor_contact',
                'jobs.quota as quota',
                'jobs.description as description',
                'deadline',
                'startdate',
                'enddate',
                'duration',
                'location',
                'jobs.linktest as linktest',
                'jobs.token as token',
                'companies.name as companyname',
                'jobs.id as id',
                'jobs.job_state as state',
                'jobs.allowance',
                'jobs.is_using_bipp_min_ipk'
            )
            ->orderBy('jobs.job_state', 'desc')
            ->get();

        foreach ($datas as $key => $value) {
            // get programs data from each job
            $degrees = JobsDegrees::join('degrees', 'degrees.id', '=', 'jobsdegrees.degreeid')
                ->whereNull('degrees.deleted_at')
                ->where('jobid', '=', $value->id)
                ->select('degrees.name as name', 'degrees.id as id')
                ->get();
            $datas[$key]->{'degrees'} = $degrees;

            // get all quota available
            // current quota + approved
            $approved_count = Recruitment::where('jobid', '=', $value->id)
                ->where('status', '=', 'approved')
                ->get()
                ->count();

            $datas[$key]->{'full_quota'} = $approved_count + $value->quota;
        }

        $listcompany = Company::all();

        return view('department.managejob',
            [
                'datas' => $datas,
                'companys' => $listcompany,
                'faculties' => Faculty::all(),
                'selected_degree' => $degree_name,
                'jobpositions' => JobPosition::all(),
                // 'degrees_six_month' => Degree::getDurationSixMonth(),
                // 'degrees_twelve_month' => Degree::getDurationTwelveMonth()
            ]
        );
    }

    // Manage > Job
    // submit new job for department
    // routes: /insertJobDesc
    public function insertDepartment(Request $request)
    {
        if ($request->input('cmbPosition') == '' || $request->input('txtJobDescription') == '' || $request->input('txtQuantity') == '' || $request->input('txtLocation') == '' || $request->input('txtDeadline') == '' || $request->input('txtDuration') == '' || $request->input('txtStartDate') == '' || $request->input('txtEndDate') == '') {
            return redirect('manageJob')->with('fyi', 'All field must be filled.')->withInput();
        } else {
            if ($request->input('txtAllowance') < 1) {
                return redirect()->back()->with(['fyi' => 'Allowance must be more than 0'])->withInput();
            }
            $date_regex = '/(0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])[- \/.](19|20)\d\d/';
            if (!preg_match($date_regex, $request->input('txtDeadline'))) {
                return redirect('manageJob')->with('fyi', 'Wrong input format Deadline.')->withInput();
            }
            // else if ($request->input('txtDuration') != 6 && $request->input('txtDuration') != 12) {
            //       return redirect('manageJob')->with('fyi', 'Duration must be 6 or 12 months')->withInput();
            // }
            //else if (preg_match('/[^A-z ]/', $request->input('cmbPosition'))) {
            //    return redirect('manageJob')->with('fyi', 'Position must be alphabet.')->withInput();}
            else if (!preg_replace('/[^0-9]/', '', $request->input('txtQuantity'))) {
                return redirect('manageJob')->with('fyi', 'Seat must be numeric')->withInput();
            } else {
                // $tc = "";
                // if ($request->TC1)$tc.="1;";
                // if ($request->TC2)$tc.="2;";
                // if ($request->TC3)$tc.="3;";
                // if ($request->TC4)$tc.="4;";
                // if ($request->TC5)$tc.="5;";
                // if ($request->TC6)$tc.="6;";
                // if ($request->TC7)$tc.="7;";
                // if ($request->TC8)$tc.="8;";
                // if ($request->TC9)$tc.="9;";
                // if ($request->TC10)$tc.="10;";
                // if ($request->TC11)$tc.="11;";
                // if ($request->TC12)$tc.="12;";
                //
                // $ss = "";
                // if ($request->SS1)$ss.="1;";
                // if ($request->SS2)$ss.="2;";
                // if ($request->SS3)$ss.="3;";
                // if ($request->SS4)$ss.="4;";
                // if ($request->SS5)$ss.="5;";
                // if ($request->SS6)$ss.="6;";

                $link_test = null;
                if (!empty($request->input('checkboxLinkTest'))) {
                    if (empty($request->input('txtiLinkTest'))) {
                        return redirect('manageJob')->with('fyi', 'All field must be filled.');
                    } else {
                        //website: always have http:// or https:// in front
                        $web = null;
                        if (strpos($request->input('txtiLinkTest'), 'http://') === false && strpos($request->input('txtiLinkTest'), 'https://') === false) {
                            $web = 'http://' . $request->input('txtiLinkTest');
                        } else {
                            $web = $request->input('txtiLinkTest');
                        }
                        $link_test = $web;
                    }
                }

                $degrees = [];
                foreach ($request->all() as $key => $value) {
                    if (substr($key, 0, 8) == 'degreei_') {
                        $degree_id = substr($key, 8);
                        $degree = Degree::where('id', '=', $degree_id)->first();
                        array_push($degrees, $degree);
                    }
                }
                if (empty($degrees)) {
                    return redirect('manageJob')->with('fyi', 'Please select a Program!')->withInput();
                }

                //$duration = $request->txtDuration;
                // $degree_filter = null;
                // if($duration == 6){
                //     $degree_filter = Degree::getDurationSixMonth();
                // }
                // else if($duration == 12){
                //     $degree_filter = Degree::getDurationTwelveMonth();
                // }
                // $degree_filter_as_array = explode(';', $degree_filter);

                // foreach ($degrees as $deg) {
                //     $found = FALSE;
                //     foreach ($degree_filter_as_array as $d) {
                //         if($deg->id == $d){
                //             $found = TRUE;
                //             break;
                //         }
                //     }
                //     if($found == FALSE){
                //         return redirect('/manageJob')->with('fyi', 'Invalid program input, please reselect the duration, to choose program.')->withInput();
                //     }
                // }

                $job = new Job;
                $job->companyid = $request->input('txtcompanyid');
                $job->supervisor_name = $request->input('txtSupervisor');
                $job->supervisor_contact = $request->input('txtSupervisorContact');
                $job->name = $request->input('cmbPosition');
                $job->description = $request->input('txtJobDescription');
                $job->quota = $request->input('txtQuantity');
                $job->location = $request->input('txtLocation');
                $job->deadline = date("Y-m-d", strtotime($request->input('txtDeadline')));
                $job->duration = $request->input('txtDuration');
                $job->linktest = $link_test;
                $job->job_state = 'open';
                $job->token = str_replace('.', 'H', str_replace("$", "S", str_replace("/", "X", Hash::make(Auth::user()->userid . $request->input('txtQuantity') . $request->input('txtLocation')))));
                $job->startdate = date("Y-m-d", strtotime($request->input('txtStartDate')));
                $job->enddate = date("Y-m-d", strtotime($request->input('txtEndDate')));
                $job->allowance = $request->input('txtAllowance');
                $job->is_using_bipp_min_ipk = ($request->rdHasMinIPK == 'yes') ? '1' : '0';
                $job->save();

                foreach ($degrees as $d) {
                    $newjobsdegrees = new JobsDegrees;
                    $newjobsdegrees->jobid = $job->id;
                    $newjobsdegrees->degreeid = $d->id;
                    $newjobsdegrees->save();
                }

                return redirect('manageJob')->with('fyi', 'Berhasil');
            }
        }
    }

    public function generateToken(Request $request, $jobid) {
        $job = Job::where('id', $jobid)->first();
        $job->token = str_replace('.', 'H', str_replace("$", "S", str_replace("/", "X", Hash::make(Auth::user()->userid . $job->quota . $job->location))));
        $job->save();
        echo 'token generated';
    }

    // Manage > Job
    // submit update job for department account
    // routes: /updateJobDesc
    public function updateDepartment(Request $request)
    {
        $job = Job::where('token', '=', $request->input('txtToken'))->first();

        if ($job == null) {
            return redirect('manageJob')->with('fyi', 'Invalid input.');
        }

        if ($request->input('cmbPositionU') == '' || $request->input('txtJobDescription') == '' || $request->input('txtQuantity') == '' || $request->input('txtLocation') == '' || $request->input('txtDeadline') == '' || $request->input('txtDuration') == '' || $request->input('txtStartDate') == '' || $request->input('txtEndDate') == '') {
            return redirect('/departmentupdatejob/' . $job->token)->with('fyi', 'All field must be filled.')->withInput();
        } else {
            if ($request->input('txtAllowance') < 1) {
                return redirect()->back()->with(['fyi' => 'Allowance must be more than 0'])->withInput();
            }
            $date_regex = '/(0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])[- \/.](19|20)\d\d/';

            if (!preg_match($date_regex, $request->input('txtDeadline'))) {
                return redirect('/departmentupdatejob/' . $job->token)->with('fyi', 'Wrong input format Deadline.')->withInput();
            }
            // else if ($request->input('txtDuration') != 6 && $request->input('txtDuration') != 12) {
            //       return redirect('/departmentupdatejob/'.$job->token)->with('fyi', 'Duration must be 6 or 12 months')->withInput();
            // }
            //else if (preg_match('/[^A-z ]/', $request->input('cmbPosition'))) {
            //    return redirect('/departmentupdatejob/' . $job->token)->with('fyi', 'Position must be alphabet.')->withInput();}
            else if (!preg_replace('/[^0-9]/', '', $request->input('txtQuantity'))) {
                return redirect('/departmentupdatejob/' . $job->token)->with('fyi', 'Seat must be numeric')->withInput();
            } else {
                // $tc = "";
                // if ($request->TC1)$tc.="1;";
                // if ($request->TC2)$tc.="2;";
                // if ($request->TC3)$tc.="3;";
                // if ($request->TC4)$tc.="4;";
                // if ($request->TC5)$tc.="5;";
                // if ($request->TC6)$tc.="6;";
                // if ($request->TC7)$tc.="7;";
                // if ($request->TC8)$tc.="8;";
                // if ($request->TC9)$tc.="9;";
                // if ($request->TC10)$tc.="10;";
                // if ($request->TC11)$tc.="11;";
                // if ($request->TC12)$tc.="12;";
                //
                // $ss = "";
                // if ($request->SS1)$ss.="1;";
                // if ($request->SS2)$ss.="2;";
                // if ($request->SS3)$ss.="3;";
                // if ($request->SS4)$ss.="4;";
                // if ($request->SS5)$ss.="5;";
                // if ($request->SS6)$ss.="6;";

                $link_test = null;
                if (!empty($request->input('checkboxLinkTestU'))) {
                    if (empty($request->input('txtuLinkTest'))) {
                        return redirect('/departmentupdatejob/' . $job->token)->with('fyi', 'All field must be filled.');
                    } else {
                        //website: always have http:// or https:// in front
                        $web = null;
                        if (strpos($request->input('txtuLinkTest'), 'http://') === false && strpos($request->input('txtuLinkTest'), 'https://') === false) {
                            $web = 'http://' . $request->input('txtuLinkTest');
                        } else {
                            $web = $request->input('txtuLinkTest');
                        }
                        $link_test = $web;
                    }
                }

                $degrees = [];
                foreach ($request->all() as $key => $value) {
                    if (substr($key, 0, strlen('degreeu_' . $job->id . '_')) == 'degreeu_' . $job->id . '_') {
                        if ($value == 'off') {
                            continue;
                        }

                        $degree_id = substr($key, strlen('degreeu_' . $job->id . '_'));
                        $degree = Degree::where('id', '=', $degree_id)->first();
                        array_push($degrees, $degree);
                    }
                }
                if (empty($degrees)) {
                    return redirect('/departmentupdatejob/' . $job->token)->with('fyi', 'Please select a program!')->withInput();
                }

                //$duration = $request->txtDuration;
                // $degree_filter = null;
                // if($duration == 6){
                //     $degree_filter = Degree::getDurationSixMonth();
                // }
                // else if($duration == 12){
                //     $degree_filter = Degree::getDurationTwelveMonth();
                // }
                // $degree_filter_as_array = explode(';', $degree_filter);

                // foreach ($degrees as $deg) {
                //     $found = FALSE;
                //     foreach ($degree_filter_as_array as $d) {
                //         if($deg->id == $d){
                //             $found = TRUE;
                //             break;
                //         }
                //     }
                //     if($found == FALSE){
                //         return redirect('/departmentupdatejob/'.$job->token)->with('fyi', 'Invalid program input, please reselect the duration, to choose program.')->withInput();
                //     }
                // }

                JobsDegrees::where('jobid', '=', $job->id)->forceDelete();

                foreach ($degrees as $d) {
                    $newjobsdegrees = new JobsDegrees;
                    $newjobsdegrees->jobid = $job->id;
                    $newjobsdegrees->degreeid = $d->id;
                    $newjobsdegrees->save();
                }
                Job::where('token', '=', $request->input('txtToken'))->update(
                    [
                        'supervisor_name' => $request->input('txtSupervisor'),
                        'supervisor_contact' => $request->input('txtSupervisorContact'),
                        'name' => $request->input('cmbPositionU'),
                        'description' => $request->input('txtJobDescription'),
                        'quota' => $request->input('txtQuantity'),
                        'location' => $request->input('txtLocation'),
                        'deadline' => date("Y-m-d", strtotime($request->input('txtDeadline'))),
                        'startdate' => date("Y-m-d", strtotime($request->input('txtStartDate'))),
                        'enddate' => date("Y-m-d", strtotime($request->input('txtEndDate'))),
                        'duration' => $request->input('txtDuration'),
                        'linktest' => $link_test,
                        'allowance' => $request->input('txtAllowance'),
                        'is_using_bipp_min_ipk' => ($request->rdHasMinIPK == 'yes') ? '1' : '0',
                    ]);
                return redirect('/departmentupdatejob/' . $job->token)->with('fyi', 'Berhasil');
            }
        }
    }

    // Manage > Job page
    // role: department
    // routes: /job
    public function indexCompany()
    {
        $getState = Company::where('companyid', '=', Auth::user()->userid)
            ->first();
        $user = Auth::user();

        $datas = Job::join('companies', 'jobs.companyid', '=', 'companies.companyid')
            ->where('jobs.companyid', '=', $user->userid)
            ->whereNull('companies.deleted_at')
            ->select(
                'jobs.name as position',
                'jobs.supervisor_name as supervisor_name',
                'jobs.supervisor_contact as supervisor_contact',
                'jobs.quota as quota',
                'jobs.description as description',
                'deadline',
                'startdate',
                'enddate',
                'duration',
                'location',
                'jobs.linktest as linktest',
                'jobs.token as token',
                'jobs.id as id',
                'jobs.job_state as state',
                'jobs.allowance',
                'jobs.is_using_bipp_min_ipk'
            )
            ->get();

        foreach ($datas as $key => $value) {
            // get all programs data for each job
            $degrees = JobsDegrees::join('degrees', 'degrees.id', '=', 'jobsdegrees.degreeid')
                ->whereNull('degrees.deleted_at')
                ->where('jobid', '=', $value->id)
                ->select('degrees.name as name', 'degrees.id as id')
                ->get();
            $datas[$key]->{'degrees'} = $degrees;

            // get all quota available
            // current quota + approved
            $approved_count = Recruitment::where('jobid', '=', $value->id)
                ->whereIn('status', ['approved', 'accepted'])
                ->get()
                ->count();

            $datas[$key]->{'full_quota'} = $approved_count + $value->quota;
        }

        $mailboxCount = Mailbox::where('type', '=', 'mailbox')
            ->where('status', '=', 'unseen')
            ->where('recipientid', '=', Auth::user()->userid)
            ->count();

        // $degrees_six_month = Degree::getDurationSixMonth();
        // $degrees_twelve_month = Degree::getDurationTwelveMonth();

        $jobPositionDegreesMapping = JobPositionDegrees::select('degree_id', 'job_position_id')->get()->groupBy('job_position_id');
//        return response()->json($jobPositionDegreesMapping);

        return view('company.job',
            [
                'faculties' => Faculty::all(),
                'jobpositions' => JobPosition::orderBy('name')->get(),
                'jobPositionDegreesMapping' => $jobPositionDegreesMapping,
                'user' => $user,
                'mailboxCount' => $mailboxCount,
                'state' => $getState->state,
                'notifCount' => app('App\Http\Controllers\MailboxController')->getCountNotification(),
                'datas' => $datas,
                'notif' => app('App\Http\Controllers\MailboxController')->getLatestNotification(),
                // 'degrees_six_month' => $degrees_six_month,
                // 'degrees_twelve_month' => $degrees_twelve_month
            ]);
    }

    // Manage > Job
    // insert job for company
    // routes: /insertJobDescs
    public function insertCompany(Request $request)
    {
        //validasi kosong
        if ($request->input('txtiAllowance') == '' || $request->input('cmbPosition') == '' || $request->input('txtiJobDescription') == '' || $request->input('txtiQuantity') == '' || $request->input('txtiLocation') == '' || $request->input('txtiDeadline') == '' || $request->input('txtiDuration') == '') {
            return redirect('job')->with('fyi', 'All field must be filled.')->withInput();
        } else {
            if ($request->input('txtiAllowance') < 1) {
                return redirect()->back()->with(['fyi' => 'Allowance must be more than 0']);
            }
            $date_regex = '/(0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])[- \/.](19|20)\d\d/';

            //validasi date with regex
            if (!preg_match($date_regex, $request->input('txtiDeadline'))) {
                return redirect('job')->with('fyi', 'Wrong input format Deadline.')->withInput();
            }
            // else if ($request->input('txtiDuration') != 6 && $request->input('txtiDuration') != 12)
            // {
            //       return redirect('job')->with('fyi', 'Duration must be 6 or 12 months')->withInput();
            // }
            else if (!preg_replace('/[^0-9]/', '', $request->input('txtiQuantity'))) {
                return redirect('job')->with('fyi', 'Seat must be numeric')->withInput();
            } else {
                $link_test = null;
                if (!empty($request->input('checkboxLinkTest'))) {
                    if (empty($request->input('txtiLinkTest'))) {
                        return redirect('job')->with('fyi', 'All field must be filled.')->withInput();
                    } else {
                        //website: always have http:// or https:// in front
                        $web = null;
                        if (strpos($request->input('txtiLinkTest'), 'http://') === false && strpos($request->input('txtiLinkTest'), 'https://') === false) {
                            $web = 'http://' . $request->input('txtiLinkTest');
                        } else {
                            $web = $request->input('txtiLinkTest');
                        }
                        $link_test = $web;
                    }
                }

                $degrees = [];
                foreach ($request->all() as $key => $value) {
                    if (substr($key, 0, 8) == 'degreei_') {
                        $degree_id = substr($key, 8);
                        $degree = Degree::where('id', '=', $degree_id)->first();
                        array_push($degrees, $degree);
                    }
                }
                if (empty($degrees)) {
                    return redirect('job')->with('fyi', 'Please select a program!')->withInput();
                }

                //$duration = $request->txtiDuration;
                // $degree_filter = null;
                // if($duration == 6){
                //     $degree_filter = Degree::getDurationSixMonth();
                // }
                // else if($duration == 12){
                //     $degree_filter = Degree::getDurationTwelveMonth();
                // }
                // $degree_filter_as_array = explode(';', $degree_filter);

                // foreach ($degrees as $deg) {
                //     $found = FALSE;
                //     foreach ($degree_filter_as_array as $d) {
                //         if($deg->id == $d){
                //             $found = TRUE;
                //             break;
                //         }
                //     }
                //     if($found == FALSE){
                //         return redirect('/job')->with('fyi', 'Invalid program input, please reselect the duration, to choose program.')->withInput();
                //     }
                // }

                // $tc = "";
                // if ($request->TC1)$tc.="1;";
                // if ($request->TC2)$tc.="2;";
                // if ($request->TC3)$tc.="3;";
                // if ($request->TC4)$tc.="4;";
                // if ($request->TC5)$tc.="5;";
                // if ($request->TC6)$tc.="6;";
                // if ($request->TC7)$tc.="7;";
                // if ($request->TC8)$tc.="8;";
                // if ($request->TC9)$tc.="9;";
                // if ($request->TC10)$tc.="10;";
                // if ($request->TC11)$tc.="11;";
                // if ($request->TC12)$tc.="12;";
                //
                // $ss = "";
                // if ($request->SS1)$ss.="1;";
                // if ($request->SS2)$ss.="2;";
                // if ($request->SS3)$ss.="3;";
                // if ($request->SS4)$ss.="4;";
                // if ($request->SS5)$ss.="5;";
                // if ($request->SS6)$ss.="6;";

                //insert jobnya
                $objJob = new Job;
                $objJob->companyid = Auth::user()->userid;
                $objJob->supervisor_name = $request->input('txtiSupervisor');
                $objJob->supervisor_contact = $request->input('txtiSupervisorContact');
                $objJob->name = $request->input('cmbPosition');
                $objJob->description = $request->input('txtiJobDescription');
                $objJob->quota = $request->input('txtiQuantity');
                $objJob->location = $request->input('txtiLocation');
                $objJob->deadline = date("Y-m-d", strtotime($request->input('txtiDeadline')));
                $objJob->duration = $request->input('txtiDuration');
                $objJob->startdate = date("Y-m-d", strtotime($request->input('txtiStartDate')));
                $objJob->enddate = date("Y-m-d", strtotime($request->input('txtiEndDate')));
                $objJob->linktest = $link_test;
                $objJob->job_state = 'waiting';

                $autoApproveJobAutomatedService = AutomatedService::where('name', 'auto_approve_job')->first();
                if ($autoApproveJobAutomatedService != null && $autoApproveJobAutomatedService->status == 'running') {
                    $objJob->job_state = 'open';
                }

                $objJob->allowance = $request->input('txtiAllowance');
                $objJob->token = str_replace('.', 'H', str_replace("$", "S", str_replace("/", "X", Hash::make(Auth::user()->userid . $request->input('txtiQuantity') . $request->input('txtiLocation')))));
                $objJob->is_using_bipp_min_ipk = ($request->rdHasMinIPK == 'yes') ? '1' : '0';
                $objJob->save();

                foreach ($degrees as $d) {
                    $newjobsdegrees = new JobsDegrees;
                    $newjobsdegrees->jobid = $objJob->id;
                    $newjobsdegrees->degreeid = $d->id;
                    $newjobsdegrees->save();
                }

                return redirect('job')->with('fyi', 'Berhasil');
            }
        }
    }

    // Manage > Job
    // close job for company
    // routes: /close_job/
    public function closeByCompany($job_token)
    {
        $job = Job::where('token', '=', $job_token)->first();
        if ($job == null) {
            return redirect('/job')->with(['fyi' => 'Job not found']);
        }

        if ($job->job_state == 'closed' || $job->job_state == 'rejected') {
            return redirect('/job')->with(['fyi' => 'Job has already closed']);
        }

        $job->job_state = 'closed';
        $job->save();
        return redirect('/job')->with(['fyi' => 'Berhasilclose']);
    }

    // Manage > Job
    // open job for company
    // routes: /open_job/
    public function openByCompany($job_token)
    {
        $job = Job::where('token', '=', $job_token)->first();
        if ($job == null) {
            return redirect('/job')->with(['fyi' => 'Job not found']);
        }

        if ($job->job_state == 'open') {
            return redirect('/job')->with(['fyi' => 'Job has already opened']);
        }

        if ($job->job_state == 'closed') {
            $job->job_state = 'open';
            $job->save();
        }

        return redirect('/job')->with(['fyi' => 'Berhasilopen']);
    }

    // Manage > Job
    // close all jobs for company
    // routes: /close_jobs/
    public function closeAllByCompany()
    {
        $company = Auth::user()->company()->first();

        $jobs = $company->job()->where('job_state', 'open')->get();

        foreach ($jobs as $j) {
            $j->job_state = 'closed';
            $j->save();
        }
        return redirect('/job')->with(['fyi' => 'Berhasilclose']);
    }

    // Manage > Job
    // open all jobs for company
    // routes: /open_jobs/
    public function openAllByCompany()
    {
        $company = Auth::user()->company()->first();

        $jobs = $company->job()->where('job_state', 'closed')->get();

        foreach ($jobs as $j) {
            $j->job_state = 'open';
            $j->save();
        }
        return redirect('/job')->with(['fyi' => 'Berhasilopen']);
    }

    // Manage > Job
    // update job for company
    // routes: /updateJobDescs
    public function updateCompany(Request $request)
    {
        //validasi kosong
        if ($request->input('txtuAllowance') == '' || $request->input('cmbPositionU') == '' || $request->input('txtuJobDescription') == '' || $request->input('txtuQuantity') == '' || $request->input('txtuLocation') == '' || $request->input('txtuDeadline') == '' || $request->input('txtuDuration') == '') {
            return redirect('job')->with('fyi', 'All field must be filled.');
        } else {
            if ($request->input('txtuAllowance') < 1) {
                return redirect()->back()->with(['fyi' => 'Allowance must be more than 0']);
            }
            //date validation
            $date_regex = '/(0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])[- \/.](19|20)\d\d/';

            if (!preg_match($date_regex, $request->input('txtuDeadline'))) {
                return redirect('job')->with('fyi', 'Wrong input format Deadline.')->withInput();
            }
            // else if ($request->input('txtuDuration') != 6 && $request->input('txtuDuration') != 12)
            // {
            //       return redirect('job')->with('fyi', 'Duration must be 6 or 12 months')->withInput();

            // }
            else if (!preg_replace('/[^0-9]/', '', $request->input('txtuQuantity'))) {
                return redirect('job')->with('fyi', 'Seat must be numeric')->withInput();

            } else {
                $link_test = null;
                if (!empty($request->input('checkboxLinkTestU'))) {
                    if (empty($request->input('txtuLinkTest'))) {
                        return redirect('job')->with('fyi', 'All field must be filled.');
                    } else {
                        //website: always have http:// or https:// in front
                        $web = null;
                        if (strpos($request->input('txtuLinkTest'), 'http://') === false && strpos($request->input('txtuLinkTest'), 'https://') === false) {
                            $web = 'http://' . $request->input('txtuLinkTest');
                        } else {
                            $web = $request->input('txtuLinkTest');
                        }
                        $link_test = $web;
                    }
                }

                // $tc = "";
                // if ($request->TC1)$tc.="1;";
                // if ($request->TC2)$tc.="2;";
                // if ($request->TC3)$tc.="3;";
                // if ($request->TC4)$tc.="4;";
                // if ($request->TC5)$tc.="5;";
                // if ($request->TC6)$tc.="6;";
                // if ($request->TC7)$tc.="7;";
                // if ($request->TC8)$tc.="8;";
                // if ($request->TC9)$tc.="9;";
                // if ($request->TC10)$tc.="10;";
                // if ($request->TC11)$tc.="11;";
                // if ($request->TC12)$tc.="12;";
                //
                // $ss = "";
                // if ($request->SS1)$ss.="1;";
                // if ($request->SS2)$ss.="2;";
                // if ($request->SS3)$ss.="3;";
                // if ($request->SS4)$ss.="4;";
                // if ($request->SS5)$ss.="5;";
                // if ($request->SS6)$ss.="6;";

                $job = Job::where('token', '=', $request->input('txtuToken'))->first();

                $degrees = [];
                foreach ($request->all() as $key => $value) {
                    if (substr($key, 0, strlen('degreeu_' . $job->id . '_')) == 'degreeu_' . $job->id . '_') {
                        if ($value == 'off') {
                            continue;
                        }

                        $degree_id = substr($key, strlen('degreeu_' . $job->id . '_'));
                        $degree = Degree::where('id', '=', $degree_id)->first();
                        array_push($degrees, $degree);
                    }
                }
                if (empty($degrees)) {
                    return redirect('job')->with('fyi', 'Invalid input.')->withInput();
                }
                //$duration = $request->txtuDuration;
                // $degree_filter = null;
                // if($duration == 6){
                //     $degree_filter = Degree::getDurationSixMonth();
                // }
                // else if($duration == 12){
                //     $degree_filter = Degree::getDurationTwelveMonth();
                // }
                // $degree_filter_as_array = explode(';', $degree_filter);

                // foreach ($degrees as $deg) {
                //     $found = FALSE;
                //     foreach ($degree_filter_as_array as $d) {
                //         if($deg->id == $d){
                //             $found = TRUE;
                //             break;
                //         }
                //     }
                //     if($found == FALSE){
                //         return redirect('/job')->with('fyi', 'Invalid program input, please reselect the duration, to choose program.')->withInput();
                //     }
                // }

                JobsDegrees::where('jobid', '=', $job->id)->forceDelete();

                foreach ($degrees as $d) {
                    $newjobsdegrees = new JobsDegrees;
                    $newjobsdegrees->jobid = $job->id;
                    $newjobsdegrees->degreeid = $d->id;
                    $newjobsdegrees->save();
                }

                //check auto approve job automated service
                $autoApproveJobAutomatedService = AutomatedService::where('name', 'auto_approve_job')->first();

                //update job
                Job::where('token', '=', $request->input('txtuToken'))->update(
                    [
                        'supervisor_name' => $request->input('txtuSupervisor'),
                        'supervisor_contact' => $request->input('txtuSupervisorContact'),
                        'name' => $request->input('cmbPositionU'),
                        'description' => $request->input('txtuJobDescription'),
                        'quota' => $request->input('txtuQuantity'),
                        'location' => $request->input('txtuLocation'),
                        'deadline' => date("Y-m-d", strtotime($request->input('txtuDeadline'))),
                        'startdate' => date("Y-m-d", strtotime($request->input('txtuStartDate'))),
                        'enddate' => date("Y-m-d", strtotime($request->input('txtuEndDate'))),
                        'duration' => $request->input('txtuDuration'),
                        'linktest' => $link_test,
                        'job_state' => ($autoApproveJobAutomatedService != null && $autoApproveJobAutomatedService->status == 'running') ? 'open' : 'waiting',
                        'allowance' => $request->input('txtuAllowance'),
                        'is_using_bipp_min_ipk' => ($request->rduHasMinIPK == 'yes') ? '1' : '0',
                    ]
                );
                return redirect('job')->with('fyi', 'Berhasilupdate');
            }
        }
    }

    // Manage > Job
    // delete job for company
    // routes: /deleteJobs
    public function deleteCompany($token)
    {
        /* Add Validation to redirect if company access this feature */
        //
        if (!Department::isSuperAdmin()) {
            return redirect('/');
        }
        //
        $job = Job::where('token', $token)->first();
        $rec = Recruitment::where('jobid', $job->id)->get();

        foreach ($rec as $r) {
            // delete approval_request dan unaccept_request
            $r->approval_request()->delete();
            $r->unaccept_request()->delete();
            $r->unreject_request()->delete();

            // delete interview
            $r->interview()->delete();

            $r->unapplied_by = 'deleted_job';
            $r->unapplied_on = Carbon::now();
            $r->save();
            $r->delete();
        }

        $job->jobsdegrees()->delete();
        $job->delete();

        return redirect('/job')->with('fyi', 'Berhasildelete');
    }

    // Manage > Job
    // approve job for department
    // routes: /approve_job/{job}
    public function approveByDepartment(Job $job)
    {
        $job->job_state = 'open';
        $job->approved_by_user_id = Auth::user()->id;
        $job->save();
        return redirect()->back()->with('success', 'Success approve job!');
    }

    // Manage > Job
    // reject job for department
    // routes: /reject_job/{job}
    public function rejectByDepartment(Job $job)
    {
        $job->job_state = 'rejected';
        $job->save();
        return redirect()->back()->with('success', 'Success reject job!');
    }

    // Manage > Job
    // reject job for department
    // routes: /close_job/{job}
    public function closeVacancyDepartment(Job $job)
    {
        $job->job_state = 'closed';
        $job->save();
        return redirect()->back()->with('success', 'Success closed job vacancies!');
    }

    // Manage > Job
    // delete job for department
    // routes: /deleteJob/
    public function deleteDepartment($token)
    {
        $job = Job::where('token', $token)->first();
        $rec = Recruitment::where('jobid', $job->id)->get();

        foreach ($rec as $r) {
            // delete approval_request dan unaccept_request
            $r->approval_request()->delete();
            $r->unaccept_request()->delete();
            $r->unreject_request()->delete();

            // delete interview
            $r->interview()->delete();

            $r->unapplied_by = 'deleted_job';
            $r->unapplied_on = Carbon::now();
            $r->save();
            $r->delete();
        }

        $job->jobsdegrees()->delete();
        $job->delete();

        return redirect()->back()->with('success', 'Success delete job!');
    }

    // Job Detail page
    // routes: /job_detail/
    public function job_detail_view_by_department($job_id)
    {
        $job = Job::where('id', '=', $job_id)->first();
        $company = $job->company()->first();
        $degrees = Degree::join('jobsdegrees', 'jobsdegrees.degreeid', '=', 'degrees.id')
            ->join('jobs', 'jobs.id', '=', 'jobsdegrees.jobid')
            ->whereNull('jobs.deleted_at')
            ->whereNull('jobsdegrees.deleted_at')
            ->where('jobsdegrees.jobid', '=', $job_id)
            ->select('degrees.name as name')
            ->get();

        /*
        $technical_competencies = explode(';', $job->learningobj);
        $soft_skills = explode(';', $job->softskill);

        $TCarray = array();
        $SSarray = array();
        for ($i=0; $i <= sizeof($technical_competencies); $i++) {
        $TCarray['tc'.$i] = 'on';
        }
        for ($i=0; $i <= sizeof($soft_skills); $i++) {
        $SSarray['ss'.$i] = 'on';
        }
         */

        $mailboxCount = Mailbox::where('type', '=', 'mailbox')
            ->where('status', '=', 'unseen')
            ->where('recipientid', '=', Auth::user()->userid)
            ->count();

        return view('department.job_detail',
            [
                'job' => $job,
                'company' => $company,
                'degrees' => $degrees,
                //'TCarray' => $TCarray,
                //'SSarray' => $SSarray,
                'mailboxCount' => $mailboxCount,
                'notifCount' => app('App\Http\Controllers\MailboxController')->getCountNotification(),
                'notif' => app('App\Http\Controllers\MailboxController')->getLatestNotification(),
            ]);
    }
}
?>