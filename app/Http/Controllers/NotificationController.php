<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Hash;
use Auth;
use DB;
use Validator;
use Storage;
use Response;
use route;
use URL;
use Illuminate\Http\Request;
use App\Http\Requests;
use Carbon\Carbon;
use App\Mailbox;
use App\User;
use App\Student;
use App\Company;
use App\UnacceptRequest;
use App\UnrejectRequest;
use App\ApprovalRequest;

class NotificationController extends BaseController
{
    public static function createNewNotification($senderid, $recipientid, $subject, $message, $type){
        $mailInsert = new Mailbox;
        $mailInsert->senderid = $senderid;
        $mailInsert->recipientid = $recipientid;
        $mailInsert->subject = $subject;
        $mailInsert->date = Carbon::now();
        $mailInsert->message = $message;
        $mailInsert->status = 'unseen';
        $mailInsert->type = $type;
        $mailInsert->token = '';
        $mailInsert->save();
    }

    public static function createApproveUnacceptRequestNotif(UnacceptRequest $req, $senderid){
        $recruitment = $req->recruitment()->first();
        $job = $recruitment->job()->first();

        // send notif to student
        $recipientid = $recruitment->studentid;
        $subject = 'Your job application has been unaccepted';
        $message = $job->company()->first()->name." has unaccept your job application as ".$job->name;
        $type = "notif";
        NotificationController::createNewNotification($senderid, $recipientid, $subject, $message, $type);

        // send notif to company
        $recipientid = $job->company()->first()->companyid;
        $subject = 'Unaccept request has been approved';
        $message = 'Internship Apps has approved your unaccept request for student: '.$recruitment->student()->first()->name;
        $type = "notif";
        NotificationController::createNewNotification($senderid, $recipientid, $subject, $message, $type);
    }

    public static function createApproveUnrejectRequestNotif(UnrejectRequest $req, $senderid){
        $recruitment = $req->recruitment()->first();
        $job = $recruitment->job()->first();

        // send notif to student
        $recipientid = $recruitment->studentid;
        $subject = 'Your job application has been unrejected';
        $message = $job->company()->first()->name." has unreject your job application as ".$job->name;
        $type = "notif";
        NotificationController::createNewNotification($senderid, $recipientid, $subject, $message, $type);

        // send notif to company
        $recipientid = $job->company()->first()->companyid;
        $subject = 'Unreject request has been approved';
        $message = 'Internship Apps has approved your unreject request for student: '.$recruitment->student()->first()->name;
        $type = "notif";
        NotificationController::createNewNotification($senderid, $recipientid, $subject, $message, $type);
    }

    public static function createRejectUnacceptRequestNotif(UnacceptRequest $req, $senderid){
        $recruitment = $req->recruitment()->first();
        $job = $recruitment->job()->first();

        // send notif to company
        $recipientid = $job->company()->first()->companyid;
        $subject = 'Unaccept request has been rejected';
        $message = 'Internship Apps has rejected your unaccept request for student: '.$recruitment->student()->first()->name;
        $type = "notif";
        NotificationController::createNewNotification($senderid, $recipientid, $subject, $message, $type);
    }

    public static function createRejectUnrejectRequestNotif(UnrejectRequest $req, $senderid){
        $recruitment = $req->recruitment()->first();
        $job = $recruitment->job()->first();

        // send notif to company
        $recipientid = $job->company()->first()->companyid;
        $subject = 'Unreject request has been rejected';
        $message = 'Internship Apps has rejected your unreject request for student: '.$recruitment->student()->first()->name;
        $type = "notif";
        NotificationController::createNewNotification($senderid, $recipientid, $subject, $message, $type);
    }

    public static function createApproveApprovalRequestNotif(ApprovalRequest $req, $senderid){
        $recruitment = $req->recruitment()->first();
        $job = $recruitment->job()->first();

        // send notif to student
        $recipientid = $recruitment->studentid;
        $subject = 'Your job application has been approved by Internship Apps';
        $message = "Internship Apps has approved your job application as ".$job->name;
        $type = "notif";
        NotificationController::createNewNotification($senderid, $recipientid, $subject, $message, $type);

        // send notif to company
        $recipientid = $job->company()->first()->companyid;
        $subject = 'Job Application has been approved by Internship Apps';
        $message = 'Internship Apps has approved job application for student: '.$recruitment->student()->first()->name.' as '.$job->name;
        $type = "notif";
        NotificationController::createNewNotification($senderid, $recipientid, $subject, $message, $type);
    }

    public static function createRejectApprovalRequestNotif(ApprovalRequest $req, $senderid){
        $recruitment = $req->recruitment()->first();
        $job = $recruitment->job()->first();

        // send notif to student
        $recipientid = $recruitment->studentid;
        $subject = 'Your job application has been rejected by Internship Apps';
        $message = "Internship Apps has rejected your job application as ".$job->name;
        $type = "notif";
        NotificationController::createNewNotification($senderid, $recipientid, $subject, $message, $type);

        // send notif to company
        $recipientid = $job->company()->first()->companyid;
        $subject = 'Job Application has been rejected by Internship Apps';
        $message = 'Internship Apps has rejected job application for student: '.$recruitment->student()->first()->name.' as '.$job->name;
        $type = "notif";
        NotificationController::createNewNotification($senderid, $recipientid, $subject, $message, $type);
    }

    public static function createPendingApprovalRequestNotif(ApprovalRequest $req, $senderid){
        $recruitment = $req->recruitment()->first();
        $job = $recruitment->job()->first();

        // send notif to student
        $recipientid = $recruitment->studentid;
        $subject = 'Internship Apps has canceled the job application update';
        $message = "Internship Apps has cancel your job application update as ".$job->name;
        $type = "notif";
        NotificationController::createNewNotification($senderid, $recipientid, $subject, $message, $type);

        // send notif to company
        $recipientid = $job->company()->first()->companyid;
        $subject = 'Internship Apps has canceled job application update';
        $message = 'Internship Apps has canceled job application update for student: '.$recruitment->student()->first()->name.' as '.$job->name;
        $type = "notif";
        NotificationController::createNewNotification($senderid, $recipientid, $subject, $message, $type);
    }
}
?>