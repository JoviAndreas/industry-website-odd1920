<?php

namespace App\Http\Controllers;

use App\AutomatedService;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use App\Http\Requests;

class AutomatedServiceController extends Controller
{
    public function index() {
        $automatedServices = [
            'auto_approve_job' => 'Automatically Approve Job Vacancies'
        ];
        return view('department.automated_service.index', compact('automatedServices'));
    }

    public function update(Request $request) {
        $requestKeys = array_keys($request->all());
        $automatedServiceRequests = preg_grep('/automated_service;/', $requestKeys);
        foreach ($automatedServiceRequests as $automatedServiceRequest) {
            $keys = explode(';', $automatedServiceRequest);
            $automatedServiceName = $keys[1];
            $automatedServiceStatus = $request->input($automatedServiceRequest);
            if ($automatedServiceStatus != 'running') {
                $automatedServiceStatus = 'stopped';
            }
            $automatedService = AutomatedService::where('name', $automatedServiceName)->first();
            if ($automatedService == null) {
                $automatedService = new AutomatedService;
                $automatedService->name = $automatedServiceName;
            }
            $automatedService->status = $automatedServiceStatus;
            $automatedService->save();
        }
        return redirect()->back()->with('success', 'saved configuration');
    }
}
?>