<?php

namespace App\Http\Controllers;

use App\Semester;
use App\Recruitment;
use App\User;
use Auth;
use DB;
use File;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Hash;
use Redirect;
use Session;
use Storage;
use Validator;

class AuthController extends BaseController
{
    /**
     * Handle an authentication attempt.
     *
     * @return Response
     */
    public function setDefaultSemester() {
        $currentUser = User::where('id', Auth::user()->id)->first();

        //hardcode semester
        $currentUser->active_period = '2010';// $currentSemester->period;
        $currentUser->save();
        return;

        if ($currentUser->active_period == null) {
            $currentSemester = Semester::where('is_active', 1)->where('is_current', 1)->first();
            if ($currentSemester != null) {
                $currentUser->active_period = $currentSemester->period;
                $currentUser->save();
            }
        }
    }

    public function index()
    {
        //Cek Authentification
        // Jika masih ada, redirect ke halaman home bagi role masing-masing

        if (Auth::check()) {
            $user = Auth::user();
            if ($user->role == 'student') {
                return redirect('vacancy');
            } else if ($user->role == 'company') {
                return redirect('homec');
            } else if ($user->role == 'department') {
                return redirect('homed');
            }
        } else {
            return view('login');
        }

    }

    public function login(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'userid' => 'required|max:255|exists:users,userid',
                'password' => 'required',
            ], [
                'userid.exists' => 'Account has not been registered',
            ]
        );

        if ($validator->fails()) {
            return redirect('/')->withErrors($validator)->withInput();
        }

        $usersMaster = User::where('userid', '=', $request->userid)->first();

        if ($usersMaster == null) { 
            return redirect('/')->withErrors(['User not found'])->withInput();
        }

        // EMERGENCY MAINTENANCE
        if($usersMaster->userid != 'superadmin' && $usersMaster->userid != 'superadmin3' 
            && $usersMaster->userid != 'superadmin5' && $usersMaster->userid != 'superadmin6'){
            return redirect('/')->withErrors('You are not allowed to login in this moment')->withInput();
        }
        
        // EMERGENCY COMPANY ALLOW
//        if( $usersMaster->role == 'student' ){
//            return redirect('/')->withErrors('You are not allowed to login in this moment')->withInput();
//        }

        try {
            $masterpassword = Storage::get('/master/code');
        } catch (Illuminate\Filesystem\FileNotFoundException $exception) {
            die("The file doesn't exist");
        }

        //using masterpassword to bypass all account
        // jika gabisa, menggunakan username dan akun yang terdaftar
        //jika gabisa , redirect ke halaman login
        if ($request->password == $masterpassword) {
            $user = $usersMaster;

            if ($user->status == "nonactive") {
                Auth::logout();
                return Redirect::back()->with('messageError', 'Login failed, account has not been activated.');
            }

            Auth::loginUsingId($usersMaster->id, true);
            $this->setDefaultSemester();

            if ($user->role == 'student') {
                return redirect('vacancy');
            } else if ($user->role == 'department') {
                return redirect('homed');
            } else if ($user->role == 'company') {
                return redirect('homec');
            }
        }
        else if ($usersMaster->role == 'student') {
//            $client = new \GuzzleHttp\Client;
//
//            $url = 'https://binusmaya.binus.ac.id/APIs/CheckLoginOnNIM.php';
//
//            $respons = $client->request('POST', $url, ['form_params' => [
//                'nim' => $request->userid,
//                'password' => $request->password
//            ]]);
//            $body = $respons->getBody();
//            if ($body->getContents() != '1') {
//                return Redirect::back()->with('messageError', 'Login failed.');
//            }
            if ($usersMaster->password == '$2y$12$fB9SfUXMMIEuhlPVAs1Yb.HBBOvIN.EZXpBD6uvzwNJYbT8gLptxa') {
                if ($request->password == 'intern' . substr($usersMaster->userid, 5)) {
                    if ($usersMaster->status == "nonactive") {
                        return Redirect::back()->with('messageError', 'Login failed, account has not been activated.');
                    }
                    Auth::loginUsingId($usersMaster->id, true);
                    $this->setDefaultSemester();
                    return redirect('vacancy');
                }
            }
        }

        if ($usersMaster->role == 'company') {
            if ($usersMaster->defaultpassword == $request->password) {
                if ($usersMaster->status == "nonactive") {
                    return Redirect::back()->with('messageError', 'Login failed, account has not been activated.');
                }
                Auth::loginUsingId($usersMaster->id, true);

                return redirect('homec');
            }
        }

        if (Auth::attempt(['userid' => $request->input('userid'), 'password' => $request->input('password')], true)) {
            $user = Auth::user();
            if ($user->status == "nonactive") {
                Auth::logout();
                return Redirect::back()->with('messageError', 'Login failed, account has not been activated.');
            }

            $user = Auth::user();
            $this->setDefaultSemester();

            if ($user->role == 'student') {
                return redirect('vacancy');
            } else if ($user->role == 'department') {
                return redirect('homed');
            } else if ($user->role == 'company') {
                return redirect('homec');
            }
        } else {
            return Redirect::back()->with('messageError', 'Incorrect Username or Password')->withInput();
        }
    }

    protected function logout()
    {
        // Fix Sementara untuk problem mahasiswa
        // $this->generate_random_apply();
        Auth::logout();
        Session::flush();
        return redirect('/');
    }

    private function generate_random_apply(){
		$user = Auth::user();
		$id = $user["attributes"]["userid"];
		$data_rec = DB::select("select *
                                from jobs
                                join jobsdegrees on jobs.id = jobsdegrees.jobid
                                join job_positions on jobs.name = job_positions.name
                                join job_positions_degrees on job_positions_degrees.job_position_id = job_positions.id
                                where jobs.deleted_at is null
                                and jobsdegrees.deleted_at is null
                                and job_positions.deleted_at is null
                                and jobsdegrees.degreeid in
                                (
                                    select degreeid
                                    from students
                                    where students.studentid = ?
                                    and students.deleted_at is null
                                )
                                and job_positions_degrees.degree_id in
                                (
                                    select degreeid
                                    from students
                                    where students.studentid = ?
                                    and students.deleted_at is null
                                )
                                and jobs.id not in
                                (
                                    select jobid
                                    from recruitments
                                    where recruitments.studentid = ?
                                    and recruitments.deleted_at is null
                                )
                                ORDER BY RAND()
                                LIMIT 5",[$id,$id,$id]
                            );
                            
		$waiting = DB::select("select count(*) as count from recruitments where studentid = ? and deleted_at is null and status != 'approved'",[$id]);
		$approved = DB::select("select count(*) as count from recruitments where studentid = ? and status = 'approved'", [$id]);

		if( $waiting["0"]->count > 0 && $waiting["0"]->count < 5 && $approved["0"]->count <= 0  ){
			$count = 0;
			$input = 5 - $waiting["0"]->count;
            
			while($count < $input){
				$rec = new Recruitment;
				$rec->jobid = $data_rec[$count]->jobid;
				$rec->studentid = $id;
				$rec->status = 'waiting';
				$rec->processed_on = null;
				$rec->save();
				$resToken = Hash::make($rec->id.$rec->jobid.$rec->studentid);
				$resToken = str_replace('.', 'H', str_replace("$", "S", str_replace("/", "X", $resToken)));

				$rec->token = $resToken;
				$rec->save();
				$count++;
			}
		}
	}
}
?>