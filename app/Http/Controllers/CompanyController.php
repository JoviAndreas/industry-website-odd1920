<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Http\Request;
use Dompdf\Dompdf;
use Storage;
use Response;
use Auth;
use DB;
use File;
use Validator;
use Hash;
use App\Recruitment;
use App\Mailbox;
use App\Company;
use App\Student;
use App\Degree;
use App\User;
use App\Interview;
use App\Feedback;
use App\Job;
use App\Bipp;
use Carbon\Carbon;

class CompanyController extends BaseController{

	use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

	//function untuk cek apakah student sudah di accept atau belum
	public function checkAccepted()
	{
		$rec = Recruitment::where('studentid','=', Auth::user()->userid)
			->where('status','=','approved')
			->first();
		if($rec != null)
		{
			return 'accepted';
		}
		else
		{
			return "no";
		}
	}

	//generate string random lower alpha
	function generateRandomLetter($length = 10)
	{
		$characters = 'abcdefghijklmnopqrstuvwxyz';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++)
		{
		    $randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}

	//generate string random upper+lower alpha + numeric
	function generateRandomString($length = 10)
	{
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++)
		{
		    $randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}

	// Manage > Company page
	// routes: /register
	public function indexDepartment()
	{
		$allowed_degree_id = DepartmentController::get_allowed_degree_list();

		$data = Company::join('users', 'companyid', '=', 'users.userid')
			->whereIn('companyid', function($query) use ($allowed_degree_id){
				$query->select('companies.companyid')
					->from('jobs')
					->join('jobsdegrees', 'jobsdegrees.jobid', '=', 'jobs.id')
					->rightJoin('companies', 'companies.companyid', '=', 'jobs.companyid')
					->where('users.active_period','2010')
					->whereNull('companies.deleted_at')
					->whereNull('jobs.deleted_at')
					->whereNull('jobsdegrees.deleted_at')
					->whereNull('jobs.id')
					->orWhereIn('jobsdegrees.degreeid', $allowed_degree_id);
			})
			->whereNull('users.deleted_at')
			->select(DB::raw('*, companies.id companyprimaryid, users.status userstatus'))
			->get();
		$bipp_id = Bipp::first()->id;

		foreach ($data as $d) {
			$total_quota = 0;
			$filled = 0;
			// get companies' quota count
			$jobs = $d->job()->get();
			foreach ($jobs as $job) {
				// get each job approved count
				$approved_count = Recruitment::where('jobid', '=', $job->id)
					->whereIn('status', ['approved', 'accepted'])
					->get()
					->count();

				//store the approved count to a var
				$filled += $approved_count;

				// store the total quota of each job
				$total_quota += $approved_count + $job->quota;
			}
			$d->total_quota = $total_quota;
			$d->available_quota = $total_quota - $filled;
		}
		return view('department.register', ['data' => $data])->with('bipp_id',$bipp_id);
	}

	// unapprove the company
	// routes: /unapproveCompany
	public function unapproveCompany($companyid){
		$company = Company::where('companyid', '=', $companyid)->first();
		if($company == null){
			return redirect('/register');
		}
		else{
			$user = $company->user()->first();
			$user->status = 'nonactive';
			$user->save();
			return redirect('/register')->with([
				'status' => 'You have successfully unapproved '.$company->name
			]);
		}
	}

	// Manage > company
	// submit company by form
	// routes: /submitByForm
	public function insertDepartment(Request $request)
	{
		
		//cek validator
		$messages =
			[
    			'txtcompany.required' => 'Field Must be filled!',
				'txtcompany.unique' => 'This company already registered!'
			];

		$rules =
			[
        		'txtcompany' => 'required|unique:companies,name'
			];

		$validator = Validator::make($request->all(), $rules, $messages);

		if ($validator->fails())
		{
	    	return redirect('/register')->withErrors($validator)->withInput();
		}
		else
		{
			//generate userid
			$name = $request->txtcompany;
			$name = trim($name);
			$userid = strtolower($name);
			$userid = str_replace('pt.','',$userid);
			$userid = str_replace('cv.','',$userid);
			$userid = str_replace('pt ','',$userid);
			$userid = str_replace('cv ','',$userid);
			$userid = trim($userid);
			$userid = str_replace(' ','',$userid);
			$userid = preg_replace('/[^A-Za-z0-9\-]/', '', $userid);

			if(strlen($userid) <6)
			{
				$userid .= $userid;
			}
			else if(strlen($userid) >10)
			{
				$userid = substr($userid,0,10);
			}

			//check jika sudah ada companynya berdasarkan generated userid
			$checkIfExist = Company::where('name', $name)->orWhere('companyid', $userid)->get();
			if(sizeof($checkIfExist) > 0)
			{
				return redirect('/register')->with('err','Company Already Exist!');
			}

			//insert data
			$defpassword = $this->generateRandomString(10);
			$password = bcrypt($defpassword);
			
			$user = new User;
			$user->userid = $userid;
			$user->role = 'company';
			$user->active_period = '2010';
			$user->defaultpassword = $defpassword;
			$user->password = $password;
			$user->save();

			$bipp_id = $request->bipp;

			$comp = new Company;
			$comp->companyid =  $userid;
			$comp->name = $name;
			$comp->bipp_id = $bipp_id;
			$comp->created_by = Auth::user()->userid;
			$comp->save();

			return redirect('/register')->with('status',"Company successfully inserted!");;
		}
	}

	// Manage > Company
	// generate new password for company (which is easier to read)
	// routes: /generateNew/
	public function generateNewPasswordDepartment($request)
	{
		//panggil fungsi generateRandomLetter untuk generate 8 karakter huruf
		$defpassword = $this->generateRandomLetter(8);
   		$password = bcrypt($defpassword);

       		//update data
       		User::where('userid', '=', $request)->update(
    			[
	   				'password' => $password,
	   				'defaultpassword'	=> $defpassword
    			]);

		return redirect('register')->with('status',"Company password successfully changed!");
	}

	// change state company to close
	public function changeStateClosedCompany()
	{
		$company = Company::where('companyid', '=', Auth::user()->userid)->first();
		$company->state = 'closed';
		$company->save();

		return redirect('/profilec/'.Auth::user()->userid)->with('fyi','Success Change your company state to closed!');
	}

	// change state of the company to be open
	public function changeStateOpenCompany(){
		$company = Company::where('companyid', '=', Auth::user()->userid)->first();
		$company->state = 'open';
		$company->save();

		return redirect('/profilec/'.Auth::user()->userid)->with('fyi','Success Change your company state to open!');;
	}

	// Manage > Company
	// change state company
	// @param $id => companyid
	// @param $state => current company's state
	// routes: /changeState/
	public function changeStateDepartment($id, $state)
	{
		if($state != 'closed' && $state != 'open')
		{
			return redirect('register')->with('err', 'wrong state input');
		}

		$company = Company::where('companyid', '=', $id)->first();
		$company->state = $state;
		$company->save();

		return redirect('register')->with('status', "Company's state successfully changed!");
	}

	// Manage > Company
	// change company status
	// @param $id => companyid
	// @param $state => current company's status
	// routes: /changeStatus/
	public function changeDepartmentStatus($id, $status)
	{
		if($status != 'active' && $status != 'nonactive')
		{
			return redirect('register')->with('err', 'wrong status input');
		}

		$company = User::where('userid', '=', $id)->first();
		$company->status = $status;
		$company->save();

		return redirect('register')->with('status', "Company's status successfully changed!");
	}

	// Manage Company
	// deleting company
	// routes: /deleteCompany/
	public function deleteDepartment($companyid)
	{
		$company = Company::where('companyid', $companyid)->first();

		if($company == null){
			return redirect('/register')->with('err', 'Cannot delete company, company not found!');
		}

		$job = $company->job()->get();

		//Delete in job application (recruitment + job table + jobsdegrees + approval_request + unaccept_request)
		foreach($job as $j)
		{
			$recruitment = Recruitment::where('jobid', $j->id)->get();
			foreach($recruitment as $r){
				// delete approval_request dan unaccept_request
				$r->approval_request()->delete();
				$r->unaccept_request()->delete();

				$r->interview()->delete();

				$r->unapplied_by = 'system';
				$r->unapplied_on = Carbon::now();
				$r->save();
				$r->delete();
			}
			$j->jobsdegrees()->delete();
			$j->delete();
		}

		// Delete supervisor
		$company->supervisor()->delete();

		//Delete mailbox
		$company->mailbox_sender()->delete();
		$company->mailbox_recipient()->delete();

		//Delete in table User
		$company->user()->delete();

		//Delete in table company
		$company->delete();

		return redirect('register')->with('status', 'Companies successfully removed!');
	}

	// Manage > companies
	// submit new companies by excel
	// routes: /submitByExcelStudent
	public function uploadDepartment(Request $request)
	{
		$messages =
			[
    			'fileexcel.required' => 'File Not Found!'
			];

		$rules =
			[
       			'fileexcel' => 'required'
    		];

   		$validator = Validator::make($request->all(), $rules, $messages);

    	if ($validator->fails())
    	{
    		return redirect('/register')->withErrors($validator)->withInput();
    	}
   		else
		{
			$arrList = array();
			\Excel::load($request['fileexcel'], function ($reader) use ($arrList)
				{
	        		$arrList = array();
	            	foreach ($reader->toArray() as $row)
	            	{
	            		if(strcmp($row['company_name'],'Pt. xxxxxxxx')==0){
	            			continue;
	            		}
						else{
	            			//generate userid buat company
	            			$name = $row['company_name'];
	            			$isBIPP = $row['is_bipp_truefalse'];
							$email = $row['company_email'];
							$phone = $row['company_phone'];

	        				$name = trim($name);
				        	$userid = strtolower($name);
				        	$userid = str_replace('pt.','',$userid);
				        	$userid = str_replace('cv.','',$userid);
				        	$userid = str_replace('pt ','',$userid);
	        				$userid = str_replace('cv ','',$userid);
				        	$userid = trim($userid);
				        	$userid = str_replace(' ','',$userid);
				        	$userid = preg_replace('/[^A-Za-z0-9\-]/', '', $userid);

							if(strlen($userid) <6){
				        		$userid .= $userid;
				        	}
				        	else if(strlen($userid) >10){
				        		$userid= substr($userid,0,10);
				        	}
				        	$userid = preg_replace('/[^A-Za-z0-9\-]/', '', $userid);

				        	//check jika sudah ada companynya berdasarkan generated companyid
				        	$checkIfExist = Company::where('name', $name)->get();

	        				if(sizeof($checkIfExist) > 0){
	        					array_push($arrList, $name);
	        				}
							else{
	        					//insert data
					        	$defpassword = $this->generateRandomString(10);
					        	$password = bcrypt($defpassword);
					        	$user = new User;
					        	$user->userid = $userid;
					        	$user->role = 'company';
					        	$user->defaultpassword = $defpassword;
					        	$user->password = $password;
					        	$user->save();

					        	$comp = new Company;
					        	$comp->companyid = $userid;
					        	$comp->name = $name;
					        	$comp->bipp_id = ($isBIPP == 'true') ? '1' : null;
					        	$comp->email = $email ?? '';
					        	$comp->phone = $phone ?? '';
					        	$comp->created_by = Auth::user()->userid;
					        	$comp->save();
				        	}
	            		}
	            	}
	            	return redirect('/register')->with('arrSame',$arrList);
	       		});
		  	return redirect('/register')->with('status',"All Company successfully inserted!");;
   		}
	}

	// Profile
	// company's profile page
	// routes: /profilec/
	public function profile($id)
	{
		$company = Company::where('companyid', '=', $id)->first();
		$user = User::where('userid', '=', $id)->first();

		$userProfile = User::join('companies', 'users.userid', '=', 'companies.companyid')
			->where('companyid', '=', $user->userid)
			->whereNull('companies.deleted_at')
			->first();

		$mailboxCount = Mailbox::where('type', '=', 'mailbox')
			->where('status', '=', 'unseen')
			->where('recipientid', '=', Auth::user()->userid)
			->count();

		return view("company.profile",
		[
			'accepted' => Auth::user()->role=='student' ? $this->checkAccepted() : null,
			'user' => $user,
			'userProfile' => $userProfile,
			'mailboxCount' => $mailboxCount,
			'state' => $company->state,
			'notifCount' => app('App\Http\Controllers\MailboxController')->getCountNotification(),
			'notif' => app('App\Http\Controllers\MailboxController')->getLatestNotification(),
			'company_user_status' => $company->user()->first()->status
		]);
	}

	// Profile
    // get update profile page for company
	// routes: /updateprofiles
	public function editProfileCompany()
	{
		$getState = Company::where('companyid', '=', Auth::user()->userid)->first();

		$user = Auth::user();

		$userProfile = User::join('companies', 'users.userid', '=', 'companies.companyid')
			->where('companyid', '=', $user->userid)
			->whereNull('companies.deleted_at')
			->first();

		$mailboxCount = Mailbox::where('type', '=', 'mailbox')
			->where('status', '=', 'unseen')
			->where('recipientid', '=', Auth::user()->userid)
			->count();

		return view("company.editprofile",
			[
				'user' => $user,
				'userProfile' => $userProfile,
				'mailboxCount' => $mailboxCount,
				'state' => $getState->state,
				'notifCount' => app('App\Http\Controllers\MailboxController')->getCountNotification(),
				'notif' => app('App\Http\Controllers\MailboxController')->getLatestNotification()
			]);
	}

	// Profile
	// submit update profile for company
	// routes: /submitprofiles
	public function updateEditProfileCompany(Request $request)
	{
		$messages =
		[
			'txtCompanyName.required' => 'Company Name must be Filled!',
			'cmbBusiness.required' => 'Business Category must be Chosen !',
            'txtEmail.required' => 'Email must be Filled!',
            'txtPhone.required' => 'Phone must be Filled!',
            'txtPic.required' => 'Contact Person must be Filled!',
            'txtAddress.required' => 'Address must be Filled!',
			'txtPic.max' => 'Contact person may not be greater than 50 characters!',
			'txtWebsite.max' => 'Website may not be greater than 100 characters!',
			'txtEmail.max' => 'Email may not be greater than 100 characters!',
		    'txtPhone.max' => 'Phone may not be greater than 100 characters!',
			'txtAddress.max' => 'Address may not be greater than 250 characters!',
			'profpic.max' => 'Profile picture may not be greater than 7 Megabytes'
		];

		$rules =
		[
			'txtCompanyName' => 'required|min:3',
			'cmbBusiness' => 'required',
        	'txtWebsite' => 'max:100',
        	'txtEmail' => 'required|max:100',
        	'txtPhone' => 'required|max:100',
        	'txtPic' => 'required|max:50',
        	'txtAddress' => 'required|max:250',
			'profpic' => 'image|max:2000'
		];

		$validator = Validator::make($request->all(), $rules, $messages);

		if ($validator->fails())
		{
		    return redirect('/updateprofiles')
                ->withErrors($validator)
                ->withInput();
		}
		else
		{
			if ($request->profpic != null)
			{
				if($request->profpic->getClientOriginalExtension() != 'jpg' && $request->profpic->getClientOriginalExtension() != 'png' && $request->profpic->getClientOriginalExtension() != 'gif' && $request->profpic->getClientOriginalExtension() != 'jpeg' && $request->profpic->getClientOriginalExtension() != 'JPG' && $request->profpic->getClientOriginalExtension() != 'PNG' && $request->profpic->getClientOriginalExtension() != 'GIF' && $request->profpic->getClientOriginalExtension() != 'JPEG')
				{
					return redirect('/updateprofiles')->withInput();
				}

				$destinationPath = storage_path().'/assets/images/'.Auth::user()->userid.'/';
				if(!File::exists($destinationPath)){
    				File::makeDirectory($destinationPath, 0777, true, true);
    			}
				$extension = $request->profpic->getClientOriginalExtension();
				$fileName = 'photo.'.$extension;
				$request->profpic->move($destinationPath, $fileName);
				$companyUpdate = Company::where('companyid', '=', Auth::user()->userid)->update(['photo' => $fileName]);
			}

			//website: always have http:// or https:// in front
			$web = null;
			if(strpos($request->txtWebsite, 'http://') === false && strpos($request->txtWebsite, 'https://') === false){
				$web = 'http://'.$request->txtWebsite;
			}
			else{
				$web = $request->txtWebsite;
			}

			$companyUpdate = Company::where('companyid', '=', Auth::user()->userid)->update(
				[
					'name' => $request->txtCompanyName,
					'business_category' => $request->cmbBusiness,
					'email' => $request->txtEmail,
					'phone' => $request->txtPhone,
					'website' => $web,
					'pic' => $request->txtPic,
					'address' => $request->txtAddress
				]);
		}
		return redirect('/profilec/'.Auth::user()->userid)->with('fyi','Success update profile!');;
	}

	// Manage > Company
	// export all companies data to excel file
	// routes: /exportaccount
	public function exportDepartment()
	{
		$data = Company::join('users', 'companyid', '=', 'users.userid')
			->join('jobs', 'jobs.companyid', '=', 'companies.companyid')
			->whereNull('users.deleted_at')
			->whereNull('jobs.deleted_at')
			->where('users.active_period','=','2010')
			->select(
				'companies.name as Company Name',
				'companies.companyid as Userid',
				'defaultpassword as Password',
				'companies.description as Description',
				'companies.email as Email',
				'companies.phone as Phone',
				'companies.pic as PIC',
				'companies.website as Website',
				'companies.address as Address',
				'companies.state as State',
				'companies.business_category as Business Categories',
				'jobs.name as job_name',
				'jobs.id as job_id'				
				)
			->orderBy('Userid', 'asc')
			->get();

		foreach ($data as $d) {
			// get each jobs' quota count
			$job = Job::where('id', '=', $d->job_id)->first();

			$d->filled = Recruitment::where('jobid', '=', $job->id)
				->whereIn('status', ['approved', 'accepted'])
				->get()
				->count();

			$d->total_quota = $d->filled + $job->quota;
		}

		\Excel::create('list_Account_Company', function($excel) use($data)
			{
				$excel->sheet('Sheet1', function($sheet) use($data)
				{
					$sheet->cell('A1', function($cell)
					{
	   					$cell->setValue('Company Name');
	   				});
					$sheet->cell('B1', function($cell)
					{
	   					$cell->setValue('User ID');
	   				});
					$sheet->cell('C1', function($cell)
					{
	   					$cell->setValue('Default Password');
	   				});
					$sheet->cell('D1', function($cell)
					{
	   					$cell->setValue('Description');
	   				});
					$sheet->cell('E1', function($cell)
					{
	   					$cell->setValue('Email');
	   				});
					$sheet->cell('F1', function($cell)
					{
	   					$cell->setValue('Phone');
	   				});
					$sheet->cell('G1', function($cell)
					{
	   					$cell->setValue('PIC');
	   				});
					$sheet->cell('H1', function($cell)
					{
	   					$cell->setValue('Website');
	   				});
					$sheet->cell('I1', function($cell)
					{
	   					$cell->setValue('Address');
	   				});
					$sheet->cell('J1', function($cell)
					{
	   					$cell->setValue('State');
	   				});
					$sheet->cell('K1', function($cell)
					{
	   					$cell->setValue('Job Name');
	   				});
					$sheet->cell('L1', function($cell)
					{
	   					$cell->setValue('Total Quota');
	   				});
					$sheet->cell('M1', function($cell)
					{
	   					$cell->setValue('Quota Filled');
					});
					$sheet->cell('N1', function($cell)
					{
	   					$cell->setValue('Business Categories');
					});
					$sheet->cell('O1', function($cell)
					{
	   					$cell->setValue('Period');
	   				});

					$counter=2;
    				foreach($data as $record)
    				{
						$sheet->cell('A'.$counter, function($cell)use($record)
						{
							 $cell->setValue($record->Company);
						});
						$sheet->cell('B'.$counter, function($cell)use($record)
						{
							 $cell->setValue($record->Userid);
						});
						$sheet->cell('C'.$counter, function($cell)use($record)
						{
							 $cell->setValue($record->Password);
						});
						$sheet->cell('D'.$counter, function($cell)use($record)
						{
							 $cell->setValue($record->Description);
						});
						$sheet->cell('E'.$counter, function($cell)use($record)
						{
							 $cell->setValue($record->Email);
						});
						$sheet->cell('F'.$counter, function($cell)use($record)
						{
							 $cell->setValue($record->Phone);
						});
						$sheet->cell('G'.$counter, function($cell)use($record)
						{
							 $cell->setValue($record->PIC);
						});
						$sheet->cell('H'.$counter, function($cell)use($record)
						{
							 $cell->setValue($record->Website);
						});
						$sheet->cell('I'.$counter, function($cell)use($record)
						{
							 $cell->setValue($record->Address);
						});
						$sheet->cell('J'.$counter, function($cell)use($record)
						{
							 $cell->setValue($record->State);
						});
						$sheet->cell('K'.$counter, function($cell)use($record)
						{
							 $cell->setValue($record->job_name);
						});
						$sheet->cell('L'.$counter, function($cell)use($record)
						{
							 $cell->setValue($record->total_quota);
						});
						$sheet->cell('M'.$counter, function($cell)use($record)
						{
							 $cell->setValue($record->filled);
						});
						$sheet->cell('N'.$counter, function($cell)use($record)
						{
							 $cell->setValue($record->Business);
						});
						$sheet->cell('O'.$counter, function($cell)use($record)
						{
							 $cell->setValue($record->active_period);
						});
						$counter++;
					}
    			});
			})->export('xlsx');
	}
}
?>
