<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Http\Request;
use Dompdf\Dompdf;
use Storage;
use Response;
use Auth;
use DB;
use Validator;
use Hash;
use App\Recruitment;
use App\Mailbox;
use App\Company;
use App\Student;
use App\User;
use App\Interview;
use App\Feedback;
use App\Job;
use App\Supervisor;
use App\Services\EmailService;

class InterviewController extends BaseController{
	use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

	// home
	// for company to submit new interview data
	// routes: /setInterview
	public function insertInterviewCompany(Request $request)
	{
		$resToken = Hash::make(Auth::user()->userid.$request->input('txtNIM').date('Y-m-d H:i:s'));
		$resToken = str_replace('.', 'H', str_replace("$", "S", str_replace("/", "X", $resToken)));

		$recruitment_id = Recruitment::where('token', '=', $request->input('txtToken'))->first()->id;
		$prev_interview = Interview::where('recruitment_id', '=', $recruitment_id)
			->where('status', '=', 'waiting')
			->get();

		if($prev_interview->count() > 0){
			return redirect('homec')->with('err', 'You have already sent Interview Invitation before');
		}

		$intvw = new Interview;
		$intvw->recruitment_id = Recruitment::where('token', '=', $request->input('txtToken'))->first()->id;
		$intvw->interviewer = Auth::user()->userid;
		$intvw->interviewee = $request->input('txtNIM');
		$intvw->location = $request->input('txtLocation');
		$intvw->date = date("Y-m-d", strtotime($request->input('txtDate')));
		$intvw->time = $request->input('txtTime');
		$intvw->status = 'waiting';
		$intvw->pic =$request->input('txtPIC');
		$intvw->phone = $request->input('txtPhone');
		$intvw->token = $resToken;
		$intvw->save();

		//Insert mailbox
		$message = '';
		$mailInsert = new Mailbox;
		$mailInsert->interview_id = $intvw->id;
		$mailInsert->senderid = Auth::user()->userid;
		$mailInsert->recipientid =$request->input('txtNIM');
		$mailInsert->subject = 'Interview Invitation';
		$mailInsert->date = date("Y-m-d H:i:s");
		$mailInsert->message =  $message;
		$mailInsert->status = 'unseen';
		$mailInsert->type = 'interview';
		$mailInsert->token = $resToken;
		$mailInsert->save();

		// send email notif to student that there is new interview invitation
		EmailService::createNewInterviewInvitationEmail($intvw);

		return redirect('homec')->with('fyi', 'Interview Berhasil');
	}

	// Mailbox Interview invitation
	// for student to accept interview invitation
	// routes: /acceptInterview
	public function updateAcceptInterviewStudent(Request $request)
	{
		$interview = Interview::where('token', '=', $request->input('tokenid'))
			->first();

		$profilesender = Student::where('studentid', '=', Auth::user()->userid)
			->first();

		if($interview->interviewee != Auth::user()->userid){
			return redirect('mailbox');
		}

		//check if the recruitment has not been rejected / accepted / unapplied
		$rec = $interview->recruitment()->first();
		if($rec == null || $rec->status != 'process'){
			return redirect('mailbox/'.$request->tokenid)->with('err', 'You cannot accept this invitation because your job application status is not "Process"');
		}

		$mailboxAppJob = new Mailbox;
		$mailboxAppJob->senderid = $interview->interviewee;
		$mailboxAppJob->recipientid = $interview->interviewer;
		$mailboxAppJob->subject = 'You have new interview schedule.';
		$mailboxAppJob->date = date("Y-m-d");
		$mailboxAppJob->message = $profilesender->name.' accept your interview schedule.';
		$mailboxAppJob->status = 'unseen';
		$mailboxAppJob->type = 'notif';
		$mailboxAppJob->token = '';
		$mailboxAppJob->save();

		//update table interview
		Interview::where('token', $request->tokenid)->update(['status' => 'accept']);
		return redirect('mailbox/'.$request->tokenid);
	}

	// Mailbox Interview invitation
	// for student to send reschedule request
	// routes: /resInterview
	public function updateRescheduleInterviewStudent(Request $request)
	{
		$max_reschedule = 2;

		//Kasih notif
		$interview = Interview::where('token', '=', $request->input('tokenid'))->first();

		$profilesender = Student::where('studentid', '=', Auth::user()->userid)->first();

		if($interview->interviewee != Auth::user()->userid){
			return redirect('mailbox');
		}

		//check if the recruitment has not been rejected / accepted / unapplied
		$rec = $interview->recruitment()->first();
		if($rec == null || $rec->status != 'process'){
			return redirect('mailbox/'.$request->tokenid)->with('err', 'You cannot reschedule this invitation because your job application status is not "Process"');
		}

		//get how many interview has been rescheduled
		$rescheduled_count = Interview::where('recruitment_id', '=', $interview->recruitment_id)
			->where('status', '=', 'reschedule')
			->count();

		if($rescheduled_count < $max_reschedule){
			$mailboxAppJob = new Mailbox;
			$mailboxAppJob->senderid = $interview->interviewee;
			$mailboxAppJob->recipientid = $interview->interviewer;
			$mailboxAppJob->subject = 'You have new interview schedule.';
			$mailboxAppJob->date = date("Y-m-d");
			$mailboxAppJob->message = $profilesender->name.' want reschedule your interview invitation.';
			$mailboxAppJob->status = 'unseen';
			$mailboxAppJob->type = 'notif';
			$mailboxAppJob->token = '';
			$mailboxAppJob->save();

			Interview::where('token', $request->tokenid)
				->update(['status' => 'reschedule']);

    		$lastData = Interview::where('token', '=', $request->tokenid)->first();

    		return redirect('/status')->with('suksesprioritas', 'You have successfully send reschedule request!');
		}
		else
		{
			return redirect('/status')->with('gagalNih', 'Cannot reschedule more than '.$max_reschedule.' times!');
		}
	}

	// Home
	// for company to export the accepted interview data to excel file
	// routes: /interviewD
	public function exportAcceptedCompany()
	{
		$data = Interview::join('students as s', 's.studentid', '=', 'interviews.interviewee')
			->where('interviews.interviewer', '=', Auth::user()->userid)
			->where('interviews.status','=','accept')
			->whereNull('s.deleted_at')
			->select('s.name as name', 'interviews.interviewee as NIM', 'interviews.date', 'interviews.time', 'interviews.location')
			->get();

		\Excel::create('Interview Schedule', function($excel) use($data)
			{
				$excel->sheet('Schedule', function($sheet) use($data)
				{
					// first row styling and writing content
					$sheet->mergeCells('A1:E1');
					$sheet->row(1, function ($row) {
				            $row->setFontFamily('Comic Sans MS');
				            $row->setFontSize(18);
				            $row->setAlignment('center');
				            $row->setValignment('center');
		        		});

	        		$sheet->row(1, array('Interview Schedule'));

	        		$sheet->cell('A3', function($cell)
					{
	   					$cell->setValue('NIM');
	   					$cell->setAlignment('center');
	   					$cell->setBorder('thin', 'thin', 'thin', 'thin');
	   				});
					$sheet->cell('B3', function($cell)
					{
	   					$cell->setValue('Name');
	   					$cell->setAlignment('center');
	   					$cell->setBorder('thin', 'thin', 'thin', 'thin');
	   				});
					$sheet->cell('C3', function($cell)
					{
	   					$cell->setValue('Date');
	   					$cell->setAlignment('center');
	   					$cell->setBorder('thin', 'thin', 'thin', 'thin');
	   				});
	   				$sheet->cell('D3', function($cell)
					{
	   					$cell->setValue('Time');
	   					$cell->setAlignment('center');
	   					$cell->setBorder('thin', 'thin', 'thin', 'thin');
	   				});
	   				$sheet->cell('E3', function($cell)
					{
	   					$cell->setValue('Location');
	   					$cell->setAlignment('center');
	   					$cell->setBorder('thin', 'thin', 'thin', 'thin');
	   				});

			        		// second row styling and writing content
					$sheet->row(3, function ($row)
					{
					    // call cell manipulation methods
					    $row->setFontFamily('Comic Sans MS');
					    $row->setFontSize(12);
					    $row->setAlignment('center');
					});

					$counter=4;
			        		foreach($data as $record)
					{
						$sheet->cell('A'.$counter, function($cell)use($record)
						{
							 $cell->setValue($record->NIM);
							 $cell->setAlignment('center');
							 $cell->setBorder('thin', 'thin', 'thin', 'thin');
						});
						$sheet->cell('B'.$counter, function($cell)use($record)
						{
							 $cell->setValue($record->name);
							 $cell->setAlignment('center');
							 $cell->setBorder('thin', 'thin', 'thin', 'thin');
						});
						$sheet->cell('C'.$counter, function($cell)use($record)
						{
							 $date=date_create($record->date);
							 $cell->setValue(date_format($date, "d F Y"));
							 $cell->setAlignment('center');
							 $cell->setBorder('thin', 'thin', 'thin', 'thin');
						});
						$sheet->cell('D'.$counter, function($cell)use($record)
						{
							 $cell->setValue($record->time);
							 $cell->setAlignment('center');
							 $cell->setBorder('thin', 'thin', 'thin', 'thin');
						});
						$sheet->cell('E'.$counter, function($cell)use($record)
						{
							 $cell->setValue($record->location);
							 $cell->setAlignment('center');
							 $cell->setBorder('thin', 'thin', 'thin', 'thin');
						});
						$counter++;
					}
	        			});
			})->export('xlsx');
	}
}
?>
