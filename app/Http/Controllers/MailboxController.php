<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Http\Request;
use Validator;
use Hash;
use Auth;
use DB;
use Carbon\Carbon;
use App\User;
use App\Job;
use App\Mailbox;
use App\Company;
use App\Interview;
use App\Recruitment;
use App\Student;
use App\Services\EmailService;


class MailboxController extends BaseController
{
	// cek apakah student sudah di accept atau belum
	public function checkAccepted()
	{
		$rec = Recruitment::where('studentid','=', Auth::user()->userid)
			->where('status','=','approved')
			->first();

		if($rec != null){
			return 'accepted';
		}
		else{
			return "no";
		}
	}

	// mailbox page
	// routes: /mailbox
	public function indexMessage()
	{
		if (Auth::user()->role == 'company'){
			$getState = Company::where('companyid', '=', Auth::user()->userid)
				->first();
			$getState = $getState->state;
		}
		else{
			$getState = 'bebas';
		}

		//$objUser = new User;
		$rolenyaNih = User::where('userid', '=', Auth::user()->userid)
			->select('role')
			->first();
		if ($rolenyaNih->role == 'student')
		{
			$data = Mailbox::leftJoin('companies as c', 'mailboxes.senderid', '=', 'c.companyid')
				->whereNull('c.deleted_at')
				->where('recipientid', '=', Auth::user()->userid)
				->whereIn('type', array('mailbox', 'interview'))
				->select('mailboxes.created_at','mailboxes.date','mailboxes.id','mailboxes.message','mailboxes.recipientid','mailboxes.senderid','mailboxes.status','mailboxes.subject','mailboxes.token','mailboxes.type','mailboxes.updated_at', 'c.name')->orderBy('date', 'desc')
				->get();

			$dataSent = Mailbox::leftJoin('companies as c', 'mailboxes.recipientid', '=', 'c.companyid')
				->whereNull('c.deleted_at')
				->where('senderid', '=', Auth::user()->userid)
				->whereIn('type', array('mailbox', 'interview'))
				->select('mailboxes.created_at','mailboxes.date','mailboxes.id','mailboxes.message','mailboxes.recipientid','mailboxes.senderid','mailboxes.status','mailboxes.subject','mailboxes.token','mailboxes.type','mailboxes.updated_at', 'c.name')
				->orderBy('date', 'desc')
				->get();
		}
		else
		{
			$data = Mailbox::where('recipientid', '=', Auth::user()->userid)
				->whereIn('type', array('mailbox', 'interview'))
				->select('mailboxes.date','mailboxes.message','mailboxes.recipientid','mailboxes.senderid as name','mailboxes.status','mailboxes.subject','mailboxes.token','mailboxes.type')
				->orderBy('date', 'desc')
				->get();
			$dataSent = Mailbox::where('senderid', '=', Auth::user()->userid)
				->whereIn('type', array('mailbox', 'interview'))
				->select('mailboxes.date','mailboxes.message','mailboxes.recipientid as name','mailboxes.senderid','mailboxes.status','mailboxes.subject','mailboxes.token','mailboxes.type')
				->orderBy('date', 'desc')
				->get();
		}

		$mailboxCount = Mailbox::whereIn('type', array('interview', 'mailbox'))
			->where('status', '=', 'unseen')
			->where('recipientid', '=', Auth::user()->userid)
			->count();

		$accepted = "";
		if(Auth::user()->role == 'student'){
			$accepted = $this->checkAccepted();
		}

		return view('others.mail',
			[
				'accepted' => $accepted,
				'inboxs' => $data,
				'sents' => $dataSent,
				'mailboxCount' => $mailboxCount,
				'state' => $getState,
				'notifCount' => app('App\Http\Controllers\MailboxController')->getCountNotification(),
				'notif' => app('App\Http\Controllers\MailboxController')->getLatestNotification()
			]);
	}

	// see detail of mailbox
	// routes: /mailbox/
	public function detailInboxMessage($id)
	{
		if (Auth::user()->role == 'company'){
			$getState = Company::where('companyid', '=', Auth::user()->userid)
				->first();
			$getState = $getState->state;
		}
		else{
			$getState = 'bebas';
		}

		Mailbox::where('token', '=', $id)
			->where('recipientid', Auth::user()->userid)
			->update(['status' => 'seen']);

		$data = Mailbox::join('users', 'users.userid', '=', 'mailboxes.senderid')
			->whereNull('users.deleted_at')
			->where('token', '=', $id)
			->where('mailboxes.recipientid', '=', Auth::user()->userid)
			->select('role') // sender's role
			->first();

		if($data == null){
			return redirect('/mailbox');
		}

		if ($data->role == 'company')
		{
			$data = Mailbox::join('companies', 'mailboxes.senderid', '=', 'companies.companyid')
				->whereNull('companies.deleted_at')
				->where('token', '=', $id)
				->first();
		}
		else if ($data->role == 'student')
		{
			$data = Mailbox::join('students', 'mailboxes.senderid', '=', 'students.studentid')
				->whereNull('students.deleted_at')
				->where('token', '=', $id)
				->first();
		}
		else if ($data->role == 'department')
		{
			$data = Mailbox::join('departments', 'mailboxes.senderid', '=', 'departments.departmentid')
				->whereNull('departments.deleted_at')
				->where('token', '=', $id)
				->first();
		}

		$mailboxCount = Mailbox::where('type', '=', 'mailbox')
			->where('status', '=', 'unseen')
			->where('recipientid', '=', Auth::user()->userid)
			->count();

		$dataInterview = Interview::join('students as s', 's.studentid', '=', 'interviews.interviewee')
			->whereNull('s.deleted_at')
			->where('interviews.id', '=', $data->interview_id)
			->select('interviews.pic', 'interviews.phone','interviews.location', 'interviews.time', 'interviews.date', 's.name', 'interviews.token', 'interviews.status', 'interviews.recruitment_id')
			->first();

		$rescheduled_count = null;
		$company_user_status = null;

		if ($data->type != 'mailbox'){ // type = interview invitation
			$jobid = Recruitment::where('id', '=', $dataInterview->recruitment_id)
				->select('jobid')
				->first();
			$job = Job::where('id', '=', $jobid->jobid)
				->first();
			$rescheduled_count = Interview::where('recruitment_id', '=', $dataInterview->recruitment_id)
				->where('status', '=', 'reschedule')
				->get()
				->count();
			$company_user_status = $job->company()->first()->user()->first()->status;

			$rec = Recruitment::where('id', '=', $dataInterview->recruitment_id)->first();

			if($rec == null){ // recruitment is deleted (unapplied)
				$data->status_recruitment = 'unapplied';
			}
			else{
				$data->status_recruitment = $rec->status;
			}
		}
		else{
			$job = null;
		}

		$accepted = "";
		if(Auth::user()->role == 'student'){
			$accepted = $this->checkAccepted();
		}

		return view('others.mailbox',
			[
				'accepted' => $accepted,
				'data' => $data,
				'mailboxCount' => $mailboxCount,
				'state' => $getState,
				'notifCount' => app('App\Http\Controllers\MailboxController')->getCountNotification(),
				'notif' => app('App\Http\Controllers\MailboxController')->getLatestNotification(),
				'interview' => $dataInterview,
				'job' => $job,
				'rescheduled_count' => $rescheduled_count,
				'max_reschedule' => 2,
				'company_user_status' => $company_user_status
			]);
	}

	// open sent message detail
	// routes: /mailboxs/
	public function detailSentMessage($id)
	{
		if (Auth::user()->role == 'company'){
			$getState = Company::where('companyid', '=', Auth::user()->userid)
				->first();
			$getState = $getState->state;
		}
		else{
			$getState = 'bebas';
		}

		$data = Mailbox::join('users', 'users.userid', '=', 'mailboxes.recipientid')
			->whereNull('users.deleted_at')
			->where('token', '=', $id)
			->select('role')
			->first();

		if ($data->role == 'company')
		{
			$data = Mailbox::join('companies', 'mailboxes.recipientid', '=', 'companies.companyid')
				->whereNull('companies.deleted_at')
				->where('token', '=', $id)
				->first();
		}
		else if ($data->role == 'student')
		{
			$data = Mailbox::join('students', 'mailboxes.recipientid', '=', 'students.studentid')
				->whereNull('students.deleted_at')
				->where('token', '=', $id)
				->first();
		}
		else if ($data->role == 'department')
		{
			$data = Mailbox::join('departments', 'mailboxes.recipientid', '=', 'departments.departmentid')
				->whereNull('departments.deleted_at')
				->where('token', '=', $id)
				->first();
		}

		$mailboxCount = Mailbox::where('type', '=', 'mailbox')
			->where('status', '=', 'unseen')
			->where('recipientid', '=', Auth::user()->userid)
			->count();

		$dataInterview = Interview::join('students as s', 's.studentid', '=', 'interviews.interviewee')
			->whereNull('s.deleted_at')
			->where('token', '=', $data->token)
			->select('interviews.pic', 'interviews.phone','interviews.location', 'interviews.time', 'interviews.date', 's.name', 'interviews.token', 'interviews.status','interviews.recruitment_id as recruitment_id')
			->first();


		if ($data->type != 'mailbox')
		{
			$jobid = Recruitment::where('id', '=', $dataInterview->recruitment_id)
				->select('jobid')
				->first();
			$namajob = Job::where('id', '=', $jobid->jobid)
				->select('name as jobname')
				->first();
		}
		else
		{
			$namajob = Job::select('name as jobname')->first();;
		}

		$accepted = "";
		if(Auth::user()->role == 'student')
		{
			$accepted = $this->checkAccepted();
		}

		return view('others.mailbox',
			[
				'accepted' => $accepted,
				'data' => $data,
				'mailboxCount' => $mailboxCount,
				'state' => $getState,
				'notifCount' => app('App\Http\Controllers\MailboxController')->getCountNotification(),
				'notif' => app('App\Http\Controllers\MailboxController')->getLatestNotification(),
				'interview' => $dataInterview,
				'namajob' => $namajob,
				'inisent' => 'ada'
			]);
	}

	// create new message page
	// routes: /create
	public function sendMessage(Request $request)
	{
		if (Auth::user()->role == 'company'){
			$getState = Company::where('companyid', '=', Auth::user()->userid)
				->first();
			$getState = $getState->state;
		}
		else{
			$getState = 'bebas';
		}

		$to_studentid = null;

		if (Auth::user()->role == 'company')
		{
			$data = Student::select('studentid as userid', DB::raw('CONCAT(studentid, " - ", name) as name'))
				->orderBy('name', 'asc')
				->get();

			$to_studentid = $request->get('studentid');
		}
		else if (Auth::user()->role == 'student')
		{
			$data = Company::join('users', 'users.userid', '=', 'companies.companyid')
				->whereNull('users.deleted_at')
				->where('users.status', '=', 'active')
				->orderBy('name', 'asc')
				->select('companyid as userid', 'name')
				->get();
		}
		else if (Auth::user()->role == 'department')
		{
			$data1 = Company::select('companyid as userid', 'name')
				->orderBy('name', 'asc');
			$data = Student::select('studentid as userid', DB::raw('CONCAT(studentid, " - ", name) as name'))
				->orderBy('name', 'asc')
				->union($data1)
				->get();
		}

		$mailboxCount = Mailbox::where('type', '=', 'mailbox')
			->where('status', '=', 'unseen')
			->where('recipientid', '=', Auth::user()->userid)
			->count();

		$accepted = "";
		if(Auth::user()->role == 'student'){
			$accepted = $this->checkAccepted();
		}

		return view('others.reply',
			[
				'accepted' => $accepted,
				'data' => $data,
				'for' => 'create',
				 'mailboxCount' => $mailboxCount,
				 'state' => $getState,
				 'notifCount' => app('App\Http\Controllers\MailboxController')->getCountNotification(),
				 'notif' => app('App\Http\Controllers\MailboxController')->getLatestNotification(),
				 'to_studentid' => $to_studentid
			]);
	}

	// create new message page with recipient & subject information
	// routes: /reply/
	public function replyMessage($id, $token)
	{
		if (Auth::user()->role == 'company'){
			$getState = Company::where('companyid', '=', Auth::user()->userid)
				->first();
			$getState = $getState->state;
		}
		else{
			$getState = 'bebas';
		}

		$data = User::where('userid', '=', $id)
			->select('role', 'status')
			->first();

		if($data == null || $data->status == 'nonactive' || Mailbox::where('token', '=', $token)->first()->recipientid != Auth::user()->userid){
			return redirect('mailbox');
		}

		if ($data->role == 'company')
		{
			$data = User::join('companies', 'companies.companyid', '=', 'users.userid')
				->whereNull('companies.deleted_at')
				->where('role', '=', $data->role)
				->where('companyid', '=', $id)
				->first();
		}
		else if ($data->role == 'student'){
			$data = User::join('students', 'students.studentid', '=', 'users.userid')
				->whereNull('students.deleted_at')
				->where('role', '=', $data->role)
				->where('studentid', '=', $id)
				->first();
		}
		else if ($data->role == 'department'){
			$data = User::join('departments', 'departments.departmentid', '=', 'users.userid')
				->whereNull('departments.deleted_at')
				->where('role', '=', $data->role)
				->where('departmentid', '=', $id)
				->first();
		}

		$data->subject = Mailbox::where('token', '=', $token)->first()->subject;

		$mailboxCount = Mailbox::where('type', '=', 'mailbox')
			->where('status', '=', 'unseen')
			->where('recipientid', '=', Auth::user()->userid)
			->count();

		//Cek apakah bila student, udah di approve atau belum
		$accepted = "";
		if(Auth::user()->role == 'student')
		{
			$accepted = $this->checkAccepted();
		}

		return view('others.reply',
			[
				'accepted' => $accepted,
				'data' => $data,
				'for' => 'reply',
				'mailboxCount' => $mailboxCount,
				'state' => $getState,
				'notifCount' => app('App\Http\Controllers\MailboxController')->getCountNotification(),
				'notif' => app('App\Http\Controllers\MailboxController')->getLatestNotification(),
				'token_reply' => $token
			]);
	}

	// submit the reply / create message
	// routes: /sendMail
	public function insertSendMessage(Request $request)
	{
		$validator = Validator::make($request->all(),
		[
            'txtSubject' => 'required|max:150',
            'txtMessage' => 'required|max:1000',
		]);

		if ($validator->fails())
		{
			if(!empty($request->token_reply)){
				return redirect('reply/'.$request->recipient.'/'.$request->token_reply)
	                ->withErrors($validator)
	                ->withInput();
			}
			else{
				return redirect('create')
	                ->withErrors($validator)
	                ->withInput();
			}
		}

		$recipient_type = null;

		if(Auth::user()->role == 'student'){
			// check if the company is active or not
			$company_user = User::where('userid', '=', $request->recipient)
				->first();
			if($company_user == null || $company_user->status == 'nonactive'){
				if(!empty($request->token_reply)){
					return redirect('reply/'.$request->recipient.'/'.$request->token_reply)
	                	->with([
							'err' => 'You cannot send message to this recipient'
						])
						->withInput();
				}
				else{
					return redirect('create')
	                	->with([
							'err' => 'You cannot send message to this recipient'
						])
						->withInput();
				}
			}
			else{
				$recipient_type = 'company';
			}
		}
		else if(Auth::user()->role == 'company'){
			// check if student is active or not
			$student_user = User::where('userid', '=', $request->recipient)
				->first();

			if($student_user == null || $student_user->status == 'nonactive'){
				if(!empty($request->token_reply)){
					return redirect('reply/'.$request->recipient.'/'.$request->token_reply)
	                	->with([
							'err' => 'You cannot send message to this recipient'
						])
						->withInput();
				}
				else{
					return redirect('create')
	                	->with([
							'err' => 'You cannot send message to this recipient'
						])
						->withInput();
				}
			}
			else{
				$recipient_type = 'student';
			}
		}

		$insertMailbox = new Mailbox;
		$insertMailbox->senderid = Auth::user()->userid;
		$insertMailbox->recipientid = $request->input('recipient');
		$insertMailbox->subject = $request->input('txtSubject');
		$insertMailbox->date = date("Y-m-d H:i:s");
		$insertMailbox->message = $request->input('txtMessage');
		$insertMailbox->status = 'unseen';
		$insertMailbox->type = 'mailbox';
		$insertMailbox->token = '';
		$insertMailbox->save();

		$token =  Hash::make($insertMailbox->id.$insertMailbox->senderid.$insertMailbox->recipientid);
		$resToken = str_replace('.', 'H', str_replace("$", "S", str_replace("/", "X", $token)));

		Mailbox::where('id', '=', $insertMailbox->id)
			->update(['token' => $resToken]);

		if($recipient_type == 'company'){
			// send email notification
			EmailService::createNewCompanyMessageEmail($insertMailbox);
		}

		return redirect('mailbox');
	}

	// Open notif page
	// routes: /notif
	public function viewNotification()
	{
		if (Auth::user()->role == 'company'){
			$getState = Company::where('companyid', '=', Auth::user()->userid)
				->first();
			$getState = $getState->state;
		}
		else{
			$getState = 'bebas';
		}

		$mailboxCount = Mailbox::where('type', '=', 'mailbox')
			->where('status', '=', 'unseen')
			->where('recipientid', '=', Auth::user()->userid)
			->count();
		$listnotif = Mailbox::where('type', '=', 'notif')
			->where('recipientid', '=', Auth::user()->userid)
			->orderBy('id', 'desc')
			->get();

		//pilih icon berdasarkan gambar
		foreach($listnotif as $ln)
		{
			if(strpos($ln->message, 'apply') !== FALSE)
			{
				$ln->icon = 'user';
			}
			else if(strpos($ln->message, 'interview') !== FALSE)
			{
				$ln->icon = 'calendar';
			}
			else if(strpos($ln->message, 'accepted') !== FALSE)
			{
				$ln->icon = 'checkmark';
			}
			else if(strpos($ln->message, 'rejected') !== FALSE)
			{
				$ln->icon = 'remove';
			}
			else if(strpos($ln->message, 'approved') !== FALSE)
			{
				$ln->icon = 'check circle';
			}
			else
			{
				$ln->icon = 'info';
			}
			//buat jeda waktunya
			$created = new Carbon($ln->created_at);
			$now = Carbon::now();
			$ln->now = $now;
			$ln->create = $created;
			$ln->time = "".$created->diffForHumans($now);
		}

		//Cek apakah bila student, udah di approve atau belum
		$accepted = "";
		if(Auth::user()->role == 'student'){
			$accepted = $this->checkAccepted();
		}

		return view('others.notif',
			[
				'accepted' => $accepted,
				'mailboxCount' => $mailboxCount,
				'state' => $getState,
				'notifCount' => app('App\Http\Controllers\MailboxController')->getCountNotification(),
				'notif' => app('App\Http\Controllers\MailboxController')->getLatestNotification(),
				'listnotif' => $listnotif
			]);
	}

	// Update all notif status to be seen
	// routes: /readNotif
	public function updateReadNotification()
	{
		Mailbox::where('status', '=', 'unseen')
			->where('type', '=', 'notif')
			->where('recipientid', '=', Auth::user()->userid)
			->update(['status' => 'seen']);
    	return "Good";
	}


	// get count of all unseen notification
	public function getCountNotification()
	{
		return Mailbox::where('type', '=', 'notif')
			->where('status', '=', 'unseen')
			->where('recipientid', '=', Auth::user()->userid)
			->count();
	}

	// get last 4 notification
	public function getLatestNotification()
	{
		return Mailbox::where('type', '=', 'notif')
			->where('recipientid', '=', Auth::user()->userid)
			->orderBy('id', 'desc')
			->take(4)
			->get();
	}


}
?>
