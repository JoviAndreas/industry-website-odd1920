<?php

namespace App\Http\Controllers;

use App\Degree;
use App\Faculty;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Validator;

class FacultyController extends BaseController
{

    //testing only method
    public function testing()
    {

        //cara bedain faculty dengan degree
        $check = explode(':', 'faculty:1;2;3:1;2');
        if (count($check) == 1) {
            $degrees = explode(';', $check[0]);
            dd($degrees);
        } else {
            $faculty = explode(';', $check[1]);
            $degree = explode(';', $check[2]);
            dd(['faculty' => $faculty, 'degree' => $degree]);
        }
    }

    //Roles : superadmin, admin faculty
    public function index(Request $request)
    {
        $filter = NULL;
        if ($request->filter !== NULL) {
            $filter = $request->filter;
            if (Faculty::where('id', $filter)->first() == null && $filter != 'all') // if out of bounds
            {
                return redirect()->route('department.faculty');
            }
        }

        if ($checkRole = session('head_of_faculty')) {
            if ($checkRole === 'all') {
                $roleFaculty = 'all';
                $filterForRole = Faculty::all();
            } else {
                //admin faculty
                $roleFaculty = $checkRole;
                if ($filter !== NULL && $filter !== $roleFaculty) { // if accessing another faculty, then is illegal
                    return redirect()->route('department.faculty', ['filter' => NULL]);
                }
                $filterForRole = Faculty::where('id', $roleFaculty)->get();
            }
        } else {
            // HOP
            return redirect('/');
        }

        if ($filter === NULL) { //default filter
            return redirect()->route('department.faculty', ['filter' => $roleFaculty]);
        }

        if ($filter === 'all') {
            return view('superadmin.managefaculty', [
                    'filterForRole' => $filterForRole,
                    'faculties' => Faculty::all(),
                    'degrees' => Degree::all(),
                ]
            );
        } else {
            return view('superadmin.managefaculty', [
                    'filterForRole' => $filterForRole,
                    'faculties' => Faculty::where('id', $filter)->get(),
                    'degrees' => Degree::where('faculty_id', $filter)->get(),
                ]
            );
        }

    }


    // POST
    public function insertFaculty(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'name' => 'required',
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $newFaculty = new Faculty;
        $newFaculty->name = $request->name;
        $newFaculty->save();

        return redirect()->back()->with(['success' => 'add faculty']);
    }

    // GET
    public function editFaculty(Faculty $faculty)
    {
        $degrees = Degree::where('faculty_id', $faculty->id)->get();
        return view('superadmin.editfaculty', [
                'faculty' => $faculty,
                'degrees' => $degrees,
                'unmappedDegrees' => Degree::where('faculty_id', NULL)->get(),
            ]
        );
    }

    // POST
    public function updateFaculty(Request $request, Faculty $faculty)
    {
        $validator = Validator::make($request->all(), [
                'name' => 'required',
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $faculty->name = $request->name;
        $faculty->save();

        $oldDegrees = Degree::where('faculty_id', $faculty->id)->get();
        if ($oldDegrees) {
            foreach ($oldDegrees as $degree) {
                $degree->faculty_id = null;
                $degree->save();
            }
        }
        if ($request->degrees) {
            foreach ($request->degrees as $degreeId) {
                $degree = Degree::where('id', $degreeId)->first();
                $degree->faculty_id = $faculty->id;
                $degree->save();
            }
        }
        if ($request->unmappedDegrees) {
            foreach ($request->unmappedDegrees as $degreeId) {
                $degree = Degree::where('id', $degreeId)->first();
                $degree->faculty_id = $faculty->id;
                $degree->save();
            }
        }
        return redirect()->back()->with(['success' => 'update faculty']);
    }

    // POST
    public function deleteFaculty(Faculty $faculty)
    {
        $facultyName = $faculty->name;
        Degree::where('faculty_id', $faculty->id)->each(function ($degree) {
            $degree->delete();
        });
        $faculty->delete();
        return redirect()->back()->with(['success' => 'delete faculty ' . $facultyName]);
    }

    // POST
    public function insertDegree(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'name' => 'required',
                'faculty' => 'required',
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $newDegree = new Degree;
        $newDegree->name = $request->name;
        $newDegree->faculty_id = $request->faculty;
        $newDegree->save();

        return redirect()->back()->with(['success' => 'add degree']);
    }

    // POST
    public function updateDegree(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'name' => 'required',
                'faculty' => 'required',
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $degree = Degree::where('id', $request->id)->first();
        if ($degree === NULL) {
            return redirect()->back()->withErrors(['degree' => 'ERROR: Degree Not Found, Please Refresh The Browser']);
        }

        $oldName = $degree->name;
        $degree->name = $request->name;
        $degree->faculty_id = $request->faculty;
        $degree->save();

        return redirect()->back()->with(['success' => 'update degree from "' . $oldName . '" to "' . $degree->name . '"']);
    }

    // POST
    public function deleteDegree(Request $request, Degree $degree)
    {
        $degree->delete();
        return redirect()->back()->with(['success' => 'delete degree ' . $degree->name]);
    }
}
?>