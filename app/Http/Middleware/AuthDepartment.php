<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthDepartment
{
    protected $auth;

    public function __construct(Guard $auth){
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if ($this->auth->guest()) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('login'); //Redirect to login page if no auth!
            }
        }

        if (auth()->check()) {
            if (Auth::user()->role == 'company'){
                return redirect('homec');
            }else if (Auth::user()->role == 'student'){
                return redirect('vacancy');
            }
            if (Auth::user()->department->head_of_program == null) {
                Auth::logout();
                return redirect('login')->with('messageError', 'Your Allowed Degrees has not been set, please contact your administrator.');
            }
            return $next($request);
        }
    }
}
