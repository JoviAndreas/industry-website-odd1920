<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Faculty extends Model
{
    use SoftDeletes;

    protected $table = 'faculties';
    protected $dates = ['deleted_at'];

    public function degrees() {
        return $this->hasMany('App\Degree', 'faculty_id', 'id');
    }
}
