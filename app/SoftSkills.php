<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class SoftSkills extends Model
{
     use SoftDeletes;

     protected $table = 'soft_skills';
     protected $dates = ['deleted_at'];
}
?>
