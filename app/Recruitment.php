<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Recruitment extends Model
{
     use SoftDeletes;

     protected $table = 'recruitments';
     protected $dates = ['deleted_at'];

     //eloquent relationship
     public function learning_plan(){
         return $this->hasOne('App\LearningPlan', 'recruitment_id', 'id');
     }

     //eloquent relationship
    public function student(){
        return $this->belongsTo('App\Student', 'studentid', 'studentid');
    }

    //eloquent relationship
    public function job(){
        return $this->belongsTo('App\Job', 'jobid', 'id');
    }

    //eloquent relationship
    public function interview(){
        return $this->hasMany('App\Interview', 'recruitment_id', 'id');
    }

    //eloquent relationship
    public function unaccept_request(){
        return $this->hasMany('App\UnacceptRequest', 'recruitment_id', 'id');
    }

    //eloquent relationship
    public function unreject_request(){
        return $this->hasMany('App\UnrejectRequest', 'recruitment_id', 'id');
    }

    //eloquent relationship
    public function approval_request(){
        return $this->hasMany('App\ApprovalRequest', 'recruitment_id', 'id');
    }

     /*
     * get approved apply dari student tertentu
     * @param $id -> studentid-nya
     * --RR:to be deleted
     */
    public function checkApproved($id)
    {
        return $this->where('studentid','=',$id)
                ->where('status','=','approved')
                ->first();
    }

    //get data recruitment for learningplan
    // --RR: to be updated -> status: accepted
    public function getDataForLP($studentid)
    {
        return $this
                ->join('jobs','jobs.id','=','recruitments.jobid')
                ->join('companies','companies.companyid','=','jobs.companyid')
                ->Where('studentid','=',$studentid)
                ->where('status','approved')
                ->select('jobs.name as jobname','companies.name as companyname')
                ->first();
    }

    //get list total perusahaan apply dari mahasiswa tertentu
    public function getListApplyCompany($studentid)
    {
        return $this
                ->join('jobs','jobs.id','=','recruitments.jobid')
                ->Where('studentid','=',$studentid)
                ->whereIn('status',array('waiting','accepted','process'))
                ->groupBy('companyid')
                ->select('companyid')
                ->get();
    }

    //get data apply
    public function getRecruitment($jobid, $studentid)
    {
          return  $this
                ->where('jobid', '=', $jobid)
                ->where('studentid', '=', $studentid)
                ->first();
    }

    //delete data apply
    public function deleteRecruitment($jobid, $studentid)
    {

        $this->where('jobid', $jobid)
                ->where('studentid', $studentid)
                ->delete();
    }

    //delete data apply by studentid
    public function deleteRecruitmentByStudent($studentid)
    {
        $this
                ->where('studentid', $studentid)
                ->delete();
    }


    //delete all data apply berdasarkan job tertentu
    public function deleteRecruitmentByJob($jobid)
    {
        $this->where('jobid', $jobid)
                ->delete();
    }

    //Delete data by rec id
    public function deleteRecruitmentById($recid)
    {
        $this->where('id', $recid)
                ->delete();
    }

    //get last recruitment data
    public  function getLastRecruitment()
    {
            return $this->orderBy('id', 'desc')->first();
    }

    public function hasJob($studentid){
            return $this->where('studentid' ,'=', $studentid)->where('status', '=', 'accepted')->first();
    }

    public function getUpdatingAccept($id){
        return $this
                ->join('jobs', 'jobs.id', '=', 'recruitments.jobid')
                ->join('companies', 'companies.companyid', '=', 'jobs.companyid')
                ->where('recruitments.token', '=', $id)
                ->select('recruitments.studentid as studentid', 'jobs.name as name')
                ->first();
    }

    public function getUpdatingUnAccept($id){
        return $this
                ->join('jobs', 'jobs.id', '=', 'recruitments.jobid')
                ->join('companies', 'companies.companyid', '=', 'jobs.companyid')
                ->where('recruitments.token', '=', $id)
                ->select('recruitments.studentid as studentid', 'jobs.name as name')
                ->first();
    }

    public function getRecruitmentForWaitingStudents($studentid)
    {
        return $this
                ->join('students', 'students.studentid', '=', 'recruitments.studentid')
                ->join('jobs','jobs.id','=','recruitments.jobid')
                ->join('companies','jobs.companyid','=','companies.companyid')
                ->where('students.studentid',$studentid)
                ->select(
                    'companies.name as companyname',
                    'jobs.name as jobname',
                    'recruitments.updated_at as time',
                    'recruitments.priority as priority',
                    'recruitments.status as status',
                    'jobs.id as jobid',
                    'jobs.description as jobdesc',
                    'recruitments.id as recid')
                ->get();
    }

    public function getApprovedRecruitmenst()
    {
       return $this
                ->where('status','approved')
                ->join('students', 'students.studentid', '=', 'recruitments.studentid')
                ->join('jobs','jobs.id','=','recruitments.jobid')
                ->join('companies','jobs.companyid','=','companies.companyid')
                ->select('recruitments.studentid as studentid','students.name as studentname','companies.name as companyname','jobs.name as jobname','recruitments.updated_at as time','recruitments.priority as priority','recruitments.status as status','cv','jobs.id as jobid','jobs.description as jobdesc','recruitments.id as recid', 'guider')
                ->get();
    }

    public function getApprovedSearchRecruitments($search)
    {
        return $this
                ->where('status','approved')
                ->join('students', 'students.studentid', '=', 'recruitments.studentid')
                ->join('jobs','jobs.id','=','recruitments.jobid')
                ->join('companies','jobs.companyid','=','companies.companyid')
                ->where('students.name', 'like', '%'.$search.'%')
                ->select('recruitments.studentid as studentid','students.name as studentname','companies.name as companyname','jobs.name as jobname','recruitments.updated_at as time','recruitments.priority as priority','recruitments.status as status','cv','jobs.id as jobid','jobs.description as jobdesc','recruitments.id as recid')
                ->get();
    }

    public function getAllRecruitments()
    {
        return $this
                ->join('students', 'students.studentid', '=', 'recruitments.studentid')
                ->select('students.studentid as studentid','students.name as studentname','cv')
                ->distinct()
                ->get();
    }

    public function getAllSearchRecruitmenst($search)
    {
        return $this
                ->join('students', 'students.studentid', '=', 'recruitments.studentid')
                ->select('students.studentid as studentid','students.name as studentname','cv')
                ->where('students.name', 'like', '%'.$search.'%')
                ->distinct()
                ->get();
    }



    public function getAcceptedRecruitments()
    {
            return $this
                    ->whereNotIn
                    ('recruitments.studentid',
                            function ($query)
                            {
                                    $query
                                            ->select('studentid')
                                            ->from('recruitments')
                                            ->whereRaw('status = "approved"');
                            }
                    )
                    ->join('students', 'students.studentid', '=', 'recruitments.studentid')
                    ->where('status','accepted')
                    ->select('students.studentid as studentid','students.name as studentname','cv')
                    ->distinct()
                    ->get();
    }

    public function getAcceptedSearchRecruitmenst($search)
    {
        return $this
            ->whereNotIn
                    ('recruitments.studentid',
                            function ($query)
                            {
                                    $query
                                            ->select('studentid')
                                            ->from('recruitments')
                                            ->whereRaw('status = "approved"');
                            }
                    )
            ->join('students', 'students.studentid', '=', 'recruitments.studentid')
            ->where('status','accepted')
            ->select('students.studentid as studentid','students.name as studentname','cv')
            ->distinct()
            ->where('students.name', 'like', '%'.$search.'%')
            ->get();
    }

    public function getDetailRecruitments($stud)
    {
       return $this
                ->join('students', 'students.studentid', '=', 'recruitments.studentid')
                ->join('jobs','jobs.id','=','recruitments.jobid')
                ->join('companies','jobs.companyid','=','companies.companyid')
                ->where('students.studentid',$stud)
                ->where('status','=','accepted')
                ->select('companies.name as companyname','jobs.name as jobname','recruitments.updated_at as time','recruitments.priority as priority','recruitments.status as status','cv','jobs.id as jobid','jobs.description as jobdesc','recruitments.id as recid')
                ->get();
    }

    public function getAllDetailRecruitments($stud)
    {
       return $this
                ->join('students', 'students.studentid', '=', 'recruitments.studentid')
                ->join('jobs','jobs.id','=','recruitments.jobid')
                ->join('companies','jobs.companyid','=','companies.companyid')
                ->where('students.studentid',$stud)
                ->select('companies.name as companyname','jobs.name as jobname','recruitments.updated_at as time','recruitments.priority as priority','recruitments.status as status','cv','jobs.id as jobid','jobs.description as jobdesc','recruitments.id as recid')
                ->get();
    }



    public function getExportUnapprovedRecruitments()
    {
            return $this
                ->whereNotIn
                        ('recruitments.studentid',
                                function ($query)
                                {
                                        $query
                                                ->select('studentid')
                                                ->from('recruitments')
                                                ->whereRaw('status = "approved"');
                                }
                        )
                ->join('students', 'students.studentid', '=', 'recruitments.studentid')
                ->join('jobs','jobs.id','=','recruitments.jobid')
                ->join('companies','jobs.companyid','=','companies.companyid')
                ->select('recruitments.studentid as studentid','students.name as studentname','companies.name as companyname','jobs.name as jobname','recruitments.updated_at as time','recruitments.priority as priority','recruitments.status as status','cv','jobs.id as jobid','jobs.description as jobdesc','recruitments.id as recid')
                ->get();
    }


    public function getExportApprovedRecruitments()
    {
        return $this
                ->where('status','approved')
                ->join('students', 'students.studentid', '=', 'recruitments.studentid')
                ->join('jobs','jobs.id','=','recruitments.jobid')
                ->join('companies','jobs.companyid','=','companies.companyid')
                ->select('recruitments.studentid as studentid','students.name as studentname','companies.name as companyname','jobs.name as jobname','recruitments.updated_at as time','recruitments.priority as priority','recruitments.status as status','cv','jobs.id as jobid','jobs.description as jobdesc','recruitments.id as recid')
                ->get();
    }

    public function getRecruitmentOnlyJobidByToken($token){
        return $this->where('token', '=', $token)->select('jobid')->first();
    }

}

?>
