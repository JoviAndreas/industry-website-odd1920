<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Job extends Model
{
    use SoftDeletes;

    protected $table = 'jobs';
    protected $dates = ['deleted_at'];

    //eloquent relationship
    public function company()
    {
        return $this->belongsTo('App\Company', 'companyid', 'companyid');
    }

    //eloquent relationship
    public function recruitment()
    {
        return $this->hasMany('App\Recruitment', 'jobid', 'id');
    }

    //eloquent relationship
    public function jobsdegrees()
    {
        return $this->hasMany('App\JobsDegrees', 'jobid', 'id');
    }

    //eloquent relationship
    public function jobsposition()
    {
        return $this->belongsTo('App\JobsPosition', 'job_position', 'name');
    }
    
    //get data job dengan token
    public function getJob($token)
    {
        return $this->where('token', '=', $token)->first();
    }

    //get data job dengan token dan ambil id nya
    public function getJobId($token)
    {
        return $this->where('token', '=', $token)->select('id')->first();
    }

    //get first data job namanya saja
    public function getJobNameWithoutCondition()
    {
        return $this->select('name as jobname')->first();
    }

    //get first data job namanya saja dari company tertentu
    public function getJobNameWithCondition($userid)
    {
        return $this->where('id', '=', $userid)->select('name as jobname')->first();
    }

    //get job dengan data company-nya dengan id company tertentu
    public function getCompanies($userid)
    {
        return $this->join('companies', 'jobs.companyid', '=', 'companies.companyid')
            ->where('jobs.companyid', '=', $userid)
            ->select('jobs.name as position', 'jobs.quota as quota', 'jobs.description as description', 'deadline', 'duration', 'location', 'jobs.linktest as linktest', 'jobs.token as token', 'learningobj', 'softskill')
            ->get();
    }

    //get job dengan data company-nya
    public function getAllCompanies()
    {
        return $this
            ->join('companies', 'jobs.companyid', '=', 'companies.companyid')
            ->select('jobs.name as position', 'jobs.quota as quota', 'jobs.description as description', 'deadline', 'duration', 'location', 'jobs.linktest as linktest', 'jobs.token as token', 'companies.name as companyname', 'learningobj', 'softskill')
            ->get();
    }

    //Hapus job berdasarkan tokennya
    //Cascade dilanjutkan dengan hapus recruitments
    public function deleteJob($tokenid)
    {
        $job = $this->where('token', $tokenid)->first();
        $rec = new Recruitment;
        $rec->deleteRecruitmentByJob($job->id);
        $job->delete();
    }

    //Hapus job berdasarkan id
    //Cascade dilanjutkan dengan hapus recruitments
    public function deleteJobByJobid($jobid)
    {
        $job = $this->where('jobid', $jobid)->first();
        $rec = new Recruitment;
        $rec->deleteRecruitmentByJob($job->id);
        $job->delete();
    }

}
