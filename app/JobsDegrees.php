<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class JobsDegrees extends Model
{

     use SoftDeletes;

     protected $table = 'jobsdegrees';
     protected $dates = ['deleted_at'];

    //eloquent relationship
    public function degree(){
        return $this->hasOne('App\Degree', 'id', 'degreeid');
    }
}

?>