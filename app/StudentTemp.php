<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentTemp extends Model
{
    protected $table = 'students_temp';
}
