<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cvdateinformation extends Model
{
	use SoftDeletes;

	protected $table = 'cvdateinformations';
    	protected $dates = ['deleted_at'];

    	//eloquent relationship
    	public function student(){
    		return $this->belongsTo('App\Student', 'studentid', 'studentid');
    	}

    	//Get semua data cv dates berdasarkan type nya(Formal, informal, org, Award)
	public function getCvDates($type, $studentid)
	{
		return $this
			->where('studentid', '=', $studentid)
			->where('type','=',$type)
			->get();
	}

	//Hapus semua data cvdates dari student tertentu (soft)
	public function deleteCvDates($studentid)
	{
		$this->where('studentid', $studentid)->delete();
	}
}

?>