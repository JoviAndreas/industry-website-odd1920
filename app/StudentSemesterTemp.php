<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentSemesterTemp extends Model
{
    protected $table = 'studentsemesters_temp';
}
