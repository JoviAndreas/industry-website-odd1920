<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Auth;

class Company extends Model
{
     use SoftDeletes;
     protected $table = 'companies';
     protected $dates = ['deleted_at'];

    public static function isBippByUserId($userId) {
        $bipp = Company::where('companyid', $userId)->first()->bipp_id ?? null;
        if ($bipp == null) {
            return false;
        }
        return true;
    }

     public static function isBipp() {
         $bipp = Company::where('companyid', Auth::user()->userid)->first()->bipp_id ?? null;
         if ($bipp == null) {
             return false;
         }
         return true;
     }

     //get BIPP Minimum IPK
     public static function getBippIpk() {
         $currCompany = Company::where('companyid', Auth::user()->userid)->first();
         if ($currCompany->bipp == null) {
             return null;
         }
         return $currCompany->bipp->min_ipk;
     }

     //eloquent relationship
    public function job(){
        return $this->hasMany('App\Job', 'companyid', 'companyid');
    }

    //eloquent relationship
    public function supervisor(){
        return $this->hasMany('App\Supervisor', 'companyid', 'companyid');
    }

    //eloquent relationship
    public function interview(){
        return $this->hasMany('App\Interview', 'interviewer', 'companyid');
    }

    //eloquent relationship
    public function mailbox_recipient(){
        return $this->hasMany('App\Mailbox', 'recipientid', 'companyid');
    }

    //eloquent relationship
    public function mailbox_sender(){
        return $this->hasMany('App\Mailbox', 'senderid', 'companyid');
    }

    //eloquent relationship
    public function user(){
        return $this->hasOne('App\User', 'userid', 'companyid');
    }

    //eloquent relationship
    public function bipp(){
        return $this->hasOne('App\Bipp','id','bipp_id');
    }

    //Get all open companies
    public static function getAllOpenCompanies(){
        return Company::where('state','=','open')->get();
    }

    // TO BE DELETED
    //Get all open companies with jobs
    public static function getAllOpenCompaniesWithJob(){
        return Company::join('jobs', 'jobs.companyid', '=', 'companies.companyid')
            ->whereNull('jobs.deleted_at')
            ->where('state','=','open')
            ->where('jobs.quota', '>', '0')
            ->select('companies.name as name', 'companies.companyid')
            ->groupBy('companies.companyid')
            ->orderBy('companies.id')
            ->get();
    }

    // TO BE DELETED
    public static function getAllOpenCompaniesWithJobWithDegree($degreeid){
        return Company::join('jobs', 'jobs.companyid', '=', 'companies.companyid')
            ->join('users', 'users.userid', '=', 'companies.companyid')
            ->whereNull('jobs.deleted_at')
            ->whereNull('users.deleted_at')
            ->where('state','=','open')
            ->where('jobs.quota', '>', 0)
            ->where('users.status', '=', 'active')
            ->whereIn('jobs.id', JobsDegrees::where('degreeid', '=', $degreeid)->select('jobid')->get()->toArray())
            ->select('companies.name as name', 'companies.companyid')
            ->groupBy('companies.companyid')
            ->orderBy('companies.id')
            ->get();
    }

    //get list job dari company yang masih available (quotanya > 0)
    public function getAvailableJobs()
    {
        return Job::where('companyid', '=', $this->companyid)->where('quota','>','0')->get();
    }

    // TO BE DELETED
    //get list job dari company yang masih available (quotanya > 0) dan sesuai dengan degree yang diinput
    public function getAvailableJobsWithDegree($degreeid)
    {
        return Job::where('companyid', '=', $this->companyid)
            ->where('quota','>','0')
            ->whereIn('jobs.id',
                JobsDegrees::where('degreeid', '=', $degreeid)
                ->select('jobid')
                ->get()
                ->toArray())
            ->get();
    }





     //Get list companies yang sudah menerima student
     public function getAcceptedCompanies()
     {
            return $this
    		->join('jobs','jobs.companyid','=','companies.companyid')
    		->join('recruitments','jobs.id','=','recruitments.jobid')
    		->where('status','=',"approved")
    		->orWhere('status','=',"accepted")
    		->select('companies.companyid','pic','address','companies.name','email','phone','website')
    		->distinct()
    		->get();
     }

     //Get list companies yang belum menerima student tapi sudah memproses
     public function getProcessedCompanies()
     {
     	return $this
		->join('jobs','jobs.companyid','=','companies.companyid')
		->join('recruitments','jobs.id','=','recruitments.jobid')
		->whereRaw('status = "process" or status = "reject"')
		->whereNotIn('jobs.id',
            		function ($query)
            		{
            			$query
            			->select('jobid')
            			->from('recruitments')
            			->whereRaw('status = "approved" or status = "accepted"');
            		})
		->select('companies.companyid','pic','address','companies.name','email','phone','website')
		->distinct()
		->get();
     }

      //Get list companies yang semua appliances nya masih waiting
     public function getWaitingCompanies()
     {
     	return $this
		->join('jobs','jobs.companyid','=','companies.companyid')
		->join('recruitments','jobs.id','=','recruitments.jobid')
		->where('status','=',"waiting")
		->whereNotIn('companies.companyid',
			function ($query)
			{
				$query
				->select('companyid')
				->from('jobs as j')
				->join('recruitments as r','r.jobid','=','j.id')
				->whereRaw('status = "approved" or status = "accepted" or status = "process"');
			})
		->select('companies.companyid','pic','address','companies.name','email','phone','website')
		->distinct()
		->get();
     }


    //Get list companies yang tidak memiliki job
     public function getNoJobCompanies()
     {
     	return $this
            	->leftJoin('jobs','jobs.companyid','=','companies.companyid')
            	->whereRaw('jobs.companyid is NULL')
            	->select('companies.companyid','pic','address','companies.name','email','phone','website')
            	->distinct()
            	->get();
    }


    //Get list companies dimana job yang dimasukan ,tidak ada yang meng-apply
     public function getNoApplyCompanies()
     {
     	return $this
		->join('jobs','jobs.companyid','=','companies.companyid')
		->whereNotIn('companies.companyid',
            		function ($query)
            		{
            			$query
            			->select('companyid')
            			->from('jobs as j')
            			->join('recruitments as r','r.jobid','=','j.id');
            		})
		->select('companies.companyid','pic','address','companies.name','email','phone','website')
		->distinct()
		->get();
     }

     //Delete company
     //Cascade delete in table Interview, job, recruitment, company + User
     public function deleteCompany($companyid)
     {
            $job = Job::where('companyid','=',$companyid)->get();

            //Delete in table interview
            $intv = new Interview;
            $intv->deleteInterviewCompany($companyid);


            //Delete in job (recruitment + job table)
            foreach($job as $j)
            {
                    $deleteJob = new Job;
                    $deleteJob->deleteJobByJobid($j->id);
            }

            //Delete in table company
            $this->where('companyid', $companyid)->delete();

            //Delete in table User
            $usr = new User;
            $user->deleteUser($companyid);
     }


     //Get list companies yang mengandung nama tertentu
     public function getSearchCompanies($searchWord)
     {
	return $this->where('companies.name', 'like', '%'.$searchWord.'%')->where('state','=','open')->get();
     }

     //Get list companies account untuk di export oleh jurusan
     public function getCompaniesExported()
     {
     	return $this->select('name as Company Name','companyid as Userid','defaultpassword as Password')
                    ->join('users', 'companyid', '=', 'users.userid')->get();
     }

     //get data user company
     //RR:to be deleted
     //public function getUser()
     //{
	//return $this->join('users', 'companyid', '=', 'users.userid')->get();
     //}

     //get data company dan department untuk recipient pesan
     public function unionCompanyandDepartment()
     {
     	$first = Department::all()->select('departmentid as userid', 'name');
	return $this->all()->select('companyid as userid', 'name')->union($first);
     }

     // get data company dengan nama lengkap di sort ascending
     public function getFullNameCompanyASC(){
    	return $this->select('companyid as userid', 'name')->orderBy('name', 'asc')->get();
     }

     // get data company dan student dengan nama lengkap di sort ascending
     public function getFullNameCompanyASCUnionStudent()
     {
    	$data1 = $this->select('companyid as userid', 'name')->orderBy('name', 'asc');
    	return Student::select('studentid as userid', DB::raw('CONCAT(studentid, " - ", name) as name'))
                    ->orderBy('name', 'asc')
                    ->union($data1)
                    ->get();
     }
}

?>
