<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Department extends Model
{
    use SoftDeletes;

    protected $table = 'departments';
    protected $dates = ['deleted_at'];

    //eloquent relationship
    public function user(){
        return $this->hasOne('App\User', 'userid', 'departmentid');
    }

    public static function getAllAccessRoles() {
        return [
            'superadmin' => 'Super Admin',
            'admin' => 'Admin',
            'dean' => 'Dean',
            'hop' => 'Head of Program',
            'epc' => 'EPC',
            'ssc' => 'SSC',
        ];
    }

    public static function isSuperAdmin()
    {
        $department = Auth::user()->department;
        if ($department->access_role != 'superadmin') {
            return false;
        }
        return true;
    }

    public static function isAdmin()
    {
        $department = Auth::user()->department;
        if ($department->access_role != 'admin') {
            return false;
        }
        return true;
    }

    public static function isDean()
    {
        $department = Auth::user()->department;
        if ($department->access_role != 'dean') {
            return false;
        }
        return true;
    }

    public static function isHOP()
    {
        $department = Auth::user()->department;
        if ($department->access_role != 'hop') {
            return false;
        }
        return true;
    }

    public static function isEPC()
    {
        $department = Auth::user()->department;
        if ($department->access_role != 'epc') {
            return false;
        }
        return true;
    }

    public static function isSSC()
    {
        $department = Auth::user()->department;
        if ($department->access_role != 'ssc') {
            return false;
        }
        return true;
    }

    public static function generateRoleSession() {
        return Department::getRoleFor(Auth::user()->department()->first());
    }

    public static function getRoleFor(Department $d)
    {
        $hop = $d->head_of_program;
        $head_of_faculty = null;
        $head_of_program = null;
        $faculties_can_be_chosen = null;
        $degrees_can_be_chosen = null;

        if ($hop != null) {
            if ($hop == 'all') {
                $head_of_faculty = 'all';
                $head_of_program = 'all';
                $faculties_can_be_chosen = Faculty::get();
                $degrees_can_be_chosen = Degree::get();
            } else {
                $checkRole = explode(':', $hop);
                if (count($checkRole) === 1) { //hop
                    $arr_hop = explode(';', $checkRole[0]);
                    $head_of_faculty = null;
                    $head_of_program = $arr_hop[0];
                    $degrees_can_be_chosen = Degree::whereIn('id', $arr_hop)->get();
                    $faculties_can_be_chosen = Faculty::whereIn('id', $degrees_can_be_chosen->pluck('faculty_id'))->get();
                } else if ($checkRole[0] === 'faculty') { //admin faculty
                    $faculty = explode(';', $checkRole[1])[0]; //id
                    $head_of_faculty = $faculty;
                    $head_of_program = Degree::where('faculty_id', $faculty)->first()->id;
                    $degrees_can_be_chosen = Degree::where('faculty_id', $faculty)->get();
                    $faculties_can_be_chosen = Faculty::where('id', $faculty)->get();
                } else { //error
                    dd(['ERROR' => 'PLEASE CONTACT YOUR SYSTEM ADMINISTRATOR']);
                }
            }
        }

        return [
            'head_of_faculty' => $head_of_faculty,
            'head_of_program' => $head_of_program,
            'degrees_can_be_chosen' => $degrees_can_be_chosen,
            'faculties_can_be_chosen' => $faculties_can_be_chosen,
        ];
    }
}

?>