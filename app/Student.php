<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Student extends Model
{
    use SoftDeletes;

    protected $table = 'students';
    protected $dates = ['deleted_at'];

    // TODO : filter Campus with local scope (
    public function scopeFilterCampus($query)
    {
        /*
         * NOTE : can't filter with global scope, because middleware hasn't run the moment global scope is applied
         * HOW TO USE : Add filterCampus() on every Student query
         * EXAMPLE :
         *     Student::filterCampus()->get();
         */

        $user = Auth::user();

        if ($user->role == 'department') {
            if ($user->department->campus == null) {
                return $query;
            } else {
                return $query->where('campus', '=', $user->department->campus);
            }
        }

        return $query;
    }

    public function scopeFilterInternship($query) {
        return $query->where('track', 'internship');
    }

    //eloquent relationship
    public function degree(){
        return $this->belongsTo('App\Degree', 'degreeid', 'id');
    }

    public function program() {
        return $this->hasOne('App\Degree', 'id', 'degreeid');
    }

    //eloquent relationship
    public function interview(){
        return $this->hasMany('App\Interview', 'interviewee', 'studentid');
    }

    //eloquent relationship
    public function user(){
        return $this->hasOne('App\User', 'userid', 'studentid');
    }

    //eloquent relationship
    public function supervisor(){
        return $this->belongsTo('App\Supervisor', 'id', 'supervisorid');
    }

    //eloquent relationship
    public function cvdateinformation(){
        return $this->hasMany('App\Cvdateinformation', 'studentid', 'studentid');
    }

    //eloquent relationship
    public function cvskillinformation(){
        return $this->hasMany('App\Cvskillinformation', 'studentid', 'studentid');
    }

    //eloquent relationship
    public function requirement(){
        return $this->hasMany('App\Requirement', 'studentid', 'studentid');
    }

    //eloquent relationship
    public function studentscore(){
        return $this->hasMany('App\Studentscore', 'studentid', 'studentid');
    }

    //eloquent relationship
    public function studentsemester(){
        return $this->hasMany('App\Studentsemester', 'studentid', 'studentid');
    }

    //eloquent relationship
    public function recruitment(){
        return $this->hasMany('App\Recruitment', 'studentid', 'studentid');
    }

    // TO BE DELETED
    //Get all companies yang dilamar student
    public function getAllCompaniesApplied(){
        return Company::join('jobs', 'jobs.companyid', '=', 'companies.companyid')
            ->join('recruitments','jobs.id','=','recruitments.jobid')
            ->join('students','recruitments.studentid','=','students.studentid')
            ->whereNull('jobs.deleted_at')
            ->whereNull('recruitments.deleted_at')
            ->whereNull('students.deleted_at')
            ->where('students.studentid','=', $this->studentid)
            ->whereIn('recruitments.status', array('waiting','accepted','process'))
            ->where('recruitments.deleted_at', null)
            ->get();
    }





    //get student yang sudah accepted/approved saja
    public function getAcceptedStudents()
    {
    	return $this
		->join('recruitments','students.studentid','=','recruitments.studentid')
		->where('status','=',"approved")
		->orWhere('status','=',"accepted")
		->select('students.studentid as studentid','name','cv','phone','sex','email')
		->distinct()
		->get();
    }


    //get student yang  waiting saja
    public function getWaitingStudents()
    {
    	return $this
		->whereNotIn('students.studentid',
			function ($query)
			{
				$query
				->select('studentid')
				->from('recruitments')
				->whereRaw('status = "approved" or status = "accepted"');
			})
		->select('students.studentid as studentid','name','cv','phone','sex','email')
		->distinct()
		->get();
    }

    //get student yang digunakan untul apply dari department
    // dengan kriteria belum di approve atau di accept
    //belum melamar di jobid tersebut
    public function getStudentsForApply($jobid)
    {
    	return $this
		->whereNotIn('students.studentid',
			function ($query)
			{
				$query
				->select('studentid')
				->from('recruitments')
				->whereRaw('status = "approved" or status = "accepted"');
			})
		->whereNotIn('students.studentid',
			function ($query) use ($jobid)
			{
				$query
				->select('studentid')
				->from('recruitments as r')
				->whereRaw('jobid ='.$jobid);
			})
		->select('studentid as NIM','name as Name','sex as Sex','phone as Phone','email as Email','cv as CV')
		->get();

    }

    //hapus student
    //Cascade di semua informasi student (cvdate, cvskill, studentscore, studentsemester)
    //Delete Recruitment
    //Delete Student
    //Delete User
    public function deleteStudent($id)
    {
    	//Delete every student information
    	$cvDate = new Cvdateinformation;
    	$cvDate->deleteCvDates($id);

    	$cvSkill = new Cvskillinformation;
    	$cvSkill->deleteCvSkills($id);

    	$score = new Studentscore;
    	$score->deleteStudentScores($id);

    	$semester = new Studentsemester;
    	$semester->deleteSemesterScores($id);


    	//Delete in recruitment table
    	$rec = new Recruitment;
    	$rec->deleteRecruitmentByStudent($id);

    	//Delete in table student
    	$this->where('studentid', $id)->delete();

    	//Delete in table User
        $user = new User;
        $user->deleteUser($id);
    }

    //get student NIM + namanya
    public function getFullNameStudentASC(){
    	return $this->select('studentid as userid', DB::raw('CONCAT(studentid, " - ", name) as name'))->orderBy('name', 'asc')->get();
    }

    public function isProgramGanda(){
            if(strtolower(trim($this->degree()->first()->name)) == 'computer science and mathematics' || strtolower(trim($this->degree()->first()->name)) == 'computer science and statistics'){
                    return true;
            }
            else{
                    return false;
            }
    }

    public function isMasterTrack(){//edit
            if(strtolower(trim($this->degree()->first()->name)) == 'master track mti'){
                    return true;
            }
            else{
                    return false;
            }
    }

    public function isCyberSecurity(){
        if(strtolower(trim($this->degree()->first()->name)) == 'cyber security'){
                return true;
        }
        else{
                return false;
        }
    }

    public function isGAT(){
        if(strtolower(trim($this->degree()->first()->name)) == 'game application and technology'){
                return true;
        }
        else{
                return false;
        }
    }

    public function requiringDepartmentApproval(){
        return false;
//        if($this->isProgramGanda() || $this->isMasterTrack() || $this->isCyberSecurity() || $this->isGAT()){
//            return true;
//        }
//        else{
//            return false;
//        }
    }

    public function isDurationSixMonth(){
        if($this->isProgramGanda() || $this->isMasterTrack()){
            return true;
        }
        return false;
    }

    public function returnEmptyProfileInfo(){
        $arr = [];
        if(empty($this->name)){
			array_push($arr, 'Name');
		}
		if(empty($this->address)){
            array_push($arr, 'Address');
		}
		if(empty($this->sex)){
			array_push($arr, 'Gender');
		}
		if(empty($this->dob)){
			array_push($arr, 'Date of Birth');
		}
		if(empty($this->semester)){
			array_push($arr, 'Semester');
		}
		if(empty($this->email)){
			array_push($arr, 'Email');
		}
		if(empty($this->phone)){
			array_push($arr, 'Phone');
		}
		if(empty($this->photo)){
			array_push($arr, 'Profile Picture');
		}
		$student_grades = Studentsemester::where('studentid', $this->studentid)->get();
		if($student_grades->count() == 0){
			array_push($arr, 'IPS/IPK History (Generated)');
        }
        /** 
         * student grade not mandatory
         */
		$student_scores = Studentscore::where('studentid', $this->studentid)->get();
		if($student_scores->count() == 0){
			array_push($arr, 'Grades (Generated)');
        }
        
		$formal_educations = Cvdateinformation::where('studentid', $this->studentid)->where('type', '=', 'Formal')->get();
		if($formal_educations->count() == 0){
			array_push($arr, 'Formal Education');
        }
        /** 
         * skill not mandatory
         */
		// $computer_skills = Cvskillinformation::where('studentid', $this->studentid)->where('type', '=', 'Skills')->get();
		// if($computer_skills->count() == 0){
		// 	array_push($arr, 'Computer / Special Skills');
		// }
		return $arr;
    }
}

?>
