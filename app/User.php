<?php

namespace App;

//use Illuminate\Database\Eloquent\Model;
/*use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;*/
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

//class User extends Model implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract
class User extends Authenticatable
{
    use SoftDeletes;

    protected $table = 'users';

    protected $fillable =
    [
        'name', 'id', 'password',
    ];

    protected $hidden =
    [
        'password', 'remember_token',
    ];

    //eloquent relationship
    public function sent_mailbox(){
        return $this->hasOne('App\Mailbox', 'senderid', 'userid');
    }

    //eloquent relationship
    public function inbox(){
        return $this->hasOne('App\Mailbox', 'recipientid', 'userid');
    }

    //eloquent relationship
    public function feedback(){
        return $this->belongsTo('App\Feedback', 'userid', 'userid');
    }

    //eloquent relationship
    public function student(){
        return $this->hasOne('App\Student', 'studentid', 'userid');
    }

    //eloquent relationship
    public function company(){
        return $this->hasOne('App\Company', 'companyid', 'userid');
    }

    //eloquent relationship
    public function department(){
        return $this->hasOne('App\Department', 'departmentid', 'userid');
    }

    //Get list User berdasarkan role
    public function hasRole($role)
    {
        return User::where('role', $role)->get();
    }

    //Get data student
    /*public function getStudent($userid)
    {
        return $this
                ->join('students', 'users.userid', '=', 'students.studentid')
                ->where('students.studentid', '=', $userid)->first();
    }*/

    //Get data company
    public function getCompany($userid)
    {
        return $this->join('companies', 'users.userid', '=', 'companies.companyid')->where('companyid', '=', $userid)->first();
    }


    //get Companies / Student / Department first data
    public function getCompanyWhereRole($companyid, $role)
    {
        return $this->join('companies', 'companies.companyid', '=', 'users.userid')->where('role', '=', $role)->where('companyid', '=', $companyid)->first();
    }

    public function getStudentWhereRole($studentid, $role){
        return $this->join('students', 'students.studentid', '=', 'users.userid')->where('role', '=', $role)->where('studentid', '=', $studentid)->first();
    }

    public function getDepartmentWhereRole($departmentid, $role){
        return $this->join('departments', 'departments.departmentid', '=', 'users.userid')->where('role', '=', $role)->where('departmentid', '=', $departmentid)->first();
    }

    //Delete User
    public function deleteUser($userid)
     {
         $this->where('userid', $userid)->delete();
     }

     //get role untuk id yang dikirim
     public function selectOnlyRole($id){
        return $this->where('userid', '=', $id)->select('role')->first();
     }

}
