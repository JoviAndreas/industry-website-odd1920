<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class UniversitySupervisor extends Model
{
     use SoftDeletes;

     protected $table = 'university_supervisors';
     protected $dates = ['deleted_at'];

    //eloquent relationship
    public function student(){
        return $this->hasMany('App\Student', 'univ_supervisor_id', 'id');
    }

}

?>
