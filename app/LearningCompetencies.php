<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class LearningCompetencies extends Model
{
     use SoftDeletes;

     protected $table = 'learning_competencies';
     protected $dates = ['deleted_at'];
}
?>
