<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobPositionDegrees extends Model
{

    protected $table = 'job_positions_degrees';

    public function degree() {
        return $this->belongsTo('App\Degree', 'degree_id', 'id');
    }
}
