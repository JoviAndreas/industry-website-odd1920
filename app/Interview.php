<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Interview extends Model
{
    use SoftDeletes;

    protected $table = 'interviews';
    protected $dates = ['deleted_at'];

    //eloquent relationship
    public function interviewee(){
        return $this->belongsTo('App\Student', 'interviewee', 'studentid');
    }

    //eloquent relationship
    public function interviewer(){
        return $this->belongsTo('App\Company', 'interviewer', 'companyid');
    }

    //eloquent relationship
    public function recruitment(){
        return $this->belongsTo('App\Recruitment', 'recruitment_id', 'id');
    }

    //Dapetin data interview dari token
    public function getInterview($tokenid)
    {
    	return $this->where('token', '=', $tokenid)->first();
    }

    //Hapus data interview berdasarkan generated token
    public function deleteInterviewToken($tokenid){
    	$this->where('token', $tokenid)->delete();
    }

    //Dapetin list jadwal interview yang diterima dari perusahaan dengan id yang dikirim
    public function getInterviews($companyid)
    {
    	return $this
		->join('students as s', 's.studentid', '=', 'interviews.interviewee')
		->where('interviews.interviewer', '=', $companyid)
		->where('interviews.status','=','accept')
                        ->select('s.name as name', 'interviews.interviewee as NIM', 'interviews.date', 'interviews.time', 'interviews.location')
		->get();
    }

    //Hapus data interview berdasarkan companyid
    public function deleteInterviewCompany($companyid)
    {
        $this->where('companyid', $companyid)->delete();
    }

}

?>
