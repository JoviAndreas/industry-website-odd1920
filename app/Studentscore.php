<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Studentscore extends Model
{

     use SoftDeletes;

     protected $table = 'studentscores';
     protected $dates = ['deleted_at'];
    protected $fillable = ['studentid', 'code', 'name', 'grade'];

     //eloquent relationship
    public function student(){
        return $this->belongsTo('App\Student', 'studentid', 'studentid');
    }

     //get student scores
     public function getStudentScores($studentid)
     {
     	return $this->where('studentid', '=', $studentid)->get();
     }

     //delete student scores
     public function deleteStudentScores($studentid)
     {
	    $this->where('studentid', $studentid)->delete();
     }

}

?>