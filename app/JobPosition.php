<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobPosition extends Model
{
    use SoftDeletes;

    protected $table = 'job_positions';
    protected $dates = ['deleted_at'];

    //eloquent relationship
    public function jobpositiondegrees()
    {
        return $this->hasMany('App\JobPositionDegrees', 'job_position_id', 'id');
    }
}
