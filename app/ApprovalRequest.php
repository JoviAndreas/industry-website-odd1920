<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class ApprovalRequest extends Model
{

     use SoftDeletes;

     protected $table = 'approval_request';
     protected $dates = ['deleted_at'];

     protected $primaryKey = 'approval_request_id';

    //eloquent relationship
    public function recruitment(){
        return $this->belongsTo('App\Recruitment', 'recruitment_id', 'id');
    }

}

?>