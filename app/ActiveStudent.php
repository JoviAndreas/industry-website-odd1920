<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class ActiveStudent extends Model
{

     use SoftDeletes;

     protected $table = 'activestudents';
     protected $dates = ['deleted_at'];

}

?>