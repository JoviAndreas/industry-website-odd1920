<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class AutomatedService extends Model
{

     use SoftDeletes;

     protected $table = 'automated_services';
     protected $dates = ['deleted_at'];

}

?>
