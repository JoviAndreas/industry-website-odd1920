<?php

namespace App;

class LocalNavigation {
    public $label;
    public $url;
    public $route;

    public function __construct($label, $url, $route = null)
    {
        $this->label = $label;
        $this->url = $url;
        $this->route = $route;
    }

    public function __toString(){
        return $this->label . ' ' . $this->url . ' ' . $this->route;
    }
}