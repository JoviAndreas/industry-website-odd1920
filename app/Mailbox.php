<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Mailbox extends Model
{
    use SoftDeletes;

    protected $table = 'mailboxes';
    protected $dates = ['deleted_at'];

    //eloquent relationship
    public function sender(){
        return $this->belongsTo('App\User', 'senderid', 'userid');
    }

    //eloquent relationship
    public function recipient(){
        return $this->belongsTo('App\User', 'recipientid', 'userid');
    }

    public function getCountMailbox($userid){
    	return $this->whereIn('type', array('interview', 'mailbox'))->where('status', '=', 'unseen')->where('recipientid', '=', $userid)->count();
    }

    public function getCountMailboxCompany($userid){
    	return $this->where('type', '=', 'mailbox')->where('status', '=', 'unseen')->where('recipientid', '=', $userid)->count();
    }

    public function getCountNotif($userid){
    	return $this->where('type', '=', 'notif')->where('status', '=', 'unseen')->where('recipientid', '=', $userid)->count();
    }

    public function getNotif($userid){
        return $this->where('type', '=', 'notif')
                ->where('recipientid', '=', $userid)
                ->orderBy('id', 'desc')
                ->take(4)
                ->get();
    }

    public function getLastMessage(){
        return $this->orderBy('id', 'desc')->select('id', 'senderid', 'recipientid')->first();
    }

    public function getInboxStudent($studentid){
        return $this->leftJoin('companies as c', 'mailboxes.senderid', '=', 'c.companyid')
                ->where('recipientid', '=', $studentid)
                ->whereIn('type', array('mailbox', 'interview'))
                ->select('mailboxes.created_at','mailboxes.date','mailboxes.id','mailboxes.message','mailboxes.recipientid','mailboxes.senderid','mailboxes.status','mailboxes.subject','mailboxes.token','mailboxes.type','mailboxes.updated_at', 'c.name')
                ->orderBy('date', 'desc')
                ->get();
    }

    public function getSentStudent($studentid){
        return $this->leftJoin('companies as c', 'mailboxes.recipientid', '=', 'c.companyid')
                ->where('senderid', '=', $studentid)
                ->whereIn('type', array('mailbox', 'interview'))
                ->select('mailboxes.created_at','mailboxes.date','mailboxes.id','mailboxes.message','mailboxes.recipientid','mailboxes.senderid','mailboxes.status','mailboxes.subject','mailboxes.token','mailboxes.type','mailboxes.updated_at', 'c.name')
                ->orderBy('date', 'desc')
                ->get();
    }

    public function getInboxCompanyandDepartment($userid){
        return $this->where('recipientid', '=', $userid)->whereIn('type', array('mailbox', 'interview'))
                ->select('mailboxes.date','mailboxes.message','mailboxes.recipientid','mailboxes.senderid as name','mailboxes.status','mailboxes.subject','mailboxes.token','mailboxes.type')
                ->orderBy('date', 'desc')
                ->get();
    }

    public function getSentCompanyandDepartment($userid){
        return $this->where('senderid', '=', $userid)
                ->whereIn('type', array('mailbox', 'interview'))
                ->select('mailboxes.date','mailboxes.message','mailboxes.recipientid as name','mailboxes.senderid','mailboxes.status','mailboxes.subject','mailboxes.token','mailboxes.type')
                ->orderBy('date', 'desc')
                ->get();
    }

    public function viewInboxUser($tokenid){
        return $this->join('users', 'users.userid', '=', 'mailboxes.senderid')->where('token', '=', $tokenid)->select('role')->first();
    }

    public function viewInboxCompany($tokenid){
        return $this->join('companies', 'mailboxes.senderid', '=', 'companies.companyid')->where('token', '=', $tokenid)->first();
    }

    public function viewInboxStudent($tokenid){
        return $this->join('students', 'mailboxes.senderid', '=', 'students.studentid')->where('token', '=', $tokenid)->first();
    }

    public function viewInboxDepartment($tokenid){
        return $this->join('departments', 'mailboxes.senderid', '=', 'departments.departmentid')->where('token', '=', $tokenid)->first();
    }

    public function viewSentUser($tokenid){
        return $this->join('users', 'users.userid', '=', 'mailboxes.recipientid')->where('token', '=', $tokenid)->select('role')->first();
    }

    public function viewSentCompany($tokenid){
        return $this->join('companies', 'mailboxes.recipientid', '=', 'companies.companyid')->where('token', '=', $tokenid)->first();
    }

    public function viewSentStudent($tokenid){
        return $this->join('students', 'mailboxes.recipientid', '=', 'students.studentid')->where('token', '=', $tokenid)->first();
    }

    public function viewSentDepartment($tokenid){
        return $this->join('departments', 'mailboxes.recipientid', '=', 'departments.departmentid')->where('token', '=', $tokenid)->first();
    }
}

?>
