<?php

namespace App\Console\Commands;

use App\Http\Controllers\APIController;
use App\Http\Controllers\StudentActivationController;
use Illuminate\Console\Command;

class SyncStudent extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:syncStudent';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $campusMapping = [
            'kmg',
            'jwc',
            'mlg',
            'bdg'
        ];

        $apiController = new APIController;
        foreach ($campusMapping as $campus) {
            $result = $apiController->insertTemporaryStudentHeaderAction($campus, '2010');
            if ($result == 1) {
                $this->comment($campus . ' Synced');
            }
        }

        $studentActivationController = new StudentActivationController();
        $studentActivationController->syncHeader();
        $this->comment('All Students Activated');
    }
}
