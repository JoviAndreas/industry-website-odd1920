<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Recruitment;
use App\Mailbox;
use App\Interview;
use App\AutomatedService;
use Carbon\Carbon;
use App\Services\EmailService;

class DeleteOld extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:deleteOld';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'To delete old job applications';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if(AutomatedService::where('id', '=', 1)->first()->status == 'running'){
            //check apabila ada data recruitment yang statusnya masih waiting / process selama 2 minggu terakhir (gak diupdate sama company / sama jurusan)
            $oldestDate = Carbon::now()->subDays(14);

            $deleted1 = Recruitment::whereIn('status', ['waiting'])->whereDate('updated_at', '<', $oldestDate)->get();

            foreach ($deleted1 as $d) {
                if($d->interview()->get()->count() > 0){
                    continue;
                }

                EmailService::createNewUnapplyAutoEmail($d);

                // send email to student that the status has been Changed
                EmailService::createNewChangeStatusEmail($d, "Unapplied");

                // delete semua jenis request terkait job application ini
        		$d->approval_request()->delete();
        		$d->unaccept_request()->delete();
                $d->unreject_request()->delete();

        		// delete interview
        		$d->interview()->delete();

                $d->unapplied_by = 'system';
                $d->unapplied_on = Carbon::now();
                $d->save();

                // delete all mailbox related to this recruitment
        		Mailbox::where('senderid', '=', $d->studentid)
                    ->where('type', '=', 'notif')
        			->where('message', 'like', '%'.$d->job()->first()->name.'%')
        			->delete();

                //notif to the company
                $mailboxUn = new Mailbox;
        		$mailboxUn->senderid = $d->studentid;
        		$mailboxUn->recipientid = $d->job()->first()->companyid;
        		$mailboxUn->subject = 'You have new cancelation applicant';
        		$mailboxUn->date = date("Y-m-d H:i:s");
        		$mailboxUn->message = 'Job application from '.$d->student()->first()->name.' as '. $d->job()->first()->name .' is automatically unapplied.';
        		$mailboxUn->status = 'unseen';
        		$mailboxUn->type = 'notif';
        		$mailboxUn->token = '';
        		$mailboxUn->save();

                //notif to the user
                $mailboxUn = new Mailbox;
        		$mailboxUn->senderid = $d->job()->first()->companyid;
        		$mailboxUn->recipientid = $d->studentid;
        		$mailboxUn->subject = 'Your job application is automatically unapplied';
        		$mailboxUn->date = date("Y-m-d H:i:s");
        		$mailboxUn->message = 'Job application to '.$d->job()->first()->company()->first()->name.' as '. $d->job()->first()->name .' is automatically unapplied.';
        		$mailboxUn->status = 'unseen';
        		$mailboxUn->type = 'notif';
        		$mailboxUn->token = '';
        		$mailboxUn->save();

                $d->delete();
            }
        }

        if(AutomatedService::where('id', '=', 2)->first()->status == 'running'){
            $oldestDate = Carbon::now()->subDays(14);

            $deleted2 = Recruitment::whereIn('status', ['process'])->whereDate('processed_on', '<', $oldestDate)->get();

            foreach ($deleted2 as $d) {
                if($d->interview()->get()->count() > 0){
                    continue;
                }

                EmailService::createNewUnapplyAutoEmail($d);

                // send email to student that the status has been Changed
                EmailService::createNewChangeStatusEmail($d, "Unapplied");

                // delete semua jenis request terkait job application ini
        		$d->approval_request()->delete();
        		$d->unaccept_request()->delete();
                $d->unreject_request()->delete();

        		// delete interview
        		$d->interview()->delete();

                $d->unapplied_by = 'system';
                $d->unapplied_on = Carbon::now();
                $d->save();

                // delete all mailbox related to this recruitment
        		Mailbox::where('senderid', '=', $d->studentid)
                    ->where('type', '=', 'notif')
        			->where('message', 'like', '%'.$d->job()->first()->name.'%')
        			->delete();

                //notif to the company
                $mailboxUn = new Mailbox;
        		$mailboxUn->senderid = $d->studentid;
        		$mailboxUn->recipientid = $d->job()->first()->companyid;
        		$mailboxUn->subject = 'You have new cancelation applicant';
        		$mailboxUn->date = date("Y-m-d H:i:s");
        		$mailboxUn->message = 'Job application from '.$d->student()->first()->name.' as '. $d->job()->first()->name .' is automatically unapplied.';
        		$mailboxUn->status = 'unseen';
        		$mailboxUn->type = 'notif';
        		$mailboxUn->token = '';
        		$mailboxUn->save();

                //notif to the user
                $mailboxUn = new Mailbox;
        		$mailboxUn->senderid = $d->job()->first()->companyid;
        		$mailboxUn->recipientid = $d->studentid;
        		$mailboxUn->subject = 'Your job application is automatically unapplied';
        		$mailboxUn->date = date("Y-m-d H:i:s");
        		$mailboxUn->message = 'Job application to '.$d->job()->first()->company()->first()->name.' as '. $d->job()->first()->name .' is automatically unapplied.';
        		$mailboxUn->status = 'unseen';
        		$mailboxUn->type = 'notif';
        		$mailboxUn->token = '';
        		$mailboxUn->save();

                $d->delete();
            }
        }

        if(AutomatedService::where('id', '=', 3)->first()->status == 'running'){
            $oldestDateForInterview = Carbon::now()->subDays(7);
            $old_interview_schedule = Interview::join('recruitments', 'interviews.recruitment_id', '=', 'recruitments.id')
                ->join('jobs', 'jobs.id', '=', 'recruitments.jobid')
                ->whereNull('jobs.deleted_at')
                ->whereNull('recruitments.deleted_at')
                ->whereIn('recruitments.status', ['process'])
                ->whereIn('interviews.status', ['waiting'])
                ->whereNotExists(function($query){
                    $query->select("*")
                        ->from('interviews')
                        ->whereIn('status', ['accept', 'reschedule'])
                        ->whereRaw('interviews.interviewee = recruitments.studentid and interviews.interviewer = jobs.companyid');
                })
                ->whereDate('interviews.created_at', '<', $oldestDateForInterview)
                ->select('recruitments.id as recruitment_id')
                ->get();

            foreach ($old_interview_schedule as $o) {
                $recruitment = Recruitment::where('id', '=', $o->recruitment_id)->first();
                $recruitment->rejected_on = Carbon::now();
                $recruitment->rejected_by = 'system';
                $recruitment->status = 'rejected';

                EmailService::createNewRejectAutoEmail($recruitment);

                // send email to student that the status has been Changed
                EmailService::createNewChangeStatusEmail($recruitment, "Rejected");

                //notif to the company
                $mailboxUn = new Mailbox;
        		$mailboxUn->senderid = $recruitment->studentid;
        		$mailboxUn->recipientid = $recruitment->job()->first()->companyid;
        		$mailboxUn->subject = 'You have new rejected application';
        		$mailboxUn->date = date("Y-m-d H:i:s");
        		$mailboxUn->message = 'Job application from '.$recruitment->student()->first()->name.' as '. $recruitment->job()->first()->name .' is automatically rejected (student did not accept your interview invitation within one week).';
        		$mailboxUn->status = 'unseen';
        		$mailboxUn->type = 'notif';
        		$mailboxUn->token = '';
        		$mailboxUn->save();

                //notif to the user
                $mailboxUn = new Mailbox;
        		$mailboxUn->senderid = $recruitment->job()->first()->companyid;
        		$mailboxUn->recipientid = $recruitment->studentid;
        		$mailboxUn->subject = 'Your job application is automatically unapplied';
        		$mailboxUn->date = date("Y-m-d H:i:s");
        		$mailboxUn->message = 'Job application to '.$recruitment->job()->first()->company()->first()->name.' as '. $recruitment->job()->first()->name .' is automatically unapplied (you did not accept the interview invitation within one week).';
        		$mailboxUn->status = 'unseen';
        		$mailboxUn->type = 'notif';
        		$mailboxUn->token = '';
        		$mailboxUn->save();

                $recruitment->save();
            }
        }
    }
}
