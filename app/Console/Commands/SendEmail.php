<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Recruitment;
use Carbon\Carbon;
use Mail;
use App\Email;
use App\User;
use App\Student;
use App\Company;

class SendEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:sendEmail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'To send emails queue';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $emails = Email::where('status', '=', 'pending')->get();
        foreach ($emails as $e) {
            if(filter_var($e->to_email, FILTER_VALIDATE_EMAIL) === FALSE){
                continue;
            }
            $data = null;
            if(!empty($e->data)){
                $data = json_decode($e->data, TRUE);
            }
            Mail::send($e->view, $data, function ($m) use ($e)
            {
                $m->from($e->from_email, $e->from_name);
                $m->to($e->to_email, $e->to_name)->subject($e->subject);
            });
            if(count(Mail::failures()) == 0) {
                //success
                $e->status = 'sent';
                $e->save();
            }
        }
    }
}
