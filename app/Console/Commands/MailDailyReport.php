<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Company;
use App\Student;
use App\Job;
use App\Recruitment;
use App\Services\EmailService;
use Carbon\Carbon;

class MailDailyReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:mailDailyReport';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create daily report mail to be sent to students and companies';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // send daily report to Company
        // this will tell the company
        // how much new job application which are not processed since yesterday
        $companies = Company::join('users', 'users.userid', '=', 'companies.companyid')
            ->whereNull('users.deleted_at')
            ->where('users.status', '=', 'active')
            ->select(
                'companies.name as name',
                'companies.companyid as companyid',
                'companies.email as email'
                )
            ->get();
        foreach($companies as $c){
            $oldestDate = Carbon::yesterday();

            $job_applications_yesterday = Recruitment::join('jobs', 'jobs.id', '=', 'recruitments.jobid')
                ->join('companies', 'companies.companyid', '=', 'jobs.companyid')
                ->whereNull('companies.deleted_at')
                ->whereNull('jobs.deleted_at')
                ->where('recruitments.status', '=', 'waiting')
                ->where('companies.companyid', '=', $c->companyid)
                ->whereDate('recruitments.created_at', '>=', $oldestDate)
                ->get()
                ->count();

            if($job_applications_yesterday > 0){
                EmailService::createNewDailyJobApplyReportEmail($c, $job_applications_yesterday);
            }
        }

        // send daily report to Student
        // this will tell the student
        // how much new job posted since yesterday
        $students = Student::join('users', 'users.userid', '=', 'students.studentid')
            ->whereNull('users.deleted_at')
            ->where('users.status', '=', 'active')
            ->select(
                'students.name as name',
                'students.studentid as studentid',
                'students.email as email',
                'students.degreeid as degreeid'
                )
            ->get();

        foreach($students as $s){
            $oldestDate = Carbon::yesterday();

            $new_job_yesterday = Job::join('companies', 'companies.companyid', '=', 'jobs.companyid')
    			->join('jobsdegrees', 'jobsdegrees.jobid', '=', 'jobs.id')
    			->whereNull('companies.deleted_at')
    			->whereNull('jobsdegrees.deleted_at')
    			->whereDate('jobs.created_at', '>=', $oldestDate)
    			->where('jobsdegrees.degreeid', '=', $s->degreeid)
    			->get()
                ->count();

            if($new_job_yesterday > 0){
                EmailService::createNewDailyNewJobsReportEmail($s, $new_job_yesterday);
            }
        }

        // send how many students will be expired after a week
        $companies = Company::join('users', 'users.userid', '=', 'companies.companyid')
            ->whereNull('users.deleted_at')
            ->where('users.status', '=', 'active')
            ->select(
                'companies.companyid',
                'companies.name',
                'companies.email'
                )
            ->get();

        foreach ($companies as $c) {
            $oldestDate = Carbon::now()->addDays(7);

            $arr_list_student = [];

            $will_be_deleted = Recruitment::whereIn('status', ['waiting'])
                ->join('students', 'students.studentid', '=', 'recruitments.studentid')
                ->join('jobs', 'jobs.id', '=', 'recruitments.jobid')
                ->whereNull('students.deleted_at')
                ->whereNull('jobs.deleted_at')
                ->where('jobs.companyid', '=', $c->companyid)
                ->whereRaw('DATE_ADD(recruitments.created_at, INTERVAL 30 DAY) < "'.$oldestDate.'"')
                ->select(
                    'students.studentid as student_id',
                    'students.name as student_name',
                    'jobs.name as job_name',
                    'recruitments.status as status',
                    DB::Raw('DATE_ADD(recruitments.created_at, INTERVAL 30 DAY) as expired_at')
                    )
                ->orderBy('expired_at', 'asc')
                ->get();

            foreach ($will_be_deleted as $w) {
                array_push($arr_list_student, [
                    'student_id' => $w->student_id,
                    'student_name' => $w->student_name,
                    'job_name' => $w->job_name,
                    'status' => $w->status,
                    'expired_at' => $w->expired_at
                ]);
            }

            $will_be_deleted2 = Recruitment::whereIn('status', ['waiting'])
                ->join('students', 'students.studentid', '=', 'recruitments.studentid')
                ->join('jobs', 'jobs.id', '=', 'recruitments.jobid')
                ->whereNull('students.deleted_at')
                ->whereNull('jobs.deleted_at')
                ->where('jobs.companyid', '=', $c->companyid)
                ->whereRaw('DATE_ADD(recruitments.processed_on, INTERVAL 30 DAY) < "'.$oldestDate.'"')
                ->select(
                    'students.studentid as student_id',
                    'students.name as student_name',
                    'jobs.name as job_name',
                    'recruitments.status as status',
                    DB::Raw('DATE_ADD(recruitments.processed_on, INTERVAL 30 DAY) as expired_at')
                    )
                ->orderBy('expired_at', 'asc')
                ->get();

            foreach ($will_be_deleted2 as $w) {
                array_push($arr_list_student, [
                    'student_id' => $w->student_id,
                    'student_name' => $w->student_name,
                    'job_name' => $w->job_name,
                    'status' => $w->status,
                    'expired_at' => $w->expired_at
                ]);
            }

            if(!empty($arr_list_student)){
                // send email if student list is not empty
                EmailService::createNewUnapplyInAWeekReportEmail($c,$arr_list_student);
            }
        }
    }
}
