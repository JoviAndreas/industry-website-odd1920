<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //Commands\Inspire::class
        Commands\DeleteOld::class,
        Commands\SendEmail::class,
        Commands\MailDailyReport::class,
        Commands\SyncStudent::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        $schedule->command('command:deleteOld')
            ->withoutOverlapping()
            ->hourly()
            ->sendOutputTo("errorDeleteOld.log");

        $schedule->command('command:syncStudent')
            ->withoutOverlapping()
            ->daily()
            ->sendOutputTo("errorSyncStudent.log");

//        $schedule->command('command:sendEmail')
//            ->withoutOverlapping()
//            ->everyMinute()
//            ->sendOutputTo("errorSendEmail.log");
//
//        $schedule->command('command:mailDailyReport')
//            ->withoutOverlapping()
//            ->dailyAt('00:10')
//            ->sendOutputTo("errorMailDaily.log");
    }
}
