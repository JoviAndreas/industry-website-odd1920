<?php

namespace App\Services;

use App\Degree;

class DepartmentService {

    // return array of degrees for a given HOP
    public static function get_degree_list($hop_data){
        $list_degree = [];

        $degrees = null;
        if($hop_data == 'all'){
            $degrees = Degree::get();
        }
        else{
            $arr_hop = explode(';', $hop_data);
            $degrees = Degree::whereIn('id', $arr_hop)->get();
        }

        foreach($degrees as $d){
            array_push($list_degree, $d->id);
        }

        return $list_degree;
    }

}

?>
