<?php

namespace App\Services;

use App\User;
use App\Recruitment;
use App\Student;
use App\Department;
use App\Company;
use App\Job;
use App\Email;
use App\Interview;
use App\UnacceptRequest;
use App\UnrejectRequest;
use App\ApprovalRequest;
use App\Mailbox;

class EmailService {

    // param notes:
    // data -> json encoded object
    // message -> null if you want to load it from view
    public static function createNewEmail($to, $name, $userid, $subject, $type, $message, $view, $data){
        $new_email = new Email;
        $new_email->to_email = $to;
        $new_email->to_name = $name;
        $new_email->to_userid = $userid;
        $new_email->subject = $subject;
        $new_email->from_email = env('MAIL_FROM', 'internshipsocs@binus.ac.id');
        $new_email->from_name = 'SOCS 3+1 Internship Recruitment';
        $new_email->type = $type;
        $new_email->message = $message;
        $new_email->status = 'pending';
        $new_email->data = $data;
        $new_email->view = $view;
        $new_email->save();
    }

    public static function createNewAccountApprovalEmail(User $usr){
        $data = json_encode(array('user' => $usr->toArray()));
        EmailService::createNewEmail(
            $usr->email,
            $usr->name,
            $usr->userid,
            'Account Registration 3+1 Internship',
            'approval',
            null,
            'email.approveaccount',
            $data
        );
    }

    public static function createNewAccountRejectionEmail(User $usr){
        $data = json_encode(array('user' => $usr->toArray()));
        EmailService::createNewEmail(
            $usr->email,
            $usr->name,
            $usr->userid,
            'Account Registration 3+1 Internship',
            'rejection',
            null,
            'email.rejectaccount',
            $data
        );
    }

    public static function createNewUnapplyAutoEmail(Recruitment $rec){
        $sent_data = [
            "company_name" => $rec->job()->first()->company()->first()->name,
            "job_name" => $rec->job()->first()->name,
            "student_name" => $rec->student()->first()->name,
            "student_id" => $rec->student()->first()->studentid
        ];

        $data = json_encode(array('rec' => $sent_data));

        $email = $rec->job()->first()->company()->first()->email;
        $name = $rec->job()->first()->company()->first()->name;
        $userid = $rec->job()->first()->company()->first()->companyid;
        EmailService::createNewEmail(
            $email,
            $name,
            $userid,
            'Email Notification 3+1 Internship',
            'unapply_auto',
            null,
            'email.unapplyauto',
            $data
        );
    }

    public static function createNewRejectAutoEmail(Recruitment $rec){
        $sent_data = [
            "company_name" => $rec->job()->first()->company()->first()->name,
            "job_name" => $rec->job()->first()->name,
            "student_name" => $rec->student()->first()->name,
            "student_id" => $rec->student()->first()->studentid
        ];

        $data = json_encode(array('rec' => $sent_data));

        $email = $rec->job()->first()->company()->first()->email;
        $name = $rec->job()->first()->company()->first()->name;
        $userid = $rec->job()->first()->company()->first()->companyid;
        EmailService::createNewEmail(
            $email,
            $name,
            $userid,
            'Email Notification 3+1 Internship',
            'reject_auto',
            null,
            'email.rejectauto',
            $data
        );
    }

    // param notes:
    // student -> student who is accepted
    // approved_company_id -> company who accept the student
    public static function createNewApprovedOtherEmail(Student $student, $approved_company_id){
        $array_company = [];
        // for saving all companies whom the student apply to
        foreach($student->recruitment()->get() as $rec){
            $companyid = $rec->job()->first()->company()->first()->companyid;
            if($companyid != $approved_company_id){
                if(array_search($companyid, $array_company) === FALSE){
                    array_push($array_company, $companyid);
                }
            }
        }

        foreach($array_company as $com){
            $company = Company::where('companyid', '=', $com)->first();

            $sent_data = [
                "company_name" => $company->name,
                "student_name" => $student->name,
                "student_id" => $student->studentid
            ];

            $data = json_encode(array('data' => $sent_data));

            $email = $company->email;
            $name = $company->name;
            $userid = $company->companyid;
            EmailService::createNewEmail(
                $email,
                $name,
                $userid,
                'Email Notification 3+1 Internship',
                'approved_other',
                null,
                'email.approvedother',
                $data
            );
        }
    }

    public static function createNewDailyJobApplyReportEmail(Company $company, $count_of_job_application){
        $sent_data = [
            "company_name" => $company->name,
            "count_of_job_application" => $count_of_job_application
        ];

        $data = json_encode(array('data' => $sent_data));

        $email = $company->email;
        $name = $company->name;
        $userid = $company->companyid;
        EmailService::createNewEmail(
            $email,
            $name,
            $userid,
            'Email Notification 3+1 Internship',
            'daily_job_apply',
            null,
            'email.dailyjobapply',
            $data
        );
    }

    public static function createNewDailyNewJobsReportEmail(Student $student, $count_of_new_jobs){
        $sent_data = [
            "student_name" => $student->name,
            "student_id" => $student->studentid,
            "count_of_new_jobs" => $count_of_new_jobs
        ];

        $data = json_encode(array('data' => $sent_data));

        $email = $student->email;
        $name = $student->name;
        $userid = $student->studentid;
        EmailService::createNewEmail(
            $email,
            $name,
            $userid,
            'Email Notification 3+1 Internship',
            'daily_new_jobs',
            null,
            'email.dailynewjobs',
            $data
        );
    }

    public static function createNewChangeStatusEmail(Recruitment $recruitment, $status){
        $sent_data = [
            "company_name" => $recruitment->job()->first()->company()->first()->name,
            "job_name" => $recruitment->job()->first()->name,
            "student_name" => $recruitment->student()->first()->name,
            "student_id" => $recruitment->student()->first()->studentid,
            "status" => $status
        ];

        $data = json_encode(array('rec' => $sent_data));

        $email = $recruitment->student()->first()->email;
        $name = $recruitment->student()->first()->name;
        $userid = $recruitment->student()->first()->studentid;
        EmailService::createNewEmail(
            $email,
            $name,
            $userid,
            'Email Notification 3+1 Internship',
            'change_status',
            null,
            'email.changestatus',
            $data
        );
    }

    public static function createNewInterviewInvitationEmail(Interview $interview){
        $sent_data = [
            "company_name" => $interview->interviewer()->first()->name,
            "job_name" => $interview->recruitment()->first()->job()->first()->name,
            "student_name" => $interview->interviewee()->first()->name,
            "student_id" => $interview->interviewee()->first()->studentid
        ];

        $data = json_encode(array('interview' => $sent_data));

        $email = $interview->interviewee()->first()->email;
        $name = $interview->interviewee()->first()->name;
        $userid = $interview->interviewee()->first()->studentid;
        EmailService::createNewEmail(
            $email,
            $name,
            $userid,
            'Email Notification 3+1 Internship',
            'interview_invitation',
            null,
            'email.interviewinvitation',
            $data
        );
    }

    public static function createNewUnapplyInAWeekReportEmail(Company $company, $arr_list_student){
        $sent_data = [
            "company_name" => $company->name,
            "list_student" => $arr_list_student
        ];

        $data = json_encode(array('data' => $sent_data));

        $email = $company->email;
        $name = $company->name;
        $userid = $company->companyid;
        EmailService::createNewEmail(
            $email,
            $name,
            $userid,
            'Email Notification 3+1 Internship',
            'unapply_in_a_week',
            null,
            'email.unapplyinaweek',
            $data
        );
    }

    public static function createNewUnacceptRequestEmail(UnacceptRequest $req){
        $departments = Department::join('users', 'users.userid', '=', 'departments.departmentid')
            ->whereNull('users.deleted_at')
            ->where('head_of_program', '=', 'all')
            ->select(
                'departments.name as department_name',
                'departments.departmentid as department_id',
                'departments.email as department_email'
                )
            ->get();

        $student = $req->recruitment()->first()->student()->first();
        $job = $req->recruitment()->first()->job()->first();
        $company = $job->company()->first();
        $degree = $student->degree()->first();

        foreach ($departments as $d) {
            $sent_data = [
                "department_name" => $d->department_name,
                "student_id" => $student->studentid,
                "student_name" => $student->name,
                "student_program" => $degree->name,
                "job_name" => $job->name,
                "company_name" => $company->name,
                "reason" => $req->reason
            ];

            $data = json_encode(array('data' => $sent_data));

            $email = $d->department_email;
            $name = $d->department_name;
            $userid = $d->department_id;
            EmailService::createNewEmail(
                $email,
                $name,
                $userid,
                'Email Notification 3+1 Internship',
                'unaccept_request',
                null,
                'email.unacceptrequest',
                $data
            );
        }
    }

    public static function createNewUnrejectRequestEmail(UnrejectRequest $req){
        $departments = Department::join('users', 'users.userid', '=', 'departments.departmentid')
            ->whereNull('users.deleted_at')
            ->where('head_of_program', '=', 'all')
            ->select(
                'departments.name as department_name',
                'departments.departmentid as department_id',
                'departments.email as department_email'
                )
            ->get();

        $student = $req->recruitment()->first()->student()->first();
        $job = $req->recruitment()->first()->job()->first();
        $company = $job->company()->first();
        $degree = $student->degree()->first();

        foreach ($departments as $d) {
            $sent_data = [
                "department_name" => $d->department_name,
                "student_id" => $student->studentid,
                "student_name" => $student->name,
                "student_program" => $degree->name,
                "job_name" => $job->name,
                "company_name" => $company->name,
                "reason" => $req->reason
            ];

            $data = json_encode(array('data' => $sent_data));

            $email = $d->department_email;
            $name = $d->department_name;
            $userid = $d->department_id;
            EmailService::createNewEmail(
                $email,
                $name,
                $userid,
                'Email Notification 3+1 Internship',
                'unreject_request',
                null,
                'email.unrejectrequest',
                $data
            );
        }
    }

    public static function createNewApprovalRequestEmail(ApprovalRequest $req){
        $student = $req->recruitment()->first()->student()->first();
        $job = $req->recruitment()->first()->job()->first();
        $company = $job->company()->first();
        $degree = $student->degree()->first();

        $departments = Department::join('users', 'users.userid', '=', 'departments.departmentid')
            ->whereNull('users.deleted_at')
            ->select(
                'departments.name as department_name',
                'departments.departmentid as department_id',
                'departments.email as department_email',
                'departments.head_of_program as hop_data'
                )
            ->get();

        foreach ($departments as $d) {
            if($d->hop_data == 'all'){
                continue;
            }

            $list_degree = DepartmentService::get_degree_list($d->hop_data);

            if(array_search($degree->id, $list_degree, FALSE) === FALSE){
                continue;
            }

            $sent_data = [
                "department_name" => $d->department_name,
                "student_id" => $student->studentid,
                "student_name" => $student->name,
                "student_program" => $degree->name,
                "job_name" => $job->name,
                "company_name" => $company->name
            ];

            $data = json_encode(array('data' => $sent_data));

            $email = $d->department_email;
            $name = $d->department_name;
            $userid = $d->department_id;
            EmailService::createNewEmail(
                $email,
                $name,
                $userid,
                'Email Notification 3+1 Internship',
                'approval_request',
                null,
                'email.approvalrequest',
                $data
            );
        }
    }

    public static function createNewCompanyMessageEmail(Mailbox $mailbox){
        $user_recipient = $mailbox->recipient()->first();
        $company_recipient = $user_recipient->company()->first();

        $user_sender = $mailbox->sender()->first();
        $student_sender = $user_sender->student()->first();

        $sent_data = [
            "recipient_company_name" => $company_recipient->name,
            "sender_student_id" => $student_sender->studentid,
            "sender_student_name" => $student_sender->name,
            "subject" => $mailbox->subject
        ];

        $data = json_encode(array('data' => $sent_data));

        $email = $company_recipient->email;
        $name = $company_recipient->name;
        $userid = $company_recipient->companyid;
        EmailService::createNewEmail(
            $email,
            $name,
            $userid,
            'Email Notification 3+1 Internship',
            'company_message',
            null,
            'email.companymessage',
            $data
        );
    }

}
