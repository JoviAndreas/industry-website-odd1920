<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Studentsemester extends Model
{

    // use SoftDeletes;

    protected $table = 'studentsemesters_temp';
    // protected $dates = ['deleted_at'];

    //eloquent relationship
    public function student(){
        return $this->belongsTo('App\Student', 'studentid', 'studentid');
    }

    //Get list semester scores untuk student tertentu
    public function getSemesterScores($studentid)
    {
        return $this
            ->where('studentid', '=', $studentid)
            ->orderBy('semester', 'asc')
            ->get();
    }

    //Delete semester scores dari student tertentu
    public function deleteSemesterScores($studentid)
    {
        $this->where('studentid', $studentid)->delete();
    }

    //Get Latest semester scores untuk student tertentu
    public function getLatestSemesterScore($studentid)
    {
        return $this
            ->where('studentid', '=', $studentid)
            ->orderBy('semester', 'desc')
            ->first();
    }
}

?>