<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Supervisor extends Model
{

     use SoftDeletes;

     protected $table = 'supervisors';
     protected $dates = ['deleted_at'];

     //eloquent relationship
    public function company(){
        return $this->belongsTo('App\Company', 'companyid', 'companyid');
    }

}

?>