<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Feedback extends Model
{
   	use SoftDeletes;

    	protected $table = 'feedbacks';
    	protected $dates = ['deleted_at'];

    	//eloquent relationship
	public function user(){
		return $this->belongsTo('App\User', 'userid', 'userid');
	}
}

?>