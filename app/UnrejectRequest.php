<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class UnrejectRequest extends Model
{

     use SoftDeletes;

     protected $table = 'unreject_request';
     protected $dates = ['deleted_at'];

     protected $primaryKey = 'request_id';

    //eloquent relationship
    public function recruitment(){
        return $this->belongsTo('App\Recruitment', 'recruitment_id', 'id');
    }

}

?>
