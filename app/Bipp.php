<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bipp extends Model
{
    use SoftDeletes;
    protected $table = 'bipp';
    protected $dates = ['deleted_at'];
    public function companies() {
        return $this->hasMany('App\Company', 'id', 'bipp_id');
    }
    public function minimumIPK() {
        return $this->first()->min_ipk ?? 0;
    }
}
